package com.navdy.hud.app.maps.here;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;

import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.MatchedGeoPosition;
import com.navdy.hud.app.debug.DriveRecorder;
import com.navdy.hud.app.device.gps.GpsConstants;
import com.navdy.hud.app.device.gps.GpsUtils;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.MapEvents$LocationFix;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Subscribe;

import com.navdy.hud.app.R;

import java.io.File;
import java.io.FileOutputStream;

import static com.navdy.hud.app.HudApplication.*;
import static com.navdy.hud.app.device.gps.GpsManager.MOCK_PROVIDER;

public class HereLocationFixManager {
    final private static java.text.SimpleDateFormat DATE_FORMAT;
    final private static byte[] RECORD_MARKER;
    final private static com.navdy.service.library.log.Logger sLogger;
    private android.location.LocationListener androidGpsLocationListener;
    final private com.squareup.otto.Bus bus;
    private Runnable checkLocationFix;
    private long counter;
    private android.location.LocationListener debugTTSLocationListener;
    private boolean firstLocationFixCheck;
    private com.here.android.mpa.common.GeoPosition geoPosition;
    private android.os.Handler handler;
    private volatile boolean hereGpsSignal;
    private volatile boolean isRecording;
    private android.location.Location lastAndroidLocation;
    private long lastAndroidLocationPostTime;
    private long lastAndroidLocationTime;
    private com.navdy.hud.app.device.gps.GpsUtils.GpsSwitch lastGpsSwitchEvent;
    private long lastHerelocationUpdateTime;
    private volatile long lastLocationTime;
    private volatile boolean locationFix;
    final private android.location.LocationListener locationListener;
    private com.here.android.mpa.common.PositioningManager.LocationMethod locationMethod;
    private com.here.android.mpa.mapping.MapMarker rawLocationMarker;
    public com.here.android.mpa.mapping.MapMarker navdyLocationMarker;
    private boolean rawLocationMarkerAdded;
    private java.io.FileOutputStream recordingFile;
    private volatile String recordingLabel;
    final private SpeedManager speedManager;
    private LocationManager locationManager;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereLocationFixManager.class);
        DATE_FORMAT = new java.text.SimpleDateFormat("yyyy-MM-dd'_'HH_mm_ss.SSS", java.util.Locale.US);
        RECORD_MARKER = "<< Marker >>\n".getBytes();
    }
    
    HereLocationFixManager(com.squareup.otto.Bus a) {
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.firstLocationFixCheck = true;
        this.androidGpsLocationListener = null;
        this.debugTTSLocationListener = null;
        this.bus = a;
        this.speedManager = SpeedManager.getInstance();
        android.os.Looper a1 = HereMapsManager.getInstance().getBkLocationReceiverLooper();
        locationManager = (LocationManager) getAppContext().getSystemService(Context.LOCATION_SERVICE);
        if (locationManager == null) {
            throw new NullPointerException();
        }

        this.checkLocationFix = new Runnable() {
            public void run() {
                label3: {
                    Throwable a;
                    label0: {
                        label2: try {
                            long j = SystemClock.elapsedRealtime() - lastAndroidLocationTime;
                            if (locationFix) {
                                if (j < 3000L) {
                                    if (sLogger.isLoggable(2)) {
                                        sLogger.v("still has location fix [" + j + "]");
                                    }
                                } else {
                                    sLogger.i("lost location fix[" + j + "]");
                                    locationFix = false;
                                    bus.post(new MapEvents$LocationFix(false, false, false));
                                }
                            } else {
                                label1: {
                                    if (lastAndroidLocation == null) {
                                        break label1;
                                    }
                                    if (j > 3000L) {
                                        break label1;
                                    }
                                    sLogger.v("got location fix [" + j + "] " + lastAndroidLocation);
                                    locationFix = true;
                                    bus.post(HereLocationFixManager.this.getLocationFixEvent());
                                    break label2;
                                }
                                if (sLogger.isLoggable(2)) {
                                    sLogger.v("still don't have location fix [" + j + "]");
                                }
                                if (firstLocationFixCheck) {
                                    bus.post(new MapEvents$LocationFix(false, false, false));
                                }
                            }
                        } catch(Throwable a2) {
                            a = a2;
                            break label0;
                        }
                        handler.postDelayed(checkLocationFix, 1000L);
                        firstLocationFixCheck = false;
                        break label3;
                    }
                    try {
                        sLogger.e(a);
                    } catch(Throwable a3) {
                        handler.postDelayed(checkLocationFix, 1000L);
                        firstLocationFixCheck = false;
                        throw a3;
                    }
                    handler.postDelayed(checkLocationFix, 1000L);
                    firstLocationFixCheck = false;
                }
            }
        };
        this.locationListener = new LocationListener() {
            // NAVDY_GPS_PROVIDER and MOCK_PROVIDER
            public void onLocationChanged(Location location) {
                if (!locationFix) {
                    HereLocationFixManager.this.setMaptoLocation(location);
                }
                label0: {
                    if (!GpsUtils.isDebugRawGpsPosEnabled()) {
                        break label0;
                    }
                    try {
                        if (rawLocationMarker == null) {
                            break label0;
                        }
                        if (rawLocationMarkerAdded) {
                            rawLocationMarker.setCoordinate(new GeoCoordinate(location.getLatitude(), location.getLongitude()));
                        }

                    } catch(Throwable a1) {
                        sLogger.e(a1);
                    }
                }
                if (isRecording) {
                    recordRawLocation(location);
                }
                lastAndroidLocationTime = SystemClock.elapsedRealtime();
                lastAndroidLocation = location;
                if (lastAndroidLocationTime - lastAndroidLocationPostTime >= 1000L) {
                    if (!TextUtils.equals(location.getProvider(), GpsConstants.NAVDY_GPS_PROVIDER)) {
                        sendLocation(location);
                    }
                    lastAndroidLocationPostTime = lastAndroidLocationTime;
                }
            }

            public void onProviderDisabled(String s) {
            }

            public void onProviderEnabled(String s) {
            }

            public void onStatusChanged(String s, int i, Bundle a) {
            }
        };

        if (TTSUtils.isDebugTTSEnabled() && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice() && locationManager.getProvider(LocationManager.GPS_PROVIDER) != null) {
            final long startTime = SystemClock.elapsedRealtime();
            this.debugTTSLocationListener = new LocationListener() {
                boolean toastSent = false;
                // GPS_PROVIDER
                public void onLocationChanged(Location a) {
                    if (this.toastSent) {
                        sLogger.v("ublox first fix toast already posted");
                    } else {
                        locationManager.removeUpdates(this);
                        this.toastSent = true;
                        long runningTime = SystemClock.elapsedRealtime() - startTime;
                        sLogger.v("got first u-blox fix, unregister (" + runningTime + ")");
                        if (runningTime >= 10000L) {
                            TTSUtils.debugShowGotUbloxFix();
                        } else {
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    TTSUtils.debugShowGotUbloxFix();
                                }
                            }, 10000L - runningTime);
                        }
                    }
                }

                public void onProviderDisabled(String s) {
                }

                public void onProviderEnabled(String s) {
                }

                public void onStatusChanged(String s, int i, Bundle a) {
                }
            };
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0L, 0.0f, this.debugTTSLocationListener, a1);
        }
        android.location.LocationProvider navdy_gps_provider = locationManager.getProvider(GpsConstants.NAVDY_GPS_PROVIDER);
        android.location.Location location = null;
        if (navdy_gps_provider != null) {
            location = locationManager.getLastKnownLocation(GpsConstants.NAVDY_GPS_PROVIDER);
            locationManager.requestLocationUpdates(GpsConstants.NAVDY_GPS_PROVIDER, 0L, 0.0f, this.locationListener, a1);
            sLogger.v("installed gps listener");
        }
        if (locationManager.getProvider(LocationManager.GPS_PROVIDER) != null) {
            this.androidGpsLocationListener = new LocationListener() {
                // GPS_PROVIDER
                public void onLocationChanged(Location a) {
                    if (HereLocationFixManager.this.isRecording && !DeviceUtil.isNavdyDevice()) {
                        HereLocationFixManager.this.recordRawLocation(a);
                    }
                    HereLocationFixManager.this.sendLocation(a);
                }

                public void onProviderDisabled(String s) {
                }

                public void onProviderEnabled(String s) {
                }

                public void onStatusChanged(String s, int i, Bundle a) {
                }
            };
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0L, 0.0f, this.androidGpsLocationListener, a1);
        }
        sLogger.v("installed gps listener");
        if (locationManager.getProvider(MOCK_PROVIDER) != null) {
            if (location == null) {
                location = locationManager.getLastKnownLocation(MOCK_PROVIDER);
            }
            locationManager.requestLocationUpdates(MOCK_PROVIDER, 0L, 0.0f, this.locationListener, a1);
            sLogger.v("installed n/w listener");
        }
        sLogger.v("got last location from location manager:" + location);
        if (!this.locationFix && location != null) {
            sLogger.v("sending to map");
            this.setMaptoLocation(location);
        }
        this.bus.register(this);
        this.handler.postDelayed(this.checkLocationFix, 1000L);
        com.navdy.service.library.events.debug.StartDriveRecordingEvent a4 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getConnectionHandler().getDriverRecordingEvent();
        if (a4 != null) {
            this.onStartDriveRecording(a4);
        }
        sLogger.v("initialized");
    }

    private void recordHereLocation(final com.here.android.mpa.common.GeoPosition position, com.here.android.mpa.common.PositioningManager.LocationMethod locationMethod, final boolean isMapMatched) {
        final com.here.android.mpa.common.PositioningManager.LocationMethod a11 = locationMethod;
        TaskManager.getInstance().execute(new Runnable() {

            public void run() {
                try {
                    String s;
                    GeoCoordinate a = position.getCoordinate();
                    if (position instanceof MatchedGeoPosition) {
                        MatchedGeoPosition a0 = (MatchedGeoPosition) position;
                        s = "," + a0.getMatchQuality() + "," + a0.isExtrapolated() + "," + a0.isOnStreet();
                    } else {
                        s = "";
                    }
                    String s0 = String.valueOf(a.getLatitude()) + "," + a.getLongitude() + "," + position.getHeading() + "," + position.getSpeed() + "," + position.getLatitudeAccuracy() + "," + a.getAltitude() + "," + position.getTimestamp().getTime() + "," + "here" + "," + a11.name() + "," + isMapMatched + s + "\n";
                    HereLocationFixManager.this.recordingFile.write(s0.getBytes());
                } catch (Throwable a1) {
                    sLogger.e(a1);
                }
            }
        }, 9);
    }
    
    protected void recordRawLocation(final android.location.Location location) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new Runnable() {

            public void run() {
                try {
                    String s = location.getProvider();
                    if ("NAVDY_GPS_PROVIDER".equals(s)) {
                        s = LocationManager.GPS_PROVIDER;
                    }
                    HereLocationFixManager.this.recordingFile.write((String.valueOf(location.getLatitude()) + "," + location.getLongitude() + "," + location.getBearing() + "," + location.getSpeed() + "," + location.getAccuracy() + "," + location.getAltitude() + "," + location.getTime() + "," + "raw" + "," + s + "\n").getBytes());
                } catch(Throwable a) {
                    sLogger.e(a);
                }
            }
        }, 9);
    }
    
    protected void sendLocation(android.location.Location location) {
        if (this.speedManager.setGpsSpeed(location.getSpeed(), location.getElapsedRealtimeNanos() / 1000000L)) {
            this.bus.post(new com.navdy.hud.app.maps.MapEvents$GPSSpeedEvent());
        }
        this.lastLocationTime = android.os.SystemClock.elapsedRealtime();
        this.bus.post(location);
    }
    
    public void addMarkers(com.navdy.hud.app.maps.here.HereMapController a) {
        label0: {
            Throwable a0;
            if (!com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled()) {
                break label0;
            }
            try {
                if (this.rawLocationMarker == null) {
                    this.rawLocationMarker = new com.here.android.mpa.mapping.MapMarker();
                    com.here.android.mpa.common.Image a1 = new com.here.android.mpa.common.Image();
                    a1.setImageResource(R.drawable.icon_position_raw_gps);
                    this.rawLocationMarker.setIcon(a1);

                    this.navdyLocationMarker = new com.here.android.mpa.mapping.MapMarker();
                    com.here.android.mpa.common.Image a2 = new com.here.android.mpa.common.Image();
                    a2.setImageResource(R.drawable.icon_position_blue);
                    this.navdyLocationMarker.setIcon(a2);
                    sLogger.v("marker created");
                }
                if (this.rawLocationMarkerAdded) {
                    break label0;
                }
                a.addMapObject(this.rawLocationMarker);
                a.addMapObject(this.navdyLocationMarker);
                this.rawLocationMarkerAdded = true;
                sLogger.v("marker added");
                break label0;
            } catch(Throwable a2) {
                a0 = a2;
            }
            sLogger.e(a0);
        }
    }
    
    public com.here.android.mpa.common.GeoCoordinate getLastGeoCoordinate() {
        return (this.geoPosition != null) ? this.geoPosition.getCoordinate() : null;
    }
    
    public long getLastLocationTime() {
        return this.lastLocationTime;
    }
    
    public com.navdy.hud.app.maps.MapEvents$LocationFix getLocationFixEvent() {
        com.navdy.hud.app.maps.MapEvents$LocationFix a = null;
        if (this.locationFix) {
            boolean usingLocalGpsLocation = false;
            boolean usingPhoneLocation = false;
            if (this.lastAndroidLocation == null) {
                usingLocalGpsLocation = false;
                usingPhoneLocation = false;
            } else if (android.text.TextUtils.equals(this.lastAndroidLocation.getProvider(), GpsConstants.NAVDY_GPS_PROVIDER)) {
                usingLocalGpsLocation = true;
                usingPhoneLocation = false;
            } else {
                usingLocalGpsLocation = false;
                usingPhoneLocation = true;
            }
            a = new com.navdy.hud.app.maps.MapEvents$LocationFix(true, usingPhoneLocation, usingLocalGpsLocation);
        } else {
            a = new com.navdy.hud.app.maps.MapEvents$LocationFix(false, false, false);
        }
        return a;
    }
    
    public boolean hasLocationFix() {
        return this.locationFix;
    }
    
    public boolean isRecording() {
        return this.isRecording;
    }
    
    @Subscribe
    public void onGpsEventSwitch(com.navdy.hud.app.device.gps.GpsUtils.GpsSwitch a) {
        sLogger.v("Gps-Switch phone=" + a.usingPhone + " ublox=" + a.usingUblox);
        this.lastGpsSwitchEvent = a;
    }
    
    @Subscribe
    public void onHereGpsEvent(com.navdy.hud.app.maps.MapEvents$GpsStatusChange a) {
        this.hereGpsSignal = a.connected;
        sLogger.v("here gps event connected [" + this.hereGpsSignal + "]");
    }

    public void onHerePositionUpdated(com.here.android.mpa.common.PositioningManager.LocationMethod a, com.here.android.mpa.common.GeoPosition a0, boolean b) {
        if (sLogger.isLoggable(2)) {
            StringBuilder a2 = new StringBuilder().append("onHerePositionUpdated [");
            long j = this.counter + 1L;
            this.counter = j;
            sLogger.v(a2.append(j).append("] geoPosition[").append(a0.getCoordinate()).append("] method [").append(a.name()).append("] valid[").append(a0.isValid()).append("]").toString());
        }
        if (a != null && a0 != null) {
            this.locationMethod = a;
            this.geoPosition = a0;
            this.lastHerelocationUpdateTime = android.os.SystemClock.elapsedRealtime();
            if (this.isRecording) {
                this.recordHereLocation(a0, a, b);
            }
        }
    }
    
    @Subscribe
    public void onStartDriveRecording(final com.navdy.service.library.events.debug.StartDriveRecordingEvent recordingEvent) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable) new Runnable() {

            public void run() {
                try {
                    if (HereLocationFixManager.this.isRecording) {
                        sLogger.v("already recording");
                    } else {
                        HereLocationFixManager.this.recordingLabel = recordingEvent.label;
                        HereLocationFixManager.this.isRecording = true;
                        File a = DriveRecorder.getDriveLogsDir(HereLocationFixManager.this.recordingLabel);
                        if (!a.exists()) {
                            a.mkdirs();
                        }
                        File a0 = new File(a, HereLocationFixManager.this.recordingLabel + ".here");
                        HereLocationFixManager.this.recordingFile = new FileOutputStream(a0);
                        sLogger.v("created recording file [" + HereLocationFixManager.this.recordingLabel + "] path:" + a0.getAbsolutePath());
                    }
                } catch(Throwable a1) {
                    sLogger.e(a1);
                    HereLocationFixManager.this.isRecording = false;
                    HereLocationFixManager.this.recordingLabel = null;
                    IOUtils.closeStream(HereLocationFixManager.this.recordingFile);
                    HereLocationFixManager.this.recordingFile = null;
                }
            }
        }, 9);
    }
    
    @Subscribe
    public void onStopDriveRecording(com.navdy.service.library.events.debug.StopDriveRecordingEvent recordingEvent) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new Runnable() {

            public void run() {
                try {
                    if (HereLocationFixManager.this.isRecording) {
                        HereLocationFixManager.this.isRecording = false;
                        IOUtils.fileSync(HereLocationFixManager.this.recordingFile);
                        IOUtils.closeStream(HereLocationFixManager.this.recordingFile);
                        HereLocationFixManager.this.recordingFile = null;
                        sLogger.v("stopped recording file [" + HereLocationFixManager.this.recordingLabel + "]");
                        HereLocationFixManager.this.recordingLabel = null;
                    } else {
                        sLogger.v("not recording");
                    }
                } catch(Throwable a) {
                    sLogger.e(a);
                }
            }
        }, 9);
    }
    
    public void recordMarker() {
        if (this.isRecording) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new Runnable() {

                public void run() {
                    try {
                        recordingFile.write(RECORD_MARKER);
                    } catch(Throwable a) {
                        sLogger.e(a);
                    }
                }
            }, 9);
        }
    }
    
    public void removeMarkers(com.navdy.hud.app.maps.here.HereMapController a) {
        if (com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled() && this.rawLocationMarker != null && this.rawLocationMarkerAdded) {
            a.removeMapObject(this.rawLocationMarker);
            a.removeMapObject(this.navdyLocationMarker);
            this.rawLocationMarkerAdded = false;
            sLogger.v("marker removed");
        }
    }
    
    public void setMaptoLocation(android.location.Location location) {
        try {
            if (!this.locationFix && this.geoPosition == null) {
                sLogger.i("marking location fix:" + location);
                this.geoPosition = new com.here.android.mpa.common.GeoPosition(new com.here.android.mpa.common.GeoCoordinate(location.getLatitude(), location.getLongitude(), location.getAltitude()));
                this.locationMethod = com.here.android.mpa.common.PositioningManager.LocationMethod.NETWORK;
                this.lastHerelocationUpdateTime = android.os.SystemClock.elapsedRealtime();
                this.locationFix = true;
                this.bus.post(new com.navdy.hud.app.maps.MapEvents$LocationFix(true, true, false));
                if (rawLocationMarkerAdded) {
                    navdyLocationMarker.setCoordinate(new GeoCoordinate(location.getLatitude(), location.getLongitude()));
                }

            }
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
    }
    
    public void shutdown() {
        locationManager.removeUpdates(this.locationListener);
        if (this.androidGpsLocationListener != null) {
            locationManager.removeUpdates(this.androidGpsLocationListener);
        }
        if (this.debugTTSLocationListener != null) {
            locationManager.removeUpdates(this.debugTTSLocationListener);
        }
    }
}
