package com.navdy.hud.app.maps;

public enum MapEvents$LaneData$Position {
    ON_ROUTE(0),
    OFF_ROUTE(1);

    private int value;
    MapEvents$LaneData$Position(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class MapEvents$LaneData$Position extends Enum {
//    final private static com.navdy.hud.app.maps.MapEvents$LaneData$Position[] $VALUES;
//    final public static com.navdy.hud.app.maps.MapEvents$LaneData$Position OFF_ROUTE;
//    final public static com.navdy.hud.app.maps.MapEvents$LaneData$Position ON_ROUTE;
//
//    static {
//        ON_ROUTE = new com.navdy.hud.app.maps.MapEvents$LaneData$Position("ON_ROUTE", 0);
//        OFF_ROUTE = new com.navdy.hud.app.maps.MapEvents$LaneData$Position("OFF_ROUTE", 1);
//        com.navdy.hud.app.maps.MapEvents$LaneData$Position[] a = new com.navdy.hud.app.maps.MapEvents$LaneData$Position[2];
//        a[0] = ON_ROUTE;
//        a[1] = OFF_ROUTE;
//        $VALUES = a;
//    }
//
//    private MapEvents$LaneData$Position(String s, int i) {
//        super(s, i);
//    }
//
//    public static com.navdy.hud.app.maps.MapEvents$LaneData$Position valueOf(String s) {
//        return (com.navdy.hud.app.maps.MapEvents$LaneData$Position)Enum.valueOf(com.navdy.hud.app.maps.MapEvents$LaneData$Position.class, s);
//    }
//
//    public static com.navdy.hud.app.maps.MapEvents$LaneData$Position[] values() {
//        return $VALUES.clone();
//    }
//}
