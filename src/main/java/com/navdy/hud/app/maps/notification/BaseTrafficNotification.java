package com.navdy.hud.app.maps.notification;

abstract public class BaseTrafficNotification implements com.navdy.hud.app.framework.notifications.INotification {
    final protected static String EMPTY = "";
    final public static float IMAGE_SCALE = 0.5f;
    protected com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayout;
    protected com.navdy.hud.app.framework.notifications.INotificationController controller;
    protected com.navdy.service.library.log.Logger logger;
    
    public BaseTrafficNotification() {
        this.logger = new com.navdy.service.library.log.Logger((this).getClass());
    }
    
    protected void dismissNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.getId());
    }
    
    public int getTimeout() {
        return 0;
    }
    
    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }
    
    public void onClick() {
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent a) {
        boolean b = false;
        if (this.choiceLayout != null) {
            switch(com.navdy.hud.app.maps.notification.BaseTrafficNotification$1.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                case 3: {
                    this.choiceLayout.executeSelectedItem();
                    b = true;
                    break;
                }
                case 2: {
                    this.choiceLayout.moveSelectionRight();
                    b = true;
                    break;
                }
                case 1: {
                    this.choiceLayout.moveSelectionLeft();
                    b = true;
                    break;
                }
                default: {
                    b = false;
                }
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController a) {
        this.controller = a;
    }
    
    public void onStop() {
        this.controller = null;
        if (this.choiceLayout != null) {
            this.choiceLayout.clear();
        }
    }
    
    public void onTrackHand(float f) {
    }
    
    public boolean supportScroll() {
        return false;
    }
}
