package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;

class HereNavigationManager$2 implements com.here.android.mpa.guidance.AudioPlayerDelegate {
    final com.navdy.hud.app.maps.here.HereNavigationManager this$0;
    
    HereNavigationManager$2(com.navdy.hud.app.maps.here.HereNavigationManager a) {

        super();
        this.this$0 = a;
    }
    
    public boolean playFiles(String[] a) {
        return true;
    }
    
    public boolean playText(String s) {
        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v(new StringBuilder().append("[CB-TTS] [").append(s).append("]").toString());
        if (com.navdy.hud.app.maps.here.HereNavigationManager.access$000(this.this$0) != com.navdy.hud.app.maps.NavigationMode.MAP) {
            boolean b = s != null && s.toLowerCase().contains((CharSequence)com.navdy.hud.app.maps.here.HereNavigationManager.access$300(this.this$0));
            if (b && !com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).hasArrived) {
                com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v(new StringBuilder().append("last maneuver marked arrived tts:").append(com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).lastManeuver).toString());
                if (!com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).lastManeuver) {
                    this.this$0.setLastManeuver();
                }
            }
            if (this.this$0.getNavigationSessionPreference().spokenTurnByTurn && !com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).hasArrived) {
                if (com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).lastManeuver && com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).destinationDirection != null && com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).destinationDirection != com.navdy.hud.app.maps.MapEvents$DestinationDirection.UNKNOWN && b) {
                    com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v(new StringBuilder().append("Found destination reached tts[").append(s).append("]").toString());
                    android.content.Context a = com.navdy.hud.app.maps.here.HereNavigationManager.access$400();
                    String s0 = (com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).destinationDirection != com.navdy.hud.app.maps.MapEvents$DestinationDirection.LEFT) ? com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.DESTINATION_RIGHT : com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.DESTINATION_LEFT;
                    Object[] a0 = new Object[1];
                    a0[0] = s0;
                    String s1 = a.getString(R.string.tbt_audio_destination_reached, a0);
                    com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v(new StringBuilder().append("Converted destination reached tts[").append(s1).append("]").toString());
                    s = s1;
                }
                com.navdy.hud.app.maps.here.HereNavigationManager.access$500(this.this$0).post(com.navdy.hud.app.maps.notification.RouteCalculationNotification.CANCEL_TBT_TTS);
                com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(s, com.navdy.service.library.events.audio.SpeechRequest.Category.SPEECH_TURN_BY_TURN, com.navdy.hud.app.maps.notification.RouteCalculationNotification.ROUTE_TBT_TTS_ID);
            }
        } else {
            com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.i("[CB-TTS] not navigating");
        }
        return true;
    }
}
