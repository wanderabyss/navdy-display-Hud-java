package com.navdy.hud.app.maps.here;

class HereMapController$14 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapController this$0;
    private final com.navdy.hud.app.maps.here.HereMapController$Callback val$cb;
    private final Runnable val$runnable;
    
    HereMapController$14(com.navdy.hud.app.maps.here.HereMapController a, Runnable a0, com.navdy.hud.app.maps.here.HereMapController$Callback a1) {
        super();
        this.this$0 = a;
        this.val$runnable = a0;
        this.val$cb = a1;
    }
    
    public void run() {
        this.val$runnable.run();
        if (this.val$cb != null) {
            this.val$cb.finish();
        }
    }
}
