package com.navdy.hud.app.maps.here;

public enum HereNavController$State {
    NAVIGATING(0),
    TRACKING(1);

    private int value;
    HereNavController$State(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class HereNavController$State extends Enum {
//    final private static com.navdy.hud.app.maps.here.HereNavController$State[] $VALUES;
//    final public static com.navdy.hud.app.maps.here.HereNavController$State NAVIGATING;
//    final public static com.navdy.hud.app.maps.here.HereNavController$State TRACKING;
//
//    static {
//        NAVIGATING = new com.navdy.hud.app.maps.here.HereNavController$State("NAVIGATING", 0);
//        TRACKING = new com.navdy.hud.app.maps.here.HereNavController$State("TRACKING", 1);
//        com.navdy.hud.app.maps.here.HereNavController$State[] a = new com.navdy.hud.app.maps.here.HereNavController$State[2];
//        a[0] = NAVIGATING;
//        a[1] = TRACKING;
//        $VALUES = a;
//    }
//
//    private HereNavController$State(String s, int i) {
//        super(s, i);
//    }
//
//    public static com.navdy.hud.app.maps.here.HereNavController$State valueOf(String s) {
//        return (com.navdy.hud.app.maps.here.HereNavController$State)Enum.valueOf(com.navdy.hud.app.maps.here.HereNavController$State.class, s);
//    }
//
//    public static com.navdy.hud.app.maps.here.HereNavController$State[] values() {
//        return $VALUES.clone();
//    }
//}
