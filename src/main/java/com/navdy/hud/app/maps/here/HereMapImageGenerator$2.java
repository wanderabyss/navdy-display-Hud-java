package com.navdy.hud.app.maps.here;

class HereMapImageGenerator$2 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapImageGenerator this$0;
    final android.graphics.Bitmap val$bitmap;
    final com.navdy.hud.app.maps.here.HereMapImageGenerator$MapGeneratorParams val$mapGeneratorParams;
    
    HereMapImageGenerator$2(com.navdy.hud.app.maps.here.HereMapImageGenerator a, android.graphics.Bitmap a0, com.navdy.hud.app.maps.here.HereMapImageGenerator$MapGeneratorParams a1) {
        super();
        this.this$0 = a;
        this.val$bitmap = a0;
        this.val$mapGeneratorParams = a1;
    }
    
    public void run() {
        label2: {
            java.io.FileOutputStream a;
            Throwable a0;
            label0: {
                try {
                    {
                        if (this.val$bitmap == null) {
                            HereMapImageGenerator.access$100().e("bitmap not saved:");
                        } else {
                            String s = this.this$0.getMapImageFile(this.val$mapGeneratorParams.id);
                            a = new java.io.FileOutputStream(s);
                            try {
                                this.val$bitmap.compress(android.graphics.Bitmap.CompressFormat.JPEG, 100, a);
                                com.navdy.service.library.util.IOUtils.fileSync(a);
                                com.navdy.service.library.util.IOUtils.closeStream(a);
                            } catch (Throwable a2) {
                                a0 = a2;
                                break label0;
                            }
                            HereMapImageGenerator.access$100().v("bitmap saved:" + s);
                        }
                    }
                    if (this.val$mapGeneratorParams.callback != null) {
                        com.navdy.hud.app.maps.here.HereMapImageGenerator.access$300(this.this$0).post(new HereMapImageGenerator$2$1(this));
                    }
                } catch(Throwable a3) {
                    a0 = a3;
                    a = null;
                    break label0;
                }
                com.navdy.service.library.util.IOUtils.fileSync(null);
                com.navdy.service.library.util.IOUtils.closeStream(null);
                break label2;
            }
            try {
                com.navdy.hud.app.maps.here.HereMapImageGenerator.access$100().e(a0);
            } catch(Throwable a4) {
                com.navdy.service.library.util.IOUtils.fileSync(a);
                com.navdy.service.library.util.IOUtils.closeStream(a);
                throw a4;
            }
            com.navdy.service.library.util.IOUtils.fileSync(a);
            com.navdy.service.library.util.IOUtils.closeStream(a);
        }
    }
}
