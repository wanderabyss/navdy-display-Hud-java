package com.navdy.hud.app.maps.here;

public enum HerePlacesManager$Error {
    BAD_REQUEST(0),
    RESPONSE_ERROR(1),
    NO_USER_LOCATION(2),
    NO_MAP_ENGINE(3),
    NO_ROUTES(4),
    UNKNOWN_ERROR(5);

    private int value;
    HerePlacesManager$Error(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class HerePlacesManager$Error extends Enum {
//    final private static com.navdy.hud.app.maps.here.HerePlacesManager$Error[] $VALUES;
//    final public static com.navdy.hud.app.maps.here.HerePlacesManager$Error BAD_REQUEST;
//    final public static com.navdy.hud.app.maps.here.HerePlacesManager$Error NO_MAP_ENGINE;
//    final public static com.navdy.hud.app.maps.here.HerePlacesManager$Error NO_ROUTES;
//    final public static com.navdy.hud.app.maps.here.HerePlacesManager$Error NO_USER_LOCATION;
//    final public static com.navdy.hud.app.maps.here.HerePlacesManager$Error RESPONSE_ERROR;
//    final public static com.navdy.hud.app.maps.here.HerePlacesManager$Error UNKNOWN_ERROR;
//
//    static {
//        BAD_REQUEST = new com.navdy.hud.app.maps.here.HerePlacesManager$Error("BAD_REQUEST", 0);
//        RESPONSE_ERROR = new com.navdy.hud.app.maps.here.HerePlacesManager$Error("RESPONSE_ERROR", 1);
//        NO_USER_LOCATION = new com.navdy.hud.app.maps.here.HerePlacesManager$Error("NO_USER_LOCATION", 2);
//        NO_MAP_ENGINE = new com.navdy.hud.app.maps.here.HerePlacesManager$Error("NO_MAP_ENGINE", 3);
//        NO_ROUTES = new com.navdy.hud.app.maps.here.HerePlacesManager$Error("NO_ROUTES", 4);
//        UNKNOWN_ERROR = new com.navdy.hud.app.maps.here.HerePlacesManager$Error("UNKNOWN_ERROR", 5);
//        com.navdy.hud.app.maps.here.HerePlacesManager$Error[] a = new com.navdy.hud.app.maps.here.HerePlacesManager$Error[6];
//        a[0] = BAD_REQUEST;
//        a[1] = RESPONSE_ERROR;
//        a[2] = NO_USER_LOCATION;
//        a[3] = NO_MAP_ENGINE;
//        a[4] = NO_ROUTES;
//        a[5] = UNKNOWN_ERROR;
//        $VALUES = a;
//    }
//
//    private HerePlacesManager$Error(String s, int i) {
//        super(s, i);
//    }
//
//    public static com.navdy.hud.app.maps.here.HerePlacesManager$Error valueOf(String s) {
//        return (com.navdy.hud.app.maps.here.HerePlacesManager$Error)Enum.valueOf(com.navdy.hud.app.maps.here.HerePlacesManager$Error.class, s);
//    }
//
//    public static com.navdy.hud.app.maps.here.HerePlacesManager$Error[] values() {
//        return $VALUES.clone();
//    }
//}
