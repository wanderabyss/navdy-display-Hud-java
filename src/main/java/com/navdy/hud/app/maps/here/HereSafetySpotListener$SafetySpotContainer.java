package com.navdy.hud.app.maps.here;

class HereSafetySpotListener$SafetySpotContainer {
    private com.here.android.mpa.mapping.MapMarker mapMarker;
    private com.here.android.mpa.guidance.SafetySpotNotificationInfo safetySpotNotificationInfo;
    
    private HereSafetySpotListener$SafetySpotContainer() {
    }
    
    HereSafetySpotListener$SafetySpotContainer(com.navdy.hud.app.maps.here.HereSafetySpotListener$1 a) {
        this();
    }
    
    static com.here.android.mpa.mapping.MapMarker access$1000(com.navdy.hud.app.maps.here.HereSafetySpotListener$SafetySpotContainer a) {
        return a.mapMarker;
    }
    
    static com.here.android.mpa.mapping.MapMarker access$1002(com.navdy.hud.app.maps.here.HereSafetySpotListener$SafetySpotContainer a, com.here.android.mpa.mapping.MapMarker a0) {
        a.mapMarker = a0;
        return a0;
    }
    
    static com.here.android.mpa.guidance.SafetySpotNotificationInfo access$600(com.navdy.hud.app.maps.here.HereSafetySpotListener$SafetySpotContainer a) {
        return a.safetySpotNotificationInfo;
    }
    
    static com.here.android.mpa.guidance.SafetySpotNotificationInfo access$602(com.navdy.hud.app.maps.here.HereSafetySpotListener$SafetySpotContainer a, com.here.android.mpa.guidance.SafetySpotNotificationInfo a0) {
        a.safetySpotNotificationInfo = a0;
        return a0;
    }
}
