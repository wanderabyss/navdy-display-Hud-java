package com.navdy.hud.app.maps.notification;

class TrafficNotificationManager$1$1 implements com.navdy.hud.app.maps.here.HereMapUtil$IImageLoadCallback {
    final com.navdy.hud.app.maps.notification.TrafficNotificationManager$1 this$1;
    
    TrafficNotificationManager$1$1(com.navdy.hud.app.maps.notification.TrafficNotificationManager$1 a) {

        super();
        this.this$1 = a;
    }
    
    public void result(com.here.android.mpa.common.Image a, android.graphics.Bitmap a0) {
        try {
            if (a0 != null) {
                com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.w(new StringBuilder().append("junction view bitmap loaded w=").append(a0.getWidth()).append(" h=").append(a0.getHeight()).append(" orig w=").append(this.this$1.val$event.junction.getWidth()).append(" h=").append(this.this$1.val$event.junction.getHeight()).toString());
                if (com.navdy.hud.app.maps.notification.TrafficNotificationManager.access$200(this.this$1.this$0) != null) {
                    long j = android.os.SystemClock.elapsedRealtime();
                    android.graphics.Bitmap a1 = com.navdy.hud.app.util.ImageUtil.hueShift(a0, 90f);
                    if (a1 != null) {
                        long j0 = android.os.SystemClock.elapsedRealtime();
                        android.graphics.Bitmap a2 = com.navdy.hud.app.util.ImageUtil.applySaturation(a0, 0.0f);
                        if (a2 != null) {
                            long j1 = android.os.SystemClock.elapsedRealtime();
                            a0.recycle();
                            android.graphics.Bitmap a3 = com.navdy.hud.app.util.ImageUtil.blend(a1, a2);
                            a2.recycle();
                            a1.recycle();
                            if (a3 != null) {
                                long j2 = android.os.SystemClock.elapsedRealtime();
                                com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.i(new StringBuilder().append("junction view bitmap took[").append(j2 - j).append("] hue [").append(j0 - j).append("] sat[").append(j1 - j0).append("] blend[").append(j2 - j1).append("]").toString());
                                com.navdy.hud.app.maps.notification.TrafficNotificationManager.access$500(this.this$1.this$0).post((Runnable)new com.navdy.hud.app.maps.notification.TrafficNotificationManager$1$1$1(this, a3));
                            } else {
                                com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.w("combine not performed");
                            }
                        } else {
                            com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.w("saturation not performed");
                            a0.recycle();
                            a1.recycle();
                        }
                    } else {
                        com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.w("hue shift not performed");
                        a0.recycle();
                    }
                }
            } else {
                com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.w("junction view bitmap not loaded");
            }
        } catch(Throwable a4) {
            com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.e("junction view mode", a4);
        }
    }
}
