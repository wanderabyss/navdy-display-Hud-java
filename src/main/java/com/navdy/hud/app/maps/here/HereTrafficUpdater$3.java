package com.navdy.hud.app.maps.here;

class HereTrafficUpdater$3 implements com.here.android.mpa.guidance.TrafficUpdater.Listener {
    final com.navdy.hud.app.maps.here.HereTrafficUpdater this$0;
    
    HereTrafficUpdater$3(com.navdy.hud.app.maps.here.HereTrafficUpdater a) {

        super();
        this.this$0 = a;
    }
    
    public void onStatusChanged(com.here.android.mpa.guidance.TrafficUpdater.RequestState a) {
        if (com.navdy.hud.app.maps.here.HereTrafficUpdater.access$200(this.this$0) != null) {
            if (a != com.here.android.mpa.guidance.TrafficUpdater.RequestState.DONE) {
                com.navdy.hud.app.maps.here.HereTrafficUpdater.access$400(this.this$0).i(new StringBuilder().append(com.navdy.hud.app.maps.here.HereTrafficUpdater.access$300(this.this$0)).append(" error on completion of TrafficUpdater.request").toString());
                com.navdy.hud.app.maps.here.HereTrafficUpdater.access$700(this.this$0);
            } else {
                com.navdy.hud.app.maps.here.HereTrafficUpdater.access$600(this.this$0).getEvents(com.navdy.hud.app.maps.here.HereTrafficUpdater.access$200(this.this$0), com.navdy.hud.app.maps.here.HereTrafficUpdater.access$500(this.this$0));
            }
        } else {
            com.navdy.hud.app.maps.here.HereTrafficUpdater.access$400(this.this$0).w(new StringBuilder().append(com.navdy.hud.app.maps.here.HereTrafficUpdater.access$300(this.this$0)).append("onRequestListener triggered and currentRoute is null").toString());
        }
    }
}
