package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;

public class HereSafetySpotListener extends com.here.android.mpa.guidance.NavigationManager.SafetySpotListener {
    final private static int CHECK_INTERVAL;
    final private static int SAFETY_SPOT_HIDE_DISTANCE = 1000;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private com.squareup.otto.Bus bus;
    private boolean canDrawOnTheMap;
    final private Runnable checkExpiredSpots;
    final private android.os.Handler handler;
    final private com.navdy.hud.app.maps.here.HereMapController mapController;
    private java.util.List safetySpotNotifications;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger("SafetySpotListener");
        CHECK_INTERVAL = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(30L);
    }
    
    HereSafetySpotListener(com.squareup.otto.Bus a, com.navdy.hud.app.maps.here.HereMapController a0) {
        this.canDrawOnTheMap = true;
        this.safetySpotNotifications = (java.util.List)new java.util.ArrayList();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.checkExpiredSpots = (Runnable)new com.navdy.hud.app.maps.here.HereSafetySpotListener$1(this);
        sLogger.v("ctor");
        this.bus = a;
        this.mapController = a0;
    }
    
    static java.util.List access$000(com.navdy.hud.app.maps.here.HereSafetySpotListener a) {
        return a.removeExpiredSpots();
    }
    
    static boolean access$100(com.navdy.hud.app.maps.here.HereSafetySpotListener a) {
        return a.canDrawOnTheMap;
    }
    
    static boolean access$102(com.navdy.hud.app.maps.here.HereSafetySpotListener a, boolean b) {
        a.canDrawOnTheMap = b;
        return b;
    }
    
    static java.util.List access$1100(com.navdy.hud.app.maps.here.HereSafetySpotListener a) {
        return a.safetySpotNotifications;
    }
    
    static com.navdy.hud.app.maps.here.HereMapController access$1200(com.navdy.hud.app.maps.here.HereSafetySpotListener a) {
        return a.mapController;
    }
    
    static void access$200(com.navdy.hud.app.maps.here.HereSafetySpotListener a, java.util.List a0) {
        a.removeMapMarkers(a0);
    }
    
    static com.navdy.service.library.log.Logger access$300() {
        return sLogger;
    }
    
    static int access$400() {
        return CHECK_INTERVAL;
    }
    
    static android.os.Handler access$500(com.navdy.hud.app.maps.here.HereSafetySpotListener a) {
        return a.handler;
    }
    
    static Runnable access$700(com.navdy.hud.app.maps.here.HereSafetySpotListener a) {
        return a.checkExpiredSpots;
    }
    
    static void access$800(com.navdy.hud.app.maps.here.HereSafetySpotListener a, java.util.List a0, com.here.android.mpa.common.GeoCoordinate a1) {
        a.addSafetySpotMarkers(a0, a1);
    }
    
    private void addSafetySpotMarkers(java.util.List a, com.here.android.mpa.common.GeoCoordinate a0) {
        java.util.ArrayList a1 = new java.util.ArrayList();
        java.util.ArrayList a2 = new java.util.ArrayList();
        java.util.Iterator a3 = a.iterator();
        com.here.android.mpa.mapping.SafetySpotInfo.Type a4 = null;
        int i = 0;
        Object a5 = a3;
        while(((java.util.Iterator)a5).hasNext()) {
            Throwable a6 = null;
            com.here.android.mpa.guidance.SafetySpotNotificationInfo a7 = (com.here.android.mpa.guidance.SafetySpotNotificationInfo)((java.util.Iterator)a5).next();
            try {
                com.navdy.hud.app.maps.here.HereSafetySpotListener$SafetySpotContainer a8 = new com.navdy.hud.app.maps.here.HereSafetySpotListener$SafetySpotContainer((com.navdy.hud.app.maps.here.HereSafetySpotListener$1)null);
                com.navdy.hud.app.maps.here.HereSafetySpotListener$SafetySpotContainer.access$602(a8, a7);
                com.here.android.mpa.mapping.SafetySpotInfo a9 = a7.getSafetySpot();
                com.here.android.mpa.mapping.SafetySpotInfo.Type a10 = a9.getType();
                com.here.android.mpa.common.GeoCoordinate a11 = a9.getCoordinate();
                com.here.android.mpa.common.Image a12 = new com.here.android.mpa.common.Image();
                if (com.navdy.hud.app.maps.here.HereSafetySpotListener$4.$SwitchMap$com$here$android$mpa$mapping$SafetySpotInfo$Type[a10.ordinal()] != 0) {
                    a12.setImageResource(R.drawable.icon_speed_camera);
                } else {
                    a12.setImageResource(R.drawable.icon_red_light_camera);
                }
                com.navdy.hud.app.maps.here.HereSafetySpotListener$SafetySpotContainer.access$1002(a8, new com.here.android.mpa.mapping.MapMarker(a11, a12));
                if (this.canDrawOnTheMap) {
                    ((java.util.List)a2).add(com.navdy.hud.app.maps.here.HereSafetySpotListener$SafetySpotContainer.access$1000(a8));
                }
                ((java.util.List)a1).add(a8);
                int i0 = (int)a0.distanceTo(a11);
                int i1 = (int)a7.getDistance();
                sLogger.i(new StringBuilder().append("add safety spot id[").append(System.identityHashCode(a7)).append("] type[").append(a10).append("] distance calc[").append(i0).append("] distance[").append(i1).append("]").toString());
                if (a4 != null && i <= i1) {
                    continue;
                }
                a4 = a10;
                i = i1;
                continue;
            } catch(Throwable a13) {
                a6 = a13;
            }
            sLogger.e("map marker error", a6);
        }
        if (this.canDrawOnTheMap) {
            this.mapController.addMapObjects((java.util.List)a2);
        }
        this.safetySpotNotifications.addAll((java.util.Collection)a1);
        if (a4 != null && i > 0) {
            this.sendTts(a4, i);
        }
    }
    
    private boolean isSafetySpotRelevant(com.here.android.mpa.guidance.SafetySpotNotificationInfo a, com.here.android.mpa.common.GeoCoordinate a0) {
        com.here.android.mpa.mapping.SafetySpotInfo a1 = a.getSafetySpot();
        int i = (int)a0.distanceTo(a.getSafetySpot().getCoordinate());
        int i0 = System.identityHashCode(a);
        sLogger.i(new StringBuilder().append("check safety spot id[").append(i0).append("] type[").append(a1.getType()).append("] distance calc[").append(i).append("] distance[").append(a.getDistance()).append("]").toString());
        return i >= 1000;
    }
    
    private java.util.List removeExpiredSpots() {
        java.util.ArrayList a = null;
        com.here.android.mpa.common.GeoCoordinate a0 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
        if (a0 != null) {
            int i = this.safetySpotNotifications.size();
            a = null;
            if (i != 0) {
                java.util.ArrayList a1 = new java.util.ArrayList();
                a = new java.util.ArrayList();
                Object a2 = this.safetySpotNotifications.iterator();
                while(((java.util.Iterator)a2).hasNext()) {
                    com.navdy.hud.app.maps.here.HereSafetySpotListener$SafetySpotContainer a3 = (com.navdy.hud.app.maps.here.HereSafetySpotListener$SafetySpotContainer)((java.util.Iterator)a2).next();
                    int i0 = System.identityHashCode(com.navdy.hud.app.maps.here.HereSafetySpotListener$SafetySpotContainer.access$600(a3));
                    if (this.isSafetySpotRelevant(com.navdy.hud.app.maps.here.HereSafetySpotListener$SafetySpotContainer.access$600(a3), a0)) {
                        ((java.util.List)a).add(a3);
                        sLogger.i(new StringBuilder().append("check remove:").append(i0).toString());
                    } else {
                        ((java.util.List)a1).add(a3);
                        sLogger.i(new StringBuilder().append("check valid:").append(i0).toString());
                    }
                }
                this.safetySpotNotifications.clear();
                this.safetySpotNotifications.addAll((java.util.Collection)a1);
            }
        } else {
            sLogger.e("check:no current location");
            a = null;
        }
        return (java.util.List)a;
    }
    
    private void removeMapMarkers(java.util.List a) {
        if (a != null) {
            java.util.ArrayList a0 = new java.util.ArrayList();
            Object a1 = a.iterator();
            while(((java.util.Iterator)a1).hasNext()) {
                ((java.util.List)a0).add(com.navdy.hud.app.maps.here.HereSafetySpotListener$SafetySpotContainer.access$1000((com.navdy.hud.app.maps.here.HereSafetySpotListener$SafetySpotContainer)((java.util.Iterator)a1).next()));
            }
            this.mapController.removeMapObjects((java.util.List)a0);
        }
    }
    
    private void sendTts(com.here.android.mpa.mapping.SafetySpotInfo.Type a, int i) {
        com.navdy.service.library.events.preferences.NavigationPreferences a0 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getNavigationPreferences();
        if (Boolean.TRUE.equals(a0.spokenCameraWarnings)) {
            int i0 = 0;
            com.navdy.hud.app.maps.util.DistanceConverter$Distance a1 = new com.navdy.hud.app.maps.util.DistanceConverter$Distance();
            com.navdy.hud.app.maps.util.DistanceConverter.convertToDistance(com.navdy.hud.app.manager.SpeedManager.getInstance().getSpeedUnit(), (float)i, a1);
            String s = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getFormattedDistance(a1.value, a1.unit, true);
            switch(com.navdy.hud.app.maps.here.HereSafetySpotListener$4.$SwitchMap$com$here$android$mpa$mapping$SafetySpotInfo$Type[a.ordinal()]) {
                case 2: {
                    i0 = R.string.camera_warning_both;
                    break;
                }
                case 1: {
                    i0 = R.string.camera_warning_speed;
                    break;
                }
                default: {
                    i0 = R.string.camera_warning_red_light;
                }
            }
            android.content.res.Resources a2 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            Object[] a3 = new Object[1];
            a3[0] = s;
            com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(a2.getString(i0, a3), com.navdy.service.library.events.audio.SpeechRequest.Category.SPEECH_CAMERA_WARNING, (String)null);
        }
    }
    
    public void onSafetySpot(com.here.android.mpa.guidance.SafetySpotNotification a) {
        this.handler.post((Runnable)new com.navdy.hud.app.maps.here.HereSafetySpotListener$2(this, a));
    }
    
    public void setSafetySpotsEnabledOnMap(boolean b) {
        this.handler.post((Runnable)new com.navdy.hud.app.maps.here.HereSafetySpotListener$3(this, b));
    }
}
