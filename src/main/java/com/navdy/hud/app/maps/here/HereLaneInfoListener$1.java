package com.navdy.hud.app.maps.here;

import java.lang.ref.WeakReference;

class HereLaneInfoListener$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereLaneInfoListener this$0;
    
    HereLaneInfoListener$1(com.navdy.hud.app.maps.here.HereLaneInfoListener a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereLaneInfoListener.access$000(this.this$0).addLaneInfoListener(new WeakReference(this.this$0));
        com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().v("added lane info listener");
    }
}
