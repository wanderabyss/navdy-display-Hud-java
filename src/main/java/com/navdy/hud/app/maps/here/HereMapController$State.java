package com.navdy.hud.app.maps.here;

public enum HereMapController$State {
    NONE(0),
    TRANSITION(1),
    ROUTE_PICKER(2),
    OVERVIEW(3),
    AR_MODE(4);

    private int value;
    HereMapController$State(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class HereMapController$State extends Enum {
//    final private static com.navdy.hud.app.maps.here.HereMapController$State[] $VALUES;
//    final public static com.navdy.hud.app.maps.here.HereMapController$State AR_MODE;
//    final public static com.navdy.hud.app.maps.here.HereMapController$State NONE;
//    final public static com.navdy.hud.app.maps.here.HereMapController$State OVERVIEW;
//    final public static com.navdy.hud.app.maps.here.HereMapController$State ROUTE_PICKER;
//    final public static com.navdy.hud.app.maps.here.HereMapController$State TRANSITION;
//
//    static {
//        NONE = new com.navdy.hud.app.maps.here.HereMapController$State("NONE", 0);
//        TRANSITION = new com.navdy.hud.app.maps.here.HereMapController$State("TRANSITION", 1);
//        ROUTE_PICKER = new com.navdy.hud.app.maps.here.HereMapController$State("ROUTE_PICKER", 2);
//        OVERVIEW = new com.navdy.hud.app.maps.here.HereMapController$State("OVERVIEW", 3);
//        AR_MODE = new com.navdy.hud.app.maps.here.HereMapController$State("AR_MODE", 4);
//        com.navdy.hud.app.maps.here.HereMapController$State[] a = new com.navdy.hud.app.maps.here.HereMapController$State[5];
//        a[0] = NONE;
//        a[1] = TRANSITION;
//        a[2] = ROUTE_PICKER;
//        a[3] = OVERVIEW;
//        a[4] = AR_MODE;
//        $VALUES = a;
//    }
//
//    private HereMapController$State(String s, int i) {
//        super(s, i);
//    }
//
//    public static com.navdy.hud.app.maps.here.HereMapController$State valueOf(String s) {
//        return (com.navdy.hud.app.maps.here.HereMapController$State)Enum.valueOf(com.navdy.hud.app.maps.here.HereMapController$State.class, s);
//    }
//
//    public static com.navdy.hud.app.maps.here.HereMapController$State[] values() {
//        return $VALUES.clone();
//    }
//}
