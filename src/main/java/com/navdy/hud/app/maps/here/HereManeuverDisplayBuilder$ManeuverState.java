package com.navdy.hud.app.maps.here;

public enum HereManeuverDisplayBuilder$ManeuverState {
    STAY(0),
    NEXT(1),
    SOON(2),
    NOW(3);

    private int value;
    HereManeuverDisplayBuilder$ManeuverState(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class HereManeuverDisplayBuilder$ManeuverState extends Enum {
//    final private static com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState[] $VALUES;
//    final public static com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState NEXT;
//    final public static com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState NOW;
//    final public static com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState SOON;
//    final public static com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState STAY;
//
//    static {
//        STAY = new com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState("STAY", 0);
//        NEXT = new com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState("NEXT", 1);
//        SOON = new com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState("SOON", 2);
//        NOW = new com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState("NOW", 3);
//        com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState[] a = new com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState[4];
//        a[0] = STAY;
//        a[1] = NEXT;
//        a[2] = SOON;
//        a[3] = NOW;
//        $VALUES = a;
//    }
//
//    private HereManeuverDisplayBuilder$ManeuverState(String s, int i) {
//        super(s, i);
//    }
//
//    public static com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState valueOf(String s) {
//        return (com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState)Enum.valueOf(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.class, s);
//    }
//
//    public static com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState[] values() {
//        return $VALUES.clone();
//    }
//}
