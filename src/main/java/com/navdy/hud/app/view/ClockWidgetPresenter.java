package com.navdy.hud.app.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.common.TimeHelper.UpdateClock;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager;
import com.navdy.hud.app.view.drawable.AnalogClockDrawable;
import com.navdy.hud.app.view.drawable.SmallAnalogClockDrawable;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.Calendar;
import java.util.Date;

public class ClockWidgetPresenter extends DashboardWidgetPresenter {
    private static final int TIME_UPDATE_INTERVAL = 30000;
    private StringBuilder amPmMarker = new StringBuilder();
    private AnalogClockDrawable analogClockDrawable;
    private String analogClockWidgetName;
    private Bus bus;
    private String dateText;
    private int dayOfTheMonth;
    private int dayOfTheWeek;
    private String dayText;
    private String digitalClock2WidgetName;
    private String digitalClockWidgetName;
    private ClockType mClockType;
    private Context mContext;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private SmallAnalogClockDrawable smallAnalogClockDrawable;
    private TimeHelper timeHelper;
    private int timeHour;
    private int timeMinute;
    private int timeSeconds;
    private String timeText;
    private Runnable updateTimeRunnable;

    public enum ClockType {
        ANALOG(0),
        DIGITAL1(1),
        DIGITAL2(2);

        private int value;
        ClockType(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    private int nextUpdateInterval() {
        int intervalSecs = 30;
        int nextSecs = (this.timeSeconds + 30) % 60;
        if (nextSecs >= 45) {
            intervalSecs = 30 + (60 - nextSecs);
        } else if (nextSecs <= 15) {
            intervalSecs = 30 - nextSecs;
        }
        return intervalSecs * 1000;
    }

    public ClockWidgetPresenter(Context context, ClockType clockType) {
        this.mClockType = clockType;
        this.mContext = context;
        this.timeHelper = RemoteDeviceManager.getInstance().getTimeHelper();
        this.bus = RemoteDeviceManager.getInstance().getBus();
        this.analogClockWidgetName = context.getResources().getString(R.string.widget_clock_analog);
        this.digitalClockWidgetName = context.getResources().getString(R.string.widget_clock_digital);
        this.digitalClock2WidgetName = context.getResources().getString(R.string.widget_clock_time_and_date);
        this.updateTimeRunnable = new Runnable() {
            public void run() {
                ClockWidgetPresenter.this.updateTime();
                ClockWidgetPresenter.this.mHandler.postDelayed(ClockWidgetPresenter.this.updateTimeRunnable, (long) ClockWidgetPresenter.this.nextUpdateInterval());
            }
        };
        switch (clockType) {
            case ANALOG:
                this.analogClockDrawable = new AnalogClockDrawable(context);
                return;
            case DIGITAL1:
                this.smallAnalogClockDrawable = new SmallAnalogClockDrawable(context);
                return;
            default:
                return;
        }
    }

    @Subscribe
    public void onUpdateClock(UpdateClock event) {
        updateTime();
    }

    private void updateTime() {
        Calendar cal = Calendar.getInstance();
        Date currentTime = new Date();
        cal.setTime(currentTime);
        this.timeMinute = cal.get(12);
        this.timeHour = cal.get(11);
        this.timeSeconds = cal.get(13);
        this.dayOfTheMonth = cal.get(5);
        this.dayOfTheWeek = cal.get(7);
        switch (this.mClockType) {
            case DIGITAL1:
                break;
            case DIGITAL2:
                this.dateText = this.timeHelper.getDate();
                this.dayText = this.timeHelper.getDay();
                this.dateText = this.dateText.toUpperCase();
                this.dayText = this.dayText.toUpperCase();
                break;
        }
        this.timeText = this.timeHelper.formatTime(currentTime, this.amPmMarker);
        reDraw();
    }

    public void setView(DashboardWidgetView dashboardWidgetView, Bundle arguments) {
        if (dashboardWidgetView != null) {
            switch (this.mClockType) {
                case ANALOG:
                    dashboardWidgetView.setContentView((int) R.layout.smart_dash_widget_circular_content_layout);
                    break;
                case DIGITAL1:
                    dashboardWidgetView.setContentView((int) R.layout.digital_clock1_layout);
                    break;
                case DIGITAL2:
                    dashboardWidgetView.setContentView((int) R.layout.digital_clock2_layout);
                    break;
            }
            super.setView(dashboardWidgetView, arguments);
            this.mHandler.removeCallbacks(this.updateTimeRunnable);
            updateTime();
            this.mHandler.postDelayed(this.updateTimeRunnable, (long) nextUpdateInterval());
            return;
        }
        this.mHandler.removeCallbacks(this.updateTimeRunnable);
        super.setView(dashboardWidgetView, arguments);
    }

    protected boolean isRegisteringToBusRequired() {
        return true;
    }

    public Drawable getDrawable() {
        switch (this.mClockType) {
            case ANALOG:
                return this.analogClockDrawable;
            case DIGITAL1:
                return this.smallAnalogClockDrawable;
            default:
                return null;
        }
    }

    protected void updateGauge() {
        if (this.mWidgetView != null) {
            switch (this.mClockType) {
                case ANALOG:
                    this.analogClockDrawable.setTime(this.dayOfTheMonth, this.timeHour, this.timeMinute, 0);
                    return;
                case DIGITAL1:
                    this.smallAnalogClockDrawable.setTime(this.timeHour, this.timeMinute, this.timeSeconds);
                    TextView timeTextView = (TextView) this.mWidgetView.findViewById(R.id.txt_value);
                    ((TextView) this.mWidgetView.findViewById(R.id.txt_unit)).setText(this.amPmMarker.toString());
                    timeTextView.setText(this.timeText);
                    return;
                case DIGITAL2:
                    TextView pmTextView = (TextView) this.mWidgetView.findViewById(R.id.txt_unit);
                    ((TextView) this.mWidgetView.findViewById(R.id.txt_value)).setText(this.timeText);
                    pmTextView.setText(this.amPmMarker.toString());
                    ((TextView) this.mWidgetView.findViewById(R.id.txt_date)).setText(this.dateText);
                    ((TextView) this.mWidgetView.findViewById(R.id.txt_day)).setText(this.dayText);
                    return;
                default:
                    return;
            }
        }
    }

    public String getWidgetIdentifier() {
        switch (this.mClockType) {
            case ANALOG:
                return SmartDashWidgetManager.ANALOG_CLOCK_WIDGET_ID;
            case DIGITAL1:
                return SmartDashWidgetManager.DIGITAL_CLOCK_WIDGET_ID;
            case DIGITAL2:
                return SmartDashWidgetManager.DIGITAL_CLOCK_2_WIDGET_ID;
            default:
                return null;
        }
    }

    public String getWidgetName() {
        switch (this.mClockType) {
            case ANALOG:
                return this.analogClockWidgetName;
            case DIGITAL1:
                return this.digitalClockWidgetName;
            case DIGITAL2:
                return this.digitalClock2WidgetName;
            default:
                return null;
        }
    }
}
