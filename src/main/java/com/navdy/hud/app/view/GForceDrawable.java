package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import com.navdy.hud.app.R;
import com.navdy.hud.app.view.drawable.CustomDrawable;
import com.navdy.service.library.log.Logger;
import java.util.Locale;

public class GForceDrawable extends CustomDrawable {
    private static final int BACKGROUND = 0;
    private static final int DANGER = 4;
    private static final int EXTREME = 5;
    private static final int HIGHLIGHT = 2;
    public static final float HIGH_G = 1.05f;
    private static final int LOW = 1;
    public static final float LOW_G = 0.45f;
    public static final float MAX_ACCEL = 2.0f;
    public static final float MEDIUM_G = 0.75f;
    public static final float NO_G = 0.15f;
    private static final int WARNING = 3;
    private static Typeface typeface;
    private int[] colors;
    protected Logger logger = new Logger(getClass());
    private Paint mTextPaint;
    private int textColor;
    private float xAccel;
    private float yAccel;
    private float zAccel;

    public GForceDrawable(Context context) {
        this.mPaint.setColor(-1);
        this.mPaint.setStrokeWidth(1.0f);
        if (typeface == null) {
            typeface = Typeface.create("sans-serif-medium", 0);
        }
        this.mTextPaint = new Paint();
        this.mTextPaint.setTextAlign(Align.CENTER);
        Resources resources = context.getResources();
        this.colors = resources.getIntArray(R.array.gforce_colors);
        this.textColor = resources.getColor(R.color.gforce_text);
    }

    public void setAcceleration(float xAccel, float yAccel, float zAccel) {
        this.xAccel = xAccel;
        this.yAccel = yAccel;
        this.zAccel = zAccel;
    }

    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        Rect bounds = getBounds();
        int width = bounds.width() - 10;
        float centerY = (float) ((bounds.height() - 10) / 2);
        float centerX = (float) (width / 2);
        float x = clamp(this.xAccel, -2.0f, 2.0f);
        float y = clamp(this.yAccel, -2.0f, 2.0f);
        float z = clamp(this.zAccel, -2.0f, 2.0f);
        if (false) {
            int subTicks = Math.min(10, Math.max(1, (width / 4) / 4));
            int ticks = 2 * subTicks;
            for (int i = -ticks; i <= ticks; i++) {
                float tickX = ((float) 5) + ((1.0f + (((float) i) / ((float) ticks))) * centerX);
                int tickHeight = i % subTicks == 0 ? 10 : 5;
                canvas.drawLine(tickX, (((float) 5) + centerY) - ((float) (tickHeight / 2)), tickX, (((float) 5) + centerY) + ((float) (tickHeight / 2)), this.mPaint);
                float tickY = ((float) 5) + ((1.0f + (((float) i) / ((float) ticks))) * centerY);
                canvas.drawLine((((float) 5) + centerX) - ((float) (tickHeight / 2)), tickY, (((float) 5) + centerX) + ((float) (tickHeight / 2)), tickY, this.mPaint);
            }
            canvas.drawCircle((((float) 5) + centerX) + ((x * centerX) / 2.0f), (((float) 5) + centerY) - ((y * centerY) / 2.0f), 4.0f + (Math.abs(this.zAccel) * 3.0f), this.mPaint);
            return;
        }
        float g = Math.max(Math.abs(x), Math.abs(y));
        this.mPaint.setColor(this.colors[0]);
        canvas.drawCircle(((float) 5) + centerX, ((float) 5) + centerY, 50.0f, this.mPaint);
        float centerAngle = (float) ((Math.atan((double) ((-y) / x)) * 180.0d) / 3.141592653589793d);
        if (x < 0.0f) {
            centerAngle += 180.0f;
        }
        int color = this.colors[1];
        if (g > 0.15f) {
            float sweepAngle = 20.0f + ((70.0f * (clamp(g, 0.15f, 0.75f) - 0.15f)) / 0.6f);
            float startAngle = centerAngle - (sweepAngle / 2.0f);
            if (g < 0.45f) {
                color = this.colors[2];
            } else if (g < 0.75f) {
                color = this.colors[3];
            } else if (g < 1.05f) {
                color = this.colors[4];
            } else {
                color = this.colors[5];
            }
            this.mPaint.setColor(color);
            canvas.drawArc(new RectF(50.0f - 50.0f, 50.0f - 50.0f, 50.0f + 50.0f, 50.0f + 50.0f), startAngle, sweepAngle, true, this.mPaint);
        }
        this.mPaint.setColor(-16777216);
        canvas.drawCircle(((float) 5) + centerX, ((float) 5) + centerY, 36.0f, this.mPaint);
        this.mTextPaint.setColor(this.textColor);
        this.mTextPaint.setTextSize(16.0f);
        this.mTextPaint.setTypeface(Typeface.SANS_SERIF);
        canvas.drawText("G", ((float) 5) + centerX, (((float) 5) + centerY) - 18.0f, this.mTextPaint);
        canvas.drawText("-", ((float) 5) + centerX, (((float) 5) + centerY) + 28.0f, this.mTextPaint);
        this.mTextPaint.setColor(color);
        this.mTextPaint.setTextSize(28.0f);
        this.mTextPaint.setTypeface(typeface);
        boolean dataInvalid = this.xAccel == 0.0f && this.yAccel == 0.0f && this.zAccel == 0.0f;
        canvas.drawText(dataInvalid ? "-" : String.format(Locale.US, "%01.2f", new Object[]{Float.valueOf(g)}), ((float) 5) + centerX, (((float) 5) + centerY) + 10.0f, this.mTextPaint);
    }

    private float clamp(float val, float min, float max) {
        if (val < min) {
            return min;
        }
        if (val > max) {
            return max;
        }
        return val;
    }
}
