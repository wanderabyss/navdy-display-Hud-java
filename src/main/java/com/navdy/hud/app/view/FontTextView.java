package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;
import com.navdy.hud.app.R;
import java.util.HashMap;

public class FontTextView extends TextView {
    private static HashMap<String, Typeface> mCachedTypeFaces = new HashMap();

    public FontTextView(Context context) {
        this(context, null);
    }

    public FontTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FontTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray customAttributes = context.obtainStyledAttributes(attrs, R.styleable.FontTextView, defStyleAttr, 0);
        if (customAttributes != null) {
            String fontFileName = customAttributes.getString(R.styleable.FontTextView_fontFile);
            if (!TextUtils.isEmpty(fontFileName)) {
                setTypeface(createFromAsset(context.getAssets(), fontFileName));
            }
            customAttributes.recycle();
        }
    }

    public static Typeface createFromAsset(AssetManager assetManager, String path) {
        if (mCachedTypeFaces.containsKey(path)) {
            return (Typeface) mCachedTypeFaces.get(path);
        }
        Typeface typeFace = Typeface.createFromAsset(assetManager, path);
        if (typeFace != null) {
            mCachedTypeFaces.put(path, typeFace);
        }
        return typeFace;
    }
}
