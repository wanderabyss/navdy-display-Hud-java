package com.navdy.hud.app.service;

import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.hud.device.connection.EASessionSocketAdapter;
import com.navdy.hud.device.connection.iAP2Link;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.NavdyEventUtil;
import com.navdy.service.library.events.audio.AudioStatus;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.file.FileTransferData;
import com.navdy.service.library.events.file.FileTransferRequest;
import com.navdy.service.library.events.file.FileTransferStatus;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.network.BTSocketFactory;
import com.navdy.service.library.network.SocketAcceptor;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.network.SocketFactory;

import java.io.IOException;
import java.util.UUID;
import com.navdy.service.library.events.location.Coordinate;
import com.squareup.wire.Wire;

import static com.navdy.hud.app.service.HudConnectionService$7.*;
import static com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_CONNECTED;
import static com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED;
import static com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_LINK_LOST;

public class HudConnectionService extends com.navdy.service.library.device.connection.ConnectionService {
    final public static String ACTION_DEVICE_FORCE_RECONNECT = "com.navdy.hud.app.force_reconnect";
    final private static String APP_TERMINATION_RECONNECT_DELAY_MS = "persist.sys.app_reconnect_ms";
    final private static String AUTO_DRIVERECORD_PROP = "persist.sys.driverecord.auto";
    final public static String EXTRA_HUD_FORCE_RECONNECT_REASON = "force_reconnect_reason";
    final private static int PROXY_ENTRY_LOCAL_PORT = 3000;
    final public static String REASON_CONNECTION_DISCONNECTED = "CONNECTION_DISCONNECTED";
    private android.content.BroadcastReceiver bluetoothReceiver;
    private android.content.BroadcastReceiver debugReceiver;
    private com.navdy.hud.app.service.HudConnectionService$ReconnectRunnable deviceReconnectRunnable;
    private com.navdy.hud.app.service.FileTransferHandler fileTransferHandler;
    final private com.navdy.hud.app.device.gps.GpsManager gpsManager;
    private com.navdy.service.library.device.link.LinkManager$LinkFactory iAPLinkFactory;
    private boolean isSimulatingGpsCoordinates;
    private boolean needAutoSearch;
    final private com.navdy.hud.app.debug.RouteRecorder routeRecorder;
    private com.navdy.hud.app.service.DeviceSearch search;
    
    public HudConnectionService() {
        this.iAPLinkFactory = new HudConnectionService$1(this);
        this.routeRecorder = com.navdy.hud.app.debug.RouteRecorder.getInstance();
        this.gpsManager = com.navdy.hud.app.device.gps.GpsManager.getInstance();
        this.needAutoSearch = true;
        this.bluetoothReceiver = new com.navdy.hud.app.service.HudConnectionService$3(this);
        this.deviceReconnectRunnable = new com.navdy.hud.app.service.HudConnectionService$ReconnectRunnable(this);
    }
    
    static void access$000(com.navdy.hud.app.service.HudConnectionService a, int i) {
        a.setBandwidthLevel(i);
    }
    
    static com.navdy.service.library.log.Logger access$100(com.navdy.hud.app.service.HudConnectionService a) {
        return a.logger;
    }
    
    static void access$1000(com.navdy.hud.app.service.HudConnectionService a, com.squareup.wire.Message a0) {
        a.forwardEventLocally(a0);
    }
    
    static com.navdy.service.library.log.Logger access$1100(com.navdy.hud.app.service.HudConnectionService a) {
        return a.logger;
    }
    
    static com.navdy.service.library.device.RemoteDevice access$1200(com.navdy.hud.app.service.HudConnectionService a) {
        return a.mRemoteDevice;
    }
    
    static com.navdy.service.library.device.RemoteDevice access$1300(com.navdy.hud.app.service.HudConnectionService a) {
        return a.mRemoteDevice;
    }
    
    static com.navdy.hud.app.service.DeviceSearch access$200(com.navdy.hud.app.service.HudConnectionService a) {
        return a.search;
    }
    
    static com.navdy.hud.app.service.DeviceSearch access$202(com.navdy.hud.app.service.HudConnectionService a, com.navdy.hud.app.service.DeviceSearch a0) {
        a.search = a0;
        return a0;
    }
    
    static com.navdy.service.library.device.RemoteDevice access$300(com.navdy.hud.app.service.HudConnectionService a) {
        return a.mRemoteDevice;
    }
    
    static com.navdy.service.library.device.RemoteDevice access$400(com.navdy.hud.app.service.HudConnectionService a) {
        return a.mRemoteDevice;
    }
    
    static com.navdy.service.library.log.Logger access$500(com.navdy.hud.app.service.HudConnectionService a) {
        return a.logger;
    }
    
    static Runnable access$600(com.navdy.hud.app.service.HudConnectionService a) {
        return a.reconnectRunnable;
    }
    
    static com.navdy.service.library.device.connection.ConnectionService$ServiceHandler access$700(com.navdy.hud.app.service.HudConnectionService a) {
        return a.serviceHandler;
    }
    
    static com.navdy.service.library.log.Logger access$800(com.navdy.hud.app.service.HudConnectionService a) {
        return a.logger;
    }
    
    static com.navdy.service.library.device.connection.ConnectionService$State access$900(com.navdy.hud.app.service.HudConnectionService a) {
        return a.state;
    }
    
    private boolean parseStartDrivePlaybackEvent(byte[] a) {
        try {
            com.navdy.service.library.events.debug.StartDrivePlaybackEvent a0 = (com.navdy.service.library.events.debug.StartDrivePlaybackEvent)com.navdy.service.library.events.NavdyEventUtil.messageFromEvent((com.navdy.service.library.events.NavdyEvent)this.mWire.parseFrom(a, com.navdy.service.library.events.NavdyEvent.class));
            this.routeRecorder.startPlayback(a0.label, a0.playSecondaryLocation != null && a0.playSecondaryLocation, false);
            this.logger.v("onStartDrivePlayback, name=" + a0.label);
        } catch(Throwable a1) {
            this.logger.e(a1);
        }
        return true;
    }
    
    private boolean parseStartDriveRecordingEvent(byte[] a) {
        try {
            com.navdy.service.library.events.debug.StartDriveRecordingEvent a0 = (com.navdy.service.library.events.debug.StartDriveRecordingEvent)com.navdy.service.library.events.NavdyEventUtil.messageFromEvent((com.navdy.service.library.events.NavdyEvent)this.mWire.parseFrom(a, com.navdy.service.library.events.NavdyEvent.class));
            if (a0 == null) {
                this.logger.e("startDriveRecording event is null");
            } else {
                String s = this.routeRecorder.startRecording(a0.label, false);
                if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                    this.logger.e("startDriveRecording event fileName is empty");
                } else {
                    this.forwardEventLocally((com.squareup.wire.Message)new com.navdy.service.library.events.debug.StartDriveRecordingEvent(s));
                    this.logger.v("onStartDriveRecording, name=" + a0.label);
                }
            }
        } catch(Throwable a1) {
            this.logger.e(a1);
        }
        return true;
    }
    
    private void selectDevice(com.navdy.service.library.device.NavdyDeviceId a) {
        if (this.search == null) {
            com.navdy.service.library.device.connection.ConnectionInfo a0 = this.deviceRegistry.findDevice(a);
            if (a0 != null) {
                this.logger.i("Found corresponding connection info:" + a0);
                this.connect(a0);
            }
        } else {
            this.logger.i("Connecting to " + a);
            com.navdy.service.library.device.RemoteDevice a1 = this.search.select(a);
            if (a1 != null) {
                this.logger.i("Found corresponding remote device:" + a1);
                this.setActiveDevice(a1);
            }
        }
    }
    
    public void broadcastReconnectingIntent(String s) {
        android.content.Intent a = new android.content.Intent("com.navdy.hud.app.force_reconnect");
        a.putExtra("force_reconnect_reason", s);
        this.sendBroadcast(a);
    }
    
//    protected com.navdy.service.library.device.connection.ProxyService createProxyService() {
//        return this.createProxyService();
//    }
    
    protected com.navdy.service.library.device.connection.tunnel.Tunnel createProxyService() {
        final HudConnectionService a = this;
        final UUID uuid1 = NAVDY_PROXY_TUNNEL_UUID;
        return new com.navdy.service.library.device.connection.tunnel.Tunnel(new com.navdy.service.library.network.TCPSocketAcceptor(3000, true), new SocketFactory() {
            final HudConnectionService this$0 = a;
            private final UUID uuid = uuid1;

            public SocketAdapter build() throws IOException {
                Object a = null;
                if (iAP2Link.proxyEASession == null) {
                    if (access$1200(this.this$0) == null) {
                        throw new IOException("can't create proxy tunnel because HUD is not connected to remote device");
                    }
                    a = new BTSocketFactory(access$1300(this.this$0).getDeviceId().getBluetoothAddress(), uuid).build();
                } else {
                    a = new EASessionSocketAdapter(iAP2Link.proxyEASession);
                }
                return (SocketAdapter)a;
            }
        });
    }

    protected com.navdy.service.library.device.connection.tunnel.Tunnel createObdService() {
        UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");  // common Serial profile uuid
        final HudConnectionService a = this;
        final UUID uuid1 = uuid;
        return new com.navdy.service.library.device.connection.tunnel.Tunnel(
                new com.navdy.service.library.network.BTSocketAcceptor("NavdyObd", uuid, false),
                new com.navdy.service.library.network.TCPSocketFactory("localhost", 6500));
    }
    
    protected void enterState(com.navdy.service.library.device.connection.ConnectionService$State a) {
        switch($SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State[a.ordinal()]) {
            case 5: case 6: {
                this.needAutoSearch = false;
                break;
            }
            case 4: {
                this.needAutoSearch = false;
                if (this.search != null) {
                    this.search.close();
                    this.search = null;
                }
                if (this.mRemoteDevice == null) {
                    break;
                }
                if (this.deviceRegistry.findDevice(this.mRemoteDevice.getDeviceId()) != null) {
                    break;
                }
                com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.recordNewDevice();
                break;
            }
            case 3: {
                if (this.isPromiscuous()) {
                    this.startBroadcasters();
                }
                this.startListeners();
                if (this.search == null) {
                    this.search = new com.navdy.hud.app.service.DeviceSearch((android.content.Context)this, (com.navdy.hud.app.service.DeviceSearch$EventSink)new com.navdy.hud.app.service.HudConnectionService$4(this), this.inProcess);
                    this.search.next();
                }
                if (this.mRemoteDevice == null) {
                    break;
                }
                boolean b = this.mRemoteDevice.isConnected();
                label0: {
                    if (b) {
                        break label0;
                    }
                    if (!this.mRemoteDevice.isConnecting()) {
                        break;
                    }
                }
                this.setState(com.navdy.service.library.device.connection.ConnectionService$State.DISCONNECTING);
                break;
            }
            case 2: {
                if (this.search == null) {
                    break;
                }
                this.setState(com.navdy.service.library.device.connection.ConnectionService$State.SEARCHING);
                break;
            }
        }
        super.enterState(a);
    }
    
    protected void exitState(com.navdy.service.library.device.connection.ConnectionService$State a) {
        super.exitState(a);
        if ($SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State[a.ordinal()] != 0) {
            android.content.IntentFilter a0 = new android.content.IntentFilter("android.bluetooth.device.action.PAIRING_REQUEST");
            a0.setPriority(1);
            a0.addAction("android.bluetooth.device.action.ACL_DISCONNECTED");
            a0.addAction("android.bluetooth.device.action.BOND_STATE_CHANGED");
            this.registerReceiver(this.bluetoothReceiver, a0);
        }
    }
    
    protected void forgetPairedDevice(android.bluetooth.BluetoothDevice a) {
        if (this.search != null) {
            this.search.forgetDevice(a);
        }
        super.forgetPairedDevice(a);
    }
    
    protected com.navdy.service.library.device.connection.ConnectionListener[] getConnectionListeners(android.content.Context a) {
        com.navdy.service.library.device.connection.ConnectionListener[] a0 = null;
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            a0 = new com.navdy.service.library.device.connection.ConnectionListener[2];
            a0[0] = new com.navdy.service.library.device.connection.AcceptorListener(a, new com.navdy.service.library.network.BTSocketAcceptor("Navdy", NAVDY_PROTO_SERVICE_UUID), com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF);
            a0[1] = new com.navdy.service.library.device.connection.AcceptorListener(a, new com.navdy.service.library.network.BTSocketAcceptor("Navdy iAP", ACCESSORY_IAP2), com.navdy.service.library.device.connection.ConnectionType.BT_IAP2_LINK);
        } else {
            a0 = new com.navdy.service.library.device.connection.ConnectionListener[2];
            a0[0] = new com.navdy.service.library.device.connection.AcceptorListener(a, new com.navdy.service.library.network.TCPSocketAcceptor(21301), com.navdy.service.library.device.connection.ConnectionType.TCP_PROTOBUF);
            a0[1] = new com.navdy.service.library.device.connection.AcceptorListener(a, new com.navdy.service.library.network.BTSocketAcceptor("Navdy", NAVDY_PROTO_SERVICE_UUID), com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF);
        }
        return a0;
    }
    
    public com.navdy.service.library.events.DeviceInfo.Platform getDevicePlatform() {
        com.navdy.service.library.events.DeviceInfo.Platform a = null;
        try {
            com.navdy.service.library.device.RemoteDevice a0 = this.mRemoteDevice;
            a = null;
            if (a0 != null) {
                a = (this.mRemoteDevice.isConnected()) ? this.mRemoteDevice.getDeviceInfo().platform : null;
            }
        } catch(Throwable ignoredException) {
            a = null;
        }
        return a;
    }
    
    protected com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster[] getRemoteDeviceBroadcasters() {
        com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster[] a = null;
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            a = new com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster[1];
            a[0] = (com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster)new com.navdy.service.library.device.discovery.BTDeviceBroadcaster();
        } else {
            a = new com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster[2];
            a[0] = (com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster)new com.navdy.service.library.device.discovery.BTDeviceBroadcaster();
            a[1] = (com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster)new com.navdy.service.library.device.discovery.TCPRemoteDeviceBroadcaster(this.getApplicationContext());
        }
        return a;
    }
    
    protected void handleDeviceDisconnect(com.navdy.service.library.device.RemoteDevice a, com.navdy.service.library.device.connection.Connection$DisconnectCause a0) {
        super.handleDeviceDisconnect(a, a0);
        this.logger.d(new StringBuilder().append("HUDConnectionService: handleDeviceDisconnect ").append(a).append(", Cause :").append(a0).toString());
        if (this.mRemoteDevice == a && a0 == com.navdy.service.library.device.connection.Connection$DisconnectCause.ABORTED && this.serverMode) {
            this.logger.d("HUDConnectionService: Reporting the non fatal crash, Bluetooth disconnected");
            com.navdy.hud.app.util.CrashReportService.dumpCrashReportAsync(com.navdy.hud.app.util.CrashReportService$CrashType.BLUETOOTH_DISCONNECTED);
        }
    }
    
    protected void heartBeat() {
        if ($SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State[this.state.ordinal()] == 3) {
            this.search.next();
        }
        super.heartBeat();
    }
    
    public boolean isPromiscuous() {
        return com.navdy.hud.app.device.PowerManager.isAwake();
    }
    
    public boolean needAutoSearch() {
        boolean b;
        boolean b0 = this.needAutoSearch;
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (com.navdy.hud.app.device.PowerManager.isAwake()) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public void onCreate() {
        super.onCreate();
        com.navdy.service.library.device.link.LinkManager.registerFactory(com.navdy.service.library.device.connection.ConnectionType.BT_IAP2_LINK, this.iAPLinkFactory);
        com.navdy.service.library.device.link.LinkManager.registerFactory(com.navdy.service.library.device.connection.ConnectionType.EA_PROTOBUF, this.iAPLinkFactory);
        this.gpsManager.setConnectionService(this);
        this.routeRecorder.setConnectionService(this);
        this.serverMode = true;
        this.inProcess = false;
        com.navdy.service.library.util.NetworkActivityTracker.getInstance().start();
        this.fileTransferHandler = new com.navdy.hud.app.service.FileTransferHandler(this.getApplicationContext());
        if (!com.navdy.hud.app.util.DeviceUtil.isUserBuild()) {
            android.content.IntentFilter a = new android.content.IntentFilter();
            a.addAction("LINK_BANDWIDTH_LEVEL_CHANGED");
            a.addCategory("NAVDY_LINK");
            this.debugReceiver = new com.navdy.hud.app.service.HudConnectionService$2(this);
            this.registerReceiver(this.debugReceiver, a);
        }
    }
    
    public void onDestroy() {
        super.onDestroy();
        this.routeRecorder.stopRecording(false);
        this.gpsManager.shutdown();
        this.unregisterReceiver(this.bluetoothReceiver);
        if (this.debugReceiver != null) {
            this.unregisterReceiver(this.debugReceiver);
        }
    }
    
    public void onDeviceConnected(com.navdy.service.library.device.RemoteDevice a) {
        super.onDeviceConnected(a);
        this.fileTransferHandler.onDeviceConnected(a);
        this.startProxyService();
    }
    
    public void onDeviceDisconnected(com.navdy.service.library.device.RemoteDevice a, com.navdy.service.library.device.connection.Connection$DisconnectCause a0) {
        super.onDeviceDisconnected(a, a0);
        this.fileTransferHandler.onDeviceDisconnected();
    }
    
    protected boolean processEvent(byte[] eventData, com.navdy.service.library.events.NavdyEvent.MessageType messageType) {
        boolean b = false;
//        switch($SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType[messageType.ordinal()]) {
        switch (messageType) {

//            case 10:
            case ConnectionStateChange:
                try {
                    switch (((ConnectionStateChange) NavdyEventUtil.messageFromEvent(this.mWire.parseFrom(eventData, NavdyEvent.class))).state) {
                        case CONNECTION_DISCONNECTED:
                            int delay = SystemProperties.getInt(APP_TERMINATION_RECONNECT_DELAY_MS, 500);
                            this.deviceReconnectRunnable.setReason(REASON_CONNECTION_DISCONNECTED);
                            this.serviceHandler.postDelayed(this.deviceReconnectRunnable, (long) delay);
                            break;
                        case CONNECTION_CONNECTED:
                        case CONNECTION_LINK_LOST:
                            this.serviceHandler.removeCallbacks(this.deviceReconnectRunnable);
                            break;
                    }
                } catch (Throwable th) {
                    this.logger.e(th);
                }
                return false;
//            case 9:
            case FileTransferData:
                try {
                    this.fileTransferHandler.onFileTransferData((FileTransferData) NavdyEventUtil.messageFromEvent(this.mWire.parseFrom(eventData, NavdyEvent.class)));
                    return true;
                } catch (Throwable t222) {
                    this.logger.e(t222);
                    return true;
                }
//            case 8:
            case FileTransferStatus:
                try {
                    this.fileTransferHandler.onFileTransferStatus((FileTransferStatus) NavdyEventUtil.messageFromEvent(this.mWire.parseFrom(eventData, NavdyEvent.class)));
                    return true;
                } catch (Throwable t22) {
                    this.logger.e(t22);
                    return true;
                }
//            case 7:
            case FileTransferRequest:
                try {
                    this.fileTransferHandler.onFileTransferRequest((FileTransferRequest) NavdyEventUtil.messageFromEvent(this.mWire.parseFrom(eventData, NavdyEvent.class)));
                    return true;
                } catch (Throwable t2) {
                    this.logger.e(t2);
                    return true;
                }
//            case 6:
            case StopDrivePlaybackEvent:
                this.routeRecorder.stopPlayback();
                this.logger.v("onStopDrivePlayback");
                return false;
//            case 5:
            case StartDrivePlaybackEvent:
                parseStartDrivePlaybackEvent(eventData);
                return false;
//            case 4:
            case DriveRecordingsRequest:
                this.routeRecorder.requestRecordings();
                this.logger.v("onDriveRecordingsRequest");
                return true;
//            case 3:
                case StopDriveRecordingEvent:
                this.routeRecorder.stopRecording(false);
                this.logger.v("onStopDriveRecording");
                return true;
//            case 2:
            case StartDriveRecordingEvent:
                return parseStartDriveRecordingEvent(eventData);
//            case 1:
            case Coordinate:
                try {
                    if (this.isSimulatingGpsCoordinates) {
                        return true;
                    }
                    this.gpsManager.feedLocation((Coordinate) NavdyEventUtil.messageFromEvent(this.mWire.parseFrom(eventData, NavdyEvent.class)));
                    return true;
                } catch (Throwable t) {
                    this.logger.e(t);
                    return true;
                }
//            case 11:
            case AudioStatus:
                try {
                    AudioStatus message = (AudioStatus) NavdyEventUtil.messageFromEvent(this.mWire.parseFrom(eventData, NavdyEvent.class));
                    if (message != null && message.profileType == AudioStatus.ProfileType.AUDIO_PROFILE_HFP) {
                        this.logger.d("AudioStatus for HFP received");
                        if (!Wire.get(message.isConnected, Boolean.FALSE)) {
                            this.logger.d("AudioStatus: HFP disconnected");
                            setBandwidthLevel(1);
                            break;
                        }
                        this.logger.d("AudioStatus: HFP connected");
                        if (!Wire.get(message.hasSCOConnection, Boolean.FALSE)) {
                            this.logger.d("AudioStatus: does not have SCO connection");
                            setBandwidthLevel(1);
                            break;
                        }
                        this.logger.d("AudioStatus: has SCO connection");
                        setBandwidthLevel(0);
                        break;
                    }
                } catch (Throwable th) {
                    this.logger.e(th);
                    break;
                }
        }
        return false;
    }
    
    protected boolean processLocalEvent(byte[] a, com.navdy.service.library.events.NavdyEvent.MessageType a0) {
        boolean b = false;
        com.navdy.service.library.events.NavdyEvent.MessageType a1 = com.navdy.service.library.events.NavdyEvent.MessageType.ConnectionRequest;
        label3: {
            label2: {
                label1: {
                    label0: {
                        label5: {
                            label4: {
                                Throwable a2 = null;
                                if (a0 != a1) {
                                    break label4;
                                }
                                try {
                                    com.navdy.service.library.events.connection.ConnectionRequest a3 = (com.navdy.service.library.events.connection.ConnectionRequest)((com.navdy.service.library.events.NavdyEvent)this.mWire.parseFrom(a, com.navdy.service.library.events.NavdyEvent.class)).getExtension(com.navdy.service.library.events.Ext_NavdyEvent.connectionRequest);
                                    if (a3 == null) {
                                        b = true;
                                        break label3;
                                    } else {
                                        switch($SwitchMap$com$navdy$service$library$events$connection$ConnectionRequest$Action[a3.action.ordinal()]) {
                                            case 3: {
                                                this.serviceHandler.post((Runnable)new com.navdy.hud.app.service.HudConnectionService$5(this));
                                                b = true;
                                                break label3;
                                            }
                                            case 2: {
                                                this.setState(com.navdy.service.library.device.connection.ConnectionService$State.SEARCHING);
                                                b = true;
                                                break label3;
                                            }
                                            case 1: {
                                                if (a3.remoteDeviceId == null) {
                                                    this.setActiveDevice((com.navdy.service.library.device.RemoteDevice)null);
                                                    b = true;
                                                    break label3;
                                                } else {
                                                    this.selectDevice(new com.navdy.service.library.device.NavdyDeviceId(a3.remoteDeviceId));
                                                    b = true;
                                                    break label3;
                                                }
                                            }
                                            default: {
                                                b = true;
                                                break label3;
                                            }
                                        }
                                    }
                                } catch(Throwable a4) {
                                    a2 = a4;
                                }
                                this.logger.e(a2);
                                break label5;
                            }
                            if (a0 == com.navdy.service.library.events.NavdyEvent.MessageType.StartDrivePlaybackEvent) {
                                break label2;
                            }
                            if (a0 == com.navdy.service.library.events.NavdyEvent.MessageType.StopDrivePlaybackEvent) {
                                break label1;
                            }
                            if (a0 == com.navdy.service.library.events.NavdyEvent.MessageType.StartDriveRecordingEvent) {
                                break label0;
                            }
                            if (a0 == com.navdy.service.library.events.NavdyEvent.MessageType.StopDriveRecordingEvent) {
                                this.routeRecorder.stopRecording(false);
                                this.forwardEventLocally(a);
                            }
                        }
                        b = false;
                        break label3;
                    }
                    b = this.parseStartDriveRecordingEvent(a);
                    break label3;
                }
                this.routeRecorder.stopPlayback();
                this.forwardEventLocally(a);
                this.logger.v("onStopDrivePlayback");
                b = true;
                break label3;
            }
            this.parseStartDrivePlaybackEvent(a);
            this.forwardEventLocally(a);
            b = true;
        }
        return b;
    }
    
    public void reconnect(String s) {
        synchronized(this) {
            this.broadcastReconnectingIntent(s);
            super.reconnect(s);
        }
        /*monexit(this)*/;
    }
    
    public boolean reconnectAfterDeadConnection() {
        return true;
    }
    
    protected void sendEventsOnLocalConnect() {
        this.logger.v("sendEventsOnLocalConnect");
        com.navdy.hud.app.debug.RouteRecorder a = com.navdy.hud.app.debug.RouteRecorder.getInstance();
        if (a.isRecording()) {
            String s = a.getLabel();
            if (s == null) {
                this.logger.v("sendEventsOnLocalConnect: invalid label");
            } else {
                this.forwardEventLocally((com.squareup.wire.Message)new com.navdy.service.library.events.debug.StartDriveRecordingEvent(s));
                this.logger.v(new StringBuilder().append("sendEventsOnLocalConnect: send recording event:").append(s).toString());
            }
        }
    }
    
    public void setSimulatingGpsCoordinates(boolean b) {
        this.isSimulatingGpsCoordinates = b;
    }
}
