package com.navdy.hud.app.device.gps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.SystemClock;
import android.os.UserHandle;

import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.debug.RouteRecorder;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport;
import com.navdy.hud.app.service.HudConnectionService;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.location.TransmitLocation;
import com.navdy.service.library.log.Logger;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static com.navdy.hud.app.device.gps.GpsConstants.NAVDY_GPS_PROVIDER;

public class GpsManager {
    final private static int ACCEPTABLE_THRESHOLD = 2;
    final private static String DISCARD_TIME = "DISCARD_TIME";
    final private static String GPS_SYSTEM_PROPERTY = "persist.sys.hud_gps";
    final public static long INACCURATE_GPS_REPORT_INTERVAL;
    final private static int INITIAL_INTERVAL = 15000;
    final private static int KEEP_PHONE_GPS_ON = -1;
    final private static int LOCATION_ACCURACY_THROW_AWAY_THRESHOLD = 30;
    final private static int LOCATION_TIME_THROW_AWAY_THRESHOLD = 2000;
    final public static double MAXIMUM_STOPPED_SPEED = 0.22353333333333333;
    final private static double METERS_PER_MILE = 1609.44;
    final public static long MINIMUM_DRIVE_TIME = 3000L;
    final public static double MINIMUM_DRIVING_SPEED = 2.2353333333333336;
    final static int MSG_NMEA = 1;
    final static int MSG_SATELLITE_STATUS = 2;
    final private static int SECONDS_PER_HOUR = 3600;
    final private static String SET_LOCATION_DISCARD_TIME_THRESHOLD = "SET_LOCATION_DISCARD_TIME_THRESHOLD";
    final private static String SET_UBLOX_ACCURACY = "SET_UBLOX_ACCURACY";
    final private static TransmitLocation START_SENDING_LOCATION;
    final private static String START_UBLOX = "START_REPORTING_UBLOX_LOCATION";
    final private static TransmitLocation STOP_SENDING_LOCATION;
    final private static String STOP_UBLOX = "STOP_REPORTING_UBLOX_LOCATION";
    final private static String TAG_GPS = "[Gps-i] ";
    final private static String TAG_GPS_LOC = "[Gps-loc] ";
    final private static String TAG_GPS_LOC_NAVDY = "[Gps-loc-navdy] ";
    final private static String TAG_GPS_NMEA = "[Gps-nmea] ";
    final private static String TAG_GPS_STATUS = "[Gps-stat] ";
    final private static String TAG_PHONE_LOC = "[Phone-loc] ";
    final private static String UBLOX_ACCURACY = "UBLOX_ACCURACY";
    final private static float UBLOX_MIN_ACCEPTABLE_THRESHOLD = 5f;
    final private static long WARM_RESET_INTERVAL;
//    final public static String MOCK_PROVIDER = LocationManager.GPS_PROVIDER;
    final public static String MOCK_PROVIDER = LocationManager.NETWORK_PROVIDER;
//    final public static String MOCK_PROVIDER = NAVDY_GPS_PROVIDER;
    final private static Logger sLogger;
    final private static GpsManager singleton;
    private LocationSource activeLocationSource;
    private HudConnectionService connectionService;
    private boolean driving;
    private BroadcastReceiver eventReceiver;
    private volatile boolean extrapolationOn;
    private volatile long extrapolationStartTime;
    private boolean firstSwitchToUbloxFromPhone;
    private float gpsAccuracy;
    private long gpsLastEventTime;
    private long gpsManagerStartTime;
    private Handler handler;
    private long inaccurateGpsCount;
    private boolean isDebugTTSEnabled;
    private boolean keepPhoneGpsOn;
    private long lastInaccurateGpsTime;
    private Coordinate lastPhoneCoordinate;
    private long lastPhoneCoordinateTime;
    private Location lastUbloxCoordinate;
    private long lastUbloxCoordinateTime;
    private Runnable locationFixRunnable;
    private LocationManager locationManager;
    private LocationListener navdyGpsLocationListener;
    private Handler.Callback nmeaCallback;
    private Handler nmeaHandler;
    private android.os.HandlerThread nmeaHandlerThread;
    private GpsNmeaParser nmeaParser;
    private Runnable noLocationUpdatesRunnable;
    private Runnable noPhoneLocationFixRunnable;
    private ArrayList queue;
    private Runnable queueCallback;
    private volatile boolean startUbloxCalled;
    private boolean switchBetweenPhoneUbloxAllowed;
    private long transitionStartTime;
    private boolean transitioning;
    private LocationListener ubloxGpsLocationListener;
    private GpsStatus.NmeaListener ubloxGpsNmeaListener;
    private GpsStatus.Listener ubloxGpsStatusListener;
    private boolean warmResetCancelled;
    private Runnable warmResetRunnable;
    
    static {
        sLogger = new Logger(GpsManager.class);
        INACCURATE_GPS_REPORT_INTERVAL = TimeUnit.SECONDS.toMillis(30L);
        WARM_RESET_INTERVAL = TimeUnit.SECONDS.toMillis(160L);
        START_SENDING_LOCATION = new TransmitLocation(Boolean.TRUE);
        STOP_SENDING_LOCATION = new TransmitLocation(Boolean.FALSE);
        singleton = new GpsManager();
    }
    
    private GpsManager() {
        this.queue = new ArrayList();
        this.handler = new Handler(Looper.getMainLooper());
        this.ubloxGpsNmeaListener = new GpsStatus.NmeaListener() {

            public void onNmeaReceived(long j, String s) {
                try {
                    if (sLogger.isLoggable(2)) {
                        sLogger.v("[Gps-nmea] " + s);
                    }
                    GpsManager.this.processNmea(s);
                } catch(Throwable a) {
                    sLogger.e(a);
                }
            }
        };
        this.ubloxGpsLocationListener = new LocationListener() {

            public void onLocationChanged(Location a) {
                label1: {
                    Throwable a0;
                    try {
                        if (sLogger.isLoggable(2)) {
                            sLogger.d("[Gps-loc] " + ((a.isFromMockProvider()) ? "[Mock] " : "") + a);
                        }
                        if (!GpsManager.this.startUbloxCalled) {
                            break label1;
                        }
                        if (a.isFromMockProvider()) {
                            break label1;
                        }
                        if (GpsManager.this.activeLocationSource == LocationSource.UBLOX) {
                            break label1;
                        }
                        GpsManager.this.activeLocationSource = LocationSource.UBLOX;
                        GpsManager.this.handler.removeCallbacks(GpsManager.this.noPhoneLocationFixRunnable);
                        GpsManager.this.handler.removeCallbacks(GpsManager.this.locationFixRunnable);
                        GpsManager.this.handler.postDelayed(GpsManager.this.locationFixRunnable, 1000L);
                        GpsManager.this.stopSendingLocation();
                        int i1 = (int) (SystemClock.elapsedRealtime() - GpsManager.this.gpsLastEventTime) / 1000;
                        long j = SystemClock.elapsedRealtime() - GpsManager.this.lastPhoneCoordinateTime;
                        float f = (GpsManager.this.lastPhoneCoordinate == null) ? -1f : (j >= 3000L) ? -1f : GpsManager.this.lastPhoneCoordinate.accuracy;
                        int i = (int) ((GpsManager.this.lastUbloxCoordinate == null) ? -1f : GpsManager.this.lastUbloxCoordinate.getAccuracy());
                        ConnectionServiceAnalyticsSupport.recordGpsAcquireLocation(String.valueOf(i1), String.valueOf(i));
                        GpsManager.this.gpsLastEventTime = SystemClock.elapsedRealtime();
                        GpsManager.this.markLocationFix("Switched to Ublox Gps", "Phone =" + ((f != -1f) ? Float.valueOf(f) : "n/a") + " ublox =" + i, false, true);

                        sLogger.v("[Gps-loc] [LocationProvider] switched from [" + GpsManager.this.activeLocationSource + "] to [" + LocationSource.UBLOX + "] " + "Phone =" + (((Float.compare(f, -1f)) != 0) ? Float.valueOf(f) : "n/a") + " ublox =" + i + " time=" + j);
                    } catch (Throwable a9) {
                        a0 = a9;
                        sLogger.e(a0);
                    }
                }
            }

            public void onProviderDisabled(String s) {
                sLogger.i("[Gps-stat] " + s + "[disabled]");
            }

            public void onProviderEnabled(String s) {
                sLogger.i("[Gps-stat] " + s + "[enabled]");
            }

            public void onStatusChanged(String s, int i, Bundle a) {
                if (PowerManager.isAwake()) {
                    sLogger.i("[Gps-stat] " + s + "," + i);
                }
            }
        };
        this.navdyGpsLocationListener = new LocationListener() {
            private float accuracyMax = 0.0f;
            private float accuracyMin = 0.0f;
            private int accuracySampleCount = 0;
            private long accuracySampleStartTime = SystemClock.elapsedRealtime();
            private float accuracySum = 0.0f;

            private void collectAccuracyStats(float f) {
                if (f != 0.0f) {
                    this.accuracySum = this.accuracySum + f;
                    this.accuracySampleCount = this.accuracySampleCount + 1;
                    float f0 = this.accuracyMin;
                    int i = Float.compare(f0, 0.0f);
                    label0: {
                        label1: {
                            if (i == 0) {
                                break label1;
                            }
                            if (!(f < this.accuracyMin)) {
                                break label0;
                            }
                        }
                        this.accuracyMin = f;
                    }
                    if (this.accuracyMax < f) {
                        this.accuracyMax = f;
                    }
                }
                if (GpsManager.this.lastUbloxCoordinateTime - this.accuracySampleStartTime > 300000L) {
                    if (this.accuracySampleCount > 0) {
                        ConnectionServiceAnalyticsSupport.recordGpsAccuracy(Integer.toString(Math.round(this.accuracyMin)), Integer.toString(Math.round(this.accuracyMax)), Integer.toString(Math.round(this.accuracySum / (float)this.accuracySampleCount)));
                        this.accuracySum = 0.0f;
                        this.accuracyMax = 0.0f;
                        this.accuracyMin = 0.0f;
                        this.accuracySampleCount = 0;
                    }
                    this.accuracySampleStartTime = GpsManager.this.lastUbloxCoordinateTime;
                }
            }

            public void onLocationChanged(Location location) {
                try {
                    if (!GpsManager.this.warmResetCancelled) {
                        GpsManager.this.cancelUbloxResetRunnable();
                    }
                    if (!RouteRecorder.getInstance().isPlaying()) {
                        GpsManager.this.updateDrivingState(location);
                        GpsManager.this.lastUbloxCoordinate = location;
                        GpsManager.this.lastUbloxCoordinateTime = SystemClock.elapsedRealtime();
                        this.collectAccuracyStats(location.getAccuracy());
                        HudConnectionService connectionService = GpsManager.this.connectionService;
                        label3:
                        {
                            label4:
                            {
                                if (connectionService == null) {
                                    break label4;
                                }
                                if (GpsManager.this.connectionService.isConnected()) {
                                    break label3;
                                }
                            }
                            if (GpsManager.this.activeLocationSource == LocationSource.UBLOX) {
                                return;
                            }
                            sLogger.v("not connected with phone, start uBloxReporting");
                            GpsManager.this.startUbloxReporting();
                            return;
                        }
                        label2:
                        {
                            if (GpsManager.this.activeLocationSource == LocationSource.UBLOX) {
                                break label2;
                            }
                            long sinceLastPhone = SystemClock.elapsedRealtime() - GpsManager.this.lastPhoneCoordinateTime;
                            if (sinceLastPhone <= 3000L) {
                                break label2;
                            }
                            sLogger.v("phone threshold exceeded= " + sinceLastPhone + ", start uBloxReporting");
                            GpsManager.this.startUbloxReporting();
                            return;
                        }
//                        label1: {
//                            if (!GpsManager.this.firstSwitchToUbloxFromPhone) {
//                                break label1;
//                            }
//                            if (!GpsManager.this.switchBetweenPhoneUbloxAllowed) {
//                                return;
//                            }
//                        }
                        GpsManager.this.checkGpsToPhoneAccuracy(location);
                    }
                } catch (Throwable a3) {
                    sLogger.e(a3);
                }
            }

            public void onProviderDisabled(String s) {
            }

            public void onProviderEnabled(String s) {
            }

            public void onStatusChanged(String s, int i, Bundle a) {
            }
        };
        this.ubloxGpsStatusListener = new GpsStatus.Listener() {

            public void onGpsStatusChanged(int i) {
                switch(i) {
                    case 3: {
                        sLogger.i("[Gps-stat] gps got first fix");
                        break;
                    }
                    case 2: {
                        sLogger.w("[Gps-stat] gps stopped searching");
                        break;
                    }
                    case 1: {
                        sLogger.i("[Gps-stat] gps searching...");
                        break;
                    }
                }
            }
        };
        this.noPhoneLocationFixRunnable = new Runnable() {

            public void run() {
                label0: {
                    Throwable a = null;
                    label1: {
                        boolean b = false;
                        try {
                            if (GpsManager.this.activeLocationSource != LocationSource.UBLOX) {
                                GpsManager.this.updateDrivingState(null);
                                GpsManager.this.startSendingLocation();
                                b = true;
                            } else {
                                sLogger.i("[Gps-loc] noPhoneLocationFixRunnable: has location fix now");
                                b = false;
                            }
                        } catch(Throwable a0) {
                            a = a0;
                            break label1;
                        }
                        if (!b) {
                            break label0;
                        }
                        GpsManager.this.handler.postDelayed(GpsManager.this.noPhoneLocationFixRunnable, 5000L);
                        break label0;
                    }
                    try {
                        sLogger.e(a);
                    } catch(Throwable a1) {
                        GpsManager.this.handler.postDelayed(GpsManager.this.noPhoneLocationFixRunnable, 5000L);
                        throw a1;
                    }
                    GpsManager.this.handler.postDelayed(GpsManager.this.noPhoneLocationFixRunnable, 5000L);
                }
            }
        };
        this.noLocationUpdatesRunnable = new Runnable() {

            public void run() {
                GpsManager.this.updateDrivingState(null);
            }
        };
        this.warmResetRunnable = new Runnable() {

            public void run() {
                try {
                    sLogger.v("issuing warm reset to ublox, no location report in last " + WARM_RESET_INTERVAL);
                    GpsManager.this.sendGpsEventBroadcast("GPS_WARM_RESET_UBLOX", null);
                } catch(Throwable a) {
                    sLogger.e(a);
                }
            }
        };
        this.locationFixRunnable = new Runnable() {

            public void run() {
                label0: {
                    boolean b = false;
                    Throwable a = null;
                    label1: {
                        boolean b0 = false;
                        label4: {
                            try {
                                b = true;
                                long j = SystemClock.elapsedRealtime() - GpsManager.this.lastUbloxCoordinateTime;
                                if (j < 3000L) {
                                    b0 = true;
                                    break label4;
                                }
                                b = true;
                                sLogger.i("[Gps-loc] locationFixRunnable: lost location fix[" + j + "]");
                                boolean b1 = GpsManager.this.extrapolationOn;
                                label2: {
                                    label3: {
                                        if (!b1) {
                                            break label3;
                                        }
                                        b = true;
                                        long j0 = SystemClock.elapsedRealtime() - GpsManager.this.extrapolationStartTime;
                                        sLogger.i("[Gps-loc] locationFixRunnable: extrapolation on time:" + j0);
                                        if (j0 < 5000L) {
                                            break label2;
                                        }
                                        b = true;
                                        GpsManager.this.extrapolationOn = false;
                                        GpsManager.this.extrapolationStartTime = 0L;
                                        sLogger.i("[Gps-loc] locationFixRunnable: expired");
                                    }
                                    b = false;
                                    GpsManager.this.updateDrivingState(null);
                                    sLogger.e("[Gps-loc] lost gps fix");
                                    GpsManager.this.handler.removeCallbacks(GpsManager.this.locationFixRunnable);
                                    GpsManager.this.handler.removeCallbacks(GpsManager.this.noPhoneLocationFixRunnable);
                                    GpsManager.this.handler.postDelayed(GpsManager.this.noPhoneLocationFixRunnable, 5000L);
                                    GpsManager.this.startSendingLocation();
                                    ConnectionServiceAnalyticsSupport.recordGpsLostLocation(String.valueOf((SystemClock.elapsedRealtime() - GpsManager.this.gpsLastEventTime) / 1000L));
                                    GpsManager.this.gpsLastEventTime = SystemClock.elapsedRealtime();
                                    b0 = false;
                                    break label4;
                                }
                                b = true;
                                sLogger.i("[Gps-loc] locationFixRunnable: reset");
                            } catch(Throwable a0) {
                                a = a0;
                                break label1;
                            }
                            GpsManager.this.handler.postDelayed(GpsManager.this.locationFixRunnable, 1000L);
                            break label0;
                        }
                        if (!b0) {
                            break label0;
                        }
                        GpsManager.this.handler.postDelayed(GpsManager.this.locationFixRunnable, 1000L);
                        break label0;
                    }
                    try {
                        sLogger.e(a);
                    } catch(Throwable a1) {
                        if (b) {
                            GpsManager.this.handler.postDelayed(GpsManager.this.locationFixRunnable, 1000L);
                        }
                        throw a1;
                    }
                    if (b) {
                        GpsManager.this.handler.postDelayed(GpsManager.this.locationFixRunnable, 1000L);
                    }
                }
            }
        };
        this.nmeaCallback = new Handler.Callback() {

            public boolean handleMessage(Message a) {
                try {
                    switch(a.what) {
                        case 2: {
                            if (SystemClock.elapsedRealtime() - GpsManager.this.gpsManagerStartTime <= 15000L) {
                                break;
                            }
                            Bundle a0 = (Bundle)a.obj;
                            Bundle a1 = new Bundle();
                            a1.putBundle("satellite_data", a0);
                            GpsManager.this.sendGpsEventBroadcast("GPS_SATELLITE_STATUS", a1);
                            break;
                        }
                        case 1: {
                            GpsManager.this.nmeaParser.parseNmeaMessage((String)a.obj);
                            break;
                        }
                    }
                } catch(Throwable a2) {
                    sLogger.e(a2);
                }
                return true;
            }
        };
        this.queueCallback = new Runnable() {

            public void run() {
                try {
                    Context a = HudApplication.getAppContext();
                    UserHandle a0 = Process.myUserHandle();
                    int i = GpsManager.this.queue.size();
                    if (i > 0) {
                        int i0 = 0;
                        while(i0 < i) {
                            Intent a1 = (Intent) GpsManager.this.queue.get(i0);
                            a.sendBroadcastAsUser(a1, a0);
                            sLogger.v(new StringBuilder().append("sent-queue gps event user:").append(a1.getAction()).toString());
                            i0 = i0 + 1;
                        }
                        GpsManager.this.queue.clear();
                    }
                } catch(Throwable a2) {
                    sLogger.e(a2);
                }
            }
        };
        this.eventReceiver = new BroadcastReceiver() {

            public void onReceive(Context a, Intent a0) {
                try {
                    String s = a0.getAction();
                    if (s != null) {
                        int i = 0;
                        RouteRecorder a1 = RouteRecorder.getInstance();
                        int i0 = s.hashCode();
                        label0: {
                            switch(i0) {
                                case -47674184: {
                                    if (!s.equals("EXTRAPOLATION")) {
                                        break;
                                    }
                                    i = 2;
                                    break label0;
                                }
                                case -1788590639: {
                                    if (!s.equals("GPS_DR_STOPPED")) {
                                        break;
                                    }
                                    i = 1;
                                    break label0;
                                }
                                case -1801456507: {
                                    if (!s.equals("GPS_DR_STARTED")) {
                                        break;
                                    }
                                    i = 0;
                                    break label0;
                                }
                                default: {
                                    sLogger.i("ERROR: eventReceiver hashCode: " + Integer.toString(i0) + ", s: " + s);
                                }
                            }
                            i = -1;
                        }
                        switch(i) {
                            case 2: {
                                GpsManager.this.extrapolationOn = a0.getBooleanExtra("EXTRAPOLATION_FLAG", false);
                                if (GpsManager.this.extrapolationOn) {
                                    GpsManager.this.extrapolationStartTime = SystemClock.elapsedRealtime();
                                } else {
                                    GpsManager.this.extrapolationStartTime = 0L;
                                }
                                sLogger.v("extrapolation is:" + GpsManager.this.extrapolationOn + " time:" + GpsManager.this.extrapolationStartTime);
                                break;
                            }
                            case 1: {
                                if (!a1.isRecording()) {
                                    break;
                                }
                                a1.injectMarker("GPS_DR_STOPPED");
                                break;
                            }
                            case 0: {
                                if (!a1.isRecording()) {
                                    break;
                                }
                                String s0 = a0.getStringExtra("drtype");
                                if (s0 == null) {
                                    s0 = "";
                                }
                                a1.injectMarker("GPS_DR_STARTED " + s0);
                                break;
                            }
                        }
                    }
                } catch(Throwable a2) {
                    sLogger.e(a2);
                }
            }
        };
        android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
        try {
            int i = com.navdy.hud.app.util.os.SystemProperties.getInt(GPS_SYSTEM_PROPERTY, 0);
            if (i == -1) {
                this.keepPhoneGpsOn = true;
                this.switchBetweenPhoneUbloxAllowed = true;
                sLogger.v("switch between phone and ublox allowed");
            }
            sLogger.v("keepPhoneGpsOn[" + this.keepPhoneGpsOn + "] val[" + i + "]");
            this.isDebugTTSEnabled = new java.io.File("/sdcard/debug_tts").exists();
            this.setUbloxAccuracyThreshold();
            this.setUbloxTimeDiscardThreshold();
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
        this.locationManager = (LocationManager)a.getSystemService(Context.LOCATION_SERVICE);
        boolean b = false;
        if (this.locationManager != null) {
            b = this.locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }
        android.location.LocationProvider a1 = null;
        if (b) {
            a1 = this.locationManager.getProvider(LocationManager.GPS_PROVIDER);
            if (a1 != null) {
                this.nmeaHandlerThread = new android.os.HandlerThread("navdynmea");
                this.nmeaHandlerThread.start();
                this.nmeaHandler = new Handler(this.nmeaHandlerThread.getLooper(), this.nmeaCallback);
                this.nmeaParser = new GpsNmeaParser(sLogger, this.nmeaHandler);
                this.locationManager.addGpsStatusListener(this.ubloxGpsStatusListener);
                this.locationManager.addNmeaListener(this.ubloxGpsNmeaListener);
            }
        }
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            sLogger.i("[Gps-i] setting up ublox gps");
            if (a1 == null) {
                sLogger.e("[Gps-i] gps provider not found");
            } else {
                this.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0L, 0.0f, this.ubloxGpsLocationListener);
                try {
                    this.locationManager.requestLocationUpdates(NAVDY_GPS_PROVIDER, 0L, 0.0f, this.navdyGpsLocationListener);
                    sLogger.v("requestLocationUpdates successful for NAVDY_GPS_PROVIDER");
                } catch(Throwable a2) {
                    sLogger.e("requestLocationUpdates", a2);
                }
                sLogger.i("[Gps-i] ublox gps listeners installed");
            }
        } else {
            sLogger.i("[Gps-i] not a Hud device,not setting up ublox gps");
        }
        this.locationManager.addTestProvider(MOCK_PROVIDER,
                false, false, false, false,
                true, true, true,
                Criteria.POWER_LOW, Criteria.ACCURACY_FINE);
        this.locationManager.setTestProviderEnabled(MOCK_PROVIDER, true);

        this.locationManager.setTestProviderStatus(MOCK_PROVIDER, LocationProvider.AVAILABLE, null, System.currentTimeMillis());
        sLogger.i("[Gps-i] added mock network provider");

        Boolean gps_enabled = this.locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        Boolean network_enabled = this.locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        Boolean passive_enabled = this.locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);

        sLogger.i("[Gps-i] gps_enabled: " + gps_enabled);
        sLogger.i("[Gps-i] network_enabled: " + network_enabled);
        sLogger.i("[Gps-i] passive_enabled: " + passive_enabled);

        this.gpsManagerStartTime = android.os.SystemClock.elapsedRealtime();
        this.gpsLastEventTime = this.gpsManagerStartTime;
        com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.recordGpsAttemptAcquireLocation();
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            this.setUbloxResetRunnable();
        }
        android.content.IntentFilter a3 = new android.content.IntentFilter();
        a3.addAction("GPS_DR_STARTED");
        a3.addAction("GPS_DR_STOPPED");
        a3.addAction("EXTRAPOLATION");
        a.registerReceiver(this.eventReceiver, a3);
    }

    private static Location androidLocationFromCoordinate(Coordinate a) {
        return androidLocationFromCoordinate(a, MOCK_PROVIDER);
    }

    private static Location androidLocationFromCoordinate(Coordinate a, String provider) {
        Location a0 = new Location(provider);
        a0.setLatitude(a.latitude);
        a0.setLongitude(a.longitude);
        a0.setAccuracy(a.accuracy);
        a0.setAltitude(a.altitude);
        a0.setBearing(a.bearing);
        a0.setSpeed(a.speed);
        a0.setTime(System.currentTimeMillis());
        a0.setElapsedRealtimeNanos(android.os.SystemClock.elapsedRealtimeNanos());
        return a0;
    }
    
    private void cancelUbloxResetRunnable() {
        this.handler.removeCallbacks(this.warmResetRunnable);
        this.warmResetCancelled = true;
    }
    
    private void checkGpsToPhoneAccuracy(Location location) {
        float accuracy = location.getAccuracy();
        long realtime = android.os.SystemClock.elapsedRealtime();
        if (sLogger.isLoggable(3)) {
            sLogger.v("[Gps-loc-navdy] checkGpsToPhoneAccuracy: " +
                    "ublox[" + location.getAccuracy() + "] " +
                    "phone[" + ((this.lastPhoneCoordinate == null) ? "n/a" : this.lastPhoneCoordinate.accuracy) + "] " +
                    "lastPhoneTime [" + ((this.lastPhoneCoordinate == null) ? "n/a" : Long.valueOf(realtime - this.lastPhoneCoordinateTime)) + "]");
        }
        if (accuracy != 0.0f) {
            if (this.activeLocationSource != LocationSource.UBLOX) {
                if (accuracy + 2f < this.lastPhoneCoordinate.accuracy) {
                    sLogger.v("ublox accuracy [" + accuracy + "] < phone accuracy[" + this.lastPhoneCoordinate.accuracy + "], start uBloxReporting, switch complete");
                    this.firstSwitchToUbloxFromPhone = true;
                    this.markLocationFix("Switched to Ublox Gps", "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox =" + accuracy, false, false);
                    this.startUbloxReporting();
                } else if (sLogger.isLoggable(3)) {
                    sLogger.v("ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "] no action");
                }
            } else if (this.lastPhoneCoordinate != null && android.os.SystemClock.elapsedRealtime() - this.lastPhoneCoordinateTime <= 3000L) {
                if (this.lastPhoneCoordinate.accuracy + 2f < accuracy) {
                    if (sLogger.isLoggable(3)) {
                        sLogger.v("[Gps-loc-navdy]  ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "]");
                    }
                    if (this.startUbloxCalled) {
                        if (accuracy <= 5f) {
                            sLogger.v("ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "], but not above threshold");
                            this.stopSendingLocation();
                        } else {
                            sLogger.v("ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "], stop uBloxReporting");
                            this.handler.removeCallbacks(this.locationFixRunnable);
                            this.handler.removeCallbacks(this.noPhoneLocationFixRunnable);
                            this.stopUbloxReporting();
                            sLogger.v("[Gps-loc] [LocationProvider] switched from [" + this.activeLocationSource + "] to [" + LocationSource.PHONE + "]");
                            this.activeLocationSource = LocationSource.PHONE;
                            this.markLocationFix("Switched to Phone Gps", "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox =" + accuracy, true, false);
                        }
                    }
                } else if (sLogger.isLoggable(3)) {
                    sLogger.v("ublox accuracy [" + accuracy + "] < phone accuracy[" + this.lastPhoneCoordinate.accuracy + "] no action");
                }
            }
        }
    }
    
    private void feedLocationToProvider(Location a, Coordinate a0) {
        try {
            if (sLogger.isLoggable(3)) {
                sLogger.d("[Phone-loc] " + a0);
            }
            if (a == null) {
                a = GpsManager.androidLocationFromCoordinate(a0);
            }
            this.updateDrivingState(a);
            this.locationManager.setTestProviderLocation(a.getProvider(), a);
            this.locationManager.setTestProviderLocation(
                    MOCK_PROVIDER,
                    GpsManager.androidLocationFromCoordinate(a0, LocationManager.GPS_PROVIDER)
            );

        } catch(Throwable a1) {
            sLogger.e("feedLocation", a1);
        }
    }
    
    public static GpsManager getInstance() {
        return singleton;
    }
    
    private void markLocationFix(String title, String info, boolean phone, boolean ublox) {
        android.os.Bundle a = new android.os.Bundle();
        a.putString("title", title);
        a.putString("info", info);
        a.putBoolean("phone", phone);
        a.putBoolean("ublox", ublox);
        this.sendGpsEventBroadcast(GpsConstants.GPS_EVENT_SWITCH, a);
    }
    
    private void processNmea(String s) {
        android.os.Message a = this.nmeaHandler.obtainMessage(1);
        a.obj = s;
        this.nmeaHandler.sendMessage(a);
    }
    
    private void sendGpsEventBroadcast(String s, android.os.Bundle a) {
        try {
            android.content.Intent a0 = new android.content.Intent(s);
            if (a != null) {
                a0.putExtras(a);
            }
            if (android.os.SystemClock.elapsedRealtime() - this.gpsManagerStartTime > 15000L) {
                android.content.Context a1 = com.navdy.hud.app.HudApplication.getAppContext();
                android.os.UserHandle a2 = android.os.Process.myUserHandle();
                if (this.queue.size() > 0) {
                    this.queueCallback.run();
                }
                a1.sendBroadcastAsUser(a0, a2);
            } else {
                this.queue.add(a0);
                this.handler.removeCallbacks(this.queueCallback);
                this.handler.postDelayed(this.queueCallback, 15000L);
            }
        } catch(Throwable a3) {
            sLogger.e(a3);
        }
    }
    
    private void sendSpeechRequest(com.navdy.service.library.events.audio.SpeechRequest a) {
        HudConnectionService a0 = this.connectionService;
        label0: {
            Throwable a1 = null;
            if (a0 == null) {
                break label0;
            }
            if (!this.connectionService.isConnected()) {
                break label0;
            }
            try {
                sLogger.i("[Gps-loc] send speech request");
                this.connectionService.sendMessage(a);
                break label0;
            } catch(Throwable a2) {
                a1 = a2;
            }
            sLogger.e(a1);
        }
    }
    
    private void setDriving(boolean b) {
        if (this.driving != b) {
            this.driving = b;
            this.sendGpsEventBroadcast(b ? "driving_started" : "driving_stopped", null);
        }
    }
    
    private void setUbloxAccuracyThreshold() {
        try {
            android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
            android.os.UserHandle a0 = android.os.Process.myUserHandle();
            android.content.Intent a1 = new android.content.Intent("SET_UBLOX_ACCURACY");
            a1.putExtra("UBLOX_ACCURACY", 30);
            a.sendBroadcastAsUser(a1, a0);
            sLogger.v("setUbloxAccuracyThreshold:30");
        } catch(Throwable a2) {
            sLogger.e("setUbloxAccuracyThreshold", a2);
        }
    }
    
    private void setUbloxResetRunnable() {
        sLogger.v("setUbloxResetRunnable");
        this.handler.postDelayed(this.warmResetRunnable, WARM_RESET_INTERVAL);
    }
    
    private void setUbloxTimeDiscardThreshold() {
        try {
            android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
            android.os.UserHandle a0 = android.os.Process.myUserHandle();
            android.content.Intent a1 = new android.content.Intent("SET_LOCATION_DISCARD_TIME_THRESHOLD");
            a1.putExtra("DISCARD_TIME", 2000);
            a.sendBroadcastAsUser(a1, a0);
            sLogger.v("setUbloxTimeDiscardThreshold:2000");
        } catch(Throwable a2) {
            sLogger.e("setUbloxTimeDiscardThreshold", a2);
        }
    }
    
    private void startSendingLocation() {
        HudConnectionService a = this.connectionService;
        label0: {
            Throwable a0 = null;
            if (a == null) {
                break label0;
            }
            if (!this.connectionService.isConnected()) {
                break label0;
            }
            try {
                sLogger.i("[Gps-loc] phone start sending location");
                this.connectionService.sendMessage(START_SENDING_LOCATION);
                break label0;
            } catch(Throwable a1) {
                a0 = a1;
            }
            sLogger.e(a0);
        }
    }
    
    private void stopSendingLocation() {
        label1: {
            Throwable a = null;
            label0: if (this.keepPhoneGpsOn) {
                sLogger.v("stopSendingLocation: keeping phone gps on");
                break label1;
            } else if (this.isDebugTTSEnabled) {
                sLogger.v("stopSendingLocation: not stopping, debug_tts enabled");
                break label1;
            } else {
                if (this.connectionService == null) {
                    break label1;
                }
                if (!this.connectionService.isConnected()) {
                    break label1;
                }
                {
                    try {
                        sLogger.i("[Gps-loc] phone stop sending location");
                        this.connectionService.sendMessage(STOP_SENDING_LOCATION);
                    } catch(Throwable a0) {
                        a = a0;
                        break label0;
                    }
                    break label1;
                }
            }
            sLogger.e(a);
        }
    }
    
    private void updateDrivingState(Location a) {
        label2: if (this.driving) {
            label3: {
                if (a == null) {
                    break label3;
                }
                if ((double)a.getSpeed() < MAXIMUM_STOPPED_SPEED) {
                    break label3;
                }
                this.transitioning = false;
                break label2;
            }
            if (this.transitioning) {
                if (android.os.SystemClock.elapsedRealtime() - this.transitionStartTime > 3000L) {
                    this.setDriving(false);
                    this.transitioning = false;
                }
            } else {
                this.transitioning = true;
                this.transitionStartTime = android.os.SystemClock.elapsedRealtime();
            }
        } else {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if ((double)a.getSpeed() > MINIMUM_DRIVING_SPEED) {
                        break label0;
                    }
                }
                this.transitioning = false;
                break label2;
            }
            if (this.transitioning) {
                if (android.os.SystemClock.elapsedRealtime() - this.transitionStartTime > 3000L) {
                    this.setDriving(true);
                    this.transitioning = false;
                }
            } else {
                this.transitioning = true;
                this.transitionStartTime = android.os.SystemClock.elapsedRealtime();
            }
        }
        this.handler.removeCallbacks(this.noLocationUpdatesRunnable);
        if (this.driving) {
            this.handler.postDelayed(this.noLocationUpdatesRunnable, 3000L);
        }
    }
    
    public void feedLocation(Coordinate coordinate) {
        boolean b = false;
        label7: {
            label5: {
                label6: {
                    if (Long.compare(this.lastPhoneCoordinateTime, 0L) > 0) {
                        break label6;
                    }
                    if (this.lastUbloxCoordinateTime <= 0L) {
                        break label5;
                    }
                }
                b = true;
                break label7;
            }
            b = false;
        }
        label1: {
            label4: {
                if (Float.compare(coordinate.accuracy, 30f) < 0) {
                    break label4;
                }
                if (!b) {
                    break label4;
                }
                this.inaccurateGpsCount = this.inaccurateGpsCount + 1L;
                this.gpsAccuracy = this.gpsAccuracy + coordinate.accuracy;
                label3: {
                    if (Long.compare(this.lastInaccurateGpsTime, 0L) == 0) {
                        break label3;
                    }
                    if (System.currentTimeMillis() - this.lastInaccurateGpsTime < INACCURATE_GPS_REPORT_INTERVAL) {
                        break label1;
                    }
                }
                float f0 = this.gpsAccuracy / (float)this.inaccurateGpsCount;
                sLogger.e("BAD gps accuracy (discarding) avg:" + f0 + ", count=" + this.inaccurateGpsCount + ", last coord: " + coordinate);
                this.inaccurateGpsCount = 0L;
                this.gpsAccuracy = 0.0f;
                this.lastInaccurateGpsTime = System.currentTimeMillis();
                break label1;
            }
            label2: {
                if (Long.compare(System.currentTimeMillis() - coordinate.timestamp, 2000L) < 0) {
                    break label2;
                }
                if (!b) {
                    break label2;
                }
                sLogger.e("OLD location from phone(discard) time:" + (System.currentTimeMillis() - coordinate.timestamp) + ", timestamp:" + coordinate.timestamp + " now:" + System.currentTimeMillis() + " lat:" + coordinate.latitude + " lng:" + coordinate.longitude);
                break label1;
            }
            label0: {
                if (this.lastPhoneCoordinate == null) {
                    break label0;
                }
                if (this.connectionService.getDevicePlatform() != com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS) {
                    break label0;
                }
                if (!this.lastPhoneCoordinate.latitude.equals(coordinate.latitude)) {
                    break label0;
                }
                if (!this.lastPhoneCoordinate.longitude.equals(coordinate.longitude)) {
                    break label0;
                }
                if (!this.lastPhoneCoordinate.accuracy.equals(coordinate.accuracy)) {
                    break label0;
                }
                if (!this.lastPhoneCoordinate.altitude.equals(coordinate.altitude)) {
                    break label0;
                }
                if (!this.lastPhoneCoordinate.bearing.equals(coordinate.bearing)) {
                    break label0;
                }
                if (!this.lastPhoneCoordinate.speed.equals(coordinate.speed)) {
                    break label0;
                }
                sLogger.e("same location(discard) diff=" + (coordinate.timestamp - this.lastPhoneCoordinate.timestamp));
                break label1;
            }
            this.lastPhoneCoordinate = coordinate;
            this.lastPhoneCoordinateTime = android.os.SystemClock.elapsedRealtime();
            this.handler.removeCallbacks(this.noPhoneLocationFixRunnable);
            com.navdy.hud.app.debug.RouteRecorder a1 = com.navdy.hud.app.debug.RouteRecorder.getInstance();
            Location locationFromCoordinate = GpsManager.androidLocationFromCoordinate(coordinate);
            if (a1.isRecording()) {
                a1.injectLocation(locationFromCoordinate, true);
            }
            if (this.activeLocationSource != null) {
                if (this.activeLocationSource != LocationSource.PHONE) {
                    if (android.os.SystemClock.elapsedRealtime() - this.lastUbloxCoordinateTime > 3000L) {
                        sLogger.v("[Gps-loc] [LocationProvider] switched from [" + this.activeLocationSource + "] to [" + LocationSource.PHONE + "]");
                        this.activeLocationSource = LocationSource.PHONE;
                        this.stopUbloxReporting();
                        this.feedLocationToProvider(locationFromCoordinate, coordinate);
                        this.markLocationFix("Switched to Phone Gps", "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox =lost", true, false);
                    }
                } else {
                    this.feedLocationToProvider(locationFromCoordinate, coordinate);
                }
            } else {
                sLogger.v("[Gps-loc] [LocationProvider] switched from [null] to [" + LocationSource.PHONE + "]");
                this.stopUbloxReporting();
                this.activeLocationSource = LocationSource.PHONE;
                this.feedLocationToProvider(locationFromCoordinate, coordinate);
                this.markLocationFix("Switched to Phone Gps", "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox=n/a", true, false);
            }
        }
    }
    
    public void setConnectionService(HudConnectionService a) {
        this.connectionService = a;
    }
    
    public void shutdown() {
        if (this.locationManager != null) {
            this.locationManager.removeUpdates(this.ubloxGpsLocationListener);
            this.locationManager.removeUpdates(this.navdyGpsLocationListener);
            this.locationManager.removeGpsStatusListener(this.ubloxGpsStatusListener);
            this.locationManager.removeNmeaListener(this.ubloxGpsNmeaListener);
        }
    }
    
    public void startUbloxReporting() {
        try {
            android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
            android.os.UserHandle a0 = android.os.Process.myUserHandle();
            a.sendBroadcastAsUser(new android.content.Intent(START_UBLOX), a0);
            this.startUbloxCalled = true;
            sLogger.v("started ublox reporting");
        } catch(Throwable a1) {
            sLogger.e("startUbloxReporting", a1);
        }
    }
    
    public void stopUbloxReporting() {
        try {
            this.startUbloxCalled = false;
            android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
            android.os.UserHandle a0 = android.os.Process.myUserHandle();
            a.sendBroadcastAsUser(new android.content.Intent(STOP_UBLOX), a0);
            sLogger.v("stopped ublox reporting");
        } catch(Throwable a1) {
            sLogger.e("stopUbloxReporting", a1);
        }
    }

    public enum LocationSource {
        UBLOX(0),
        PHONE(1);

        private int value;
        LocationSource(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
