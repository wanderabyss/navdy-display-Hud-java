package com.navdy.hud.app.device;

public enum PowerManager$RunState {
    Unknown(0),
    Booting(1),
    Waking(2),
    Running(3);

    private int value;
    PowerManager$RunState(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final class PowerManager$RunState extends Enum {
//    final private static com.navdy.hud.app.device.PowerManager$RunState[] $VALUES;
//    final public static com.navdy.hud.app.device.PowerManager$RunState Booting;
//    final public static com.navdy.hud.app.device.PowerManager$RunState Running;
//    final public static com.navdy.hud.app.device.PowerManager$RunState Unknown;
//    final public static com.navdy.hud.app.device.PowerManager$RunState Waking;
//
//    static {
//        Unknown = new com.navdy.hud.app.device.PowerManager$RunState("Unknown", 0);
//        Booting = new com.navdy.hud.app.device.PowerManager$RunState("Booting", 1);
//        Waking = new com.navdy.hud.app.device.PowerManager$RunState("Waking", 2);
//        Running = new com.navdy.hud.app.device.PowerManager$RunState("Running", 3);
//        com.navdy.hud.app.device.PowerManager$RunState[] a = new com.navdy.hud.app.device.PowerManager$RunState[4];
//        a[0] = Unknown;
//        a[1] = Booting;
//        a[2] = Waking;
//        a[3] = Running;
//        $VALUES = a;
//    }
//
//    private PowerManager$RunState(String s, int i) {
//        super(s, i);
//    }
//
//    public static com.navdy.hud.app.device.PowerManager$RunState valueOf(String s) {
//        return Enum.valueOf(PowerManager$RunState.class, s);
//    }
//
//    public static com.navdy.hud.app.device.PowerManager$RunState[] values() {
//        return $VALUES.clone();
//    }
//}
