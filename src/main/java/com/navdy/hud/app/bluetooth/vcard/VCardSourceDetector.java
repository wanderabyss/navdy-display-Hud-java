package com.navdy.hud.app.bluetooth.vcard;

import java.util.List;
import android.util.Log;
import android.text.TextUtils;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Set;

public class VCardSourceDetector implements VCardInterpreter
{
    private static Set<String> APPLE_SIGNS;
    private static Set<String> FOMA_SIGNS;
    private static Set<String> JAPANESE_MOBILE_PHONE_SIGNS;
    private static final String LOG_TAG = "vCard";
    private static final int PARSE_TYPE_APPLE = 1;
    private static final int PARSE_TYPE_DOCOMO_FOMA = 3;
    private static final int PARSE_TYPE_MOBILE_PHONE_JP = 2;
    public static final int PARSE_TYPE_UNKNOWN = 0;
    private static final int PARSE_TYPE_WINDOWS_MOBILE_V65_JP = 4;
    private static String TYPE_FOMA_CHARSET_SIGN;
    private static Set<String> WINDOWS_MOBILE_PHONE_SIGNS;
    private int mParseType;
    private String mSpecifiedCharset;
    private int mVersion;
    
    static {
        VCardSourceDetector.APPLE_SIGNS = new HashSet<String>(Arrays.<String>asList("X-PHONETIC-FIRST-NAME", "X-PHONETIC-MIDDLE-NAME", "X-PHONETIC-LAST-NAME", "X-ABADR", "X-ABUID"));
        VCardSourceDetector.JAPANESE_MOBILE_PHONE_SIGNS = new HashSet<String>(Arrays.<String>asList("X-GNO", "X-GN", "X-REDUCTION"));
        VCardSourceDetector.WINDOWS_MOBILE_PHONE_SIGNS = new HashSet<String>(Arrays.<String>asList("X-MICROSOFT-ASST_TEL", "X-MICROSOFT-ASSISTANT", "X-MICROSOFT-OFFICELOC"));
        VCardSourceDetector.FOMA_SIGNS = new HashSet<String>(Arrays.<String>asList("X-SD-VERN", "X-SD-FORMAT_VER", "X-SD-CATEGORIES", "X-SD-CLASS", "X-SD-DCREATED", "X-SD-DESCRIPTION"));
        VCardSourceDetector.TYPE_FOMA_CHARSET_SIGN = "X-SD-CHAR_CODE";
    }
    
    public VCardSourceDetector() {
        this.mParseType = 0;
        this.mVersion = -1;
    }
    
    public String getEstimatedCharset() {
        String mSpecifiedCharset = null;
        if (TextUtils.isEmpty((CharSequence)this.mSpecifiedCharset)) {
            mSpecifiedCharset = this.mSpecifiedCharset;
        }
        else {
            switch (this.mParseType) {
                default:
                    mSpecifiedCharset = null;
                    break;
                case 2:
                case 3:
                case 4:
                    mSpecifiedCharset = "SHIFT_JIS";
                    break;
                case 1:
                    mSpecifiedCharset = "UTF-8";
                    break;
            }
        }
        return mSpecifiedCharset;
    }
    
    public int getEstimatedType() {
        int n = 0;
        switch (this.mParseType) {
            default:
                if (this.mVersion == 0) {
                    n = -1073741824;
                    break;
                }
                if (this.mVersion == 1) {
                    n = -1073741823;
                    break;
                }
                if (this.mVersion == 2) {
                    n = -1073741822;
                    break;
                }
                n = 0;
                break;
            case 3:
                n = 939524104;
                break;
            case 2:
                n = 402653192;
                break;
        }
        return n;
    }
    
    @Override
    public void onEntryEnded() {
    }
    
    @Override
    public void onEntryStarted() {
    }
    
    @Override
    public void onPropertyCreated(final VCardProperty vCardProperty) {
        final String name = vCardProperty.getName();
        final List<String> valueList = vCardProperty.getValueList();
        if (name.equalsIgnoreCase("VERSION") && valueList.size() > 0) {
            final String s = valueList.get(0);
            if (s.equals("2.1")) {
                this.mVersion = 0;
            }
            else if (s.equals("3.0")) {
                this.mVersion = 1;
            }
            else if (s.equals("4.0")) {
                this.mVersion = 2;
            }
            else {
                Log.w("vCard", "Invalid version string: " + s);
            }
        }
        else if (name.equalsIgnoreCase(VCardSourceDetector.TYPE_FOMA_CHARSET_SIGN)) {
            this.mParseType = 3;
            if (valueList.size() > 0) {
                this.mSpecifiedCharset = valueList.get(0);
            }
        }
        if (this.mParseType == 0) {
            if (VCardSourceDetector.WINDOWS_MOBILE_PHONE_SIGNS.contains(name)) {
                this.mParseType = 4;
            }
            else if (VCardSourceDetector.FOMA_SIGNS.contains(name)) {
                this.mParseType = 3;
            }
            else if (VCardSourceDetector.JAPANESE_MOBILE_PHONE_SIGNS.contains(name)) {
                this.mParseType = 2;
            }
            else if (VCardSourceDetector.APPLE_SIGNS.contains(name)) {
                this.mParseType = 1;
            }
        }
    }
    
    @Override
    public void onVCardEnded() {
    }
    
    @Override
    public void onVCardStarted() {
    }
}
