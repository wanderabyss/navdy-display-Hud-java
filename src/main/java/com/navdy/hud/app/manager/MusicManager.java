package com.navdy.hud.app.manager;

import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.events.audio.ResumeMusicRequest;
import com.navdy.service.library.events.photo.PhotoUpdate;
import com.navdy.service.library.events.callcontrol.PhoneStatus;
import com.navdy.service.library.events.callcontrol.PhoneEvent;
import com.navdy.service.library.events.audio.MusicCollectionSourceUpdate;
import com.navdy.service.library.events.audio.MusicCapability;
import java.util.HashMap;
import com.navdy.service.library.util.MusicDataUtils;
import com.navdy.hud.app.event.LocalSpeechRequest;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.service.library.events.Capabilities;
import com.squareup.wire.Wire;
import com.navdy.service.library.events.audio.MusicCapabilitiesRequest;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.ui.component.mainmenu.MusicMenu2;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.audio.MusicShuffleMode;
import com.navdy.service.library.events.audio.MusicTrackInfoRequest;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.audio.MusicEvent;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.events.photo.PhotoUpdatesRequest;
import com.navdy.service.library.events.audio.MusicArtworkRequest;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import android.support.annotation.Nullable;

import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.events.input.MediaRemoteKeyEvent;
import com.navdy.service.library.events.input.KeyEvent;
import com.navdy.service.library.events.input.MediaRemoteKey;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.events.audio.MusicArtworkResponse;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import android.os.SystemClock;
import java.util.HashSet;
import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.events.audio.MusicPlaybackState;
import java.util.concurrent.TimeUnit;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.service.library.events.audio.MusicDataSource;
import com.navdy.hud.app.service.pandora.PandoraManager;
import com.navdy.hud.app.framework.music.MusicNotification;
import com.navdy.service.library.events.audio.MusicCollectionResponse;
import com.navdy.hud.app.storage.cache.MessageCache;
import com.navdy.service.library.events.audio.MusicCollectionType;
import java.util.List;
import com.navdy.service.library.events.audio.MusicCollectionSource;
import java.util.Map;
import com.navdy.service.library.events.audio.MusicCapabilitiesResponse;
import java.util.concurrent.atomic.AtomicBoolean;
import android.os.Handler;
import android.support.annotation.NonNull;
import okio.ByteString;
import java.util.Set;
import com.squareup.otto.Bus;
import com.navdy.hud.app.util.MusicArtworkCache;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.events.audio.MusicTrackInfo;

public class MusicManager
{
    public static final MediaControl[] CONTROLS;
    private static final int DEFAULT_ARTWORK_SIZE = 200;
    public static final MusicTrackInfo EMPTY_TRACK;
    private static final long PAUSED_DUE_TO_HFP_EXPIRY_TIME;
    private static final String PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE = "PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE";
    private static final String PREFERENCE_MUSIC_CACHE_PROFILE = "PREFERENCE_MUSIC_CACHE_PROFILE";
    private static final String PREFERENCE_MUSIC_CACHE_SERIAL = "PREFERENCE_MUSIC_CACHE_SERIAL";
    private static final int PROGRESS_BAR_UPDATE_ON_ANDROID = 2000;
    private static final int THRESHOLD_TIME_BETWEEN_PAUSE_AND_POSSIBLE_CAUSE_FOR_HFP = 10000;
    private static final Logger sLogger;
    private boolean acceptingResumes;
    private MusicArtworkCache artworkCache;
    private Bus bus;
    private boolean clientSupportsArtworkCaching;
    private Set<MediaControl> currentControls;
    private ByteString currentPhoto;
    private int currentPosition;
    @NonNull
    private MusicTrackInfo currentTrack;
    private Handler handler;
    private volatile long interestingEventTime;
    private AtomicBoolean isLongPress;
    private boolean isMusicLibraryCachingEnabled;
    private volatile long lastPausedTime;
    private long lastTrackUpdateTime;
    private MusicCapabilitiesResponse musicCapabilities;
    private Map<MusicCollectionSource, List<MusicCollectionType>> musicCapabilityTypeMap;
    private MessageCache<MusicCollectionResponse> musicCollectionResponseMessageCache;
    private String musicMenuPath;
    private MusicNotification musicNotification;
    private Set<MusicUpdateListener> musicUpdateListeners;
    private final Runnable openNotificationRunnable;
    private PandoraManager pandoraManager;
    private boolean pausedByUs;
    private volatile long pausedDueToHfpDetectedTime;
    @NonNull
    private MusicDataSource previousDataSource;
    private Runnable progressBarUpdater;
    private UIStateManager uiStateManager;
    private boolean updateListeners;
    private volatile boolean wasPossiblyPausedDueToHFP;
    private static final boolean glanceArtFadeFromBlack = SystemProperties.getBoolean("persist.sys.music_glance_fade_black", true);


    static {
        sLogger = new Logger(MusicManager.class);
        CONTROLS = MediaControl.values();
        PAUSED_DUE_TO_HFP_EXPIRY_TIME = TimeUnit.MINUTES.toMillis(10L);
        EMPTY_TRACK = new MusicTrackInfo.Builder().playbackState(MusicPlaybackState.PLAYBACK_NONE).name(HudApplication.getAppContext().getString(R.string.music_init_title)).author("").album("").isPreviousAllowed(true).isNextAllowed(true).build();
    }

    public MusicManager(final MessageCache<MusicCollectionResponse> musicCollectionResponseMessageCache, final Bus bus, final Resources resources, final UIStateManager uiStateManager, final PandoraManager pandoraManager, final MusicArtworkCache artworkCache) {
        this.updateListeners = true;
        this.handler = new Handler();
        this.pausedByUs = false;
        this.acceptingResumes = false;
        this.isMusicLibraryCachingEnabled = false;
        this.previousDataSource = MusicDataSource.MUSIC_SOURCE_NONE;
        this.wasPossiblyPausedDueToHFP = false;
        this.pausedDueToHfpDetectedTime = 0L;
        this.musicUpdateListeners = new HashSet<>();
        this.musicMenuPath = null;
        this.progressBarUpdater = new Runnable() {
            @Override
            public void run() {
                if (MusicPlaybackState.PLAYBACK_PLAYING.equals(MusicManager.this.currentTrack.playbackState) && MusicManager.this.currentTrack.currentPosition != null) {
                    MusicManager.this.currentPosition = MusicManager.this.currentTrack.currentPosition + (int)(SystemClock.elapsedRealtime() - MusicManager.this.lastTrackUpdateTime);
                    MusicManager.this.musicNotification.updateProgressBar(MusicManager.this.currentPosition);
                    MusicManager.this.handler.postDelayed(MusicManager.this.progressBarUpdater, PROGRESS_BAR_UPDATE_ON_ANDROID);
                }
            }
        };
        this.openNotificationRunnable = new Runnable() {
            @Override
            public void run() {
                NotificationManager.getInstance().addNotification(MusicManager.this.musicNotification, MusicManager.this.uiStateManager.getMainScreensSet());
            }
        };
        this.isLongPress = new AtomicBoolean(false);
        this.musicCollectionResponseMessageCache = musicCollectionResponseMessageCache;
        this.bus = bus;
        this.uiStateManager = uiStateManager;
        this.pandoraManager = pandoraManager;
        this.currentTrack = MusicManager.EMPTY_TRACK;
        (this.currentControls = new HashSet<>()).add(MediaControl.PLAY);
        this.musicNotification = new MusicNotification(this, bus);
        this.artworkCache = artworkCache;
    }

    private void cacheAlbumArt(final MusicArtworkResponse musicArtworkResponse) {
        if (musicArtworkResponse.author == null && musicArtworkResponse.album == null && musicArtworkResponse.name == null) {
            MusicManager.sLogger.d("Not a now playing update, returning");
        }
        else {
            this.artworkCache.putArtwork(musicArtworkResponse.author, musicArtworkResponse.album, musicArtworkResponse.name, musicArtworkResponse.photo.toByteArray());
        }
    }

    private void cacheAlbumArt(final byte[] array, final MusicTrackInfo musicTrackInfo) {
        if (musicTrackInfo != null) {
            this.artworkCache.putArtwork(musicTrackInfo.author, musicTrackInfo.album, musicTrackInfo.name, array);
        }
    }

    private void callAlbumArtCallbacks() {
        if (this.updateListeners) {
            MusicManager.sLogger.d("callAlbumArtCallbacks");
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    MusicManager.this.handler.post(new Runnable() {
                        @Override
                        public void run() {
                            for (MusicUpdateListener musicUpdateListener : MusicManager.this.musicUpdateListeners) {
                                musicUpdateListener.onAlbumArtUpdate(MusicManager.this.currentPhoto, true);
                            }
                        }
                    });
                }
            }, 1);
        }
        else {
            MusicManager.sLogger.w("callAlbumArtCallbacks but updateListeners was false");
        }
    }

    private void callTrackUpdateCallbacks(final boolean b) {
        if (this.updateListeners) {
            MusicManager.sLogger.d("callTrackUpdateCallbacks");
            for (MusicUpdateListener musicUpdateListener : this.musicUpdateListeners) {
                musicUpdateListener.onTrackUpdated(this.currentTrack, this.currentControls, b);
            }
        }
        else {
            MusicManager.sLogger.w("callTrackUpdateCallbacks but updateListeners was false");
        }
    }

    private boolean canSeekBackward() {
        return Boolean.TRUE.equals(this.currentTrack.isPreviousAllowed) && !Boolean.TRUE.equals(this.currentTrack.isOnlineStream);
    }

    private boolean canSeekForward() {
        return Boolean.TRUE.equals(this.currentTrack.isNextAllowed) && !Boolean.TRUE.equals(this.currentTrack.isOnlineStream);
    }

    private void checkCache(final String s, final long n) {
        final SharedPreferences sharedPreferences = RemoteDeviceManager.getInstance().getSharedPreferences();
        final String profileName = DriverProfileHelper.getInstance().getCurrentProfile().getProfileName();
        final String string = sharedPreferences.getString(PREFERENCE_MUSIC_CACHE_PROFILE, null);
        final long long1 = sharedPreferences.getLong(PREFERENCE_MUSIC_CACHE_SERIAL, 0L);
        final String string2 = sharedPreferences.getString(PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE, null);
        MusicManager.sLogger.d("Current Cache details " + string + ", Source : " + string2 + ", Serial : " + long1);
        MusicManager.sLogger.d("Current Profile details " + profileName + ", Source : " + s + ", Serial : " + n);
        if (!TextUtils.equals(profileName, string) || long1 != n || !TextUtils.equals(s, string2)) {
            MusicManager.sLogger.d("Clearing the Music data cache");
            this.musicCollectionResponseMessageCache.clear();
            sharedPreferences.edit().putString("PREFERENCE_MUSIC_CACHE_PROFILE", profileName).putLong("PREFERENCE_MUSIC_CACHE_SERIAL", n).putString("PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE", s).apply();
        }
    }

    private MusicDataSource dataSourceToUse() {
        final MusicTrackInfo currentTrack = this.currentTrack;
        MusicDataSource musicDataSource;
        if (currentTrack.dataSource != null && !MusicDataSource.MUSIC_SOURCE_NONE.equals(currentTrack.dataSource)) {
            musicDataSource = currentTrack.dataSource;
        }
        else {
            musicDataSource = this.previousDataSource;
        }
        return musicDataSource;
    }

    private void postKeyDownUp(final MediaRemoteKey mediaRemoteKey) {
        this.bus.post(new RemoteEvent(new MediaRemoteKeyEvent(mediaRemoteKey, KeyEvent.KEY_DOWN)));
        this.handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                MusicManager.this.bus.post(new RemoteEvent(new MediaRemoteKeyEvent(mediaRemoteKey, KeyEvent.KEY_UP)));
            }
        }, 100L);
    }

    private void recordAnalytics(@Nullable final android.view.KeyEvent keyEvent, final MediaControl mediaControl, final boolean b) {
        Label_0036: {
            if (keyEvent == null) {
                switch (mediaControl) {
                    case PLAY:
                        AnalyticsSupport.recordMusicAction("Play");
                        break;
                    case PAUSE:
                        AnalyticsSupport.recordMusicAction("Pause");
                        break;
                }
            }
            else {
                switch (keyEvent.getAction()) {
                    case 0:
                        if (!keyEvent.isLongPress() || this.isLongPress.get()) {
                            break;
                        }
                        switch (mediaControl) {
                            default:
                                break Label_0036;
                            case NEXT:
                                AnalyticsSupport.recordMusicAction("Fast-forward");
                                break Label_0036;
                            case PREVIOUS:
                                AnalyticsSupport.recordMusicAction("Rewind");
                                break Label_0036;
                        }
//                        break;
                    case 1:
                        if (!b || this.isLongPress.get()) {
                            break;
                        }
                        switch (mediaControl) {
                            default:
                                break Label_0036;
                            case NEXT:
                                AnalyticsSupport.recordMusicAction("Next");
                                break Label_0036;
                            case PREVIOUS:
                                AnalyticsSupport.recordMusicAction("Previous");
                        }
//                        break;
                }
            }
        }
    }

    private void requestArtwork(final MusicTrackInfo musicTrackInfo) {
        MusicManager.sLogger.d("requestArtwork: " + musicTrackInfo);
        if (TextUtils.isEmpty(musicTrackInfo.author) && TextUtils.isEmpty(musicTrackInfo.album) && TextUtils.isEmpty(musicTrackInfo.name)) {
            this.currentPhoto = null;
            this.callAlbumArtCallbacks();
            MusicManager.sLogger.d("Empty track, returning");
        }
        else {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    MusicManager.this.artworkCache.getArtwork(musicTrackInfo.author, musicTrackInfo.album, musicTrackInfo.name, new MusicArtworkCache.Callback() {
                        @Override
                        public void onHit(@NonNull final byte[] array) {
                            MusicManager.sLogger.d("CACHE HIT");
                            MusicManager.this.currentPhoto = ByteString.of(array);
                            MusicManager.this.callAlbumArtCallbacks();
                        }

                        @Override
                        public void onMiss() {
                            MusicManager.sLogger.d("CACHE MISS");
                            final MusicArtworkRequest build = new MusicArtworkRequest.Builder().name(musicTrackInfo.name).album(musicTrackInfo.album).author(musicTrackInfo.author).size(DEFAULT_ARTWORK_SIZE).build();
                            MusicManager.sLogger.d("requesting artwork: " + build);
                            MusicManager.this.bus.post(new RemoteEvent(build));
                        }
                    });
                }
            }, 22);
        }
    }

    private void requestPhotoUpdates(final boolean b) {
        if (b && this.clientSupportsArtworkCaching) {
            MusicManager.sLogger.d("client supports caching, no need to request photo updates");
        }
        else {
            this.bus.post(new RemoteEvent(new PhotoUpdatesRequest.Builder().start(b).photoType(PhotoType.PHOTO_ALBUM_ART).build()));
        }
    }

    private void runAndroidAction(final MusicEvent.Action action) {
        final MusicDataSource dataSourceToUse = this.dataSourceToUse();
        if (MusicEvent.Action.MUSIC_ACTION_PLAY.equals(action) && MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(dataSourceToUse)) {
            this.pandoraManager.startAndPlay();
        }
        else {
            MusicManager.sLogger.d("Media action: " + action);
            this.bus.post(new RemoteEvent(new MusicEvent.Builder().action(action).dataSource(dataSourceToUse).build()));
        }
    }

    private void sendMediaKeyEvent(final MediaControl mediaControl, final boolean b) {
        final MediaRemoteKey mediaRemoteKey = null;
        MediaRemoteKey mediaRemoteKey2;
        switch (mediaControl) {
            default:
                mediaRemoteKey2 = mediaRemoteKey;
                break;
            case PLAY:
            case PAUSE:
                mediaRemoteKey2 = MediaRemoteKey.MEDIA_REMOTE_KEY_PLAY;
                break;
            case NEXT:
                mediaRemoteKey2 = MediaRemoteKey.MEDIA_REMOTE_KEY_NEXT;
                break;
            case PREVIOUS:
                mediaRemoteKey2 = MediaRemoteKey.MEDIA_REMOTE_KEY_PREV;
                break;
        }
        final Bus bus = this.bus;
        final MediaRemoteKeyEvent.Builder key = new MediaRemoteKeyEvent.Builder().key(mediaRemoteKey2);
        KeyEvent keyEvent;
        if (b) {
            keyEvent = KeyEvent.KEY_DOWN;
        }
        else {
            keyEvent = KeyEvent.KEY_UP;
        }
        bus.post(new RemoteEvent(key.action(keyEvent).build()));
    }

    public static boolean tryingToPlay(final MusicPlaybackState musicPlaybackState) {
        return !MusicPlaybackState.PLAYBACK_NONE.equals(musicPlaybackState) && !MusicPlaybackState.PLAYBACK_CONNECTING.equals(musicPlaybackState) && !MusicPlaybackState.PLAYBACK_ERROR.equals(musicPlaybackState) && !MusicPlaybackState.PLAYBACK_PAUSED.equals(musicPlaybackState) && !MusicPlaybackState.PLAYBACK_STOPPED.equals(musicPlaybackState);
    }

    private void updateControls(final MusicTrackInfo musicTrackInfo) {
        this.currentControls = new HashSet<>();
        if (Boolean.TRUE.equals(musicTrackInfo.isPreviousAllowed)) {
            this.currentControls.add(MediaControl.PREVIOUS);
        }
        if (tryingToPlay(musicTrackInfo.playbackState)) {
            this.currentControls.add(MediaControl.PAUSE);
        }
        else {
            this.currentControls.add(MediaControl.PLAY);
        }
        if (Boolean.TRUE.equals(musicTrackInfo.isNextAllowed)) {
            this.currentControls.add(MediaControl.NEXT);
        }
    }

    public void acceptResumes() {
        this.acceptingResumes = true;
        MusicManager.sLogger.d("Now accepting music resume events");
    }

    public void addMusicUpdateListener(@NonNull final MusicUpdateListener musicUpdateListener) {
        MusicManager.sLogger.d("addMusicUpdateListener");
        if (!this.musicUpdateListeners.contains(musicUpdateListener)) {
            if (this.musicUpdateListeners.size() == 0) {
                MusicManager.sLogger.d("Enabling album art updates (PhotoUpdate)");
                this.requestPhotoUpdates(true);
            }
            this.musicUpdateListeners.add(musicUpdateListener);
        }
        else {
            MusicManager.sLogger.w("Tried to add a listener that's already listening");
        }
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                MusicManager.this.handler.post(new Runnable() {
                    @Override
                    public void run() {
                        musicUpdateListener.onAlbumArtUpdate(MusicManager.this.currentPhoto, false);
                        musicUpdateListener.onTrackUpdated(MusicManager.this.getCurrentTrack(), MusicManager.this.getCurrentControls(), false);
                    }
                });
                MusicManager.sLogger.v("album art listeners:");
                for (MusicUpdateListener musicUpdateListener1 : MusicManager.this.musicUpdateListeners) {
                    MusicManager.sLogger.d("- " + musicUpdateListener1);
                }
            }
        }, 1);
    }

    public void executeMediaControl(final MediaControl mediaControl, final boolean b) {
        final DeviceInfo.Platform remoteDevicePlatform = RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
        Label_0011: {
            if (remoteDevicePlatform != null) {
                this.recordAnalytics(null, mediaControl, b);
                switch (remoteDevicePlatform) {
                    case PLATFORM_Android:
                        switch (mediaControl) {
                            default:
                                break Label_0011;
                            case PLAY:
                                this.runAndroidAction(MusicEvent.Action.MUSIC_ACTION_PLAY);
                                break Label_0011;
                            case PAUSE:
                                this.runAndroidAction(MusicEvent.Action.MUSIC_ACTION_PAUSE);
                                break Label_0011;
                        }
//                        break;
                    case PLATFORM_iOS:
                        this.sendMediaKeyEvent(mediaControl, true);
                        this.sendMediaKeyEvent(mediaControl, false);
                        break;
                }
            }
        }
    }

    public List<MusicCollectionType> getCollectionTypesForSource(final MusicCollectionSource musicCollectionSource) {
        return this.musicCapabilityTypeMap.get(musicCollectionSource);
    }

    public Set<MediaControl> getCurrentControls() {
        return this.currentControls;
    }

    public int getCurrentPosition() {
        return this.currentPosition;
    }

    @NonNull
    public MusicTrackInfo getCurrentTrack() {
        return this.currentTrack;
    }

    public MusicCapabilitiesResponse getMusicCapabilities() {
        return this.musicCapabilities;
    }

    public String getMusicMenuPath() {
        return this.musicMenuPath;
    }

    public void handleKeyEvent(final android.view.KeyEvent keyEvent, final MediaControl mediaControl, final boolean b) {
        final DeviceInfo.Platform remoteDevicePlatform = RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
        Label_0023: {
            if (remoteDevicePlatform != null && InputManager.isCenterKey(keyEvent.getKeyCode())) {
                this.recordAnalytics(keyEvent, mediaControl, b);
                switch (remoteDevicePlatform) {
                    case PLATFORM_Android:
                        switch (keyEvent.getAction()) {
                            default:
                                break Label_0023;
                            case 0:
                                if (!keyEvent.isLongPress() || !this.isLongPress.compareAndSet(false, true)) {
                                    break Label_0023;
                                }
                                switch (mediaControl) {
                                    default:
                                        break Label_0023;
                                    case NEXT:
                                        if (this.canSeekBackward()) {
                                            this.runAndroidAction(MusicEvent.Action.MUSIC_ACTION_FAST_FORWARD_START);
                                            break Label_0023;
                                        }
                                        MusicManager.sLogger.w("Fast-forward is not allowed");
                                        break Label_0023;
                                    case PREVIOUS:
                                        if (this.canSeekForward()) {
                                            this.runAndroidAction(MusicEvent.Action.MUSIC_ACTION_REWIND_START);
                                            break Label_0023;
                                        }
                                        MusicManager.sLogger.w("Rewind is not allowed");
                                        break Label_0023;
                                }
//                                break;
                            case 1:
                                if (!b) {
                                    break Label_0023;
                                }
                                if (this.isLongPress.compareAndSet(true, false)) {
                                    switch (mediaControl) {
                                        default:
                                            break Label_0023;
                                        case NEXT:
                                            this.runAndroidAction(MusicEvent.Action.MUSIC_ACTION_FAST_FORWARD_STOP);
                                            break Label_0023;
                                        case PREVIOUS:
                                            this.runAndroidAction(MusicEvent.Action.MUSIC_ACTION_REWIND_STOP);
                                            break Label_0023;
                                    }
                                }
                                else {
                                    switch (mediaControl) {
                                        default:
                                            break Label_0023;
                                        case NEXT:
                                            if (Boolean.TRUE.equals(this.currentTrack.isNextAllowed)) {
                                                this.runAndroidAction(MusicEvent.Action.MUSIC_ACTION_NEXT);
                                                break Label_0023;
                                            }
                                            MusicManager.sLogger.w("Next song action is not allowed");
                                            break Label_0023;
                                        case PREVIOUS:
                                            if (Boolean.TRUE.equals(this.currentTrack.isPreviousAllowed)) {
                                                this.runAndroidAction(MusicEvent.Action.MUSIC_ACTION_PREVIOUS);
                                                break Label_0023;
                                            }
                                            MusicManager.sLogger.w("Previous song action is not allowed");
                                            break Label_0023;
                                    }
                                }
//                                break;
                        }
//                        break;
                    case PLATFORM_iOS:
                        switch (keyEvent.getAction()) {
                            default:
                                break Label_0023;
                            case 0:
                                if (b) {
                                    break Label_0023;
                                }
                                if (mediaControl == MediaControl.PLAY || mediaControl == MediaControl.PAUSE) {
                                    this.sendMediaKeyEvent(mediaControl, true);
                                    this.sendMediaKeyEvent(mediaControl, false);
                                    break Label_0023;
                                }
                                this.sendMediaKeyEvent(mediaControl, true);
                                break Label_0023;
                            case 1:
                                if (b) {
                                    this.sendMediaKeyEvent(mediaControl, false);
                                }
                        }
//                        break;
                }
            }
        }
    }

    public boolean hasMusicCapabilities() {
        return this.musicCapabilities != null && this.musicCapabilities.capabilities != null && this.musicCapabilities.capabilities.size() != 0;
    }

    public void initialize() {
        this.bus.post(new RemoteEvent(new MusicTrackInfoRequest()));
        MusicManager.sLogger.d("Request for music track info sent to the client.");
    }

    public boolean isMusicLibraryCachingEnabled() {
        return this.isMusicLibraryCachingEnabled;
    }

    public boolean isPandoraDataSource() {
        return MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(this.currentTrack.dataSource);
    }

    public boolean isShuffling() {
        return this.currentTrack.shuffleMode != null && this.currentTrack.shuffleMode != MusicShuffleMode.MUSIC_SHUFFLE_MODE_UNKNOWN && this.currentTrack.shuffleMode != MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF;
    }

    @Subscribe
    public void onConnectionStateChange(final ConnectionStateChange connectionStateChange) {
        if (connectionStateChange.state == ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED) {
            NotificationManager.getInstance().removeNotification(this.musicNotification.getId());
            this.currentTrack = MusicManager.EMPTY_TRACK;
            this.currentPhoto = null;
            this.currentControls = null;
            this.updateListeners = true;
            this.callAlbumArtCallbacks();
            this.callTrackUpdateCallbacks(false);
            this.musicCapabilities = null;
            this.musicCapabilityTypeMap = null;
            this.musicMenuPath = null;
            MusicMenu2.clearMenuData();
        }
    }

    @Subscribe
    public void onDeviceInfo(final DeviceInfo deviceInfo) {
        MusicManager.sLogger.d("onDeviceInfo - " + deviceInfo);
        this.bus.post(new RemoteEvent(new MusicCapabilitiesRequest.Builder().build()));
        final Capabilities capabilities = deviceInfo.capabilities;
        if (deviceInfo.platform == DeviceInfo.Platform.PLATFORM_iOS || (capabilities != null && Wire.<Boolean>get(capabilities.musicArtworkCache, false))) {
            this.clientSupportsArtworkCaching = true;
        }
    }

    @Subscribe
    public void onDriverProfileChanged(final DriverProfileChanged driverProfileChanged) {
        MusicManager.sLogger.d("onDriverProfileChanged - " + driverProfileChanged);
        if (!DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile().isDefaultProfile() && this.musicUpdateListeners.size() > 0) {
            this.requestPhotoUpdates(true);
        }
    }

    @Subscribe
    public void onLocalSpeechRequest(final LocalSpeechRequest localSpeechRequest) {
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        if (!this.wasPossiblyPausedDueToHFP) {
            MusicManager.sLogger.d("Recording the speech request as an interesting event");
            this.interestingEventTime = elapsedRealtime;
        }
    }

    @Subscribe
    public void onMediaKeyEventFromClient(final MediaRemoteKeyEvent mediaRemoteKeyEvent) {
        this.bus.post(new RemoteEvent(mediaRemoteKeyEvent));
    }

    @Subscribe
    public void onMusicArtworkResponse(final MusicArtworkResponse musicArtworkResponse) {
        MusicManager.sLogger.d("onMusicArtworkResponse " + musicArtworkResponse);
        if (musicArtworkResponse != null && musicArtworkResponse.photo != null) {
            this.cacheAlbumArt(musicArtworkResponse);
            if (TextUtils.equals(MusicDataUtils.songIdentifierFromTrackInfo(this.currentTrack), MusicDataUtils.songIdentifierFromArtworkResponse(musicArtworkResponse))) {
                this.currentPhoto = musicArtworkResponse.photo;
            }
        }
        else {
            this.currentPhoto = null;
        }
        this.callAlbumArtCallbacks();
    }

    @Subscribe
    public void onMusicCapabilitiesResponse(MusicCapabilitiesResponse musicCapabilitiesResponse) {
        sLogger.d("onMusicCapabilitiesResponse " + musicCapabilitiesResponse);
        this.musicCapabilities = musicCapabilitiesResponse;
        String musicCollectionSource = null;
        this.isMusicLibraryCachingEnabled = false;
        long serialNumber = -1;
        if (hasMusicCapabilities()) {
            this.musicCapabilityTypeMap = new HashMap<>();
            for (MusicCapability capability : this.musicCapabilities.capabilities) {
                this.musicCapabilityTypeMap.put(capability.collectionSource, capability.collectionTypes);
                if (!(this.isMusicLibraryCachingEnabled || capability.serial_number == null)) {
                    this.isMusicLibraryCachingEnabled = true;
                    musicCollectionSource = capability.collectionSource.name();
                    serialNumber = capability.serial_number;
                    sLogger.d("Enabling caching with Source " + musicCollectionSource + ", Serial :" + serialNumber);
                }
            }
        }
        if (this.isMusicLibraryCachingEnabled) {
            checkCache(musicCollectionSource, serialNumber);
        } else {
            sLogger.d("Cache is not enabled");
        }
    }

    @Subscribe
    public void onMusicCollectionSourceUpdate(final MusicCollectionSourceUpdate musicCollectionSourceUpdate) {
        MusicManager.sLogger.d("onMusicCollectionSourceUpdate " + musicCollectionSourceUpdate);
        if (musicCollectionSourceUpdate.collectionSource != null && musicCollectionSourceUpdate.serial_number != null) {
            this.checkCache(musicCollectionSourceUpdate.collectionSource.name(), musicCollectionSourceUpdate.serial_number);
        }
    }

    @Subscribe
    public void onPhoneEvent(final PhoneEvent phoneEvent) {
        if (phoneEvent != null && phoneEvent.status != null) {
            MusicManager.sLogger.d("PhoneEvent : new status " + phoneEvent.status);
            if (phoneEvent.status == PhoneStatus.PHONE_DIALING || phoneEvent.status == PhoneStatus.PHONE_RINGING || phoneEvent.status == PhoneStatus.PHONE_OFFHOOK) {
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                if (!this.wasPossiblyPausedDueToHFP) {
                    if (this.currentTrack.playbackState.equals(MusicPlaybackState.PLAYBACK_PAUSED) && this.lastPausedTime - elapsedRealtime < THRESHOLD_TIME_BETWEEN_PAUSE_AND_POSSIBLE_CAUSE_FOR_HFP) {
                        MusicManager.sLogger.d("Current state is paused recently, possibly due to call");
                        this.wasPossiblyPausedDueToHFP = true;
                        this.pausedDueToHfpDetectedTime = elapsedRealtime;
                    }
                    else {
                        MusicManager.sLogger.d("Recording the phone event as interesting event");
                        this.interestingEventTime = elapsedRealtime;
                    }
                }
            }
        }
    }

    @Subscribe
    public void onPhotoUpdate(final PhotoUpdate photoUpdate) {
        MusicManager.sLogger.d("onPhotoUpdate " + photoUpdate);
        if (photoUpdate != null && photoUpdate.photo != null) {
            this.currentPhoto = photoUpdate.photo;
            final String songIdentifierFromTrackInfo = MusicDataUtils.songIdentifierFromTrackInfo(this.currentTrack);
            if (TextUtils.equals(songIdentifierFromTrackInfo, photoUpdate.identifier)) {
                this.cacheAlbumArt(this.currentPhoto.toByteArray(), this.currentTrack);
            }
            else {
                MusicManager.sLogger.w("Received PhotoUpdate with wrong identifier: " + songIdentifierFromTrackInfo + " != " + photoUpdate.identifier);
            }
        }
        else {
            this.currentPhoto = null;
        }
        this.callAlbumArtCallbacks();
    }

    @Subscribe
    public void onResumeMusicRequest(final ResumeMusicRequest resumeMusicRequest) {
        this.softResume();
    }

    @Subscribe
    public void onTrackInfo(final MusicTrackInfo currentTrack) {
        MusicManager.sLogger.d("updateState " + currentTrack);
        final boolean is_playing = MusicPlaybackState.PLAYBACK_PLAYING.equals(currentTrack.playbackState);
        final boolean was_playing = MusicPlaybackState.PLAYBACK_PLAYING.equals(this.currentTrack.playbackState);
        if (currentTrack.collectionSource == MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN) {
            this.setMusicMenuPath(null);
        }
        if (!is_playing && was_playing && !MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(this.currentTrack.dataSource) && MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(currentTrack.dataSource)) {
            MusicManager.sLogger.d("Ignoring update from non playing Pandora");
        }
        else {
            boolean new_track;
            new_track = !TextUtils.equals(MusicDataUtils.songIdentifierFromTrackInfo(currentTrack), MusicDataUtils.songIdentifierFromTrackInfo(this.currentTrack));
            final boolean playing_new_track = is_playing && new_track;
            boolean new_album = true;
            try {
                new_album = !currentTrack.album.equals(this.currentTrack.album);
            } catch(java.lang.RuntimeException ex) { }
            if (playing_new_track && new_album && glanceArtFadeFromBlack) {
                // clear the notification artwork ahead of new one coming in
                this.musicNotification.clearImage();
            }
            if (new_track && this.clientSupportsArtworkCaching) {
                this.requestArtwork(currentTrack);
            }
            final boolean do_playing = is_playing && !was_playing;
            final boolean is_paused = MusicPlaybackState.PLAYBACK_PAUSED.equals(currentTrack.playbackState);
            final boolean was_paused = MusicPlaybackState.PLAYBACK_PAUSED.equals(this.currentTrack.playbackState);
            boolean do_paused = is_paused && !was_paused;
            if ((do_playing || do_paused) && DeviceInfo.Platform.PLATFORM_iOS.equals(RemoteDeviceManager.getInstance().getRemoteDevicePlatform())) {
                this.bus.post(new RemoteEvent(currentTrack));
            }
            if (do_playing) {
                MusicManager.sLogger.d("Music started playing");
            }
            if (do_playing && this.pausedByUs) {
                MusicManager.sLogger.d("User started playback after we paused - setting pauseByUs = false");
                this.pausedByUs = false;
            }
            if (do_paused) {
                MusicManager.sLogger.d("New state is paused");
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                if (!this.wasPossiblyPausedDueToHFP) {
                    if (elapsedRealtime - this.interestingEventTime < 10000L) {
                        MusicManager.sLogger.d("Interesting event happened just before the song was paused, possibly playing through HFP");
                        this.wasPossiblyPausedDueToHFP = true;
                        this.pausedDueToHfpDetectedTime = elapsedRealtime;
                    }
                    else {
                        this.lastPausedTime = elapsedRealtime;
                    }
                }
            }
            this.lastTrackUpdateTime = SystemClock.elapsedRealtime();
            boolean suppress_notif = do_playing && this.wasPossiblyPausedDueToHFP && this.lastTrackUpdateTime - this.pausedDueToHfpDetectedTime < MusicManager.PAUSED_DUE_TO_HFP_EXPIRY_TIME;
            if (suppress_notif) {
                MusicManager.sLogger.d("Should suppress Notification");
            }
            if (suppress_notif || this.lastTrackUpdateTime - this.pausedDueToHfpDetectedTime > MusicManager.PAUSED_DUE_TO_HFP_EXPIRY_TIME) {
                this.wasPossiblyPausedDueToHFP = false;
                this.pausedDueToHfpDetectedTime = 0L;
            }
            if (currentTrack.dataSource != null && !MusicDataSource.MUSIC_SOURCE_NONE.equals(currentTrack.dataSource)) {
                this.previousDataSource = currentTrack.dataSource;
            }
            this.updateControls(currentTrack);
            boolean b5 = currentTrack.duration != null && currentTrack.duration > 0 && currentTrack.currentPosition != null;
            if (b5) {
                this.currentPosition = currentTrack.currentPosition;
                this.musicNotification.updateProgressBar(this.currentPosition);
            }
            if (DeviceInfo.Platform.PLATFORM_Android.equals(RemoteDeviceManager.getInstance().getRemoteDevicePlatform())) {
                this.handler.removeCallbacks(this.progressBarUpdater);
                if (MusicPlaybackState.PLAYBACK_PLAYING.equals(currentTrack.playbackState) && b5) {
                    this.handler.postDelayed(this.progressBarUpdater, 2000L);
                }
            }
            int n2;
            if (MusicPlaybackState.PLAYBACK_PAUSED.equals(this.currentTrack.playbackState) || MusicPlaybackState.PLAYBACK_NONE.equals(this.currentTrack.playbackState)) {
                n2 = 1;
            }
            else {
                n2 = 0;
            }
            int n3;
            if (do_paused && !new_track) {
                n3 = 1;
            }
            else {
                n3 = 0;
            }
            int n4;
            if (b5 && currentTrack.currentPosition.equals(currentTrack.duration)) {
                n4 = 1;
            }
            else {
                n4 = 0;
            }
            int n5;
            if (is_paused && n4 == 0 && (n3 != 0 || (was_paused && new_track) || n2 != 0)) {
                n5 = 1;
            }
            else {
                n5 = 0;
            }
            int n6;
            if (this.currentTrack.shuffleMode != currentTrack.shuffleMode) {
                n6 = 1;
            }
            else {
                n6 = 0;
            }
            this.updateListeners = (is_playing || n5 != 0 || n6 != 0);
            if (this.updateListeners) {
                this.currentTrack = currentTrack;
                boolean b6 = playing_new_track || do_playing;
                final boolean showGlance = !suppress_notif && GlanceHelper.isMusicNotificationEnabled() && b6;
                if (b6 || n5 != 0 || n6 != 0) {
                    this.callTrackUpdateCallbacks(showGlance);
                    if (!this.clientSupportsArtworkCaching) {
                        this.callAlbumArtCallbacks();
                    }
                }
                if (showGlance) {
                    MusicManager.sLogger.d("Showing notification New Song ? : " + playing_new_track + ", New state playing : " + do_playing);
                    this.handler.removeCallbacks(this.openNotificationRunnable);
                    this.handler.post(this.openNotificationRunnable);
                }
            }
            else {
                MusicManager.sLogger.d("Ignoring updates");
            }
        }
    }

    public void removeMusicUpdateListener(@NonNull final MusicUpdateListener musicUpdateListener) {
        MusicManager.sLogger.d("removeMusicUpdateListener");
        if (this.musicUpdateListeners.contains(musicUpdateListener)) {
            this.musicUpdateListeners.remove(musicUpdateListener);
            if (this.musicUpdateListeners.size() == 0) {
                MusicManager.sLogger.d("Disabling album art updates");
                this.requestPhotoUpdates(false);
            }
        }
        else {
            MusicManager.sLogger.w("Tried to remove a non-existent listener");
        }
        MusicManager.sLogger.d("album art listeners:");
        for (MusicUpdateListener musicUpdateListener1 : this.musicUpdateListeners) {
            MusicManager.sLogger.v("- " + musicUpdateListener1);
        }
    }

    public void setMusicMenuPath(final String musicMenuPath) {
        MusicManager.sLogger.d("setMusicMenuPath: " + musicMenuPath);
        this.musicMenuPath = musicMenuPath;
    }

    public void showMusicNotification() {
        NotificationManager.getInstance().addNotification(this.musicNotification);
    }

    public void softPause() {
        if (this.currentTrack.playbackState == MusicPlaybackState.PLAYBACK_PLAYING) {
            MusicManager.sLogger.d("Pausing music");
            this.pausedByUs = true;
            this.acceptingResumes = false;
            this.postKeyDownUp(MediaRemoteKey.MEDIA_REMOTE_KEY_PLAY);
        }
    }

    protected void softResume() {
        if (this.pausedByUs && this.acceptingResumes) {
            this.postKeyDownUp(MediaRemoteKey.MEDIA_REMOTE_KEY_PLAY);
            this.pausedByUs = false;
            MusicManager.sLogger.d("Unpausing music (resuming)");
        }
    }

    public enum MediaControl {
        PLAY(0),
        PAUSE(1),
        NEXT(2),
        PREVIOUS(3),
        MUSIC_MENU(4),
        MUSIC_MENU_DEEP(5);

        private int value;
        MediaControl(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    public interface MusicUpdateListener
    {
        void onAlbumArtUpdate(@Nullable final ByteString p0, final boolean p1);

        void onTrackUpdated(final MusicTrackInfo p0, final Set<MediaControl> p1, final boolean p2);
    }
}
