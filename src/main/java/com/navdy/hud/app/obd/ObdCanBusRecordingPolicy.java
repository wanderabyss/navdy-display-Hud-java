package com.navdy.hud.app.obd;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.RemoteException;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.device.gps.GpsConstants;
import com.navdy.hud.app.event.DrivingStateChange;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.service.ObdCANBusDataUploadService;
import com.navdy.hud.app.util.CrashReportService$FilesModifiedTimeComparator;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.obd.ECU;
import com.navdy.obd.ICarService;
import com.navdy.obd.Pid;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.MusicDataUtils;
import com.squareup.otto.Subscribe;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ObdCanBusRecordingPolicy {
    public static final String FILES_TO_UPLOAD_DIRECTORY = "/sdcard/.canBusLogs/upload";
    public static final String META_DATA_FILE = "/sdcard/.canBusLogs/upload/meta_data_";
    private static final long CAN_BUS_MONITORING_DISTANCE_LIMIT_METERS = 40234;
    public static final String CAN_PROTOCOL_PREFIX = "ISO 15765-4";
    public static final int MAX_META_DATA_FILES = 10;
    public static final int MAX_UPLOAD_FILES = 5;
    public static final int MINIMUM_TIME_FOR_MOTION_DETECTION = 10000;
    public static final int MINIMUM_TIME_FOR_STOP_DETECTION = 10000;
    private static final float MOVING_SPEED_METERS_PER_SECOND = 1.34112f;
    public static final String PREFERENCE_NAVDY_MILES_WHEN_LISTENING_STARTED = "preference_navdy_miles_when_listening_started";
    public static final String PREF_CAN_BUS_DATA_RECORDED_AND_SENT = "canBusDataRecordedAndSent";
    public static final String PREF_CAN_BUS_DATA_SENT_VERSION = "canBusDataSentVersion";
    public static final String PREF_CAN_BUS_DATA_STATUS_REPORTED_VERSION = "canBusDataStatusReportedVersion";
    private static final File UPLOAD_DIRECTORY_FILE = new File(FILES_TO_UPLOAD_DIRECTORY);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS");
    private static PriorityBlockingQueue<File> metaDataFiles = new PriorityBlockingQueue(10, new CrashReportService$FilesModifiedTimeComparator());
    private static final Logger sLogger = new Logger(ObdCanBusRecordingPolicy.class);
    private static PriorityBlockingQueue<File> uploadFiles = new PriorityBlockingQueue(10, new CrashReportService$FilesModifiedTimeComparator());
    private int canBusMonitoringDataSize;
    private CanBusMonitoringState canBusMonitoringState;
    private Context context;
    private File currentMetaDataFile;
    private long firstTimeMovementDetected;
    private long firstTimeStoppingDetected;
    private Handler handler;
    private boolean isCanBusMonitoringLimitReached;
    private boolean isCanProtocol;
    private boolean isEngineeringBuild;
    private boolean isInstantaneousModeOn;
    private boolean isObdConnected;
    int lastSpeed;
    private String make;
    private String model;
    private boolean motionDetected;
    private ObdManager obdManager;
    private int requiredObdDataVersion;
    private SharedPreferences sharedPreferences;
    private TripManager tripManager;
    private String vin;
    private String year;

    public enum CanBusMonitoringState {
        UNKNOWN(0),
        SUCCESS(1),
        FAILURE(2);

        private int value;
        CanBusMonitoringState(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    public ObdCanBusRecordingPolicy(Context context, SharedPreferences sharedPreferences, ObdManager obdManager, TripManager tripManager) {
        this.isEngineeringBuild = !DeviceUtil.isUserBuild();
        this.firstTimeMovementDetected = 0;
        this.firstTimeStoppingDetected = 0;
        this.isObdConnected = false;
        this.isCanProtocol = false;
        this.isInstantaneousModeOn = false;
        this.motionDetected = false;
        this.lastSpeed = -1;
        this.isCanBusMonitoringLimitReached = false;
        this.canBusMonitoringState = CanBusMonitoringState.UNKNOWN;
        this.canBusMonitoringDataSize = 0;
        this.obdManager = obdManager;
        this.context = context;
        this.tripManager = tripManager;
        this.sharedPreferences = sharedPreferences;
        this.requiredObdDataVersion = context.getResources().getInteger(R.integer.obd_data_version);
        if (!UPLOAD_DIRECTORY_FILE.exists()) {
            UPLOAD_DIRECTORY_FILE.mkdirs();
        }
        populateFilesQueue();
        this.handler = new Handler();
    }

    public synchronized void onObdConnectionStateChanged(boolean connected) {
        Throwable th;
        sLogger.d("onObdConnectionStateChanged , Connected : " + connected);
        this.isObdConnected = connected;
        if (connected) {
            String protocol = this.obdManager.getProtocol();
            if (TextUtils.isEmpty(protocol) || !protocol.startsWith(CAN_PROTOCOL_PREFIX)) {
                this.isCanProtocol = false;
            } else {
                this.isCanProtocol = true;
            }
        } else {
            this.isCanProtocol = false;
        }
        this.canBusMonitoringState = CanBusMonitoringState.UNKNOWN;
        this.canBusMonitoringDataSize = 0;
        if (connected) {
            long time = System.currentTimeMillis();
            if (metaDataFiles.size() > 10) {
                File oldestFile = (File) metaDataFiles.poll();
                if (oldestFile != null) {
                    sLogger.d("Deleting file " + oldestFile.getName() + ", As its old");
                    IOUtils.deleteFile(HudApplication.getAppContext(), oldestFile.getAbsolutePath());
                }
            }
            this.currentMetaDataFile = getMetaDataFile();
            FileWriter fw = null;
            BufferedWriter bw = null;
            JSONObject jsonObject = new JSONObject();
            try {
                FileWriter fw2 = new FileWriter(this.currentMetaDataFile, false);
                try {
                    bw = new BufferedWriter(fw2);
                    ICarService carService = this.obdManager.getCarService();
                    this.vin = this.obdManager.getVin();
                    try {
                        jsonObject.accumulate(GpsConstants.GPS_EVENT_TIME, dateFormat.format(new Date(time)));
                        jsonObject.accumulate("vin", this.vin);
                        if (carService != null) {
                            try {
                                jsonObject.accumulate("protocol", carService.getProtocol());
                                List<ECU> ecus = carService.getEcus();
                                JSONArray jsonArray = new JSONArray();
                                if (ecus != null) {
                                    for (int i = 0; i < ecus.size(); i++) {
                                        JSONObject ecuObject = new JSONObject();
                                        ECU ecu = (ECU) ecus.get(i);
                                        ecuObject.accumulate("Address ", Integer.valueOf(ecu.address));
                                        List<Pid> pids = ecu.supportedPids.asList();
                                        JSONArray pidsArray = new JSONArray();
                                        if (pids != null) {
                                            for (Pid pid : pids) {
                                                pidsArray.put(pid.getId());
                                            }
                                        }
                                        ecuObject.accumulate("PIDs", pidsArray);
                                        jsonArray.put(ecuObject);
                                    }
                                    jsonObject.accumulate("ecus", jsonArray);
                                }
                            } catch (RemoteException e2) {
                                sLogger.d("Error get the data from obd service");
                            }
                        }
                        if (!TextUtils.isEmpty(this.make)) {
                            jsonObject.accumulate("Make", this.make);
                        }
                        if (!TextUtils.isEmpty(this.model)) {
                            jsonObject.accumulate("Model", this.model);
                        }
                        if (!TextUtils.isEmpty(this.year)) {
                            jsonObject.accumulate("Year", this.year);
                        }
                    } catch (JSONException e3) {
                        sLogger.d("Error writing meta data JSON ", e3);
                    }
                    bw.write(jsonObject.toString());
                    bw.flush();
                    IOUtils.closeStream(fw2);
                    IOUtils.closeStream(bw);
                    fw = fw2;
                } catch (IOException e4) {
                    fw = fw2;
                    sLogger.d("Error writing to the metadata file");
                    IOUtils.closeStream(fw);
                    IOUtils.closeStream(bw);
                } catch (Throwable th4) {
                    fw = fw2;
                    IOUtils.closeStream(fw);
                    IOUtils.closeStream(bw);
                    throw th4;
                }
            } catch (IOException e5) {
                sLogger.d("Error writing to the metadata file");
                IOUtils.closeStream(fw);
                IOUtils.closeStream(bw);
            }
        }
        this.currentMetaDataFile = null;
    }

    public synchronized void onCarDetailsAvailable(String make, String model, String year) {
        Throwable th;
        sLogger.d("onCarDetailsAvailable Make : " + make + " , Model : " + model + ", Year : " + year);
        reportBusMonitoringState();
        if (TextUtils.equals(this.make, make) && TextUtils.equals(this.model, model) && TextUtils.equals(this.year, year)) {
            sLogger.d("Car details same");
        } else {
            sLogger.d("Car details changed");
            this.make = make;
            this.model = model;
            this.year = year;
            if (this.isObdConnected && this.currentMetaDataFile != null) {
                FileWriter fw = null;
                BufferedWriter bw = null;
                try {
                    FileWriter fw2 = new FileWriter(this.currentMetaDataFile, true);
                    try {
                        bw = new BufferedWriter(fw2);

                        if (!TextUtils.isEmpty(make)) {
                            bw.write("Make : " + make + GlanceConstants.NEWLINE);
                        }
                        if (!TextUtils.isEmpty(model)) {
                            bw.write("Model : " + model + GlanceConstants.NEWLINE);
                        }
                        if (!TextUtils.isEmpty(year)) {
                            bw.write("Year : " + year + GlanceConstants.NEWLINE);
                        }
                        bw.flush();
                        IOUtils.closeStream(fw2);
                        IOUtils.closeStream(bw);
                        fw = fw2;
                    } catch (IOException e2) {
                        fw = fw2;
                        sLogger.d("Error writing to the metadata file");
                        IOUtils.closeStream(fw);
                        IOUtils.closeStream(bw);
                    } catch (Throwable th4) {
                        fw = fw2;
                        IOUtils.closeStream(fw);
                        IOUtils.closeStream(bw);
                        throw th4;
                    }
                } catch (IOException e3) {
                    sLogger.d("Error writing to the metadata file");
                    IOUtils.closeStream(fw);
                    IOUtils.closeStream(bw);
                }
            }
        }
    }

    public boolean isCanBusMonitoringNeeded() {
        boolean monitoringNeeded;
        boolean dataRecordedAndSent = this.sharedPreferences.getBoolean(PREF_CAN_BUS_DATA_RECORDED_AND_SENT, false);
        int sentDataVersion = this.sharedPreferences.getInt(PREF_CAN_BUS_DATA_SENT_VERSION, -1);
        sLogger.d("IsCanBusMonitoringNeeded : ? Is Engineering Build " + this.isEngineeringBuild + "Data record sent : " + dataRecordedAndSent + ", Sent Data Version : " + sentDataVersion + ", isMoving : " + this.motionDetected + ", InstantaneousMode : " + this.isInstantaneousModeOn + ", Obd Connected : " + this.isObdConnected + ", Is CAN protocol :" + this.isCanProtocol);
        if (this.isEngineeringBuild && ((!dataRecordedAndSent || sentDataVersion != this.requiredObdDataVersion) && this.motionDetected && !this.isInstantaneousModeOn && this.isObdConnected && this.isCanProtocol)) {
            monitoringNeeded = true;
        } else {
            monitoringNeeded = false;
        }
        if (monitoringNeeded) {
            long distanceTravelledWhenMonitoringStarted = this.sharedPreferences.getLong(PREFERENCE_NAVDY_MILES_WHEN_LISTENING_STARTED, -1);
            long distanceDrivenSoFar = this.tripManager.getTotalDistanceTravelledWithNavdy();
            if (distanceTravelledWhenMonitoringStarted == -1) {
                this.sharedPreferences.edit().putLong(PREFERENCE_NAVDY_MILES_WHEN_LISTENING_STARTED, distanceDrivenSoFar).apply();
            } else if (distanceDrivenSoFar - distanceTravelledWhenMonitoringStarted > CAN_BUS_MONITORING_DISTANCE_LIMIT_METERS) {
                sLogger.d("CAN bus monitoring limit reached, Distance recorded when listening started " + distanceTravelledWhenMonitoringStarted + ", Current distance travelled " + distanceDrivenSoFar);
                this.isCanBusMonitoringLimitReached = true;
            }
        } else {
            this.isCanBusMonitoringLimitReached = false;
        }
        return monitoringNeeded;
    }

    public void onNewDataAvailable(String filePath) {
        sLogger.d("onNewDataAvailable , File path : " + filePath);
        File dataFile = new File(filePath);
        String dateString = dateFormat.format(new Date(System.currentTimeMillis()));
        if (dataFile.exists()) {
            File newFile = new File(FILES_TO_UPLOAD_DIRECTORY + File.separator + dataFile.getName() + "" + dateString + ".log");
            sLogger.d("Moving From : " + dataFile.getAbsolutePath() + ", TO : " + newFile.getAbsolutePath());
            dataFile.renameTo(newFile);
        }
        File[] files = new File(FILES_TO_UPLOAD_DIRECTORY).listFiles();
        List<File> filesToBeBundled = new ArrayList();
        if (files != null) {
            for (File childrenFile : files) {
                sLogger.d("Child " + childrenFile.getName());
                if (childrenFile.isFile() && !childrenFile.getName().endsWith(".zip")) {
                    filesToBeBundled.add(childrenFile);
                }
            }
            if (filesToBeBundled.size() > 0) {
                File[] filesToBeBundledArray = new File[filesToBeBundled.size()];
                filesToBeBundled.toArray(filesToBeBundledArray);
                StringBuilder builder = new StringBuilder();
                builder.append("CAN_BUS_DATA_");
                if (!TextUtils.isEmpty(this.make)) {
                    builder.append(this.make + MusicDataUtils.ALTERNATE_SEPARATOR);
                }
                if (!TextUtils.isEmpty(this.model)) {
                    builder.append(this.model + MusicDataUtils.ALTERNATE_SEPARATOR);
                }
                if (!TextUtils.isEmpty(this.year)) {
                    builder.append(this.year + MusicDataUtils.ALTERNATE_SEPARATOR);
                }
                if (!TextUtils.isEmpty(this.vin)) {
                    builder.append(this.vin + MusicDataUtils.ALTERNATE_SEPARATOR);
                }
                builder.append("V(" + this.requiredObdDataVersion + HereManeuverDisplayBuilder.CLOSE_BRACKET);
                builder.append(dateString).append(".zip");
                File uploadFile = new File(FILES_TO_UPLOAD_DIRECTORY + File.separator + builder.toString());
                sLogger.d("Compressing the files to " + uploadFile.getName());
                IOUtils.compressFilesToZip(HudApplication.getAppContext(), filesToBeBundledArray, uploadFile.getAbsolutePath());
                for (File temp : filesToBeBundledArray) {
                    IOUtils.deleteFile(HudApplication.getAppContext(), temp.getAbsolutePath());
                }
                uploadFiles.add(uploadFile);
                if (uploadFiles.size() > 5) {
                    File oldestFile = (File) uploadFiles.poll();
                    if (oldestFile != null) {
                        sLogger.d("Deleting upload file " + oldestFile.getName() + ", As its old");
                        IOUtils.deleteFile(HudApplication.getAppContext(), oldestFile.getAbsolutePath());
                    }
                }
                ObdCANBusDataUploadService.addObdDataFileToQueue(uploadFile);
                ObdCANBusDataUploadService.syncNow();
                return;
            }
            return;
        }
        sLogger.d("No files found in the upload directory");
    }

    private void populateFilesQueue() {
        metaDataFiles.clear();
        uploadFiles.clear();
        for (File childFile : UPLOAD_DIRECTORY_FILE.listFiles()) {
            File oldestFile;
            if (childFile.isFile() && childFile.getName().startsWith("meta_data")) {
                metaDataFiles.add(childFile);
                if (metaDataFiles.size() > 10) {
                    oldestFile = (File) metaDataFiles.poll();
                    if (oldestFile != null) {
                        sLogger.d("Deleting meta data file " + oldestFile.getName() + ", As its old");
                        IOUtils.deleteFile(HudApplication.getAppContext(), oldestFile.getAbsolutePath());
                    }
                }
            } else if (childFile.isFile() && childFile.getName().startsWith("CAN_BUS_DATA_")) {
                uploadFiles.add(childFile);
                if (uploadFiles.size() > 5) {
                    oldestFile = (File) uploadFiles.poll();
                    if (oldestFile != null) {
                        sLogger.d("Deleting upload file " + oldestFile.getName() + ", As its old");
                        IOUtils.deleteFile(HudApplication.getAppContext(), oldestFile.getAbsolutePath());
                    }
                }
            }
        }
    }

    public void onFileUploaded(File file) {
        sLogger.d("File successfully uploaded " + file.getAbsolutePath());
        this.sharedPreferences.edit().putBoolean(PREF_CAN_BUS_DATA_RECORDED_AND_SENT, true).commit();
        this.sharedPreferences.edit().putInt(PREF_CAN_BUS_DATA_SENT_VERSION, this.requiredObdDataVersion).commit();
        this.sharedPreferences.edit().putLong(PREFERENCE_NAVDY_MILES_WHEN_LISTENING_STARTED, -1);
        populateFilesQueue();
        AnalyticsSupport.recordObdCanBusDataSent(Integer.toString(this.requiredObdDataVersion));
        this.obdManager.updateListener();
    }

    public void onInstantaneousModeChanged(boolean enabled) {
        this.isInstantaneousModeOn = enabled;
    }

    @Subscribe
    public void onDrivingStateChanged(DrivingStateChange drivingStateChange) {
        boolean motionDetected = drivingStateChange.driving;
        if (this.motionDetected != motionDetected) {
            boolean isCanBusMonitoringNeededPrev = isCanBusMonitoringNeeded();
            this.motionDetected = motionDetected;
            if (isCanBusMonitoringNeededPrev != isCanBusMonitoringNeeded()) {
                sLogger.d("Can BUS monitoring need changed, update listener");
                this.obdManager.updateListener();
                return;
            }
            sLogger.d("Can BUS monitoring need is not changed due to driving state change");
        }
    }

    private File getMetaDataFile() {
        File file = new File(META_DATA_FILE + dateFormat.format(new Date(System.currentTimeMillis())) + ".txt");
        File dir = file.getParentFile();
        if (!dir.exists()) {
            sLogger.d("Upload directory not found, creating");
            dir.mkdirs();
        }
        sLogger.d("Creating Meta data file " + file.getName());
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                sLogger.e("IOException while creating file ", e);
            }
        }
        return file;
    }

    public void onCanBusMonitorSuccess(int approxDataSize) {
        this.canBusMonitoringState = CanBusMonitoringState.SUCCESS;
        this.canBusMonitoringDataSize = approxDataSize;
        reportBusMonitoringState();
    }

    public void onCanBusMonitoringFailed() {
        double speed = (double) SpeedManager.getInstance().getObdSpeed();
        double rpm = (double) this.obdManager.getEngineRpm();
        sLogger.d("onCanBusMonitoringFailed , RawSpeed : " + speed + ", RPM : " + rpm);
        if (speed > 0.0d || rpm > 0.0d) {
            this.canBusMonitoringState = CanBusMonitoringState.FAILURE;
            reportBusMonitoringState();
        }
    }

    private void reportBusMonitoringState() {
        boolean isMonitoringStateReported;
        boolean z = true;
        int i = 0;
        if (this.sharedPreferences.getInt(PREF_CAN_BUS_DATA_STATUS_REPORTED_VERSION, -1) == this.requiredObdDataVersion) {
            isMonitoringStateReported = true;
        } else {
            isMonitoringStateReported = false;
        }
        sLogger.d("reportBusMonitoringState, Already reported ? : " + isMonitoringStateReported);
        if (!isMonitoringStateReported) {
            sLogger.d("Reporting, Make " + this.make + ", Model : " + this.model + ", Year : " + this.year + ", State : " + this.canBusMonitoringState);
            if (!TextUtils.isEmpty(this.make) && !TextUtils.isEmpty(this.model) && !TextUtils.isEmpty(this.year) && this.canBusMonitoringState != CanBusMonitoringState.UNKNOWN) {
                String str = this.vin;
                if (this.canBusMonitoringState != CanBusMonitoringState.SUCCESS) {
                    z = false;
                }
                if (this.canBusMonitoringState == CanBusMonitoringState.SUCCESS) {
                    i = this.canBusMonitoringDataSize;
                }
                AnalyticsSupport.recordObdCanBusMonitoringState(str, z, i, this.requiredObdDataVersion);
                this.sharedPreferences.edit().putInt(PREF_CAN_BUS_DATA_STATUS_REPORTED_VERSION, this.requiredObdDataVersion).apply();
            }
        }
    }

    public boolean isCanBusMonitoringLimitReached() {
        return this.isCanBusMonitoringLimitReached;
    }
}
