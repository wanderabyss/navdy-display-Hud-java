package com.navdy.hud.app.util;

import android.content.Context;
//import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;
import android.os.Build;
import android.webkit.MimeTypeMap;

//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;

//import com.loopj.android.http.AsyncHttpClient;
//import com.loopj.android.http.RequestParams;
//import com.loopj.android.http.AsyncHttpResponseHandler;
//import cz.msebera.android.httpclient.Header;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Request;
import okhttp3.Response;

//import com.loopj.android.http.*;
import com.navdy.hud.app.BuildConfig;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager$ToastParams;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.service.library.log.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class HockeySender {
    private static final String TAG = HockeySender.class.getName();
    private static String BASE_URL = "https://rink.hockeyapp.net/api/2/apps/";
    private static final String HOCKEY_APP_ID = "94c05da80f9f4219a02cad7b7604b330";
    private static final String HOCKEY_APP_DEVELOP_ID = "49b87814edb942dea24362a20278abe4";
    private static String FORM_KEY = DeviceUtil.isTaggedRelease() ? HOCKEY_APP_ID : HOCKEY_APP_DEVELOP_ID;
    private static String CRASHES_PATH = "/crashes/upload";
    private static Logger sLogger = new Logger(TAG);

    private static final OkHttpClient client = new OkHttpClient.Builder()
                                                    .connectTimeout(10, TimeUnit.SECONDS)
                                                    .writeTimeout(30, TimeUnit.SECONDS)
                                                    .readTimeout(30, TimeUnit.SECONDS)
                                                    .build();
    private static final MediaType MEDIA_TYPE_ZIP = MediaType.parse("application/zip");
    private static final MediaType MEDIA_TYPE_TXT = MediaType.parse("text/plain");

    public static Boolean uploadReport(String log, String description,
                                    String userID, String contact,
                                       File attachment0, File attachment1, File attachment2) {

        Boolean success = false;
        String url = BASE_URL + FORM_KEY + CRASHES_PATH;

        MultipartBody.Builder bldr = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("log", "log.txt", RequestBody.create(MEDIA_TYPE_TXT, HockeySender.createCrashLog(log, contact)))
                .addFormDataPart("description", "desc.txt", RequestBody.create(MEDIA_TYPE_TXT, description))
                .addFormDataPart("userID", userID)
                .addFormDataPart("contact", contact);

        if (attachment0 != null) {
            bldr.addFormDataPart("attachment0", attachment0.getName(),
                    RequestBody.create(getMimeType(attachment0.getName()), attachment0));
        }
        if (attachment1 != null) {
            bldr.addFormDataPart("attachment1", attachment1.getName(),
                    RequestBody.create(getMimeType(attachment1.getName()), attachment1));
        }
        if (attachment2 != null) {
            bldr.addFormDataPart("attachment2", attachment2.getName(),
                    RequestBody.create(getMimeType(attachment2.getName()), attachment2));
        }
        RequestBody requestBody = bldr.build();

        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(url)
                .post(requestBody)
                .build();

        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

            String rsp = response.body().string();
            System.out.println(rsp);
            success = true;
            HockeySender.showReportSendToast(true, null);

            //TODO on success, delete snapshot zip

        } catch (IOException e) {
            e.printStackTrace();
            HockeySender.showReportSendToast(false, e.toString());
        }

        return success;

    }

    public static MediaType getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return MediaType.parse(type);
    }

    public static String createCrashLog(String log, String contact) {
        Date now = new Date();
        String header = ("Package: " + BuildConfig.APPLICATION_ID + "\n") +
                "Version Code: " + BuildConfig.VERSION_CODE + "\n" +
                "Version Name: " + BuildConfig.VERSION_NAME + "\n" +
                "Android: " + Build.VERSION.RELEASE + "\n" +
                "Manufacturer: " + "Navdy" + "\n" +
                "Model: " + "Display" + "\n" +
                "Date: " + now + "\n" +
                "CrashReporter Key: " + CrashReporter.getUserID() + "\n\n" +
                "Manual Report Submission: \"" + contact + "\"\n" +
                "  at User.Report(User_Report:-1)\n\n";

        // Hockeyapp limits log to 200kB
        String details = log;
        while (header.length() + details.length() >= 192*1024) {
            details = details.substring(details.indexOf('\n')+1);
        }
        return header + details;
    }

    public static void showReportSendToast(Boolean sent, String error) {
        Bundle bundle = new Bundle();
        int icon;
        ToastManager toastManager = ToastManager.getInstance();
        String detail;
        if (sent) {
            icon = R.drawable.icon_success;
            detail = "Report Sent, please follow up on gitlab and/or reddit";
        } else {
            icon = R.drawable.icon_msg_failed;
            detail = "Report upload failed, please try again.\n" + error;
        }
        bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 3000);
        String id = "report-send";
        bundle.putBoolean(ToastPresenter.EXTRA_DEFAULT_CHOICE, true);
        toastManager.dismissCurrentToast(id);
        toastManager.clearPendingToast(id);

        bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, icon);
        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, detail);
        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
        if (!TextUtils.equals(id, toastManager.getCurrentToastId())) {
            ToastManager.getInstance().addToast(new ToastManager$ToastParams(id, bundle, null, false, false));
        }
    }


}