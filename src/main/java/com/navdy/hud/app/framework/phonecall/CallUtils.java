package com.navdy.hud.app.framework.phonecall;

import com.navdy.hud.app.manager.RemoteDeviceManager;

public class CallUtils {
    private static CallManager callManager;

    public static synchronized boolean isPhoneCallInProgress() {
        boolean isCallInProgress;
        synchronized (CallUtils.class) {
            if (callManager == null) {
                callManager = RemoteDeviceManager.getInstance().getCallManager();
            }
            isCallInProgress = callManager.isCallInProgress();
        }
        return isCallInProgress;
    }
}
