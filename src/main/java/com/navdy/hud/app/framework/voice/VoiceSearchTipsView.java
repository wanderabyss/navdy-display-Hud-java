package com.navdy.hud.app.framework.voice;

import java.util.Locale;
import android.animation.Animator.AnimatorListener;

import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.animation.AnimatorInflater;
import butterknife.ButterKnife;
import android.util.AttributeSet;
import android.content.Context;
import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;
import android.view.View;
import android.view.ViewGroup;
import android.os.Handler;
import android.animation.Animator;
import android.widget.TextView;
import butterknife.InjectView;
import android.widget.ImageView;
import android.animation.AnimatorSet;
import android.content.res.TypedArray;
import com.navdy.hud.app.ui.activity.Main;
import android.widget.RelativeLayout;

public class VoiceSearchTipsView extends RelativeLayout implements Main.INotificationExtensionView
{
    public static final int ANIMATION_DURATION = 600;
    public static final int ANIMATION_INTERVAL = 3000;
    private static int TIPS_COUNT;
    private static TypedArray VOICE_SEARCH_TIPS_ICONS;
    private static String[] VOICE_SEARCH_TIPS_TEXTS;
    private AnimatorSet animatorSet;
    @InjectView(R.id.audio_source_icon)
    public ImageView audioSourceIcon;
    @InjectView(R.id.audio_source_text)
    public TextView audioSourceText;
    private int currentTipIndex;
    private Animator fadeIn;
    private Animator fadeOut;
    private Animator fadeOutVoiceSearchInformationView;
    private Handler handler;
    @InjectView(R.id.tip_holder_1)
    ViewGroup holder1;
    @InjectView(R.id.tip_holder_2)
    ViewGroup holder2;
    private boolean isListeningOverBluetooth;
    private String langauge;
    private String languageName;
    @InjectView(R.id.locale_full_text)
    public TextView localeFullNameText;
    @InjectView(R.id.locale_tag_text)
    public TextView localeTagText;
    private Runnable runnable;
    private boolean runningForFirstTime;
    @InjectView(R.id.tip_icon_1)
    ImageView tipsIcon1;
    @InjectView(R.id.tip_icon_2)
    ImageView tipsIcon2;
    @InjectView(R.id.tip_text_1)
    TextView tipsText1;
    @InjectView(R.id.tip_text_2)
    TextView tipsText2;
    @InjectView(R.id.voice_search_information)
    public View voiceSearchInformationView;
    
    static {
        VoiceSearchTipsView.TIPS_COUNT = 0;
        final Resources resources = HudApplication.getAppContext().getResources();
        VoiceSearchTipsView.VOICE_SEARCH_TIPS_TEXTS = resources.getStringArray(R.array.voice_search_tips);
        VoiceSearchTipsView.TIPS_COUNT = VoiceSearchTipsView.VOICE_SEARCH_TIPS_TEXTS.length;
        VoiceSearchTipsView.VOICE_SEARCH_TIPS_ICONS = resources.obtainTypedArray(R.array.voice_search_tips_icons);
    }
    
    public VoiceSearchTipsView(final Context context) {
        this(context, null);
    }
    
    public VoiceSearchTipsView(final Context context, final AttributeSet set) {
        this(context, set, -1);
    }
    
    public VoiceSearchTipsView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.isListeningOverBluetooth = false;
        this.currentTipIndex = 0;
        this.runningForFirstTime = true;
    }
    
    private int getIconIdForIndex(final int n) {
        return VoiceSearchTipsView.VOICE_SEARCH_TIPS_ICONS.getResourceId(n, -1);
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        this.fadeIn = AnimatorInflater.loadAnimator(this.getContext(), 17498112);
        this.fadeOut = AnimatorInflater.loadAnimator(this.getContext(), 17498113);
        this.fadeIn.setTarget(this.holder2);
        this.fadeOut.setTarget(this.holder1);
        (this.fadeOutVoiceSearchInformationView = AnimatorInflater.loadAnimator(this.getContext(), 17498113)).setTarget(this.voiceSearchInformationView);
        this.fadeOutVoiceSearchInformationView.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                super.onAnimationEnd(animator);
                VoiceSearchTipsView.this.handler.post(VoiceSearchTipsView.this.runnable);
            }
        });
        (this.animatorSet = new AnimatorSet()).addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                super.onAnimationEnd(animator);
                VoiceSearchTipsView.this.currentTipIndex = (VoiceSearchTipsView.this.currentTipIndex + 1) % VoiceSearchTipsView.TIPS_COUNT;
                final String text = VoiceSearchTipsView.VOICE_SEARCH_TIPS_TEXTS[VoiceSearchTipsView.this.currentTipIndex];
                final int access$500 = VoiceSearchTipsView.this.getIconIdForIndex(VoiceSearchTipsView.this.currentTipIndex);
                VoiceSearchTipsView.this.tipsText1.setText((CharSequence)text);
                VoiceSearchTipsView.this.tipsIcon1.setImageResource(access$500);
                VoiceSearchTipsView.this.holder1.setAlpha(1.0f);
                VoiceSearchTipsView.this.holder2.setAlpha(0.0f);
            }
        });
        this.animatorSet.setDuration(600L);
        this.animatorSet.playTogether(new Animator[] { this.fadeIn, this.fadeOut });
        this.handler = new Handler();
        this.runnable = new Runnable() {
            @Override
            public void run() {
                if (VoiceSearchTipsView.this.runningForFirstTime) {
                    VoiceSearchTipsView.this.currentTipIndex = 0;
                    final String text = VoiceSearchTipsView.VOICE_SEARCH_TIPS_TEXTS[VoiceSearchTipsView.this.currentTipIndex];
                    final int access$500 = VoiceSearchTipsView.this.getIconIdForIndex(VoiceSearchTipsView.this.currentTipIndex);
                    VoiceSearchTipsView.this.tipsText1.setText((CharSequence)text);
                    VoiceSearchTipsView.this.tipsIcon1.setImageResource(access$500);
                    VoiceSearchTipsView.this.holder1.setAlpha(1.0f);
                    VoiceSearchTipsView.this.holder2.setAlpha(0.0f);
                    VoiceSearchTipsView.this.runningForFirstTime = false;
                }
                else {
                    final int n = (VoiceSearchTipsView.this.currentTipIndex + 1) % VoiceSearchTipsView.TIPS_COUNT;
                    final String text2 = VoiceSearchTipsView.VOICE_SEARCH_TIPS_TEXTS[n];
                    final int access$501 = VoiceSearchTipsView.this.getIconIdForIndex(n);
                    VoiceSearchTipsView.this.tipsText2.setText((CharSequence)text2);
                    VoiceSearchTipsView.this.tipsIcon2.setImageResource(access$501);
                    VoiceSearchTipsView.this.animatorSet.start();
                }
                VoiceSearchTipsView.this.handler.postDelayed((Runnable)this, 3000L);
            }
        };
    }
    
    public void onStart() {
        this.currentTipIndex = 0;
        if (this.isListeningOverBluetooth) {
            this.audioSourceIcon.setImageResource(R.drawable.icon_badge_bt_tips);
            this.audioSourceText.setText(R.string.bluetooth_microphone);
        }
        else {
            this.audioSourceIcon.setImageResource(R.drawable.icon_badge_phone_source);
            this.audioSourceText.setText(R.string.phone_microphone);
        }
        this.localeTagText.setText((CharSequence)this.langauge);
        this.localeFullNameText.setText((CharSequence)this.languageName);
        this.handler.postDelayed((Runnable)new Runnable() {
            @Override
            public void run() {
                VoiceSearchTipsView.this.fadeOutVoiceSearchInformationView.start();
            }
        }, 3000L);
    }
    
    public void onStop() {
        this.handler.removeCallbacks(this.runnable);
    }
    
    public void setListeningOverBluetooth(final boolean isListeningOverBluetooth) {
        this.isListeningOverBluetooth = isListeningOverBluetooth;
    }
    
    public void setVoiceSearchLocale(final Locale locale) {
        this.langauge = locale.getLanguage();
        this.languageName = locale.getDisplayLanguage();
    }
}
