package com.navdy.hud.app.framework.notifications;

class NotificationManager$12 implements Runnable {
    final com.navdy.hud.app.framework.notifications.NotificationManager this$0;
    final boolean val$hideNotification;
    
    NotificationManager$12(com.navdy.hud.app.framework.notifications.NotificationManager a, boolean b) {
        super();
        this.this$0 = a;
        this.val$hideNotification = b;
    }
    
    public void run() {
        com.navdy.hud.app.framework.notifications.NotificationManager.access$700(this.this$0).setVisibility(8);
        com.navdy.hud.app.framework.notifications.NotificationManager.access$1102(this.this$0, false);
        boolean b = this.val$hideNotification;
        com.navdy.hud.app.framework.notifications.NotificationManager$Info a = com.navdy.hud.app.framework.notifications.NotificationManager.access$1200(this.this$0);
        label0: {
            label1: {
                label2: {
                    if (a == null) {
                        break label2;
                    }
                    if (com.navdy.hud.app.framework.notifications.NotificationManager.access$1200(this.this$0) != com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0)) {
                        break label1;
                    }
                }
                if (com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0) == null) {
                    break label0;
                }
                if (com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).removed) {
                    com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("stackchange:hide-remove");
                    b = true;
                    break label0;
                } else {
                    if (com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).startCalled) {
                        break label0;
                    }
                    if (com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).removed) {
                        break label0;
                    }
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.getView(com.navdy.hud.app.framework.notifications.NotificationManager.access$3500(this.this$0).getContext());
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).startCalled = true;
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.onStart(com.navdy.hud.app.framework.notifications.NotificationManager.access$1900(this.this$0));
                    break label0;
                }
            }
            com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("stackchange:current and stack are diff");
            if (com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0) != null && com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).startCalled) {
                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v(new StringBuilder().append("stackchange:stopping current:").append(com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.getId()).toString());
                com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).startCalled = false;
                com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.onStop();
            }
            com.navdy.hud.app.framework.notifications.NotificationManager.access$402(this.this$0, com.navdy.hud.app.framework.notifications.NotificationManager.access$1200(this.this$0));
            if (com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0) != null) {
                com.navdy.hud.app.framework.notifications.NotificationManager.access$3500(this.this$0).switchNotfication(com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification);
                if (com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).removed) {
                    com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("stackchange:hide-diff-remove");
                    b = true;
                } else if (!com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).startCalled) {
                    com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v(new StringBuilder().append("stackchange:starting stacked:").append(com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.getId()).toString());
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.getView(com.navdy.hud.app.framework.notifications.NotificationManager.access$3500(this.this$0).getContext());
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).startCalled = true;
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.onStart(com.navdy.hud.app.framework.notifications.NotificationManager.access$1900(this.this$0));
                }
                if (!b) {
                    com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("calling onExpandedNotificationEvent-collapse-");
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode.COLLAPSE);
                }
            }
            this.this$0.setNotificationColor();
            com.navdy.hud.app.framework.notifications.NotificationManager.access$3500(this.this$0).showNextNotificationColor();
        }
        com.navdy.hud.app.framework.notifications.NotificationManager.access$1202(this.this$0, (com.navdy.hud.app.framework.notifications.NotificationManager$Info)null);
        com.navdy.hud.app.framework.notifications.NotificationManager.access$1302(this.this$0, (com.navdy.hud.app.framework.notifications.NotificationManager$Info)null);
        this.this$0.clearOperationQueue();
        if (b) {
            com.navdy.hud.app.framework.notifications.NotificationManager.access$4400(this.this$0);
        } else {
            com.navdy.hud.app.framework.notifications.NotificationManager.access$502(this.this$0, false);
        }
    }
}
