package com.navdy.hud.app.framework.notifications;

import android.animation.AnimatorSet;
import android.content.Context;
import android.view.View;
import com.navdy.hud.app.gesture.GestureDetector.GestureListener;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;

public interface INotification extends IInputHandler, GestureListener {
    boolean canAddToStackIfCurrentExists();

    boolean expandNotification();

    int getColor();

    View getExpandedView(Context context, Object obj);

    int getExpandedViewIndicatorColor();

    String getId();

    int getTimeout();

    NotificationType getType();

    View getView(Context context);

    AnimatorSet getViewSwitchAnimation(boolean z);

    boolean isAlive();

    boolean isPurgeable();

    void onExpandedNotificationEvent(Mode mode);

    void onExpandedNotificationSwitched();

    void onNotificationEvent(Mode mode);

    void onStart(INotificationController iNotificationController);

    void onStop();

    void onUpdate();

    boolean supportScroll();
}
