package com.navdy.hud.app.framework.places;

import com.navdy.hud.app.R;
import com.navdy.hud.app.maps.here.HerePlacesManager$Error;
import com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener;
import com.navdy.service.library.events.input.Gesture;
import com.navdy.service.library.events.RequestStatus;
import com.squareup.otto.Subscribe;
import android.text.TextUtils;
import com.navdy.service.library.events.places.PlaceTypeSearchResponse;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.manager.InputManager;
import android.animation.AnimatorSet;
import butterknife.ButterKnife;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.navdy.hud.app.framework.notifications.NotificationType;
import android.animation.Animator.AnimatorListener;
import android.animation.Animator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.animation.TimeInterpolator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.View;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.service.library.events.places.PlaceTypeSearchRequest;
import com.here.android.mpa.search.Address;
import com.here.android.mpa.common.GeoCoordinate;
import java.util.Iterator;
import com.here.android.mpa.search.NavigationPosition;
import com.navdy.service.library.events.location.LatLong;
import com.here.android.mpa.search.Place;
import android.content.Context;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.ui.component.destination.IDestinationPicker;
import com.navdy.service.library.events.ui.Screen;
import android.os.Parcelable;
import com.navdy.hud.app.maps.util.DestinationUtil;
import com.navdy.hud.app.ui.component.destination.DestinationParcelable;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.graphics.Shader;
import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen;
import com.navdy.hud.app.maps.here.HerePlacesManager;
import com.here.android.mpa.search.CategoryFilter;
import java.util.UUID;
import android.os.Looper;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.content.res.Resources;
import java.util.HashMap;
import java.util.ArrayList;
import com.navdy.hud.app.HudApplication;
import android.widget.ImageView;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.service.library.events.destination.Destination;
import android.widget.TextView;
import com.navdy.service.library.events.places.PlaceType;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import android.animation.ObjectAnimator;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import android.os.Handler;
import com.navdy.hud.app.framework.notifications.INotificationController;
import butterknife.InjectView;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import java.util.List;
import java.util.Map;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout2.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Selection;

import static android.view.View.GONE;

public class NearbyPlaceSearchNotification implements INotification, IListener
{
    private static final float ICON_SCALE = 1.38f;
    private static final int N_OFFLINE_RESULTS = 7;
    private static final long TIMEOUT = 10000L;
    private static final Map<PlaceTypeSearchState, List<Choice>> choicesMapping;
    private static final Logger logger;
    private static final String searchAgain;
    private static final int searchColor;
    private static final int secondaryColor;
    private final Bus bus;
    @InjectView(R.id.choiceLayout)
    ChoiceLayout2 choiceLayout;
    private INotificationController controller;
    private PlaceTypeSearchState currentState;
    private final Handler handler;
    @InjectView(R.id.iconColorView)
    IconColorImageView iconColorView;
    @InjectView(R.id.iconSide)
    IconColorImageView iconSide;
    private ObjectAnimator loadingAnimator;
    private final NotificationManager notificationManager;
    private final PlaceType placeType;
    private final String requestId;
    @InjectView(R.id.resultsCount)
    TextView resultsCount;
    @InjectView(R.id.resultsLabel)
    TextView resultsLabel;
    private List<Destination> returnedDestinations;
    private final SpeedManager speedManager;
    @InjectView(R.id.iconSpinner)
    ImageView statusBadge;
    @InjectView(R.id.subTitle)
    TextView subTitle;
    private final Runnable timeoutForFailure;
    @InjectView(R.id.title)
    TextView title;
    
    static {
        logger = new Logger(NearbyPlaceSearchNotification.class);
        final Resources resources = HudApplication.getAppContext().getResources();
        final String string = resources.getString(R.string.dismiss);
        final int color = resources.getColor(R.color.glance_dismiss);
        final int color2 = resources.getColor(R.color.glance_ok_blue);
        final ChoiceLayout2.Choice choice = new ChoiceLayout2.Choice(R.id.dismiss, R.drawable.icon_glances_dismiss, color, R.drawable.icon_glances_dismiss, -16777216, string, color);
        final ChoiceLayout2.Choice choice2 = new ChoiceLayout2.Choice(R.id.retry, R.drawable.icon_glances_retry, color2, R.drawable.icon_glances_retry, -16777216, resources.getString(R.string.retry), color2);
        final ArrayList<Choice> list = new ArrayList<Choice>();
        list.add(new ChoiceLayout2.Choice(R.id.dismiss, R.drawable.icon_glances_dismiss, color, R.drawable.icon_glances_dismiss, -16777216, resources.getString(R.string.cancel), color));
        final ArrayList<Choice> list2 = new ArrayList<Choice>();
        list2.add(choice2);
        list2.add(choice);
        final ArrayList<Choice> list3 = new ArrayList<Choice>();
        list3.add(choice2);
        list3.add(choice);
        final int color3 = resources.getColor(R.color.glance_ok_blue);
        final ArrayList<Choice> list4 = new ArrayList<Choice>();
        list4.add(new ChoiceLayout2.Choice(R.id.dismiss, R.drawable.icon_glances_read, color3, R.drawable.icon_glances_read, -16777216, resources.getString(R.string.view), color3));
        list4.add(choice);
        secondaryColor = resources.getColor(R.color.place_type_search_secondary_color);
        (choicesMapping = new HashMap<PlaceTypeSearchState, List<Choice>>(4)).put(PlaceTypeSearchState.SEARCHING, list);
        NearbyPlaceSearchNotification.choicesMapping.put(PlaceTypeSearchState.ERROR, list2);
        NearbyPlaceSearchNotification.choicesMapping.put(PlaceTypeSearchState.NO_RESULTS, list3);
        NearbyPlaceSearchNotification.choicesMapping.put(PlaceTypeSearchState.SEARCH_COMPLETE, list4);
        searchColor = resources.getColor(R.color.mm_search);
        searchAgain = resources.getString(R.string.search_again);
    }
    
    public NearbyPlaceSearchNotification(final PlaceType placeType) {
        this.timeoutForFailure = new Runnable() {
            @Override
            public void run() {
                if (NearbyPlaceSearchNotification.this.controller != null) {
                    NearbyPlaceSearchNotification.this.stopLoadingAnimation();
                    NearbyPlaceSearchNotification.this.goToFailedState();
                }
            }
        };
        this.bus = RemoteDeviceManager.getInstance().getBus();
        this.speedManager = SpeedManager.getInstance();
        this.notificationManager = NotificationManager.getInstance();
        this.handler = new Handler(Looper.getMainLooper());
        this.requestId = UUID.randomUUID().toString();
        this.placeType = placeType;
    }
    
    private void dismissNotification() {
        this.notificationManager.removeNotification("navdy#place#type#search#notif");
    }
    
    private CategoryFilter getHereCategoryFilter(final PlaceType placeType) {
        final CategoryFilter categoryFilter = new CategoryFilter();
        switch (placeType) {
            case PLACE_TYPE_GAS:
                categoryFilter.add("petrol-station");
                break;
            case PLACE_TYPE_PARKING:
                categoryFilter.add("parking-facility");
                break;
            case PLACE_TYPE_RESTAURANT: {
                final String[] here_PLACE_TYPE_RESTAURANT = HerePlacesManager.HERE_PLACE_TYPE_RESTAURANT;
                for (int length = here_PLACE_TYPE_RESTAURANT.length, i = 0; i < length; ++i) {
                    categoryFilter.add(here_PLACE_TYPE_RESTAURANT[i]);
                }
                break;
            }
            case PLACE_TYPE_STORE:
                categoryFilter.add("shopping");
                break;
            case PLACE_TYPE_COFFEE:
                categoryFilter.add("coffee-tea");
                break;
            case PLACE_TYPE_ATM:
                categoryFilter.add("atm-bank-exchange");
                break;
            case PLACE_TYPE_HOSPITAL:
                categoryFilter.add("hospital-health-care-facility");
                break;
        }
        return categoryFilter;
    }
    
    private void goToFailedState() {
        this.iconColorView.setIcon(DestinationPickerScreen.getPlaceTypeHolder(this.placeType).iconRes, NearbyPlaceSearchNotification.secondaryColor, null, 1.38f);
        this.subTitle.setText(R.string.place_type_search_failed);
        this.statusBadge.setImageResource(R.drawable.icon_badge_alert);
        this.statusBadge.setVisibility(View.VISIBLE);
        this.iconSide.setVisibility(GONE);
        this.currentState = PlaceTypeSearchState.ERROR;
        this.choiceLayout.setChoices(NearbyPlaceSearchNotification.choicesMapping.get(this.currentState), 0, (ChoiceLayout2.IListener)this);
    }
    
    private void goToSuccessfulState(final List<Destination> returnedDestinations) {
        final Resources resources = HudApplication.getAppContext().getResources();
        this.iconColorView.setIcon(0, NearbyPlaceSearchNotification.secondaryColor, null, 1.38f);
        this.resultsCount.setVisibility(View.VISIBLE);
        this.resultsCount.setText((CharSequence)String.valueOf(returnedDestinations.size()));
        this.resultsLabel.setVisibility(View.VISIBLE);
        final DestinationPickerScreen.PlaceTypeResourceHolder placeTypeHolder = DestinationPickerScreen.getPlaceTypeHolder(this.placeType);
        final int iconRes = placeTypeHolder.iconRes;
        final int color = resources.getColor(placeTypeHolder.colorRes);
        if (iconRes != 0) {
            this.statusBadge.setVisibility(GONE);
            this.iconSide.setIcon(iconRes, color, null, 0.5f);
            this.iconSide.setVisibility(View.VISIBLE);
        }
        this.currentState = PlaceTypeSearchState.SEARCH_COMPLETE;
        this.choiceLayout.setChoices(NearbyPlaceSearchNotification.choicesMapping.get(this.currentState), 0, (ChoiceLayout2.IListener)this);
        this.returnedDestinations = returnedDestinations;
    }
    
    private void goToZeroResultsState() {
        final Resources resources = HudApplication.getAppContext().getResources();
        this.iconColorView.setIcon(0, NearbyPlaceSearchNotification.secondaryColor, null, 1.38f);
        this.resultsCount.setVisibility(View.VISIBLE);
        this.resultsCount.setText((CharSequence)resources.getString(R.string.zero));
        this.resultsLabel.setVisibility(View.VISIBLE);
        final DestinationPickerScreen.PlaceTypeResourceHolder placeTypeHolder = DestinationPickerScreen.getPlaceTypeHolder(this.placeType);
        final int iconRes = placeTypeHolder.iconRes;
        final int color = resources.getColor(placeTypeHolder.colorRes);
        if (iconRes != 0) {
            this.statusBadge.setVisibility(GONE);
            this.iconSide.setIcon(iconRes, color, null, 0.5f);
            this.iconSide.setVisibility(View.VISIBLE);
        }
        this.currentState = PlaceTypeSearchState.NO_RESULTS;
        this.choiceLayout.setChoices(NearbyPlaceSearchNotification.choicesMapping.get(this.currentState), 0, (ChoiceLayout2.IListener)this);
    }
    
    private void launchPicker() {
        NearbyPlaceSearchNotification.logger.v("launch picker screen");
        final Context appContext = HudApplication.getAppContext();
        final Resources resources = appContext.getResources();
        final Bundle bundle = new Bundle();
        bundle.putBoolean("PICKER_SHOW_DESTINATION_MAP", true);
        int destinationIcon = -1;
        final DestinationPickerScreen.PlaceTypeResourceHolder placeTypeHolder = DestinationPickerScreen.getPlaceTypeHolder(this.placeType);
        if (placeTypeHolder != null) {
            destinationIcon = placeTypeHolder.destinationIcon;
        }
        bundle.putInt("PICKER_DESTINATION_ICON", destinationIcon);
        bundle.putString("PICKER_LEFT_TITLE", resources.getString(R.string.quick_search));
        bundle.putInt("PICKER_LEFT_ICON", R.drawable.icon_mm_search_2);
        bundle.putInt("PICKER_LEFT_ICON_BKCOLOR", ContextCompat.getColor(appContext, R.color.mm_search));
        bundle.putInt("PICKER_INITIAL_SELECTION", 1);
        final int color = ContextCompat.getColor(appContext, placeTypeHolder.colorRes);
        final int color2 = ContextCompat.getColor(appContext, R.color.icon_bk_color_unselected);
        final DestinationParcelable destinationParcelable = new DestinationParcelable(R.id.search_again, appContext.getString(DestinationPickerScreen.getPlaceTypeHolder(this.placeType).titleRes), NearbyPlaceSearchNotification.searchAgain, false, null, true, null, 0.0, 0.0, 0.0, 0.0, R.drawable.icon_mm_search_2, 0, NearbyPlaceSearchNotification.searchColor, color2, DestinationParcelable.DestinationType.NONE, null);
        final List<DestinationParcelable> convert = DestinationUtil.convert(appContext, this.returnedDestinations, color, color2, false);
        convert.add(0, destinationParcelable);
        final DestinationParcelable[] array = new DestinationParcelable[convert.size()];
        convert.<DestinationParcelable>toArray(array);
        bundle.putParcelableArray("PICKER_DESTINATIONS", (Parcelable[])array);
        NotificationManager.getInstance().removeNotification("navdy#place#type#search#notif", true, Screen.SCREEN_DESTINATION_PICKER, bundle, new IDestinationPicker() {
            private boolean itemSelected = false;
            private boolean retrySelected = false;
            
            @Override
            public void onDestinationPickerClosed() {
                if (!this.itemSelected && !this.retrySelected) {
                    AnalyticsSupport.recordNearbySearchResultsClose();
                }
                if (this.retrySelected) {
                    final NotificationManager instance = NotificationManager.getInstance();
                    NearbyPlaceSearchNotification nearbyPlaceSearchNotification;
                    if ((nearbyPlaceSearchNotification = (NearbyPlaceSearchNotification)instance.getNotification("navdy#place#type#search#notif")) == null) {
                        nearbyPlaceSearchNotification = new NearbyPlaceSearchNotification(NearbyPlaceSearchNotification.this.placeType);
                    }
                    NearbyPlaceSearchNotification.logger.v("launching notif search again:" + NearbyPlaceSearchNotification.this.placeType);
                    instance.addNotification(nearbyPlaceSearchNotification);
                }
            }
            
            @Override
            public boolean onItemClicked(final int n, final int n2, final DestinationPickerScreen.DestinationPickerState destinationPickerState) {
                boolean b = true;
                AnalyticsSupport.recordNearbySearchSelection(NearbyPlaceSearchNotification.this.placeType, n2);
                this.itemSelected = true;
                if (n == R.id.search_again) {
                    NearbyPlaceSearchNotification.logger.v("search again");
                    this.retrySelected = true;
                }
                else {
                    b = false;
                }
                return b;
            }
            
            @Override
            public boolean onItemSelected(final int n, final int n2, final DestinationPickerScreen.DestinationPickerState destinationPickerState) {
                return false;
            }
        });
    }
    
    private void performOfflineSearch() {
        HerePlacesManager.handleCategoriesRequest(this.getHereCategoryFilter(this.placeType), 7, (HerePlacesManager$OnCategoriesSearchListener)new HerePlacesManager$OnCategoriesSearchListener() {
            @Override
            public void onCompleted(final List<Place> list) {
                if (NearbyPlaceSearchNotification.this.controller != null) {
                    if (NearbyPlaceSearchNotification.this.currentState == PlaceTypeSearchState.ERROR) {
                        NearbyPlaceSearchNotification.logger.w("received response after place type search has already failed, no-op");
                    }
                    else {
                        NearbyPlaceSearchNotification.logger.v("performing offline search, returned places: ");
                        final Iterator<Place> iterator = list.iterator();
                        while (iterator.hasNext()) {
                            NearbyPlaceSearchNotification.logger.v(iterator.next().getName());
                        }
                        final ArrayList<Destination> list2 = new ArrayList<Destination>();
                        for (final Place place : list) {
                            final GeoCoordinate coordinate = place.getLocation().getCoordinate();
                            final LatLong latLong = new LatLong(Double.valueOf(coordinate.getLatitude()), Double.valueOf(coordinate.getLongitude()));
                            final List<NavigationPosition> accessPoints = place.getLocation().getAccessPoints();
                            LatLong latLong2;
                            if (accessPoints.size() > 0) {
                                final GeoCoordinate coordinate2 = accessPoints.get(0).getCoordinate();
                                latLong2 = new LatLong(Double.valueOf(coordinate2.getLatitude()), Double.valueOf(coordinate2.getLongitude()));
                            }
                            else {
                                latLong2 = latLong;
                            }
                            final Address address = place.getLocation().getAddress();
                            String s;
                            if (address.getHouseNumber() != null && address.getStreet() != null) {
                                s = address.getHouseNumber() + " " + address.getStreet();
                            }
                            else {
                                s = address.toString();
                            }
                            list2.add(new Destination.Builder().navigation_position(latLong2).display_position(latLong).full_address(place.getLocation().getAddress().toString()).destination_title(place.getName()).destination_subtitle(s).favorite_type(Destination.FavoriteType.FAVORITE_NONE).identifier(UUID.randomUUID().toString()).suggestion_type(Destination.SuggestionType.SUGGESTION_NONE).is_recommendation(false).last_navigated_to(0L).place_type(NearbyPlaceSearchNotification.this.placeType).build());
                        }
                        NearbyPlaceSearchNotification.this.handler.post((Runnable)new Runnable() {
                            @Override
                            public void run() {
                                if (NearbyPlaceSearchNotification.this.controller != null) {
                                    NearbyPlaceSearchNotification.this.handler.removeCallbacks(NearbyPlaceSearchNotification.this.timeoutForFailure);
                                    NearbyPlaceSearchNotification.this.stopLoadingAnimation();
                                    if (list2.size() > 0) {
                                        NearbyPlaceSearchNotification.this.goToSuccessfulState(list2);
                                    }
                                    else {
                                        NearbyPlaceSearchNotification.this.goToZeroResultsState();
                                    }
                                }
                            }
                        });
                    }
                }
            }
            
            @Override
            public void onError(final HerePlacesManager$Error error) {
                if (NearbyPlaceSearchNotification.this.controller != null) {
                    NearbyPlaceSearchNotification.logger.w("error while performing offline search: " + error.name());
                    NearbyPlaceSearchNotification.this.handler.post((Runnable)new Runnable() {
                        @Override
                        public void run() {
                            if (NearbyPlaceSearchNotification.this.controller != null) {
                                NearbyPlaceSearchNotification.this.handler.removeCallbacks(NearbyPlaceSearchNotification.this.timeoutForFailure);
                                NearbyPlaceSearchNotification.this.stopLoadingAnimation();
                                NearbyPlaceSearchNotification.this.goToFailedState();
                            }
                        }
                    });
                }
            }
        }, true);
    }
    
    private void recordSelection(final int n, final int n2) {
        switch (this.currentState) {
            case SEARCHING:
                if (n2 == R.id.dismiss) {
                    AnalyticsSupport.recordNearbySearchDismiss();
                    break;
                }
                break;
            case NO_RESULTS:
                AnalyticsSupport.recordNearbySearchNoResult(n2 == R.id.retry);
                break;
            case SEARCH_COMPLETE:
                if (n2 != R.id.dismiss) {
                    break;
                }
                if (n == 0) {
                    AnalyticsSupport.recordNearbySearchResultsView();
                    break;
                }
                AnalyticsSupport.recordNearbySearchResultsDismiss();
                break;
        }
    }
    
    private void sendRequest() {
        this.currentState = PlaceTypeSearchState.SEARCHING;
        final PlaceTypeSearchRequest build = new PlaceTypeSearchRequest.Builder().request_id(this.requestId).place_type(this.placeType).build();
        if (RemoteDeviceManager.getInstance().isAppConnected() && NetworkStateManager.isConnectedToNetwork(HudApplication.getAppContext())) {
            NearbyPlaceSearchNotification.logger.v("performing online quick search");
            this.bus.post(new RemoteEvent(build));
        }
        else {
            NearbyPlaceSearchNotification.logger.v("performing offline quick search");
            this.performOfflineSearch();
        }
        this.handler.postDelayed(this.timeoutForFailure, 10000L);
    }
    
    private void startLoadingAnimation() {
        if (this.loadingAnimator == null) {
            this.statusBadge.setImageResource(R.drawable.loader_circle);
            this.statusBadge.setVisibility(View.VISIBLE);
            this.iconSide.setVisibility(GONE);
            (this.loadingAnimator = ObjectAnimator.ofFloat(this.statusBadge, View.ROTATION, new float[] { 360.0f })).setDuration(500L);
            this.loadingAnimator.setInterpolator((TimeInterpolator)new AccelerateDecelerateInterpolator());
            this.loadingAnimator.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
                @Override
                public void onAnimationEnd(final Animator animator) {
                    if (NearbyPlaceSearchNotification.this.loadingAnimator != null) {
                        NearbyPlaceSearchNotification.this.loadingAnimator.setStartDelay(33L);
                        NearbyPlaceSearchNotification.this.loadingAnimator.start();
                    }
                    else {
                        NearbyPlaceSearchNotification.logger.v("abandon loading animation");
                    }
                }
            });
        }
        if (!this.loadingAnimator.isRunning()) {
            NearbyPlaceSearchNotification.logger.v("started loading animation");
            this.loadingAnimator.start();
        }
    }
    
    private void stopLoadingAnimation() {
        if (this.loadingAnimator != null) {
            NearbyPlaceSearchNotification.logger.v("cancelled loading animation");
            this.loadingAnimator.removeAllListeners();
            this.loadingAnimator.cancel();
            this.statusBadge.setRotation(0.0f);
            this.loadingAnimator = null;
        }
    }
    
    @Override
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    @Override
    public void executeItem(final Selection selection) {
        this.recordSelection(selection.pos, selection.id);
        switch (selection.id) {
            case R.id.dismiss: {
                if (this.currentState != PlaceTypeSearchState.SEARCH_COMPLETE) {
                    this.dismissNotification();
                    break;
                }
                int n;
                if (selection.pos == 0) {
                    n = 1;
                }
                else {
                    n = 0;
                }
                if (n != 0) {
                    this.launchPicker();
                    break;
                }
                this.dismissNotification();
                break;
            }
            case R.id.retry:
                this.startLoadingAnimation();
                this.sendRequest();
                break;
        }
    }
    
    @Override
    public boolean expandNotification() {
        return false;
    }
    
    @Override
    public int getColor() {
        return 0;
    }
    
    @Override
    public View getExpandedView(final Context context, final Object o) {
        return null;
    }
    
    @Override
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    @Override
    public String getId() {
        return "navdy#place#type#search#notif";
    }
    
    @Override
    public int getTimeout() {
        return 0;
    }
    
    @Override
    public NotificationType getType() {
        return NotificationType.PLACE_TYPE_SEARCH;
    }
    
    @Override
    public View getView(final Context context) {
        final View inflate = LayoutInflater.from(context).inflate(R.layout.notification_place_type_search, (ViewGroup)null);
        ButterKnife.inject(this, inflate);
        final DestinationPickerScreen.PlaceTypeResourceHolder placeTypeHolder = DestinationPickerScreen.getPlaceTypeHolder(this.placeType);
        this.title.setText((CharSequence)context.getString(placeTypeHolder.titleRes));
        this.iconColorView.setIcon(placeTypeHolder.iconRes, context.getResources().getColor(placeTypeHolder.colorRes), null, 1.38f);
        this.choiceLayout.setVisibility(View.VISIBLE);
        this.choiceLayout.setChoices(NearbyPlaceSearchNotification.choicesMapping.get(PlaceTypeSearchState.SEARCHING), 0, (ChoiceLayout2.IListener)this);
        return inflate;
    }
    
    @Override
    public AnimatorSet getViewSwitchAnimation(final boolean b) {
        return null;
    }
    
    @Override
    public boolean isAlive() {
        return false;
    }
    
    @Override
    public boolean isPurgeable() {
        return false;
    }
    
    @Override
    public void itemSelected(final Selection selection) {
    }
    
    @Override
    public InputManager.IInputHandler nextHandler() {
        return null;
    }
    
    @Override
    public void onClick() {
    }
    
    @Override
    public void onExpandedNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Override
    public void onExpandedNotificationSwitched() {
    }
    
    @Override
    public boolean onGesture(final GestureEvent gestureEvent) {
        final boolean b = false;
        boolean b2;
        if (this.controller == null) {
            b2 = b;
        }
        else {
            b2 = b;
            if (this.currentState == PlaceTypeSearchState.SEARCH_COMPLETE) {
                switch (gestureEvent.gesture) {
                    default:
                        b2 = b;
                        break;
                    case GESTURE_SWIPE_LEFT:
                        NearbyPlaceSearchNotification.logger.v("show route picker:gesture");
                        this.launchPicker();
                        b2 = true;
                        break;
                }
            }
        }
        return b2;
    }
    
    @Override
    public boolean onKey(final InputManager.CustomKeyEvent customKeyEvent) {
        boolean b = false;
        if (this.controller != null) {
            switch (customKeyEvent) {
                case LEFT:
                    this.choiceLayout.moveSelectionLeft();
                    b = true;
                    break;
                case RIGHT:
                    this.choiceLayout.moveSelectionRight();
                    b = true;
                    break;
                case SELECT:
                    this.choiceLayout.executeSelectedItem();
                    b = true;
                    break;
            }
        }
        return b;
    }
    
    @Override
    public void onNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Subscribe
    public void onPlaceTypeSearchResponse(final PlaceTypeSearchResponse placeTypeSearchResponse) {
        NearbyPlaceSearchNotification.logger.v("PlaceTypeSearchResponse:" + placeTypeSearchResponse.request_status);
        this.handler.removeCallbacks(this.timeoutForFailure);
        this.stopLoadingAnimation();
        if (!TextUtils.equals((CharSequence)placeTypeSearchResponse.request_id, (CharSequence)this.requestId)) {
            NearbyPlaceSearchNotification.logger.w("received wrong request_id on the PlaceTypeSearchResponse, no-op");
        }
        else if (this.currentState == PlaceTypeSearchState.ERROR) {
            NearbyPlaceSearchNotification.logger.w("received response after place type search has already failed, no-op");
        }
        else {
            switch (placeTypeSearchResponse.request_status) {
                case REQUEST_SERVICE_ERROR:
                    this.goToFailedState();
                    break;
                case REQUEST_NOT_AVAILABLE:
                    this.goToZeroResultsState();
                    break;
                case REQUEST_SUCCESS:
                    this.goToSuccessfulState(placeTypeSearchResponse.destinations);
                    break;
            }
        }
    }
    
    @Override
    public void onStart(final INotificationController controller) {
        this.controller = controller;
        this.bus.register(this);
        this.startLoadingAnimation();
        this.sendRequest();
    }
    
    @Override
    public void onStop() {
        this.controller = null;
        this.bus.unregister(this);
    }
    
    @Override
    public void onTrackHand(final float n) {
    }
    
    @Override
    public void onUpdate() {
    }
    
    @Override
    public boolean supportScroll() {
        return false;
    }

    private enum PlaceTypeSearchState {
        SEARCHING(0),
        ERROR(1),
        NO_RESULTS(2),
        SEARCH_COMPLETE(3);

        private int value;
        PlaceTypeSearchState(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
