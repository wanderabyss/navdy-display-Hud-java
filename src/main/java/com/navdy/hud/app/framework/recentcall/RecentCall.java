package com.navdy.hud.app.framework.recentcall;

import android.text.TextUtils;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.framework.contacts.NumberType;
import com.navdy.hud.app.util.PhoneUtil;
import com.navdy.service.library.log.Logger;
import java.util.Date;

public class RecentCall implements Comparable<RecentCall> {
    private static final Logger sLogger = new Logger(RecentCall.class);
    public Date callTime;
    public CallType callType;
    public Category category;
    public int defaultImageIndex;
    public String firstName;
    public String formattedNumber;
    public String initials;
    public String name;
    public String number;
    public NumberType numberType;
    public String numberTypeStr;
    public long numericNumber;

    public enum CallType {
        UNNKNOWN(0),
        INCOMING(1),
        OUTGOING(2),
        MISSED(3);
        
        int value;

        private CallType(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static CallType buildFromValue(int n) {
            switch (n) {
                case 1:
                    return INCOMING;
                case 2:
                    return OUTGOING;
                case 3:
                    return MISSED;
                default:
                    return UNNKNOWN;
            }
        }
    }

    public enum Category {
        UNNKNOWN(0),
        PHONE_CALL(1),
        MESSAGE(2);
        
        int value;

        private Category(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static Category buildFromValue(int n) {
            switch (n) {
                case 1:
                    return PHONE_CALL;
                case 2:
                    return MESSAGE;
                default:
                    return UNNKNOWN;
            }
        }
    }

    public RecentCall(String name, Category category, String number, NumberType numberType, Date callTime, CallType callType, int defaultImageIndex, long numericNumber) {
        this.name = name;
        this.number = number;
        this.category = category;
        this.numberType = numberType;
        this.callTime = callTime;
        this.callType = callType;
        this.defaultImageIndex = defaultImageIndex;
        validateArguments();
    }

    public RecentCall(RecentCall call) {
        this.name = call.name;
        this.number = call.number;
        this.category = call.category;
        this.numberType = call.numberType;
        this.callTime = call.callTime;
        this.callType = call.callType;
        this.defaultImageIndex = call.defaultImageIndex;
    }

    public int compareTo(RecentCall another) {
        if (another == null) {
            return 0;
        }
        return this.callTime.compareTo(another.callTime);
    }

    public void validateArguments() {
        if (!TextUtils.isEmpty(this.name)) {
            this.firstName = ContactUtil.getFirstName(this.name);
            this.initials = ContactUtil.getInitials(this.name);
        }
        this.numberTypeStr = ContactUtil.getPhoneType(this.numberType);
        this.formattedNumber = PhoneUtil.formatPhoneNumber(this.number);
        if (!TextUtils.isEmpty(this.number)) {
            if (this.numericNumber > 0) {
                return;
            }
            try {
                this.numericNumber = PhoneNumberUtil.getInstance().parse(this.number, DriverProfileHelper.getInstance().getCurrentLocale().getCountry()).getNationalNumber();
            } catch (Throwable t) {
                if (sLogger.isLoggable(2)) {
                    sLogger.e(t);
                }
            }
        }
    }

    public String toString() {
        return "RecentCall{name='" + this.name + '\'' + ", category=" + this.category + ", number='" + this.number + '\'' + ", numberType=" + this.numberType + ", callTime=" + this.callTime + ", callType=" + this.callType + ", defaultImageIndex=" + this.defaultImageIndex + '}';
    }
}
