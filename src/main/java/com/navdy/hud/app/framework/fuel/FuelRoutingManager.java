package com.navdy.hud.app.framework.fuel;

import android.content.res.Resources;

import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.toast.IToastCallback;
import android.os.Bundle;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.framework.toast.ToastManager$ToastParams;
import com.navdy.hud.app.maps.MapEvents$NewRouteAdded;
import com.navdy.hud.app.maps.here.HerePlacesManager$Error;
import com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener;
import com.navdy.hud.app.maps.here.HereRouteCalculator$RouteCalculatorListener;
import com.navdy.service.library.events.navigation.NavigationSessionRequest;
import com.navdy.service.library.events.navigation.NavigationSessionState;
import com.navdy.hud.app.maps.here.HereRouteCache;
import com.here.android.mpa.guidance.NavigationManager;
import com.navdy.service.library.events.navigation.NavigationSessionRouteChange;
import com.navdy.hud.app.maps.MapEvents;
import com.squareup.otto.Subscribe;
import com.navdy.service.library.events.glances.FuelConstants;
import com.navdy.service.library.events.glances.KeyValue;
import com.here.android.mpa.routing.RoutingError;
import java.util.UUID;
import com.navdy.service.library.events.destination.Destination;
import com.navdy.service.library.events.location.Coordinate;
import android.support.annotation.Nullable;
import com.here.android.mpa.common.RoadElement;
import com.here.android.mpa.routing.RouteElements;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.RouteElement;
import android.os.SystemClock;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.service.library.task.TaskManager;
import com.here.android.mpa.search.Location;
import java.util.Iterator;
import java.util.Queue;
import java.util.LinkedList;
import java.util.Collections;
import java.util.Comparator;
import com.navdy.hud.app.util.GenericUtil;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.hud.app.maps.here.HerePlacesManager;
import java.util.List;
import android.os.Looper;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.service.library.events.glances.GlanceEvent;
import java.util.concurrent.TimeUnit;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.maps.here.HereRouteCalculator;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.here.HereMapsManager;
import android.os.Handler;
import com.here.android.mpa.routing.RouteOptions;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import java.util.ArrayList;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.here.android.mpa.search.Place;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import com.here.android.mpa.search.CategoryFilter;

public final class FuelRoutingManager
{
    private static final int CANCEL_NOTIFICATION_THRESHOLD = 20;
    private static final int DEFAULT_LOW_FUEL_THRESHOLD = 15;
    public static final String FINDING_FAST_STATION_TOAST_ID = "toast#find#gas";
    private static final long FUEL_GLANCE_REDISPLAY_THRESHOLD;
    public static final CategoryFilter GAS_CATEGORY;
    private static final String GAS_STATION_CATEGORY = "petrol-station";
    public static final String LOW_FUEL_ID;
    private static final String LOW_FUEL_ID_STR = "low#fuel#level";
    private static final int N_GAS_STATIONS_REQUESTED = 3;
    private static final int ONE_MINUTE_SECONDS = 60;
    private static final long TEST_SIMULATION_SPEED = 20L;
    private static FuelRoutingManager sInstance;
    private static final Logger sLogger;
    private boolean available;
    private final Bus bus;
    private Place currentGasStation;
    private int currentLowFuelThreshold;
    private NavigationRouteRequest currentNavigationRouteRequest;
    private ArrayList<NavigationRouteResult> currentOutgoingResults;
    private long fuelGlanceDismissTime;
    private RouteOptions gasRouteOptions;
    private final Handler handler;
    private final HereMapsManager hereMapsManager;
    private HereNavigationManager hereNavigationManager;
    private HereRouteCalculator hereRouteCalculator;
    private boolean isCalculatingFuelRoute;
    private boolean isInTestMode;
    private final ObdManager obdManager;
    
    static {
        sLogger = new Logger(FuelRoutingManager.class);
        FUEL_GLANCE_REDISPLAY_THRESHOLD = TimeUnit.MINUTES.toMillis(20L);
        LOW_FUEL_ID = GlanceHelper.getNotificationId(GlanceEvent.GlanceType.GLANCE_TYPE_FUEL, "low#fuel#level");
        GAS_CATEGORY = new CategoryFilter();
        FuelRoutingManager.sInstance = new FuelRoutingManager();
        FuelRoutingManager.GAS_CATEGORY.add("petrol-station");
    }
    
    private FuelRoutingManager() {
        this.obdManager = ObdManager.getInstance();
        this.hereMapsManager = HereMapsManager.getInstance();
        this.bus = RemoteDeviceManager.getInstance().getBus();
        this.handler = new Handler(Looper.getMainLooper());
        this.isInTestMode = false;
        this.currentLowFuelThreshold = 15;
    }

    private void calculateGasStationRoutes(final OnRouteToGasStationCallback onRouteToGasStationCallback) {
        if (onRouteToGasStationCallback == null) {
            throw new IllegalArgumentException();
        }
        final GeoCoordinate gasCalculationCoords = getBestInitialGeo();
        if (gasCalculationCoords == null) {
            sLogger.w("No user location while calculating routes to gas station");
            onRouteToGasStationCallback.onError(OnRouteToGasStationCallback.Error.NO_USER_LOCATION);
            return;
        }
        HerePlacesManager.handleCategoriesRequest(GAS_CATEGORY, 3, new HerePlacesManager$OnCategoriesSearchListener() {
            public void onCompleted(List<Place> places) {
                try {
                    GenericUtil.checkNotOnMainThread();
                    if (FuelRoutingManager.this.isCalculatingFuelRoute) {
                        if (places.size() > 1) {
                            Collections.sort(places, new Comparator<Place>() {
                                public int compare(Place lhs, Place rhs) {
                                    return (int) (gasCalculationCoords.distanceTo(lhs.getLocation().getCoordinate()) - gasCalculationCoords.distanceTo(rhs.getLocation().getCoordinate()));
                                }
                            });
                        }
                        for (Place p : places) {
                            Location location = p.getLocation();
                            GeoCoordinate gasNavGeo = HerePlacesManager.getPlaceEntry(p);
                            String address = location.getAddress().toString().replace("<br/>", ", ").replace(GlanceConstants.NEWLINE, "");
                            FuelRoutingManager.sLogger.v("gas station name [" + p.getName() + "]" + " address [" + address + "]" + " distance [" + ((int) gasCalculationCoords.distanceTo(gasNavGeo)) + "] meters" + " displayPos [" + location.getCoordinate() + "]" + " navPos [" + gasNavGeo + "]");
                        }
                        Queue<FuelRoutingAction> routingQueue = new LinkedList();
                        List<RouteCacheItem> routeCache = new ArrayList(3);
                        for (Place place : places) {
                            GeoCoordinate endPoint;
                            List<GeoCoordinate> waypoints = new ArrayList();
                            GeoCoordinate placeEntry = HerePlacesManager.getPlaceEntry(place);
                            if (FuelRoutingManager.this.hereNavigationManager.isNavigationModeOn()) {
                                waypoints.add(placeEntry);
                                endPoint = FuelRoutingManager.this.hereNavigationManager.getCurrentRoute().getDestination();
                            } else {
                                endPoint = placeEntry;
                            }
                            routingQueue.add(new FuelRoutingAction(places.size(), place, routingQueue, routeCache, onRouteToGasStationCallback, gasCalculationCoords, waypoints, endPoint));
                        }
                        FuelRoutingManager.sLogger.v("posting to handler");
                        FuelRoutingManager.this.handler.post((Runnable) routingQueue.poll());
                        return;
                    }
                    onRouteToGasStationCallback.onError(OnRouteToGasStationCallback.Error.INVALID_STATE);
                } catch (Throwable t) {
                    FuelRoutingManager.sLogger.e("calculateGasStationRoutes", t);
                    onRouteToGasStationCallback.onError(OnRouteToGasStationCallback.Error.UNKNOWN_ERROR);
                }
            }

            public void onError(HerePlacesManager$Error error) {
                GenericUtil.checkNotOnMainThread();
                FuelRoutingManager.sLogger.e("calculateGasStationRoutes error:" + error);
                onRouteToGasStationCallback.onError(OnRouteToGasStationCallback.Error.RESPONSE_ERROR);
            }
        });
    }
    
    private void calculateOptimalGasStation(final List<RouteCacheItem> list, final OnRouteToGasStationCallback onRouteToGasStationCallback) {
        final RouteCacheItem routeCacheItem = null;
        int n = -1;
        final Iterator<RouteCacheItem> iterator = list.iterator();
        RouteCacheItem routeCacheItem2 = routeCacheItem;
        while (iterator.hasNext()) {
            final RouteCacheItem routeCacheItem3 = iterator.next();
            final int intValue = routeCacheItem3.route.duration;
            FuelRoutingManager.sLogger.v("evaluating [" + routeCacheItem3.gasStation.getName() + "] distance [" + (int)routeCacheItem3.gasStation.getLocation().getCoordinate().distanceTo(this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate()) + "] tta [" + intValue + "]");
            if (n < 0 || intValue < n) {
                n = intValue;
                routeCacheItem2 = routeCacheItem3;
            }
        }
        if (routeCacheItem2 != null) {
            FuelRoutingManager.sLogger.v("Optimal route: \n\tName:" + routeCacheItem2.gasStation.getName() + "\n\tAddress: " + routeCacheItem2.gasStation.getLocation().getAddress() + "\n\tTTA: " + n + " s\n\tGas station distance: " + routeCacheItem2.gasStation.getLocation().getCoordinate().distanceTo(this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate()) + " m");
            this.requestOptimalRoute(routeCacheItem2, onRouteToGasStationCallback);
        }
        else {
            FuelRoutingManager.sLogger.w("No routes to any gas stations");
            onRouteToGasStationCallback.onError(OnRouteToGasStationCallback.Error.NO_ROUTES);
        }
    }
    
    private void checkFuelLevel() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                final int fuelLevel = FuelRoutingManager.this.obdManager.getFuelLevel();
                if (!FuelRoutingManager.this.isInTestMode && fuelLevel == -1) {
                    FuelRoutingManager.sLogger.d("checkFuelLevel:fuel level is not available from OBD Manager");
                    NotificationManager.getInstance().removeNotification(FuelRoutingManager.LOW_FUEL_ID);
                }
                else if (FuelRoutingManager.this.isCalculatingFuelRoute) {
                    FuelRoutingManager.sLogger.d("checkFuelLevel: already calculating fuel route");
                }
                else if (FuelRoutingManager.this.hereNavigationManager.isOnGasRoute()) {
                    FuelRoutingManager.sLogger.d("checkFuelLevel: already on fuel route");
                }
                else {
                    if (FuelRoutingManager.this.fuelGlanceDismissTime > 0L) {
                        final long n = SystemClock.elapsedRealtime() - FuelRoutingManager.this.fuelGlanceDismissTime;
                        if (n < FuelRoutingManager.FUEL_GLANCE_REDISPLAY_THRESHOLD) {
                            FuelRoutingManager.sLogger.d("checkFuelLevel: fuel glance dismiss threshold still valid:" + n);
                            return;
                        }
                        FuelRoutingManager.sLogger.d("checkFuelLevel: fuel glance dismiss threshold expired:" + n);
                        FuelRoutingManager.this.fuelGlanceDismissTime = 0L;
                    }
                    FuelRoutingManager.sLogger.i("checkFuelLevel: Fuel level:" + fuelLevel + " threshold:" + FuelRoutingManager.this.currentLowFuelThreshold + " testMode:" + FuelRoutingManager.this.isInTestMode);
                    if (FuelRoutingManager.this.isInTestMode || fuelLevel <= FuelRoutingManager.this.currentLowFuelThreshold) {
                        if (!FuelRoutingManager.this.isInTestMode && !GlanceHelper.isFuelNotificationEnabled()) {
                            FuelRoutingManager.sLogger.i("checkFuelLevel:fuel notifications are not enabled");
                        }
                        else if (NotificationManager.getInstance().isNotificationPresent(FuelRoutingManager.LOW_FUEL_ID)) {
                            FuelRoutingManager.sLogger.i("checkFuelLevel:low fuel glance already shown to user");
                        }
                        else {
                            FuelRoutingManager.this.findGasStations(fuelLevel, true);
                        }
                    }
                    else if (fuelLevel > 20) {
                        FuelRoutingManager.this.setFuelLevelBackToNormal();
                    }
                    else if (FuelRoutingManager.sLogger.isLoggable(2)) {
                        FuelRoutingManager.sLogger.i("checkFuelLevel: no-op");
                    }
                }
            }
        }, 21);
    }

    private GeoCoordinate getBestInitialGeo() {
        GenericUtil.checkNotOnMainThread();
        GeoCoordinate bestInitialGeo = this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate();
        Route route = this.hereNavigationManager.getCurrentRoute();
        if (route == null) {
            return bestInitialGeo;
        }
        RouteElements routeElements = route.getRouteElementsFromLength((int) this.hereNavigationManager.getNavController().getElapsedDistance());
        if (routeElements == null) {
            return bestInitialGeo;
        }
        List<RouteElement> routeElementsList = routeElements.getElements();
        if (routeElementsList == null) {
            return bestInitialGeo;
        }
        GeoCoordinate oneMinuteFromNowGeo = null;
        int timeCount = 0;
        for (RouteElement routeElement : routeElementsList) {
            RoadElement roadElement = routeElement.getRoadElement();
            if (roadElement != null && roadElement.getDefaultSpeed() > 0.0f) {
                timeCount = (int) (((long) timeCount) + Math.round(roadElement.getGeometryLength() / ((double) roadElement.getDefaultSpeed())));
            }
            if (timeCount > 60) {
                List<GeoCoordinate> geometry = routeElement.getGeometry();
                if (geometry != null && geometry.size() > 0) {
                    oneMinuteFromNowGeo = (GeoCoordinate) geometry.get(geometry.size() - 1);
                    break;
                }
            }
        }
        if (oneMinuteFromNowGeo != null) {
            return oneMinuteFromNowGeo;
        }
        return bestInitialGeo;
    }
    
    @Nullable
    public static FuelRoutingManager getInstance() {
        return FuelRoutingManager.sInstance;
    }
    
    private void requestOptimalRoute(final RouteCacheItem routeCacheItem, final OnRouteToGasStationCallback onRouteToGasStationCallback) {
        try {
            final GeoCoordinate placeEntry = HerePlacesManager.getPlaceEntry(routeCacheItem.gasStation);
            final GeoCoordinate coordinate = routeCacheItem.gasStation.getLocation().getCoordinate();
            final Coordinate build = new Coordinate.Builder().latitude(placeEntry.getLatitude()).longitude(placeEntry.getLongitude()).build();
            final Coordinate build2 = new Coordinate.Builder().latitude(coordinate.getLatitude()).longitude(coordinate.getLongitude()).build();
            final ArrayList<NavigationRouteRequest.RouteAttribute> list = new ArrayList<NavigationRouteRequest.RouteAttribute>(1);
            list.add(NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_GAS);
            final NavigationRouteRequest build3 = new NavigationRouteRequest.Builder().destination(build).label(routeCacheItem.gasStation.getName()).streetAddress(routeCacheItem.gasStation.getLocation().getAddress().toString()).destination_identifier(routeCacheItem.gasStation.getId()).originDisplay(true).geoCodeStreetAddress(false).destinationType(Destination.FavoriteType.FAVORITE_NONE).requestId(UUID.randomUUID().toString()).destinationDisplay(build2).routeAttributes(list).build();
            this.hereRouteCalculator.calculateRoute(build3, this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate(), null, placeEntry, true, (HereRouteCalculator$RouteCalculatorListener)new HereRouteCalculator$RouteCalculatorListener() {
                @Override
                public void error(final RoutingError routingError, final Throwable t) {
                    onRouteToGasStationCallback.onError(OnRouteToGasStationCallback.Error.RESPONSE_ERROR);
                }
                
                @Override
                public void postSuccess(final ArrayList<NavigationRouteResult> list) {
                    onRouteToGasStationCallback.onOptimalRouteCalculationComplete(routeCacheItem, list, build3);
                }
                
                @Override
                public void preSuccess() {
                }
                
                @Override
                public void progress(final int n) {
                    if (FuelRoutingManager.sLogger.isLoggable(2)) {
                        FuelRoutingManager.sLogger.v("calculating route to optimal gas station progress %: " + n);
                    }
                }
            }, 1, this.gasRouteOptions, true, true, false);
        }
        catch (Throwable t) {
            FuelRoutingManager.sLogger.e("requestOptimalRoute", t);
            onRouteToGasStationCallback.onError(OnRouteToGasStationCallback.Error.UNKNOWN_ERROR);
        }
    }
    
    private void reset(final int currentLowFuelThreshold) {
        this.hereRouteCalculator.cancel();
        if (this.hereNavigationManager.isOnGasRoute()) {
            this.hereNavigationManager.arrived();
        }
        this.currentLowFuelThreshold = currentLowFuelThreshold;
        this.isInTestMode = false;
        this.currentOutgoingResults = null;
        this.currentNavigationRouteRequest = null;
        this.currentGasStation = null;
        if (FuelRoutingManager.sLogger.isLoggable(2)) {
            FuelRoutingManager.sLogger.v("reset, state is now TRACKING");
        }
    }
    
    private void setFuelLevelBackToNormal() {
        FuelRoutingManager.sLogger.i("checkFuelLevel:Fuel level normal");
        this.reset();
        NotificationManager.getInstance().removeNotification(FuelRoutingManager.LOW_FUEL_ID);
        this.fuelGlanceDismissTime = 0L;
    }
    
    public void dismissGasRoute() {
        FuelRoutingManager.sLogger.v("dismissGasRoute");
        this.reset(this.currentLowFuelThreshold /= 2);
        this.fuelGlanceDismissTime = SystemClock.elapsedRealtime();
    }
    
    public void findGasStations(final int n, final boolean b) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                if (!FuelRoutingManager.this.available) {
                    FuelRoutingManager.sLogger.w("Fuel manager not available");
                }
                else {
                    FuelRoutingManager.this.isCalculatingFuelRoute = true;
                    FuelRoutingManager.this.calculateGasStationRoutes((OnRouteToGasStationCallback)new OnRouteToGasStationCallback() {
                        @Override
                        public void onError(final Error error) {
                            TaskManager.getInstance().execute(new Runnable() {
                                @Override
                                public void run() {
                                    FuelRoutingManager.sLogger.w("received an error on calculateGasStationRoutes " + error.name());
                                    FuelRoutingManager.this.isCalculatingFuelRoute = false;
                                    if (b) {
                                        final ArrayList<KeyValue> list = new ArrayList<KeyValue>(1);
                                        list.add(new KeyValue(FuelConstants.FUEL_LEVEL.name(), String.valueOf(n)));
                                        list.add(new KeyValue(FuelConstants.NO_ROUTE.name(), ""));
                                        FuelRoutingManager.this.bus.post(new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_FUEL).id("low#fuel#level").postTime(System.currentTimeMillis()).provider("com.navdy.fuel").glanceData(list).build());
                                        if (FuelRoutingManager.sLogger.isLoggable(2)) {
                                            FuelRoutingManager.sLogger.v("posting low fuel glance with no routes");
                                        }
                                        FuelRoutingManager.this.reset();
                                    }
                                }
                            }, 21);
                        }
                        
                        @Override
                        public void onOptimalRouteCalculationComplete(final RouteCacheItem routeCacheItem, final ArrayList<NavigationRouteResult> list, final NavigationRouteRequest navigationRouteRequest) {
                            TaskManager.getInstance().execute(new Runnable() {
                                @Override
                                public void run() {
                                    FuelRoutingManager.this.currentGasStation = routeCacheItem.gasStation;
                                    FuelRoutingManager.this.isCalculatingFuelRoute = false;
                                    FuelRoutingManager.this.currentOutgoingResults = list;
                                    FuelRoutingManager.this.currentNavigationRouteRequest = navigationRouteRequest;
                                    final double n = Math.round(routeCacheItem.gasStation.getLocation().getCoordinate().distanceTo(FuelRoutingManager.this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate()) * 10.0 / 1609.34) / 10.0;
                                    final String s = routeCacheItem.gasStation.getLocation().getAddress().getText().split("<br/>")[0];
                                    final ArrayList<KeyValue> list = new ArrayList<KeyValue>(1);
                                    if (n != -1) {
                                        list.add(new KeyValue(FuelConstants.FUEL_LEVEL.name(), String.valueOf(FuelRoutingManager.this.obdManager.getFuelLevel())));
                                    }
                                    list.add(new KeyValue(FuelConstants.GAS_STATION_NAME.name(), routeCacheItem.gasStation.getName()));
                                    list.add(new KeyValue(FuelConstants.GAS_STATION_ADDRESS.name(), s));
                                    list.add(new KeyValue(FuelConstants.GAS_STATION_DISTANCE.name(), String.valueOf(n)));
                                    FuelRoutingManager.this.bus.post(new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_FUEL).id("low#fuel#level").postTime(System.currentTimeMillis()).provider("com.navdy.fuel").glanceData(list).build());
                                    if (FuelRoutingManager.sLogger.isLoggable(2)) {
                                        FuelRoutingManager.sLogger.v("state is now LOW_FUEL, posting low fuel glance");
                                    }
                                }
                            }, 21);
                        }
                    });
                }
            }
        }, 21);
    }
    
    public void findNearestGasStation(final OnNearestGasStationCallback onNearestGasStationCallback) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                if (!FuelRoutingManager.this.available) {
                    FuelRoutingManager.sLogger.w("Fuel manager not available");
                }
                else {
                    if (onNearestGasStationCallback == null) {
                        throw new IllegalArgumentException();
                    }
                    if (FuelRoutingManager.this.isCalculatingFuelRoute) {
                        onNearestGasStationCallback.onError(OnRouteToGasStationCallback.Error.INVALID_STATE);
                    }
                    else {
                        FuelRoutingManager.this.isCalculatingFuelRoute = true;
                        FuelRoutingManager.this.calculateGasStationRoutes((OnRouteToGasStationCallback)new OnRouteToGasStationCallback() {
                            @Override
                            public void onError(final OnRouteToGasStationCallback.Error error) {
                                onNearestGasStationCallback.onError(OnRouteToGasStationCallback.Error.RESPONSE_ERROR);
                            }
                            
                            @Override
                            public void onOptimalRouteCalculationComplete(final RouteCacheItem routeCacheItem, final ArrayList<NavigationRouteResult> list, final NavigationRouteRequest navigationRouteRequest) {
                                onNearestGasStationCallback.onComplete(list.get(0));
                            }
                        });
                    }
                }
            }
        }, 21);
    }
    
    public Place getCurrentGasStation() {
        return this.currentGasStation;
    }
    
    public long getFuelGlanceDismissTime() {
        return this.fuelGlanceDismissTime;
    }
    
    public boolean isAvailable() {
        return this.available;
    }
    
    public boolean isBusy() {
        return this.isCalculatingFuelRoute || this.hereNavigationManager.isOnGasRoute() || NotificationManager.getInstance().isNotificationPresent(FuelRoutingManager.LOW_FUEL_ID);
    }
    
    public void markAvailable() {
        if (!this.available) {
            this.available = true;
            this.hereNavigationManager = HereNavigationManager.getInstance();
            this.hereRouteCalculator = new HereRouteCalculator(FuelRoutingManager.sLogger, false);
            (this.gasRouteOptions = new RouteOptions()).setRouteCount(1);
            this.gasRouteOptions.setTransportMode(RouteOptions.TransportMode.CAR);
            this.gasRouteOptions.setRouteType(RouteOptions.Type.FASTEST);
            this.bus.register(this);
            this.handler.postDelayed((Runnable)new Runnable() {
                @Override
                public void run() {
                    FuelRoutingManager.this.checkFuelLevel();
                }
            }, 20000L);
            FuelRoutingManager.sLogger.v("mark available");
        }
    }
    
    @Subscribe
    public void onClearTestObdLowFuelLevel(final ClearTestObdLowFuelLevel clearTestObdLowFuelLevel) {
        this.reset();
        this.fuelGlanceDismissTime = 0L;
    }
    
    @Subscribe
    public void onFuelAddedTestEvent(final FuelAddedTestEvent fuelAddedTestEvent) {
        this.setFuelLevelBackToNormal();
    }
    
    @Subscribe
    public void onNewRouteAdded(final MapEvents$NewRouteAdded newRouteAdded) {
        if (FuelRoutingManager.sLogger.isLoggable(2)) {
            FuelRoutingManager.sLogger.v("new route added, resetting gas routes");
        }
        final NotificationManager instance = NotificationManager.getInstance();
        if (newRouteAdded.rerouteReason == null && instance.isNotificationPresent(FuelRoutingManager.LOW_FUEL_ID)) {
            instance.removeNotification(FuelRoutingManager.LOW_FUEL_ID);
        }
    }
    
    @Subscribe
    public void onObdPidChangeEvent(final ObdManager.ObdPidChangeEvent obdPidChangeEvent) {
        if (obdPidChangeEvent.pid.getId() == 47) {
            this.checkFuelLevel();
        }
    }
    
    @Subscribe
    public void onTestObdLowFuelLevel(final TestObdLowFuelLevel testObdLowFuelLevel) {
        if (!this.isInTestMode) {
            this.isInTestMode = true;
            this.checkFuelLevel();
        }
    }
    
    public void reset() {
        FuelRoutingManager.sLogger.v("reset");
        this.reset(15);
    }
    
    public void routeToGasStation() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                FuelRoutingManager.sLogger.v("routeToGasStation");
                if (!FuelRoutingManager.this.available) {
                    FuelRoutingManager.sLogger.w("Fuel manager not available");
                }
                else if (FuelRoutingManager.this.currentOutgoingResults == null || FuelRoutingManager.this.currentOutgoingResults.size() == 0) {
                    FuelRoutingManager.sLogger.w("outgoing results not available");
                    FuelRoutingManager.this.reset();
                }
                else if (FuelRoutingManager.this.hereNavigationManager.isNavigationModeOn()) {
                    FuelRoutingManager.sLogger.v("navon: saving current non-gas route to storage");
                    long n;
                    if (FuelRoutingManager.this.isInTestMode) {
                        n = 20L;
                    }
                    else {
                        n = -1L;
                    }
                    if (FuelRoutingManager.this.hereNavigationManager.hasArrived()) {
                        FuelRoutingManager.sLogger.v("navon: already arrived, set ignore flag");
                        FuelRoutingManager.this.hereNavigationManager.setIgnoreArrived(true);
                    }
                    final NavigationManager.Error addNewRoute = FuelRoutingManager.this.hereNavigationManager.addNewRoute(FuelRoutingManager.this.currentOutgoingResults, NavigationSessionRouteChange.RerouteReason.NAV_SESSION_FUEL_REROUTE, FuelRoutingManager.this.currentNavigationRouteRequest, n);
                    if (addNewRoute == NavigationManager.Error.NONE) {
                        FuelRoutingManager.sLogger.v("navon:gas route navigation started");
                    }
                    else {
                        FuelRoutingManager.sLogger.e("navon:arrival route navigation started failed:" + addNewRoute);
                        FuelRoutingManager.this.reset();
                        NotificationManager.getInstance().removeNotification(FuelRoutingManager.LOW_FUEL_ID);
                    }
                }
                else {
                    final NavigationRouteResult navigationRouteResult = FuelRoutingManager.this.currentOutgoingResults.get(0);
                    if (HereRouteCache.getInstance().getRoute(navigationRouteResult.routeId) == null) {
                        FuelRoutingManager.sLogger.w("navoff:route not available in cache:" + navigationRouteResult.routeId);
                        FuelRoutingManager.this.reset();
                    }
                    else {
                        FuelRoutingManager.this.hereNavigationManager.handleNavigationSessionRequest(new NavigationSessionRequest(NavigationSessionState.NAV_SESSION_STARTED, navigationRouteResult.label, navigationRouteResult.routeId, 0, true));
                        NotificationManager.getInstance().removeNotification(FuelRoutingManager.LOW_FUEL_ID);
                        FuelRoutingManager.sLogger.v("navoff:gas route navigation started");
                    }
                }
            }
        }, 21);
    }
    
    public void showFindingGasStationToast() {
        final Resources resources = HudApplication.getAppContext().getResources();
        final ToastManager instance = ToastManager.getInstance();
        instance.dismissCurrentToast("toast#find#gas");
        instance.clearPendingToast("toast#find#gas");
        final Bundle bundle = new Bundle();
        bundle.putInt("13", 1500);
        bundle.putInt("8", R.drawable.icon_glance_fuel_low);
        bundle.putString("4", resources.getString(R.string.search_gas_station));
        bundle.putInt("5", R.style.Glances_1);
        instance.addToast(new ToastManager$ToastParams("toast#find#gas", bundle, null, true, false, false));
    }
    
    public static class ClearTestObdLowFuelLevel
    {
    }
    
    public static class FuelAddedTestEvent
    {
    }
    
    private class FuelRoutingAction implements Runnable
    {
        private int currentRouteCacheSize;
        private final GeoCoordinate endPoint;
        private final OnRouteToGasStationCallback onRouteToGasStationCallback;
        private final Place place;
        private final List<RouteCacheItem> routeCache;
        private final Queue<FuelRoutingAction> routingQueue;
        private final GeoCoordinate routingStart;
        private final List<GeoCoordinate> waypoints;
        
        public FuelRoutingAction(final int currentRouteCacheSize, final Place place, final Queue<FuelRoutingAction> routingQueue, final List<RouteCacheItem> routeCache, final OnRouteToGasStationCallback onRouteToGasStationCallback, final GeoCoordinate routingStart, final List<GeoCoordinate> waypoints, final GeoCoordinate endPoint) {
            this.place = place;
            this.routingQueue = routingQueue;
            this.routeCache = routeCache;
            this.onRouteToGasStationCallback = onRouteToGasStationCallback;
            this.routingStart = routingStart;
            this.waypoints = waypoints;
            this.endPoint = endPoint;
            this.currentRouteCacheSize = currentRouteCacheSize;
        }
        
        @Override
        public void run() {
            if (FuelRoutingManager.this.isCalculatingFuelRoute) {
                FuelRoutingManager.this.hereRouteCalculator.calculateRoute(null, this.routingStart, this.waypoints, this.endPoint, false, (HereRouteCalculator$RouteCalculatorListener)new HereRouteCalculator$RouteCalculatorListener() {
                    @Override
                    public void error(final RoutingError routingError, final Throwable t) {
                        FuelRoutingManager.sLogger.w("Calculated route to " + FuelRoutingAction.this.place.getName() + " with error " + routingError.name());
                        --FuelRoutingAction.this.currentRouteCacheSize;
                        if (FuelRoutingAction.this.routeCache.size() == FuelRoutingAction.this.currentRouteCacheSize) {
                            FuelRoutingManager.this.calculateOptimalGasStation(FuelRoutingAction.this.routeCache, FuelRoutingAction.this.onRouteToGasStationCallback);
                        }
                        else {
                            FuelRoutingManager.this.handler.post((Runnable)FuelRoutingAction.this.routingQueue.poll());
                        }
                    }
                    
                    @Override
                    public void postSuccess(final ArrayList<NavigationRouteResult> list) {
                        if (FuelRoutingManager.sLogger.isLoggable(2)) {
                            FuelRoutingManager.sLogger.v("Calculated route to " + FuelRoutingAction.this.place.getName() + " with no errors");
                        }
                        FuelRoutingAction.this.routeCache.add(new RouteCacheItem(list.get(0), FuelRoutingAction.this.place));
                        if (FuelRoutingAction.this.routeCache.size() == FuelRoutingAction.this.currentRouteCacheSize) {
                            FuelRoutingManager.this.calculateOptimalGasStation(FuelRoutingAction.this.routeCache, FuelRoutingAction.this.onRouteToGasStationCallback);
                        }
                        else {
                            FuelRoutingManager.this.handler.post((Runnable)FuelRoutingAction.this.routingQueue.poll());
                        }
                    }
                    
                    @Override
                    public void preSuccess() {
                    }
                    
                    @Override
                    public void progress(final int n) {
                        if (FuelRoutingManager.sLogger.isLoggable(2)) {
                            FuelRoutingManager.sLogger.v("Calculating route to " + FuelRoutingAction.this.place.getName() + "; progress %: " + n);
                        }
                    }
                }, 1, FuelRoutingManager.this.gasRouteOptions, false, false, false);
            }
        }
    }
    
    public interface OnNearestGasStationCallback
    {
        void onComplete(final NavigationRouteResult p0);
        
        void onError(final OnRouteToGasStationCallback.Error p0);
    }
    
    public interface OnRouteToGasStationCallback
    {
        void onError(final OnRouteToGasStationCallback.Error p0);
        
        void onOptimalRouteCalculationComplete(final RouteCacheItem p0, final ArrayList<NavigationRouteResult> p1, final NavigationRouteRequest p2);

        public enum Error {
            BAD_REQUEST(0),
            RESPONSE_ERROR(1),
            NO_USER_LOCATION(2),
            NO_ROUTES(3),
            UNKNOWN_ERROR(4),
            INVALID_STATE(5);

            private int value;
            Error(int value) {
                this.value = value;
            }
            public int getValue() {
                return value;
            }
        }
    }
    
    private static class RouteCacheItem
    {
        Place gasStation;
        NavigationRouteResult route;
        
        public RouteCacheItem(final NavigationRouteResult route, final Place gasStation) {
            this.route = route;
            this.gasStation = gasStation;
        }
    }
    
    public static class TestObdLowFuelLevel
    {
    }
}
