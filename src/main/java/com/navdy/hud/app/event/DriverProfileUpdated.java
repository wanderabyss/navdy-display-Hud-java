package com.navdy.hud.app.event;

public class DriverProfileUpdated {
    public State state;

    public enum State {
        UP_TO_DATE(0),
        UPDATED(1);

        private int value;
        State(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    public DriverProfileUpdated(State state) {
        this.state = state;
    }
}
