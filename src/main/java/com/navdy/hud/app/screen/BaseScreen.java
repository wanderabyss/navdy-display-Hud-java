package com.navdy.hud.app.screen;

import android.os.Bundle;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import flow.Flow.Direction;
import java.lang.reflect.Field;
import mortar.Blueprint;
import mortar.Mortar;
import mortar.MortarScope;

public abstract class BaseScreen implements Blueprint {
    public static final int ANIMATION_NONE = -1;
    private static final Logger sLogger = new Logger(BaseScreen.class);
    protected Bundle arguments;
    protected Object arguments2;

    public abstract Screen getScreen();

    public int getAnimationIn(Direction direction) {
        return direction == Direction.FORWARD ? R.anim.slide_in_down : R.anim.slide_in_up;
    }

    public int getAnimationOut(Direction direction) {
        return direction == Direction.FORWARD ? R.anim.slide_out_down : R.anim.slide_out_up;
    }

    public Bundle getArguments() {
        return this.arguments;
    }

    public Object getArguments2() {
        return this.arguments2;
    }

    public void setArguments(Bundle bundle, Object arg) {
        this.arguments = bundle;
        this.arguments2 = arg;
        updateInstanceState();
    }

    public void onAnimationInStart() {
    }

    public void onAnimationInEnd() {
    }

    public void onAnimationOutStart() {
    }

    public void onAnimationOutEnd() {
    }

    private void updateInstanceState() {
        MortarScope scope = Mortar.getScope(HudApplication.getAppContext()).findChild(Main.class.getName());
        if (scope != null) {
            MortarScope screenScope = scope.requireChild(this);
            Bundle latestState = getInstanceState(screenScope);
            if (latestState == null) {
                latestState = new Bundle();
                setInstanceState(screenScope, latestState);
            }
            latestState.putBundle(getMortarScopeName() + "$Presenter", this.arguments);
        }
    }

    private Bundle getInstanceState(MortarScope scope) {
        try {
            Field field = scope.getClass().getDeclaredField("latestSavedInstanceState");
            field.setAccessible(true);
            return (Bundle) field.get(scope);
        } catch (Throwable e) {
            sLogger.e("failed to get screen instance state", e);
            return null;
        }
    }

    private void setInstanceState(MortarScope scope, Bundle bundle) {
        try {
            Field field = scope.getClass().getDeclaredField("latestSavedInstanceState");
            field.setAccessible(true);
            field.set(scope, bundle);
        } catch (Throwable e) {
            sLogger.e("failed to set screen instance state", e);
        }
    }
}
