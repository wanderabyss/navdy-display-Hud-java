package com.navdy.hud.app.screen;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;

import com.navdy.hud.app.R;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.dial.DialConstants.DialManagerInitEvent;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.hud.app.view.FirstLaunchView;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import flow.Flow.Direction;
import flow.Layout;
import javax.inject.Inject;
import javax.inject.Singleton;

@Layout(R.layout.screen_first_launch)
public class FirstLaunchScreen extends BaseScreen {
    private static final Logger sLogger = new Logger(FirstLaunchScreen.class);

    private static class MediaServerUp {
        private MediaServerUp() {
        }
    }

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {FirstLaunchView.class})
    public class Module {
    }

    @Singleton
    public static class Presenter extends BasePresenter<FirstLaunchView> {
        @Inject
        Bus bus;
        private DialManager dialManager;
        @Inject
        PowerManager powerManager;
        private boolean registered;
        private boolean stateChecked;
        @Inject
        UIStateManager uiStateManager;

        public void onLoad(Bundle savedInstanceState) {
            FirstLaunchScreen.sLogger.v("onLoad");
            this.bus.register(this);
            this.registered = true;
            this.dialManager = DialManager.getInstance();
            if (this.dialManager.isInitialized()) {
                FirstLaunchScreen.sLogger.v("already inititalized");
                checkState();
            } else {
                FirstLaunchScreen.sLogger.v("show view");
            }
            this.uiStateManager.enableSystemTray(false);
            this.uiStateManager.enableNotificationColor(false);
            FirstLaunchScreen.sLogger.v("systemtray:invisible");
        }

        protected void onUnload() {
            FirstLaunchScreen.sLogger.v("onUnload");
            if (this.registered) {
                this.bus.unregister(this);
            }
            super.onUnload();
            this.uiStateManager.enableSystemTray(true);
            this.uiStateManager.enableNotificationColor(true);
            FirstLaunchScreen.sLogger.v("systemtray:visible");
        }

        @Subscribe
        public void onDialManagerInitEvent(DialManagerInitEvent event) {
            FirstLaunchScreen.sLogger.v("got dialmanager init event");
            if (this.registered) {
                FirstLaunchView view = (FirstLaunchView) getView();
                if (view != null) {
                    view.firstLaunchLogo.animate().translationY((float) ToastPresenter.ANIMATION_TRANSLATION).alpha(0.0f).withEndAction(new Runnable() {
                        public void run() {
                            Presenter.this.checkState();
                        }
                    }).start();
                    return;
                } else {
                    checkState();
                    return;
                }
            }
            checkState();
        }

        @Subscribe
        public void onMediaServerUp(MediaServerUp event) {
            FirstLaunchScreen.sLogger.v("media service is up, stop boot-animation");
            launchDialPairing();
        }

        private void checkState() {
            if (this.stateChecked) {
                FirstLaunchScreen.sLogger.v("state already checked");
                return;
            }
            this.stateChecked = true;
            if (this.powerManager.inQuietMode()) {
                SystemProperties.set("service.bootanim.exit", ToastPresenter.EXTRA_MAIN_TITLE);
                return;
            }
            int n = this.dialManager.getBondedDialCount();
            if (n > 0) {
                FirstLaunchScreen.sLogger.v("bonded dial count=" + n + " , go to next screen, stop boot animation");
                SystemProperties.set("service.bootanim.exit", ToastPresenter.EXTRA_MAIN_TITLE);
                exitScreen();
                return;
            }
            FirstLaunchScreen.sLogger.v("checkForMediaService");
            checkForMediaService();
        }

        private void launchDialPairing() {
            SystemProperties.set("service.bootanim.exit", ToastPresenter.EXTRA_MAIN_TITLE);
            FirstLaunchScreen.sLogger.v("bonded dial count=" + this.dialManager.getBondedDialCount() + " , go to dial pairing screen");
            this.bus.post(new Builder().screen(Screen.SCREEN_DIAL_PAIRING).build());
        }

        private void exitScreen() {
            if (RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                FirstLaunchScreen.sLogger.v("go to default screen");
                this.bus.post(new Builder().screen(RemoteDeviceManager.getInstance().getUiStateManager().getDefaultMainActiveScreen()).build());
                return;
            }
            FirstLaunchScreen.sLogger.v("go to welcome screen");
            Bundle args = new Bundle();
            args.putString(WelcomeScreen.ARG_ACTION, WelcomeScreen.ACTION_RECONNECT);
            this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_WELCOME, args, false));
        }
        private void checkForMediaService() {
            final Presenter a = this;
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable) new Runnable() {
                final Presenter this$0 = a;

                public void run() {
                    int i = 1;
                    while(true) {
                        int i0 = 0;
                        Throwable a;
                        MediaPlayer a0;
                        label1: {
                            long j;
                            Logger a1;
                            StringBuilder a2;
                            label3: {
                                try {
                                    i0 = i;
                                    j = SystemClock.elapsedRealtime();
                                    a1 = sLogger;
                                    a2 = new StringBuilder().append("creating media player counter=");
                                    i0 = i + 1;
                                    break label3;
                                } catch(Throwable a3) {
                                    a = a3;
                                }
                                a0 = null;
                                break label1;
                            }
                            try {
                                a1.v(a2.append(i).toString());
                                a0 = new MediaPlayer();
                                label2: {
                                    try {
                                        a0.release();
                                        break label2;
                                    } catch(Throwable a4) {
                                        a = a4;
                                    }
                                    break label1;
                                }
                                sLogger.v("created media player time=" + (SystemClock.elapsedRealtime() - j));
                                this.this$0.bus.post(new MediaServerUp());
                            } catch(Throwable a5) {
                                a0 = null;
                                a = a5;
                                break label1;
                            }
                            return;
                        }
                        sLogger.e("checkForMediaService", a);
                        label0: {
                            Throwable a6 = null;
                            if (a0 == null) {
                                i = i0;
                                break label0;
                            } else {
                                try {
                                    i = i0;
                                    a0.release();
                                    i = i0;
                                    break label0;
                                } catch(Throwable a7) {
                                    a6 = a7;
                                }
                            }
                            sLogger.e(a6);
                        }
                        GenericUtil.sleep(1000);
                    }
                }
            }, 1);
        }
        /*
        private void checkForMediaService() {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    Throwable t;
                    int counter = 1;
                    MediaPlayer mediaPlayer = null;
                    try {
                        long l1 = SystemClock.elapsedRealtime();
                        int counter2 = counter + 1;
                        try {
                            FirstLaunchScreen.sLogger.v("creating media player counter=" + counter);
                            MediaPlayer mediaPlayer2 = new MediaPlayer();
                            try {
                                mediaPlayer2.release();
                                mediaPlayer = null;
                                FirstLaunchScreen.sLogger.v("created media player time=" + (SystemClock.elapsedRealtime() - l1));
                                Presenter.this.bus.post(new MediaServerUp());
                                return;
                            } catch (Throwable th) {
                                t = th;
                                mediaPlayer = mediaPlayer2;
                                counter = counter2;
                                FirstLaunchScreen.sLogger.e("checkForMediaService", t);
                                if (mediaPlayer != null) {
                                    try {
                                        mediaPlayer.release();
                                    } catch (Throwable t1) {
                                        FirstLaunchScreen.sLogger.e(t1);
                                    }
                                }
                                GenericUtil.sleep(1000);
                            }
                        } catch (Throwable th2) {
                            t = th2;
                            counter = counter2;
                        }
                    } catch (Throwable th3) {
                        t = th3;
                        FirstLaunchScreen.sLogger.e("checkForMediaService", t);
                        if (mediaPlayer != null) {
                            mediaPlayer.release();
                        }
                        GenericUtil.sleep(1000);
                    }
                    GenericUtil.sleep(1000);
                }
            }, 1);
        }
        */
    }

    public Screen getScreen() {
        return Screen.SCREEN_FIRST_LAUNCH;
    }

    public String getMortarScopeName() {
        return getClass().getName();
    }

    public Object getDaggerModule() {
        return new Module();
    }

    public int getAnimationIn(Direction direction) {
        return -1;
    }

    public int getAnimationOut(Direction direction) {
        return -1;
    }
}
