package com.navdy.hud.app.analytics;


    public enum AnalyticsSupport$VoiceSearchCancelReason {
        end_trip(0),
        new_voice_search(1),
        new_non_voice_search(2),
        cancel_search(3),
        cancel_route_calculation(4),
        cancel_list(5);

        private int value;
        AnalyticsSupport$VoiceSearchCancelReason(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class AnalyticsSupport$VoiceSearchCancelReason extends Enum {
//    final private static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason[] $VALUES;
//    final public static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason cancel_list;
//    final public static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason cancel_route_calculation;
//    final public static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason cancel_search;
//    final public static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason end_trip;
//    final public static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason new_non_voice_search;
//    final public static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason new_voice_search;
//    
//    static {
//        end_trip = new com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason("end_trip", 0);
//        new_voice_search = new com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason("new_voice_search", 1);
//        new_non_voice_search = new com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason("new_non_voice_search", 2);
//        cancel_search = new com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason("cancel_search", 3);
//        cancel_route_calculation = new com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason("cancel_route_calculation", 4);
//        cancel_list = new com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason("cancel_list", 5);
//        com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason[] a = new com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason[6];
//        a[0] = end_trip;
//        a[1] = new_voice_search;
//        a[2] = new_non_voice_search;
//        a[3] = cancel_search;
//        a[4] = cancel_route_calculation;
//        a[5] = cancel_list;
//        $VALUES = a;
//    }
//    
//    private AnalyticsSupport$VoiceSearchCancelReason(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason valueOf(String s) {
//        return (com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason)Enum.valueOf(com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason.class, s);
//    }
//    
//    public static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason[] values() {
//        return $VALUES.clone();
//    }
//}
//