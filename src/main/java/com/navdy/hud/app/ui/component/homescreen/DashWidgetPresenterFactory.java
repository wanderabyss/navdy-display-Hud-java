package com.navdy.hud.app.ui.component.homescreen;

import com.navdy.hud.app.R;
import com.navdy.hud.app.view.DriveScoreGaugePresenter;
import com.navdy.hud.app.view.EngineTemperaturePresenter;
import com.navdy.hud.app.view.GForcePresenter;
import com.navdy.hud.app.view.SpeedLimitSignPresenter;
import com.navdy.hud.app.view.MusicWidgetPresenter;
import com.navdy.hud.app.view.MPGGaugePresenter;
import com.navdy.hud.app.view.ETAGaugePresenter;
import com.navdy.hud.app.view.CalendarWidgetPresenter;
import com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter;
import com.navdy.hud.app.view.ClockWidgetPresenter;
import com.navdy.hud.app.view.CompassPresenter;
import com.navdy.hud.app.view.FuelGaugePresenter2;
import com.navdy.hud.app.view.EmptyGaugePresenter;
import com.navdy.hud.app.view.DashboardWidgetPresenter;
import android.content.Context;
import java.util.HashMap;

import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ANALOG_CLOCK_WIDGET_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.CALENDAR_WIDGET_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.COMPASS_WIDGET_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.DIGITAL_CLOCK_2_WIDGET_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.DIGITAL_CLOCK_WIDGET_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.DRIVE_SCORE_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.EMPTY_WIDGET_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ETA_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.FUEL_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.GFORCE_WIDGET_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.MPG_AVG_WIDGET_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.MPG_GRAPH_WIDGET_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.MUSIC_WIDGET_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SPEED_LIMIT_SIGN_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.TRAFFIC_INCIDENT_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.WEATHER_WIDGET_ID;

public class DashWidgetPresenterFactory
{
    private static final HashMap<String, Integer> RESOURCE_MAP;

    static {
        (RESOURCE_MAP = new HashMap<>()).put(EMPTY_WIDGET_ID, 0);
        DashWidgetPresenterFactory.RESOURCE_MAP.put(MPG_GRAPH_WIDGET_ID, R.drawable.asset_dash20_proto_gauge_mpg);
        DashWidgetPresenterFactory.RESOURCE_MAP.put(MPG_AVG_WIDGET_ID, R.drawable.asset_dash20_proto_gauge_clock_analog);
        DashWidgetPresenterFactory.RESOURCE_MAP.put(WEATHER_WIDGET_ID, R.drawable.asset_dash20_proto_gauge_weather);
    }

    public static DashboardWidgetPresenter createDashWidgetPresenter(final Context context, final String s) {
        DashboardWidgetPresenter dashboardWidgetPresenter;
        switch (s) {
            default:
                dashboardWidgetPresenter = new ImageWidgetPresenter(context, DashWidgetPresenterFactory.RESOURCE_MAP.get(s), s);
                break;
            case EMPTY_WIDGET_ID:
                dashboardWidgetPresenter = new EmptyGaugePresenter(context);
                break;
            case FUEL_GAUGE_ID:
                dashboardWidgetPresenter = new FuelGaugePresenter2(context);
                break;
            case COMPASS_WIDGET_ID:
                dashboardWidgetPresenter = new CompassPresenter(context);
                break;
            case ANALOG_CLOCK_WIDGET_ID:
                dashboardWidgetPresenter = new ClockWidgetPresenter(context, ClockWidgetPresenter.ClockType.ANALOG);
                break;
            case DIGITAL_CLOCK_WIDGET_ID:
                dashboardWidgetPresenter = new ClockWidgetPresenter(context, ClockWidgetPresenter.ClockType.DIGITAL1);
                break;
            case DIGITAL_CLOCK_2_WIDGET_ID:
                dashboardWidgetPresenter = new ClockWidgetPresenter(context, ClockWidgetPresenter.ClockType.DIGITAL2);
                break;
            case TRAFFIC_INCIDENT_GAUGE_ID:
                dashboardWidgetPresenter = new TrafficIncidentWidgetPresenter(context);
                break;
            case CALENDAR_WIDGET_ID:
                dashboardWidgetPresenter = new CalendarWidgetPresenter(context);
                break;
            case ETA_GAUGE_ID:
                dashboardWidgetPresenter = new ETAGaugePresenter(context);
                break;
            case MPG_AVG_WIDGET_ID:
                dashboardWidgetPresenter = new MPGGaugePresenter(context);
                break;
            case MUSIC_WIDGET_ID:
                dashboardWidgetPresenter = new MusicWidgetPresenter(context);
                break;
            case SPEED_LIMIT_SIGN_GAUGE_ID:
                dashboardWidgetPresenter = new SpeedLimitSignPresenter(context);
                break;
            case GFORCE_WIDGET_ID:
                dashboardWidgetPresenter = new GForcePresenter(context);
                break;
            case ENGINE_TEMPERATURE_GAUGE_ID:
                dashboardWidgetPresenter = new EngineTemperaturePresenter(context);
                break;
            case DRIVE_SCORE_GAUGE_ID:
                dashboardWidgetPresenter = new DriveScoreGaugePresenter(context, R.layout.drive_score_gauge_layout, true);
                break;


        }
        return dashboardWidgetPresenter;
    }
}
