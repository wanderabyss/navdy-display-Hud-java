package com.navdy.hud.app.ui.component.mainmenu;

class PlacesMenu$4 {
    final static int[] $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType;
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel;
    
    static {
        $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType = new int[com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType;
        com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType a0 = com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.FAVORITE_HOME;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType[com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.FAVORITE_WORK.ordinal()] = 2;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType[com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.FAVORITE_CONTACT.ordinal()] = 3;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType[com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.FAVORITE_CUSTOM.ordinal()] = 4;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType[com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.FAVORITE_CALENDAR.ordinal()] = 5;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType[com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.FAVORITE_NONE.ordinal()] = 6;
        } catch(NoSuchFieldError ignored) {
        }
        $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel = new int[com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel;
        com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel a2 = com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel.CLOSE;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignored) {
        }
    }
}
