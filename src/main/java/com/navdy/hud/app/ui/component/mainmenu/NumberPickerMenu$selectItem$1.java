package com.navdy.hud.app.ui.component.mainmenu;

import com.navdy.hud.app.framework.contacts.Contact;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
/* compiled from: NumberPickerMenu.kt */
final class NumberPickerMenu$selectItem$1 implements Runnable {
    final /* synthetic */ int $contactIndex;
    final /* synthetic */ NumberPickerMenu this$0;

    NumberPickerMenu$selectItem$1(NumberPickerMenu numberPickerMenu, int i) {
        this.this$0 = numberPickerMenu;
        this.$contactIndex = i;
    }

    public final void run() {
        this.this$0.callback.selected((Contact) this.this$0.contacts.get(this.$contactIndex));
    }
}
