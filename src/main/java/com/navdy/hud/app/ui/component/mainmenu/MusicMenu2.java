package com.navdy.hud.app.ui.component.mainmenu;

import java.util.Set;

import com.navdy.hud.app.R;
import com.navdy.service.library.util.ScalingUtilities$ScalingLogic;
import com.squareup.otto.Subscribe;
import android.support.annotation.Nullable;
import okio.ByteString;

import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.service.library.events.audio.MusicEvent;
import com.navdy.service.library.events.audio.MusicShuffleMode;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;
import android.support.annotation.NonNull;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder;
import android.view.View;
import com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder;
import com.squareup.wire.Wire;
import com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.ui.framework.IScreenAnimationListener;
import com.navdy.service.library.events.audio.MusicCapability;
import com.navdy.hud.app.ExtensionsKt;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.task.TaskManager;
import android.os.SystemClock;
import android.text.TextUtils;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import android.graphics.Bitmap;
import com.makeramen.RoundedTransformationBuilder;
import com.navdy.service.library.util.ScalingUtilities;
import java.util.ArrayList;
import com.navdy.service.library.events.MessageStore;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import java.util.Iterator;
import com.navdy.service.library.events.audio.MusicCharacterMap;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.events.audio.MusicArtworkResponse;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import mortar.Mortar;
import android.os.Looper;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.HudApplication;
import java.util.HashMap;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.manager.MusicManager;
import com.navdy.service.library.events.audio.MusicCollectionResponse;
import com.navdy.hud.app.storage.cache.MessageCache;
import javax.inject.Inject;
import com.navdy.hud.app.util.MusicArtworkCache;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.service.library.events.audio.MusicCollectionRequest;
import java.util.List;
import com.squareup.otto.Bus;
import android.content.res.Resources;
import com.navdy.service.library.events.audio.MusicCollectionSource;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.service.library.events.audio.MusicCollectionInfo;
import com.navdy.service.library.events.audio.MusicArtworkRequest;
import android.util.Pair;
import java.util.Stack;
import com.navdy.service.library.events.audio.MusicCollectionType;
import java.util.Map;

public class MusicMenu2 implements IMenu
{
    private static Map<MusicCollectionType, String> analyticsTypeStringMap;
    private static int androidBgColor;
    private static int appleMusicBgColor;
    private static int applePodcastsBgColor;
    private static final Object artworkRequestLock;
    private static Stack<Pair<MusicArtworkRequest, MusicCollectionInfo>> artworkRequestQueue;
    private static int artworkSize;
    private static VerticalList.Model back;
    private static int backColor;
    private static int bgColorUnselected;
    private static BusDelegate busDelegate;
    private static MusicCollectionInfo infoForCurrentArtworkRequest;
    private static Map<String, MusicMenuData> lastPlayedMusicMenuData;
    private static final Logger logger;
    private static Map<MusicCollectionSource, Integer> menuSourceIconMap;
    private static Map<MusicCollectionSource, String> menuSourceStringMap;
    private static Map<MusicCollectionType, Integer> menuTypeIconMap;
    private static Map<MusicCollectionType, String> menuTypeStringMap;
    private static int mmMusicColor;
    private static Resources resources;
    private static String shuffle;
    private static String shuffleOff;
    private static String shuffleOn;
    private static String shufflePlay;
    private static int sourceBgColor;
    private static int transparentColor;
    private boolean anyPlayersPermitted;
    private long artWorkRequestTime;
    private int backSelection;
    private int backSelectionId;
    private int bgColorSelected;
    private Bus bus;
    private List<VerticalList.Model> cachedList;
    private MusicCollectionRequest currentMusicCollectionRequest;
    private long currentMusicCollectionRequestTime;
    private VerticalFastScrollIndex fastScrollIndex;
    private Handler handler;
    private int highlightedItem;
    private boolean initialized;
    private boolean isRequestingMusicCollection;
    private boolean isShuffling;
    private final IndexOffset loadingOffset;
    private MusicMenuData menuData;
    private AnimationDrawable musicAnimation;
    @Inject
    MusicArtworkCache musicArtworkCache;
    @Inject
    MessageCache<MusicCollectionResponse> musicCollectionResponseMessageCache;
    private boolean musicCollectionSyncComplete;
    private final MusicManager musicManager;
    private int nextRequestLimit;
    private int nextRequestOffset;
    private int nowPlayingTrackPosition;
    private IMenu parentMenu;
    private boolean partialRefresh;
    private String path;
    private MainMenuScreen2.Presenter presenter;
    private int requestMusicOffset;
    private Map<String, Integer> requestedArtworkModelPositions;
    private List<VerticalList.Model> returnToCacheList;
    private Map<String, Integer> trackIdToPositionMap;
    private VerticalMenuComponent vmenuComponent;

    static {
        logger = new Logger(MusicMenu2.class);
        MusicMenu2.lastPlayedMusicMenuData = new HashMap<>();
        MusicMenu2.menuSourceStringMap = new HashMap<>();
        MusicMenu2.menuSourceIconMap = new HashMap<>();
        MusicMenu2.menuTypeStringMap = new HashMap<>();
        MusicMenu2.menuTypeIconMap = new HashMap<>();
        MusicMenu2.artworkRequestQueue = new Stack<>();
        MusicMenu2.infoForCurrentArtworkRequest = null;
        artworkRequestLock = new Object();
        MusicMenu2.analyticsTypeStringMap = new HashMap<>();
        MusicMenu2.resources = HudApplication.getAppContext().getResources();
        MusicMenu2.menuSourceStringMap.put(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL, MusicMenu2.resources.getString(R.string.local_music));
        MusicMenu2.menuSourceStringMap.put(MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER, MusicMenu2.resources.getString(R.string.music_library));
        MusicMenu2.menuSourceIconMap.put(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL, R.drawable.icon_google_pm);
        MusicMenu2.menuSourceIconMap.put(MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER, R.drawable.icon_apple_music);
        MusicMenu2.menuTypeStringMap.put(MusicCollectionType.COLLECTION_TYPE_ALBUMS, MusicMenu2.resources.getString(R.string.albums));
        MusicMenu2.menuTypeStringMap.put(MusicCollectionType.COLLECTION_TYPE_ARTISTS, MusicMenu2.resources.getString(R.string.artists));
        MusicMenu2.menuTypeStringMap.put(MusicCollectionType.COLLECTION_TYPE_PLAYLISTS, MusicMenu2.resources.getString(R.string.playlists));
        MusicMenu2.menuTypeStringMap.put(MusicCollectionType.COLLECTION_TYPE_PODCASTS, MusicMenu2.resources.getString(R.string.podcasts));
        MusicMenu2.menuTypeIconMap.put(MusicCollectionType.COLLECTION_TYPE_ALBUMS, R.drawable.icon_album);
        MusicMenu2.menuTypeIconMap.put(MusicCollectionType.COLLECTION_TYPE_ARTISTS, R.drawable.icon_artist);
        MusicMenu2.menuTypeIconMap.put(MusicCollectionType.COLLECTION_TYPE_PLAYLISTS, R.drawable.icon_playlist);
        MusicMenu2.menuTypeIconMap.put(MusicCollectionType.COLLECTION_TYPE_PODCASTS, R.drawable.icon_podcasts);
        MusicMenu2.mmMusicColor = MusicMenu2.resources.getColor(R.color.mm_music);
        MusicMenu2.sourceBgColor = MusicMenu2.resources.getColor(R.color.hud_white);
        MusicMenu2.bgColorUnselected = MusicMenu2.resources.getColor(R.color.grey_4a);
        MusicMenu2.androidBgColor = MusicMenu2.resources.getColor(R.color.music_android_local);
        MusicMenu2.appleMusicBgColor = MusicMenu2.resources.getColor(R.color.music_apple_music);
        MusicMenu2.applePodcastsBgColor = MusicMenu2.resources.getColor(R.color.music_apple_podcasts);
        MusicMenu2.transparentColor = MusicMenu2.resources.getColor(R.color.white_half_transparent);
        MusicMenu2.artworkSize = MusicMenu2.resources.getDimensionPixelSize(R.dimen.vmenu_selected_image);
        MusicMenu2.backColor = MusicMenu2.resources.getColor(R.color.mm_back);
        MusicMenu2.back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, MusicMenu2.backColor, MusicMenu2.bgColorUnselected, MusicMenu2.backColor, MusicMenu2.resources.getString(R.string.back), null);
        MusicMenu2.shuffle = MusicMenu2.resources.getString(R.string.shuffle);
        MusicMenu2.shufflePlay = MusicMenu2.resources.getString(R.string.shuffle_play);
        MusicMenu2.shuffleOn = MusicMenu2.resources.getString(R.string.shuffle_on);
        MusicMenu2.shuffleOff = MusicMenu2.resources.getString(R.string.shuffle_off);
        MusicMenu2.analyticsTypeStringMap.put(MusicCollectionType.COLLECTION_TYPE_ALBUMS, "Album");
        MusicMenu2.analyticsTypeStringMap.put(MusicCollectionType.COLLECTION_TYPE_ARTISTS, "Artist");
        MusicMenu2.analyticsTypeStringMap.put(MusicCollectionType.COLLECTION_TYPE_PLAYLISTS, "Playlist");
        MusicMenu2.analyticsTypeStringMap.put(MusicCollectionType.COLLECTION_TYPE_PODCASTS, "Podcast");
    }

    MusicMenu2(final Bus bus, final VerticalMenuComponent vmenuComponent, final MainMenuScreen2.Presenter presenter, final IMenu parentMenu, final MusicMenuData menuData) {
        this.path = null;
        this.highlightedItem = 1;
        this.isRequestingMusicCollection = false;
        this.musicCollectionSyncComplete = false;
        this.nextRequestOffset = -1;
        this.nextRequestLimit = -1;
        this.anyPlayersPermitted = true;
        this.requestMusicOffset = -1;
        this.loadingOffset = new IndexOffset();
        this.cachedList = null;
        this.returnToCacheList = null;
        this.handler = new Handler(Looper.getMainLooper());
        this.requestedArtworkModelPositions = new HashMap<>();
        this.artWorkRequestTime = 0L;
        this.trackIdToPositionMap = null;
        this.initialized = false;
        this.isShuffling = false;
        this.bus = bus;
        this.vmenuComponent = vmenuComponent;
        this.presenter = presenter;
        this.parentMenu = parentMenu;
        if (menuData != null) {
            this.menuData = menuData;
        }
        else {
            this.menuData = new MusicMenuData();
        }
        Mortar.inject(HudApplication.getAppContext(), this);
        MusicMenu2.logger.d("MusicMenu " + this.menuData.musicCollectionInfo);
        this.musicManager = RemoteDeviceManager.getInstance().getMusicManager();
    }

    private VerticalFastScrollIndex buildIndexFromCharacterMap(final List<MusicCharacterMap> list) {
        VerticalFastScrollIndex.Builder builder;
        Label_0072: {
            VerticalFastScrollIndex build;
            try {
                builder = new VerticalFastScrollIndex.Builder();
                for (final MusicCharacterMap musicCharacterMap : list) {
                    builder.setEntry(musicCharacterMap.character.charAt(0), musicCharacterMap.offset);
                }
                break Label_0072;
            }
            catch (Throwable t) {
                MusicMenu2.logger.e("buildIndexFromCharacterMap", t);
                build = null;
            }
            return null;
        }
        builder.positionOffset(1);
        return builder.build();
    }

    private VerticalList.Model buildModelfromCollectionInfo(final int n, final MusicCollectionInfo state) {
        String s;
        if (state.subtitle != null) {
            s = state.subtitle;
        }
        else {
            int n2;
            if (state.collectionType == MusicCollectionType.COLLECTION_TYPE_PODCASTS) {
                n2 = R.plurals.episodes_count;
            }
            else {
                n2 = R.plurals.songs_count;
            }
            s = MusicMenu2.resources.getQuantityString(n2, state.size, state.size);
        }
        IconColorImageView.IconShape iconShape;
        if (state.collectionType == MusicCollectionType.COLLECTION_TYPE_ARTISTS) {
            iconShape = IconColorImageView.IconShape.CIRCLE;
        }
        else {
            iconShape = IconColorImageView.IconShape.SQUARE;
        }
        final VerticalList.Model buildModel = IconBkColorViewHolder.buildModel(n, MusicMenu2.menuTypeIconMap.get(state.collectionType), this.bgColorSelected, MusicMenu2.bgColorUnselected, MusicMenu2.transparentColor, state.name, s, null, iconShape);
        buildModel.state = state;
        return buildModel;
    }

    private void calculateOffset(int n, final boolean b) {
        final int offset = n / 25 * 25;
        if (b) {
            n = this.menuData.musicCollections.size();
        }
        else {
            n = this.menuData.musicTracks.size();
        }
        n = Math.min(n - 1, offset + 25);
        this.loadingOffset.clear();
        this.loadingOffset.offset = offset;
        this.loadingOffset.limit = n - offset + 1;
    }

    private VerticalFastScrollIndex checkCharacterMap(final List<MusicCharacterMap> list, final int n, int n2, final boolean b) {
        if (list != null) {
            n2 = 1;
        }
        else {
            n2 = 0;
        }
        int size = -1;
        if (n2 != 0) {
            size = list.size();
        }
        if (n2 == 0) {
            return null;
        }
        if (b && (n <= 40 || size < 2)) {
            MusicMenu2.logger.v("checkCharacterMap index-no created len[" + n + "] index-len[" + size + "]");
            return null;
        }
        VerticalFastScrollIndex buildIndexFromCharacterMap = this.buildIndexFromCharacterMap(list);
        if (buildIndexFromCharacterMap != null) {
            MusicMenu2.logger.v("checkCharacterMap index-yes len[" + n + "] map[" + size + "] index[" + buildIndexFromCharacterMap.getEntryCount() + "]");
        }
        else {
            MusicMenu2.logger.v("checkCharacterMap index-error len[" + n + "] map[" + size + "]");
        }
        return buildIndexFromCharacterMap;
    }

    public static void clearMenuData() {
        MusicMenu2.lastPlayedMusicMenuData.clear();
    }

    private String collectionIdString(final MusicArtworkResponse musicArtworkResponse) {
        return MusicMenu2.menuSourceStringMap.get(musicArtworkResponse.collectionSource) + " - " + MusicMenu2.menuTypeStringMap.get(musicArtworkResponse.collectionType) + " - " + musicArtworkResponse.collectionId;
    }

    private String collectionIdString(MusicCollectionInfo musicCollectionInfo) {
        String string;
        if (musicCollectionInfo == null) {
            string = null;
        }
        else {
            musicCollectionInfo = MessageStore.removeNulls(musicCollectionInfo);
            string = MusicMenu2.menuSourceStringMap.get(musicCollectionInfo.collectionSource) + " - " + MusicMenu2.menuTypeStringMap.get(musicCollectionInfo.collectionType) + " - " + musicCollectionInfo.collectionId;
        }
        return string;
    }

    private MusicMenu2 getMusicPlayerMenu(final int backSelectionId, final int backSelectionPos) {
        MusicMenu2.logger.d("getMusicPlayerMenu " + backSelectionId + ", " + backSelectionPos);
        final String subPath = this.getSubPath(backSelectionId, backSelectionPos);
        MusicMenuData musicMenuData;
        if (MusicMenu2.lastPlayedMusicMenuData.containsKey(subPath)) {
            musicMenuData = MusicMenu2.lastPlayedMusicMenuData.get(subPath);
        }
        else {
            MusicCollectionInfo musicCollectionInfo = null;
            final List<MusicCollectionInfo> list = null;
            List<MusicCollectionInfo> list2;
            if (this.menuData.musicCollectionInfo.collectionSource == null) {
                if (backSelectionId == R.id.apple_podcasts) {
                    musicCollectionInfo = new MusicCollectionInfo.Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER).collectionType(MusicCollectionType.COLLECTION_TYPE_PODCASTS).build();
                    list2 = list;
                }
                else {
                    final MusicCollectionInfo musicCollectionInfo2 = this.menuData.musicCollections.get(backSelectionId);
                    final MusicCollectionInfo build = new MusicCollectionInfo.Builder().collectionSource(musicCollectionInfo2.collectionSource).build();
                    final List<MusicCollectionType> collectionTypesForSource = this.musicManager.getCollectionTypesForSource(musicCollectionInfo2.collectionSource);
                    final ArrayList list3 = new ArrayList<MusicCollectionInfo>(collectionTypesForSource.size());
                    final Iterator<MusicCollectionType> iterator = collectionTypesForSource.iterator();
                    while (true) {
                        list2 = (List<MusicCollectionInfo>)list3;
                        musicCollectionInfo = build;
                        if (!iterator.hasNext()) {
                            break;
                        }
                        list3.add(new MusicCollectionInfo.Builder().collectionSource(musicCollectionInfo2.collectionSource).collectionType(iterator.next()).build());
                    }
                }
            }
            else if (this.menuData.musicCollectionInfo.collectionType == null) {
                musicCollectionInfo = new MusicCollectionInfo.Builder().collectionSource(this.menuData.musicCollectionInfo.collectionSource).collectionType(this.menuData.musicCollections.get(backSelectionId).collectionType).build();
                list2 = list;
            }
            else {
                list2 = list;
                if (this.menuData.musicCollections.size() > 0) {
                    musicCollectionInfo = this.menuData.musicCollections.get(backSelectionId);
                    MusicMenu2.logger.i("fetching collection " + musicCollectionInfo.collectionId);
                    list2 = list;
                }
            }
            musicMenuData = new MusicMenuData(musicCollectionInfo, list2);
        }
        final MusicMenu2 musicMenu2 = new MusicMenu2(this.bus, this.vmenuComponent, this.presenter, this, musicMenuData);
        musicMenu2.setBackSelectionPos(backSelectionPos);
        musicMenu2.setBackSelectionId(backSelectionId);
        return musicMenu2;
    }

    private VerticalList.Model getShuffleModel() {
        String s = null;
        String s2;
        if (this.isThisQueuePlaying()) {
            s2 = MusicMenu2.shuffle;
            if (this.musicManager.isShuffling()) {
                s = MusicMenu2.shuffleOn;
                this.isShuffling = true;
            }
            else {
                s = MusicMenu2.shuffleOff;
            }
        }
        else {
            s2 = MusicMenu2.shufflePlay;
        }
        return IconBkColorViewHolder.buildModel(R.id.music_browser_shuffle, R.drawable.icon_shuffle, this.bgColorSelected, MusicMenu2.bgColorUnselected, this.bgColorSelected, s2, s);
    }

    private String getSubPath(final int n, final int n2) {
        return this.getPath() + "/" + n + ":" + n2;
    }

    private void handleArtwork(final byte[] array, final Integer n, final String s, final boolean b) {
        final int artworkSize = MusicMenu2.artworkSize;
        Bitmap bitmap = ScalingUtilities.decodeByteArray(array, artworkSize, artworkSize, ScalingUtilities$ScalingLogic.FIT);
        if (MusicCollectionType.COLLECTION_TYPE_ARTISTS == this.menuData.musicCollectionInfo.collectionType) {
            bitmap = new RoundedTransformationBuilder().oval(true).build().transform(bitmap);
        }
        final Bitmap finalBitmap = bitmap;
        this.handler.post(new Runnable() {
            @Override
            public void run() {
                PicassoUtil.setBitmapInCache(s, finalBitmap);
                MusicCollectionInfo musicCollectionInfo;
                Label_0190: {
                    if (n == null) {
                        MusicMenu2.this.vmenuComponent.setSelectedIconImage(finalBitmap);
                        musicCollectionInfo = MusicMenu2.this.menuData.musicCollectionInfo;
                        break Label_0190;
                    }
                    if (MusicMenu2.this.presenter.getCurrentMenu() == MusicMenu2.this) {
                        MusicMenu2.logger.d("Refreshing artwork for position: " + n);
                        MusicMenu2.this.presenter.refreshDataforPos(n);
                    }
                    final int size = MusicMenu2.this.cachedList.size();
                    if (n < 0 || n >= size) {
                        MusicMenu2.logger.e("Refreshing artwork invalid index: " + n + " size=" + size);
                    }
                    else {
                        final Object state = MusicMenu2.this.cachedList.get(n).state;
                        if (state instanceof MusicCollectionInfo) {
                            musicCollectionInfo = (MusicCollectionInfo)state;
                            break Label_0190;
                        }
                        MusicMenu2.logger.e("Something has gone terribly wrong.");
                    }
                    return;
                }
                if (b && musicCollectionInfo != null && musicCollectionInfo.collectionType != null) {
                    MusicMenu2.logger.d("Setting bitmap in cache for collection: " + musicCollectionInfo);
                    MusicMenu2.this.musicArtworkCache.putArtwork(musicCollectionInfo, array);
                }
            }
        });
    }

    private boolean hasShuffleEntry() {
        return this.cachedList != null && this.cachedList.size() >= 2 && this.cachedList.get(1).id == R.id.music_browser_shuffle;
    }

    private boolean isIndexSupported(final MusicCollectionInfo musicCollectionInfo) {
        if (musicCollectionInfo != null && musicCollectionInfo.collectionType != null) {
            switch (musicCollectionInfo.collectionType) {
                case COLLECTION_TYPE_PLAYLISTS:
                case COLLECTION_TYPE_ARTISTS:
                case COLLECTION_TYPE_ALBUMS: {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isThisQueuePlaying() {
        return TextUtils.equals(this.getPath(), this.musicManager.getMusicMenuPath());
    }

    private void onMusicArtworkResponse(com.navdy.service.library.events.audio.MusicArtworkResponse a) {
        logger.d("REQUESTQUEUE onMusicArtworkResponse " + a);
        String s = this.collectionIdString(a);
        Integer a0 = this.requestedArtworkModelPositions.get(s);
        label1: {
            label0: {
                if (a0 != null) {
                    break label0;
                }
                if (android.text.TextUtils.equals(s, this.collectionIdString(this.menuData.musicCollectionInfo))) {
                    break label0;
                }
                logger.w("Wrong menu");
                break label1;
            }
            long j = android.os.SystemClock.elapsedRealtime();
            long j0 = this.artWorkRequestTime;
            if (a.photo == null) {
                logger.w("No photo in artwork response");
            } else {
                logger.d("[Timing] MusicArtworkRequest, response received in " + (j - j0) + " MS, Size : " + a.photo.size());
                final MusicMenu2 a1 = this;
                final MusicArtworkResponse a01 = a;
                final Integer a11 = a0;
                final String s1 = s;
                com.navdy.service.library.task.TaskManager.getInstance().execute(new Runnable() {
                    final MusicMenu2 this$0 = a1;
                    final MusicArtworkResponse val$artworkResponse = a01;
                    final String val$idString = s1;
                    final Integer val$pos = a11;

                    public void run() {
                        byte[] a = this.val$artworkResponse.photo.toByteArray();
                        label2: {
                            label0: {
                                label1: {
                                    if (a == null) {
                                        break label1;
                                    }
                                    if (a.length != 0) {
                                        break label0;
                                    }
                                }
                                logger.w("Received photo has null or empty byte array");
                                break label2;
                            }
                            this.this$0.handleArtwork(a, this.val$pos, this.val$idString, true);
                        }
                    }
                }, 1);
            }            synchronized(artworkRequestLock) {
                java.util.Stack a2 = artworkRequestQueue;
                if (a2.isEmpty()) {
                    logger.d("REQUESTQUEUE onMusicArtworkResponse queue empty");
                    infoForCurrentArtworkRequest = null;
                } else {
                    android.util.Pair a3 = artworkRequestQueue.pop();
                    com.navdy.service.library.events.audio.MusicArtworkRequest a4 = (com.navdy.service.library.events.audio.MusicArtworkRequest)a3.first;
                    logger.d("REQUESTQUEUE onMusicArtworkResponse posting: " + a4);
                    infoForCurrentArtworkRequest = (com.navdy.service.library.events.audio.MusicCollectionInfo)a3.second;
                    this.artWorkRequestTime = android.os.SystemClock.elapsedRealtime();
                    logger.d("[Timing] Making MusicArtWorkRequest " + a4);
                    this.bus.post(new com.navdy.hud.app.event.RemoteEvent(a4));
                }
                /*monexit(a1)*/
            }
        }
    }

    private void onMusicCollectionResponse(final MusicCollectionResponse musicCollectionResponse) {
        if (!TextUtils.equals(this.collectionIdString(musicCollectionResponse.collectionInfo), this.collectionIdString(this.menuData.musicCollectionInfo))) {
            MusicMenu2.logger.w("wrong collection");
        }
        else if (this.requestMusicOffset == -1) {
            MusicMenu2.logger.w("no outstanding request");
        }
        else {
            final long n = SystemClock.elapsedRealtime() - this.currentMusicCollectionRequestTime;
            if (this.musicManager.isMusicLibraryCachingEnabled()) {
                this.musicCollectionResponseMessageCache.put(ExtensionsKt.cacheKey(this.currentMusicCollectionRequest), musicCollectionResponse);
            }
            if (this.menuData.musicCollectionInfo == null) {
                this.menuData.musicCollectionInfo = musicCollectionResponse.collectionInfo;
            }
            else {
                final MusicCollectionInfo musicCollectionInfo = this.menuData.musicCollectionInfo;
                final MusicCollectionInfo.Builder builder = new MusicCollectionInfo.Builder(musicCollectionResponse.collectionInfo);
                if (TextUtils.isEmpty(musicCollectionResponse.collectionInfo.name)) {
                    builder.name(musicCollectionInfo.name);
                }
                if (TextUtils.isEmpty(musicCollectionResponse.collectionInfo.subtitle)) {
                    builder.subtitle(musicCollectionInfo.subtitle);
                }
                this.menuData.musicCollectionInfo = builder.build();
            }
            int n2 = 1;
            final boolean b = true;
            boolean b2 = true;
            final VerticalList.Model[] array = null;
            VerticalList.Model[] array2;
            if (musicCollectionResponse.musicCollections != null && musicCollectionResponse.musicCollections.size() > 0) {
                if (this.menuData.musicCollections == null) {
                    this.menuData.musicCollections = new ArrayList<>();
                }
                else {
                    b2 = false;
                }
                MusicMenu2.logger.d("[Timing] MusicCollectionRequest , received response  in : " + n + " MS, Collections : " + musicCollectionResponse.musicCollections.size());
                if (b2) {
                    this.menuData.musicCollections.addAll(musicCollectionResponse.musicCollections);
                    final int intValue = this.menuData.musicCollectionInfo.size;
                    final int size = musicCollectionResponse.musicCollections.size();
                    this.fastScrollIndex = this.checkCharacterMap(musicCollectionResponse.characterMap, intValue, size, true);
                    if (this.fastScrollIndex != null) {
                        this.menuData.musicIndex = musicCollectionResponse.characterMap;
                    }
                    n2 = 1;
                    array2 = array;
                    if (intValue != size) {
                        final int n3 = intValue - size;
                        for (int i = 0; i < n3; ++i) {
                            this.menuData.musicCollections.add(null);
                        }
                        MusicMenu2.logger.v("1-resp added[" + n3 + "]");
                        array2 = array;
                        n2 = 1;
                    }
                }
                else {
                    final int intValue2 = this.menuData.musicCollectionInfo.size;
                    final int size2 = musicCollectionResponse.musicCollections.size();
                    final VerticalList.Model[] array3 = new VerticalList.Model[size2];
                    MusicMenu2.logger.v("2-resp offset[" + this.requestMusicOffset + "] total[" + intValue2 + "] got[" + size2 + "]");
                    int n4 = 0;
                    while (true) {
                        n2 = 0;
                        array2 = array3;
                        if (n4 >= size2) {
                            break;
                        }
                        final MusicCollectionInfo musicCollectionInfo2 = musicCollectionResponse.musicCollections.get(n4);
                        this.menuData.musicCollections.set(this.requestMusicOffset + n4, musicCollectionInfo2);
                        array3[n4] = this.buildModelfromCollectionInfo(this.requestMusicOffset + n4, musicCollectionInfo2);
                        ++n4;
                    }
                }
            }
            else if (musicCollectionResponse.musicTracks != null && musicCollectionResponse.musicTracks.size() > 0) {
                boolean b3;
                if (this.menuData.musicTracks == null) {
                    this.menuData.musicTracks = new ArrayList<>();
                    b3 = b;
                }
                else {
                    b3 = false;
                }
                MusicMenu2.logger.d("[Timing] MusicCollectionRequest , received response  in : " + n + " MS, Tracks : " + musicCollectionResponse.musicTracks.size());
                if (b3) {
                    this.menuData.musicTracks.addAll(musicCollectionResponse.musicTracks);
                    final int intValue3 = this.menuData.musicCollectionInfo.size;
                    final int size3 = this.menuData.musicTracks.size();
                    this.fastScrollIndex = this.checkCharacterMap(musicCollectionResponse.characterMap, intValue3, size3, true);
                    if (this.fastScrollIndex != null) {
                        this.menuData.musicIndex = musicCollectionResponse.characterMap;
                    }
                    n2 = 1;
                    array2 = array;
                    if (intValue3 != size3) {
                        final int n5 = intValue3 - size3;
                        for (int j = 0; j < n5; ++j) {
                            this.menuData.musicTracks.add(null);
                        }
                        MusicMenu2.logger.v("1-resp-track added[" + n5 + "]");
                        n2 = 1;
                        array2 = array;
                    }
                }
                else {
                    final int intValue4 = this.menuData.musicCollectionInfo.size;
                    final int size4 = musicCollectionResponse.musicTracks.size();
                    final VerticalList.Model[] array4 = new VerticalList.Model[size4];
                    MusicMenu2.logger.v("2-resp-track offset[" + this.requestMusicOffset + "] total[" + intValue4 + "]  got[" + size4 + "]");
                    int n6 = 0;
                    while (true) {
                        n2 = 0;
                        array2 = array4;
                        if (n6 >= size4) {
                            break;
                        }
                        final MusicTrackInfo state = musicCollectionResponse.musicTracks.get(n6);
                        this.menuData.musicTracks.set(this.requestMusicOffset + n6, state);
                        final VerticalList.Model buildModel = IconBkColorViewHolder.buildModel(this.requestMusicOffset + n6, R.drawable.icon_song, this.bgColorSelected, MusicMenu2.bgColorUnselected, this.bgColorSelected, state.name, state.author);
                        buildModel.state = state;
                        array4[n6] = buildModel;
                        ++n6;
                    }
                }
            }
            else {
                MusicMenu2.logger.w("No collections or tracks in this collection...");
                this.musicCollectionSyncComplete = true;
                array2 = array;
            }
            if (n2 != 0) {
                this.presenter.cancelLoadingAnimation(1);
                this.highlightedItem = this.vmenuComponent.verticalList.getCurrentPosition();
                this.presenter.updateCurrentMenu(this);
            }
            else {
                int n8;
                final int n7 = n8 = this.requestMusicOffset + 1;
                if (this.hasShuffleEntry()) {
                    n8 = n7 + 1;
                }
                MusicMenu2.logger.v("updatemodels pos=" + n8 + " len=" + array2.length);
                this.partialRefresh = true;
                if (this.cachedList != null) {
                    final int size5 = this.cachedList.size();
                    for (int k = 0; k < array2.length; ++k) {
                        final int n9 = n8 + k;
                        if (n9 >= size5) {
                            MusicMenu2.logger.v("invalid index:" + n9 + " size:" + size5);
                            break;
                        }
                        this.cachedList.set(n9, array2[k]);
                    }
                }
                this.presenter.refreshData(n8, array2, this);
            }
            this.requestMusicOffset = -1;
            this.isRequestingMusicCollection = false;
            if (this.nextRequestOffset != -1) {
                int n10;
                if (this.menuData.musicCollections != null) {
                    n10 = 0;
                    if (this.menuData.musicCollections.get(this.nextRequestOffset) == null) {
                        n10 = 1;
                    }
                }
                else {
                    n10 = 0;
                    if (this.menuData.musicTracks != null) {
                        n10 = 0;
                        if (this.menuData.musicTracks.get(this.nextRequestOffset) == null) {
                            n10 = 1;
                        }
                    }
                }
                if (n10 != 0) {
                    this.requestMusicCollection(this.nextRequestOffset, this.nextRequestLimit, false);
                    MusicMenu2.logger.v("pending request:" + this.nextRequestLimit + " limit=" + this.nextRequestLimit);
                }
                this.nextRequestOffset = -1;
                this.nextRequestLimit = -1;
            }
        }
    }

    private void onTrackUpdated(final MusicTrackInfo musicTrackInfo) {
        if (!this.isThisQueuePlaying()) {
            MusicMenu2.logger.w("This queue isn't playing");
        }
        else {
            final String trackId = musicTrackInfo.trackId;
            if (this.trackIdToPositionMap != null && trackId != null && this.trackIdToPositionMap.containsKey(trackId)) {
                final Integer n = this.trackIdToPositionMap.get(trackId);
                if (this.nowPlayingTrackPosition > 0 && this.nowPlayingTrackPosition != n) {
                    this.stopAudioAnimation();
                    this.presenter.refreshDataforPos(this.nowPlayingTrackPosition);
                }
                this.presenter.refreshDataforPos(n);
                this.nowPlayingTrackPosition = n;
            }
            final boolean shuffling = this.musicManager.isShuffling();
            if (this.isShuffling != shuffling) {
                this.isShuffling = shuffling;
                this.vmenuComponent.verticalList.refreshData(1, this.getShuffleModel());
            }
        }
    }

    private void postOrQueueArtworkRequest(final MusicArtworkRequest musicArtworkRequest, final MusicCollectionInfo infoForCurrentArtworkRequest) {
        final Object artworkRequestLock = MusicMenu2.artworkRequestLock;
        synchronized (artworkRequestLock) {
            if (MusicMenu2.artworkRequestQueue.isEmpty() && MusicMenu2.infoForCurrentArtworkRequest == null) {
                MusicMenu2.logger.d("REQUESTQUEUE postOrQueueArtworkRequest posting: " + musicArtworkRequest + " title:" + infoForCurrentArtworkRequest.name);
                MusicMenu2.infoForCurrentArtworkRequest = infoForCurrentArtworkRequest;
                this.artWorkRequestTime = SystemClock.elapsedRealtime();
                MusicMenu2.logger.d("[Timing] Making MusicArtWorkRequest " + musicArtworkRequest);
                this.bus.post(new RemoteEvent(musicArtworkRequest));
            }
            else {
                MusicMenu2.logger.d("REQUESTQUEUE postOrQueueArtworkRequest queueing: " + musicArtworkRequest + " title:" + infoForCurrentArtworkRequest.name);
                MusicMenu2.artworkRequestQueue.push((Pair<MusicArtworkRequest, MusicCollectionInfo>)new Pair(musicArtworkRequest, infoForCurrentArtworkRequest));
            }
        }
    }

    private void purgeArtworkQueue() {
        final Object artworkRequestLock = MusicMenu2.artworkRequestLock;
        synchronized (artworkRequestLock) {
            final int size = MusicMenu2.artworkRequestQueue.size();
            MusicMenu2.artworkRequestQueue.clear();
            MusicMenu2.logger.d("REQUESTQUEUE purged: " + size);
        }
    }

    private void requestMusicCollection(final int n, int nextRequestLimit, final boolean b) {
        MusicMenu2.logger.d("requestMusicCollection Offset : " + n + ", Limit : " + nextRequestLimit + ", AFI : " + b);
        if (this.musicCollectionSyncComplete) {
            MusicMenu2.logger.i("sync is complete");
        }
        else if (this.isRequestingMusicCollection) {
            MusicMenu2.logger.i("Request already in process next=" + n + " limit:" + nextRequestLimit);
            this.nextRequestOffset = n;
            this.nextRequestLimit = nextRequestLimit;
        }
        else {
            this.nextRequestOffset = -1;
            this.nextRequestLimit = -1;
            this.isRequestingMusicCollection = true;
            this.requestMusicOffset = n;
            int n2;
            if ((n2 = nextRequestLimit) == -1) {
                n2 = 25;
            }
            MusicMenu2.logger.i("request music =" + n + " limit:" + n2);
            nextRequestLimit = n2;
            if (this.menuData.musicCollections == null) {
                nextRequestLimit = n2;
                if (this.menuData.musicTracks == null) {
                    nextRequestLimit = 50;
                }
            }
            final MusicCollectionRequest.Builder limit = new MusicCollectionRequest.Builder().collectionSource(this.menuData.musicCollectionInfo.collectionSource).collectionType(this.menuData.musicCollectionInfo.collectionType).collectionId(this.menuData.musicCollectionInfo.collectionId).offset(n).limit(nextRequestLimit);
            if (this.menuData.musicCollectionInfo.collectionType == MusicCollectionType.COLLECTION_TYPE_ARTISTS) {
                limit.groupBy(MusicCollectionType.COLLECTION_TYPE_ALBUMS);
            }
            if (b) {
                limit.includeCharacterMap(true);
            }
            final MusicCollectionRequest build = limit.build();
            this.currentMusicCollectionRequest = build;
            this.currentMusicCollectionRequestTime = SystemClock.elapsedRealtime();
            MusicMenu2.logger.i("got-asked offset=" + n + " limit=" + nextRequestLimit);
            MusicMenu2.logger.d("[Timing] MusicCollectionRequest " + build);
            if (this.musicManager.isMusicLibraryCachingEnabled()) {
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        final String cacheKey = ExtensionsKt.cacheKey(build);
                        final MusicCollectionResponse musicCollectionResponse = MusicMenu2.this.musicCollectionResponseMessageCache.get(cacheKey);
                        if (musicCollectionResponse != null) {
                            MusicMenu2.logger.d("Cache hit for MusicCollectionRequest " + build + ", Key :" + cacheKey);
                            MusicMenu2.this.bus.post(musicCollectionResponse);
                        }
                        else {
                            MusicMenu2.logger.d("Cache miss for MusicCollectionRequest " + build + ", Key :" + cacheKey);
                            MusicMenu2.this.bus.post(new RemoteEvent(build));
                        }
                    }
                }, 1);
            }
            else {
                this.bus.post(new RemoteEvent(build));
            }
        }
    }

    private void setupIconBgColor() {
        if (this.menuData.musicCollectionInfo != null) {
            if (this.menuData.musicCollectionInfo.collectionSource == MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL) {
                this.bgColorSelected = MusicMenu2.androidBgColor;
            }
            else if (this.menuData.musicCollectionInfo.collectionType == MusicCollectionType.COLLECTION_TYPE_PODCASTS) {
                this.bgColorSelected = MusicMenu2.applePodcastsBgColor;
            }
            else {
                this.bgColorSelected = MusicMenu2.appleMusicBgColor;
            }
        }
    }

    private void setupMenu() {
        if (MusicMenu2.busDelegate == null) {
            MusicMenu2.logger.v("bus-register ctor");
            MusicMenu2.busDelegate = new BusDelegate(this.presenter);
            this.bus.register(MusicMenu2.busDelegate);
            this.presenter.setScrollIdleEvents(true);
            this.musicManager.addMusicUpdateListener(MusicMenu2.busDelegate);
        }
        if (!this.initialized) {
            this.initialized = true;
            if (!this.musicManager.hasMusicCapabilities()) {
                MusicMenu2.logger.e("Navigated to Music menu with no music capabilities");
            }
            else {
                if (this.menuData.musicCollectionInfo == null) {
                    final List<MusicCapability> capabilities = this.musicManager.getMusicCapabilities().capabilities;
                    this.anyPlayersPermitted = false;
                    final ArrayList list = new ArrayList<MusicCollectionSource>(capabilities.size());
                    for (final MusicCapability musicCapability : capabilities) {
                        if (musicCapability.authorizationStatus == MusicCapability.MusicAuthorizationStatus.MUSIC_AUTHORIZATION_AUTHORIZED) {
                            this.anyPlayersPermitted = true;
                            list.add(musicCapability.collectionSource);
                        }
                    }
                    if (capabilities.size() == 1 && capabilities.get(0).collectionSource == MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL) {
                        this.menuData.musicCollectionInfo = new MusicCollectionInfo.Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).build();
                        final List<MusicCollectionType> collectionTypesForSource = this.musicManager.getCollectionTypesForSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL);
                        this.menuData.musicCollections = new ArrayList<>(collectionTypesForSource.size());
                        for (MusicCollectionType aCollectionTypesForSource : collectionTypesForSource) {
                            this.menuData.musicCollections.add(new MusicCollectionInfo.Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(aCollectionTypesForSource).build());
                        }
                    }
                    else {
                        this.menuData.musicCollectionInfo = new MusicCollectionInfo.Builder().build();
                        this.menuData.musicCollections = new ArrayList<>(list.size());
                        for (MusicCollectionSource aList : (Iterable<MusicCollectionSource>) list) {
                            this.menuData.musicCollections.add(new MusicCollectionInfo.Builder().collectionSource(aList).build());
                        }
                    }
                }
                this.setupIconBgColor();
            }
        }
    }

    private void showMusicPlayer() {
        MusicMenu2.logger.i("showMusicPlayer");
        final UIStateManager uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
        uiStateManager.addScreenAnimationListener(new IScreenAnimationListener() {
            @Override
            public void onStart(final BaseScreen baseScreen, final BaseScreen baseScreen2) {
            }

            @Override
            public void onStop(final BaseScreen baseScreen, final BaseScreen baseScreen2) {
//                MusicMenu2.this.musicManager.showMusicNotification();
                MusicMenu2.this.handler.post(new Runnable() {
                    IScreenAnimationListener val$listener;

                    @Override
                    public void run() {
                        uiStateManager.removeScreenAnimationListener(this.val$listener);
                    }
                });
            }
        });
        this.presenter.close();
        clearMenuData();
        this.storeMenuData();
        this.musicManager.setMusicMenuPath(this.getPath());
    }

    private void startAudioAnimation(final IconColorImageView iconColorImageView) {
        iconColorImageView.animate().cancel();
        iconColorImageView.setImageResource(R.drawable.audio_loop);
        final AnimationDrawable musicAnimation = (AnimationDrawable)iconColorImageView.getDrawable();
        musicAnimation.start();
        this.musicAnimation = musicAnimation;
        MusicMenu2.logger.v("startAudioAnimation");
    }

    private void stopAudioAnimation() {
        if (this.musicAnimation != null && this.musicAnimation.isRunning()) {
            this.musicAnimation.stop();
            MusicMenu2.logger.v("stopAudioAnimation");
        }
        this.musicAnimation = null;
    }

    private void storeMenuData() {
        if (this.parentMenu.getType() == Menu.MUSIC) {
            ((MusicMenu2)this.parentMenu).storeMenuData();
        }
        MusicMenu2.lastPlayedMusicMenuData.put(this.getPath(), this.menuData);
    }

    @Override
    public IMenu getChildMenu(final IMenu menu, String s, final String s2) {
        MusicMenu2.logger.v("getChildMenu:" + s + "," + s2);
        IMenu childMenu;
        if (!this.musicManager.hasMusicCapabilities()) {
            MusicMenu2.logger.e("No music capabilities...");
            childMenu = null;
        }
        else {
            final String[] split = s.split(":");
            final MusicMenu2 musicPlayerMenu = this.getMusicPlayerMenu(Integer.valueOf(split[0]), Integer.valueOf(split[1]));
            final String s3 = s = null;
            String substring;
            if ((substring = s2) != null) {
                if (s2.indexOf("/") == 0) {
                    s = s2.substring(1);
                    final int index = s.indexOf("/");
                    if (index >= 0) {
                        substring = s2.substring(index + 1);
                        s = s.substring(0, index);
                    }
                    else {
                        substring = null;
                    }
                }
                else {
                    substring = null;
                    s = null;
                }
            }
            childMenu = musicPlayerMenu;
            if (!TextUtils.isEmpty(s)) {
                childMenu = musicPlayerMenu.getChildMenu(this, s, substring);
            }
        }
        return childMenu;
    }

    @Override
    public int getInitialSelection() {
        MusicMenu2.logger.i("getInitialSelection " + this.highlightedItem);
        return this.highlightedItem;
    }

    @Override
    public List<VerticalList.Model> getItems() {
        MusicMenu2.logger.v("getItems " + this.getPath());
        this.setupMenu();
        final ArrayList<VerticalList.Model> cachedList = new ArrayList<>();
        this.returnToCacheList = new ArrayList<>();
        if (!this.musicManager.hasMusicCapabilities()) {
            MusicMenu2.logger.e("Navigated to Music menu with no music capabilities");
            cachedList.add(MusicMenu2.back);
        }
        else if (!this.anyPlayersPermitted) {
            String s;
            String s2;
            if (RemoteDeviceManager.getInstance().getRemoteDevicePlatform() == DeviceInfo.Platform.PLATFORM_iOS) {
                s = MusicMenu2.resources.getString(R.string.music_enable_permissions_title_ios);
                s2 = MusicMenu2.resources.getString(R.string.music_enable_permissions_subtitle_ios);
            }
            else {
                s = MusicMenu2.resources.getString(R.string.music_enable_permissions_title_android);
                s2 = MusicMenu2.resources.getString(R.string.music_enable_permissions_subtitle_android);
            }
            final VerticalList.Model buildModel = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, MusicMenu2.backColor, MusicMenu2.bgColorUnselected, MusicMenu2.backColor, s, s2);
            cachedList.add(buildModel);
            this.returnToCacheList.add(buildModel);
        }
        else if (this.menuData.musicCollectionInfo.size != null && this.menuData.musicCollectionInfo.size == 0) {
            final VerticalList.Model buildModel2 = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, MusicMenu2.backColor, MusicMenu2.bgColorUnselected, MusicMenu2.backColor, MusicMenu2.resources.getString(R.string.back), MusicMenu2.resources.getString(R.string.empty_queue, MusicMenu2.menuTypeStringMap.get(this.menuData.musicCollectionInfo.collectionType)));
            cachedList.add(buildModel2);
            this.returnToCacheList.add(buildModel2);
        }
        else {
            cachedList.add(MusicMenu2.back);
            if (this.menuData.musicCollectionInfo.collectionSource == null) {
                if (MusicMenu2.busDelegate == null && this.parentMenu.getType() == Menu.MAIN) {
                    MusicMenu2.logger.v("bus-register getItems");
                    MusicMenu2.busDelegate = new BusDelegate(this.presenter);
                    this.bus.register(MusicMenu2.busDelegate);
                    this.presenter.setScrollIdleEvents(true);
                }
                for (int i = 0; i < this.menuData.musicCollections.size(); ++i) {
                    final MusicCollectionInfo state = this.menuData.musicCollections.get(i);
                    final VerticalList.Model buildModel3 = IconBkColorViewHolder.buildModel(i, MusicMenu2.menuSourceIconMap.get(state.collectionSource), MusicMenu2.sourceBgColor, MusicMenu2.bgColorUnselected, MusicMenu2.sourceBgColor, MusicMenu2.menuSourceStringMap.get(state.collectionSource), null);
                    buildModel3.state = state;
                    cachedList.add(buildModel3);
                    this.returnToCacheList.add(buildModel3);
                    final List<MusicCollectionType> collectionTypesForSource = this.musicManager.getCollectionTypesForSource(state.collectionSource);
                    if (state.collectionSource == MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER && collectionTypesForSource.contains(MusicCollectionType.COLLECTION_TYPE_PODCASTS)) {
                        final VerticalList.Model buildModel4 = IconBkColorViewHolder.buildModel(R.id.apple_podcasts, MusicMenu2.menuTypeIconMap.get(MusicCollectionType.COLLECTION_TYPE_PODCASTS), MusicMenu2.applePodcastsBgColor, MusicMenu2.bgColorUnselected, MusicMenu2.applePodcastsBgColor, MusicMenu2.resources.getString(R.string.apple_podcasts), null);
                        buildModel4.state = MusicCollectionType.COLLECTION_TYPE_PODCASTS;
                        cachedList.add(buildModel4);
                        this.returnToCacheList.add(buildModel4);
                    }
                }
            }
            else if (this.menuData.musicCollectionInfo.collectionType == null) {
                for (int j = 0; j < this.menuData.musicCollections.size(); ++j) {
                    final MusicCollectionType collectionType = this.menuData.musicCollections.get(j).collectionType;
                    if (this.menuData.musicCollectionInfo.collectionSource != MusicCollectionSource.COLLECTION_SOURCE_IOS_MEDIA_PLAYER || collectionType != MusicCollectionType.COLLECTION_TYPE_PODCASTS) {
                        final VerticalList.Model buildModel5 = IconBkColorViewHolder.buildModel(j, MusicMenu2.menuTypeIconMap.get(collectionType), this.bgColorSelected, MusicMenu2.bgColorUnselected, this.bgColorSelected, MusicMenu2.menuTypeStringMap.get(collectionType), null);
                        buildModel5.state = collectionType;
                        cachedList.add(buildModel5);
                        this.returnToCacheList.add(buildModel5);
                    }
                }
            }
            else if (this.menuData.musicCollections == null && this.menuData.musicTracks == null) {
                cachedList.add(LoadingViewHolder.buildModel());
                this.requestMusicCollection(0, -1, this.isIndexSupported(this.menuData.musicCollectionInfo));
            }
            else if (this.menuData.musicCollections != null && this.menuData.musicCollections.size() > 0) {
                MusicMenu2.logger.i("Loading music collections for type " + this.backSelection);
                if (this.menuData.musicIndex != null && this.fastScrollIndex == null) {
                    this.fastScrollIndex = this.checkCharacterMap(this.menuData.musicIndex, 0, 0, false);
                }
                Integer n = null;
                if (this.menuData.musicCollectionInfo != null) {
                    if (this.menuData.musicCollectionInfo.collectionType != null) {
                        n = MusicMenu2.menuTypeIconMap.get(this.menuData.musicCollectionInfo.collectionType);
                    }
                }
                Integer value;
                if ((value = n) == null) {
                    value = 0;
                }
                if (this.menuData.musicCollectionInfo.collectionType == MusicCollectionType.COLLECTION_TYPE_ARTISTS && !TextUtils.isEmpty(this.menuData.musicCollectionInfo.collectionId) && Wire.<Boolean>get(this.menuData.musicCollectionInfo.canShuffle, false)) {
                    cachedList.add(this.getShuffleModel());
                    this.highlightedItem = 2;
                }
                for (int k = 0; k < this.menuData.musicCollections.size(); ++k) {
                    final MusicCollectionInfo musicCollectionInfo = this.menuData.musicCollections.get(k);
                    if (musicCollectionInfo == null) {
                        final VerticalList.Model buildModel6 = ContentLoadingViewHolder.buildModel(value, this.bgColorSelected, MusicMenu2.bgColorUnselected);
                        cachedList.add(buildModel6);
                        this.returnToCacheList.add(buildModel6);
                    }
                    else {
                        final VerticalList.Model buildModelfromCollectionInfo = this.buildModelfromCollectionInfo(k, musicCollectionInfo);
                        cachedList.add(buildModelfromCollectionInfo);
                        this.returnToCacheList.add(buildModelfromCollectionInfo);
                    }
                }
            }
            else if (this.menuData.musicTracks != null && this.menuData.musicTracks.size() > 0) {
                if (this.trackIdToPositionMap == null) {
                    this.trackIdToPositionMap = new HashMap<>();
                }
                int n2;
                if (this.menuData.musicCollectionInfo.collectionType == MusicCollectionType.COLLECTION_TYPE_PODCASTS) {
                    n2 = R.drawable.icon_play_music;
                }
                else {
                    final int n3 = n2 = R.drawable.icon_song;
                    if (this.menuData.musicTracks.size() > 1) {
                        cachedList.add(this.getShuffleModel());
                        n2 = n3;
                        if (this.menuData.musicCollectionInfo.collectionType != MusicCollectionType.COLLECTION_TYPE_PLAYLISTS) {
                            this.highlightedItem = 2;
                            n2 = n3;
                        }
                    }
                }
                for (int l = 0; l < this.menuData.musicTracks.size(); ++l) {
                    final MusicTrackInfo state2 = this.menuData.musicTracks.get(l);
                    if (state2 == null) {
                        final VerticalList.Model buildModel7 = ContentLoadingViewHolder.buildModel(n2, this.bgColorSelected, MusicMenu2.bgColorUnselected);
                        cachedList.add(buildModel7);
                        this.returnToCacheList.add(buildModel7);
                    }
                    else {
                        final VerticalList.Model buildModel8 = IconBkColorViewHolder.buildModel(l, n2, this.bgColorSelected, MusicMenu2.bgColorUnselected, this.bgColorSelected, state2.name, state2.author);
                        buildModel8.state = state2;
                        cachedList.add(buildModel8);
                        this.returnToCacheList.add(buildModel8);
                        this.trackIdToPositionMap.put(state2.trackId, cachedList.size() - 1);
                    }
                }
                if (this.menuData.musicTracks.size() < this.menuData.musicCollectionInfo.size) {
                    cachedList.add(LoadingViewHolder.buildModel());
                }
            }
            else {
                MusicMenu2.logger.e("No collections or tracks in this collection...");
            }
            this.cachedList = cachedList;
        }
        return cachedList;
    }

    @Override
    public VerticalList.Model getModelfromPos(final int n) {
        VerticalList.Model model;
        if (this.cachedList != null && this.cachedList.size() > n) {
            model = this.cachedList.get(n);
        }
        else {
            model = null;
        }
        return model;
    }

    public String getPath() {
        if (this.path == null) {
            if (this.parentMenu.getType() == Menu.MUSIC) {
                this.path = ((MusicMenu2)this.parentMenu).getPath() + "/" + this.backSelectionId + ":" + this.backSelection;
            }
            else {
                this.path = "/" + Menu.MUSIC.name();
            }
        }
        return this.path;
    }

    @Override
    public VerticalFastScrollIndex getScrollIndex() {
        return this.fastScrollIndex;
    }

    @Override
    public Menu getType() {
        return Menu.MUSIC;
    }

    @Override
    public boolean isBindCallsEnabled() {
        return true;
    }

    @Override
    public boolean isFirstItemEmpty() {
        return true;
    }

    @Override
    public boolean isItemClickable(final int n, final int n2) {
        return true;
    }

    @Override
    public void onBindToView(final VerticalList.Model model, final View view, final int n, final VerticalList.ModelState modelState) {
        if (this.partialRefresh && n == this.vmenuComponent.verticalList.getRawPosition()) {
            this.partialRefresh = false;
            MusicMenu2.logger.v("partial refresh:" + n);
            this.vmenuComponent.verticalList.setViewHolderState(n, VerticalViewHolder.State.SELECTED, VerticalViewHolder.AnimationType.NONE, 0);
        }
        MusicMenu2.logger.v("onBindToView:" + n + " current=" + this.vmenuComponent.verticalList.getCurrentPosition());
        if (model.state instanceof MusicTrackInfo) {
            final IconColorImageView iconColorImageView = (IconColorImageView)VerticalList.findImageView(view);
            final IconColorImageView iconColorImageView2 = (IconColorImageView)VerticalList.findSmallImageView(view);
            final MusicTrackInfo musicTrackInfo = (MusicTrackInfo)model.state;
            final MusicTrackInfo currentTrack = this.musicManager.getCurrentTrack();
            if (TextUtils.equals(currentTrack.trackId, musicTrackInfo.trackId)) {
                modelState.updateImage = false;
                modelState.updateSmallImage = false;
                if (MusicManager.tryingToPlay(currentTrack.playbackState)) {
                    this.startAudioAnimation(iconColorImageView);
                }
                else {
                    iconColorImageView.setImageResource(R.drawable.audio_seq_32_sm);
                    this.stopAudioAnimation();
                }
                iconColorImageView2.setImageResource(R.drawable.audio_seq_32_sm);
            }
            else {
                modelState.updateImage = true;
                modelState.updateSmallImage = true;
            }
        }
        else if (model.state instanceof MusicCollectionInfo) {
            final MusicCollectionInfo musicCollectionInfo = (MusicCollectionInfo)model.state;
            if (!TextUtils.isEmpty(musicCollectionInfo.collectionId)) {
                final IconColorImageView iconColorImageView3 = (IconColorImageView)VerticalList.findImageView(view);
                final IconColorImageView iconColorImageView4 = (IconColorImageView)VerticalList.findSmallImageView(view);
                iconColorImageView3.setTag(null);
                final String collectionIdString = this.collectionIdString(musicCollectionInfo);
                final Bitmap bitmapfromCache = PicassoUtil.getBitmapfromCache(collectionIdString);
                if (bitmapfromCache != null) {
                    iconColorImageView3.setImageBitmap(bitmapfromCache);
                    iconColorImageView4.setImageBitmap(bitmapfromCache);
                    modelState.updateImage = false;
                    modelState.updateSmallImage = false;
                }
                else {
                    iconColorImageView3.setImageBitmap(null);
                    iconColorImageView4.setImageBitmap(null);
                    if (this.requestedArtworkModelPositions.get(collectionIdString) != null) {
                        MusicMenu2.logger.i("Already requested artwork for collection: " + collectionIdString);
                    }
                    else {
                        this.requestedArtworkModelPositions.put(collectionIdString, n);
                        MusicMenu2.logger.i("Checking cache for artwork for collection: " + musicCollectionInfo);
                        this.musicArtworkCache.getArtwork(musicCollectionInfo, new MusicArtworkCache.Callback() {
                            @Override
                            public void onHit(@NonNull final byte[] array) {
                                MusicMenu2.this.handleArtwork(array, n, MusicMenu2.this.collectionIdString(musicCollectionInfo), false);
                            }

                            @Override
                            public void onMiss() {
                                MusicMenu2.logger.i("Requesting artwork for collection: " + musicCollectionInfo);
                                MusicMenu2.this.postOrQueueArtworkRequest(new MusicArtworkRequest.Builder().collectionSource(musicCollectionInfo.collectionSource).collectionType(musicCollectionInfo.collectionType).collectionId(musicCollectionInfo.collectionId).size(200).build(), musicCollectionInfo);
                            }
                        });
                    }
                }
            }
        }
        else {
            modelState.updateImage = true;
            modelState.updateSmallImage = true;
        }
    }

    @Override
    public void onFastScrollEnd() {
        MusicMenu2.logger.v("REQUESTQUEUE onFastScrollEnd");
        this.purgeArtworkQueue();
    }

    @Override
    public void onFastScrollStart() {
    }

    @Override
    public void onItemSelected(final VerticalList.ItemSelectionState itemSelectionState) {
    }

    @Override
    public void onScrollIdle() {
        if (!this.musicCollectionSyncComplete) {
            final boolean b = this.menuData.musicCollections != null && this.menuData.musicCollections.size() > 0;
            boolean b2;
            b2 = this.menuData.musicTracks != null && this.menuData.musicTracks.size() > 0;
            if (this.menuData.musicCollectionInfo != null && this.menuData.musicCollectionInfo.collectionType != null && this.cachedList != null && (b || b2)) {
                int n = this.vmenuComponent.verticalList.getCurrentPosition() - 1;
                if (this.hasShuffleEntry()) {
                    --n;
                }
                int n2;
                if ((n2 = n) < 0) {
                    n2 = 0;
                }
                int n3;
                if (b) {
                    n3 = this.menuData.musicCollections.size();
                }
                else {
                    n3 = this.menuData.musicTracks.size();
                }
                if (n2 >= n3) {
                    MusicMenu2.logger.v("onScrollIdle pos(" + n2 + ") >= size(" + n3 + ")");
                }
                else {
                    Message message;
                    if (b) {
                        message = this.menuData.musicCollections.get(n2);
                    }
                    else {
                        message = this.menuData.musicTracks.get(n2);
                    }
                    int n4 = n2;
                    Message message2 = message;
                    if (message != null) {
                        final int n5 = n2 - 1;
                        int n6 = n2;
                        if (n5 >= 0) {
                            Message message3;
                            if (b) {
                                message3 = this.menuData.musicCollections.get(n5);
                            }
                            else {
                                message3 = this.menuData.musicTracks.get(n5);
                            }
                            n6 = n2;
                            message = message3;
                            if (message3 == null) {
                                n6 = n2 - 1;
                                message = null;
                            }
                        }
                        n4 = n6;
                        if ((message2 = message) != null) {
                            final int n7 = n6 + 1;
                            if (n7 <= n3 - 1) {
                                if (b) {
                                    message = this.menuData.musicCollections.get(n7);
                                }
                                else {
                                    message = this.menuData.musicTracks.get(n7);
                                }
                            }
                            n4 = n6;
                            if ((message2 = message) == null) {
                                n4 = n6 + 1;
                                message2 = null;
                            }
                        }
                    }
                    if (message2 == null) {
                        this.calculateOffset(n4, b);
                        MusicMenu2.logger.v("onScrollIdle newOffset:" + this.loadingOffset.offset + " count=" + this.loadingOffset.limit);
                        this.requestMusicCollection(this.loadingOffset.offset, this.loadingOffset.limit, false);
                    }
                }
            }
        }
    }

    @Override
    public void onUnload(final MenuLevel menuLevel) {
        switch (menuLevel) {
            case BACK_TO_PARENT:
                if (MusicMenu2.busDelegate != null && this.parentMenu.getType() == Menu.MAIN) {
                    this.bus.unregister(MusicMenu2.busDelegate);
                    this.presenter.setScrollIdleEvents(false);
                    this.purgeArtworkQueue();
                    MusicMenu2.infoForCurrentArtworkRequest = null;
                    this.isRequestingMusicCollection = false;
                    this.musicManager.removeMusicUpdateListener(MusicMenu2.busDelegate);
                    this.stopAudioAnimation();
                    MusicMenu2.busDelegate = null;
                    MusicMenu2.logger.v("onUnload-back bus-unregister");
                }
                if (this.returnToCacheList != null) {
                    MusicMenu2.logger.v("onUnload add to cache");
                    VerticalModelCache.addToCache(this.returnToCacheList);
                    this.returnToCacheList = null;
                    break;
                }
                break;
            case CLOSE:
                if (MusicMenu2.busDelegate != null) {
                    this.bus.unregister(MusicMenu2.busDelegate);
                    this.presenter.setScrollIdleEvents(false);
                    this.purgeArtworkQueue();
                    MusicMenu2.infoForCurrentArtworkRequest = null;
                    this.isRequestingMusicCollection = false;
                    this.musicManager.removeMusicUpdateListener(MusicMenu2.busDelegate);
                    this.stopAudioAnimation();
                    MusicMenu2.busDelegate = null;
                    MusicMenu2.logger.v("onUnload-close bus-unregister");
                }
                if (this.returnToCacheList != null) {
                    MusicMenu2.logger.v("onUnload add to cache");
                    VerticalModelCache.addToCache(this.returnToCacheList);
                    this.returnToCacheList = null;
                }
                if (this.parentMenu != null && this.parentMenu.getType() == Menu.MUSIC) {
                    this.parentMenu.onUnload(MenuLevel.CLOSE);
                    break;
                }
                break;
        }
    }

    @Override
    public boolean selectItem(final VerticalList.ItemSelectionState itemSelectionState) {
        MusicMenu2.logger.i("onItemClicked " + itemSelectionState.id + ", " + itemSelectionState.pos);
        if (itemSelectionState.id == R.id.menu_back) {
            this.presenter.loadMenu(this.parentMenu, MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
            this.backSelection = 0;
            this.backSelectionId = 0;
        }
        else if (itemSelectionState.id == R.id.music_browser_shuffle) {
            if (this.isThisQueuePlaying()) {
                MusicShuffleMode musicShuffleMode;
                if (this.musicManager.isShuffling()) {
                    musicShuffleMode = MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF;
                }
                else {
                    musicShuffleMode = MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS;
                }
                this.bus.post(new RemoteEvent(new MusicEvent.Builder().action(MusicEvent.Action.MUSIC_ACTION_MODE_CHANGE).shuffleMode(musicShuffleMode).build()));
                this.handler.post(new Runnable() {
                    @Override
                    public void run() {
                        MusicMenu2.this.presenter.reset();
                        MusicMenu2.this.vmenuComponent.verticalList.unlock();
                    }
                });
            }
            else {
                final MusicCollectionInfo musicCollectionInfo = this.menuData.musicCollectionInfo;
                this.bus.post(new RemoteEvent(new MusicEvent.Builder().collectionSource(musicCollectionInfo.collectionSource).collectionType(musicCollectionInfo.collectionType).collectionId(musicCollectionInfo.collectionId).action(MusicEvent.Action.MUSIC_ACTION_PLAY).shuffleMode(MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS).build()));
                this.showMusicPlayer();
            }
        }
        else if (this.menuData.musicTracks != null && this.menuData.musicTracks.size() > 0) {
            VerticalList.Model model2;
            final VerticalList.Model model = model2 = null;
            if (this.cachedList != null) {
                model2 = null;
                if (itemSelectionState.pos >= 0) {
                    model2 = null;
                    if (itemSelectionState.pos < this.cachedList.size()) {
                        model2 = this.cachedList.get(itemSelectionState.pos);
                    }
                }
            }
            final Logger logger = MusicMenu2.logger;
            final StringBuilder append = new StringBuilder().append("onItemClicked track:");
            String title;
            if (model2 != null) {
                title = model2.title;
            }
            else {
                title = "null";
            }
            logger.v(append.append(title).toString());
            final MusicCollectionInfo musicCollectionInfo2 = this.menuData.musicCollectionInfo;
            this.bus.post(new RemoteEvent(new MusicEvent.Builder().collectionSource(musicCollectionInfo2.collectionSource).collectionType(musicCollectionInfo2.collectionType).collectionId(musicCollectionInfo2.collectionId).index(itemSelectionState.id).action(MusicEvent.Action.MUSIC_ACTION_PLAY).shuffleMode(MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF).build()));
            this.showMusicPlayer();
            AnalyticsSupport.recordMusicBrowsePlayAction(MusicMenu2.analyticsTypeStringMap.get(musicCollectionInfo2.collectionType));
        }
        else {
            this.presenter.loadMenu(this.getMusicPlayerMenu(itemSelectionState.id, itemSelectionState.pos), MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
        }
        return true;
    }

    @Override
    public void setBackSelectionId(final int backSelectionId) {
        MusicMenu2.logger.i("setBackSelectionId: " + backSelectionId);
        this.backSelectionId = backSelectionId;
    }

    @Override
    public void setBackSelectionPos(final int backSelection) {
        this.backSelection = backSelection;
    }

    @Override
    public void setSelectedIcon() {
        MusicMenu2.logger.i("setSelectedIcon");
        this.setupMenu();
        if (this.parentMenu.getType() == Menu.MAIN) {
            this.vmenuComponent.setSelectedIconColorImage(R.drawable.icon_mm_music_2, MusicMenu2.mmMusicColor, null, 1.0f);
            this.vmenuComponent.selectedText.setText(R.string.music);
        }
        else if (this.menuData.musicCollectionInfo.collectionType == null) {
            this.vmenuComponent.setSelectedIconColorImage(MusicMenu2.menuSourceIconMap.get(this.menuData.musicCollectionInfo.collectionSource), MusicMenu2.sourceBgColor, null, 1.0f);
            this.vmenuComponent.selectedText.setText(MusicMenu2.menuSourceStringMap.get(this.menuData.musicCollectionInfo.collectionSource));
        }
        else if (TextUtils.isEmpty(this.menuData.musicCollectionInfo.collectionId)) {
            this.vmenuComponent.setSelectedIconColorImage(MusicMenu2.menuTypeIconMap.get(this.menuData.musicCollectionInfo.collectionType), this.bgColorSelected, null, 1.0f);
            this.vmenuComponent.selectedText.setText(MusicMenu2.menuTypeStringMap.get(this.menuData.musicCollectionInfo.collectionType));
        }
        else {
            final Bitmap bitmapfromCache = PicassoUtil.getBitmapfromCache(this.collectionIdString(this.menuData.musicCollectionInfo));
            if (bitmapfromCache != null) {
                this.vmenuComponent.setSelectedIconImage(bitmapfromCache);
            }
            else {
                IconColorImageView.IconShape iconShape;
                if (this.menuData.musicCollectionInfo.collectionType == MusicCollectionType.COLLECTION_TYPE_ARTISTS) {
                    iconShape = IconColorImageView.IconShape.CIRCLE;
                }
                else {
                    iconShape = IconColorImageView.IconShape.SQUARE;
                }
                this.vmenuComponent.setSelectedIconColorImage(MusicMenu2.menuTypeIconMap.get(this.menuData.musicCollectionInfo.collectionType), this.bgColorSelected, null, 1.0f, iconShape);
                this.musicArtworkCache.getArtwork(this.menuData.musicCollectionInfo, new MusicArtworkCache.Callback() {
                    @Override
                    public void onHit(@NonNull final byte[] array) {
                        MusicMenu2.this.handleArtwork(array, null, MusicMenu2.this.collectionIdString(MusicMenu2.this.menuData.musicCollectionInfo), false);
                    }

                    @Override
                    public void onMiss() {
                        MusicMenu2.this.postOrQueueArtworkRequest(new MusicArtworkRequest.Builder().collectionSource(MusicMenu2.this.menuData.musicCollectionInfo.collectionSource).collectionType(MusicMenu2.this.menuData.musicCollectionInfo.collectionType).collectionId(MusicMenu2.this.menuData.musicCollectionInfo.collectionId).size(200).build(), MusicMenu2.this.menuData.musicCollectionInfo);
                    }
                });
            }
            this.vmenuComponent.selectedText.setText(this.menuData.musicCollectionInfo.name);
        }
    }

    @Override
    public void showToolTip() {
    }

    private static class BusDelegate implements MusicManager.MusicUpdateListener
    {
        MainMenuScreen2.Presenter presenter;

        BusDelegate(final MainMenuScreen2.Presenter presenter) {
            this.presenter = presenter;
        }

        @Override
        public void onAlbumArtUpdate(@Nullable final ByteString byteString, final boolean b) {
        }

        @Subscribe
        public void onMusicArtworkResponse(final MusicArtworkResponse musicArtworkResponse) {
            final IMenu currentMenu = this.presenter.getCurrentMenu();
            if (currentMenu != null && currentMenu.getType() == Menu.MUSIC) {
                ((MusicMenu2)currentMenu).onMusicArtworkResponse(musicArtworkResponse);
            }
        }

        @Subscribe
        public void onMusicCollectionResponse(final MusicCollectionResponse musicCollectionResponse) {
            final IMenu currentMenu = this.presenter.getCurrentMenu();
            if (currentMenu != null && currentMenu.getType() == Menu.MUSIC) {
                ((MusicMenu2)currentMenu).onMusicCollectionResponse(musicCollectionResponse);
            }
        }

        @Override
        public void onTrackUpdated(final MusicTrackInfo musicTrackInfo, final Set<MusicManager.MediaControl> set, final boolean b) {
            final IMenu currentMenu = this.presenter.getCurrentMenu();
            if (currentMenu != null && currentMenu.getType() == Menu.MUSIC) {
                ((MusicMenu2)currentMenu).onTrackUpdated(musicTrackInfo);
            }
        }
    }

    private static class IndexOffset
    {
        int limit;
        int offset;

        void clear() {
            this.offset = 0;
            this.limit = -1;
        }
    }

    private class MusicMenuData
    {
        MusicCollectionInfo musicCollectionInfo;
        List<MusicCollectionInfo> musicCollections;
        List<MusicCharacterMap> musicIndex;
        List<MusicTrackInfo> musicTracks;

        MusicMenuData() {
            this.musicCollections = null;
            this.musicTracks = null;
            this.musicIndex = null;
        }

        MusicMenuData(final MusicCollectionInfo musicCollectionInfo, final List<MusicCollectionInfo> musicCollections) {
            this.musicCollections = null;
            this.musicTracks = null;
            this.musicIndex = null;
            this.musicCollectionInfo = musicCollectionInfo;
            this.musicCollections = musicCollections;
        }
    }
}
