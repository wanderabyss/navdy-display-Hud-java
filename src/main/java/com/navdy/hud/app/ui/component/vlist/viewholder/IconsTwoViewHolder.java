package com.navdy.hud.app.ui.component.vlist.viewholder;

import com.navdy.hud.app.R;
import android.animation.Animator;
import android.view.View;
import com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils;
import android.view.ViewGroup;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;
import android.os.Handler;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import android.view.ViewGroup;
import com.navdy.service.library.log.Logger;

public class IconsTwoViewHolder extends IconBaseViewHolder
{
    private static final Logger sLogger;
    private int imageIconSize;
    
    static {
        sLogger = new Logger(IconsTwoViewHolder.class);
    }
    
    private IconsTwoViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        super(viewGroup, list, handler);
        this.imageIconSize = -1;
    }
    
    public static VerticalList.Model buildModel(final int n, final int n2, final int n3, final int n4, final int n5, final String s, final String s2) {
        return buildModel(n, n2, n3, n4, n5, s, s2, null);
    }
    
    public static VerticalList.Model buildModel(final int id, final int icon, final int iconSmall, final int iconFluctuatorColor, final int iconDeselectedColor, final String title, final String subTitle, final String subTitle2) {
        VerticalList.Model fromCache;
        if ((fromCache = VerticalModelCache.getFromCache(VerticalList.ModelType.TWO_ICONS)) == null) {
            fromCache = new VerticalList.Model();
        }
        fromCache.type = VerticalList.ModelType.TWO_ICONS;
        fromCache.id = id;
        fromCache.icon = icon;
        fromCache.iconSmall = iconSmall;
        fromCache.iconFluctuatorColor = iconFluctuatorColor;
        fromCache.iconDeselectedColor = iconDeselectedColor;
        fromCache.title = title;
        fromCache.subTitle = subTitle;
        fromCache.subTitle2 = subTitle2;
        return fromCache;
    }
    
    public static IconsTwoViewHolder buildViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        return new IconsTwoViewHolder(IconBaseViewHolder.getLayout(viewGroup, R.layout.vlist_item, R.layout.crossfade_image_lyt), list, handler);
    }
    
    private void setIcon(final int n, final int n2, final boolean b, final boolean b2, final int bkColor, final int imageIconSize) {
        this.imageIconSize = imageIconSize;
        String s = null;
        if (this.extras != null) {
            s = this.extras.get("INITIAL");
        }
        if (b) {
            InitialsImageView.Style style;
            if (s != null) {
                style = InitialsImageView.Style.MEDIUM;
            }
            else {
                style = InitialsImageView.Style.DEFAULT;
            }
            ((InitialsImageView)this.crossFadeImageView.getBig()).setImage(n, s, style);
        }
        if (b2) {
            final InitialsImageView initialsImageView = (InitialsImageView)this.crossFadeImageView.getSmall();
            initialsImageView.setAlpha(1.0f);
            if (n2 == 0) {
                initialsImageView.setBkColor(bkColor);
            }
            else {
                initialsImageView.setBkColor(0);
            }
            initialsImageView.setImage(n2, s, InitialsImageView.Style.TINY);
        }
    }
    
    @Override
    public void bind(final VerticalList.Model model, final VerticalList.ModelState modelState) {
        super.bind(model, modelState);
        this.setIcon(model.icon, model.iconSmall, modelState.updateImage, modelState.updateSmallImage, model.iconDeselectedColor, model.iconSize);
    }
    
    @Override
    public VerticalList.ModelType getModelType() {
        return VerticalList.ModelType.TWO_ICONS;
    }
    
    @Override
    public void setItemState(final State state, final AnimationType animationType, int n, final boolean b) {
        super.setItemState(state, animationType, n, b);
        n = 0;
        final CrossFadeImageView.Mode mode = null;
        CrossFadeImageView.Mode mode2 = null;
        switch (state) {
            default:
                mode2 = mode;
                break;
            case SELECTED:
                n = IconsTwoViewHolder.selectedIconSize;
                mode2 = CrossFadeImageView.Mode.BIG;
                break;
            case UNSELECTED:
                if (this.iconScaleAnimationDisabled) {
                    n = IconsTwoViewHolder.selectedIconSize;
                    mode2 = CrossFadeImageView.Mode.SMALL;
                    break;
                }
                n = IconsTwoViewHolder.unselectedIconSize;
                mode2 = CrossFadeImageView.Mode.SMALL;
                break;
        }
        if (this.imageIconSize != -1) {
            n = this.imageIconSize;
        }
        switch (animationType) {
            case NONE:
            case INIT: {
                final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup.MarginLayoutParams)this.iconContainer.getLayoutParams();
                viewGroup$MarginLayoutParams.width = n;
                viewGroup$MarginLayoutParams.height = n;
                this.crossFadeImageView.setMode(mode2);
                break;
            }
            case MOVE:
                this.animatorSetBuilder.with(VerticalAnimationUtils.animateDimension((View)this.iconContainer, n));
                if (this.crossFadeImageView.getMode() != mode2) {
                    this.animatorSetBuilder.with((Animator)this.crossFadeImageView.getCrossFadeAnimator());
                    break;
                }
                break;
        }
    }
}
