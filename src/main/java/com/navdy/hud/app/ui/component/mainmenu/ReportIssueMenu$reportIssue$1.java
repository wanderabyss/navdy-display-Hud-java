package com.navdy.hud.app.ui.component.mainmenu;

import com.navdy.hud.app.util.ReportIssueService;
import com.navdy.hud.app.util.ReportIssueService.IssueType;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
/* compiled from: ReportIssueMenu.kt */
final class ReportIssueMenu$reportIssue$1 implements Runnable {
    final /* synthetic */ IssueType issueType;
    final /* synthetic */ ReportIssueMenu this$0;

    ReportIssueMenu$reportIssue$1(ReportIssueMenu reportIssueMenu, IssueType issueType) {
        this.this$0 = reportIssueMenu;
        this.issueType = issueType;
    }

    public final void run() {
        this.this$0.presenter.close(new Runnable() {
            public final void run() {
                ReportIssueService.dispatchReportNewIssue(ReportIssueMenu$reportIssue$1.this.issueType);
                ReportIssueMenu$reportIssue$1.this.this$0.showSentToast();
            }
        });
    }
}
