package com.navdy.hud.app.ui.component.carousel;

import android.view.View;
import android.animation.ObjectAnimator;
import android.animation.Animator;
import android.animation.AnimatorSet;

public class ShrinkAnimator extends CarouselAnimator
{
    @Override
    public AnimatorSet createHiddenViewAnimation(final CarouselLayout carouselLayout, final Direction direction) {
        final AnimatorSet set = new AnimatorSet();
        View view;
        View view2;
        if (direction == Direction.RIGHT) {
            view = carouselLayout.newLeftView;
            view2 = carouselLayout.leftView;
        }
        else {
            view = carouselLayout.newRightView;
            view2 = carouselLayout.rightView;
        }
        final float n = 20.0f / view2.getMeasuredWidth();
        view.setScaleX(n);
        view.setScaleY(n);
        final float x = view2.getX();
        int n2;
        if (direction == Direction.RIGHT) {
            n2 = -1;
        }
        else {
            n2 = 1;
        }
        final float n3 = n2 * carouselLayout.viewPadding / 2;
        int measuredWidth;
        if (direction == Direction.RIGHT) {
            measuredWidth = 0;
        }
        else {
            measuredWidth = view2.getMeasuredWidth();
        }
        view.setX(measuredWidth + (x + n3));
        view.setY(view2.getY() + view2.getMeasuredHeight() / 2 - 20.0f / 2.0f);
        set.playTogether(new Animator[] { ObjectAnimator.ofFloat(view, "x", new float[] { view2.getX() }), ObjectAnimator.ofFloat(view, "y", new float[] { view2.getY() }), ObjectAnimator.ofFloat(view, "scaleX", new float[] { 1.0f }), ObjectAnimator.ofFloat(view, "scaleY", new float[] { 1.0f }) });
        view.setPivotX(0.5f);
        view.setPivotY(0.5f);
        return set;
    }
    
    @Override
    public Animator createViewOutAnimation(final CarouselLayout carouselLayout, final Direction direction) {
        float n;
        View view;
        if (direction == Direction.RIGHT) {
            final View rightView = carouselLayout.rightView;
            n = rightView.getX() + rightView.getMeasuredWidth() + carouselLayout.viewPadding / 2 - 20.0f / 2.0f;
            view = rightView;
        }
        else {
            final View leftView = carouselLayout.leftView;
            n = leftView.getX() - carouselLayout.viewPadding / 2 - 20.0f / 2.0f;
            view = leftView;
        }
        final float y = view.getY();
        final float n2 = view.getMeasuredHeight() / 2;
        final float n3 = 20.0f / 2.0f;
        final float n4 = 20.0f / view.getMeasuredWidth();
        final AnimatorSet set = new AnimatorSet();
        set.playTogether(new Animator[] { ObjectAnimator.ofFloat(view, "x", new float[] { n }), ObjectAnimator.ofFloat(view, "y", new float[] { y + n2 - n3 }), ObjectAnimator.ofFloat(view, "scaleX", new float[] { n4 }), ObjectAnimator.ofFloat(view, "scaleY", new float[] { n4 }) });
        view.setPivotX(0.5f);
        view.setPivotY(0.5f);
        return (Animator)set;
    }
}
