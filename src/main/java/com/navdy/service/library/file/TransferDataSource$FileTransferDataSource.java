package com.navdy.service.library.file;

import java.io.IOException;

class TransferDataSource$FileTransferDataSource extends com.navdy.service.library.file.TransferDataSource {
    java.io.File mFile;
    
    TransferDataSource$FileTransferDataSource(java.io.File a) {
        this.mFile = a;
    }
    
    public String checkSum() throws IOException {
        return com.navdy.service.library.util.IOUtils.hashForFile(this.mFile);
    }
    
    public String checkSum(long j) throws IOException {
        return com.navdy.service.library.util.IOUtils.hashForFile(this.mFile, j);
    }
    
    public java.io.File getFile() {
        return this.mFile;
    }
    
    public String getName() {
        return this.mFile.getName();
    }
    
    public long length() {
        return this.mFile.length();
    }
    
    public int read(byte[] a) {
        int i = 0;
        label3: {
            java.io.RandomAccessFile a0 = null;
            label1: {
                label2: {
                    try {
                        a0 = new java.io.RandomAccessFile(this.mFile, "r");
                        break label2;
                    } catch(Throwable ignoredException) {
                    }
                    a0 = null;
                    break label1;
                }
                label0: {
                    try {
                        a0.seek(this.mCurOffset);
                        i = a0.read(a);
                        break label0;
                    } catch(Throwable ignoredException0) {
                    }
                    break label1;
                }
                com.navdy.service.library.util.IOUtils.closeObject((java.io.Closeable)a0);
                break label3;
            }
            com.navdy.service.library.util.IOUtils.closeObject((java.io.Closeable)a0);
            i = -1;
        }
        return i;
    }
}
