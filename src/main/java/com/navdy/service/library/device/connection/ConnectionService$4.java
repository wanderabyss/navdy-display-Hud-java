package com.navdy.service.library.device.connection;

class ConnectionService$4 {
    final static int[] $SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State;
    
    static {
        $SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State = new int[com.navdy.service.library.device.connection.ConnectionService$State.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State;
        com.navdy.service.library.device.connection.ConnectionService$State a0 = com.navdy.service.library.device.connection.ConnectionService$State.IDLE;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State[com.navdy.service.library.device.connection.ConnectionService$State.PAIRING.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State[com.navdy.service.library.device.connection.ConnectionService$State.LISTENING.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State[com.navdy.service.library.device.connection.ConnectionService$State.CONNECTING.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State[com.navdy.service.library.device.connection.ConnectionService$State.RECONNECTING.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State[com.navdy.service.library.device.connection.ConnectionService$State.DISCONNECTING.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State[com.navdy.service.library.device.connection.ConnectionService$State.CONNECTED.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State[com.navdy.service.library.device.connection.ConnectionService$State.START.ordinal()] = 8;
        } catch(NoSuchFieldError ignoredException6) {
        }
    }
}
