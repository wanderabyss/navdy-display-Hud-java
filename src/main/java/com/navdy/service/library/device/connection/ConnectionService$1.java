package com.navdy.service.library.device.connection;

import android.os.RemoteException;

class ConnectionService$1 extends com.navdy.hud.app.IEventSource.Stub {
    final com.navdy.service.library.device.connection.ConnectionService this$0;
    
    ConnectionService$1(com.navdy.service.library.device.connection.ConnectionService a) {
        super();
        this.this$0 = a;
    }
    
    public void addEventListener(com.navdy.hud.app.IEventListener a) {
        synchronized(this.this$0.listenerLock) {
            com.navdy.service.library.device.connection.ConnectionService.access$200(this.this$0).add(a);
            com.navdy.service.library.device.connection.ConnectionService.access$300(this.this$0);
            /*monexit(a0)*/;
        }
        this.this$0.logger.v(new StringBuilder().append("listener added: pid:").append(com.navdy.service.library.util.SystemUtils.getProcessName(this.this$0.getApplicationContext(), com.navdy.service.library.device.connection.ConnectionService$1.getCallingPid())).toString());
        com.navdy.service.library.device.RemoteDevice a5 = this.this$0.mRemoteDevice;
        label2: {
            label0: {
                label1: {
                    if (a5 == null) {
                        break label1;
                    }
                    if (this.this$0.mRemoteDevice.isConnected()) {
                        break label0;
                    }
                }
                this.this$0.forwardEventLocally((com.squareup.wire.Message)new com.navdy.service.library.events.connection.ConnectionStateChange("", com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED));
                this.this$0.forwardEventLocally((com.squareup.wire.Message)new com.navdy.service.library.events.connection.ConnectionStateChange("", com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_LINK_LOST));
                break label2;
            }
            this.this$0.forwardEventLocally((com.squareup.wire.Message)new com.navdy.service.library.events.connection.ConnectionStateChange(this.this$0.mRemoteDevice.getDeviceId().toString(), com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_LINK_ESTABLISHED));
            this.this$0.forwardEventLocally((com.squareup.wire.Message)new com.navdy.service.library.events.connection.ConnectionStateChange(this.this$0.mRemoteDevice.getDeviceId().toString(), com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_CONNECTED));
            com.navdy.service.library.events.DeviceInfo a6 = this.this$0.mRemoteDevice.getDeviceInfo();
            if (a6 != null) {
                this.this$0.logger.v("send device info");
                this.this$0.forwardEventLocally((com.squareup.wire.Message)a6);
            }
        }
        this.this$0.sendEventsOnLocalConnect();
    }
    
    public void postEvent(byte[] a) {
        boolean b = false;
        boolean b0 = false;
        int i = 0;
        synchronized (this.this$0.listenerLock) {
            while (i < com.navdy.service.library.device.connection.ConnectionService.access$400(this.this$0).length) {
                label0:
                {
                    android.os.TransactionTooLargeException a1 = null;
                    label1:
                    {
                        android.os.DeadObjectException a2 = null;
                        try {
                            try {
                                try {
                                    Object a3 = com.navdy.service.library.device.connection.ConnectionService.access$400(this.this$0)[i];
                                    ((com.navdy.hud.app.IEventListener) a3).onEvent(a);
                                    b = true;
                                    break label0;
                                } catch (android.os.DeadObjectException a4) {
                                    a2 = a4;
                                }
                            } catch (android.os.TransactionTooLargeException a5) {
                                a1 = a5;
                                break label1;
                            }
                        } catch (Throwable a6) {
                            this.this$0.logger.w("Exception throws by remote:", a6);
                            break label0;
                        }
                        this.this$0.logger.w("Reaping dead listener", (Throwable) a2);
                        java.util.List a7 = com.navdy.service.library.device.connection.ConnectionService.access$200(this.this$0);
                        Object a8 = com.navdy.service.library.device.connection.ConnectionService.access$400(this.this$0)[i];
                        a7.remove(a8);
                        b0 = true;
                        break label0;
                    }
                    this.this$0.logger.w("Communication Pipe is full:", (Throwable) a1);
                }
                i = i + 1;
            }
            if (this.this$0.logger.isLoggable(2) && !b) {
                this.this$0.logger.d(new StringBuilder().append("No one listening for event - byte length ").append(a.length).toString());
            }
            if (b0) {
                com.navdy.service.library.device.connection.ConnectionService.access$300(this.this$0);
            }
            /*monexit(a0)*/
            ;
        }
    }
    
    public void postRemoteEvent(String s, byte[] a) throws RemoteException {
        if (s != null && a != null) {
            com.navdy.service.library.device.NavdyDeviceId a0 = new com.navdy.service.library.device.NavdyDeviceId(s);
            label0: if (a0.equals(com.navdy.service.library.device.NavdyDeviceId.getThisDevice((android.content.Context)this.this$0))) {
                com.navdy.service.library.events.NavdyEvent.MessageType a1 = com.navdy.service.library.events.WireUtil.getEventType(a);
                label3: {
                    if (a1 == null) {
                        break label3;
                    }
                    if (this.this$0.processLocalEvent(a, a1)) {
                        break label0;
                    }
                }
                this.this$0.logger.w(new StringBuilder().append("Connection service ignored message:").append(a1).toString());
            } else {
                com.navdy.service.library.device.RemoteDevice a2 = this.this$0.mRemoteDevice;
                label1: {
                    label2: {
                        if (a2 == null) {
                            break label2;
                        }
                        if (this.this$0.mRemoteDevice.isConnected()) {
                            break label1;
                        }
                    }
                    if (!this.this$0.logger.isLoggable(2)) {
                        break label0;
                    }
                    this.this$0.logger.e("app not connected");
                    break label0;
                }
                if (this.this$0.mRemoteDevice.getDeviceId().equals(a0)) {
                    this.this$0.mRemoteDevice.postEvent(a);
                } else {
                    this.this$0.logger.i(new StringBuilder().append("Device id mismatch: deviceId=").append(a0).append(" remote:").append(this.this$0.mRemoteDevice.getDeviceId()).toString());
                }
            }
            return;
        }
        this.this$0.logger.e("illegal argument");
        throw new android.os.RemoteException("ILLEGAL_ARGUMENT");
    }
    
    public void removeEventListener(com.navdy.hud.app.IEventListener a) {
        synchronized(this.this$0.listenerLock) {
            com.navdy.service.library.device.connection.ConnectionService.access$200(this.this$0).remove(a);
            com.navdy.service.library.device.connection.ConnectionService.access$300(this.this$0);
            /*monexit(a0)*/;
        }
        this.this$0.logger.v(new StringBuilder().append("listener removed: pid:").append(com.navdy.service.library.util.SystemUtils.getProcessName(this.this$0.getApplicationContext(), com.navdy.service.library.device.connection.ConnectionService$1.getCallingPid())).toString());
    }
}
