package com.navdy.service.library.device;

class RemoteDevice$3 implements com.navdy.service.library.device.RemoteDevice$EventDispatcher {
    final com.navdy.service.library.device.RemoteDevice this$0;
    final com.navdy.service.library.device.connection.Connection$ConnectionFailureCause val$cause;
    
    RemoteDevice$3(com.navdy.service.library.device.RemoteDevice a, com.navdy.service.library.device.connection.Connection$ConnectionFailureCause a0) {
        super();
        this.this$0 = a;
        this.val$cause = a0;
    }
    
    public void dispatchEvent(com.navdy.service.library.device.RemoteDevice a, com.navdy.service.library.device.RemoteDevice$Listener a0) {
        a0.onDeviceConnectFailure(a, this.val$cause);
    }
    
    public void dispatchEvent(com.navdy.service.library.util.Listenable a, com.navdy.service.library.util.Listenable$Listener a0) {
        this.dispatchEvent((com.navdy.service.library.device.RemoteDevice)a, (com.navdy.service.library.device.RemoteDevice$Listener)a0);
    }
}
