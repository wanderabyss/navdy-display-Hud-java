package com.navdy.service.library.device.link;

public class LinkManager {
    private static com.navdy.service.library.device.link.LinkManager$LinkFactory sDefaultFactory;
    private static java.util.HashMap sFactoryMap;
    
    static {
        sFactoryMap = new java.util.HashMap();
        sDefaultFactory = (com.navdy.service.library.device.link.LinkManager$LinkFactory)new com.navdy.service.library.device.link.LinkManager$1();
        com.navdy.service.library.device.link.LinkManager.registerFactory(com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF, sDefaultFactory);
        com.navdy.service.library.device.link.LinkManager.registerFactory(com.navdy.service.library.device.connection.ConnectionType.TCP_PROTOBUF, sDefaultFactory);
    }
    
    public LinkManager() {
    }
    
    public static com.navdy.service.library.device.link.Link build(com.navdy.service.library.device.connection.ConnectionInfo a) {
        Object a0 = sFactoryMap.get(a.getType());
        com.navdy.service.library.device.link.Link a1 = (a0 == null) ? null : ((com.navdy.service.library.device.link.LinkManager$LinkFactory)a0).build(a);
        return a1;
    }
    
    public static void registerFactory(com.navdy.service.library.device.connection.ConnectionType a, com.navdy.service.library.device.link.LinkManager$LinkFactory a0) {
        if (a0 != null) {
            sFactoryMap.put(a, a0);
        }
    }
}
