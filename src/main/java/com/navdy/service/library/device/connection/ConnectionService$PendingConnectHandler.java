package com.navdy.service.library.device.connection;

class ConnectionService$PendingConnectHandler implements Runnable {
    private com.navdy.service.library.device.connection.Connection connection;
    private com.navdy.service.library.device.RemoteDevice device;
    final com.navdy.service.library.device.connection.ConnectionService this$0;
    
    public ConnectionService$PendingConnectHandler(com.navdy.service.library.device.connection.ConnectionService a, com.navdy.service.library.device.RemoteDevice a0, com.navdy.service.library.device.connection.Connection a1) {
        super();
        this.this$0 = a;
        this.device = a0;
        this.connection = a1;
    }
    
    public void run() {
        boolean b = this.this$0.mRemoteDevice != this.device;
        this.this$0.logger.d(new StringBuilder().append("Trying to ").append(b ? "connect" : "reconnect").append(" to ").append(this.device.getDeviceId().getDisplayName()).toString());
        this.this$0.setRemoteDevice(this.device);
        if (this.connection == null) {
            if (this.this$0.mRemoteDevice.isConnected()) {
                this.this$0.onDeviceConnected(this.this$0.mRemoteDevice);
            } else {
                this.this$0.setState(b ? com.navdy.service.library.device.connection.ConnectionService$State.CONNECTING : com.navdy.service.library.device.connection.ConnectionService$State.RECONNECTING);
                this.this$0.mRemoteDevice.connect();
            }
        } else {
            this.this$0.mRemoteDevice.setActiveConnection(this.connection);
        }
        com.navdy.service.library.device.connection.ConnectionService.access$502(this.this$0, (com.navdy.service.library.device.connection.ConnectionService$PendingConnectHandler)null);
    }
}
