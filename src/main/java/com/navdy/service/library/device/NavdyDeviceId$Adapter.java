package com.navdy.service.library.device;

import java.io.IOException;

public class NavdyDeviceId$Adapter extends com.google.gson.TypeAdapter {
    public NavdyDeviceId$Adapter() {
    }
    
    public Object read(com.google.gson.stream.JsonReader a) throws IOException {
        return new com.navdy.service.library.device.NavdyDeviceId(a.nextString());
    }
    
    public void write(com.google.gson.stream.JsonWriter a, Object a0) throws IOException {
        a.value(a0.toString());
    }
}
