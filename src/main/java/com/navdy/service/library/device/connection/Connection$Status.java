package com.navdy.service.library.device.connection;


public enum Connection$Status {
    DISCONNECTED(0),
    CONNECTING(1),
    CONNECTED(2),
    DISCONNECTING(3);

    private int value;
    Connection$Status(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class Connection$Status extends Enum {
//    final private static com.navdy.service.library.device.connection.Connection$Status[] $VALUES;
//    final public static com.navdy.service.library.device.connection.Connection$Status CONNECTED;
//    final public static com.navdy.service.library.device.connection.Connection$Status CONNECTING;
//    final public static com.navdy.service.library.device.connection.Connection$Status DISCONNECTED;
//    final public static com.navdy.service.library.device.connection.Connection$Status DISCONNECTING;
//    
//    static {
//        DISCONNECTED = new com.navdy.service.library.device.connection.Connection$Status("DISCONNECTED", 0);
//        CONNECTING = new com.navdy.service.library.device.connection.Connection$Status("CONNECTING", 1);
//        CONNECTED = new com.navdy.service.library.device.connection.Connection$Status("CONNECTED", 2);
//        DISCONNECTING = new com.navdy.service.library.device.connection.Connection$Status("DISCONNECTING", 3);
//        com.navdy.service.library.device.connection.Connection$Status[] a = new com.navdy.service.library.device.connection.Connection$Status[4];
//        a[0] = DISCONNECTED;
//        a[1] = CONNECTING;
//        a[2] = CONNECTED;
//        a[3] = DISCONNECTING;
//        $VALUES = a;
//    }
//    
//    private Connection$Status(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.service.library.device.connection.Connection$Status valueOf(String s) {
//        return (com.navdy.service.library.device.connection.Connection$Status)Enum.valueOf(com.navdy.service.library.device.connection.Connection$Status.class, s);
//    }
//    
//    public static com.navdy.service.library.device.connection.Connection$Status[] values() {
//        return $VALUES.clone();
//    }
//}
//