package com.navdy.service.library.log;

import java.io.IOException;

public class MemoryMapFileAppender implements com.navdy.service.library.log.LogAppender {
    final private static String COLON = ":";
    final private static String CURRENT_POINTER_PREF = "pointer";
    final private static String CURRENT_POINTER_PREF_FILE_SUFFIX = "_current_log_pointer";
    final private static long MIN_FILE_SIZE = 16384L;
    final private static String NEWLINE = "\r\n";
    final private static byte[] ROLLOVER_MARKER;
    final private static int ROLLOVER_META_LEN;
    final private static String SLASH = "/";
    final private static String SPACE = " ";
    final private static String TAG = "MemoryMapFileAppender";
    private StringBuilder builder;
    private android.content.Context context;
    private android.content.SharedPreferences.Editor currentPointerPrefEditor;
    private String currentPointerPrefFileName;
    private java.text.DateFormat dateFormat;
    private String fileName;
    private long fileSize;
    private int maxFiles;
    private java.nio.MappedByteBuffer memoryMap;
    
    static {
        ROLLOVER_MARKER = "\r\n<<<<rolling>>>>\r\n".getBytes();
        ROLLOVER_META_LEN = ROLLOVER_MARKER.length;
    }
    
    public MemoryMapFileAppender(android.content.Context a, String s, String s0, long j, int i) {
        this(a, s, s0, j, i, true);
    }
    
    public MemoryMapFileAppender(android.content.Context a, String s, String s0, long j, int i, boolean b) {
        this.maxFiles = 0;
        this.builder = new StringBuilder(20480);
        this.dateFormat = new java.text.SimpleDateFormat("MM-dd HH:mm:ss.SSS", java.util.Locale.US);
        if (a != null && !android.text.TextUtils.isEmpty((CharSequence)s0)) {
            this.maxFiles = i;
            if (j < 16384L) {
                j = 16384L;
            }
            String s1 = b ? new StringBuilder().append("_").append(com.navdy.service.library.util.SystemUtils.getProcessName(a, android.os.Process.myPid())).toString() : "";
            this.context = a;
            this.fileName = new StringBuilder().append(s).append(java.io.File.separator).append(s0).append(s1).toString();
            this.fileSize = j;
            this.currentPointerPrefFileName = new StringBuilder().append(s1).append("_current_log_pointer").toString();
            try {
                android.util.Log.d("MemoryMapFileAppender", "MemoryMapFileAppender::ctor::start");
                com.navdy.service.library.util.IOUtils.createDirectory(s);
                android.content.SharedPreferences a0 = a.getSharedPreferences(this.currentPointerPrefFileName, 0);
                int i0 = a0.getInt("pointer", 0);
                if (i0 > 0 && (long)i0 < j) {
                    a0.edit().remove("pointer").apply();
                    android.util.Log.i("MemoryMapFileAppender", new StringBuilder().append("MemoryMapFileAppender, pos set = ").append(i0).append(" for ").append(this.fileName).toString());
                }
                this.setMemoryMap(new StringBuilder().append(this.fileName).append(".txt").toString(), i0, j);
                android.util.Log.d("MemoryMapFileAppender", "MemoryMapFileAppender::ctor::end");
            } catch(Throwable a1) {
                android.util.Log.e("MemoryMapFileAppender", "MemoryMapFileAppender.ctor()::", a1);
            }
            return;
        }
        throw new IllegalArgumentException();
    }
    
    private String getFormatedNumber(int i) {
        int i0 = (int)(Math.log10((double)this.maxFiles) + 1.0);
        String s = new StringBuilder().append("%0").append(i0).append("d").toString();
        Object[] a = new Object[1];
        a[0] = Integer.valueOf(i);
        return String.format(s, a);
    }
    
    public static String getStackTraceString(Throwable a) {
        String s = null;
        if (a != null) {
            Throwable a0 = a;
            while(true) {
                if (a0 == null) {
                    java.io.StringWriter a1 = new java.io.StringWriter();
                    com.navdy.service.library.log.FastPrintWriter a2 = new com.navdy.service.library.log.FastPrintWriter((java.io.Writer)a1, false, 256);
                    a.printStackTrace((java.io.PrintWriter)a2);
                    ((java.io.PrintWriter)a2).flush();
                    s = a1.toString();
                    break;
                } else {
                    if (!(a0 instanceof java.net.UnknownHostException)) {
                        a0 = a0.getCause();
                        continue;
                    }
                    s = "";
                    break;
                }
            }
        } else {
            s = "";
        }
        return s;
    }
    
    private void rollFiles() {
        if (this.maxFiles > 0) {
            com.navdy.service.library.util.IOUtils.deleteFile(this.context, new StringBuilder().append(this.fileName).append(".").append(this.getFormatedNumber(0)).append(".txt").toString());
        }
        int i = 0;
        while(i < this.maxFiles) {
            java.io.File a = (i != this.maxFiles - 1) ? new java.io.File(new StringBuilder().append(this.fileName).append(".").append(this.getFormatedNumber(i + 1)).append(".txt").toString()) : new java.io.File(new StringBuilder().append(this.fileName).append(".txt").toString());
            if (a.exists() && !a.renameTo(new java.io.File(new StringBuilder().append(this.fileName).append(".").append(this.getFormatedNumber(i)).append(".txt").toString()))) {
                android.util.Log.w("MemoryMapFileAppender", new StringBuilder().append("Unable to rename ").append(this.fileName).append(".txt").toString());
            }
            i = i + 1;
        }
    }
    
    private void rollOver() {
        synchronized(this) {
            if (this.maxFiles != 0) {
                this.memoryMap.force();
                this.rollFiles();
                this.setMemoryMap(new StringBuilder().append(this.fileName).append(".txt").toString(), 0, this.fileSize);
            } else {
                this.memoryMap.position(0);
                this.memoryMap.put(ROLLOVER_MARKER);
            }
        }
        /*monexit(this)*/;
    }
    
    private void setMemoryMap(String s, int i, long j) {
        label2: {
            java.nio.channels.FileChannel a = null;
            java.io.RandomAccessFile a0 = null;
            Throwable a1 = null;
            label1: try {
                java.nio.channels.FileChannel a2 = null;
                a = null;
                java.io.File a3 = new java.io.File(s);
                if (i == 0) {
                    a = null;
                    if (a3.exists()) {
                        a = null;
                        this.rollFiles();
                    }
                }
                a = null;
                a0 = new java.io.RandomAccessFile(s, "rw");
                label0: {
                    java.nio.channels.FileChannel a4 = null;
                    try {
                        a4 = null;
                        a2 = a0.getChannel();
                        java.nio.channels.FileChannel.MapMode a5 = java.nio.channels.FileChannel.MapMode.READ_WRITE;
                        long j0 = 1L + j;
                        a4 = a2;
                        this.memoryMap = a2.map(a5, 0L, j0);
                        if (i > 0) {
                            a4 = a2;
                            this.memoryMap.position(i);
                        }
                        a4 = a2;
                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                        break label0;
                    } catch(Throwable a6) {
                        a1 = a6;
                    }
                    a = a4;
                    break label1;
                }
                a = a2;
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                break label2;
            } catch(Throwable a7) {
                a0 = null;
                a1 = a7;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
            this.memoryMap = null;
            android.util.Log.e("MemoryMapFileAppender", "setMemoryMap::", a1);
        }
    }
    
    private void truncateFileAtCurrentPosition(String s) {
        long j = (long)this.memoryMap.position();
        label8: {
            java.io.RandomAccessFile a = null;
            label7: {
                label6: {
                    label2: {
                        java.io.IOException a1 = null;
                        {
                            IOException a2 = null;
                            label1:
                            {
                                label3:
                                {
                                    java.io.FileNotFoundException a3 = null;
                                    {
                                        try {
                                            a = new java.io.RandomAccessFile(s, "rws");
                                        } catch (java.io.FileNotFoundException a4) {
                                            break label3;
                                        }
                                        try {
                                            try {
                                                a.setLength(j);
                                                break label2;
                                            } catch (java.io.FileNotFoundException a6) {
                                                a3 = a6;
                                            }
                                        } catch (IOException a7) {
                                            a2 = a7;
                                            break label1;
                                        }

                                    }
                                    a3.printStackTrace();
                                    break label6;
                                }
                                a = null;
                            }
                            a2.printStackTrace();
                            break label7;
                        }
                    }
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
                    break label8;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
                break label8;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
        }
    }
    
    protected void append(String s) {
        try {
            if (this.memoryMap != null && s != null) {
                byte[] a = s.getBytes();
                int i = a.length;
                if ((long)(ROLLOVER_META_LEN + i) < this.fileSize) {
                    if ((long)(this.memoryMap.position() + i) >= this.fileSize) {
                        this.rollOver();
                    }
                    this.memoryMap.put(a);
                }
            }
        } catch(Throwable a0) {
            android.util.Log.e("MemoryMapFileAppender", "MemoryMapFileAppender.append()::", a0);
        }
    }
    
    protected void append(String s, String s0, String s1, Throwable a) {
        label0: synchronized(this) {
                this.builder.setLength(0);
                this.builder.append(this.dateFormat.format(Long.valueOf(System.currentTimeMillis())));
                this.builder.append(" ");
                this.builder.append(s);
                this.builder.append("/");
                this.builder.append(s0);
                this.builder.append(":");
                this.builder.append(" ");
                if (s1 != null) {
                    this.builder.append(s1);
                }
                if (a != null) {
                    this.builder.append("\r\n");
                    this.builder.append(com.navdy.service.library.log.MemoryMapFileAppender.getStackTraceString(a));
                }
                this.builder.append("\r\n");
                this.append(this.builder.toString());
                /*monexit(this)*/;
                break label0;
        }
    }
    
    public void close() {
        synchronized(this) {
            android.util.Log.d("MemoryMapFileAppender", "MemoryMapFileAppender:closing");
            this.flush();
            this.memoryMap = null;
            /*monexit(this)*/;
        }
    }
    
    public void d(String s, String s0) {
        this.append("D", s, s0, (Throwable)null);
    }
    
    public void d(String s, String s0, Throwable a) {
        this.append("D", s, s0, a);
    }
    
    public void e(String s, String s0) {
        this.append("W", s, s0, (Throwable)null);
    }
    
    public void e(String s, String s0, Throwable a) {
        this.append("W", s, s0, a);
    }
    
    public void flush() {
        synchronized(this) {
            try {
                if (this.memoryMap != null) {
                    int i = this.memoryMap.position();
                    this.memoryMap.force();
                    if (i > 0) {
                        if (this.currentPointerPrefEditor == null) {
                            this.currentPointerPrefEditor = this.context.getSharedPreferences(this.currentPointerPrefFileName, 0).edit();
                        }
                        this.currentPointerPrefEditor.putInt("pointer", i).commit();
                        android.util.Log.d("MemoryMapFileAppender", new StringBuilder().append("MemoryMapFileAppender:stored pref pos:").append(i).toString());
                    }
                }
            } catch(Throwable a) {
                android.util.Log.d("MemoryMapFileAppender", "MemoryMapFileAppender:flush", a);
            }
            /*monexit(this)*/;
        }
    }
    
    public java.util.ArrayList getLogFiles() {
        java.util.ArrayList a = new java.util.ArrayList(this.maxFiles);
        int i = 0;
        while(i < this.maxFiles) {
            String s = new StringBuilder().append(this.fileName).append(".").append(this.getFormatedNumber(i)).append(".txt").toString();
            int i0 = this.maxFiles - 1;
            label0: {
                java.io.IOException a0 = null;
                if (i != i0) {
                    break label0;
                }
                try {
                    com.navdy.service.library.util.IOUtils.copyFile(new StringBuilder().append(this.fileName).append(".txt").toString(), s);
                    this.truncateFileAtCurrentPosition(s);
                    break label0;
                } catch(java.io.IOException a1) {
                    a0 = a1;
                }
                a0.printStackTrace();
            }
            java.io.File a2 = new java.io.File(s);
            if (a2.exists() && a2.length() > 0L) {
                a.add(a2);
            }
            i = i + 1;
        }
        return a;
    }
    
    public void i(String s, String s0) {
        this.append("I", s, s0, (Throwable)null);
    }
    
    public void i(String s, String s0, Throwable a) {
        this.append("I", s, s0, a);
    }
    
    public void v(String s, String s0) {
        this.append("V", s, s0, (Throwable)null);
    }
    
    public void v(String s, String s0, Throwable a) {
        this.append("V", s, s0, a);
    }
    
    public void w(String s, String s0) {
        this.append("W", s, s0, (Throwable)null);
    }
    
    public void w(String s, String s0, Throwable a) {
        this.append("W", s, s0, a);
    }
}
