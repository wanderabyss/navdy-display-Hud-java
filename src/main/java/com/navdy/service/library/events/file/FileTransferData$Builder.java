package com.navdy.service.library.events.file;

final public class FileTransferData$Builder extends com.squareup.wire.Message.Builder {
    public Integer chunkIndex;
    public okio.ByteString dataBytes;
    public String fileCheckSum;
    public Boolean lastChunk;
    public Integer transferId;
    
    public FileTransferData$Builder() {
    }
    
    public FileTransferData$Builder(com.navdy.service.library.events.file.FileTransferData a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.transferId = a.transferId;
            this.chunkIndex = a.chunkIndex;
            this.dataBytes = a.dataBytes;
            this.lastChunk = a.lastChunk;
            this.fileCheckSum = a.fileCheckSum;
        }
    }
    
    public com.navdy.service.library.events.file.FileTransferData build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.file.FileTransferData(this, (com.navdy.service.library.events.file.FileTransferData$1)null);
    }
    
//    public com.squareup.wire.Message build() {
//        return this.build();
//    }
    
    public com.navdy.service.library.events.file.FileTransferData$Builder chunkIndex(Integer a) {
        this.chunkIndex = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferData$Builder dataBytes(okio.ByteString a) {
        this.dataBytes = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferData$Builder fileCheckSum(String s) {
        this.fileCheckSum = s;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferData$Builder lastChunk(Boolean a) {
        this.lastChunk = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferData$Builder transferId(Integer a) {
        this.transferId = a;
        return this;
    }
}
