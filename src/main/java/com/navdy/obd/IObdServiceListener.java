package com.navdy.obd;

import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import java.util.List;
import android.os.RemoteException;
import android.os.IInterface;

public interface IObdServiceListener extends IInterface
{
    void onConnectionStateChange(final int p0) throws RemoteException;
    
    void onRawData(final String p0) throws RemoteException;
    
    void onStatusMessage(final String p0) throws RemoteException;
    
    void scannedPids(final List<Pid> p0) throws RemoteException;
    
    void scannedVIN(final String p0) throws RemoteException;
    
    void supportedPids(final List<Pid> p0) throws RemoteException;
    
    public abstract static class Stub extends Binder implements IObdServiceListener
    {
        private static final String DESCRIPTOR = "com.navdy.obd.IObdServiceListener";
        static final int TRANSACTION_onConnectionStateChange = 1;
        static final int TRANSACTION_onRawData = 3;
        static final int TRANSACTION_onStatusMessage = 2;
        static final int TRANSACTION_scannedPids = 4;
        static final int TRANSACTION_scannedVIN = 6;
        static final int TRANSACTION_supportedPids = 5;
        
        public Stub() {
            this.attachInterface((IInterface)this, "com.navdy.obd.IObdServiceListener");
        }
        
        public static IObdServiceListener asInterface(final IBinder binder) {
            IObdServiceListener obdServiceListener;
            if (binder == null) {
                obdServiceListener = null;
            }
            else {
                final IInterface queryLocalInterface = binder.queryLocalInterface("com.navdy.obd.IObdServiceListener");
                if (queryLocalInterface != null && queryLocalInterface instanceof IObdServiceListener) {
                    obdServiceListener = (IObdServiceListener)queryLocalInterface;
                }
                else {
                    obdServiceListener = new Proxy(binder);
                }
            }
            return obdServiceListener;
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            boolean onTransact = true;
            switch (n) {
                default:
                    onTransact = super.onTransact(n, parcel, parcel2, n2);
                    break;
                case 1598968902:
                    parcel2.writeString("com.navdy.obd.IObdServiceListener");
                    break;
                case 1:
                    parcel.enforceInterface("com.navdy.obd.IObdServiceListener");
                    this.onConnectionStateChange(parcel.readInt());
                    parcel2.writeNoException();
                    break;
                case 2:
                    parcel.enforceInterface("com.navdy.obd.IObdServiceListener");
                    this.onStatusMessage(parcel.readString());
                    parcel2.writeNoException();
                    break;
                case 3:
                    parcel.enforceInterface("com.navdy.obd.IObdServiceListener");
                    this.onRawData(parcel.readString());
                    parcel2.writeNoException();
                    break;
                case 4:
                    parcel.enforceInterface("com.navdy.obd.IObdServiceListener");
                    this.scannedPids(parcel.createTypedArrayList((Parcelable.Creator)Pid.CREATOR));
                    parcel2.writeNoException();
                    break;
                case 5:
                    parcel.enforceInterface("com.navdy.obd.IObdServiceListener");
                    this.supportedPids(parcel.createTypedArrayList((Parcelable.Creator)Pid.CREATOR));
                    parcel2.writeNoException();
                    break;
                case 6:
                    parcel.enforceInterface("com.navdy.obd.IObdServiceListener");
                    this.scannedVIN(parcel.readString());
                    parcel2.writeNoException();
                    break;
            }
            return onTransact;
        }
        
        private static class Proxy implements IObdServiceListener
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            public String getInterfaceDescriptor() {
                return "com.navdy.obd.IObdServiceListener";
            }
            
            @Override
            public void onConnectionStateChange(final int n) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IObdServiceListener");
                    obtain.writeInt(n);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void onRawData(final String s) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IObdServiceListener");
                    obtain.writeString(s);
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void onStatusMessage(final String s) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IObdServiceListener");
                    obtain.writeString(s);
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void scannedPids(final List<Pid> list) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IObdServiceListener");
                    obtain.writeTypedList((List)list);
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void scannedVIN(final String s) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IObdServiceListener");
                    obtain.writeString(s);
                    this.mRemote.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void supportedPids(final List<Pid> list) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IObdServiceListener");
                    obtain.writeTypedList((List)list);
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
