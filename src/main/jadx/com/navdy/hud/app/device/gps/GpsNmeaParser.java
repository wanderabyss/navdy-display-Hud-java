package com.navdy.hud.app.device.gps;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.text.TextUtils;
import com.navdy.hud.app.bluetooth.obex.HeaderSet;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import kotlin.text.Typography;

public class GpsNmeaParser {
    private static final String COLON = ":";
    private static final String COMMA = ",";
    private static final char COMMA_CHAR = ',';
    private static final String DB = "db";
    private static final String EQUAL = "=";
    public static final int FIX_TYPE_2D_3D = 1;
    public static final int FIX_TYPE_DIFFERENTIAL = 2;
    public static final int FIX_TYPE_DR = 6;
    public static final int FIX_TYPE_FRTK = 5;
    public static final int FIX_TYPE_NONE = 0;
    public static final int FIX_TYPE_PPS = 3;
    public static final int FIX_TYPE_RTK = 4;
    private static final String GGA = "GGA";
    private static final String GLL = "GLL";
    private static final String GNS = "GNS";
    private static final String GSV = "GSV";
    private static final String RMC = "RMC";
    private static final int SATELLITE_REPORT_INTERVAL = 5000;
    private static final String SPACE = " ";
    private static final String SVID = "SV-";
    private static final String VTG = "VTG";
    private int fixType;
    private Handler handler;
    private long lastSatelliteReportTime;
    private int messageCount;
    private ArrayList<String> messages = new ArrayList();
    private String[] nmeaResult = new String[200];
    private Logger sLogger;
    private HashMap<String, HashMap<Integer, Integer>> satellitesSeen = new HashMap();
    private HashMap<String, Integer> satellitesUsed = new HashMap();

    enum NmeaMessage {
        NOT_SUPPORTED,
        GGA,
        GLL,
        GNS,
        RMC,
        VTG,
        GSV
    }

    GpsNmeaParser(Logger logger, Handler handler) {
        this.sLogger = logger;
        this.handler = handler;
    }

    public void parseNmeaMessage(String nmea) {
        if (nmea != null) {
            char[] nmeaChars = nmea.toCharArray();
            if (nmeaChars.length >= 7 && nmeaChars[0] == Typography.dollar) {
                NmeaMessage nmeaType = getMessageType(nmeaChars[3], nmeaChars[4], nmeaChars[5]);
                if (nmeaType != NmeaMessage.NOT_SUPPORTED) {
                    for (int i = 0; i < this.nmeaResult.length; i++) {
                        this.nmeaResult[i] = null;
                    }
                    switch (nmeaType) {
                        case GGA:
                            processGGA(nmea);
                            return;
                        case GNS:
                            processGNS(nmea);
                            return;
                        case GSV:
                            processGSV(nmea);
                            return;
                        default:
                            return;
                    }
                }
            }
        }
    }

    static NmeaMessage getMessageType(char ch1, char ch2, char ch3) {
        switch (ch1) {
            case HeaderSet.HTTP /*71*/:
                if (ch2 == 'S' && ch3 == 'V') {
                    return NmeaMessage.GSV;
                }
                if (ch2 == 'G' && ch3 == 'A') {
                    return NmeaMessage.GGA;
                }
                if (ch2 == 'L' && ch3 == 'L') {
                    return NmeaMessage.GLL;
                }
                if (ch2 == 'N' && ch3 == 'S') {
                    return NmeaMessage.GNS;
                }
                return NmeaMessage.NOT_SUPPORTED;
            case 'R':
                if (ch2 == 'M' && ch3 == 'C') {
                    return NmeaMessage.RMC;
                }
                return NmeaMessage.NOT_SUPPORTED;
            case 'V':
                if (ch2 == 'T' && ch3 == 'G') {
                    return NmeaMessage.VTG;
                }
                return NmeaMessage.NOT_SUPPORTED;
            default:
                return NmeaMessage.NOT_SUPPORTED;
        }
    }

    private void processGGA(String nmea) {
        parseData(nmea, this.nmeaResult);
        try {
            this.fixType = Integer.parseInt(this.nmeaResult[6]);
        } catch (Throwable th) {
            this.fixType = 0;
        }
    }

    private void processGLL(String nmea) {
    }

    private void processGNS(String nmea) {
        parseData(nmea, this.nmeaResult);
        String gnsType = this.nmeaResult[0].substring(1, 3);
        String lat = this.nmeaResult[2];
        String lng = this.nmeaResult[4];
        String satellites = this.nmeaResult[7];
        if (TextUtils.isEmpty(lat) || TextUtils.isEmpty(lng) || TextUtils.isEmpty(satellites)) {
            this.satellitesUsed.put(gnsType, Integer.valueOf(0));
        } else {
            this.satellitesUsed.put(gnsType, Integer.valueOf(Integer.parseInt(satellites)));
        }
    }

    private void processRMC(String nmea) {
    }

    private void processVTG(String nmea) {
    }

    private void processGSV(String nmea) {
        if (parseData(nmea, this.nmeaResult) >= 8) {
            String gnsType = this.nmeaResult[0].substring(1, 3);
            int messageLen = Integer.parseInt(this.nmeaResult[1]);
            int messageId = Integer.parseInt(this.nmeaResult[2]);
            if (messageId == 1) {
                this.messages.clear();
                this.messageCount = messageLen;
            } else if (this.messages.size() == 0) {
                return;
            }
            this.messages.add(nmea);
            if (messageId == this.messageCount) {
                HashMap<Integer, Integer> data = (HashMap) this.satellitesSeen.get(gnsType);
                if (data == null) {
                    data = new HashMap();
                    this.satellitesSeen.put(gnsType, data);
                } else {
                    data.clear();
                }
                int len = this.messages.size();
                for (int i = 0; i < len; i++) {
                    int elements = parseData((String) this.messages.get(i), this.nmeaResult);
                    for (int j = 4; j + 4 < elements; j += 4) {
                        String satelliteId = this.nmeaResult[j];
                        String elevation = this.nmeaResult[j + 1];
                        String azmuth = this.nmeaResult[j + 2];
                        String cNO = this.nmeaResult[j + 3];
                        if (!TextUtils.isEmpty(satelliteId)) {
                            if (TextUtils.isEmpty(elevation) || TextUtils.isEmpty(azmuth) || TextUtils.isEmpty(cNO)) {
                                data.remove(satelliteId);
                            } else {
                                try {
                                    int nSatelliteId = Integer.parseInt(satelliteId);
                                    try {
                                        int cNOVal = Integer.parseInt(cNO);
                                        data.put(Integer.valueOf(nSatelliteId), Integer.valueOf(cNOVal));
                                    } catch (NumberFormatException e) {
                                    }
                                } catch (NumberFormatException e2) {
                                }
                            }
                        }
                    }
                }
                this.messageCount = 0;
                this.messages.clear();
                long now = SystemClock.elapsedRealtime();
                if (now - this.lastSatelliteReportTime >= 5000) {
                    sendGpsSatelliteStatus();
                    this.lastSatelliteReportTime = now;
                }
            }
        }
    }

    static int parseData(String nmea, String[] result) {
        int resultIndex;
        int resultIndex2 = 0;
        int start = 0;
        char[] ch = nmea.toCharArray();
        int i = 0;
        while (i < ch.length) {
            if (ch[i] == COMMA_CHAR) {
                resultIndex = resultIndex2 + 1;
                result[resultIndex2] = new String(ch, start, i - start);
                start = i + 1;
                resultIndex2 = resultIndex;
            }
            i++;
        }
        resultIndex = resultIndex2 + 1;
        result[resultIndex2] = new String(ch, start, (i - start) - 2);
        return resultIndex;
    }

    private void sendGpsSatelliteStatus() {
        Bundle bundle = new Bundle();
        int counter = 0;
        int maxDB = 0;
        for (Entry<String, HashMap<Integer, Integer>> data : this.satellitesSeen.entrySet()) {
            String key = (String) data.getKey();
            for (Entry<Integer, Integer> sData : ((HashMap) data.getValue()).entrySet()) {
                int id = ((Integer) sData.getKey()).intValue();
                int cno = ((Integer) sData.getValue()).intValue();
                counter++;
                bundle.putString(GpsConstants.GPS_SATELLITE_PROVIDER + counter, key);
                bundle.putInt(GpsConstants.GPS_SATELLITE_ID + counter, id);
                bundle.putInt(GpsConstants.GPS_SATELLITE_DB + counter, cno);
                if (cno > maxDB) {
                    maxDB = cno;
                }
            }
        }
        bundle.putInt(GpsConstants.GPS_SATELLITE_SEEN, counter);
        bundle.putInt(GpsConstants.GPS_SATELLITE_MAX_DB, maxDB);
        int nSatellitesUsed = 0;
        for (Entry<String, Integer> data2 : this.satellitesUsed.entrySet()) {
            nSatellitesUsed += ((Integer) data2.getValue()).intValue();
        }
        bundle.putInt(GpsConstants.GPS_SATELLITE_USED, nSatellitesUsed);
        bundle.putInt(GpsConstants.GPS_FIX_TYPE, this.fixType);
        Message msg = this.handler.obtainMessage(2);
        msg.obj = bundle;
        this.handler.sendMessage(msg);
    }

    private String getGnssProviderName(String id) {
        Object obj = -1;
        switch (id.hashCode()) {
            case 2266:
                if (id.equals("GA")) {
                    obj = 2;
                    break;
                }
                break;
            case 2267:
                if (id.equals("GB")) {
                    obj = 3;
                    break;
                }
                break;
            case 2277:
                if (id.equals("GL")) {
                    obj = 1;
                    break;
                }
                break;
            case 2279:
                if (id.equals("GN")) {
                    obj = 4;
                    break;
                }
                break;
            case 2281:
                if (id.equals("GP")) {
                    obj = null;
                    break;
                }
                break;
        }
        switch (obj) {
            case null:
                return "GPS";
            case 1:
                return "GLONASS";
            case 2:
                return "Galileo";
            case 3:
                return "BeiDou";
            case 4:
                return "MultiGNSS";
            default:
                return "UNK";
        }
    }
}
