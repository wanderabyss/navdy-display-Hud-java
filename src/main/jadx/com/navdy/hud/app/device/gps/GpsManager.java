package com.navdy.hud.app.device.gps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.GpsStatus.Listener;
import android.location.GpsStatus.NmeaListener;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.SystemClock;
import android.os.UserHandle;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.debug.RouteRecorder;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport;
import com.navdy.hud.app.service.HudConnectionService;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.events.DeviceInfo.Platform;
import com.navdy.service.library.events.audio.SpeechRequest;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.location.TransmitLocation;
import com.navdy.service.library.log.Logger;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class GpsManager {
    private static final int ACCEPTABLE_THRESHOLD = 2;
    private static final String DISCARD_TIME = "DISCARD_TIME";
    private static final String GPS_SYSTEM_PROPERTY = "persist.sys.hud_gps";
    public static final long INACCURATE_GPS_REPORT_INTERVAL = TimeUnit.SECONDS.toMillis(30);
    private static final int INITIAL_INTERVAL = 15000;
    private static final int KEEP_PHONE_GPS_ON = -1;
    private static final int LOCATION_ACCURACY_THROW_AWAY_THRESHOLD = 30;
    private static final int LOCATION_TIME_THROW_AWAY_THRESHOLD = 2000;
    public static final double MAXIMUM_STOPPED_SPEED = 0.22353333333333333d;
    private static final double METERS_PER_MILE = 1609.44d;
    public static final long MINIMUM_DRIVE_TIME = 3000;
    public static final double MINIMUM_DRIVING_SPEED = 2.2353333333333336d;
    static final int MSG_NMEA = 1;
    static final int MSG_SATELLITE_STATUS = 2;
    private static final int SECONDS_PER_HOUR = 3600;
    private static final String SET_LOCATION_DISCARD_TIME_THRESHOLD = "SET_LOCATION_DISCARD_TIME_THRESHOLD";
    private static final String SET_UBLOX_ACCURACY = "SET_UBLOX_ACCURACY";
    private static final TransmitLocation START_SENDING_LOCATION = new TransmitLocation(Boolean.valueOf(true));
    private static final String START_UBLOX = "START_REPORTING_UBLOX_LOCATION";
    private static final TransmitLocation STOP_SENDING_LOCATION = new TransmitLocation(Boolean.valueOf(false));
    private static final String STOP_UBLOX = "STOP_REPORTING_UBLOX_LOCATION";
    private static final String TAG_GPS = "[Gps-i] ";
    private static final String TAG_GPS_LOC = "[Gps-loc] ";
    private static final String TAG_GPS_LOC_NAVDY = "[Gps-loc-navdy] ";
    private static final String TAG_GPS_NMEA = "[Gps-nmea] ";
    private static final String TAG_GPS_STATUS = "[Gps-stat] ";
    private static final String TAG_PHONE_LOC = "[Phone-loc] ";
    private static final String UBLOX_ACCURACY = "UBLOX_ACCURACY";
    private static final float UBLOX_MIN_ACCEPTABLE_THRESHOLD = 5.0f;
    private static final long WARM_RESET_INTERVAL = TimeUnit.SECONDS.toMillis(160);
    private static final Logger sLogger = new Logger(GpsManager.class);
    private static final GpsManager singleton = new GpsManager();
    private LocationSource activeLocationSource;
    private HudConnectionService connectionService;
    private boolean driving;
    private BroadcastReceiver eventReceiver = new BroadcastReceiver() {
        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onReceive(Context context, Intent intent) {
            Object obj = null;
            try {
                String action = intent.getAction();
                if (action != null) {
                    RouteRecorder routeRecorder = RouteRecorder.getInstance();
                    switch (action.hashCode()) {
                        case -1801456507:
                            break;
                        case -1788590639:
                            if (action.equals(GpsConstants.GPS_EVENT_DEAD_RECKONING_STOPPED)) {
                                obj = 1;
                                break;
                            }
                        case -47674184:
                            if (action.equals(GpsConstants.EXTRAPOLATION)) {
                                obj = 2;
                                break;
                            }
                    }
                    obj = -1;
                    switch (obj) {
                        case null:
                            if (routeRecorder.isRecording()) {
                                String marker = intent.getStringExtra(GpsConstants.GPS_EXTRA_DR_TYPE);
                                if (marker == null) {
                                    marker = "";
                                }
                                routeRecorder.injectMarker("GPS_DR_STARTED " + marker);
                                return;
                            }
                            return;
                        case 1:
                            if (routeRecorder.isRecording()) {
                                routeRecorder.injectMarker(GpsConstants.GPS_EVENT_DEAD_RECKONING_STOPPED);
                                return;
                            }
                            return;
                        case 2:
                            GpsManager.this.extrapolationOn = intent.getBooleanExtra(GpsConstants.EXTRAPOLATION_FLAG, false);
                            if (GpsManager.this.extrapolationOn) {
                                GpsManager.this.extrapolationStartTime = SystemClock.elapsedRealtime();
                            } else {
                                GpsManager.this.extrapolationStartTime = 0;
                            }
                            GpsManager.sLogger.v("extrapolation is:" + GpsManager.this.extrapolationOn + " time:" + GpsManager.this.extrapolationStartTime);
                            return;
                        default:
                            return;
                    }
                }
            } catch (Throwable t) {
                GpsManager.sLogger.e(t);
            }
        }
    };
    private volatile boolean extrapolationOn;
    private volatile long extrapolationStartTime;
    private boolean firstSwitchToUbloxFromPhone;
    private float gpsAccuracy;
    private long gpsLastEventTime;
    private long gpsManagerStartTime;
    private Handler handler = new Handler(Looper.getMainLooper());
    private long inaccurateGpsCount;
    private boolean isDebugTTSEnabled;
    private boolean keepPhoneGpsOn;
    private long lastInaccurateGpsTime;
    private Coordinate lastPhoneCoordinate;
    private long lastPhoneCoordinateTime;
    private Location lastUbloxCoordinate;
    private long lastUbloxCoordinateTime;
    private Runnable locationFixRunnable = new Runnable() {
        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            boolean resetRunnable = true;
            try {
                long time = SystemClock.elapsedRealtime() - GpsManager.this.lastUbloxCoordinateTime;
                if (time >= 3000) {
                    GpsManager.sLogger.i("[Gps-loc] locationFixRunnable: lost location fix[" + time + "]");
                    if (GpsManager.this.extrapolationOn) {
                        long diff = SystemClock.elapsedRealtime() - GpsManager.this.extrapolationStartTime;
                        GpsManager.sLogger.i("[Gps-loc] locationFixRunnable: extrapolation on time:" + diff);
                        if (diff < 5000) {
                            GpsManager.sLogger.i("[Gps-loc] locationFixRunnable: reset");
                            if (1 != null) {
                                GpsManager.this.handler.postDelayed(GpsManager.this.locationFixRunnable, 1000);
                                return;
                            }
                            return;
                        }
                        GpsManager.this.extrapolationOn = false;
                        GpsManager.this.extrapolationStartTime = 0;
                        GpsManager.sLogger.i("[Gps-loc] locationFixRunnable: expired");
                    }
                    resetRunnable = false;
                    GpsManager.this.updateDrivingState(null);
                    GpsManager.sLogger.e("[Gps-loc] lost gps fix");
                    GpsManager.this.handler.removeCallbacks(GpsManager.this.locationFixRunnable);
                    GpsManager.this.handler.removeCallbacks(GpsManager.this.noPhoneLocationFixRunnable);
                    GpsManager.this.handler.postDelayed(GpsManager.this.noPhoneLocationFixRunnable, 5000);
                    GpsManager.this.startSendingLocation();
                    ConnectionServiceAnalyticsSupport.recordGpsLostLocation(String.valueOf((SystemClock.elapsedRealtime() - GpsManager.this.gpsLastEventTime) / 1000));
                    GpsManager.this.gpsLastEventTime = SystemClock.elapsedRealtime();
                }
                if (resetRunnable) {
                    GpsManager.this.handler.postDelayed(GpsManager.this.locationFixRunnable, 1000);
                }
            } catch (Throwable th) {
                if (1 != null) {
                    GpsManager.this.handler.postDelayed(GpsManager.this.locationFixRunnable, 1000);
                }
            }
        }
    };
    private LocationManager locationManager;
    private LocationListener navdyGpsLocationListener = new LocationListener() {
        private float accuracyMax = 0.0f;
        private float accuracyMin = 0.0f;
        private int accuracySampleCount = 0;
        private long accuracySampleStartTime = SystemClock.elapsedRealtime();
        private float accuracySum = 0.0f;

        private void collectAccuracyStats(float accuracy) {
            if (accuracy != 0.0f) {
                this.accuracySum += accuracy;
                this.accuracySampleCount++;
                if (this.accuracyMin == 0.0f || accuracy < this.accuracyMin) {
                    this.accuracyMin = accuracy;
                }
                if (this.accuracyMax < accuracy) {
                    this.accuracyMax = accuracy;
                }
            }
            if (GpsManager.this.lastUbloxCoordinateTime - this.accuracySampleStartTime > 300000) {
                if (this.accuracySampleCount > 0) {
                    ConnectionServiceAnalyticsSupport.recordGpsAccuracy(Integer.toString(Math.round(this.accuracyMin)), Integer.toString(Math.round(this.accuracyMax)), Integer.toString(Math.round(this.accuracySum / ((float) this.accuracySampleCount))));
                    this.accuracySum = 0.0f;
                    this.accuracyMax = 0.0f;
                    this.accuracyMin = 0.0f;
                    this.accuracySampleCount = 0;
                }
                this.accuracySampleStartTime = GpsManager.this.lastUbloxCoordinateTime;
            }
        }

        public void onLocationChanged(Location location) {
            try {
                if (!GpsManager.this.warmResetCancelled) {
                    GpsManager.this.cancelUbloxResetRunnable();
                }
                if (!RouteRecorder.getInstance().isPlaying()) {
                    GpsManager.this.updateDrivingState(location);
                    GpsManager.this.lastUbloxCoordinate = location;
                    GpsManager.this.lastUbloxCoordinateTime = SystemClock.elapsedRealtime();
                    collectAccuracyStats(location.getAccuracy());
                    if (GpsManager.this.connectionService != null && GpsManager.this.connectionService.isConnected()) {
                        if (GpsManager.this.activeLocationSource != LocationSource.UBLOX) {
                            long time = SystemClock.elapsedRealtime() - GpsManager.this.lastPhoneCoordinateTime;
                            if (time > 3000) {
                                GpsManager.sLogger.v("phone threshold exceeded= " + time + ", start uBloxReporting");
                                GpsManager.this.startUbloxReporting();
                                return;
                            }
                        }
                        if (!GpsManager.this.firstSwitchToUbloxFromPhone || GpsManager.this.switchBetweenPhoneUbloxAllowed) {
                            GpsManager.this.checkGpsToPhoneAccuracy(location);
                        }
                    } else if (GpsManager.this.activeLocationSource != LocationSource.UBLOX) {
                        GpsManager.sLogger.v("not connected with phone, start uBloxReporting");
                        GpsManager.this.startUbloxReporting();
                    }
                }
            } catch (Throwable t) {
                GpsManager.sLogger.e(t);
            }
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };
    private Callback nmeaCallback = new Callback() {
        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean handleMessage(Message msg) {
            try {
                switch (msg.what) {
                    case 1:
                        GpsManager.this.nmeaParser.parseNmeaMessage((String) msg.obj);
                        break;
                    case 2:
                        if (SystemClock.elapsedRealtime() - GpsManager.this.gpsManagerStartTime > 15000) {
                            Bundle dataBundle = msg.obj;
                            Bundle bundle = new Bundle();
                            bundle.putBundle(GpsConstants.GPS_EVENT_SATELLITE_DATA, dataBundle);
                            GpsManager.this.sendGpsEventBroadcast(GpsConstants.GPS_SATELLITE_STATUS, bundle);
                            break;
                        }
                        break;
                }
            } catch (Throwable t) {
                GpsManager.sLogger.e(t);
            }
            return true;
        }
    };
    private Handler nmeaHandler;
    private HandlerThread nmeaHandlerThread;
    private GpsNmeaParser nmeaParser;
    private Runnable noLocationUpdatesRunnable = new Runnable() {
        public void run() {
            GpsManager.this.updateDrivingState(null);
        }
    };
    private Runnable noPhoneLocationFixRunnable = new Runnable() {
        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            boolean resetRunnable = true;
            try {
                if (GpsManager.this.activeLocationSource == LocationSource.UBLOX) {
                    GpsManager.sLogger.i("[Gps-loc] noPhoneLocationFixRunnable: has location fix now");
                    resetRunnable = false;
                } else {
                    GpsManager.this.updateDrivingState(null);
                    GpsManager.this.startSendingLocation();
                }
                if (resetRunnable) {
                    GpsManager.this.handler.postDelayed(GpsManager.this.noPhoneLocationFixRunnable, 5000);
                }
            } catch (Throwable th) {
                if (1 != null) {
                    GpsManager.this.handler.postDelayed(GpsManager.this.noPhoneLocationFixRunnable, 5000);
                }
            }
        }
    };
    private ArrayList<Intent> queue = new ArrayList();
    private Runnable queueCallback = new Runnable() {
        public void run() {
            try {
                Context context = HudApplication.getAppContext();
                UserHandle handle = Process.myUserHandle();
                int len = GpsManager.this.queue.size();
                if (len > 0) {
                    for (int i = 0; i < len; i++) {
                        Intent intent = (Intent) GpsManager.this.queue.get(i);
                        context.sendBroadcastAsUser(intent, handle);
                        GpsManager.sLogger.v("sent-queue gps event user:" + intent.getAction());
                    }
                    GpsManager.this.queue.clear();
                }
            } catch (Throwable t) {
                GpsManager.sLogger.e(t);
            }
        }
    };
    private volatile boolean startUbloxCalled;
    private boolean switchBetweenPhoneUbloxAllowed;
    private long transitionStartTime;
    private boolean transitioning;
    private LocationListener ubloxGpsLocationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            try {
                if (GpsManager.sLogger.isLoggable(2)) {
                    GpsManager.sLogger.d(GpsManager.TAG_GPS_LOC + (location.isFromMockProvider() ? "[Mock] " : "") + location);
                }
                if (GpsManager.this.startUbloxCalled && !location.isFromMockProvider() && GpsManager.this.activeLocationSource != LocationSource.UBLOX) {
                    float accuracy;
                    Object obj;
                    LocationSource last = GpsManager.this.activeLocationSource;
                    GpsManager.this.activeLocationSource = LocationSource.UBLOX;
                    GpsManager.this.handler.removeCallbacks(GpsManager.this.noPhoneLocationFixRunnable);
                    GpsManager.this.handler.removeCallbacks(GpsManager.this.locationFixRunnable);
                    GpsManager.this.handler.postDelayed(GpsManager.this.locationFixRunnable, 1000);
                    GpsManager.this.stopSendingLocation();
                    int second = ((int) (SystemClock.elapsedRealtime() - GpsManager.this.gpsLastEventTime)) / 1000;
                    float phoneAccuracy = -1.0f;
                    long elapsedTime = SystemClock.elapsedRealtime() - GpsManager.this.lastPhoneCoordinateTime;
                    if (GpsManager.this.lastPhoneCoordinate != null && elapsedTime < 3000) {
                        phoneAccuracy = GpsManager.this.lastPhoneCoordinate.accuracy.floatValue();
                    }
                    if (GpsManager.this.lastUbloxCoordinate != null) {
                        accuracy = GpsManager.this.lastUbloxCoordinate.getAccuracy();
                    } else {
                        accuracy = -1.0f;
                    }
                    int accuracy2 = (int) accuracy;
                    ConnectionServiceAnalyticsSupport.recordGpsAcquireLocation(String.valueOf(second), String.valueOf(accuracy2));
                    GpsManager.this.gpsLastEventTime = SystemClock.elapsedRealtime();
                    GpsManager.this.markLocationFix(GpsConstants.DEBUG_TTS_UBLOX_SWITCH, "Phone =" + (phoneAccuracy == -1.0f ? "n/a" : Float.valueOf(phoneAccuracy)) + " ublox =" + accuracy2, false, true);
                    Logger access$000 = GpsManager.sLogger;
                    StringBuilder append = new StringBuilder().append("[Gps-loc] [LocationProvider] switched from [").append(last).append("] to [").append(LocationSource.UBLOX).append("] ").append("Phone =");
                    if (phoneAccuracy == -1.0f) {
                        obj = "n/a";
                    } else {
                        obj = Float.valueOf(phoneAccuracy);
                    }
                    access$000.v(append.append(obj).append(" ublox =").append(accuracy2).append(" time=").append(elapsedTime).toString());
                }
            } catch (Throwable t) {
                GpsManager.sLogger.e(t);
            }
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (PowerManager.isAwake()) {
                GpsManager.sLogger.i(GpsManager.TAG_GPS_STATUS + provider + HereManeuverDisplayBuilder.COMMA + status);
            }
        }

        public void onProviderEnabled(String provider) {
            GpsManager.sLogger.i(GpsManager.TAG_GPS_STATUS + provider + "[enabled]");
        }

        public void onProviderDisabled(String provider) {
            GpsManager.sLogger.i(GpsManager.TAG_GPS_STATUS + provider + "[disabled]");
        }
    };
    private NmeaListener ubloxGpsNmeaListener = new NmeaListener() {
        public void onNmeaReceived(long timestamp, String nmea) {
            try {
                if (GpsManager.sLogger.isLoggable(2)) {
                    GpsManager.sLogger.v(GpsManager.TAG_GPS_NMEA + nmea);
                }
                GpsManager.this.processNmea(nmea);
            } catch (Throwable t) {
                GpsManager.sLogger.e(t);
            }
        }
    };
    private Listener ubloxGpsStatusListener = new Listener() {
        public void onGpsStatusChanged(int event) {
            switch (event) {
                case 1:
                    GpsManager.sLogger.i("[Gps-stat] gps searching...");
                    return;
                case 2:
                    GpsManager.sLogger.w("[Gps-stat] gps stopped searching");
                    return;
                case 3:
                    GpsManager.sLogger.i("[Gps-stat] gps got first fix");
                    return;
                default:
                    return;
            }
        }
    };
    private boolean warmResetCancelled;
    private Runnable warmResetRunnable = new Runnable() {
        public void run() {
            try {
                GpsManager.sLogger.v("issuing warm reset to ublox, no location report in last " + GpsManager.WARM_RESET_INTERVAL);
                GpsManager.this.sendGpsEventBroadcast(GpsConstants.GPS_EVENT_WARM_RESET_UBLOX, null);
            } catch (Throwable t) {
                GpsManager.sLogger.e(t);
            }
        }
    };

    enum LocationSource {
        UBLOX,
        PHONE
    }

    public static GpsManager getInstance() {
        return singleton;
    }

    private GpsManager() {
        Context context = HudApplication.getAppContext();
        try {
            int n = SystemProperties.getInt(GPS_SYSTEM_PROPERTY, 0);
            if (n == -1) {
                this.keepPhoneGpsOn = true;
                this.switchBetweenPhoneUbloxAllowed = true;
                sLogger.v("switch between phone and ublox allowed");
            }
            sLogger.v("keepPhoneGpsOn[" + this.keepPhoneGpsOn + "] val[" + n + "]");
            this.isDebugTTSEnabled = new File("/sdcard/debug_tts").exists();
            setUbloxAccuracyThreshold();
            setUbloxTimeDiscardThreshold();
        } catch (Throwable t) {
            sLogger.e(t);
        }
        LocationProvider gpsProvider = null;
        this.locationManager = (LocationManager) context.getSystemService("location");
        if (this.locationManager.isProviderEnabled("gps")) {
            gpsProvider = this.locationManager.getProvider("gps");
            if (gpsProvider != null) {
                this.nmeaHandlerThread = new HandlerThread("navdynmea");
                this.nmeaHandlerThread.start();
                this.nmeaHandler = new Handler(this.nmeaHandlerThread.getLooper(), this.nmeaCallback);
                this.nmeaParser = new GpsNmeaParser(sLogger, this.nmeaHandler);
                this.locationManager.addGpsStatusListener(this.ubloxGpsStatusListener);
                this.locationManager.addNmeaListener(this.ubloxGpsNmeaListener);
            }
        }
        if (DeviceUtil.isNavdyDevice()) {
            sLogger.i("[Gps-i] setting up ublox gps");
            if (gpsProvider != null) {
                this.locationManager.requestLocationUpdates("gps", 0, 0.0f, this.ubloxGpsLocationListener);
                try {
                    this.locationManager.requestLocationUpdates(GpsConstants.NAVDY_GPS_PROVIDER, 0, 0.0f, this.navdyGpsLocationListener);
                    sLogger.v("requestLocationUpdates successful for NAVDY_GPS_PROVIDER");
                } catch (Throwable t2) {
                    sLogger.e("requestLocationUpdates", t2);
                }
                sLogger.i("[Gps-i] ublox gps listeners installed");
            } else {
                sLogger.e("[Gps-i] gps provider not found");
            }
        } else {
            sLogger.i("[Gps-i] not a Hud device,not setting up ublox gps");
        }
        this.locationManager.addTestProvider("network", false, true, false, false, true, true, true, 0, 3);
        this.locationManager.setTestProviderEnabled("network", true);
        this.locationManager.setTestProviderStatus("network", 2, null, System.currentTimeMillis());
        sLogger.i("[Gps-i] added mock network provider");
        this.gpsManagerStartTime = SystemClock.elapsedRealtime();
        this.gpsLastEventTime = this.gpsManagerStartTime;
        ConnectionServiceAnalyticsSupport.recordGpsAttemptAcquireLocation();
        if (DeviceUtil.isNavdyDevice()) {
            setUbloxResetRunnable();
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(GpsConstants.GPS_EVENT_DEAD_RECKONING_STARTED);
        filter.addAction(GpsConstants.GPS_EVENT_DEAD_RECKONING_STOPPED);
        filter.addAction(GpsConstants.EXTRAPOLATION);
        context.registerReceiver(this.eventReceiver, filter);
    }

    public void setConnectionService(HudConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    public void feedLocation(Coordinate coordinate) {
        boolean hasAtleastOneLocation = this.lastPhoneCoordinateTime > 0 || this.lastUbloxCoordinateTime > 0;
        long now = System.currentTimeMillis();
        if (coordinate.accuracy.floatValue() < 30.0f || !hasAtleastOneLocation) {
            long timeDiff = now - coordinate.timestamp.longValue();
            if (timeDiff >= 2000 && hasAtleastOneLocation) {
                sLogger.e("OLD location from phone(discard) time:" + timeDiff + ", timestamp:" + coordinate.timestamp + " now:" + now + " lat:" + coordinate.latitude + " lng:" + coordinate.longitude);
                return;
            } else if (this.lastPhoneCoordinate != null && this.connectionService.getDevicePlatform() == Platform.PLATFORM_iOS && this.lastPhoneCoordinate.latitude == coordinate.latitude && this.lastPhoneCoordinate.longitude == coordinate.longitude && this.lastPhoneCoordinate.accuracy == coordinate.accuracy && this.lastPhoneCoordinate.altitude == coordinate.altitude && this.lastPhoneCoordinate.bearing == coordinate.bearing && this.lastPhoneCoordinate.speed == coordinate.speed) {
                sLogger.e("same location(discard) diff=" + (coordinate.timestamp.longValue() - this.lastPhoneCoordinate.timestamp.longValue()));
                return;
            } else {
                this.lastPhoneCoordinate = coordinate;
                this.lastPhoneCoordinateTime = SystemClock.elapsedRealtime();
                this.handler.removeCallbacks(this.noPhoneLocationFixRunnable);
                Location androidLocation = null;
                RouteRecorder routeRecorder = RouteRecorder.getInstance();
                if (routeRecorder.isRecording()) {
                    androidLocation = androidLocationFromCoordinate(coordinate);
                    routeRecorder.injectLocation(androidLocation, true);
                }
                if (this.activeLocationSource == null) {
                    sLogger.v("[Gps-loc] [LocationProvider] switched from [" + this.activeLocationSource + "] to [" + LocationSource.PHONE + "]");
                    stopUbloxReporting();
                    this.activeLocationSource = LocationSource.PHONE;
                    feedLocationToProvider(androidLocation, coordinate);
                    markLocationFix(GpsConstants.DEBUG_TTS_PHONE_SWITCH, "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox=n/a", true, false);
                    return;
                } else if (this.activeLocationSource == LocationSource.PHONE) {
                    feedLocationToProvider(androidLocation, coordinate);
                    return;
                } else if (SystemClock.elapsedRealtime() - this.lastUbloxCoordinateTime > 3000) {
                    sLogger.v("[Gps-loc] [LocationProvider] switched from [" + this.activeLocationSource + "] to [" + LocationSource.PHONE + "]");
                    this.activeLocationSource = LocationSource.PHONE;
                    stopUbloxReporting();
                    feedLocationToProvider(androidLocation, coordinate);
                    markLocationFix(GpsConstants.DEBUG_TTS_PHONE_SWITCH, "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox =lost", true, false);
                    return;
                } else {
                    return;
                }
            }
        }
        this.inaccurateGpsCount++;
        this.gpsAccuracy += coordinate.accuracy.floatValue();
        if (this.lastInaccurateGpsTime == 0 || now - this.lastInaccurateGpsTime >= INACCURATE_GPS_REPORT_INTERVAL) {
            sLogger.e("BAD gps accuracy (discarding) avg:" + (this.gpsAccuracy / ((float) this.inaccurateGpsCount)) + ", count=" + this.inaccurateGpsCount + ", last coord: " + coordinate);
            this.inaccurateGpsCount = 0;
            this.gpsAccuracy = 0.0f;
            this.lastInaccurateGpsTime = now;
        }
    }

    private void feedLocationToProvider(Location androidLocation, Coordinate coordinate) {
        try {
            if (sLogger.isLoggable(3)) {
                sLogger.d(TAG_PHONE_LOC + coordinate);
            }
            if (androidLocation == null) {
                androidLocation = androidLocationFromCoordinate(coordinate);
            }
            updateDrivingState(androidLocation);
            this.locationManager.setTestProviderLocation(androidLocation.getProvider(), androidLocation);
        } catch (Throwable t) {
            sLogger.e("feedLocation", t);
        }
    }

    private static Location androidLocationFromCoordinate(Coordinate c) {
        Location location = new Location("network");
        location.setLatitude(c.latitude.doubleValue());
        location.setLongitude(c.longitude.doubleValue());
        location.setAccuracy(c.accuracy.floatValue());
        location.setAltitude(c.altitude.doubleValue());
        location.setBearing(c.bearing.floatValue());
        location.setSpeed(c.speed.floatValue());
        location.setTime(System.currentTimeMillis());
        location.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());
        return location;
    }

    private void startSendingLocation() {
        if (this.connectionService != null && this.connectionService.isConnected()) {
            try {
                sLogger.i("[Gps-loc] phone start sending location");
                this.connectionService.sendMessage(START_SENDING_LOCATION);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    private void setDriving(boolean driving) {
        if (this.driving != driving) {
            this.driving = driving;
            sendGpsEventBroadcast(driving ? GpsConstants.GPS_EVENT_DRIVING_STARTED : GpsConstants.GPS_EVENT_DRIVING_STOPPED, null);
        }
    }

    private void updateDrivingState(Location location) {
        if (this.driving) {
            if (location != null && ((double) location.getSpeed()) >= 0.22353333333333333d) {
                this.transitioning = false;
            } else if (!this.transitioning) {
                this.transitioning = true;
                this.transitionStartTime = SystemClock.elapsedRealtime();
            } else if (SystemClock.elapsedRealtime() - this.transitionStartTime > 3000) {
                setDriving(false);
                this.transitioning = false;
            }
        } else if (location == null || ((double) location.getSpeed()) <= 2.2353333333333336d) {
            this.transitioning = false;
        } else if (!this.transitioning) {
            this.transitioning = true;
            this.transitionStartTime = SystemClock.elapsedRealtime();
        } else if (SystemClock.elapsedRealtime() - this.transitionStartTime > 3000) {
            setDriving(true);
            this.transitioning = false;
        }
        this.handler.removeCallbacks(this.noLocationUpdatesRunnable);
        if (this.driving) {
            this.handler.postDelayed(this.noLocationUpdatesRunnable, 3000);
        }
    }

    private void stopSendingLocation() {
        if (this.keepPhoneGpsOn) {
            sLogger.v("stopSendingLocation: keeping phone gps on");
        } else if (this.isDebugTTSEnabled) {
            sLogger.v("stopSendingLocation: not stopping, debug_tts enabled");
        } else if (this.connectionService != null && this.connectionService.isConnected()) {
            try {
                sLogger.i("[Gps-loc] phone stop sending location");
                this.connectionService.sendMessage(STOP_SENDING_LOCATION);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    private void sendSpeechRequest(SpeechRequest speechRequest) {
        if (this.connectionService != null && this.connectionService.isConnected()) {
            try {
                sLogger.i("[Gps-loc] send speech request");
                this.connectionService.sendMessage(speechRequest);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    private void processNmea(String nmea) {
        Message msg = this.nmeaHandler.obtainMessage(1);
        msg.obj = nmea;
        this.nmeaHandler.sendMessage(msg);
    }

    private void sendGpsEventBroadcast(String eventName, Bundle extras) {
        try {
            Intent intent = new Intent(eventName);
            if (extras != null) {
                intent.putExtras(extras);
            }
            if (SystemClock.elapsedRealtime() - this.gpsManagerStartTime <= 15000) {
                this.queue.add(intent);
                this.handler.removeCallbacks(this.queueCallback);
                this.handler.postDelayed(this.queueCallback, 15000);
                return;
            }
            Context context = HudApplication.getAppContext();
            UserHandle handle = Process.myUserHandle();
            if (this.queue.size() > 0) {
                this.queueCallback.run();
            }
            context.sendBroadcastAsUser(intent, handle);
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public void startUbloxReporting() {
        try {
            HudApplication.getAppContext().sendBroadcastAsUser(new Intent(START_UBLOX), Process.myUserHandle());
            this.startUbloxCalled = true;
            sLogger.v("started ublox reporting");
        } catch (Throwable t) {
            sLogger.e("startUbloxReporting", t);
        }
    }

    public void stopUbloxReporting() {
        try {
            this.startUbloxCalled = false;
            HudApplication.getAppContext().sendBroadcastAsUser(new Intent(STOP_UBLOX), Process.myUserHandle());
            sLogger.v("stopped ublox reporting");
        } catch (Throwable t) {
            sLogger.e("stopUbloxReporting", t);
        }
    }

    private void setUbloxAccuracyThreshold() {
        try {
            Context context = HudApplication.getAppContext();
            UserHandle handle = Process.myUserHandle();
            Intent intent = new Intent(SET_UBLOX_ACCURACY);
            intent.putExtra(UBLOX_ACCURACY, 30);
            context.sendBroadcastAsUser(intent, handle);
            sLogger.v("setUbloxAccuracyThreshold:30");
        } catch (Throwable t) {
            sLogger.e("setUbloxAccuracyThreshold", t);
        }
    }

    private void setUbloxTimeDiscardThreshold() {
        try {
            Context context = HudApplication.getAppContext();
            UserHandle handle = Process.myUserHandle();
            Intent intent = new Intent(SET_LOCATION_DISCARD_TIME_THRESHOLD);
            intent.putExtra(DISCARD_TIME, 2000);
            context.sendBroadcastAsUser(intent, handle);
            sLogger.v("setUbloxTimeDiscardThreshold:2000");
        } catch (Throwable t) {
            sLogger.e("setUbloxTimeDiscardThreshold", t);
        }
    }

    private void checkGpsToPhoneAccuracy(Location location) {
        float accuracy = location.getAccuracy();
        long time = SystemClock.elapsedRealtime() - this.lastPhoneCoordinateTime;
        if (sLogger.isLoggable(3)) {
            sLogger.v("[Gps-loc-navdy] checkGpsToPhoneAccuracy: ublox[" + location.getAccuracy() + "] phone[" + (this.lastPhoneCoordinate != null ? this.lastPhoneCoordinate.accuracy : "n/a") + "] lastPhoneTime [" + (this.lastPhoneCoordinate != null ? Long.valueOf(time) : "n/a") + "]");
        }
        if (accuracy != 0.0f) {
            if (this.activeLocationSource == LocationSource.UBLOX) {
                if (this.lastPhoneCoordinate != null && SystemClock.elapsedRealtime() - this.lastPhoneCoordinateTime <= 3000) {
                    if (this.lastPhoneCoordinate.accuracy.floatValue() + 2.0f < accuracy) {
                        if (sLogger.isLoggable(3)) {
                            sLogger.v("[Gps-loc-navdy]  ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "]");
                        }
                        if (!this.startUbloxCalled) {
                            return;
                        }
                        if (accuracy <= UBLOX_MIN_ACCEPTABLE_THRESHOLD) {
                            sLogger.v("ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "], but not above threshold");
                            stopSendingLocation();
                            return;
                        }
                        sLogger.v("ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "], stop uBloxReporting");
                        this.handler.removeCallbacks(this.locationFixRunnable);
                        this.handler.removeCallbacks(this.noPhoneLocationFixRunnable);
                        stopUbloxReporting();
                        sLogger.v("[Gps-loc] [LocationProvider] switched from [" + this.activeLocationSource + "] to [" + LocationSource.PHONE + "]");
                        this.activeLocationSource = LocationSource.PHONE;
                        markLocationFix(GpsConstants.DEBUG_TTS_PHONE_SWITCH, "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox =" + accuracy, true, false);
                    } else if (sLogger.isLoggable(3)) {
                        sLogger.v("ublox accuracy [" + accuracy + "] < phone accuracy[" + this.lastPhoneCoordinate.accuracy + "] no action");
                    }
                }
            } else if (accuracy + 2.0f < this.lastPhoneCoordinate.accuracy.floatValue()) {
                sLogger.v("ublox accuracy [" + accuracy + "] < phone accuracy[" + this.lastPhoneCoordinate.accuracy + "], start uBloxReporting, switch complete");
                this.firstSwitchToUbloxFromPhone = true;
                markLocationFix(GpsConstants.DEBUG_TTS_UBLOX_SWITCH, "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox =" + accuracy, false, false);
                startUbloxReporting();
            } else if (sLogger.isLoggable(3)) {
                sLogger.v("ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "] no action");
            }
        }
    }

    private void markLocationFix(String title, String info, boolean usingPhone, boolean usingUblox) {
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("info", info);
        bundle.putBoolean(GpsConstants.USING_PHONE_LOCATION, usingPhone);
        bundle.putBoolean(GpsConstants.USING_UBLOX_LOCATION, usingUblox);
        sendGpsEventBroadcast(GpsConstants.GPS_EVENT_SWITCH, bundle);
    }

    private void setUbloxResetRunnable() {
        sLogger.v("setUbloxResetRunnable");
        this.handler.postDelayed(this.warmResetRunnable, WARM_RESET_INTERVAL);
    }

    private void cancelUbloxResetRunnable() {
        this.handler.removeCallbacks(this.warmResetRunnable);
        this.warmResetCancelled = true;
    }

    public void shutdown() {
        if (this.locationManager != null) {
            this.locationManager.removeUpdates(this.ubloxGpsLocationListener);
            this.locationManager.removeUpdates(this.navdyGpsLocationListener);
            this.locationManager.removeGpsStatusListener(this.ubloxGpsStatusListener);
            this.locationManager.removeNmeaListener(this.ubloxGpsNmeaListener);
        }
    }
}
