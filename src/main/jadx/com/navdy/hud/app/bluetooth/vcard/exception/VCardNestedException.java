package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardNestedException extends VCardNotSupportedException {
    public VCardNestedException(String message) {
        super(message);
    }
}
