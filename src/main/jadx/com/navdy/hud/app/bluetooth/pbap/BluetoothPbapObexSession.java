package com.navdy.hud.app.bluetooth.pbap;

import android.os.Handler;
import android.util.Log;
import com.navdy.hud.app.bluetooth.obex.ClientSession;
import com.navdy.hud.app.bluetooth.obex.HeaderSet;
import com.navdy.hud.app.bluetooth.obex.ObexTransport;
import com.navdy.hud.app.bluetooth.obex.ResponseCodes;

final class BluetoothPbapObexSession {
    static final int OBEX_SESSION_AUTHENTICATION_REQUEST = 105;
    static final int OBEX_SESSION_AUTHENTICATION_TIMEOUT = 106;
    static final int OBEX_SESSION_CONNECTED = 100;
    static final int OBEX_SESSION_DISCONNECTED = 102;
    static final int OBEX_SESSION_FAILED = 101;
    static final int OBEX_SESSION_REQUEST_COMPLETED = 103;
    static final int OBEX_SESSION_REQUEST_FAILED = 104;
    private static final byte[] PBAP_TARGET = new byte[]{(byte) 121, (byte) 97, (byte) 53, (byte) -16, (byte) -16, (byte) -59, (byte) 17, (byte) -40, (byte) 9, (byte) 102, (byte) 8, (byte) 0, (byte) 32, (byte) 12, (byte) -102, (byte) 102};
    private static final String TAG = "BTPbapObexSession";
    private BluetoothPbapObexAuthenticator mAuth = null;
    private ObexClientThread mObexClientThread;
    private Handler mSessionHandler;
    private final ObexTransport mTransport;

    private class ObexClientThread extends Thread {
        private static final String TAG = "ObexClientThread";
        private ClientSession mClientSession = null;
        private BluetoothPbapRequest mRequest = null;
        private volatile boolean mRunning = true;

        public boolean isRunning() {
            return this.mRunning;
        }

        public void run() {
            super.run();
            if (connect()) {
                BluetoothPbapObexSession.this.mSessionHandler.obtainMessage(100).sendToTarget();
                while (this.mRunning) {
                    synchronized (this) {
                        try {
                            if (this.mRequest == null) {
                                wait();
                            }
                        } catch (InterruptedException e) {
                            this.mRunning = false;
                        }
                    }
                    if (this.mRunning && this.mRequest != null) {
                        try {
                            this.mRequest.execute(this.mClientSession);
                        } catch (Throwable th) {
                            this.mRunning = false;
                        }
                        if (this.mRequest.isSuccess()) {
                            BluetoothPbapObexSession.this.mSessionHandler.obtainMessage(103, this.mRequest).sendToTarget();
                        } else {
                            BluetoothPbapObexSession.this.mSessionHandler.obtainMessage(104, this.mRequest).sendToTarget();
                        }
                    }
                    this.mRequest = null;
                }
                disconnect();
                BluetoothPbapObexSession.this.mSessionHandler.obtainMessage(102).sendToTarget();
                return;
            }
            BluetoothPbapObexSession.this.mSessionHandler.obtainMessage(101).sendToTarget();
        }

        public synchronized boolean schedule(BluetoothPbapRequest request) {
            boolean z;
            Log.d(TAG, "schedule: " + request.getClass().getSimpleName());
            if (this.mRequest != null) {
                z = false;
            } else {
                this.mRequest = request;
                notify();
                z = true;
            }
            return z;
        }

        private boolean connect() {
            Log.d(TAG, "connect");
            try {
                this.mClientSession = new ClientSession(BluetoothPbapObexSession.this.mTransport);
                this.mClientSession.setAuthenticator(BluetoothPbapObexSession.this.mAuth);
                HeaderSet hs = new HeaderSet();
                hs.setHeader(70, BluetoothPbapObexSession.PBAP_TARGET);
                try {
                    if (this.mClientSession.connect(hs).getResponseCode() == ResponseCodes.OBEX_HTTP_OK) {
                        return true;
                    }
                    disconnect();
                    return false;
                } catch (Throwable th) {
                    return false;
                }
            } catch (Throwable th2) {
                return false;
            }
        }

        private void disconnect() {
            Log.d(TAG, "disconnect");
            if (this.mClientSession != null) {
                try {
                    this.mClientSession.disconnect(null);
                    this.mClientSession.close();
                } catch (Throwable th) {
                }
            }
        }
    }

    public BluetoothPbapObexSession(ObexTransport transport) {
        this.mTransport = transport;
    }

    public void start(Handler handler) {
        Log.d(TAG, "start");
        this.mSessionHandler = handler;
        this.mAuth = new BluetoothPbapObexAuthenticator(this.mSessionHandler);
        this.mObexClientThread = new ObexClientThread();
        this.mObexClientThread.start();
    }

    public void stop() {
        Log.d(TAG, "stop");
        if (this.mObexClientThread != null) {
            boolean wait = false;
            try {
                wait = this.mObexClientThread.isAlive() && this.mObexClientThread.isRunning();
            } catch (Throwable th) {
            }
            if (wait) {
                try {
                    this.mObexClientThread.interrupt();
                } catch (Throwable th2) {
                }
                try {
                    this.mObexClientThread.join();
                } catch (Throwable th3) {
                }
            }
            this.mObexClientThread = null;
        }
    }

    public void abort() {
        Log.d(TAG, "abort");
        if (this.mObexClientThread != null && this.mObexClientThread.mRequest != null) {
            new Thread() {
                public void run() {
                    BluetoothPbapObexSession.this.mObexClientThread.mRequest.abort();
                }
            }.run();
        }
    }

    public boolean schedule(BluetoothPbapRequest request) {
        Log.d(TAG, "schedule: " + request.getClass().getSimpleName());
        if (this.mObexClientThread != null) {
            return this.mObexClientThread.schedule(request);
        }
        Log.e(TAG, "OBEX session not started");
        return false;
    }

    public boolean setAuthReply(String key) {
        Log.d(TAG, "setAuthReply key=" + key);
        if (this.mAuth == null) {
            return false;
        }
        this.mAuth.setReply(key);
        return true;
    }
}
