package com.navdy.hud.app.view;

import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.ConfirmationLayout;

public class FactoryResetView$$ViewInjector {
    public static void inject(Finder finder, FactoryResetView target, Object source) {
        target.factoryResetConfirmation = (ConfirmationLayout) finder.findRequiredView(source, R.id.factory_reset_confirmation, "field 'factoryResetConfirmation'");
    }

    public static void reset(FactoryResetView target) {
        target.factoryResetConfirmation = null;
    }
}
