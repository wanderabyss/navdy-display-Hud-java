package com.navdy.hud.app.view.drawable;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import com.navdy.hud.app.R;

public class SpeedoMeterGaugeDrawable2 extends GaugeDrawable {
    private static final float GAUGE_START_ANGLE = 146.2f;
    private static final float SPEED_LIMIT_MARKER_ANGLE_WIDTH = 4.0f;
    public static final int TEXT_MARGIN = 15;
    private int mGaugeMargin;
    private int mOuterRingColor;
    private int mSpeedLimit;
    private int mSpeedLimitMarkerColor;
    private int mSpeedLimitTextColor;
    private int mSpeedLimitTextSize;
    private int mValueRingWidth;
    public int speedLimitTextAlpha = 0;

    public SpeedoMeterGaugeDrawable2(Context context, int backgroundResource, int stateColorsResId, int valueRingWidth) {
        super(context, backgroundResource, stateColorsResId);
        this.mValueRingWidth = context.getResources().getDimensionPixelSize(valueRingWidth);
        this.mGaugeMargin = context.getResources().getDimensionPixelSize(R.dimen.speedometer_gauge_margin);
        this.mSpeedLimitTextColor = context.getResources().getColor(17170443);
        this.mOuterRingColor = this.mColorTable[3];
        this.mSpeedLimitMarkerColor = this.mColorTable[4];
        this.mSpeedLimitTextSize = context.getResources().getDimensionPixelSize(R.dimen.speedometer_gauge_speed_limit_text_size);
    }

    public void setSpeedLimit(int mSpeedLimit) {
        this.mSpeedLimit = mSpeedLimit;
    }

    public void setState(int index) {
        super.setState(index);
        switch (index) {
            case 0:
                this.mSpeedLimitMarkerColor = this.mColorTable[1];
                return;
            case 1:
                this.mSpeedLimitMarkerColor = this.mColorTable[4];
                return;
            case 2:
                this.mSpeedLimitMarkerColor = this.mColorTable[4];
                return;
            default:
                return;
        }
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        Rect bounds = getBounds();
        int innerRingInset = this.mValueRingWidth / 2;
        RectF oval = new RectF((float) (bounds.left + innerRingInset), (float) (bounds.top + innerRingInset), (float) (bounds.right - innerRingInset), (float) ((bounds.top + bounds.width()) - innerRingInset));
        oval.inset((float) this.mGaugeMargin, (float) this.mGaugeMargin);
        this.mPaint.setStrokeWidth((float) this.mValueRingWidth);
        this.mPaint.setColor(this.mOuterRingColor);
        this.mPaint.setStyle(Style.STROKE);
        canvas.drawArc(oval, GAUGE_START_ANGLE, 247.6f, false, this.mPaint);
        this.mPaint.setStrokeWidth((float) this.mValueRingWidth);
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setColor(this.mDefaultColor);
        canvas.drawArc(oval, GAUGE_START_ANGLE, (this.mValue / this.mMaxValue) * 247.6f, false, this.mPaint);
        if (this.mSpeedLimit > 0) {
            this.mPaint.setColor(this.mSpeedLimitMarkerColor);
            float speedLimitMarkerAngle = ((GAUGE_START_ANGLE + ((((float) this.mSpeedLimit) / this.mMaxValue) * 247.6f)) - 2.0f) % 360.0f;
            canvas.drawArc(oval, speedLimitMarkerAngle, SPEED_LIMIT_MARKER_ANGLE_WIDTH, false, this.mPaint);
            RectF rectF = new RectF(bounds);
            rectF.inset(15.0f, 15.0f);
            int radius = (int) (rectF.width() / 2.0f);
            int alpha = this.mPaint.getAlpha();
            this.mPaint.setStyle(Style.FILL);
            this.mPaint.setColor(this.mSpeedLimitTextColor);
            this.mPaint.setTextSize((float) this.mSpeedLimitTextSize);
            this.mPaint.setAlpha(this.speedLimitTextAlpha);
            this.mPaint.setTypeface(Typeface.create(Typeface.DEFAULT, 1));
            String text = Integer.toString(this.mSpeedLimit);
            Rect textBounds = new Rect();
            this.mPaint.getTextBounds(text, 0, text.length(), textBounds);
            int y = (int) (((double) rectF.centerY()) + (((double) radius) * Math.sin(Math.toRadians((double) speedLimitMarkerAngle))));
            canvas.drawText(text, (float) (((int) (((double) rectF.centerX()) + (((double) radius) * Math.cos(Math.toRadians((double) speedLimitMarkerAngle))))) - (textBounds.width() / 2)), (float) ((textBounds.height() / 2) + y), this.mPaint);
            this.mPaint.setAlpha(alpha);
        }
    }
}
