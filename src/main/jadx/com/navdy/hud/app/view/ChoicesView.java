package com.navdy.hud.app.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import com.glympse.android.hal.NotificationListener;
import com.navdy.hud.app.R;
import com.navdy.hud.app.gesture.GestureDetector;
import com.navdy.hud.app.gesture.GestureDetector.GestureListener;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;

public abstract class ChoicesView extends RelativeLayout implements IInputHandler, GestureListener {
    protected boolean animateItemOnExecution;
    protected boolean animateItemOnSelection;
    protected GestureDetector detector;
    protected Animation executeAnimation;
    protected int lastTouchX;
    protected int lastTouchY;
    private AnimationListener listener;
    protected Logger logger;
    protected int selectedItem;
    protected boolean wrapAround;

    protected abstract void animateDown();

    protected abstract void animateUp();

    public abstract View getItemAt(int i);

    public abstract int getItemCount();

    public abstract float getValue();

    public abstract void onExecuteItem(int i);

    public abstract void setValue(float f);

    public ChoicesView(Context context) {
        this(context, null);
    }

    public ChoicesView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChoicesView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.logger = new Logger(getClass());
        this.selectedItem = -1;
        this.wrapAround = false;
        this.animateItemOnExecution = true;
        this.animateItemOnSelection = true;
        this.listener = new AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                ChoicesView.this.onExecuteItem(ChoicesView.this.selectedItem);
            }

            public void onAnimationRepeat(Animation animation) {
            }
        };
        this.detector = new GestureDetector(this);
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == 1) {
                    ChoicesView.this.lastTouchX = (int) event.getX();
                    ChoicesView.this.lastTouchY = (int) event.getY();
                }
                return false;
            }
        });
        setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Rect hitRect = new Rect();
                int[] myPos = new int[2];
                int[] parentPos = new int[2];
                ChoicesView.this.getLocationInWindow(myPos);
                int i = 0;
                while (i < ChoicesView.this.getItemCount()) {
                    View childView = ChoicesView.this.getItemAt(i);
                    ((View) childView.getParent()).getLocationInWindow(parentPos);
                    int touchX = ChoicesView.this.lastTouchX - (parentPos[0] - myPos[0]);
                    int touchY = ChoicesView.this.lastTouchY - (parentPos[1] - myPos[1]);
                    childView.getHitRect(hitRect);
                    if (!hitRect.contains(touchX, touchY)) {
                        i++;
                    } else if (i == ChoicesView.this.getSelectedItem()) {
                        ChoicesView.this.executeSelectedItem(ChoicesView.this.animateItemOnExecution);
                        return;
                    } else {
                        ChoicesView.this.setSelectedItem(i);
                        return;
                    }
                }
            }
        });
    }

    protected void onDeselectItem(View choice, int newSelectionIndex) {
    }

    protected void onSelectItem(View choice, int oldSelectionIndex) {
    }

    public int getSelectedItem() {
        return this.selectedItem;
    }

    public void setSelectedItem(int index) {
        if (index != this.selectedItem) {
            this.logger.d("selecting option - " + (index < 0 ? "none" : String.valueOf(index + 1)));
            View oldView = null;
            View newView = null;
            if (this.selectedItem != -1) {
                oldView = getItemAt(this.selectedItem);
            }
            if (index != -1) {
                newView = getItemAt(index);
            }
            if (oldView != null) {
                onDeselectItem(oldView, index);
            }
            int oldIndex = this.selectedItem;
            this.selectedItem = index;
            if (newView != null) {
                onSelectItem(newView, oldIndex);
            }
            if (index != -1) {
                setValue(valueForIndex(index));
            }
        }
    }

    public int indexForValue(float value) {
        int itemCount = getItemCount();
        if (itemCount <= 1) {
            return 0;
        }
        return (int) ((((float) (itemCount - 1)) * value) + 0.5f);
    }

    public float valueForIndex(int index) {
        int itemCount = getItemCount();
        if (itemCount <= 1) {
            return 0.0f;
        }
        return ((float) index) / ((float) (itemCount - 1));
    }

    public void executeSelectedItem(boolean animated) {
        if (this.selectedItem != -1 && getItemCount() > 0) {
            this.logger.d("USER_STUDY executing option " + (this.selectedItem + 1));
            if (animated) {
                if (this.executeAnimation == null) {
                    this.executeAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.press_button);
                    this.executeAnimation.setAnimationListener(this.listener);
                }
                getItemAt(this.selectedItem).startAnimation(this.executeAnimation);
                return;
            }
            onExecuteItem(this.selectedItem);
        }
    }

    public void onClick() {
        executeSelectedItem(this.animateItemOnExecution);
    }

    public void onTrackHand(float position) {
        setValue(position);
    }

    public boolean onGesture(GestureEvent event) {
        this.detector.onGesture(event);
        switch (event.gesture) {
            case GESTURE_FINGER_UP:
            case GESTURE_HAND_UP:
                animateUp();
                break;
            case GESTURE_FINGER_DOWN:
            case GESTURE_HAND_DOWN:
                animateDown();
                setSelectedItem(-1);
                break;
            case GESTURE_SWIPE_LEFT:
                if (this.wrapAround || this.selectedItem != 0) {
                    moveSelectionLeft();
                    return true;
                }
                buildBounceAnim(0.0f, valueForIndex(0)).start();
                return true;
            case GESTURE_SWIPE_RIGHT:
                if (this.wrapAround || this.selectedItem + 1 != getItemCount()) {
                    moveSelectionRight();
                    return true;
                }
                buildBounceAnim(1.0f, valueForIndex(this.selectedItem)).start();
                return true;
        }
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.selectedItem == -1) {
            animateUp();
            return true;
        }
        switch (event) {
            case LEFT:
                moveSelectionLeft();
                return true;
            case RIGHT:
                moveSelectionRight();
                return true;
            case SELECT:
                executeSelectedItem(true);
                return true;
            default:
                return false;
        }
    }

    private void moveSelectionLeft() {
        int itemCount = getItemCount();
        if (this.selectedItem > 0 && itemCount > 0) {
            setSelectedItem(((this.selectedItem - 1) + itemCount) % itemCount);
        }
    }

    private void moveSelectionRight() {
        if (this.selectedItem != -1) {
            int itemCount = getItemCount();
            if (this.selectedItem + 1 < itemCount) {
                setSelectedItem((this.selectedItem + 1) % itemCount);
            }
        }
    }

    private Animator buildBounceAnim(float edgeValue, float backToValue) {
        AnimatorSet s = new AnimatorSet();
        s.play(ObjectAnimator.ofFloat(this, NotificationListener.INTENT_EXTRA_VALUE, new float[]{edgeValue})).before(ObjectAnimator.ofFloat(this, NotificationListener.INTENT_EXTRA_VALUE, new float[]{backToValue}));
        return s;
    }

    public IInputHandler nextHandler() {
        return InputManager.nextContainingHandler(this);
    }
}
