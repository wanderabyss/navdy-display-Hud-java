package com.navdy.hud.app.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.screen.ForceUpdateScreen.Presenter;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.hud.app.ui.activity.Main.ProtocolStatus;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout.Mode;
import com.navdy.hud.app.util.OTAUpdateService.UpdateVerified;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import mortar.Mortar;

public class ForceUpdateView extends RelativeLayout implements IListener, IInputHandler {
    private static final int TAG_DISMISS = 2;
    private static final int TAG_INSTALL = 1;
    private static final int TAG_SHUT_DOWN = 0;
    private static final int UPDATE_MESSAGE_MAX_WIDTH = 350;
    private static final Logger sLogger = new Logger(ForceUpdateView.class);
    @InjectView(R.id.choiceLayout)
    ChoiceLayout mChoiceLayout;
    @InjectView(R.id.image)
    ImageView mIcon;
    @Inject
    Presenter mPresenter;
    @InjectView(R.id.mainTitle)
    TextView mScreenTitleText;
    @InjectView(R.id.title1)
    TextView mTextView1;
    @InjectView(R.id.title2)
    TextView mTextView2;
    @InjectView(R.id.title3)
    TextView mTextView3;
    private boolean waitingForUpdate = false;

    public void executeItem(int pos, int id) {
        performAction(id);
    }

    public void itemSelected(int pos, int id) {
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.mChoiceLayout == null || this.mChoiceLayout.getVisibility() != 0) {
            return false;
        }
        switch (event) {
            case LEFT:
                this.mChoiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                this.mChoiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                this.mChoiceLayout.executeSelectedItem(true);
                return true;
            default:
                return true;
        }
    }

    public IInputHandler nextHandler() {
        return null;
    }

    public ForceUpdateView(Context context) {
        super(context);
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
    }

    public ForceUpdateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
    }

    public ForceUpdateView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
    }

    @Subscribe
    public void onUpdateReady(UpdateVerified event) {
        if (this.waitingForUpdate) {
            List<Choice> list = new ArrayList();
            list.add(new Choice(getContext().getString(R.string.install), 1));
            list.add(new Choice(getContext().getString(R.string.shutdown), 0));
            this.mChoiceLayout.setChoices(Mode.LABEL, list, 0, this);
            this.mTextView3.setText(R.string.hud_update_install);
            this.waitingForUpdate = false;
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        boolean updateAvailable = this.mPresenter.isSoftwareUpdatePending();
        List<Choice> list = new ArrayList();
        ButterKnife.inject((View) this);
        this.mPresenter.takeView(this);
        this.mPresenter.getBus().register(this);
        ((MaxWidthLinearLayout) findViewById(R.id.infoContainer)).setMaxWidth(UPDATE_MESSAGE_MAX_WIDTH);
        this.mIcon.setImageResource(R.drawable.icon_software_update);
        this.mTextView1.setVisibility(8);
        this.mTextView2.setVisibility(8);
        this.mTextView3.setSingleLine(false);
        this.mTextView3.setVisibility(0);
        if (Main.mProtocolStatus == ProtocolStatus.PROTOCOL_REMOTE_NEEDS_UPDATE) {
            this.mScreenTitleText.setText(R.string.title_app_update_required);
            this.mTextView3.setText(R.string.phone_update_required);
            list.add(new Choice(getContext().getString(R.string.dismiss), 2));
        } else {
            this.mScreenTitleText.setText(R.string.title_display_update_required);
            if (updateAvailable) {
                this.mTextView3.setText(R.string.hud_update_install);
                list.add(new Choice(getContext().getString(R.string.install), 1));
            } else {
                this.mTextView3.setText(R.string.hud_update_download);
                this.waitingForUpdate = true;
            }
            list.add(new Choice(getContext().getString(R.string.shutdown), 0));
        }
        this.mChoiceLayout.setChoices(Mode.LABEL, list, 0, this);
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mPresenter.getBus().unregister(this);
    }

    private void performAction(int tag) {
        switch (tag) {
            case 0:
                this.mPresenter.shutDown();
                return;
            case 1:
                this.mPresenter.install();
                return;
            case 2:
                this.mPresenter.dismiss();
                return;
            default:
                sLogger.e("unknown action received: " + tag);
                return;
        }
    }
}
