package com.navdy.hud.app.framework.glympse;

import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.contacts.Contact;
import com.navdy.hud.app.framework.contacts.ContactImageHelper;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader;
import com.navdy.hud.app.framework.glympse.GlympseManager.Error;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout2.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Selection;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GlympseNotification implements INotification, IListener {
    private static final float NOTIFICATION_ALERT_BADGE_SCALE = 0.95f;
    private static final float NOTIFICATION_BADGE_SCALE = 0.5f;
    private static final float NOTIFICATION_DEFAULT_ICON_SCALE = 1.0f;
    private static final float NOTIFICATION_ICON_SCALE = 1.38f;
    private static final int NOTIFICATION_TIMEOUT = 5000;
    private static final int alertColor = resources.getColor(R.color.share_location_trip_alert_color);
    private static final List<Choice> choicesOnlyDismiss = new ArrayList();
    private static final List<Choice> choicesRetryAndDismiss = new ArrayList();
    private static final Handler handler = new Handler(Looper.getMainLooper());
    private static final Logger logger = new Logger(GlympseNotification.class);
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private static final int shareLocationTripColor = resources.getColor(R.color.share_location_trip_color);
    @InjectView(R.id.badge)
    ImageView badge;
    @InjectView(R.id.badge_icon)
    IconColorImageView badgeIcon;
    @InjectView(R.id.choice_layout)
    ChoiceLayout2 choiceLayout;
    private final Contact contact;
    private INotificationController controller;
    private final String destinationLabel;
    private final double latitude;
    private final double longitude;
    private final String message;
    private final String notifId;
    @InjectView(R.id.notification_icon)
    IconColorImageView notificationIcon;
    @InjectView(R.id.notification_user_image)
    ImageView notificationUserImage;
    private boolean retrySendingMessage;
    @InjectView(R.id.subtitle)
    TextView subtitle;
    @InjectView(R.id.title)
    TextView title;
    private final Type type;
    private final String uuid;

    public enum Type {
        SENT,
        READ,
        OFFLINE
    }

    static {
        int dismissColor = resources.getColor(R.color.glance_dismiss);
        Choice dismissChoice = new Choice(R.id.dismiss, R.drawable.icon_glances_dismiss, dismissColor, R.drawable.icon_glances_dismiss, -16777216, resources.getString(R.string.dismiss), dismissColor);
        choicesOnlyDismiss.add(dismissChoice);
        int retryColor = resources.getColor(R.color.glance_ok_blue);
        choicesRetryAndDismiss.add(new Choice(R.id.retry, R.drawable.icon_glances_retry, retryColor, R.drawable.icon_glances_retry, -16777216, resources.getString(R.string.retry), retryColor));
        choicesRetryAndDismiss.add(dismissChoice);
    }

    GlympseNotification(Contact contact, Type type) {
        this(contact, type, null, null, 0.0d, 0.0d);
    }

    public GlympseNotification(Contact contact, Type type, String message, String destinationLabel, double latitude, double longitude) {
        this.contact = contact;
        this.type = type;
        this.message = message;
        this.destinationLabel = destinationLabel;
        this.latitude = latitude;
        this.longitude = longitude;
        this.uuid = UUID.randomUUID().toString();
        this.notifId = "navdy#glympse#notif#" + type.name() + "#" + this.uuid;
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public NotificationType getType() {
        return NotificationType.GLYMPSE;
    }

    public String getId() {
        return this.notifId;
    }

    public View getView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.notification_glympse, null);
        ButterKnife.inject( this, view);
        switch (this.type) {
            case SENT:
                setSentUI();
                break;
            case READ:
                setReadUI();
                break;
            case OFFLINE:
                setOfflineUI();
                break;
        }
        return view;
    }

    private void setSentUI() {
        if (HereMapsManager.getInstance().isInitialized() && HereNavigationManager.getInstance().isNavigationModeOn()) {
            this.title.setText(resources.getString(R.string.trip_sent));
        } else {
            this.title.setText(resources.getString(R.string.location_sent));
        }
        this.subtitle.setText(resources.getString(R.string.to_contact_name, new Object[]{getContactName()}));
        this.notificationIcon.setIcon(R.drawable.icon_message, shareLocationTripColor, null, NOTIFICATION_ICON_SCALE);
        this.notificationIcon.setVisibility(0);
        this.badge.setImageResource(R.drawable.icon_msg_success);
        this.badge.setVisibility(0);
        this.choiceLayout.setChoices(choicesOnlyDismiss, 0, this);
    }

    private void setReadUI() {
        this.title.setText(getContactName());
        if (HereMapsManager.getInstance().isInitialized() && HereNavigationManager.getInstance().isNavigationModeOn()) {
            this.subtitle.setText(resources.getString(R.string.viewed_your_trip));
        } else {
            this.subtitle.setText(resources.getString(R.string.viewed_your_location));
        }
        Bitmap bitmap = PicassoUtil.getBitmapfromCache(PhoneImageDownloader.getInstance().getImagePath(this.contact.number, PhotoType.PHOTO_CONTACT));
        if (bitmap != null) {
            this.notificationUserImage.setImageBitmap(bitmap);
            this.notificationUserImage.setVisibility(0);
        } else {
            ContactImageHelper contactImageHelper = ContactImageHelper.getInstance();
            this.notificationIcon.setIcon(contactImageHelper.getResourceId(this.contact.defaultImageIndex), contactImageHelper.getResourceColor(this.contact.defaultImageIndex), null, 1.0f);
            this.notificationIcon.setVisibility(0);
        }
        this.badgeIcon.setIcon(R.drawable.icon_message, shareLocationTripColor, null, 0.5f);
        this.badgeIcon.setVisibility(0);
        this.choiceLayout.setChoices(choicesOnlyDismiss, 0, this);
    }

    private void setOfflineUI() {
        this.title.setText(resources.getString(R.string.sending_failed));
        this.subtitle.setText(resources.getString(R.string.offline));
        this.notificationIcon.setIcon(R.drawable.icon_message, shareLocationTripColor, null, NOTIFICATION_ICON_SCALE);
        this.notificationIcon.setVisibility(0);
        this.badgeIcon.setIcon(R.drawable.icon_msg_alert, alertColor, null, NOTIFICATION_ALERT_BADGE_SCALE);
        this.badgeIcon.setVisibility(0);
        this.choiceLayout.setChoices(choicesRetryAndDismiss, 0, this);
    }

    public View getExpandedView(Context context, Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(INotificationController controller) {
        this.controller = controller;
    }

    public void onUpdate() {
    }

    public void onStop() {
        this.controller = null;
        if (this.retrySendingMessage) {
            handler.post(new Runnable() {
                public void run() {
                    boolean hasFailed;
                    boolean z;
                    boolean z2 = true;
                    GlympseNotification.logger.v("Glympse:retry sending message");
                    StringBuilder builder = new StringBuilder();
                    if (GlympseManager.getInstance().addMessage(GlympseNotification.this.contact, GlympseNotification.this.message, GlympseNotification.this.destinationLabel, GlympseNotification.this.latitude, GlympseNotification.this.longitude, builder) != Error.NONE) {
                        hasFailed = true;
                    } else {
                        hasFailed = false;
                    }
                    String shareType = GlympseNotification.this.destinationLabel == null ? "Location" : GlympseManager.GLYMPSE_TYPE_SHARE_TRIP;
                    if (hasFailed) {
                        GlympseNotification.addGlympseOfflineGlance(GlympseNotification.this.message, GlympseNotification.this.contact, GlympseNotification.this.destinationLabel, GlympseNotification.this.latitude, GlympseNotification.this.longitude);
                    }
                    Logger access$000 = GlympseNotification.logger;
                    StringBuilder append = new StringBuilder().append("Glympse:retry sucess:");
                    if (hasFailed) {
                        z = false;
                    } else {
                        z = true;
                    }
                    access$000.v(append.append(z).toString());
                    if (hasFailed) {
                        z2 = false;
                    }
                    AnalyticsSupport.recordGlympseSent(z2, shareType, GlympseNotification.this.message, builder.toString());
                }
            });
        }
    }

    public int getTimeout() {
        if (this.type == Type.OFFLINE) {
            return 0;
        }
        return 5000;
    }

    public boolean isAlive() {
        return false;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        return 0;
    }

    public void onNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    public boolean supportScroll() {
        return false;
    }

    public void executeItem(Selection selection) {
        if (selection.id == R.id.retry) {
            this.retrySendingMessage = true;
            NotificationManager.getInstance().removeNotification(this.notifId);
        } else if (selection.id == R.id.dismiss) {
            NotificationManager.getInstance().removeNotification(this.notifId);
        }
    }

    public void itemSelected(Selection selection) {
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.controller == null) {
            return false;
        }
        switch (event) {
            case LEFT:
                this.choiceLayout.moveSelectionLeft();
                if (this.type != Type.OFFLINE) {
                    this.controller.resetTimeout();
                }
                return true;
            case RIGHT:
                this.choiceLayout.moveSelectionRight();
                if (this.type != Type.OFFLINE) {
                    this.controller.resetTimeout();
                }
                return true;
            case SELECT:
                this.choiceLayout.executeSelectedItem();
                return true;
            default:
                return false;
        }
    }

    public IInputHandler nextHandler() {
        return null;
    }

    private static void addGlympseOfflineGlance(String message, Contact contact, String destinationLabel, double latitude, double longitude) {
        NotificationManager.getInstance().addNotification(new GlympseNotification(contact, Type.OFFLINE, message, destinationLabel, latitude, longitude));
    }

    private String getContactName() {
        if (!TextUtils.isEmpty(this.contact.name)) {
            return this.contact.name;
        }
        if (TextUtils.isEmpty(this.contact.formattedNumber)) {
            return "";
        }
        return this.contact.formattedNumber;
    }
}
