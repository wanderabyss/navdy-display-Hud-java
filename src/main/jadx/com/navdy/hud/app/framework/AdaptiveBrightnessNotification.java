package com.navdy.hud.app.framework;

import android.animation.AnimatorSet;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.settings.HUDSettings;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout2.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Selection;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.navdy.hud.app.view.Gauge;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.List;

public class AdaptiveBrightnessNotification extends ContentObserver implements INotification, OnSharedPreferenceChangeListener {
    private static final int DEFAULT_BRIGHTNESS_INT = Integer.parseInt("128");
    private static final long DETENT_TIMEOUT = 250;
    private static final int INITIAL_CHANGING_STEP = 1;
    private static final int MAX_BRIGHTNESS = 255;
    private static final int MAX_STEP = 16;
    private static final int NOTIFICATION_TIMEOUT = 10000;
    private static final Logger sLogger = new Logger(AdaptiveBrightnessNotification.class);
    private final boolean AUTO_BRIGHTNESS_ADJ_ENABLED = true;
    private int changingStep = 1;
    private ChoiceLayout2 choiceLayout;
    private ViewGroup container;
    private INotificationController controller;
    private int currentValue;
    private boolean isAutoBrightnessOn = getCurrentIsAutoBrightnessOn();
    private boolean isLastChangeIncreasing = false;
    private long lastChangeActionTime = -251;
    private Gauge progressBar;
    private SharedPreferences sharedPreferences = RemoteDeviceManager.getInstance().getSharedPreferences();
    private Editor sharedPreferencesEditor = this.sharedPreferences.edit();
    private TextView textIndicator;

    public AdaptiveBrightnessNotification() {
        super(new Handler());
    }

    public void onChange(boolean selfChange, Uri uri) {
    }

    public NotificationType getType() {
        return NotificationType.BRIGHTNESS;
    }

    public String getId() {
        return NotificationId.BRIGHTNESS_NOTIFICATION_ID;
    }

    public View getView(Context context) {
        if (this.container == null) {
            this.container = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.notification_adaptive_auto_brightness, null);
            this.textIndicator = (TextView) this.container.findViewById(R.id.subTitle);
            this.progressBar = (Gauge) this.container.findViewById(R.id.circle_progress);
            this.progressBar.setAnimated(false);
            showFixedChoices(this.container);
        }
        return this.container;
    }

    public View getExpandedView(Context context, Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    private void showFixedChoices(ViewGroup container) {
        this.choiceLayout = (ChoiceLayout2) container.findViewById(R.id.choiceLayout);
        List<Choice> choices = new ArrayList(3);
        Resources resources = container.getResources();
        String ok = resources.getString(R.string.glances_ok);
        int okColor = resources.getColor(R.color.glance_ok_blue);
        choices.add(new Choice(2, R.drawable.icon_glances_ok, okColor, R.drawable.icon_glances_ok, -16777216, ok, okColor));
        this.choiceLayout.setChoices(choices, 1, new IListener() {
            public void executeItem(Selection selection) {
                AdaptiveBrightnessNotification.this.sharedPreferencesEditor.putString(HUDSettings.AUTO_BRIGHTNESS_ADJUSTMENT, String.valueOf(AdaptiveBrightnessNotification.this.currentValue));
                AdaptiveBrightnessNotification.this.sharedPreferencesEditor.apply();
                AdaptiveBrightnessNotification.this.close();
            }

            public void itemSelected(Selection selection) {
            }
        });
        this.choiceLayout.setVisibility(0);
    }

    public void onStart(INotificationController controller) {
        this.currentValue = getCurrentBrightness();
        this.sharedPreferencesEditor.putString(HUDSettings.AUTO_BRIGHTNESS, "false");
        this.sharedPreferencesEditor.apply();
        this.sharedPreferencesEditor.putString(HUDSettings.BRIGHTNESS, String.valueOf(this.currentValue));
        this.sharedPreferencesEditor.apply();
        this.isAutoBrightnessOn = false;
        this.controller = controller;
        this.sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        updateState();
    }

    public void updateState() {
        if (this.controller == null) {
            sLogger.v("brightness notif offscreen");
            return;
        }
        this.currentValue = getCurrentBrightness();
        this.progressBar.setValue(this.currentValue);
    }

    public void onUpdate() {
        if (this.controller != null) {
            this.controller.resetTimeout();
        }
        updateState();
    }

    public void onStop() {
        this.sharedPreferencesEditor.putString(HUDSettings.AUTO_BRIGHTNESS, "true");
        this.sharedPreferencesEditor.apply();
        this.isAutoBrightnessOn = false;
        this.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
        this.controller = null;
    }

    public int getTimeout() {
        return 10000;
    }

    public boolean isAlive() {
        return false;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return false;
    }

    public int getColor() {
        return 0;
    }

    public void onNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    public boolean supportScroll() {
        return false;
    }

    public static void showNotification() {
        NotificationManager.getInstance().addNotification(new AdaptiveBrightnessNotification());
    }

    private void close() {
        NotificationManager.getInstance().removeNotification(getId());
        AnalyticsSupport.recordAdaptiveAutobrightnessAdjustmentChanged(getCurrentBrightness());
    }

    public void onClick() {
        close();
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    private void detentChangingStep(boolean isChangeIncreasing) {
        long now = SystemClock.elapsedRealtime();
        if (isChangeIncreasing != this.isLastChangeIncreasing || this.lastChangeActionTime + 250 < now) {
            this.changingStep = 1;
        } else {
            this.changingStep <<= 1;
            if (this.changingStep > 16) {
                this.changingStep = 16;
            }
        }
        this.isLastChangeIncreasing = isChangeIncreasing;
        this.lastChangeActionTime = now;
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.controller == null) {
            return false;
        }
        if (event != CustomKeyEvent.SELECT) {
            this.controller.resetTimeout();
        }
        switch (event) {
            case SELECT:
                if (this.choiceLayout != null) {
                    this.choiceLayout.executeSelectedItem();
                } else {
                    close();
                }
                return true;
            case LEFT:
                sLogger.v("Decreasing brightness");
                detentChangingStep(false);
                updateBrightness(-this.changingStep);
                return true;
            case RIGHT:
                sLogger.v("Increasing brightness");
                detentChangingStep(true);
                updateBrightness(this.changingStep);
                return true;
            default:
                return false;
        }
    }

    public IInputHandler nextHandler() {
        return null;
    }

    private boolean getCurrentIsAutoBrightnessOn() {
        return "true".equals(this.sharedPreferences.getString(HUDSettings.AUTO_BRIGHTNESS, ""));
    }

    private int getCurrentBrightness() {
        if (this.isAutoBrightnessOn) {
            try {
                return System.getInt(HudApplication.getAppContext().getContentResolver(), "screen_brightness");
            } catch (SettingNotFoundException e) {
                sLogger.e("Settings not found exception ", e);
                return DEFAULT_BRIGHTNESS_INT;
            }
        }
        try {
            return Integer.parseInt(this.sharedPreferences.getString(HUDSettings.BRIGHTNESS, "128"));
        } catch (NumberFormatException e2) {
            sLogger.e("Cannot parse brightness from the shared preferences", e2);
            return DEFAULT_BRIGHTNESS_INT;
        }
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(HUDSettings.BRIGHTNESS)) {
            updateState();
        }
    }

    private void updateBrightness(int delta) {
        if (delta != 0) {
            int newValue = this.currentValue + delta;
            if (newValue < 0) {
                newValue = 0;
            }
            if (newValue > 255) {
                newValue = 255;
            }
            this.sharedPreferencesEditor.putString(HUDSettings.BRIGHTNESS, String.valueOf(newValue));
            this.sharedPreferencesEditor.apply();
        }
    }
}
