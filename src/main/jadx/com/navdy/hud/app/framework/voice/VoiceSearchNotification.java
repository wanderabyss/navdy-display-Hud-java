package com.navdy.hud.app.framework.voice;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.analytics.AnalyticsSupport.VoiceSearchAdditionalResultsAction;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.voice.VoiceSearchHandler.VoiceSearchUserInterface;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.MusicManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.profile.HudLocale;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout2.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Selection;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.navdy.service.library.events.audio.VoiceSearchResponse;
import com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError;
import com.navdy.service.library.events.destination.Destination;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import com.squareup.wire.Wire;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class VoiceSearchNotification implements INotification, IListener, VoiceSearchUserInterface {
    public static final int ANIMATION_DURATION = 300;
    private static final HashMap<VoiceSearchError, Integer> BADGE_ICON_MAPPING = new HashMap();
    private static final HashMap<VoiceSearchError, Integer> ERROR_TEXT_MAPPING = new HashMap();
    public static final int INITIALIZATION_DELAY_MILLIS = ((int) TimeUnit.SECONDS.toMillis(30));
    public static final int INPUT_DELAY_MILLIS = ((int) TimeUnit.MINUTES.toMillis(2));
    public static final int RESULT_SELECTION_TIMEOUT = 10000;
    public static final int SEARCH_DELAY_MILLIS = ((int) TimeUnit.MINUTES.toMillis(2));
    private static final HashMap<VoiceSearchStateInternal, Integer> STATE_NAME_MAPPING = new HashMap();
    private static final Logger sLogger = new Logger(VoiceSearchNotification.class);
    public AnimatorSet animatorSet;
    @InjectView(R.id.choiceLayout)
    public ChoiceLayout2 choiceLayout;
    private INotificationController controller;
    private VoiceSearchError error;
    private Handler handler;
    @InjectView(R.id.image_voice_search_active)
    public ImageView imageActive;
    @InjectView(R.id.image_voice_search_inactive)
    public ImageView imageInactive;
    @InjectView(R.id.img_processing_badge)
    public ImageView imageProcessing;
    @InjectView(R.id.img_status_badge)
    public ImageView imageStatus;
    private boolean isListeningOverBluetooth;
    private ViewGroup layout;
    private AnimatorSet listeningFeedbackAnimation;
    @InjectView(R.id.voice_search_listening_feedback)
    public View listeningFeedbackView;
    private Locale locale;
    private AnimatorSet mainImageAnimation;
    private boolean multipleResultsMode = false;
    private AnimatorSet processingImageAnimation;
    private volatile boolean responseFromClientTimedOut;
    private List<Destination> results;
    @InjectView(R.id.results_count)
    public TextView resultsCount;
    @InjectView(R.id.results_count_container)
    public ViewGroup resultsCountContainer;
    private AnimatorSet statusImageAnimation;
    @InjectView(R.id.subStatus)
    public TextView subStatus;
    private List<Choice> voiceSearchActiveChoices;
    private List<Choice> voiceSearchFailedChoices;
    private VoiceSearchHandler voiceSearchHandler;
    private VoiceSearchStateInternal voiceSearchState = VoiceSearchStateInternal.IDLE;
    private List<Choice> voiceSearchSuccessResultsChoices;
    private Runnable waitingTimeoutRunnable = new Runnable() {
        public void run() {
            if (VoiceSearchNotification.this.controller != null) {
                VoiceSearchNotification.this.multipleResultsMode = false;
                VoiceSearchNotification.this.error = null;
                VoiceSearchNotification.this.responseFromClientTimedOut = true;
                VoiceSearchNotification.this.updateUI(VoiceSearchStateInternal.FAILED);
            }
        }
    };
    private List<String> words;

    static class SinusoidalInterpolator implements Interpolator {
        SinusoidalInterpolator() {
        }

        public float getInterpolation(float input) {
            return (float) ((Math.sin((((double) input) * 3.141592653589793d) - 1.5707963267948966d) * 0.5d) + 0.5d);
        }
    }

    enum VoiceSearchStateInternal {
        IDLE(-1),
        STARTING(R.string.voice_search_starting),
        LISTENING(R.string.voice_search_listening),
        SEARCHING(R.string.voice_search_searching),
        FAILED(R.string.voice_search_failed),
        RESULTS(R.string.voice_search_more_results);
        
        private int statusTextResId;

        public int getStatusTextResId() {
            return this.statusTextResId;
        }

        private VoiceSearchStateInternal(int statusTextResId) {
            this.statusTextResId = statusTextResId;
        }
    }

    static {
        HudApplication.getAppContext().getResources();
        ERROR_TEXT_MAPPING.put(VoiceSearchError.FAILED_TO_START, Integer.valueOf(R.string.voice_search_failed));
        ERROR_TEXT_MAPPING.put(VoiceSearchError.OFFLINE, Integer.valueOf(R.string.voice_search_offline));
        ERROR_TEXT_MAPPING.put(VoiceSearchError.NEED_PERMISSION, Integer.valueOf(R.string.voice_search_need_permission));
        ERROR_TEXT_MAPPING.put(VoiceSearchError.NO_WORDS_RECOGNIZED, Integer.valueOf(R.string.voice_search_failed));
        ERROR_TEXT_MAPPING.put(VoiceSearchError.NOT_AVAILABLE, Integer.valueOf(R.string.voice_search_failed));
        ERROR_TEXT_MAPPING.put(VoiceSearchError.NO_RESULTS_FOUND, Integer.valueOf(R.string.voice_search_no_results_found));
        ERROR_TEXT_MAPPING.put(VoiceSearchError.AMBIENT_NOISE, Integer.valueOf(R.string.voice_search_ambient_noise));
        BADGE_ICON_MAPPING.put(VoiceSearchError.FAILED_TO_START, Integer.valueOf(R.drawable.icon_badge_alert));
        BADGE_ICON_MAPPING.put(VoiceSearchError.OFFLINE, Integer.valueOf(R.drawable.icon_badge_alert));
        BADGE_ICON_MAPPING.put(VoiceSearchError.NEED_PERMISSION, Integer.valueOf(R.drawable.icon_badge_alert));
        BADGE_ICON_MAPPING.put(VoiceSearchError.NO_RESULTS_FOUND, Integer.valueOf(R.drawable.icon_badge_voice_search));
        BADGE_ICON_MAPPING.put(VoiceSearchError.NOT_AVAILABLE, Integer.valueOf(R.drawable.icon_badge_alert));
        BADGE_ICON_MAPPING.put(VoiceSearchError.AMBIENT_NOISE, Integer.valueOf(R.drawable.icon_badge_alert));
        STATE_NAME_MAPPING.put(VoiceSearchStateInternal.STARTING, Integer.valueOf(R.string.voice_search_wait));
        STATE_NAME_MAPPING.put(VoiceSearchStateInternal.LISTENING, Integer.valueOf(R.string.voice_search_speak_now));
        STATE_NAME_MAPPING.put(VoiceSearchStateInternal.FAILED, Integer.valueOf(R.string.voice_search_failed));
        STATE_NAME_MAPPING.put(VoiceSearchStateInternal.SEARCHING, Integer.valueOf(R.string.voice_search_searching));
    }

    public NotificationType getType() {
        return NotificationType.VOICE_SEARCH;
    }

    public String getId() {
        return NotificationId.VOICE_SEARCH_NOTIFICATION_ID;
    }

    public View getExpandedView(Context context, Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(INotificationController controller) {
        this.handler = new Handler();
        if (this.layout != null) {
            this.voiceSearchHandler = RemoteDeviceManager.getInstance().getVoiceSearchHandler();
            this.voiceSearchHandler.setVoiceSearchUserInterface(this);
            this.controller = controller;
            this.error = null;
            this.isListeningOverBluetooth = false;
            this.words = null;
            this.imageStatus.setImageDrawable(null);
            AnalyticsSupport.recordVoiceSearchEntered();
            startVoiceSearch();
        }
    }

    private void startVoiceSearch() {
        this.handler.removeCallbacks(this.waitingTimeoutRunnable);
        this.responseFromClientTimedOut = false;
        updateUI(VoiceSearchStateInternal.STARTING);
        this.voiceSearchHandler.startVoiceSearch();
        this.handler.postDelayed(this.waitingTimeoutRunnable, (long) INITIALIZATION_DELAY_MILLIS);
    }

    public void onVoiceSearchResponse(VoiceSearchResponse response) {
        sLogger.d("onVoiceSearchResponse:" + response);
        this.handler.removeCallbacks(this.waitingTimeoutRunnable);
        if (this.responseFromClientTimedOut) {
            sLogger.d("Not processing further response as the client did not respond in time");
            return;
        }
        AnalyticsSupport.recordVoiceSearchResult(response);
        MusicManager musicManager = RemoteDeviceManager.getInstance().getMusicManager();
        switch (response.state) {
            case VOICE_SEARCH_STARTING:
                this.multipleResultsMode = false;
                this.error = response.error;
                if (!TextUtils.isEmpty(response.locale)) {
                    this.locale = HudLocale.getLocaleForID(response.locale);
                }
                updateUI(VoiceSearchStateInternal.STARTING);
                this.handler.postDelayed(this.waitingTimeoutRunnable, (long) INITIALIZATION_DELAY_MILLIS);
                return;
            case VOICE_SEARCH_SEARCHING:
                if (musicManager != null) {
                    musicManager.acceptResumes();
                }
                this.multipleResultsMode = false;
                this.error = response.error;
                updateUI(VoiceSearchStateInternal.SEARCHING);
                this.handler.postDelayed(this.waitingTimeoutRunnable, (long) SEARCH_DELAY_MILLIS);
                return;
            case VOICE_SEARCH_LISTENING:
                this.multipleResultsMode = false;
                this.isListeningOverBluetooth = ((Boolean) Wire.get(response.listeningOverBluetooth, Boolean.valueOf(false))).booleanValue();
                this.error = response.error;
                updateUI(VoiceSearchStateInternal.LISTENING);
                this.handler.postDelayed(this.waitingTimeoutRunnable, (long) INPUT_DELAY_MILLIS);
                return;
            case VOICE_SEARCH_ERROR:
                if (musicManager != null) {
                    musicManager.acceptResumes();
                }
                this.multipleResultsMode = false;
                this.error = response.error;
                sLogger.d("Error " + response.error);
                updateUI(VoiceSearchStateInternal.FAILED);
                return;
            case VOICE_SEARCH_SUCCESS:
                this.error = response.error;
                if (response.results != null) {
                    boolean startTrip = false;
                    sLogger.v("onVoiceSearchResponse: size=" + response.results.size());
                    if (response.results.size() == 1) {
                        Destination d = (Destination) response.results.get(0);
                        if (d.favorite_type != null) {
                            switch (d.favorite_type) {
                                case FAVORITE_HOME:
                                case FAVORITE_WORK:
                                case FAVORITE_CUSTOM:
                                    startTrip = true;
                                    break;
                                case FAVORITE_NONE:
                                    if (d.last_navigated_to != null && d.last_navigated_to.longValue() > 0) {
                                        startTrip = true;
                                        break;
                                    }
                            }
                        }
                    }
                    if (startTrip) {
                        sLogger.v("onVoiceSearchResponse: start trip");
                        this.multipleResultsMode = false;
                        this.voiceSearchHandler.go();
                        dismissNotification();
                        return;
                    }
                    sLogger.v("onVoiceSearchResponse: show picker");
                    AnalyticsSupport.recordVoiceSearchAdditionalResultsAction(VoiceSearchAdditionalResultsAction.ADDITIONAL_RESULTS_LIST);
                    this.voiceSearchHandler.showResults();
                    return;
                }
                this.multipleResultsMode = false;
                updateUI(VoiceSearchStateInternal.IDLE);
                dismissNotification();
                return;
            default:
                return;
        }
    }

    public void close() {
        dismissNotification();
    }

    private void updateUI(VoiceSearchStateInternal state) {
        updateUI(state, this.error, this.results);
    }

    private void showHideVoiceSearchTipsIfNeeded(boolean show) {
        sLogger.d("showHideVoiceSearchTipsIfNeeded, show : " + show);
        Main main = RemoteDeviceManager.getInstance().getUiStateManager().getRootScreen();
        if (show) {
            sLogger.d("Should show voice search tips :" + this.voiceSearchHandler.shouldShowVoiceSearchTipsToUser());
            if (this.voiceSearchHandler.shouldShowVoiceSearchTipsToUser()) {
                ViewGroup notificationExtensionHolder = (ViewGroup) LayoutInflater.from(HudApplication.getAppContext()).inflate(R.layout.voice_search_tips_layout, main.getNotificationExtensionViewHolder(), false);
                VoiceSearchTipsView voiceSearchTipsView = (VoiceSearchTipsView) notificationExtensionHolder.findViewById(R.id.voice_search_tips);
                voiceSearchTipsView.setListeningOverBluetooth(this.isListeningOverBluetooth);
                if (this.locale != null) {
                    voiceSearchTipsView.setVoiceSearchLocale(this.locale);
                } else {
                    voiceSearchTipsView.setVoiceSearchLocale(DriverProfileHelper.getInstance().getCurrentProfile().getLocale());
                }
                main.addNotificationExtensionView(notificationExtensionHolder);
                this.voiceSearchHandler.showedVoiceSearchTipsToUser();
                return;
            }
            return;
        }
        sLogger.d("Remove notification extension");
        main.removeNotificationExtensionView();
    }

    private void updateUI(VoiceSearchStateInternal newState, VoiceSearchError error, List<Destination> results) {
        sLogger.d("updateUI newState :" + newState + " , Error :" + error);
        boolean stateChanged = this.voiceSearchState != newState;
        if (stateChanged) {
            sLogger.d("State Changed From : " + this.voiceSearchState + " , New state : " + newState);
        }
        this.voiceSearchState = newState;
        AnimatorSet animatorSet = new AnimatorSet();
        Builder animatorSetBuilder = null;
        if (stateChanged) {
            setChoices(newState);
        }
        switch (newState) {
            case FAILED:
                if (error != null && !this.responseFromClientTimedOut) {
                    switch (error) {
                        case NO_RESULTS_FOUND:
                            this.subStatus.setText("");
                            break;
                        default:
                            Integer err = error == null ? null : (Integer) ERROR_TEXT_MAPPING.get(error);
                            if (err == null) {
                                err = Integer.valueOf(R.string.voice_search_failed);
                                sLogger.e(String.format("unhandled error response: %s", new Object[]{error}));
                            }
                            this.subStatus.setText(err.intValue());
                            break;
                    }
                }
                this.subStatus.setText(this.voiceSearchState.getStatusTextResId());
                break;
                break;
            case IDLE:
                if (stateChanged) {
                    this.subStatus.setText("");
                    break;
                }
                break;
            case RESULTS:
                this.subStatus.setText("");
                break;
            default:
                if (stateChanged) {
                    this.subStatus.setText(((Integer) STATE_NAME_MAPPING.get(newState)).intValue());
                    break;
                }
                break;
        }
        AnimatorSet mainImageAnimationReference = null;
        if (stateChanged) {
            this.resultsCountContainer.setVisibility(4);
            if (this.mainImageAnimation != null) {
                this.mainImageAnimation.removeAllListeners();
                this.mainImageAnimation.cancel();
            }
            this.mainImageAnimation = new AnimatorSet();
            mainImageAnimationReference = this.mainImageAnimation;
            switch (newState) {
                case LISTENING:
                    ObjectAnimator showActive = ObjectAnimator.ofFloat(this.imageActive, "alpha", new float[]{0.0f, 1.0f});
                    showActive.addListener(new DefaultAnimationListener() {
                        public void onAnimationEnd(Animator animator) {
                            VoiceSearchNotification.this.imageActive.setAlpha(1.0f);
                        }
                    });
                    mainImageAnimationReference.play(showActive);
                    break;
                default:
                    this.imageInactive.setVisibility(0);
                    if (this.imageActive.getAlpha() > 0.0f) {
                        ObjectAnimator hideActive = ObjectAnimator.ofFloat(this.imageActive, "alpha", new float[]{1.0f, 0.0f});
                        hideActive.addListener(new DefaultAnimationListener() {
                            public void onAnimationEnd(Animator animator) {
                                VoiceSearchNotification.this.imageActive.setAlpha(0.0f);
                            }
                        });
                        mainImageAnimationReference.play(hideActive);
                        break;
                    }
                    break;
            }
            switch (newState) {
                case FAILED:
                    if (error == VoiceSearchError.NO_RESULTS_FOUND) {
                        this.resultsCountContainer.setVisibility(0);
                        this.imageInactive.setVisibility(4);
                        break;
                    }
                    break;
                case RESULTS:
                    if (results != null && results.size() > 0) {
                        this.resultsCount.setText(Integer.toString(results.size()));
                    }
                    this.resultsCountContainer.setVisibility(0);
                    this.imageInactive.setVisibility(4);
                    break;
            }
        }
        Animator processingAnimator = null;
        if (stateChanged) {
            if (this.statusImageAnimation != null) {
                this.statusImageAnimation.removeAllListeners();
                this.statusImageAnimation.cancel();
                this.statusImageAnimation = null;
            }
            this.imageStatus.setVisibility(4);
            if (this.processingImageAnimation != null) {
                this.processingImageAnimation.cancel();
            }
            this.imageProcessing.setVisibility(4);
            this.statusImageAnimation = new AnimatorSet();
            switch (newState) {
                case FAILED:
                    if (error == null) {
                        this.imageStatus.setImageDrawable(null);
                        break;
                    }
                    if (VoiceSearchError.NO_WORDS_RECOGNIZED != error) {
                        Integer icon = error == null ? null : (Integer) BADGE_ICON_MAPPING.get(error);
                        if (icon == null) {
                            icon = Integer.valueOf(R.drawable.icon_badge_alert);
                            sLogger.e(String.format("no icon for error: %s", new Object[]{error}));
                        }
                        this.imageStatus.setImageResource(icon.intValue());
                    } else if (this.isListeningOverBluetooth) {
                        this.imageStatus.setImageResource(R.drawable.icon_badge_b_t);
                    } else {
                        this.imageStatus.setImageResource(R.drawable.icon_badge_phone);
                    }
                    this.statusImageAnimation.play(getBadgeAnimation(this.imageStatus, true));
                    break;
                case RESULTS:
                    this.imageStatus.setImageResource(R.drawable.icon_badge_voice_search);
                    this.statusImageAnimation.play(getBadgeAnimation(this.imageStatus, true));
                    break;
                case LISTENING:
                    if (this.isListeningOverBluetooth) {
                        this.imageStatus.setImageResource(R.drawable.icon_badge_b_t);
                    } else {
                        this.imageStatus.setImageResource(R.drawable.icon_badge_phone);
                    }
                    this.statusImageAnimation.play(getBadgeAnimation(this.imageStatus, true));
                    break;
                case STARTING:
                case SEARCHING:
                    this.imageProcessing.setVisibility(0);
                    if (this.processingImageAnimation == null) {
                        this.processingImageAnimation = new AnimatorSet();
                        this.processingImageAnimation.addListener(new DefaultAnimationListener() {
                            private boolean canceled = false;

                            public void onAnimationStart(Animator animator) {
                                VoiceSearchNotification.this.imageProcessing.setRotation(0.0f);
                                this.canceled = false;
                            }

                            public void onAnimationEnd(Animator animator) {
                                if (!this.canceled) {
                                    VoiceSearchNotification.this.processingImageAnimation.setStartDelay(33);
                                    VoiceSearchNotification.this.processingImageAnimation.start();
                                }
                            }

                            public void onAnimationCancel(Animator animator) {
                                this.canceled = true;
                            }
                        });
                        this.processingImageAnimation.play(getProcessingBadgeAnimation(this.imageProcessing));
                    }
                    processingAnimator = this.processingImageAnimation;
                    break;
            }
        }
        if (mainImageAnimationReference != null) {
            animatorSetBuilder = animatorSet.play(mainImageAnimationReference);
        }
        if (stateChanged && this.statusImageAnimation != null) {
            if (animatorSetBuilder != null) {
                animatorSetBuilder.before(this.statusImageAnimation);
            } else {
                animatorSet.play(this.statusImageAnimation);
            }
        }
        if (stateChanged && processingAnimator != null) {
            this.processingImageAnimation.start();
        }
        animatorSet.setDuration(300);
        animatorSet.start();
        if (stateChanged) {
            if (this.voiceSearchState == VoiceSearchStateInternal.LISTENING) {
                this.listeningFeedbackView.setVisibility(0);
                if (this.listeningFeedbackAnimation == null) {
                    this.listeningFeedbackAnimation = getListeningFeedbackDefaultAnimation(this.listeningFeedbackView);
                }
                this.listeningFeedbackAnimation.start();
            } else {
                this.listeningFeedbackView.setVisibility(8);
                if (this.listeningFeedbackAnimation != null && this.listeningFeedbackAnimation.isRunning()) {
                    this.listeningFeedbackAnimation.cancel();
                }
            }
        }
        if (!stateChanged || newState != VoiceSearchStateInternal.FAILED) {
            return;
        }
        if (error == VoiceSearchError.NO_RESULTS_FOUND || error == VoiceSearchError.NO_WORDS_RECOGNIZED) {
            sLogger.d("VoiceSearch error , error :" + error + " , showing voice search");
            showHideVoiceSearchTipsIfNeeded(true);
        }
    }

    public void onStop() {
        this.handler.removeCallbacks(this.waitingTimeoutRunnable);
        this.controller = null;
        this.voiceSearchHandler.stopVoiceSearch();
        this.voiceSearchHandler.setVoiceSearchUserInterface(null);
        if (this.layout != null) {
            if (this.processingImageAnimation != null) {
                this.processingImageAnimation.removeAllListeners();
                this.processingImageAnimation.cancel();
                this.processingImageAnimation = null;
            }
            if (this.listeningFeedbackAnimation != null) {
                this.listeningFeedbackAnimation.removeAllListeners();
                this.listeningFeedbackAnimation.cancel();
                this.listeningFeedbackAnimation = null;
            }
        }
        ToastManager.getInstance().disableToasts(false);
        sLogger.v("startVoiceSearch: enabled toast");
    }

    public View getView(Context context) {
        if (this.layout == null) {
            this.layout = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.notification_voice_search, null);
        }
        ButterKnife.inject( this, this.layout);
        Resources resources = context.getResources();
        int dismissColor = resources.getColor(R.color.glance_dismiss);
        int retryColor = resources.getColor(R.color.glance_ok_blue);
        int goColor = resources.getColor(R.color.glance_ok_go);
        int showColor = resources.getColor(R.color.glance_ok_blue);
        Choice dismissChoice = new Choice(R.id.dismiss, R.drawable.icon_glances_dismiss, dismissColor, R.drawable.icon_glances_dismiss, -16777216, resources.getString(R.string.cancel), dismissColor);
        this.voiceSearchFailedChoices = new ArrayList();
        this.voiceSearchFailedChoices.add(new Choice(R.id.retry, R.drawable.icon_glances_retry, retryColor, R.drawable.icon_glances_retry, -16777216, resources.getString(R.string.retry), retryColor));
        this.voiceSearchFailedChoices.add(dismissChoice);
        this.voiceSearchActiveChoices = new ArrayList();
        this.voiceSearchActiveChoices.add(dismissChoice);
        this.voiceSearchSuccessResultsChoices = new ArrayList();
        this.voiceSearchSuccessResultsChoices.add(new Choice(R.id.list, R.drawable.icon_glances_read, showColor, R.drawable.icon_glances_read, -16777216, resources.getString(R.string.view), showColor));
        this.voiceSearchSuccessResultsChoices.add(new Choice(R.id.go, R.drawable.icon_go, goColor, R.drawable.icon_go, -16777216, resources.getString(R.string.go), goColor));
        this.choiceLayout.setChoices(this.voiceSearchActiveChoices, 0, this);
        this.choiceLayout.setVisibility(0);
        return this.layout;
    }

    private void setChoices(VoiceSearchStateInternal newState) {
        switch (newState) {
            case FAILED:
                this.choiceLayout.setVisibility(0);
                this.choiceLayout.setChoices(this.voiceSearchFailedChoices, 0, this);
                return;
            case RESULTS:
                if (this.results != null && this.results.size() > 1) {
                    this.choiceLayout.setVisibility(0);
                    this.choiceLayout.setChoices(this.voiceSearchSuccessResultsChoices, 0, this);
                    return;
                }
                return;
            case LISTENING:
            case SEARCHING:
                this.choiceLayout.setVisibility(0);
                this.choiceLayout.setChoices(this.voiceSearchActiveChoices, 0, this);
                return;
            default:
                this.choiceLayout.setVisibility(8);
                return;
        }
    }

    public void onUpdate() {
    }

    public int getTimeout() {
        return 0;
    }

    public boolean isAlive() {
        return false;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        return 0;
    }

    public void onNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    public boolean supportScroll() {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        switch (event) {
            case LEFT:
                this.choiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                this.choiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                this.choiceLayout.executeSelectedItem();
                return true;
            default:
                return false;
        }
    }

    private void dismissNotification() {
        NotificationManager.getInstance().removeNotification(NotificationId.VOICE_SEARCH_NOTIFICATION_ID);
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(GestureEvent event) {
        if (this.multipleResultsMode) {
            switch (event.gesture) {
                case GESTURE_SWIPE_LEFT:
                    this.voiceSearchHandler.showResults();
                    return true;
                case GESTURE_SWIPE_RIGHT:
                    this.voiceSearchHandler.go();
                    dismissNotification();
                    return true;
            }
        }
        return false;
    }

    public IInputHandler nextHandler() {
        return null;
    }

    public void executeItem(Selection selection) {
        switch (selection.id) {
            case R.id.dismiss:
                AnalyticsSupport.recordRouteSelectionCancelled();
                dismissNotification();
                return;
            case R.id.go:
                AnalyticsSupport.recordVoiceSearchAdditionalResultsAction(VoiceSearchAdditionalResultsAction.ADDITIONAL_RESULTS_GO);
                this.voiceSearchHandler.go();
                dismissNotification();
                return;
            case R.id.list:
                AnalyticsSupport.recordVoiceSearchAdditionalResultsAction(VoiceSearchAdditionalResultsAction.ADDITIONAL_RESULTS_LIST);
                this.voiceSearchHandler.showResults();
                return;
            case R.id.retry:
                AnalyticsSupport.recordVoiceSearchRetry();
                showHideVoiceSearchTipsIfNeeded(false);
                startVoiceSearch();
                return;
            default:
                return;
        }
    }

    public void itemSelected(Selection selection) {
    }

    public static AnimatorSet getBadgeAnimation(final View view, boolean show) {
        view.setAlpha(1.0f);
        AnimatorSet animationSet = new AnimatorSet();
        animationSet.addListener(new DefaultAnimationListener() {
            public void onAnimationStart(Animator animation) {
                view.setVisibility(0);
                super.onAnimationStart(animation);
            }
        });
        if (show) {
            view.setTranslationY(-10.0f);
            ObjectAnimator translateY = ObjectAnimator.ofFloat(view, "translationY", new float[]{-10.0f, 0.0f});
            animationSet.setDuration(200);
            animationSet.play(translateY);
            animationSet.setInterpolator(new BounceInterpolator());
        } else {
            view.setTranslationY(0.0f);
            ObjectAnimator alpha = ObjectAnimator.ofFloat(view, "alpha", new float[]{1.0f, 0.0f});
            animationSet.setDuration(200);
            animationSet.play(alpha);
        }
        return animationSet;
    }

    public static Animator getProcessingBadgeAnimation(View view) {
        Animator processingAnimator = ObjectAnimator.ofFloat(view, View.ROTATION, new float[]{360.0f});
        processingAnimator.setDuration(300);
        processingAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        return processingAnimator;
    }

    public static AnimatorSet getListeningFeedbackDefaultAnimation(View view) {
        final AnimatorSet animationSet = (AnimatorSet) AnimatorInflater.loadAnimator(view.getContext(), R.animator.listening_feedback_animation);
        animationSet.setTarget(view);
        animationSet.addListener(new DefaultAnimationListener() {
            private boolean canceled = false;

            public void onAnimationStart(Animator animator) {
                this.canceled = false;
            }

            public void onAnimationEnd(Animator animator) {
                if (!this.canceled) {
                    animationSet.start();
                }
            }

            public void onAnimationCancel(Animator animator) {
                this.canceled = true;
            }
        });
        return animationSet;
    }
}
