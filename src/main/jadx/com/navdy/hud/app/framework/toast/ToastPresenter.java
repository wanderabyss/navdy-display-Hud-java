package com.navdy.hud.app.framework.toast;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.amazonaws.services.s3.internal.Constants;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.device.light.HUDLightUtils;
import com.navdy.hud.app.device.light.LED.Settings;
import com.navdy.hud.app.device.light.LightManager;
import com.navdy.hud.app.framework.toast.ToastManager.DismissedToast;
import com.navdy.hud.app.framework.toast.ToastManager.ToastInfo;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout.IListener;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.ui.component.image.InitialsImageView.Style;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.util.ViewUtil;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.hud.app.view.MaxWidthLinearLayout;
import com.navdy.hud.app.view.ToastView;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class ToastPresenter {
    public static final int ANIMATION_TRANSLATION;
    private static final List<String> DISMISS_CHOICE = new ArrayList();
    public static final String EXTRA_CHOICE_LAYOUT_PADDING = "15";
    public static final String EXTRA_CHOICE_LIST = "20";
    public static final String EXTRA_DEFAULT_CHOICE = "12";
    public static final String EXTRA_INFO_CONTAINER_DEFAULT_MAX_WIDTH = "16_1";
    public static final String EXTRA_INFO_CONTAINER_LEFT_PADDING = "16_2";
    public static final String EXTRA_INFO_CONTAINER_MAX_WIDTH = "16";
    public static final String EXTRA_MAIN_IMAGE = "8";
    public static final String EXTRA_MAIN_IMAGE_CACHE_KEY = "9";
    public static final String EXTRA_MAIN_IMAGE_INITIALS = "10";
    public static final String EXTRA_MAIN_TITLE = "1";
    public static final String EXTRA_MAIN_TITLE_1 = "2";
    public static final String EXTRA_MAIN_TITLE_1_STYLE = "3";
    public static final String EXTRA_MAIN_TITLE_2 = "4";
    public static final String EXTRA_MAIN_TITLE_2_STYLE = "5";
    public static final String EXTRA_MAIN_TITLE_3 = "6";
    public static final String EXTRA_MAIN_TITLE_3_STYLE = "7";
    public static final String EXTRA_MAIN_TITLE_STYLE = "1_1";
    public static final String EXTRA_NO_START_DELAY = "21";
    public static final String EXTRA_SHOW_SCREEN_ID = "18";
    public static final String EXTRA_SIDE_IMAGE = "11";
    public static final String EXTRA_SUPPORTS_GESTURE = "19";
    public static final String EXTRA_TIMEOUT = "13";
    public static final String EXTRA_TOAST_ID = "id";
    public static final String EXTRA_TTS = "17";
    public static final int TOAST_ANIM_DURATION_IN = 100;
    public static final int TOAST_ANIM_DURATION_OUT = 100;
    public static final int TOAST_ANIM_DURATION_OUT_QUICK = 50;
    public static final int TOAST_START_DELAY = 250;
    private static final Bus bus;
    private static IListener choiceListener = new IListener() {
        public void executeItem(int pos, int id) {
            if (ToastPresenter.toastCallback != null) {
                ToastPresenter.toastCallback.executeChoiceItem(pos, id);
            }
        }

        public void itemSelected(int pos, int id) {
            ToastPresenter.resetTimeout();
        }
    };
    private static ToastInfo currentInfo;
    private static IListener defaultChoiceListener = new IListener() {
        public void executeItem(int pos, int id) {
            ToastPresenter.dismiss(false);
        }

        public void itemSelected(int pos, int id) {
        }
    };
    public static final int defaultMaxInfoWidth;
    private static Settings gestureLedSettings;
    private static final GestureServiceConnector gestureServiceConnector;
    private static Handler handler = new Handler(Looper.getMainLooper());
    private static String id;
    private static int mainSectionBottomPadding = -1;
    private static final Logger sLogger = new Logger(ToastPresenter.class);
    private static volatile String screenName;
    private static boolean supportsGesture;
    private static int timeout;
    private static Runnable timeoutRunnable = new Runnable() {
        public void run() {
            if (ToastPresenter.toastView != null) {
                ToastPresenter.sLogger.v("timeoutRunnable");
                ToastPresenter.toastView.animateOut(false);
            }
        }
    };
    private static IToastCallback toastCallback;
    private static ToastView toastView;

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        DISMISS_CHOICE.add(resources.getString(R.string.dismiss));
        ANIMATION_TRANSLATION = (int) resources.getDimension(R.dimen.toast_anim_translation);
        defaultMaxInfoWidth = (int) resources.getDimension(R.dimen.carousel_main_right_section_width);
        RemoteDeviceManager remoteDeviceManager = RemoteDeviceManager.getInstance();
        bus = remoteDeviceManager.getBus();
        gestureServiceConnector = remoteDeviceManager.getGestureServiceConnector();
    }

    private ToastPresenter() {
    }

    public static void dismiss() {
        dismiss(false);
    }

    public static void dismiss(final boolean quickly) {
        if (GenericUtil.isMainThread()) {
            dismissInternal(quickly);
        } else {
            handler.post(new Runnable() {
                public void run() {
                    ToastPresenter.dismissInternal(quickly);
                }
            });
        }
    }

    private static void dismissInternal(boolean quickly) {
        sLogger.v("dismissInternal");
        handler.removeCallbacks(timeoutRunnable);
        if (toastView != null) {
            toastView.animateOut(quickly);
        }
    }

    static void present(ToastView view, ToastInfo info) {
        if (view == null || info == null) {
            sLogger.e("invalid toast info");
            return;
        }
        currentInfo = info;
        toastView = view;
        if (mainSectionBottomPadding == -1) {
            mainSectionBottomPadding = toastView.getMainLayout().mainSection.getPaddingBottom();
        }
        updateView(view, info);
    }

    public static void clear() {
        if (GenericUtil.isMainThread()) {
            clearInternal();
        } else {
            handler.post(new Runnable() {
                public void run() {
                    ToastPresenter.clearInternal();
                }
            });
        }
    }

    private static void clearInternal() {
        sLogger.v("clearInternal");
        handler.removeCallbacks(timeoutRunnable);
        if (toastCallback != null) {
            sLogger.v("called onStop():" + id);
            toastCallback.onStop();
            if (supportsGesture) {
                HUDLightUtils.removeSettings(gestureLedSettings);
            }
        }
        bus.post(new DismissedToast(id));
        currentInfo = null;
        supportsGesture = false;
        toastCallback = null;
        sLogger.v("toast-cb null");
        toastView = null;
        id = null;
        timeout = 0;
    }

    public static void resetTimeout() {
        if (timeout > 0) {
            handler.removeCallbacks(timeoutRunnable);
            handler.postDelayed(timeoutRunnable, (long) timeout);
            sLogger.v("reset timeout = " + timeout);
        }
    }

    public static IToastCallback getCurrentCallback() {
        return toastCallback;
    }

    public static String getCurrentId() {
        return id;
    }

    public static boolean hasTimeout() {
        return timeout > 0;
    }

    private static void updateView(ToastView view, ToastInfo info) {
        Context context = HudApplication.getAppContext();
        Bundle data = info.toastParams.data;
        ConfirmationLayout layout = view.getConfirmation();
        layout.screenTitle.setTextAppearance(context, R.style.mainTitle);
        layout.title1.setTextAppearance(context, R.style.title1);
        layout.title2.setTextAppearance(context, R.style.title2);
        layout.title3.setTextAppearance(context, R.style.title3);
        int padding = data.getInt(EXTRA_CHOICE_LAYOUT_PADDING, 0);
        if (padding > 0) {
            LayoutParams params = (LayoutParams) layout.choiceLayout.getLayoutParams();
            params.topMargin = 0;
            params.leftMargin = 0;
            params.rightMargin = 0;
            params.bottomMargin = padding;
        }
        id = data.getString("id");
        toastCallback = ToastManager.getInstance().getCallback(id);
        sLogger.v(new StringBuilder().append("toast-cb-update :").append(toastCallback).toString() == null ? Constants.NULL_VERSION_ID : "not null");
        int mainImage = data.getInt(EXTRA_MAIN_IMAGE, 0);
        String mainImagePath = data.getString(EXTRA_MAIN_IMAGE_CACHE_KEY);
        String initials = data.getString(EXTRA_MAIN_IMAGE_INITIALS);
        timeout = data.getInt(EXTRA_TIMEOUT, 0);
        boolean defaultChoice = data.getBoolean(EXTRA_DEFAULT_CHOICE, false);
        int sideImage = data.getInt(EXTRA_SIDE_IMAGE, 0);
        String tts = null;
        screenName = data.getString(EXTRA_SHOW_SCREEN_ID, null);
        supportsGesture = data.getBoolean(EXTRA_SUPPORTS_GESTURE);
        if (supportsGesture && !gestureServiceConnector.isRunning()) {
            sLogger.v("gesture not available, turn off");
            supportsGesture = false;
        }
        ArrayList<Choice> choices = data.getParcelableArrayList(EXTRA_CHOICE_LIST);
        MaxWidthLinearLayout infoContainer = (MaxWidthLinearLayout) layout.findViewById(R.id.infoContainer);
        int maxWidth = 0;
        if (data.containsKey(EXTRA_INFO_CONTAINER_MAX_WIDTH)) {
            int infoContainerWidth = data.getInt(EXTRA_INFO_CONTAINER_MAX_WIDTH);
            maxWidth = infoContainerWidth > 0 ? infoContainerWidth : 0;
        } else if (data.containsKey(EXTRA_INFO_CONTAINER_DEFAULT_MAX_WIDTH)) {
            maxWidth = defaultMaxInfoWidth;
        }
        infoContainer.setMaxWidth(maxWidth);
        ((MarginLayoutParams) infoContainer.getLayoutParams()).width = -2;
        if (data.containsKey(EXTRA_INFO_CONTAINER_LEFT_PADDING)) {
            infoContainer.setPadding(data.getInt(EXTRA_INFO_CONTAINER_LEFT_PADDING), infoContainer.getPaddingTop(), infoContainer.getPaddingRight(), infoContainer.getPaddingBottom());
        }
        layout.fluctuatorView.setVisibility(8);
        int lines = (((0 + applyTextAndStyle(layout.screenTitle, data, EXTRA_MAIN_TITLE, EXTRA_MAIN_TITLE_STYLE, 1)) + applyTextAndStyle(layout.title1, data, EXTRA_MAIN_TITLE_1, EXTRA_MAIN_TITLE_1_STYLE, 1)) + applyTextAndStyle(layout.title2, data, EXTRA_MAIN_TITLE_2, EXTRA_MAIN_TITLE_2_STYLE, 1)) + applyTextAndStyle(layout.title3, data, EXTRA_MAIN_TITLE_3, EXTRA_MAIN_TITLE_3_STYLE, 3);
        View v = layout.mainSection;
        if (lines > 4) {
            ViewUtil.setBottomPadding(layout.mainSection, mainSectionBottomPadding + context.getResources().getDimensionPixelOffset(R.dimen.main_section_vertical_padding));
        } else {
            ViewUtil.setBottomPadding(layout.mainSection, mainSectionBottomPadding);
        }
        if (mainImage != 0) {
            if (initials != null) {
                layout.screenImage.setImage(mainImage, initials, Style.LARGE);
                layout.screenImage.setVisibility(0);
            } else {
                layout.screenImage.setImage(mainImage, null, Style.DEFAULT);
                layout.screenImage.setVisibility(0);
            }
        } else if (mainImagePath != null) {
            Bitmap bitmap = PicassoUtil.getBitmapfromCache(new File(mainImagePath));
            if (bitmap != null) {
                layout.screenImage.setInitials(null, Style.DEFAULT);
                layout.screenImage.setImageBitmap(bitmap);
                layout.screenImage.setVisibility(0);
            }
        } else {
            layout.screenImage.setImageResource(0);
            layout.screenImage.setVisibility(8);
        }
        if (sideImage != 0) {
            layout.sideImage.setImageResource(sideImage);
            layout.sideImage.setVisibility(0);
        } else {
            layout.sideImage.setVisibility(8);
        }
        if (defaultChoice) {
            layout.setChoices(DISMISS_CHOICE, 0, defaultChoiceListener);
            layout.choiceLayout.setVisibility(0);
            supportsGesture = false;
        } else if (choices == null) {
            supportsGesture = false;
            layout.choiceLayout.setVisibility(8);
        } else if (!supportsGesture || choices.size() <= 2) {
            layout.setChoicesList(choices, 0, choiceListener);
            layout.choiceLayout.setVisibility(0);
        } else {
            throw new RuntimeException("max 2 choice allowed when gesture are on");
        }
        if (supportsGesture) {
            layout.leftSwipe.setVisibility(0);
            layout.rightSwipe.setVisibility(0);
            toastView.gestureOn = true;
        } else {
            layout.leftSwipe.setVisibility(8);
            layout.rightSwipe.setVisibility(8);
            toastView.gestureOn = defaultChoice;
        }
        if (timeout > 0) {
            handler.postDelayed(timeoutRunnable, (long) (timeout + 100));
            sLogger.v("timeout = " + (timeout + 100));
        }
        if (toastCallback != null) {
            sLogger.v("called onStart():" + id);
            toastCallback.onStart(view);
            if (supportsGesture) {
                gestureLedSettings = HUDLightUtils.showGestureDetectionEnabled(HudApplication.getAppContext(), LightManager.getInstance(), "Toast" + data.getString(EXTRA_MAIN_TITLE));
            }
        }
        if (null != null) {
            if (info.ttsDone) {
                tts = null;
            } else {
                info.ttsDone = true;
            }
        }
        view.animateIn(tts, screenName, id, data.getBoolean(EXTRA_NO_START_DELAY, false));
    }

    private static int applyTextAndStyle(TextView view, Bundle bundle, String textAttribute, String styleAttribute, int maxLines) {
        return ViewUtil.applyTextAndStyle(view, bundle.getString(textAttribute), maxLines, bundle.getInt(styleAttribute, -1));
    }

    public static void clearScreenName() {
        screenName = null;
    }

    public static String getScreenName() {
        return screenName;
    }
}
