package com.navdy.hud.app.framework.message;

import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.makeramen.RoundedTransformationBuilder;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.framework.contacts.ContactUtil.IContactCallback;
import com.navdy.hud.app.framework.glance.GlanceHandler;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout2.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Selection;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.util.PhoneUtil;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import com.squareup.picasso.Transformation;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SmsNotification implements INotification, IListener, IContactCallback {
    private static final int NOTIFICATION_TIMEOUT = ((int) TimeUnit.SECONDS.toMillis(5));
    private static final int TIMEOUT = ((int) TimeUnit.SECONDS.toMillis(10));
    private static final List<Choice> choicesDismiss = new ArrayList();
    private static final List<Choice> choicesRetryAndDismiss = new ArrayList();
    private static final Handler handler = new Handler(Looper.getMainLooper());
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private static final Logger sLogger = new Logger(SmsNotification.class);
    @InjectView(R.id.choice_layout)
    ChoiceLayout2 choiceLayout;
    private INotificationController controller;
    private final String message;
    private final Mode mode;
    private final String name;
    private final String notifId;
    @InjectView(R.id.notification_user_image)
    InitialsImageView notificationUserImage;
    private final String number;
    private final String rawNumber;
    private boolean retrySendingMessage;
    private Transformation roundTransformation = new RoundedTransformationBuilder().oval(true).build();
    @InjectView(R.id.badge)
    ImageView sideImage;
    @InjectView(R.id.subtitle)
    TextView subtitle;
    @InjectView(R.id.title)
    TextView title;

    public enum Mode {
        Success,
        Failed
    }

    static {
        String dismiss = resources.getString(R.string.dismiss);
        int dismissColor = resources.getColor(R.color.glance_dismiss);
        int retryColor = resources.getColor(R.color.glance_ok_blue);
        choicesRetryAndDismiss.add(new Choice(R.id.retry, R.drawable.icon_glances_retry, retryColor, R.drawable.icon_glances_retry, -16777216, resources.getString(R.string.retry), retryColor));
        choicesRetryAndDismiss.add(new Choice(R.id.dismiss, R.drawable.icon_glances_dismiss, dismissColor, R.drawable.icon_glances_dismiss, -16777216, dismiss, dismissColor));
        choicesDismiss.add(new Choice(R.id.dismiss, R.drawable.icon_glances_dismiss, dismissColor, R.drawable.icon_glances_dismiss, -16777216, dismiss, dismissColor));
    }

    public SmsNotification(Mode mode, String id, String number, String message, String name) {
        this.mode = mode;
        this.notifId = NotificationId.SMS_NOTIFICATION_ID + id;
        this.number = PhoneUtil.formatPhoneNumber(number);
        this.rawNumber = PhoneUtil.convertToE164Format(number);
        this.message = message;
        this.name = name;
    }

    public NotificationType getType() {
        return NotificationType.SMS;
    }

    public String getId() {
        return this.notifId;
    }

    public View getView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.notification_sms, null);
        ButterKnife.inject( this, view);
        return view;
    }

    private void setUI() {
        if (!TextUtils.isEmpty(this.name)) {
            this.subtitle.setText(this.name);
        } else if (TextUtils.isEmpty(this.number)) {
            this.subtitle.setText("");
        } else {
            this.subtitle.setText(PhoneUtil.formatPhoneNumber(this.number));
        }
        ContactUtil.setContactPhoto(this.name, this.number, false, this.notificationUserImage, this.roundTransformation, this);
        switch (this.mode) {
            case Failed:
                this.title.setText(resources.getString(R.string.reply_failed));
                this.subtitle.setText(getContactName());
                this.sideImage.setImageResource(R.drawable.icon_msg_failed);
                this.choiceLayout.setChoices(choicesRetryAndDismiss, 0, this);
                return;
            case Success:
                this.title.setText(resources.getString(R.string.reply_sent));
                this.subtitle.setText(getContactName());
                this.sideImage.setImageResource(R.drawable.icon_msg_success);
                this.choiceLayout.setChoices(choicesDismiss, 0, this);
                return;
            default:
                return;
        }
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public View getExpandedView(Context context, Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(INotificationController controller) {
        this.controller = controller;
        setUI();
    }

    public void onUpdate() {
    }

    public void onStop() {
        this.controller = null;
        if (this.retrySendingMessage) {
            handler.post(new Runnable() {
                public void run() {
                    GlanceHandler glanceHandler = GlanceHandler.getInstance();
                    if (glanceHandler.sendMessage(SmsNotification.this.rawNumber, SmsNotification.this.message, SmsNotification.this.name)) {
                        SmsNotification.sLogger.v("retry message sent");
                        glanceHandler.sendSmsSuccessNotification(SmsNotification.this.notifId, SmsNotification.this.number, SmsNotification.this.message, SmsNotification.this.name);
                        return;
                    }
                    SmsNotification.sLogger.v("retry message not sent");
                    glanceHandler.sendSmsFailedNotification(SmsNotification.this.number, SmsNotification.this.message, SmsNotification.this.name);
                }
            });
        }
    }

    public int getTimeout() {
        switch (this.mode) {
            case Failed:
                return TIMEOUT;
            default:
                return NOTIFICATION_TIMEOUT;
        }
    }

    public boolean isAlive() {
        return false;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        return 0;
    }

    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    public boolean supportScroll() {
        return false;
    }

    public void executeItem(Selection selection) {
        switch (selection.id) {
            case R.id.dismiss:
                NotificationManager.getInstance().removeNotification(this.notifId);
                return;
            case R.id.retry:
                this.retrySendingMessage = true;
                NotificationManager.getInstance().removeNotification(this.notifId);
                return;
            default:
                return;
        }
    }

    public void itemSelected(Selection selection) {
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.controller == null) {
            return false;
        }
        switch (event) {
            case LEFT:
                this.choiceLayout.moveSelectionLeft();
                this.controller.resetTimeout();
                return true;
            case RIGHT:
                this.choiceLayout.moveSelectionRight();
                this.controller.resetTimeout();
                return true;
            case SELECT:
                this.choiceLayout.executeSelectedItem();
                return true;
            default:
                return false;
        }
    }

    public IInputHandler nextHandler() {
        return null;
    }

    public boolean isContextValid() {
        return this.controller != null;
    }

    private String getContactName() {
        if (!TextUtils.isEmpty(this.name)) {
            return this.name;
        }
        if (TextUtils.isEmpty(this.number)) {
            return "";
        }
        return this.number;
    }
}
