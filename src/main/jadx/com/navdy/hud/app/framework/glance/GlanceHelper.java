package com.navdy.hud.app.framework.glance;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager.ToastParams;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.profile.NotificationSettings;
import com.navdy.service.library.events.glances.CalendarConstants;
import com.navdy.service.library.events.glances.EmailConstants;
import com.navdy.service.library.events.glances.FuelConstants;
import com.navdy.service.library.events.glances.GenericConstants;
import com.navdy.service.library.events.glances.GlanceEvent;
import com.navdy.service.library.events.glances.GlanceEvent.GlanceType;
import com.navdy.service.library.events.glances.KeyValue;
import com.navdy.service.library.events.glances.MessageConstants;
import com.navdy.service.library.events.glances.SocialConstants;
import com.navdy.service.library.log.Logger;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class GlanceHelper {
    private static final HashMap<String, GlanceApp> APP_ID_MAP = new HashMap();
    public static final String DELETE_ALL_GLANCES_TOAST_ID = "glance-deleted";
    public static final String FUEL_PACKAGE = "com.navdy.fuel";
    private static final AtomicLong ID = new AtomicLong(1);
    public static final String IOS_PHONE_PACKAGE = "com.apple.mobilephone";
    public static final String MICROSOFT_OUTLOOK = "com.microsoft.Office.Outlook";
    public static final String MUSIC_PACKAGE = "com.navdy.music";
    public static final String PHONE_PACKAGE = "com.navdy.phone";
    public static final String SMS_PACKAGE = "com.navdy.sms";
    public static final String TRAFFIC_PACKAGE = "com.navdy.traffic";
    public static final String VOICE_SEARCH_PACKAGE = "com.navdy.voice.search";
    private static final StringBuilder builder = new StringBuilder();
    private static final GestureServiceConnector gestureServiceConnector = RemoteDeviceManager.getInstance().getGestureServiceConnector();
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private static final Logger sLogger = new Logger(GlanceHelper.class);

    static {
        APP_ID_MAP.put(FUEL_PACKAGE, GlanceApp.FUEL);
        APP_ID_MAP.put("com.google.android.calendar", GlanceApp.GOOGLE_CALENDAR);
        APP_ID_MAP.put("com.google.android.gm", GlanceApp.GOOGLE_MAIL);
        APP_ID_MAP.put("com.google.android.talk", GlanceApp.GOOGLE_HANGOUT);
        APP_ID_MAP.put("com.Slack", GlanceApp.SLACK);
        APP_ID_MAP.put("com.whatsapp", GlanceApp.WHATS_APP);
        APP_ID_MAP.put("com.facebook.orca", GlanceApp.FACEBOOK_MESSENGER);
        APP_ID_MAP.put("com.facebook.katana", GlanceApp.FACEBOOK);
        APP_ID_MAP.put("com.twitter.android", GlanceApp.TWITTER);
        APP_ID_MAP.put(SMS_PACKAGE, GlanceApp.SMS);
        APP_ID_MAP.put("com.google.android.apps.inbox", GlanceApp.GOOGLE_INBOX);
        APP_ID_MAP.put("com.google.calendar", GlanceApp.GOOGLE_CALENDAR);
        APP_ID_MAP.put("com.google.Gmail", GlanceApp.GOOGLE_MAIL);
        APP_ID_MAP.put("com.google.hangouts", GlanceApp.GOOGLE_HANGOUT);
        APP_ID_MAP.put("com.tinyspeck.chatlyio", GlanceApp.SLACK);
        APP_ID_MAP.put("net.whatsapp.WhatsApp", GlanceApp.WHATS_APP);
        APP_ID_MAP.put("com.facebook.Messenger", GlanceApp.FACEBOOK_MESSENGER);
        APP_ID_MAP.put("com.facebook.Facebook", GlanceApp.FACEBOOK);
        APP_ID_MAP.put("com.atebits.Tweetie2", GlanceApp.TWITTER);
        APP_ID_MAP.put(NotificationSettings.APP_ID_SMS, GlanceApp.IMESSAGE);
        APP_ID_MAP.put(NotificationSettings.APP_ID_MAIL, GlanceApp.APPLE_MAIL);
        APP_ID_MAP.put(NotificationSettings.APP_ID_CALENDAR, GlanceApp.APPLE_CALENDAR);
        APP_ID_MAP.put(MICROSOFT_OUTLOOK, GlanceApp.GENERIC_MAIL);
    }

    public static String getId() {
        return String.valueOf(ID.getAndIncrement());
    }

    public static GlanceApp getGlancesApp(String appId) {
        return (GlanceApp) APP_ID_MAP.get(appId);
    }

    public static GlanceApp getGlancesApp(GlanceEvent event) {
        if (event == null) {
            return null;
        }
        GlanceApp app = (GlanceApp) APP_ID_MAP.get(event.provider);
        if (app != null) {
            return app;
        }
        switch (event.glanceType) {
            case GLANCE_TYPE_EMAIL:
                return GlanceApp.GENERIC_MAIL;
            case GLANCE_TYPE_CALENDAR:
                return GlanceApp.GENERIC_CALENDAR;
            case GLANCE_TYPE_MESSAGE:
                return GlanceApp.GENERIC_MESSAGE;
            case GLANCE_TYPE_SOCIAL:
                return GlanceApp.GENERIC_SOCIAL;
            default:
                return null;
        }
    }

    public static Map<String, String> buildDataMap(GlanceEvent event) {
        Map<String, String> map = new HashMap();
        if (event.glanceData != null) {
            for (KeyValue keyValue : event.glanceData) {
                map.put(keyValue.key, ContactUtil.sanitizeString(keyValue.value));
            }
        }
        return map;
    }

    public static String getNotificationId(GlanceEvent event) {
        return getNotificationId(event.glanceType, event.id);
    }

    public static String getNotificationId(GlanceType glanceType, String id) {
        String prefix;
        switch (glanceType) {
            case GLANCE_TYPE_EMAIL:
                prefix = GlanceConstants.EMAIL_PREFIX;
                break;
            case GLANCE_TYPE_CALENDAR:
                prefix = GlanceConstants.CALENDAR_PREFIX;
                break;
            case GLANCE_TYPE_MESSAGE:
                prefix = GlanceConstants.MESSAGE_PREFIX;
                break;
            case GLANCE_TYPE_SOCIAL:
                prefix = GlanceConstants.SOCIAL_PREFIX;
                break;
            case GLANCE_TYPE_FUEL:
                prefix = GlanceConstants.FUEL_PREFIX;
                break;
            default:
                prefix = GlanceConstants.GENERIC_PREFIX;
                break;
        }
        return prefix + id;
    }

    public static String getTitle(GlanceApp app, Map<String, String> data) {
        String title = "";
        if (data == null) {
            return title;
        }
        switch (app) {
            case GOOGLE_HANGOUT:
                title = (String) data.get(MessageConstants.MESSAGE_FROM.name());
                break;
            case SLACK:
            case WHATS_APP:
            case FACEBOOK_MESSENGER:
            case GENERIC_MESSAGE:
                title = (String) data.get(MessageConstants.MESSAGE_FROM.name());
                break;
            case FACEBOOK:
                title = (String) data.get(SocialConstants.SOCIAL_FROM.name());
                if (TextUtils.isEmpty(title)) {
                    title = GlanceConstants.facebook;
                    break;
                }
                break;
            case TWITTER:
            case GENERIC_SOCIAL:
                title = (String) data.get(SocialConstants.SOCIAL_FROM.name());
                break;
            case IMESSAGE:
                title = (String) data.get(MessageConstants.MESSAGE_FROM.name());
                if (title == null) {
                    title = (String) data.get(MessageConstants.MESSAGE_FROM_NUMBER.name());
                    break;
                }
                break;
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                title = (String) data.get(CalendarConstants.CALENDAR_TITLE.name());
                break;
            case GOOGLE_MAIL:
            case APPLE_MAIL:
            case GENERIC_MAIL:
            case GOOGLE_INBOX:
                title = (String) data.get(EmailConstants.EMAIL_FROM_NAME.name());
                if (title == null) {
                    title = (String) data.get(EmailConstants.EMAIL_FROM_EMAIL.name());
                    break;
                }
                break;
            case SMS:
                title = (String) data.get(MessageConstants.MESSAGE_FROM.name());
                if (title == null) {
                    title = (String) data.get(MessageConstants.MESSAGE_FROM_NUMBER.name());
                    break;
                }
                break;
            case FUEL:
                if (((String) data.get(FuelConstants.FUEL_LEVEL.name())) == null) {
                    title = resources.getString(R.string.gas_station);
                    break;
                }
                title = String.format("%s %s%%", new Object[]{GlanceConstants.fuelNotificationTitle, data.get(FuelConstants.FUEL_LEVEL.name())});
                break;
            default:
                title = GlanceConstants.notification;
                break;
        }
        if (title == null) {
            title = "";
        }
        return title;
    }

    public static String getSubTitle(GlanceApp app, Map<String, String> data) {
        String subTitle = "";
        if (data == null) {
            return subTitle;
        }
        switch (app) {
            case SLACK:
                subTitle = (String) data.get(MessageConstants.MESSAGE_DOMAIN.name());
                if (subTitle != null) {
                    subTitle = resources.getString(R.string.slack_team, new Object[]{subTitle});
                    break;
                }
                break;
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                subTitle = (String) data.get(CalendarConstants.CALENDAR_TIME_STR.name());
                break;
            case GOOGLE_MAIL:
            case APPLE_MAIL:
            case GENERIC_MAIL:
            case GOOGLE_INBOX:
                subTitle = (String) data.get(EmailConstants.EMAIL_SUBJECT.name());
                break;
            case FUEL:
                if (data.get(FuelConstants.NO_ROUTE.name()) == null) {
                    subTitle = GlanceConstants.fuelNotificationSubtitle;
                    break;
                }
                break;
            case GENERIC:
                subTitle = (String) data.get(GenericConstants.GENERIC_TITLE.name());
                break;
        }
        if (subTitle == null) {
            subTitle = "";
        }
        return subTitle;
    }

    public static String getNumber(GlanceApp app, Map<String, String> data) {
        String number = null;
        if (data == null) {
            return null;
        }
        switch (app) {
            case GOOGLE_HANGOUT:
            case IMESSAGE:
            case SMS:
                number = (String) data.get(MessageConstants.MESSAGE_FROM_NUMBER.name());
                break;
        }
        return number;
    }

    public static String getFrom(GlanceApp app, Map<String, String> data) {
        String from = null;
        if (data == null) {
            return null;
        }
        switch (app) {
            case GOOGLE_HANGOUT:
            case IMESSAGE:
            case SMS:
                from = (String) data.get(MessageConstants.MESSAGE_FROM.name());
                break;
        }
        return from;
    }

    public static boolean isPhotoCheckRequired(GlanceApp app) {
        switch (app) {
            case GOOGLE_HANGOUT:
            case IMESSAGE:
            case SMS:
                return true;
            default:
                return false;
        }
    }

    public static synchronized String getGlanceMessage(GlanceApp glanceApp, Map<String, String> dataMap) {
        String message;
        synchronized (GlanceHelper.class) {
            String body;
            switch (glanceApp) {
                case GOOGLE_HANGOUT:
                case SLACK:
                case WHATS_APP:
                case FACEBOOK_MESSENGER:
                case GENERIC_MESSAGE:
                case IMESSAGE:
                case SMS:
                    body = (String) dataMap.get(MessageConstants.MESSAGE_BODY.name());
                    if (body != null) {
                        builder.append(body);
                        break;
                    }
                    break;
                case FACEBOOK:
                case TWITTER:
                case GENERIC_SOCIAL:
                    body = (String) dataMap.get(SocialConstants.SOCIAL_MESSAGE.name());
                    if (body != null) {
                        builder.append(body);
                        break;
                    }
                    break;
                case GOOGLE_MAIL:
                case APPLE_MAIL:
                case GENERIC_MAIL:
                case GOOGLE_INBOX:
                    body = (String) dataMap.get(EmailConstants.EMAIL_BODY.name());
                    if (body != null) {
                        builder.append(body);
                        break;
                    }
                    break;
                case GENERIC:
                    body = (String) dataMap.get(GenericConstants.GENERIC_MESSAGE.name());
                    if (body != null) {
                        builder.append(body);
                        break;
                    }
                    break;
            }
            message = builder.toString();
            builder.setLength(0);
        }
        return message;
    }

    public static synchronized String getTtsMessage(GlanceApp glanceApp, Map<String, String> dataMap, StringBuilder stringBuilder1, StringBuilder stringBuilder2, TimeHelper timeHelper) {
        String tts;
        synchronized (GlanceHelper.class) {
            String body;
            switch (glanceApp) {
                case GOOGLE_HANGOUT:
                case SLACK:
                case WHATS_APP:
                case FACEBOOK_MESSENGER:
                case GENERIC_MESSAGE:
                case IMESSAGE:
                case SMS:
                    body = (String) dataMap.get(MessageConstants.MESSAGE_BODY.name());
                    if (body != null) {
                        builder.append(body);
                        break;
                    }
                    break;
                case FACEBOOK:
                case TWITTER:
                case GENERIC_SOCIAL:
                    body = (String) dataMap.get(SocialConstants.SOCIAL_MESSAGE.name());
                    if (body != null) {
                        builder.append(body);
                        break;
                    }
                    break;
                case GOOGLE_CALENDAR:
                case APPLE_CALENDAR:
                case GENERIC_CALENDAR:
                    String location = (String) dataMap.get(CalendarConstants.CALENDAR_LOCATION.name());
                    String title = (String) dataMap.get(CalendarConstants.CALENDAR_TITLE.name());
                    long millis = 0;
                    String str = (String) dataMap.get(CalendarConstants.CALENDAR_TIME.name());
                    if (str != null) {
                        try {
                            millis = Long.parseLong(str);
                        } catch (Throwable t) {
                            sLogger.e(t);
                        }
                    }
                    String time = null;
                    if (millis > 0) {
                        String data = getCalendarTime(millis, stringBuilder1, stringBuilder2, timeHelper);
                        if (TextUtils.isEmpty(stringBuilder1.toString())) {
                            String pmMarker = stringBuilder2.toString();
                            if (TextUtils.isEmpty(pmMarker)) {
                                time = data + GlanceConstants.PERIOD;
                            } else {
                                String ampmMarker = TextUtils.equals(pmMarker, GlanceConstants.pmMarker) ? GlanceConstants.pm : GlanceConstants.am;
                                time = resources.getString(R.string.tts_calendar_time_at, new Object[]{data, ampmMarker});
                            }
                        } else {
                            try {
                                time = Long.parseLong(data) > 1 ? resources.getString(R.string.tts_calendar_time_mins, new Object[]{data}) : resources.getString(R.string.tts_calendar_time_min, new Object[]{data});
                            } catch (Throwable th) {
                                sLogger.e(th);
                            }
                        }
                    }
                    if (title != null) {
                        builder.append(resources.getString(R.string.tts_calendar_title, new Object[]{title}));
                    }
                    if (time != null) {
                        builder.append(" ");
                        builder.append(time);
                    }
                    if (location != null) {
                        builder.append(" ");
                        builder.append(resources.getString(R.string.tts_calendar_location, new Object[]{location}));
                        break;
                    }
                    break;
                case GOOGLE_MAIL:
                case APPLE_MAIL:
                case GENERIC_MAIL:
                case GOOGLE_INBOX:
                    String subject = (String) dataMap.get(EmailConstants.EMAIL_SUBJECT.name());
                    if (subject != null) {
                        builder.append(subject);
                    }
                    body = (String) dataMap.get(EmailConstants.EMAIL_BODY.name());
                    if (body != null) {
                        if (builder.length() > 0) {
                            builder.append(GlanceConstants.PERIOD);
                            builder.append(" ");
                        }
                        builder.append(body);
                        break;
                    }
                    break;
                case FUEL:
                    if (dataMap.containsKey(FuelConstants.FUEL_LEVEL.name())) {
                        builder.append(resources.getString(R.string.your_fuel_level_is_low_tts));
                    }
                    builder.append(resources.getString(R.string.i_can_add_trip_gas_station, new Object[]{dataMap.get(FuelConstants.GAS_STATION_NAME.name())}));
                    builder.append(GlanceConstants.PERIOD);
                    break;
                case GENERIC:
                    body = (String) dataMap.get(GenericConstants.GENERIC_MESSAGE.name());
                    if (body != null) {
                        builder.append(body);
                        break;
                    }
                    break;
            }
            tts = builder.toString();
            builder.setLength(0);
        }
        return tts;
    }

    public static ViewType getSmallViewType(GlanceApp glanceApp) {
        switch (glanceApp) {
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                return ViewType.SMALL_SIGN;
            default:
                return ViewType.SMALL_GLANCE_MESSAGE;
        }
    }

    public static ViewType getLargeViewType(GlanceApp glanceApp) {
        switch (glanceApp) {
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                return ViewType.BIG_CALENDAR;
            case FUEL:
                return ViewType.BIG_MULTI_TEXT;
            default:
                return ViewType.BIG_GLANCE_MESSAGE_SINGLE;
        }
    }

    public static boolean usesMessageLayout(GlanceApp glanceApp) {
        switch (glanceApp) {
            case GOOGLE_HANGOUT:
            case SLACK:
            case WHATS_APP:
            case FACEBOOK_MESSENGER:
            case GENERIC_MESSAGE:
            case FACEBOOK:
            case TWITTER:
            case GENERIC_SOCIAL:
            case IMESSAGE:
            case GOOGLE_MAIL:
            case APPLE_MAIL:
            case GENERIC_MAIL:
            case GOOGLE_INBOX:
            case SMS:
            case GENERIC:
                return true;
            default:
                return false;
        }
    }

    public static String getTimeStr(long now, Date date, TimeHelper timeHelper) {
        if (now - date.getTime() <= 60000) {
            return GlanceConstants.nowStr;
        }
        return timeHelper.formatTime(date, null);
    }

    public static boolean isDeleteAllView(View view) {
        if (view == null || view.getTag() != GlanceConstants.DELETE_ALL_VIEW_TAG) {
            return false;
        }
        return true;
    }

    public static void showGlancesDeletedToast() {
        Bundle bundle = new Bundle();
        bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 1000);
        bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_qm_glances_grey);
        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(R.string.glances_deleted));
        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
        ToastManager toastManager = ToastManager.getInstance();
        toastManager.dismissCurrentToast();
        toastManager.clearAllPendingToast();
        toastManager.addToast(new ToastParams(DELETE_ALL_GLANCES_TOAST_ID, bundle, null, true, true));
    }

    public static boolean isTrafficNotificationEnabled() {
        return isPackageEnabled(TRAFFIC_PACKAGE);
    }

    public static boolean isFuelNotificationEnabled() {
        return isPackageEnabled(FUEL_PACKAGE);
    }

    public static boolean isPhoneNotificationEnabled() {
        return isPackageEnabled(PHONE_PACKAGE) || isPackageEnabled("com.apple.mobilephone");
    }

    public static boolean isMusicNotificationEnabled() {
        return isPackageEnabled(MUSIC_PACKAGE);
    }

    public static boolean isPackageEnabled(String packageName) {
        if (!DriverProfileHelper.getInstance().getCurrentProfile().getNotificationPreferences().enabled.booleanValue()) {
            return false;
        }
        return Boolean.TRUE.equals(DriverProfileHelper.getInstance().getCurrentProfile().getNotificationSettings().enabled(packageName));
    }

    public static boolean isCalendarApp(GlanceApp glanceApp) {
        switch (glanceApp) {
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                return true;
            default:
                return false;
        }
    }

    public static String getCalendarTime(long time, StringBuilder timeUnit, StringBuilder ampmMarker, TimeHelper timeHelper) {
        long minutes = (long) Math.round(((float) (time - System.currentTimeMillis())) / 60000.0f);
        timeUnit.setLength(0);
        ampmMarker.setLength(0);
        if (minutes >= 60 || minutes < -5) {
            return timeHelper.formatTime12Hour(new Date(time), ampmMarker, true);
        }
        if (minutes <= 0) {
            return GlanceConstants.nowStr;
        }
        String str = String.valueOf(minutes);
        timeUnit.append(GlanceConstants.minute);
        return str;
    }

    public static void initMessageAttributes() {
        Context context = HudApplication.getAppContext();
        TextView textView = new TextView(context);
        textView.setTextAppearance(context, R.style.glance_message_title);
        StaticLayout layout = new StaticLayout("RockgOn", textView.getPaint(), GlanceConstants.messageWidthBound, Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        int height = layout.getHeight() + (GlanceConstants.messageTitleMarginTop + GlanceConstants.messageTitleMarginBottom);
        sLogger.v("message-title-ht = " + height + " layout=" + layout.getHeight());
        GlanceConstants.setMessageTitleHeight(height);
    }

    public static boolean needsScrollLayout(String text) {
        if (TextUtils.isEmpty(text)) {
            return false;
        }
        boolean needsScroll;
        Context context = HudApplication.getAppContext();
        TextView textView = new TextView(context);
        textView.setTextAppearance(context, R.style.glance_message_body);
        float left = ((float) (GlanceConstants.messageHeightBound / 2)) - ((float) (new StaticLayout(text, textView.getPaint(), GlanceConstants.messageWidthBound, Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false).getHeight() / 2));
        int titleHt = GlanceConstants.getMessageTitleHeight();
        if (left > ((float) titleHt)) {
            needsScroll = false;
        } else {
            needsScroll = true;
        }
        sLogger.v("message-tittle-calc left[" + left + "] titleHt [" + titleHt + "]");
        return needsScroll;
    }

    public static GestureServiceConnector getGestureService() {
        return gestureServiceConnector;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int getIcon(String str) {
        if (TextUtils.isEmpty(str)) {
            return -1;
        }
        int i;
        switch (str.hashCode()) {
            case -30680433:
                if (str.equals("GLANCE_ICON_MESSAGE_SIDE_BLUE")) {
                    i = 1;
                    break;
                }
            case -9470533:
                if (str.equals("GLANCE_ICON_NAVDY_MAIN")) {
                    i = 0;
                    break;
                }
            default:
                i = -1;
                break;
        }
        switch (i) {
            case 0:
                return R.drawable.icon_user_navdy;
            case 1:
                return R.drawable.icon_message_blue;
            default:
                return -1;
        }
    }
}
