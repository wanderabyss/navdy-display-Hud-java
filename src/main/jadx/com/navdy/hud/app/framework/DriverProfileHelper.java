package com.navdy.hud.app.framework;

import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.profile.DriverProfileManager;
import java.util.Locale;
import javax.inject.Inject;
import mortar.Mortar;

public class DriverProfileHelper {
    private static final DriverProfileHelper sInstance = new DriverProfileHelper();
    @Inject
    DriverProfileManager driverProfileManager;

    private DriverProfileHelper() {
        Mortar.inject(HudApplication.getAppContext(), this);
    }

    public static DriverProfileHelper getInstance() {
        return sInstance;
    }

    public DriverProfile getCurrentProfile() {
        return this.driverProfileManager.getCurrentProfile();
    }

    public DriverProfileManager getDriverProfileManager() {
        return this.driverProfileManager;
    }

    public Locale getCurrentLocale() {
        return this.driverProfileManager.getCurrentLocale();
    }
}
