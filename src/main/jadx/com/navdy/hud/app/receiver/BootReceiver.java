package com.navdy.hud.app.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.navdy.hud.app.common.ProdModule;
import com.navdy.hud.app.debug.SettingsActivity;
import com.navdy.hud.app.ui.activity.MainActivity;
import com.navdy.service.library.log.Logger;

public class BootReceiver extends BroadcastReceiver {
    private static final Logger sLogger = new Logger(BootReceiver.class);

    public void onReceive(Context context, Intent intent) {
        boolean startOnBoot = context.getSharedPreferences(ProdModule.PREF_NAME, 0).getBoolean(SettingsActivity.START_ON_BOOT_KEY, true);
        sLogger.d("Start on boot:" + startOnBoot);
        if (startOnBoot) {
            Intent myIntent = new Intent(context, MainActivity.class);
            myIntent.addFlags(268435456);
            context.startActivity(myIntent);
        }
    }
}
