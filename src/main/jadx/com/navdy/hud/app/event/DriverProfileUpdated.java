package com.navdy.hud.app.event;

public class DriverProfileUpdated {
    public State state;

    public enum State {
        UP_TO_DATE,
        UPDATED
    }

    public DriverProfileUpdated(State state) {
        this.state = state;
    }
}
