package com.navdy.hud.app.event;

import com.squareup.wire.Message;

public class RemoteEvent {
    protected Message message;

    public RemoteEvent(Message message) {
        this.message = message;
    }

    public Message getMessage() {
        return this.message;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RemoteEvent that = (RemoteEvent) o;
        if (this.message != null) {
            if (this.message.equals(that.message)) {
                return true;
            }
        } else if (that.message == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.message != null ? this.message.hashCode() : 0;
    }
}
