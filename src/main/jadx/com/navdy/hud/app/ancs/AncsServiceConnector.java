package com.navdy.hud.app.ancs;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.ParcelUuid;
import android.os.RemoteException;
import android.os.SystemClock;
import com.amazonaws.services.s3.internal.Constants;
import com.navdy.ancs.AppleNotification;
import com.navdy.ancs.IAncsService;
import com.navdy.ancs.IAncsServiceListener;
import com.navdy.ancs.IAncsServiceListener.Stub;
import com.navdy.hud.app.common.ServiceReconnector;
import com.navdy.hud.app.event.DeviceInfoAvailable;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.InitEvents.InitPhase;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.event.Shutdown.State;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.glance.GlanceApp;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.profile.NotificationSettings;
import com.navdy.hud.app.util.NotificationActionCache;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.DeviceInfo.Platform;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.glances.GlanceEvent;
import com.navdy.service.library.events.notification.NotificationAction;
import com.navdy.service.library.events.notification.NotificationCategory;
import com.navdy.service.library.events.notification.NotificationEvent;
import com.navdy.service.library.events.notification.NotificationsError;
import com.navdy.service.library.events.notification.NotificationsState;
import com.navdy.service.library.events.notification.NotificationsStatusRequest;
import com.navdy.service.library.events.notification.NotificationsStatusUpdate.Builder;
import com.navdy.service.library.events.notification.ServiceType;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import com.navdy.service.library.events.ui.DismissScreen;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class AncsServiceConnector extends ServiceReconnector {
    private static final int NOTIFICATION_EVENT_SEND_THRESHOLD = 15000;
    private static final int ONE_HOUR_IN_MS = 3600000;
    private static HashMap<String, Boolean> blackList = new HashMap();
    private static HashMap<String, Long> enableNotificationTimeMap = new HashMap();
    static final Logger sLogger = new Logger(AncsServiceConnector.class);
    private NotificationCategory[] categoryMap = new NotificationCategory[]{NotificationCategory.CATEGORY_OTHER, NotificationCategory.CATEGORY_INCOMING_CALL, NotificationCategory.CATEGORY_MISSED_CALL, NotificationCategory.CATEGORY_VOICE_MAIL, NotificationCategory.CATEGORY_SOCIAL, NotificationCategory.CATEGORY_SCHEDULE, NotificationCategory.CATEGORY_EMAIL, NotificationCategory.CATEGORY_NEWS, NotificationCategory.CATEGORY_HEALTH_AND_FITNESS, NotificationCategory.CATEGORY_BUSINESS_AND_FINANCE, NotificationCategory.CATEGORY_LOCATION, NotificationCategory.CATEGORY_ENTERTAINMENT};
    private Runnable connectRunnable = new Runnable() {
        public void run() {
            AncsServiceConnector.sLogger.d("Establishing ANCS connection");
            if (AncsServiceConnector.this.mService != null && AncsServiceConnector.this.mDeviceInfo != null && AncsServiceConnector.this.localeUpToDate) {
                try {
                    AncsServiceConnector.this.mService.connectToDevice(new ParcelUuid(UUID.fromString(AncsServiceConnector.this.mDeviceInfo.deviceUuid)), new NavdyDeviceId(AncsServiceConnector.this.mDeviceInfo.deviceId).getBluetoothAddress());
                } catch (RemoteException e) {
                    AncsServiceConnector.sLogger.e("Failed to connect to navdy BTLE service", e);
                }
            }
        }
    };
    private Runnable disconnectRunnable = new Runnable() {
        public void run() {
            AncsServiceConnector.this.stop();
            AncsServiceConnector.this.restart();
        }
    };
    private IAncsServiceListener listener = new Stub() {
        public void onConnectionStateChange(int newState) throws RemoteException {
            AncsServiceConnector.sLogger.d("ANCS connection state change - " + newState);
            NotificationsState mappedState = null;
            NotificationsError error = null;
            switch (newState) {
                case 0:
                    mappedState = NotificationsState.NOTIFICATIONS_STOPPED;
                    break;
                case 1:
                    mappedState = NotificationsState.NOTIFICATIONS_CONNECTING;
                    break;
                case 2:
                    mappedState = NotificationsState.NOTIFICATIONS_ENABLED;
                    break;
                case 4:
                    error = NotificationsError.NOTIFICATIONS_ERROR_AUTH_FAILED;
                    mappedState = NotificationsState.NOTIFICATIONS_PAIRING_FAILED;
                    break;
                case 5:
                    error = NotificationsError.NOTIFICATIONS_ERROR_BOND_REMOVED;
                    mappedState = NotificationsState.NOTIFICATIONS_PAIRING_FAILED;
                    break;
            }
            if (mappedState != null) {
                AncsServiceConnector.this.setNotificationsState(mappedState, error);
            }
        }

        public void onNotification(AppleNotification notification) throws RemoteException {
            String appId = notification.getAppId();
            AncsServiceConnector.sLogger.d("Hud listener notified of apple notification id[" + appId);
            try {
                if (RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                    if (!(AncsServiceConnector.blackList.get(appId) != null) && notification.getEventId() == 0) {
                        Boolean enabled = AncsServiceConnector.this.mNotificationSettings.enabled(notification);
                        if (Boolean.TRUE.equals(enabled) && !notification.isPreExisting()) {
                            GlanceEvent event = AncsServiceConnector.this.convertToGlanceEvent(notification);
                            if (event == null) {
                                AncsServiceConnector.sLogger.e("glance not converted");
                                return;
                            }
                            AncsServiceConnector.sLogger.d("posting notification");
                            AncsServiceConnector.this.mBus.post(event);
                            return;
                        } else if (enabled == null) {
                            Long lastEventTime = (Long) AncsServiceConnector.enableNotificationTimeMap.get(appId);
                            long clockTime = SystemClock.elapsedRealtime();
                            if (lastEventTime == null || clockTime - lastEventTime.longValue() >= 15000) {
                                NotificationEvent event2 = AncsServiceConnector.this.convertNotification(notification);
                                AncsServiceConnector.sLogger.d("sending unknown notification to client - " + event2);
                                AncsServiceConnector.this.mBus.post(new RemoteEvent(event2));
                                AncsServiceConnector.enableNotificationTimeMap.put(appId, Long.valueOf(clockTime));
                                return;
                            }
                            AncsServiceConnector.sLogger.d("not sending unknown notification to client, within threshold");
                            return;
                        } else {
                            return;
                        }
                    }
                    return;
                }
                AncsServiceConnector.sLogger.d("not connected");
            } catch (Throwable t) {
                AncsServiceConnector.sLogger.e("ancs-notif", t);
            }
        }
    };
    private boolean localeUpToDate;
    protected NotificationActionCache mActionCache;
    protected Bus mBus;
    protected Context mContext;
    private DeviceInfo mDeviceInfo;
    protected NotificationSettings mNotificationSettings;
    protected IAncsService mService;
    private NotificationsState notificationsState = NotificationsState.NOTIFICATIONS_STOPPED;
    private boolean reconnect;

    static {
        blackList.put("com.apple.mobilephone", Boolean.valueOf(true));
    }

    public AncsServiceConnector(Context context, Bus bus) {
        super(context, getServiceIntent(), null);
        this.mContext = context;
        this.mBus = bus;
        this.mNotificationSettings = DriverProfileHelper.getInstance().getCurrentProfile().getNotificationSettings();
        this.mActionCache = new NotificationActionCache(getClass());
        bus.register(this);
    }

    private static Intent getServiceIntent() {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("com.navdy.ancs.app", "com.navdy.ancs.ANCSService"));
        return intent;
    }

    private void setNotificationsState(NotificationsState state, NotificationsError error) {
        if (this.notificationsState != state) {
            this.notificationsState = state;
            this.mBus.post(new RemoteEvent(new Builder().state(this.notificationsState).service(ServiceType.SERVICE_ANCS).errorDetails(error).build()));
        }
    }

    protected void onConnected(ComponentName name, IBinder service) {
        this.mService = IAncsService.Stub.asInterface(service);
        try {
            updateNotificationFilters();
            this.mService.addListener(this.listener);
        } catch (RemoteException e) {
            sLogger.e("Failed to add notification listener", e);
        }
        if (this.reconnect) {
            connect();
        }
    }

    protected void onDisconnected(ComponentName name) {
        this.mService = null;
    }

    public void performAction(int notificationUid, int actionId) {
        if (this.mService != null) {
            try {
                sLogger.d("Performing action (" + actionId + ") for notification - " + notificationUid);
                this.mService.performNotificationAction(notificationUid, actionId);
            } catch (RemoteException e) {
                sLogger.e("Exception when performing notification action", e);
            }
        }
    }

    private GlanceEvent convertToGlanceEvent(AppleNotification notification) {
        String appId = notification.getAppId();
        String id = String.valueOf(notification.getNotificationUid());
        String title = notification.getTitle();
        String subtitle = notification.getSubTitle();
        String message = notification.getMessage();
        String eventId = String.valueOf(notification.getEventId());
        Date time = notification.getDate();
        if (title == null && message == null && subtitle == null) {
            return null;
        }
        boolean whiteListed;
        GlanceApp app = GlanceHelper.getGlancesApp(appId);
        if (app == null) {
            app = GlanceApp.GENERIC;
            whiteListed = false;
        } else {
            whiteListed = true;
        }
        sLogger.v("[ancs-notif] appId[" + appId + "]" + " id[" + id + "]" + " title[" + title + "]" + " subTitle[" + subtitle + "]" + " message[" + message + "]" + " eventId[" + eventId + "]" + " time[" + time + "]" + " whitelisted[" + whiteListed + "]" + " app[" + app + "]");
        switch (app) {
            case GOOGLE_CALENDAR:
                return AncsGlanceHelper.buildGoogleCalendarEvent(appId, id, title, subtitle, message, time);
            case GOOGLE_MAIL:
                return AncsGlanceHelper.buildGoogleMailEvent(appId, id, title, subtitle, message, time);
            case GENERIC_MAIL:
                return AncsGlanceHelper.buildGenericMailEvent(appId, id, title, subtitle, message, time);
            case GOOGLE_HANGOUT:
                return AncsGlanceHelper.buildGoogleHangoutEvent(appId, id, title, subtitle, message, time);
            case SLACK:
                return AncsGlanceHelper.buildSlackEvent(appId, id, title, subtitle, message, time);
            case WHATS_APP:
                return AncsGlanceHelper.buildWhatsappEvent(appId, id, title, subtitle, message, time);
            case FACEBOOK_MESSENGER:
                return AncsGlanceHelper.buildFacebookMessengerEvent(appId, id, title, subtitle, message, time);
            case FACEBOOK:
                return AncsGlanceHelper.buildFacebookEvent(appId, id, title, subtitle, message, time);
            case TWITTER:
                return AncsGlanceHelper.buildTwitterEvent(appId, id, title, subtitle, message, time);
            case IMESSAGE:
                return AncsGlanceHelper.buildiMessageEvent(appId, id, title, subtitle, message, time);
            case APPLE_MAIL:
                return AncsGlanceHelper.buildAppleMailEvent(appId, id, title, subtitle, message, time);
            case APPLE_CALENDAR:
                return AncsGlanceHelper.buildAppleCalendarEvent(appId, id, title, subtitle, message, time);
            case GENERIC:
                return AncsGlanceHelper.buildGenericEvent(appId, id, title, subtitle, message, time);
            default:
                sLogger.w("[ancs-notif] app of type [" + app + "] not handled");
                return null;
        }
    }

    private NotificationEvent convertNotification(AppleNotification notification) {
        String title = notification.getTitle();
        return new NotificationEvent(Integer.valueOf(notification.getNotificationUid()), mapCategory(notification.getCategoryId()), title, notification.getSubTitle(), notification.getMessage(), notification.getAppId(), buildActions(notification), null, null, null, title, Boolean.valueOf(true), notification.getAppName());
    }

    private List<NotificationAction> buildActions(AppleNotification notification) {
        if (notification.getAppId().equals(NotificationSettings.APP_ID_SMS)) {
            return null;
        }
        return getDefaultActions(notification);
    }

    private List<NotificationAction> getDefaultActions(AppleNotification notification) {
        List<NotificationAction> actions = new ArrayList();
        int notificationId = notification.getNotificationUid();
        if (notification.hasPositiveAction()) {
            actions.add(this.mActionCache.buildAction(notificationId, 0, 0, notification.getPositiveActionLabel()));
        }
        if (notification.hasNegativeAction()) {
            actions.add(this.mActionCache.buildAction(notificationId, 1, 0, notification.getNegativeActionLabel()));
        }
        return actions;
    }

    private NotificationCategory mapCategory(int categoryId) {
        if (categoryId < 0 || categoryId >= this.categoryMap.length) {
            return NotificationCategory.CATEGORY_OTHER;
        }
        return this.categoryMap[categoryId];
    }

    @Subscribe
    public void onNotificationsRequest(NotificationsStatusRequest request) {
        if (request.service == null || ServiceType.SERVICE_ANCS.equals(request.service)) {
            sLogger.i("Received NotificationsStatusRequest:" + request);
            if (request.newState != null) {
                switch (request.newState) {
                    case NOTIFICATIONS_ENABLED:
                        connect();
                        break;
                    case NOTIFICATIONS_STOPPED:
                        disconnect();
                        break;
                }
            }
            this.mBus.post(new RemoteEvent(new Builder().state(this.notificationsState).service(ServiceType.SERVICE_ANCS).build()));
        }
    }

    private void updateNotificationFilters() {
        this.mNotificationSettings = DriverProfileHelper.getInstance().getCurrentProfile().getNotificationSettings();
        if (this.mService != null) {
            try {
                long oneHourAgo = System.currentTimeMillis() - 3600000;
                List<String> enabledApps = this.mNotificationSettings != null ? this.mNotificationSettings.enabledApps() : null;
                sLogger.d("Setting ANCS to filter apps: " + (enabledApps != null ? Arrays.toString(enabledApps.toArray()) : Constants.NULL_VERSION_ID));
                this.mService.setNotificationFilter(enabledApps, oneHourAgo);
            } catch (RemoteException e) {
                sLogger.w("Failed to update notification filter");
            }
        }
    }

    @Subscribe
    public void onDriverProfileChanged(DriverProfileChanged event) {
        updateNotificationFilters();
    }

    @Subscribe
    public void onNotificationPreferences(NotificationPreferences preferences) {
        updateNotificationFilters();
    }

    private boolean isIOS(DeviceInfo deviceInfo) {
        return deviceInfo != null && ((deviceInfo.platform != null && deviceInfo.platform == Platform.PLATFORM_iOS) || deviceInfo.model.contains("iPhone"));
    }

    @Subscribe
    public void onNotificationAction(NotificationAction action) {
        if (this.mActionCache.validAction(action)) {
            performAction(action.notificationId.intValue(), action.actionId.intValue());
            this.mBus.post(new DismissScreen(Screen.SCREEN_NOTIFICATION));
            this.mActionCache.markComplete(action);
        }
    }

    @Subscribe
    public void onConnectionChange(ConnectionStateChange change) {
        switch (change.state) {
            case CONNECTION_DISCONNECTED:
                this.mDeviceInfo = null;
                this.localeUpToDate = false;
                disconnect();
                return;
            default:
                return;
        }
    }

    @Subscribe
    public void onDeviceInfoAvailable(DeviceInfoAvailable event) {
        sLogger.i("device info updated");
        this.mDeviceInfo = event.deviceInfo;
    }

    @Subscribe
    public void onInitPhase(InitPhase event) {
        switch (event.phase) {
            case LOCALE_UP_TO_DATE:
                this.localeUpToDate = true;
                if (this.reconnect) {
                    connect();
                    return;
                }
                return;
            case SWITCHING_LOCALE:
                shutdown();
                return;
            default:
                return;
        }
    }

    public void shutdown() {
        this.reconnect = false;
        stop();
        super.shutdown();
    }

    @Subscribe
    public void onShutdown(Shutdown event) {
        if (event.state == State.CONFIRMED) {
            shutdown();
        }
    }

    private void stop() {
        sLogger.d("Disconnecting ANCS connection");
        if (this.mService != null) {
            try {
                this.mService.disconnect();
            } catch (RemoteException e) {
                sLogger.e("Failed to disconnect ANCS", e);
            }
        }
    }

    private void connect() {
        this.reconnect = true;
        if (isIOS(this.mDeviceInfo)) {
            TaskManager.getInstance().execute(this.connectRunnable, 1);
        }
    }

    private void disconnect() {
        this.reconnect = false;
        TaskManager.getInstance().execute(this.disconnectRunnable, 1);
    }
}
