package com.navdy.hud.app.config;

import android.text.TextUtils;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.os.SystemProperties;

public class BooleanSetting extends Setting {
    private boolean changed;
    private boolean defaultValue;
    private boolean enabled;
    private Scope scope;

    public enum Scope {
        NEVER,
        ENG,
        BETA,
        ALWAYS,
        CUSTOM
    }

    public BooleanSetting(String name, Scope scope, String path, String description) {
        this(name, scope, false, path, description);
    }

    public BooleanSetting(String name, Scope scope, boolean defaultValue, String path, String description) {
        super(name, path, description);
        this.scope = scope;
        this.defaultValue = defaultValue;
        load();
    }

    public boolean isEnabled() {
        boolean z = true;
        switch (this.scope) {
            case CUSTOM:
                return this.enabled;
            case ENG:
                if (DeviceUtil.isUserBuild()) {
                    z = false;
                }
                return z;
            case ALWAYS:
                return true;
            default:
                return false;
        }
    }

    public void setEnabled(boolean enabled) {
        if (this.scope != Scope.CUSTOM) {
            this.scope = Scope.CUSTOM;
        }
        if (this.enabled != enabled) {
            this.enabled = enabled;
            this.changed = true;
            changed();
            save();
        }
    }

    public void save() {
        if (this.changed) {
            this.changed = false;
            String propName = getProperty();
            if (propName != null) {
                SystemProperties.set(propName, Boolean.toString(this.enabled));
            }
        }
    }

    public void load() {
        String propName = getProperty();
        if (propName != null) {
            String setting = SystemProperties.get(propName);
            if (TextUtils.isEmpty(setting)) {
                this.enabled = this.defaultValue;
                return;
            }
            this.scope = Scope.CUSTOM;
            boolean z = Boolean.parseBoolean(setting) || setting.equalsIgnoreCase(ToastPresenter.EXTRA_MAIN_TITLE) || setting.equalsIgnoreCase("yes");
            this.enabled = z;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("BooleanSetting{");
        sb.append("name=").append(getName());
        sb.append(HereManeuverDisplayBuilder.COMMA);
        sb.append("enabled=").append(this.enabled);
        sb.append('}');
        return sb.toString();
    }
}
