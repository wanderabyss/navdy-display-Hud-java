package com.navdy.hud.app.maps.notification;

import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent;
import com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Severity;
import com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Type;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout2.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Selection;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.squareup.otto.Bus;
import java.util.ArrayList;
import java.util.List;

public class TrafficEventNotification extends BaseTrafficNotification {
    private static final int TAG_DISMISS = 1;
    private static String ahead;
    private static String dismiss;
    private static List<Choice> dismissChoices = new ArrayList(1);
    private static String enableInternet;
    private static String noUpdatedTraffic;
    private static String slowTraffic;
    private static String stoppedTraffic;
    private static String trafficIncident;
    private static String unknownIncident;
    private Bus bus;
    private IListener choiceListener = new IListener() {
        public void executeItem(Selection selection) {
            TrafficEventNotification.this.dismissNotification();
            TrafficEventNotification.this.trafficEvent = null;
        }

        public void itemSelected(Selection selection) {
        }
    };
    private ViewGroup container;
    private ImageView image;
    private int notifColor;
    private TextView subTitle;
    private TextView title;
    private LiveTrafficEvent trafficEvent;

    public TrafficEventNotification(Bus bus) {
        if (stoppedTraffic == null) {
            Resources resources = HudApplication.getAppContext().getResources();
            stoppedTraffic = resources.getString(R.string.traffic_notification_text_stoppedtraffic);
            slowTraffic = resources.getString(R.string.traffic_notification_text_slowtraffic);
            trafficIncident = resources.getString(R.string.traffic_notification_text_incident);
            noUpdatedTraffic = resources.getString(R.string.traffic_no_updated);
            enableInternet = resources.getString(R.string.enable_internet);
            ahead = resources.getString(R.string.traffic_notification_ahead);
            dismiss = resources.getString(R.string.traffic_notification_dismiss);
            unknownIncident = resources.getString(R.string.traffic_notification_text_default);
            int dismissColor = resources.getColor(R.color.glance_dismiss);
            dismissChoices.add(new Choice(1, R.drawable.icon_glances_dismiss, dismissColor, R.drawable.icon_glances_dismiss, -16777216, dismiss, dismissColor));
            this.notifColor = resources.getColor(R.color.traffic_bad);
        }
        this.bus = bus;
    }

    public NotificationType getType() {
        return NotificationType.TRAFFIC_EVENT;
    }

    public String getId() {
        return NotificationId.TRAFFIC_EVENT_NOTIFICATION_ID;
    }

    public View getView(Context context) {
        if (this.container == null) {
            this.container = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.notification_traffic_event, null);
            this.title = (TextView) this.container.findViewById(R.id.title);
            this.subTitle = (TextView) this.container.findViewById(R.id.subTitle);
            this.image = (ImageView) this.container.findViewById(R.id.image);
            this.choiceLayout = (ChoiceLayout2) this.container.findViewById(R.id.choiceLayout);
        }
        return this.container;
    }

    public View getExpandedView(Context context, Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(INotificationController controller) {
        super.onStart(controller);
        updateState();
    }

    public void onUpdate() {
        updateState();
    }

    public void onStop() {
        super.onStop();
    }

    public int getTimeout() {
        return 0;
    }

    public boolean isAlive() {
        return true;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        return this.notifColor;
    }

    public void onNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    private void updateState() {
        if (this.trafficEvent != null) {
            this.title.setText(getLiveTrafficEventText(this.trafficEvent));
            this.subTitle.setText(ahead);
            this.image.setImageResource(getLiveTrafficEventImage(this.trafficEvent));
            this.choiceLayout.setChoices(dismissChoices, 0, this.choiceListener, 0.5f);
        }
    }

    private int getLiveTrafficEventImage(LiveTrafficEvent trafficEvent) {
        if (trafficEvent.type == Type.CONGESTION && trafficEvent.severity.value > Severity.VERY_HIGH.value) {
            return R.drawable.icon_mm_stopped_traffic;
        }
        if (trafficEvent.type == Type.INCIDENT) {
            return R.drawable.icon_mm_incident;
        }
        return R.drawable.icon_mm_slow_traffic;
    }

    private String getLiveTrafficEventText(LiveTrafficEvent trafficEvent) {
        if (trafficEvent.type == Type.CONGESTION) {
            if (trafficEvent.severity.value > Severity.HIGH.value) {
                return stoppedTraffic;
            }
            return slowTraffic;
        } else if (trafficEvent.type == Type.INCIDENT) {
            return trafficIncident;
        } else {
            return unknownIncident;
        }
    }

    public void setTrafficEvent(LiveTrafficEvent event) {
        this.trafficEvent = event;
    }
}
