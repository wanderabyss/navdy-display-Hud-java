package com.navdy.hud.app.maps.here;

import com.here.android.mpa.common.RoadElement;
import com.here.android.mpa.guidance.LaneInformation;
import com.here.android.mpa.guidance.LaneInformation.Direction;
import com.here.android.mpa.guidance.LaneInformation.RecommendationState;
import com.here.android.mpa.guidance.NavigationManager.LaneInformationListener;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Maneuver.Turn;
import com.navdy.hud.app.maps.MapEvents.DisplayTrafficLaneInfo;
import com.navdy.hud.app.maps.MapEvents.HideTrafficLaneInfo;
import com.navdy.hud.app.maps.MapEvents.LaneData;
import com.navdy.hud.app.maps.MapEvents.LaneData.Position;
import com.navdy.hud.app.maps.here.HereLaneInfoBuilder.DirectionType;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;

public class HereLaneInfoListener extends LaneInformationListener {
    private static final LaneData EMPTY_LANE_DATA = new LaneData(Position.OFF_ROUTE, null);
    private static final HideTrafficLaneInfo HIDE_LANE_INFO = new HideTrafficLaneInfo();
    private static Logger sLogger = new Logger(HereLaneInfoListener.class);
    private Bus bus;
    private DisplayTrafficLaneInfo lastLanesData;
    private HereNavController navController;
    private volatile boolean running;
    private Runnable startRunnable = new Runnable() {
        public void run() {
            HereLaneInfoListener.this.navController.addLaneInfoListener(new WeakReference(HereLaneInfoListener.this));
            HereLaneInfoListener.sLogger.v("added lane info listener");
        }
    };
    private Runnable stopRunnable = new Runnable() {
        public void run() {
            HereLaneInfoListener.this.navController.removeLaneInfoListener(HereLaneInfoListener.this);
            HereLaneInfoListener.sLogger.v("removed lane info listener");
        }
    };
    private TaskManager taskManager = TaskManager.getInstance();
    private ArrayList<Direction> tempDirections = new ArrayList();
    private ArrayList<Direction> tempDirections2 = new ArrayList();
    private ArrayList<LaneData> tempLanes = new ArrayList();

    /* renamed from: com.navdy.hud.app.maps.here.HereLaneInfoListener$4 */
    static /* synthetic */ class AnonymousClass4 {
        static final /* synthetic */ int[] $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction = new int[Direction.values().length];
        static final /* synthetic */ int[] $SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState = new int[RecommendationState.values().length];

        static {
            $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn = new int[Turn.values().length];
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[Turn.KEEP_RIGHT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[Turn.LIGHT_RIGHT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[Turn.QUITE_RIGHT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[Turn.HEAVY_RIGHT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[Turn.KEEP_LEFT.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[Turn.LIGHT_LEFT.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[Turn.QUITE_LEFT.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[Turn.HEAVY_LEFT.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[Direction.UNDEFINED.ordinal()] = 1;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[RecommendationState.NOT_RECOMMENDED.ordinal()] = 1;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[RecommendationState.HIGHLY_RECOMMENDED.ordinal()] = 2;
            } catch (NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[RecommendationState.RECOMMENDED.ordinal()] = 3;
            } catch (NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[RecommendationState.NOT_AVAILABLE.ordinal()] = 4;
            } catch (NoSuchFieldError e13) {
            }
        }
    }

    HereLaneInfoListener(Bus bus, HereNavController navController) {
        sLogger.v("ctor");
        this.bus = bus;
        this.navController = navController;
    }

    public void onLaneInformation(final List<LaneInformation> laneInfoList, RoadElement roadElement) {
        if (this.running) {
            this.taskManager.execute(new Runnable() {
                public void run() {
                    if (laneInfoList != null && laneInfoList.size() != 0) {
                        try {
                            Iterator it;
                            LaneInformation laneInfo;
                            EnumSet<Direction> directions;
                            int i;
                            int numLanes = laneInfoList.size();
                            if (HereLaneInfoListener.sLogger.isLoggable(2)) {
                                HereLaneInfoListener.sLogger.v("onShowLaneInfo: size=" + numLanes);
                                for (LaneInformation laneInfo2 : laneInfoList) {
                                    HereLaneInfoListener.sLogger.v("onShowLaneInfo:recommended state:" + laneInfo2.getRecommendationState().name());
                                    directions = laneInfo2.getDirections();
                                    if (directions != null) {
                                        Iterator it2 = directions.iterator();
                                        while (it2.hasNext()) {
                                            HereLaneInfoListener.sLogger.v("onShowLaneInfo:direction:" + ((Direction) it2.next()).name());
                                        }
                                    }
                                }
                            }
                            int hasHighlyRecommendedLanes = 0;
                            int hasRecommendedLanes = 0;
                            int hasNonRecommendedLanes = 0;
                            for (i = 0; i < numLanes; i++) {
                                switch (AnonymousClass4.$SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[((LaneInformation) laneInfoList.get(i)).getRecommendationState().ordinal()]) {
                                    case 1:
                                        hasNonRecommendedLanes++;
                                        break;
                                    case 2:
                                        hasHighlyRecommendedLanes++;
                                        break;
                                    case 3:
                                        hasRecommendedLanes++;
                                        break;
                                    default:
                                        break;
                                }
                            }
                            if (hasHighlyRecommendedLanes == 0 && hasRecommendedLanes == 0) {
                                if (HereLaneInfoListener.this.lastLanesData != null) {
                                    HereLaneInfoListener.this.lastLanesData = null;
                                    HereLaneInfoListener.this.bus.post(HereLaneInfoListener.HIDE_LANE_INFO);
                                }
                                if (HereLaneInfoListener.sLogger.isLoggable(2)) {
                                    HereLaneInfoListener.sLogger.v("Invalid lane data");
                                    return;
                                }
                                return;
                            }
                            Direction onRouteDirection = null;
                            boolean needOnRouteDirection = false;
                            EnumSet<Direction> needOnRouteDirectionList = null;
                            Direction onRouteHighlyRecommendedDirection = null;
                            EnumSet<Direction> needOnRouteHighlyRecommendedDirectionList = null;
                            Direction onRouteRecommendedDirection = null;
                            EnumSet<Direction> needOnRouteRecommendedDirectionList = null;
                            for (i = 0; i < numLanes; i++) {
                                laneInfo2 = (LaneInformation) laneInfoList.get(i);
                                switch (AnonymousClass4.$SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[laneInfo2.getRecommendationState().ordinal()]) {
                                    case 2:
                                        if (onRouteHighlyRecommendedDirection != null) {
                                            break;
                                        }
                                        directions = laneInfo2.getDirections();
                                        if (directions != null && directions.size() > 0) {
                                            if (directions.size() != 1) {
                                                needOnRouteDirection = true;
                                                needOnRouteHighlyRecommendedDirectionList = directions;
                                                break;
                                            }
                                            onRouteHighlyRecommendedDirection = (Direction) directions.iterator().next();
                                            if (!HereLaneInfoListener.sLogger.isLoggable(2)) {
                                                break;
                                            }
                                            HereLaneInfoListener.sLogger.v("onRouteDirection:" + null);
                                            break;
                                        }
                                    case 3:
                                        if (onRouteRecommendedDirection != null) {
                                            break;
                                        }
                                        directions = laneInfo2.getDirections();
                                        if (directions != null && directions.size() > 0) {
                                            if (directions.size() != 1) {
                                                needOnRouteDirection = true;
                                                needOnRouteRecommendedDirectionList = directions;
                                                break;
                                            }
                                            onRouteRecommendedDirection = (Direction) directions.iterator().next();
                                            if (!HereLaneInfoListener.sLogger.isLoggable(2)) {
                                                break;
                                            }
                                            HereLaneInfoListener.sLogger.v("onRouteDirection:" + null);
                                            break;
                                        }
                                    default:
                                        break;
                                }
                            }
                            if (hasHighlyRecommendedLanes > 0) {
                                if (onRouteHighlyRecommendedDirection != null) {
                                    onRouteDirection = onRouteHighlyRecommendedDirection;
                                } else if (onRouteRecommendedDirection == null || !needOnRouteHighlyRecommendedDirectionList.contains(onRouteRecommendedDirection)) {
                                    needOnRouteDirectionList = needOnRouteHighlyRecommendedDirectionList;
                                } else {
                                    onRouteDirection = onRouteRecommendedDirection;
                                }
                            } else if (onRouteRecommendedDirection != null) {
                                onRouteDirection = onRouteRecommendedDirection;
                            } else {
                                needOnRouteDirectionList = needOnRouteRecommendedDirectionList;
                            }
                            if (onRouteDirection == null && needOnRouteDirection) {
                                if (HereLaneInfoListener.sLogger.isLoggable(2)) {
                                    HereLaneInfoListener.sLogger.v("onRouteDirection is needed, but not available");
                                }
                                onRouteDirection = HereLaneInfoListener.this.getOnRouteDirectionFromManeuver(needOnRouteDirectionList);
                                if (onRouteDirection == null) {
                                    if (HereLaneInfoListener.sLogger.isLoggable(2)) {
                                        HereLaneInfoListener.sLogger.v("onRouteDirection:could not get onroute direction from maneuver");
                                    }
                                    if (HereLaneInfoListener.this.lastLanesData != null) {
                                        HereLaneInfoListener.this.lastLanesData = null;
                                        HereLaneInfoListener.this.bus.post(HereLaneInfoListener.HIDE_LANE_INFO);
                                        return;
                                    }
                                    return;
                                } else if (HereLaneInfoListener.sLogger.isLoggable(2)) {
                                    HereLaneInfoListener.sLogger.v("onRouteDirection:got on route direction:" + onRouteDirection);
                                }
                            }
                            HereLaneInfoListener.this.tempLanes.clear();
                            for (i = 0; i < numLanes; i++) {
                                laneInfo2 = (LaneInformation) laneInfoList.get(i);
                                directions = laneInfo2.getDirections();
                                if (directions != null && directions.size() != 0) {
                                    HereLaneInfoListener.this.tempDirections.clear();
                                    it = directions.iterator();
                                    while (it.hasNext()) {
                                        Direction direction = (Direction) it.next();
                                        switch (AnonymousClass4.$SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[direction.ordinal()]) {
                                            case 1:
                                                break;
                                            default:
                                                HereLaneInfoListener.this.tempDirections.add(direction);
                                                break;
                                        }
                                    }
                                    if (HereLaneInfoListener.this.tempDirections.size() != 0) {
                                        Iterator<Direction> iterator;
                                        Direction d;
                                        switch (AnonymousClass4.$SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[laneInfo2.getRecommendationState().ordinal()]) {
                                            case 1:
                                                HereLaneInfoListener.this.tempLanes.add(new LaneData(Position.OFF_ROUTE, HereLaneInfoBuilder.getDrawable(HereLaneInfoListener.this.tempDirections, Position.OFF_ROUTE, null)));
                                                break;
                                            case 2:
                                                if (onRouteDirection != null && HereLaneInfoListener.this.tempDirections.size() > 1) {
                                                    HereLaneInfoListener.this.tempDirections2.clear();
                                                    iterator = HereLaneInfoListener.this.tempDirections.iterator();
                                                    while (iterator.hasNext()) {
                                                        d = (Direction) iterator.next();
                                                        if (d == onRouteDirection) {
                                                            HereLaneInfoListener.this.tempDirections2.add(d);
                                                            iterator.remove();
                                                        }
                                                    }
                                                    HereLaneInfoListener.this.tempDirections.addAll(HereLaneInfoListener.this.tempDirections2);
                                                }
                                                HereLaneInfoListener.this.tempLanes.add(new LaneData(Position.ON_ROUTE, HereLaneInfoBuilder.getDrawable(HereLaneInfoListener.this.tempDirections, Position.ON_ROUTE, onRouteDirection)));
                                                break;
                                            case 3:
                                                if (onRouteDirection != null && HereLaneInfoListener.this.tempDirections.size() > 1) {
                                                    HereLaneInfoListener.this.tempDirections2.clear();
                                                    iterator = HereLaneInfoListener.this.tempDirections.iterator();
                                                    while (iterator.hasNext()) {
                                                        d = (Direction) iterator.next();
                                                        if (d == onRouteDirection) {
                                                            HereLaneInfoListener.this.tempDirections2.add(d);
                                                            iterator.remove();
                                                        }
                                                    }
                                                    HereLaneInfoListener.this.tempDirections.addAll(HereLaneInfoListener.this.tempDirections2);
                                                }
                                                if (hasHighlyRecommendedLanes != 0) {
                                                    HereLaneInfoListener.this.tempLanes.add(new LaneData(Position.OFF_ROUTE, HereLaneInfoBuilder.getDrawable(HereLaneInfoListener.this.tempDirections, Position.OFF_ROUTE, null)));
                                                    break;
                                                }
                                                HereLaneInfoListener.this.tempLanes.add(new LaneData(Position.ON_ROUTE, HereLaneInfoBuilder.getDrawable(HereLaneInfoListener.this.tempDirections, Position.ON_ROUTE, onRouteDirection)));
                                                break;
                                            case 4:
                                                HereLaneInfoListener.this.tempLanes.add(new LaneData(Position.OFF_ROUTE, HereLaneInfoBuilder.getDrawable(HereLaneInfoListener.this.tempDirections, Position.OFF_ROUTE, null)));
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                    HereLaneInfoListener.this.tempLanes.add(HereLaneInfoListener.EMPTY_LANE_DATA);
                                } else {
                                    HereLaneInfoListener.this.tempLanes.add(HereLaneInfoListener.EMPTY_LANE_DATA);
                                }
                            }
                            if (HereLaneInfoListener.sLogger.isLoggable(2)) {
                                HereLaneInfoListener.sLogger.v("LaneData: size=" + HereLaneInfoListener.this.tempLanes.size());
                                it = HereLaneInfoListener.this.tempLanes.iterator();
                                while (it.hasNext()) {
                                    LaneData laneData = (LaneData) it.next();
                                    HereLaneInfoListener.sLogger.v("LaneData:position: " + laneData.position);
                                    if (laneData.icons != null) {
                                        for (i = 0; i < laneData.icons.length; i++) {
                                            HereLaneInfoListener.sLogger.v("LaneData:icons" + (i + 1) + " = " + laneData.icons[i]);
                                        }
                                    } else {
                                        HereLaneInfoListener.sLogger.v("LaneData:icons: null");
                                    }
                                }
                            }
                            if (HereLaneInfoListener.this.lastLanesData == null || !HereLaneInfoBuilder.compareLaneData(HereLaneInfoListener.this.lastLanesData.laneData, HereLaneInfoListener.this.tempLanes)) {
                                ArrayList<LaneData> lanes = new ArrayList(HereLaneInfoListener.this.tempLanes);
                                HereLaneInfoListener.this.lastLanesData = new DisplayTrafficLaneInfo(lanes);
                                HereLaneInfoListener.this.tempLanes.clear();
                                HereLaneInfoListener.sLogger.v("onShowLaneInfo::" + lanes.size());
                                if (HereLaneInfoListener.this.running) {
                                    HereLaneInfoListener.this.bus.post(HereLaneInfoListener.this.lastLanesData);
                                    return;
                                }
                                return;
                            }
                            if (HereLaneInfoListener.sLogger.isLoggable(2)) {
                                HereLaneInfoListener.sLogger.v("LaneData: lanedata is same as last");
                            }
                            HereLaneInfoListener.this.tempLanes.clear();
                        } catch (Throwable t) {
                            HereLaneInfoListener.sLogger.e(t);
                        }
                    } else if (HereLaneInfoListener.this.lastLanesData != null) {
                        HereLaneInfoListener.this.lastLanesData = null;
                        HereLaneInfoListener.this.hideLaneInfo();
                    }
                }
            }, 15);
        }
    }

    public void hideLaneInfo() {
        sLogger.v("hideLaneInfo::");
        this.bus.post(HIDE_LANE_INFO);
    }

    synchronized void start() {
        if (this.running) {
            sLogger.v("already running");
        } else {
            sLogger.v("start");
            this.taskManager.execute(this.startRunnable, 15);
            this.running = true;
        }
    }

    synchronized void stop() {
        if (this.running) {
            sLogger.v("stop");
            this.taskManager.execute(this.stopRunnable, 15);
            this.running = false;
            this.bus.post(HIDE_LANE_INFO);
            this.lastLanesData = null;
        } else {
            sLogger.v("not running");
        }
    }

    private Direction getOnRouteDirectionFromManeuver(EnumSet<Direction> directions) {
        try {
            Maneuver maneuver = HereNavigationManager.getInstance().getNextManeuver();
            if (maneuver == null) {
                return null;
            }
            Turn turn = maneuver.getTurn();
            if (turn == null) {
                return null;
            }
            switch (turn) {
                case KEEP_RIGHT:
                    if (directions.contains(Direction.RIGHT)) {
                        return Direction.RIGHT;
                    }
                    if (HereLaneInfoBuilder.getNumDirections(directions, DirectionType.RIGHT) == 1) {
                        return HereLaneInfoBuilder.getDirection(directions, DirectionType.RIGHT);
                    }
                    return null;
                case LIGHT_RIGHT:
                    if (directions.contains(Direction.SLIGHTLY_RIGHT)) {
                        return Direction.SLIGHTLY_RIGHT;
                    }
                    if (HereLaneInfoBuilder.getNumDirections(directions, DirectionType.RIGHT) == 1) {
                        return HereLaneInfoBuilder.getDirection(directions, DirectionType.RIGHT);
                    }
                    return null;
                case QUITE_RIGHT:
                case HEAVY_RIGHT:
                    if (directions.contains(Direction.SHARP_RIGHT)) {
                        return Direction.SHARP_RIGHT;
                    }
                    if (HereLaneInfoBuilder.getNumDirections(directions, DirectionType.RIGHT) == 1) {
                        return HereLaneInfoBuilder.getDirection(directions, DirectionType.RIGHT);
                    }
                    return null;
                case KEEP_LEFT:
                    if (directions.contains(Direction.LEFT)) {
                        return Direction.LEFT;
                    }
                    if (HereLaneInfoBuilder.getNumDirections(directions, DirectionType.LEFT) == 1) {
                        return HereLaneInfoBuilder.getDirection(directions, DirectionType.LEFT);
                    }
                    return null;
                case LIGHT_LEFT:
                    if (directions.contains(Direction.SLIGHTLY_LEFT)) {
                        return Direction.SLIGHTLY_LEFT;
                    }
                    if (HereLaneInfoBuilder.getNumDirections(directions, DirectionType.LEFT) == 1) {
                        return HereLaneInfoBuilder.getDirection(directions, DirectionType.LEFT);
                    }
                    return null;
                case QUITE_LEFT:
                case HEAVY_LEFT:
                    if (directions.contains(Direction.SHARP_LEFT)) {
                        return Direction.SHARP_LEFT;
                    }
                    if (HereLaneInfoBuilder.getNumDirections(directions, DirectionType.LEFT) == 1) {
                        return HereLaneInfoBuilder.getDirection(directions, DirectionType.LEFT);
                    }
                    return null;
                default:
                    return null;
            }
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }
}
