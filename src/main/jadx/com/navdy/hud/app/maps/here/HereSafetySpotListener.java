package com.navdy.hud.app.maps.here;

import android.os.Handler;
import android.os.Looper;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.guidance.NavigationManager.SafetySpotListener;
import com.here.android.mpa.guidance.SafetySpotNotification;
import com.here.android.mpa.guidance.SafetySpotNotificationInfo;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapObject;
import com.here.android.mpa.mapping.SafetySpotInfo;
import com.here.android.mpa.mapping.SafetySpotInfo.Type;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.util.DistanceConverter;
import com.navdy.hud.app.maps.util.DistanceConverter.Distance;
import com.navdy.service.library.events.audio.SpeechRequest.Category;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class HereSafetySpotListener extends SafetySpotListener {
    private static final int CHECK_INTERVAL = ((int) TimeUnit.SECONDS.toMillis(30));
    private static final int SAFETY_SPOT_HIDE_DISTANCE = 1000;
    private static final Logger sLogger = new Logger("SafetySpotListener");
    private final Bus bus;
    private boolean canDrawOnTheMap = true;
    private final Runnable checkExpiredSpots = new Runnable() {
        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            try {
                List<SafetySpotContainer> invalid = HereSafetySpotListener.this.removeExpiredSpots();
                if (HereSafetySpotListener.this.canDrawOnTheMap) {
                    HereSafetySpotListener.this.removeMapMarkers(invalid);
                }
                HereSafetySpotListener.this.handler.postDelayed(this, (long) HereSafetySpotListener.CHECK_INTERVAL);
            } catch (Throwable th) {
                HereSafetySpotListener.this.handler.postDelayed(this, (long) HereSafetySpotListener.CHECK_INTERVAL);
            }
        }
    };
    private final Handler handler = new Handler(Looper.getMainLooper());
    private final HereMapController mapController;
    private List<SafetySpotContainer> safetySpotNotifications = new ArrayList();

    private static class SafetySpotContainer {
        private MapMarker mapMarker;
        private SafetySpotNotificationInfo safetySpotNotificationInfo;

        private SafetySpotContainer() {
        }

        /* synthetic */ SafetySpotContainer(AnonymousClass1 x0) {
            this();
        }
    }

    HereSafetySpotListener(Bus bus, HereMapController mapController) {
        sLogger.v("ctor");
        this.bus = bus;
        this.mapController = mapController;
    }

    private List<SafetySpotContainer> removeExpiredSpots() {
        List<SafetySpotContainer> invalid = null;
        GeoCoordinate currentLocation = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
        if (currentLocation == null) {
            sLogger.e("check:no current location");
        } else if (this.safetySpotNotifications.size() != 0) {
            List<SafetySpotContainer> valid = new ArrayList();
            invalid = new ArrayList();
            for (SafetySpotContainer info : this.safetySpotNotifications) {
                int id = System.identityHashCode(info.safetySpotNotificationInfo);
                if (isSafetySpotRelevant(info.safetySpotNotificationInfo, currentLocation)) {
                    invalid.add(info);
                    sLogger.i("check remove:" + id);
                } else {
                    valid.add(info);
                    sLogger.i("check valid:" + id);
                }
            }
            this.safetySpotNotifications.clear();
            this.safetySpotNotifications.addAll(valid);
        }
        return invalid;
    }

    private boolean isSafetySpotRelevant(SafetySpotNotificationInfo safetySpotNotificationInfo, GeoCoordinate currentLocation) {
        int distanceCalc = (int) currentLocation.distanceTo(safetySpotNotificationInfo.getSafetySpot().getCoordinate());
        sLogger.i("check safety spot id[" + System.identityHashCode(safetySpotNotificationInfo) + "] type[" + safetySpotNotificationInfo.getSafetySpot().getType() + "] distance calc[" + distanceCalc + "] distance[" + safetySpotNotificationInfo.getDistance() + "]");
        return distanceCalc >= 1000;
    }

    public void onSafetySpot(final SafetySpotNotification safetySpotNotification) {
        this.handler.post(new Runnable() {
            public void run() {
                List<SafetySpotNotificationInfo> safetySpotNotificationList = safetySpotNotification.getSafetySpotNotificationInfos();
                if (safetySpotNotificationList == null || safetySpotNotificationList.size() == 0) {
                    HereSafetySpotListener.sLogger.i("no safety spots");
                    return;
                }
                GeoCoordinate currentLocation = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                if (currentLocation == null) {
                    HereSafetySpotListener.sLogger.e("no current location");
                    return;
                }
                List<SafetySpotNotificationInfo> tmpList = new ArrayList();
                for (SafetySpotNotificationInfo info : safetySpotNotificationList) {
                    SafetySpotInfo safetySpotInfo = info.getSafetySpot();
                    if (!(safetySpotInfo == null || safetySpotInfo.getType() == Type.UNDEFINED)) {
                        tmpList.add(info);
                    }
                }
                if (tmpList.size() == 0) {
                    HereSafetySpotListener.sLogger.i("no valid safety spots");
                    return;
                }
                HereSafetySpotListener.this.handler.removeCallbacks(HereSafetySpotListener.this.checkExpiredSpots);
                HereSafetySpotListener.this.addSafetySpotMarkers(tmpList, currentLocation);
                HereSafetySpotListener.this.handler.postDelayed(HereSafetySpotListener.this.checkExpiredSpots, (long) HereSafetySpotListener.CHECK_INTERVAL);
            }
        });
    }

    private void addSafetySpotMarkers(List<SafetySpotNotificationInfo> list, GeoCoordinate currentLocation) {
        Type nearestType = null;
        int nearestDistance = 0;
        List<SafetySpotContainer> addList = new ArrayList();
        List<MapObject> mapMarkers = new ArrayList();
        for (SafetySpotNotificationInfo info : list) {
            try {
                SafetySpotContainer container = new SafetySpotContainer(null);
                container.safetySpotNotificationInfo = info;
                SafetySpotInfo safetySpot = info.getSafetySpot();
                Type type = safetySpot.getType();
                GeoCoordinate safetySpotLocation = safetySpot.getCoordinate();
                Image image = new Image();
                switch (type) {
                    case SPEED_CAMERA:
                        image.setImageResource(R.drawable.icon_speed_camera);
                        break;
                    default:
                        image.setImageResource(R.drawable.icon_red_light_camera);
                        break;
                }
                container.mapMarker = new MapMarker(safetySpotLocation, image);
                if (this.canDrawOnTheMap) {
                    mapMarkers.add(container.mapMarker);
                }
                addList.add(container);
                int distance = (int) info.getDistance();
                sLogger.i("add safety spot id[" + System.identityHashCode(info) + "] type[" + type + "] distance calc[" + ((int) currentLocation.distanceTo(safetySpotLocation)) + "] distance[" + distance + "]");
                if (nearestType == null || nearestDistance > distance) {
                    nearestType = type;
                    nearestDistance = distance;
                }
            } catch (Throwable t) {
                sLogger.e("map marker error", t);
            }
        }
        if (this.canDrawOnTheMap) {
            this.mapController.addMapObjects(mapMarkers);
        }
        this.safetySpotNotifications.addAll(addList);
        if (nearestType != null && nearestDistance > 0) {
            sendTts(nearestType, nearestDistance);
        }
    }

    private void sendTts(Type type, int distance) {
        if (Boolean.TRUE.equals(DriverProfileHelper.getInstance().getCurrentProfile().getNavigationPreferences().spokenCameraWarnings)) {
            int resId;
            Distance distanceConverter = new Distance();
            DistanceConverter.convertToDistance(SpeedManager.getInstance().getSpeedUnit(), (float) distance, distanceConverter);
            String formattedDistance = HereManeuverDisplayBuilder.getFormattedDistance(distanceConverter.value, distanceConverter.unit, true);
            switch (type) {
                case SPEED_CAMERA:
                    resId = R.string.camera_warning_speed;
                    break;
                case SPEED_REDLIGHT_CAMERA:
                    resId = R.string.camera_warning_both;
                    break;
                default:
                    resId = R.string.camera_warning_red_light;
                    break;
            }
            TTSUtils.sendSpeechRequest(HudApplication.getAppContext().getResources().getString(resId, new Object[]{formattedDistance}), Category.SPEECH_CAMERA_WARNING, null);
        }
    }

    public void setSafetySpotsEnabledOnMap(final boolean enabled) {
        this.handler.post(new Runnable() {
            public void run() {
                if (HereSafetySpotListener.this.canDrawOnTheMap != enabled) {
                    HereSafetySpotListener.this.canDrawOnTheMap = enabled;
                    if (HereSafetySpotListener.this.canDrawOnTheMap) {
                        HereSafetySpotListener.sLogger.d("Enabling the safety spots on the Map, Total spots Valid : " + HereSafetySpotListener.this.safetySpotNotifications.size() + ", Invalid : " + HereSafetySpotListener.this.removeExpiredSpots());
                        List<MapObject> mapMarkers = new ArrayList();
                        for (SafetySpotContainer container : HereSafetySpotListener.this.safetySpotNotifications) {
                            mapMarkers.add(container.mapMarker);
                        }
                        HereSafetySpotListener.this.mapController.addMapObjects(mapMarkers);
                        return;
                    }
                    HereSafetySpotListener.sLogger.d("Remove all the safety spots from the map");
                    HereSafetySpotListener.this.removeMapMarkers(HereSafetySpotListener.this.safetySpotNotifications);
                }
            }
        });
    }

    private void removeMapMarkers(List<SafetySpotContainer> list) {
        if (list != null) {
            List<MapObject> mapMarkers = new ArrayList();
            for (SafetySpotContainer info : list) {
                mapMarkers.add(info.mapMarker);
            }
            this.mapController.removeMapObjects(mapMarkers);
        }
    }
}
