package com.navdy.hud.app.maps.here;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import com.here.android.mpa.guidance.LaneInformation.Direction;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.maps.MapEvents.LaneData;
import com.navdy.hud.app.maps.MapEvents.LaneData.Position;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class HereLaneInfoBuilder {
    private static boolean initialized;
    private static HashMap<Integer, Drawable> sLaneDrawableCache;
    private static HashMap<Direction, Integer> sLeftOnlyNotRecommendedDirection;
    private static HashMap<Direction, Integer> sLeftOnlyRecommendedDirection;
    private static HashMap<Direction, Integer> sLeftRightNotRecommendedDirection;
    private static HashMap<Direction, Integer> sLeftRightRecommendedDirection;
    private static final Logger sLogger = new Logger(HereLaneInfoBuilder.class);
    private static HashMap<Direction, Integer> sRightOnlyNotRecommendedDirection;
    private static HashMap<Direction, Integer> sRightOnlyRecommendedDirection;
    private static HashMap<Direction, Integer> sSingleNotRecommendedDirection;
    private static HashMap<Direction, Integer> sSingleRecommendedDirection;

    public enum DirectionType {
        LEFT,
        RIGHT,
        LEFT_RIGHT
    }

    public static synchronized void init() {
        synchronized (HereLaneInfoBuilder.class) {
            if (!initialized) {
                GenericUtil.checkNotOnMainThread();
                initialized = true;
                Resources resources = HudApplication.getAppContext().getResources();
                sLaneDrawableCache = new HashMap();
                sSingleRecommendedDirection = new HashMap();
                sSingleRecommendedDirection.put(Direction.STRAIGHT, Integer.valueOf(R.drawable.icon_lg_rec_straight_only));
                sSingleRecommendedDirection.put(Direction.SLIGHTLY_RIGHT, Integer.valueOf(R.drawable.icon_lg_rec_slight_right_only));
                sSingleRecommendedDirection.put(Direction.RIGHT, Integer.valueOf(R.drawable.icon_lg_rec_right_only));
                sSingleRecommendedDirection.put(Direction.SHARP_RIGHT, Integer.valueOf(R.drawable.icon_lg_rec_heavy_right_only));
                sSingleRecommendedDirection.put(Direction.U_TURN_RIGHT, Integer.valueOf(R.drawable.icon_lg_rec_uturn_right_only));
                sSingleRecommendedDirection.put(Direction.SECOND_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sSingleRecommendedDirection.put(Direction.MERGE_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sSingleRecommendedDirection.put(Direction.U_TURN_LEFT, Integer.valueOf(R.drawable.icon_lg_rec_uturn_left_only));
                sSingleRecommendedDirection.put(Direction.SHARP_LEFT, Integer.valueOf(R.drawable.icon_lg_rec_heavy_left_only));
                sSingleRecommendedDirection.put(Direction.LEFT, Integer.valueOf(R.drawable.icon_lg_rec_left_only));
                sSingleRecommendedDirection.put(Direction.SLIGHTLY_LEFT, Integer.valueOf(R.drawable.icon_lg_rec_slight_left_only));
                sSingleRecommendedDirection.put(Direction.SECOND_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sSingleRecommendedDirection.put(Direction.MERGE_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sSingleRecommendedDirection.put(Direction.MERGE_LANES, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_rec_straight_only), resources.getDrawable(R.drawable.icon_lg_rec_straight_only));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_rec_slight_right_only), resources.getDrawable(R.drawable.icon_lg_rec_slight_right_only));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_rec_right_only), resources.getDrawable(R.drawable.icon_lg_rec_right_only));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_rec_heavy_right_only), resources.getDrawable(R.drawable.icon_lg_rec_heavy_right_only));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_rec_uturn_right_only), resources.getDrawable(R.drawable.icon_lg_rec_uturn_right_only));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_rec_uturn_left_only), resources.getDrawable(R.drawable.icon_lg_rec_uturn_left_only));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_rec_heavy_left_only), resources.getDrawable(R.drawable.icon_lg_rec_heavy_left_only));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_rec_left_only), resources.getDrawable(R.drawable.icon_lg_rec_left_only));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_rec_slight_left_only), resources.getDrawable(R.drawable.icon_lg_rec_slight_left_only));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_glance_whatsapp), resources.getDrawable(R.drawable.icon_glance_whatsapp));
                sSingleNotRecommendedDirection = new HashMap();
                sSingleNotRecommendedDirection.put(Direction.STRAIGHT, Integer.valueOf(R.drawable.icon_lg_straight_only));
                sSingleNotRecommendedDirection.put(Direction.SLIGHTLY_RIGHT, Integer.valueOf(R.drawable.icon_lg_slight_right_only));
                sSingleNotRecommendedDirection.put(Direction.RIGHT, Integer.valueOf(R.drawable.icon_lg_right_only));
                sSingleNotRecommendedDirection.put(Direction.SHARP_RIGHT, Integer.valueOf(R.drawable.icon_lg_heavy_right_only));
                sSingleNotRecommendedDirection.put(Direction.U_TURN_RIGHT, Integer.valueOf(R.drawable.icon_lg_uturn_right_only));
                sSingleNotRecommendedDirection.put(Direction.SECOND_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sSingleNotRecommendedDirection.put(Direction.MERGE_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sSingleNotRecommendedDirection.put(Direction.U_TURN_LEFT, Integer.valueOf(R.drawable.icon_lg_uturn_left_only));
                sSingleNotRecommendedDirection.put(Direction.SHARP_LEFT, Integer.valueOf(R.drawable.icon_lg_heavy_left_only));
                sSingleNotRecommendedDirection.put(Direction.LEFT, Integer.valueOf(R.drawable.icon_lg_left_only));
                sSingleNotRecommendedDirection.put(Direction.SLIGHTLY_LEFT, Integer.valueOf(R.drawable.icon_lg_slight_left_only));
                sSingleNotRecommendedDirection.put(Direction.SECOND_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sSingleNotRecommendedDirection.put(Direction.MERGE_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sSingleNotRecommendedDirection.put(Direction.MERGE_LANES, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_straight_only), resources.getDrawable(R.drawable.icon_lg_straight_only));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_slight_right_only), resources.getDrawable(R.drawable.icon_lg_slight_right_only));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_right_only), resources.getDrawable(R.drawable.icon_lg_right_only));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_heavy_right_only), resources.getDrawable(R.drawable.icon_lg_heavy_right_only));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_uturn_right_only), resources.getDrawable(R.drawable.icon_lg_uturn_right_only));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_uturn_left_only), resources.getDrawable(R.drawable.icon_lg_uturn_left_only));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_heavy_left_only), resources.getDrawable(R.drawable.icon_lg_heavy_left_only));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_left_only), resources.getDrawable(R.drawable.icon_lg_left_only));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_slight_left_only), resources.getDrawable(R.drawable.icon_lg_slight_left_only));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_glance_whatsapp), resources.getDrawable(R.drawable.icon_glance_whatsapp));
                sRightOnlyRecommendedDirection = new HashMap();
                sRightOnlyRecommendedDirection.put(Direction.STRAIGHT, Integer.valueOf(R.drawable.icon_lg_compright_rec_straight));
                sRightOnlyRecommendedDirection.put(Direction.SLIGHTLY_RIGHT, Integer.valueOf(R.drawable.icon_lg_compright_rec_slight_right));
                sRightOnlyRecommendedDirection.put(Direction.RIGHT, Integer.valueOf(R.drawable.icon_lg_compright_rec_right));
                sRightOnlyRecommendedDirection.put(Direction.SHARP_RIGHT, Integer.valueOf(R.drawable.icon_lg_compright_rec_heavy_right));
                sRightOnlyRecommendedDirection.put(Direction.U_TURN_RIGHT, Integer.valueOf(R.drawable.icon_lg_compright_rec_uturn_right));
                sRightOnlyRecommendedDirection.put(Direction.SECOND_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sRightOnlyRecommendedDirection.put(Direction.MERGE_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_rec_straight), resources.getDrawable(R.drawable.icon_lg_compright_rec_straight));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_rec_slight_right), resources.getDrawable(R.drawable.icon_lg_compright_rec_slight_right));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_rec_right), resources.getDrawable(R.drawable.icon_lg_compright_rec_right));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_rec_heavy_right), resources.getDrawable(R.drawable.icon_lg_compright_rec_heavy_right));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_rec_uturn_right), resources.getDrawable(R.drawable.icon_lg_compright_rec_uturn_right));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_glance_whatsapp), resources.getDrawable(R.drawable.icon_glance_whatsapp));
                sRightOnlyNotRecommendedDirection = new HashMap();
                sRightOnlyNotRecommendedDirection.put(Direction.STRAIGHT, Integer.valueOf(R.drawable.icon_lg_compright_straight));
                sRightOnlyNotRecommendedDirection.put(Direction.SLIGHTLY_RIGHT, Integer.valueOf(R.drawable.icon_lg_compright_slight_right));
                sRightOnlyNotRecommendedDirection.put(Direction.RIGHT, Integer.valueOf(R.drawable.icon_lg_compright_right));
                sRightOnlyNotRecommendedDirection.put(Direction.SHARP_RIGHT, Integer.valueOf(R.drawable.icon_lg_compright_heavy_right));
                sRightOnlyNotRecommendedDirection.put(Direction.U_TURN_RIGHT, Integer.valueOf(R.drawable.icon_lg_compright_uturn_right));
                sRightOnlyNotRecommendedDirection.put(Direction.SECOND_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sRightOnlyNotRecommendedDirection.put(Direction.MERGE_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_straight), resources.getDrawable(R.drawable.icon_lg_compright_straight));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_slight_right), resources.getDrawable(R.drawable.icon_lg_compright_slight_right));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_right), resources.getDrawable(R.drawable.icon_lg_compright_right));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_heavy_right), resources.getDrawable(R.drawable.icon_lg_compright_heavy_right));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_uturn_right), resources.getDrawable(R.drawable.icon_lg_compright_uturn_right));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_glance_whatsapp), resources.getDrawable(R.drawable.icon_glance_whatsapp));
                sLeftOnlyRecommendedDirection = new HashMap();
                sLeftOnlyRecommendedDirection.put(Direction.STRAIGHT, Integer.valueOf(R.drawable.icon_lg_compleft_rec_straight));
                sLeftOnlyRecommendedDirection.put(Direction.SLIGHTLY_LEFT, Integer.valueOf(R.drawable.icon_lg_compleft_rec_slight_left));
                sLeftOnlyRecommendedDirection.put(Direction.LEFT, Integer.valueOf(R.drawable.icon_lg_compleft_rec_left));
                sLeftOnlyRecommendedDirection.put(Direction.SHARP_LEFT, Integer.valueOf(R.drawable.icon_lg_compleft_rec_heavy_left));
                sLeftOnlyRecommendedDirection.put(Direction.U_TURN_LEFT, Integer.valueOf(R.drawable.icon_lg_compleft_rec_uturn_left));
                sLeftOnlyRecommendedDirection.put(Direction.SECOND_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sLeftOnlyRecommendedDirection.put(Direction.MERGE_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_rec_straight), resources.getDrawable(R.drawable.icon_lg_compleft_rec_straight));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_rec_slight_left), resources.getDrawable(R.drawable.icon_lg_compleft_rec_slight_left));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_rec_left), resources.getDrawable(R.drawable.icon_lg_compleft_rec_left));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_rec_heavy_left), resources.getDrawable(R.drawable.icon_lg_compleft_rec_heavy_left));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_rec_uturn_left), resources.getDrawable(R.drawable.icon_lg_compleft_rec_uturn_left));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_glance_whatsapp), resources.getDrawable(R.drawable.icon_glance_whatsapp));
                sLeftOnlyNotRecommendedDirection = new HashMap();
                sLeftOnlyNotRecommendedDirection.put(Direction.STRAIGHT, Integer.valueOf(R.drawable.icon_lg_compleft_straight));
                sLeftOnlyNotRecommendedDirection.put(Direction.SLIGHTLY_LEFT, Integer.valueOf(R.drawable.icon_lg_compleft_slight_left));
                sLeftOnlyNotRecommendedDirection.put(Direction.LEFT, Integer.valueOf(R.drawable.icon_lg_compleft_left));
                sLeftOnlyNotRecommendedDirection.put(Direction.SHARP_LEFT, Integer.valueOf(R.drawable.icon_lg_compleft_heavy_left));
                sLeftOnlyNotRecommendedDirection.put(Direction.U_TURN_LEFT, Integer.valueOf(R.drawable.icon_lg_compleft_uturn_left));
                sLeftOnlyNotRecommendedDirection.put(Direction.SECOND_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sLeftOnlyNotRecommendedDirection.put(Direction.MERGE_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_straight), resources.getDrawable(R.drawable.icon_lg_compleft_straight));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_slight_left), resources.getDrawable(R.drawable.icon_lg_compleft_slight_left));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_left), resources.getDrawable(R.drawable.icon_lg_compleft_left));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_heavy_left), resources.getDrawable(R.drawable.icon_lg_compleft_heavy_left));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_uturn_left), resources.getDrawable(R.drawable.icon_lg_compleft_uturn_left));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_glance_whatsapp), resources.getDrawable(R.drawable.icon_glance_whatsapp));
                sLeftRightRecommendedDirection = new HashMap();
                sLeftRightRecommendedDirection.put(Direction.STRAIGHT, Integer.valueOf(R.drawable.icon_lg_compboth_rec_straight));
                sLeftRightRecommendedDirection.put(Direction.SLIGHTLY_RIGHT, Integer.valueOf(R.drawable.icon_lg_compboth_rec_slight_right));
                sLeftRightRecommendedDirection.put(Direction.RIGHT, Integer.valueOf(R.drawable.icon_lg_compboth_rec_right));
                sLeftRightRecommendedDirection.put(Direction.SHARP_RIGHT, Integer.valueOf(R.drawable.icon_lg_compboth_rec_heavy_right));
                sLeftRightRecommendedDirection.put(Direction.SECOND_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sLeftRightRecommendedDirection.put(Direction.MERGE_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sLeftRightRecommendedDirection.put(Direction.SLIGHTLY_LEFT, Integer.valueOf(R.drawable.icon_lg_compboth_rec_slight_left));
                sLeftRightRecommendedDirection.put(Direction.LEFT, Integer.valueOf(R.drawable.icon_lg_compboth_rec_left));
                sLeftRightRecommendedDirection.put(Direction.SHARP_LEFT, Integer.valueOf(R.drawable.icon_lg_compboth_rec_heavy_left));
                sLeftRightRecommendedDirection.put(Direction.MERGE_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_rec_straight), resources.getDrawable(R.drawable.icon_lg_compboth_rec_straight));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_rec_slight_right), resources.getDrawable(R.drawable.icon_lg_compboth_rec_slight_right));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_rec_right), resources.getDrawable(R.drawable.icon_lg_compboth_rec_right));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_rec_heavy_right), resources.getDrawable(R.drawable.icon_lg_compboth_rec_heavy_right));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_rec_slight_left), resources.getDrawable(R.drawable.icon_lg_compboth_rec_slight_left));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_rec_left), resources.getDrawable(R.drawable.icon_lg_compboth_rec_left));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_rec_heavy_left), resources.getDrawable(R.drawable.icon_lg_compboth_rec_heavy_left));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_glance_whatsapp), resources.getDrawable(R.drawable.icon_glance_whatsapp));
                sLeftRightNotRecommendedDirection = new HashMap();
                sLeftRightNotRecommendedDirection.put(Direction.STRAIGHT, Integer.valueOf(R.drawable.icon_lg_compboth_straight));
                sLeftRightNotRecommendedDirection.put(Direction.SLIGHTLY_RIGHT, Integer.valueOf(R.drawable.icon_lg_compboth_slight_right));
                sLeftRightNotRecommendedDirection.put(Direction.RIGHT, Integer.valueOf(R.drawable.icon_lg_compboth_right));
                sLeftRightNotRecommendedDirection.put(Direction.SHARP_RIGHT, Integer.valueOf(R.drawable.icon_lg_compboth_heavy_right));
                sLeftRightNotRecommendedDirection.put(Direction.MERGE_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sLeftRightNotRecommendedDirection.put(Direction.SLIGHTLY_LEFT, Integer.valueOf(R.drawable.icon_lg_compboth_slight_left));
                sLeftRightNotRecommendedDirection.put(Direction.LEFT, Integer.valueOf(R.drawable.icon_lg_compboth_left));
                sLeftRightNotRecommendedDirection.put(Direction.SHARP_LEFT, Integer.valueOf(R.drawable.icon_lg_compboth_heavy_left));
                sLeftRightNotRecommendedDirection.put(Direction.MERGE_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_straight), resources.getDrawable(R.drawable.icon_lg_compboth_straight));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_slight_right), resources.getDrawable(R.drawable.icon_lg_compboth_slight_right));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_right), resources.getDrawable(R.drawable.icon_lg_compboth_right));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_heavy_right), resources.getDrawable(R.drawable.icon_lg_compboth_heavy_right));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_slight_left), resources.getDrawable(R.drawable.icon_lg_compboth_slight_left));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_left), resources.getDrawable(R.drawable.icon_lg_compboth_left));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_heavy_left), resources.getDrawable(R.drawable.icon_lg_compboth_heavy_left));
                sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_glance_whatsapp), resources.getDrawable(R.drawable.icon_glance_whatsapp));
            }
        }
    }

    public static Drawable[] getDrawable(List<Direction> directions, Position position, Direction onRouteDirection) {
        if (directions == null) {
            return null;
        }
        Drawable[] ret;
        Drawable[] drawables;
        int counter;
        int counter2;
        if (directions.size() == 1) {
            ret = new Drawable[1];
            ret[0] = getDrawableForDirection((Direction) directions.get(0), position, sSingleRecommendedDirection, sSingleNotRecommendedDirection);
        } else {
            DirectionType directionType = getDirectionType(directions);
            drawables = new Drawable[directions.size()];
            counter = 0;
            for (Direction d : directions) {
                Position tempPosition = position;
                if (position == Position.ON_ROUTE && onRouteDirection != null) {
                    tempPosition = d == onRouteDirection ? Position.ON_ROUTE : Position.OFF_ROUTE;
                }
                switch (directionType) {
                    case LEFT:
                        counter2 = counter + 1;
                        drawables[counter] = getDrawableForDirection(d, tempPosition, sLeftOnlyRecommendedDirection, sLeftOnlyNotRecommendedDirection);
                        counter = counter2;
                        break;
                    case RIGHT:
                        counter2 = counter + 1;
                        drawables[counter] = getDrawableForDirection(d, tempPosition, sRightOnlyRecommendedDirection, sRightOnlyNotRecommendedDirection);
                        counter = counter2;
                        break;
                    case LEFT_RIGHT:
                        counter2 = counter + 1;
                        drawables[counter] = getDrawableForDirection(d, tempPosition, sLeftRightRecommendedDirection, sLeftRightNotRecommendedDirection);
                        counter = counter2;
                        break;
                    default:
                        break;
                }
            }
            ret = drawables;
        }
        if (ret == null) {
            return ret;
        }
        int i;
        int nonNulls = 0;
        int nulls = 0;
        for (Drawable drawable : ret) {
            if (drawable == null) {
                nulls++;
            } else {
                nonNulls++;
            }
        }
        if (nonNulls == 0) {
            return null;
        }
        if (nulls <= 0) {
            return ret;
        }
        drawables = new Drawable[nonNulls];
        counter = 0;
        for (i = 0; i < ret.length; i++) {
            if (ret[i] != null) {
                counter2 = counter + 1;
                drawables[counter] = ret[i];
                counter = counter2;
            }
        }
        return drawables;
    }

    private static Drawable getDrawableForDirection(Direction direction, Position position, HashMap<Direction, Integer> recommendedMap, HashMap<Direction, Integer> notRecommendedMap) {
        Integer icon;
        if (position == Position.ON_ROUTE) {
            icon = (Integer) recommendedMap.get(direction);
        } else {
            icon = (Integer) notRecommendedMap.get(direction);
        }
        if (icon == null) {
            return null;
        }
        return (Drawable) sLaneDrawableCache.get(icon);
    }

    private static DirectionType getDirectionType(List<Direction> directions) {
        int left = 0;
        int right = 0;
        for (Direction d : directions) {
            switch (d) {
                case STRAIGHT:
                    break;
                case SLIGHTLY_RIGHT:
                    right++;
                    break;
                case RIGHT:
                    right++;
                    break;
                case SHARP_RIGHT:
                    right++;
                    break;
                case U_TURN_LEFT:
                    left++;
                    break;
                case SHARP_LEFT:
                    left++;
                    break;
                case LEFT:
                    left++;
                    break;
                case SLIGHTLY_LEFT:
                    left++;
                    break;
                case U_TURN_RIGHT:
                    right++;
                    break;
                default:
                    break;
            }
        }
        if (left > 0 && right > 0) {
            return DirectionType.LEFT_RIGHT;
        }
        if (left > 0) {
            return DirectionType.LEFT;
        }
        return DirectionType.RIGHT;
    }

    public static boolean compareLaneData(ArrayList<LaneData> l1, ArrayList<LaneData> l2) {
        if (l1 == null || l2 == null) {
            return false;
        }
        int size1 = l1.size();
        if (size1 != l2.size()) {
            return false;
        }
        for (int i = 0; i < size1; i++) {
            LaneData laneData1 = (LaneData) l1.get(i);
            LaneData laneData2 = (LaneData) l2.get(i);
            if (laneData1 == null || laneData2 == null || laneData1.position != laneData2.position || laneData1.icons == null || laneData2.icons == null || laneData1.icons.length != laneData2.icons.length) {
                return false;
            }
            for (int j = 0; j < laneData1.icons.length; j++) {
                if (laneData1.icons[j] != laneData2.icons[j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static int getNumDirections(EnumSet<Direction> directions, DirectionType type) {
        int n = 0;
        Iterator it = directions.iterator();
        while (it.hasNext()) {
            switch ((Direction) it.next()) {
                case SLIGHTLY_RIGHT:
                case RIGHT:
                case SHARP_RIGHT:
                    if (type != DirectionType.RIGHT) {
                        break;
                    }
                    n++;
                    break;
                case SHARP_LEFT:
                case LEFT:
                case SLIGHTLY_LEFT:
                    if (type != DirectionType.LEFT) {
                        break;
                    }
                    n++;
                    break;
                default:
                    break;
            }
        }
        return n;
    }

    public static Direction getDirection(EnumSet<Direction> directions, DirectionType type) {
        Iterator it = directions.iterator();
        while (it.hasNext()) {
            Direction d = (Direction) it.next();
            switch (d) {
                case SLIGHTLY_RIGHT:
                case RIGHT:
                case SHARP_RIGHT:
                    if (type != DirectionType.RIGHT) {
                        break;
                    }
                    return d;
                case SHARP_LEFT:
                case LEFT:
                case SLIGHTLY_LEFT:
                    if (type != DirectionType.LEFT) {
                        break;
                    }
                    return d;
                default:
                    break;
            }
        }
        return null;
    }
}
