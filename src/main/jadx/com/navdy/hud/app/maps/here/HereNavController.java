package com.navdy.hud.app.maps.here;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import com.here.android.mpa.guidance.NavigationManager;
import com.here.android.mpa.guidance.NavigationManager.AspectRatio;
import com.here.android.mpa.guidance.NavigationManager.Error;
import com.here.android.mpa.guidance.NavigationManager.LaneInformationListener;
import com.here.android.mpa.guidance.NavigationManager.NavigationMode;
import com.here.android.mpa.guidance.NavigationManager.RealisticViewListener;
import com.here.android.mpa.guidance.NavigationManager.RealisticViewMode;
import com.here.android.mpa.guidance.NavigationManager.TrafficAvoidanceMode;
import com.here.android.mpa.guidance.NavigationManager.TrafficRerouteListener;
import com.here.android.mpa.guidance.NavigationManager.UnitSystem;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.Route.TrafficPenaltyMode;
import com.here.android.mpa.routing.RouteTta;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.analytics.NavigationQualityTracker;
import com.navdy.hud.app.framework.glympse.GlympseManager;
import com.navdy.hud.app.framework.trips.TripManager.FinishedTripRouteEvent;
import com.navdy.hud.app.framework.trips.TripManager.NewTripEvent;
import com.navdy.hud.app.maps.MapSettings;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class HereNavController {
    private static final int ANIMATE_STATE_CLEAR_DELAY = 2000;
    private static final FinishedTripRouteEvent TRIP_END = new FinishedTripRouteEvent();
    private static final NewTripEvent TRIP_START = new NewTripEvent();
    private static final Logger sLogger = new Logger(HereNavController.class);
    private final Bus bus;
    private Runnable clearAnimatorState = new Runnable() {
        public void run() {
            TaskManager.getInstance().execute(HereNavController.this.clearAnimatorStateBk, 17);
        }
    };
    private Runnable clearAnimatorStateBk = new Runnable() {
        public void run() {
            HereMapsManager.getInstance().getMapAnimator().clearState();
        }
    };
    private boolean firstTrip = true;
    private Handler handler = new Handler(Looper.getMainLooper());
    private boolean initialized;
    private final NavigationManager navigationManager;
    private final NavigationQualityTracker navigationQualityTracker;
    private Route route;
    private volatile State state;

    public enum State {
        NAVIGATING,
        TRACKING
    }

    HereNavController(NavigationManager navigationManager, Bus bus) {
        this.navigationManager = navigationManager;
        this.bus = bus;
        sLogger.v(":ctor: state:" + this.state);
        this.navigationQualityTracker = NavigationQualityTracker.getInstance();
    }

    public State getState() {
        return this.state;
    }

    public synchronized void initialize() {
        if (!this.initialized) {
            this.initialized = true;
            this.state = State.TRACKING;
            sLogger.v("initialize state:" + this.state + " error = " + this.navigationManager.startTracking());
            startTrip();
        }
    }

    public synchronized boolean isInitialized() {
        return this.initialized;
    }

    public synchronized Error startNavigation(Route route, int simulationSpeed) {
        Error error;
        GenericUtil.checkNotOnMainThread();
        sLogger.v("startNavigation state[" + this.state + "]");
        sLogger.v("startNavigation simulationSpeed:" + simulationSpeed + " route-id=" + System.identityHashCode(route));
        if (this.state == State.NAVIGATING) {
            sLogger.v("startNavigation stop existing nav");
            stopNavigation(false);
        }
        long l1 = SystemClock.elapsedRealtime();
        if (simulationSpeed == 0) {
            simulationSpeed = MapSettings.getSimulationSpeed();
        }
        if (simulationSpeed > 0) {
            error = this.navigationManager.simulate(route, (long) simulationSpeed);
        } else {
            error = this.navigationManager.startNavigation(route);
        }
        sLogger.v("startNavigation took [" + (SystemClock.elapsedRealtime() - l1) + "]");
        if (error == Error.NONE) {
            this.state = State.NAVIGATING;
            this.route = route;
            sLogger.v("startNavigation: success [" + this.state + "] route-id=" + System.identityHashCode(route));
            endTrip();
            startTrip();
            this.navigationQualityTracker.trackTripStarted((long) route.getTta(TrafficPenaltyMode.OPTIMAL, 268435455).getDuration(), (long) route.getTta(TrafficPenaltyMode.DISABLED, 268435455).getDuration(), (long) route.getLength());
            AnalyticsSupport.recordNavigation(true);
        } else {
            sLogger.e("startNavigation error [" + this.state + "] " + error.name());
        }
        return error;
    }

    public synchronized void stopNavigation(boolean sendTripEvent) {
        boolean resetMapAnimator = false;
        synchronized (this) {
            GenericUtil.checkNotOnMainThread();
            sLogger.v("stopNavigation state[" + this.state + "]");
            if (this.state == State.NAVIGATING) {
                long l1 = SystemClock.elapsedRealtime();
                sLogger.v("stopNavigation:stopping nav state[" + this.state + " ] route-id=" + System.identityHashCode(this.route));
                if (this.navigationManager.getNavigationMode() == NavigationMode.SIMULATION) {
                    resetMapAnimator = true;
                }
                expireGlympseTickets();
                this.navigationManager.stop();
                if (resetMapAnimator) {
                    this.handler.postDelayed(this.clearAnimatorState, 2000);
                }
                sLogger.v("stopNavigation:stopped nav state[" + this.state + " ] took [" + (SystemClock.elapsedRealtime() - l1) + "]");
                sLogger.v("stopNavigation: tracking error state =" + this.navigationManager.startTracking());
                this.state = State.TRACKING;
                AnalyticsSupport.recordNavigation(false);
                if (sendTripEvent) {
                    endTrip();
                    startTrip();
                }
                this.route = null;
                this.navigationQualityTracker.cancelTrip();
            } else {
                sLogger.v("stopNavigation: already " + this.state);
            }
        }
    }

    public synchronized void arrivedAtDestination() {
        GenericUtil.checkNotOnMainThread();
        sLogger.v("arrivedAtDestination state[" + this.state + "]");
        if (this.state == State.NAVIGATING) {
            this.navigationQualityTracker.trackTripEnded(getElapsedDistance());
            expireGlympseTickets();
            this.navigationManager.stop();
            sLogger.v("arrivedAtDestination: tracking error state =" + this.navigationManager.startTracking());
        } else {
            sLogger.v("arrivedAtDestination: not navigating:" + this.state);
        }
    }

    public synchronized void pause() {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.pause();
    }

    public synchronized Error resume() {
        GenericUtil.checkNotOnMainThread();
        return this.navigationManager.resume();
    }

    public RouteTta getTta(TrafficPenaltyMode trafficPenaltyMode, boolean wholeRoute) {
        GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getTta(trafficPenaltyMode, wholeRoute);
    }

    public long getDestinationDistance() {
        GenericUtil.checkNotOnMainThread();
        if (this.state == State.TRACKING) {
            return 0;
        }
        return this.navigationManager.getDestinationDistance();
    }

    public Date getEta(boolean wholeRoute, TrafficPenaltyMode trafficPenaltyMode) {
        GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getEta(wholeRoute, trafficPenaltyMode);
    }

    public Date getTtaDate(boolean wholeRoute, TrafficPenaltyMode trafficPenaltyMode) {
        GenericUtil.checkNotOnMainThread();
        RouteTta routeTta = this.navigationManager.getTta(trafficPenaltyMode, wholeRoute);
        if (routeTta == null) {
            return null;
        }
        int trafficDuration = routeTta.getDuration();
        if (trafficDuration > 0) {
            return new Date(System.currentTimeMillis() + TimeUnit.SECONDS.toMillis((long) trafficDuration));
        }
        return null;
    }

    public Maneuver getNextManeuver() {
        GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getNextManeuver();
    }

    public Maneuver getAfterNextManeuver() {
        GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getAfterNextManeuver();
    }

    public long getElapsedDistance() {
        GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getElapsedDistance();
    }

    public void addLaneInfoListener(WeakReference<LaneInformationListener> listener) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.addLaneInformationListener(listener);
    }

    public void removeLaneInfoListener(LaneInformationListener listener) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.removeLaneInformationListener(listener);
    }

    public void setRealisticViewMode(RealisticViewMode mode) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.setRealisticViewMode(mode);
    }

    public void addRealisticViewAspectRatio(AspectRatio aspectRatio) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.addRealisticViewAspectRatio(aspectRatio);
    }

    public void addRealisticViewListener(WeakReference<RealisticViewListener> listener) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.addRealisticViewListener(listener);
    }

    public void removeRealisticViewListener(RealisticViewListener listener) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.removeRealisticViewListener(listener);
    }

    public Error setRoute(Route route) {
        GenericUtil.checkNotOnMainThread();
        return this.navigationManager.setRoute(route);
    }

    public void addTrafficRerouteListener(WeakReference<TrafficRerouteListener> listener) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.addTrafficRerouteListener(listener);
    }

    public void removeTrafficRerouteListener(TrafficRerouteListener listener) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.removeTrafficRerouteListener(listener);
    }

    public void setDistanceUnit(UnitSystem unitSystem) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.setDistanceUnit(unitSystem);
    }

    public long getNextManeuverDistance() {
        GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getNextManeuverDistance();
    }

    private void startTrip() {
        if (this.firstTrip) {
            this.firstTrip = false;
            sLogger.v("first trip started");
        } else {
            sLogger.v("trip started");
        }
        this.bus.post(TRIP_START);
    }

    public void endTrip() {
        sLogger.v("trip ended");
        this.bus.post(TRIP_END);
    }

    public void setTrafficAvoidanceMode(TrafficAvoidanceMode mode) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.setTrafficAvoidanceMode(mode);
    }

    void expireGlympseTickets() {
        this.handler.post(new Runnable() {
            public void run() {
                GlympseManager.getInstance().expireActiveTickets();
            }
        });
    }
}
