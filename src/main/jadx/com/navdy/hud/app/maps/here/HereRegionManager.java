package com.navdy.hud.app.maps.here;

import android.os.Handler;
import android.os.Looper;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.search.Address;
import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.Location;
import com.here.android.mpa.search.ResultListener;
import com.here.android.mpa.search.ReverseGeocodeRequest2;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.maps.MapEvents.RegionEvent;
import com.navdy.hud.app.util.CrashReporter;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import javax.inject.Inject;
import mortar.Mortar;

public class HereRegionManager {
    private static final long REGION_CHECK_INTERVAL = 900000;
    private static final HereRegionManager sInstance = new HereRegionManager();
    private static final Logger sLogger = new Logger(HereRegionManager.class);
    @Inject
    Bus bus;
    private final Runnable checkRegionRunnable = new Runnable() {
        public void run() {
            HereRegionManager.this.checkRegion();
        }
    };
    private RegionEvent currentRegionEvent;
    private final Handler handler;
    private final HereMapsManager hereMapsManager;

    public static HereRegionManager getInstance() {
        return sInstance;
    }

    private HereRegionManager() {
        Mortar.inject(HudApplication.getAppContext(), this);
        this.hereMapsManager = HereMapsManager.getInstance();
        this.handler = new Handler(Looper.getMainLooper());
        this.handler.postDelayed(this.checkRegionRunnable, REGION_CHECK_INTERVAL);
    }

    private void checkRegion() {
        try {
            GeoPosition geoPosition = this.hereMapsManager.getLastGeoPosition();
            if (geoPosition == null) {
                sLogger.v("invalid checkRegion call with unknown geoPosition");
                return;
            }
            ErrorCode error = new ReverseGeocodeRequest2(geoPosition.getCoordinate()).execute(new ResultListener<Location>() {
                public void onCompleted(Location location, ErrorCode errorCode) {
                    try {
                        if (errorCode != ErrorCode.NONE) {
                            HereRegionManager.sLogger.e("reverseGeoCode:onComplete: Error[" + errorCode.name() + "]");
                        } else if (location == null) {
                            HereRegionManager.sLogger.e("reverseGeoCode:onComplete: address is null");
                        } else {
                            Address address = location.getAddress();
                            if (address != null) {
                                RegionEvent regionEvent = new RegionEvent(address.getState(), address.getCountryName());
                                if (!regionEvent.equals(HereRegionManager.this.currentRegionEvent)) {
                                    HereRegionManager.sLogger.v("" + regionEvent);
                                    HereRegionManager.this.currentRegionEvent = regionEvent;
                                    HereRegionManager.this.bus.post(HereRegionManager.this.currentRegionEvent);
                                }
                            }
                        }
                    } catch (Throwable t) {
                        HereRegionManager.sLogger.e(t);
                        CrashReporter.getInstance().reportNonFatalException(t);
                    }
                }
            });
            if (error != ErrorCode.NONE) {
                sLogger.e("reverseGeoCode:execute: Error[" + error.name() + "]");
            }
            this.handler.postDelayed(this.checkRegionRunnable, REGION_CHECK_INTERVAL);
        } catch (Throwable t) {
            sLogger.e(t);
            CrashReporter.getInstance().reportNonFatalException(t);
        } finally {
            this.handler.postDelayed(this.checkRegionRunnable, REGION_CHECK_INTERVAL);
        }
    }
}
