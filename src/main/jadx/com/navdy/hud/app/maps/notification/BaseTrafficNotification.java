package com.navdy.hud.app.maps.notification;

import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;

public abstract class BaseTrafficNotification implements INotification {
    protected static final String EMPTY = "";
    public static final float IMAGE_SCALE = 0.5f;
    protected ChoiceLayout2 choiceLayout;
    protected INotificationController controller;
    protected Logger logger = new Logger(getClass());

    public void onStart(INotificationController controller) {
        this.controller = controller;
    }

    public void onStop() {
        this.controller = null;
        if (this.choiceLayout != null) {
            this.choiceLayout.clear();
        }
    }

    public int getTimeout() {
        return 0;
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.choiceLayout == null) {
            return false;
        }
        switch (event) {
            case LEFT:
                this.choiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                this.choiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                this.choiceLayout.executeSelectedItem();
                return true;
            default:
                return false;
        }
    }

    protected void dismissNotification() {
        NotificationManager.getInstance().removeNotification(getId());
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public IInputHandler nextHandler() {
        return null;
    }

    public boolean supportScroll() {
        return false;
    }
}
