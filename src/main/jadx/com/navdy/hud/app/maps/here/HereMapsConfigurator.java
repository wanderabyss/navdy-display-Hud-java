package com.navdy.hud.app.maps.here;

import android.content.Context;
import android.os.SystemClock;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.util.PackagedResource;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.File;
import java.util.List;

class HereMapsConfigurator {
    private static final HereMapsConfigurator sInstance = new HereMapsConfigurator();
    private static final Logger sLogger = new Logger(HereMapsConfigurator.class);
    private volatile boolean isAlreadyConfigured;

    HereMapsConfigurator() {
    }

    public static HereMapsConfigurator getInstance() {
        return sInstance;
    }

    synchronized void updateMapsConfig() {
        if (this.isAlreadyConfigured) {
            sLogger.w("MWConfig is already configured");
        } else {
            GenericUtil.checkNotOnMainThread();
            Context context = HudApplication.getAppContext();
            String latestHereMapConfigFolder = PathManager.getInstance().getLatestHereMapsConfigPath();
            sLogger.i("MWConfig: starting");
            List<String> hereMapsConfigFolders = PathManager.getInstance().getHereMapsConfigDirs();
            PackagedResource mwConfigLatest = new PackagedResource("mwconfig_client", latestHereMapConfigFolder);
            try {
                long l1 = SystemClock.elapsedRealtime();
                mwConfigLatest.updateFromResources(context, R.raw.mwconfig_hud, R.raw.mwconfig_hud_md5);
                removeOldHereMapsFolders(hereMapsConfigFolders, latestHereMapConfigFolder);
                sLogger.i("MWConfig: time took " + (SystemClock.elapsedRealtime() - l1));
                this.isAlreadyConfigured = true;
            } catch (Throwable throwable) {
                sLogger.e("MWConfig error", throwable);
            }
            IOUtils.checkIntegrity(context, latestHereMapConfigFolder, R.raw.here_mwconfig_integrity);
        }
        return;
    }

    void removeMWConfigFolder() {
        IOUtils.deleteDirectory(HudApplication.getAppContext(), new File(PathManager.getInstance().getLatestHereMapsConfigPath()));
    }

    private void removeOldHereMapsFolders(List<String> hereMapsConfigFolders, String latestHereMapConfigFolder) {
        for (String hereMapsConfigFolder : hereMapsConfigFolders) {
            if (!TextUtils.equals(hereMapsConfigFolder, latestHereMapConfigFolder)) {
                IOUtils.deleteDirectory(HudApplication.getAppContext(), new File(hereMapsConfigFolder));
            }
        }
    }
}
