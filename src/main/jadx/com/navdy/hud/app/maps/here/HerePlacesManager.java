package com.navdy.hud.app.maps.here;

import android.content.Context;
import android.os.SystemClock;
import android.text.TextUtils;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.search.Category;
import com.here.android.mpa.search.CategoryFilter;
import com.here.android.mpa.search.DiscoveryResult;
import com.here.android.mpa.search.DiscoveryResult.ResultType;
import com.here.android.mpa.search.DiscoveryResultPage;
import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.ExploreRequest;
import com.here.android.mpa.search.GeocodeRequest;
import com.here.android.mpa.search.Location;
import com.here.android.mpa.search.NavigationPosition;
import com.here.android.mpa.search.Place;
import com.here.android.mpa.search.PlaceLink;
import com.here.android.mpa.search.ResultListener;
import com.here.android.mpa.search.SearchRequest;
import com.here.android.mpa.search.TextSuggestionRequest;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapsEventHandler;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.places.AutoCompleteRequest;
import com.navdy.service.library.events.places.AutoCompleteResponse;
import com.navdy.service.library.events.places.PlacesSearchRequest;
import com.navdy.service.library.events.places.PlacesSearchResponse.Builder;
import com.navdy.service.library.events.places.PlacesSearchResult;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class HerePlacesManager {
    private static final ArrayList<String> EMPTY_AUTO_COMPLETE_RESULT = new ArrayList(0);
    private static final ArrayList<PlaceLink> EMPTY_RESULT = new ArrayList(0);
    public static final String HERE_PLACE_TYPE_ATM = "atm-bank-exchange";
    public static final String HERE_PLACE_TYPE_COFFEE = "coffee-tea";
    public static final String HERE_PLACE_TYPE_GAS = "petrol-station";
    public static final String HERE_PLACE_TYPE_HOSPITAL = "hospital-health-care-facility";
    public static final String HERE_PLACE_TYPE_PARKING = "parking-facility";
    public static final String[] HERE_PLACE_TYPE_RESTAURANT = new String[]{"restaurant", "snacks-fast-food"};
    public static final String HERE_PLACE_TYPE_STORE = "shopping";
    private static final int MAX_RESULTS_AUTOCOMPLETE = 10;
    private static final int MAX_RESULTS_SEARCH = 15;
    private static final Context context = HudApplication.getAppContext();
    private static final HereMapsManager hereMapsManager = HereMapsManager.getInstance();
    private static final Logger sLogger = new Logger(HerePlacesManager.class);
    private static volatile boolean willRestablishOnline;

    public interface OnCategoriesSearchListener {
        void onCompleted(List<Place> list);

        void onError(Error error);
    }

    public interface PlacesSearchCallback {
        void result(ErrorCode errorCode, DiscoveryResultPage discoveryResultPage);
    }

    public interface AutoCompleteCallback {
        void result(ErrorCode errorCode, List<String> list);
    }

    public enum Error {
        BAD_REQUEST,
        RESPONSE_ERROR,
        NO_USER_LOCATION,
        NO_MAP_ENGINE,
        NO_ROUTES,
        UNKNOWN_ERROR
    }

    public interface GeoCodeCallback {
        void result(ErrorCode errorCode, List<Location> list);
    }

    public static void searchPlaces(GeoCoordinate currentPosition, String queryText, int searchArea, int maxResults, final PlacesSearchCallback callBack) {
        SearchRequest request = new SearchRequest(queryText);
        request.setSearchCenter(currentPosition);
        request.setCollectionSize(maxResults);
        ErrorCode errorCode = request.execute(new ResultListener<DiscoveryResultPage>() {
            public void onCompleted(DiscoveryResultPage discoveryResultPage, ErrorCode errorCode) {
                try {
                    callBack.result(errorCode, discoveryResultPage);
                } catch (Throwable t) {
                    HerePlacesManager.sLogger.e(t);
                }
            }
        });
        if (errorCode != ErrorCode.NONE) {
            try {
                callBack.result(errorCode, null);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public static GeocodeRequest geoCodeStreetAddress(GeoCoordinate currentPosition, String queryText, int searchArea, final GeoCodeCallback callBack) {
        GeocodeRequest geocodeRequest = new GeocodeRequest(queryText);
        if (currentPosition != null) {
            geocodeRequest.setSearchArea(currentPosition, searchArea);
        }
        ErrorCode errorCode = geocodeRequest.execute(new ResultListener<List<Location>>() {
            public void onCompleted(List<Location> locations, ErrorCode errorCode) {
                try {
                    callBack.result(errorCode, locations);
                } catch (Throwable t) {
                    HerePlacesManager.sLogger.e(t);
                }
            }
        });
        if (errorCode == ErrorCode.NONE) {
            return geocodeRequest;
        }
        try {
            callBack.result(errorCode, null);
        } catch (Throwable t) {
            sLogger.e(t);
        }
        return null;
    }

    public static void autoComplete(GeoCoordinate currentPosition, String queryText, int searchArea, int maxResults, final AutoCompleteCallback callBack) {
        TextSuggestionRequest request = new TextSuggestionRequest(queryText);
        request.setCollectionSize(maxResults);
        request.setSearchCenter(currentPosition);
        ErrorCode errorCode = request.execute(new ResultListener<List<String>>() {
            public void onCompleted(List<String> results, ErrorCode errorCode) {
                try {
                    callBack.result(errorCode, results);
                } catch (Throwable t) {
                    HerePlacesManager.sLogger.e(t);
                }
            }
        });
        if (errorCode != ErrorCode.NONE) {
            try {
                callBack.result(errorCode, null);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public static void handlePlacesSearchRequest(final PlacesSearchRequest request) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                final long l1 = SystemClock.elapsedRealtime();
                final String searchQuery = request.searchQuery;
                final int searchArea = request.searchArea != null ? request.searchArea.intValue() : -1;
                int n = request.maxResults != null ? request.maxResults.intValue() : 15;
                if (n <= 0 || n > 15) {
                    n = 15;
                }
                final int maxResults = n;
                try {
                    HerePlacesManager.printSearchRequest(request);
                    if (!HerePlacesManager.hereMapsManager.isInitialized()) {
                        HerePlacesManager.sLogger.i("[search] engine not ready");
                        HerePlacesManager.returnSearchErrorResponse(searchQuery, RequestStatus.REQUEST_NOT_READY, HerePlacesManager.context.getString(R.string.map_engine_not_ready));
                        if (HerePlacesManager.sLogger.isLoggable(2)) {
                            HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-1 =" + (SystemClock.elapsedRealtime() - l1));
                        }
                    } else if (TextUtils.isEmpty(searchQuery)) {
                        HerePlacesManager.sLogger.i("[search] invalid search query");
                        HerePlacesManager.returnSearchErrorResponse(searchQuery, RequestStatus.REQUEST_INVALID_REQUEST, HerePlacesManager.context.getString(R.string.empty_search_query));
                        if (HerePlacesManager.sLogger.isLoggable(2)) {
                            HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-1 =" + (SystemClock.elapsedRealtime() - l1));
                        }
                    } else {
                        HereLocationFixManager hereLocationFixManager = HerePlacesManager.hereMapsManager.getLocationFixManager();
                        if (hereLocationFixManager == null) {
                            HerePlacesManager.sLogger.i("[search] engine not ready, location fix manager n/a");
                            HerePlacesManager.returnSearchErrorResponse(searchQuery, RequestStatus.REQUEST_NOT_READY, HerePlacesManager.context.getString(R.string.map_engine_not_ready));
                            if (HerePlacesManager.sLogger.isLoggable(2)) {
                                HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-1 =" + (SystemClock.elapsedRealtime() - l1));
                                return;
                            }
                            return;
                        }
                        final GeoCoordinate geoPosition = hereLocationFixManager.getLastGeoCoordinate();
                        if (geoPosition == null) {
                            HerePlacesManager.sLogger.i("start location n/a");
                            HerePlacesManager.returnSearchErrorResponse(searchQuery, RequestStatus.REQUEST_NO_LOCATION_SERVICE, HerePlacesManager.context.getString(R.string.no_current_position));
                            if (HerePlacesManager.sLogger.isLoggable(2)) {
                                HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-1 =" + (SystemClock.elapsedRealtime() - l1));
                                return;
                            }
                            return;
                        }
                        HerePlacesManager.searchPlaces(geoPosition, searchQuery, searchArea, maxResults, new PlacesSearchCallback() {
                            public void result(final ErrorCode errorCode, final DiscoveryResultPage result) {
                                TaskManager.getInstance().execute(new Runnable() {
                                    /* JADX WARNING: inconsistent code. */
                                    /* Code decompiled incorrectly, please refer to instructions dump. */
                                    public void run() {
                                        try {
                                            if (errorCode != ErrorCode.NONE) {
                                                HerePlacesManager.returnSearchErrorResponse(searchQuery, RequestStatus.REQUEST_SERVICE_ERROR, HerePlacesManager.context.getString(R.string.search_error, new Object[]{errorCode.toString()}));
                                                if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                    HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-2 =" + (SystemClock.elapsedRealtime() - l1));
                                                    return;
                                                }
                                                return;
                                            }
                                            if (result != null) {
                                                List<PlaceLink> links = result.getPlaceLinks();
                                                if (links == null || links.size() == 0) {
                                                    HerePlacesManager.sLogger.d("[search] no results");
                                                    HerePlacesManager.returnSearchResults(geoPosition, searchQuery, HerePlacesManager.EMPTY_RESULT, maxResults, searchArea);
                                                } else {
                                                    HerePlacesManager.sLogger.d("[search] returned results:" + links.size());
                                                    HerePlacesManager.returnSearchResults(geoPosition, searchQuery, links, maxResults, searchArea);
                                                }
                                            } else {
                                                HerePlacesManager.sLogger.d("[search] no results");
                                                HerePlacesManager.returnSearchResults(geoPosition, searchQuery, HerePlacesManager.EMPTY_RESULT, maxResults, searchArea);
                                            }
                                            if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-2 =" + (SystemClock.elapsedRealtime() - l1));
                                            }
                                        } catch (Throwable th) {
                                            if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-2 =" + (SystemClock.elapsedRealtime() - l1));
                                            }
                                        }
                                    }
                                }, 19);
                            }
                        });
                        if (HerePlacesManager.sLogger.isLoggable(2)) {
                            HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-1 =" + (SystemClock.elapsedRealtime() - l1));
                        }
                    }
                } catch (Throwable th) {
                    if (HerePlacesManager.sLogger.isLoggable(2)) {
                        HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-1 =" + (SystemClock.elapsedRealtime() - l1));
                    }
                }
            }
        }, 19);
    }

    public static void handleAutoCompleteRequest(final AutoCompleteRequest request) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                final long l1 = SystemClock.elapsedRealtime();
                final String autoCompleteSearch = request.partialSearch;
                int searchArea = request.searchArea != null ? request.searchArea.intValue() : -1;
                int n = request.maxResults != null ? request.maxResults.intValue() : 10;
                if (n <= 0 || n > 10) {
                    n = 10;
                }
                final int maxResults = n;
                try {
                    HerePlacesManager.printAutoCompleteRequest(request);
                    if (!HerePlacesManager.hereMapsManager.isInitialized()) {
                        HerePlacesManager.sLogger.i("engine not ready");
                        HerePlacesManager.returnAutoCompleteErrorResponse(autoCompleteSearch, RequestStatus.REQUEST_NOT_READY, HerePlacesManager.context.getString(R.string.map_engine_not_ready));
                        if (HerePlacesManager.sLogger.isLoggable(2)) {
                            HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-1 =" + (SystemClock.elapsedRealtime() - l1));
                        }
                    } else if (TextUtils.isEmpty(autoCompleteSearch)) {
                        HerePlacesManager.sLogger.i("invalid autocomplete query");
                        HerePlacesManager.returnAutoCompleteErrorResponse(autoCompleteSearch, RequestStatus.REQUEST_INVALID_REQUEST, HerePlacesManager.context.getString(R.string.empty_search_query));
                        if (HerePlacesManager.sLogger.isLoggable(2)) {
                            HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-1 =" + (SystemClock.elapsedRealtime() - l1));
                        }
                    } else {
                        HereLocationFixManager hereLocationFixManager = HerePlacesManager.hereMapsManager.getLocationFixManager();
                        if (hereLocationFixManager == null) {
                            HerePlacesManager.sLogger.i("engine not ready, location fix manager n/a");
                            HerePlacesManager.returnAutoCompleteErrorResponse(autoCompleteSearch, RequestStatus.REQUEST_NOT_READY, HerePlacesManager.context.getString(R.string.map_engine_not_ready));
                            if (HerePlacesManager.sLogger.isLoggable(2)) {
                                HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-1 =" + (SystemClock.elapsedRealtime() - l1));
                                return;
                            }
                            return;
                        }
                        GeoCoordinate geoPosition = hereLocationFixManager.getLastGeoCoordinate();
                        if (geoPosition == null) {
                            HerePlacesManager.sLogger.i("start location n/a");
                            HerePlacesManager.returnAutoCompleteErrorResponse(autoCompleteSearch, RequestStatus.REQUEST_NO_LOCATION_SERVICE, HerePlacesManager.context.getString(R.string.no_current_position));
                            if (HerePlacesManager.sLogger.isLoggable(2)) {
                                HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-1 =" + (SystemClock.elapsedRealtime() - l1));
                                return;
                            }
                            return;
                        }
                        HerePlacesManager.autoComplete(geoPosition, autoCompleteSearch, searchArea, maxResults, new AutoCompleteCallback() {
                            public void result(final ErrorCode errorCode, final List<String> result) {
                                TaskManager.getInstance().execute(new Runnable() {
                                    /* JADX WARNING: inconsistent code. */
                                    /* Code decompiled incorrectly, please refer to instructions dump. */
                                    public void run() {
                                        try {
                                            if (errorCode != ErrorCode.NONE) {
                                                HerePlacesManager.returnAutoCompleteErrorResponse(autoCompleteSearch, RequestStatus.REQUEST_SERVICE_ERROR, HerePlacesManager.context.getString(R.string.search_error, new Object[]{errorCode.toString()}));
                                                if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                    HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-2 =" + (SystemClock.elapsedRealtime() - l1));
                                                    return;
                                                }
                                                return;
                                            }
                                            if (result == null || result.size() == 0) {
                                                HerePlacesManager.sLogger.d("no results");
                                                HerePlacesManager.returnAutoCompleteResults(autoCompleteSearch, maxResults, HerePlacesManager.EMPTY_AUTO_COMPLETE_RESULT);
                                            } else {
                                                HerePlacesManager.sLogger.d("returned results:" + result.size());
                                                HerePlacesManager.returnAutoCompleteResults(autoCompleteSearch, maxResults, result);
                                            }
                                            if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-2 =" + (SystemClock.elapsedRealtime() - l1));
                                            }
                                        } catch (Throwable th) {
                                            if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-2 =" + (SystemClock.elapsedRealtime() - l1));
                                            }
                                        }
                                    }
                                }, 19);
                            }
                        });
                        if (HerePlacesManager.sLogger.isLoggable(2)) {
                            HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-1 =" + (SystemClock.elapsedRealtime() - l1));
                        }
                    }
                } catch (Throwable th) {
                    if (HerePlacesManager.sLogger.isLoggable(2)) {
                        HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-1 =" + (SystemClock.elapsedRealtime() - l1));
                    }
                }
            }
        }, 19);
    }

    public static void handleCategoriesRequest(final CategoryFilter filter, final int nResults, final OnCategoriesSearchListener listener) {
        handleCategoriesRequest(filter, nResults, new OnCategoriesSearchListener() {
            public void onCompleted(List<Place> places) {
                listener.onCompleted(places);
            }

            public void onError(Error error) {
                HerePlacesManager.handleCategoriesRequest(filter, nResults, new OnCategoriesSearchListener() {
                    public void onCompleted(List<Place> places) {
                        listener.onCompleted(places);
                    }

                    public void onError(Error error) {
                        listener.onError(error);
                    }
                }, true);
            }
        }, false);
    }

    public static void handleCategoriesRequest(final CategoryFilter filter, final int nResults, final OnCategoriesSearchListener listener, final boolean offline) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                final long l1 = SystemClock.elapsedRealtime();
                if (filter == null || nResults <= 0 || listener == null) {
                    throw new IllegalArgumentException();
                }
                try {
                    if (HerePlacesManager.hereMapsManager.isInitialized()) {
                        HereLocationFixManager hereLocationFixManager = HerePlacesManager.hereMapsManager.getLocationFixManager();
                        if (hereLocationFixManager == null) {
                            HerePlacesManager.sLogger.i("engine not ready, location fix manager n/a");
                            listener.onError(Error.NO_USER_LOCATION);
                            HerePlacesManager.sLogger.v("handleCategoriesRequest time-1=" + (SystemClock.elapsedRealtime() - l1));
                            return;
                        }
                        GeoCoordinate geoPosition = HerePlacesManager.hereMapsManager.getRouteStartPoint();
                        if (geoPosition == null) {
                            geoPosition = hereLocationFixManager.getLastGeoCoordinate();
                        } else {
                            HerePlacesManager.sLogger.v("using debug start point:" + geoPosition);
                        }
                        if (geoPosition == null) {
                            HerePlacesManager.sLogger.i("start location n/a");
                            listener.onError(Error.NO_USER_LOCATION);
                            HerePlacesManager.sLogger.v("handleCategoriesRequest time-1=" + (SystemClock.elapsedRealtime() - l1));
                            return;
                        }
                        if (offline) {
                            HerePlacesManager.establishOffline();
                        } else if (!HereMapsManager.getInstance().isEngineOnline()) {
                            listener.onError(Error.BAD_REQUEST);
                            HerePlacesManager.sLogger.v("handleCategoriesRequest time-1=" + (SystemClock.elapsedRealtime() - l1));
                            return;
                        }
                        ErrorCode error = new ExploreRequest().setCategoryFilter(filter).setSearchCenter(geoPosition).setCollectionSize(nResults).execute(new ResultListener<DiscoveryResultPage>() {
                            public void onCompleted(final DiscoveryResultPage results, final ErrorCode error) {
                                TaskManager.getInstance().execute(new Runnable() {
                                    public void run() {
                                        try {
                                            if (error != ErrorCode.NONE) {
                                                HerePlacesManager.sLogger.e("Error in nearby categories response: " + error.name());
                                                HerePlacesManager.tryRestablishOnline();
                                                listener.onError(Error.RESPONSE_ERROR);
                                                return;
                                            }
                                            List<DiscoveryResult> discoveryResults = results.getItems();
                                            List<PlaceLink> placeLinks = new ArrayList();
                                            for (DiscoveryResult discoveryResult : discoveryResults) {
                                                if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                    HerePlacesManager.sLogger.v("Found a nearby category item: " + discoveryResult.getTitle());
                                                }
                                                if (discoveryResult.getResultType() == ResultType.PLACE) {
                                                    HerePlacesManager.sLogger.v("Discovered a place: " + discoveryResult.getTitle());
                                                    placeLinks.add((PlaceLink) discoveryResult);
                                                }
                                            }
                                            if (placeLinks.size() == 0) {
                                                HerePlacesManager.sLogger.e("no places links found");
                                                HerePlacesManager.tryRestablishOnline();
                                                listener.onError(Error.RESPONSE_ERROR);
                                                HerePlacesManager.sLogger.v("handleCategoriesRequest- time-2 =" + (SystemClock.elapsedRealtime() - l1));
                                                return;
                                            }
                                            HerePlacesManager.handlePlaceLinksRequest(placeLinks, listener);
                                            HerePlacesManager.sLogger.v("handleCategoriesRequest- time-2 =" + (SystemClock.elapsedRealtime() - l1));
                                        } catch (Throwable t) {
                                            HerePlacesManager.sLogger.e("HERE internal DiscoveryRequest.execute callback exception: ", t);
                                            HerePlacesManager.tryRestablishOnline();
                                            listener.onError(Error.UNKNOWN_ERROR);
                                        } finally {
                                            HerePlacesManager.sLogger.v("handleCategoriesRequest- time-2 =" + (SystemClock.elapsedRealtime() - l1));
                                        }
                                    }
                                }, 2);
                            }
                        });
                        if (error != ErrorCode.NONE) {
                            HerePlacesManager.sLogger.e("Error while requesting nearby gas stations: " + error.name());
                            HerePlacesManager.tryRestablishOnline();
                            listener.onError(Error.BAD_REQUEST);
                        }
                        HerePlacesManager.sLogger.v("handleCategoriesRequest time-1=" + (SystemClock.elapsedRealtime() - l1));
                        return;
                    }
                    HerePlacesManager.sLogger.i("engine not ready");
                    listener.onError(Error.NO_MAP_ENGINE);
                } catch (Throwable t) {
                    HerePlacesManager.sLogger.e("HERE internal DiscoveryRequest.execute exception: ", t);
                    HerePlacesManager.tryRestablishOnline();
                    listener.onError(Error.UNKNOWN_ERROR);
                } finally {
                    HerePlacesManager.sLogger.v("handleCategoriesRequest time-1=" + (SystemClock.elapsedRealtime() - l1));
                }
            }
        }, 2);
    }

    private static void handlePlaceLinksRequest(List<PlaceLink> placeLinks, final OnCategoriesSearchListener listener) {
        final List<Place> places = new ArrayList();
        final AtomicInteger counter = new AtomicInteger(placeLinks.size());
        for (PlaceLink placeLink : placeLinks) {
            ErrorCode error = placeLink.getDetailsRequest().execute(new ResultListener<Place>() {
                public void onCompleted(final Place place, final ErrorCode error) {
                    TaskManager.getInstance().execute(new Runnable() {
                        public void run() {
                            try {
                                int left = counter.decrementAndGet();
                                if (error != ErrorCode.NONE || place == null) {
                                    HerePlacesManager.sLogger.e("Error in place detail response: " + error.name());
                                } else {
                                    HerePlacesManager.sLogger.v("Found a Place: " + place.getName());
                                    for (Category category : place.getCategories()) {
                                        HerePlacesManager.sLogger.v("place category: " + category.getName());
                                    }
                                    places.add(place);
                                }
                                if (left == 0) {
                                    HerePlacesManager.invokeCategorySearchListener(places, listener);
                                }
                            } catch (Throwable t) {
                                HerePlacesManager.sLogger.e("HERE internal PlaceLink.getDetailsRequest callback exception: ", t);
                                if (counter.get() == 0) {
                                    HerePlacesManager.invokeCategorySearchListener(places, listener);
                                }
                            }
                        }
                    }, 2);
                }
            });
            if (error != ErrorCode.NONE) {
                sLogger.e("Error in place detail request: " + error.name());
                if (counter.decrementAndGet() == 0 && places.size() == 0) {
                    tryRestablishOnline();
                    listener.onError(Error.BAD_REQUEST);
                }
            }
        }
    }

    private static void invokeCategorySearchListener(List<Place> places, OnCategoriesSearchListener listener) {
        int size = places.size();
        if (size == 0) {
            sLogger.v("places query complete, no result");
            tryRestablishOnline();
            listener.onError(Error.RESPONSE_ERROR);
            return;
        }
        sLogger.v("places query complete:" + size);
        tryRestablishOnline();
        listener.onCompleted(places);
    }

    private static void establishOffline() {
        willRestablishOnline = HereMapsManager.getInstance().isEngineOnline();
        if (willRestablishOnline) {
            sLogger.v("turn engine offline");
            HereMapsManager.getInstance().turnOffline();
        }
    }

    private static void tryRestablishOnline() {
        if (willRestablishOnline && !HereMapsManager.getInstance().isEngineOnline() && RemoteDeviceManager.getInstance().isRemoteDeviceConnected() && NetworkStateManager.isConnectedToNetwork(HudApplication.getAppContext())) {
            sLogger.v("turn engine online");
            HereMapsManager.getInstance().turnOnline();
        }
        willRestablishOnline = false;
    }

    private static void returnSearchErrorResponse(String searchQuery, RequestStatus status, String errorText) {
        try {
            sLogger.e("[search]" + status + ": " + errorText);
            MapsEventHandler.getInstance().sendEventToClient(new Builder().searchQuery(searchQuery).status(status).statusDetail(errorText).results(null).build());
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    private static void returnSearchResults(GeoCoordinate currentLocation, String queryText, List<PlaceLink> destinationLinks, int maxResults, int searchArea) {
        final int i = maxResults;
        final List<PlaceLink> list = destinationLinks;
        final GeoCoordinate geoCoordinate = currentLocation;
        final int i2 = searchArea;
        final String str = queryText;
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                int count = 0;
                try {
                    ArrayList<PlacesSearchResult> arrayList = new ArrayList(i);
                    for (PlaceLink placeLink : list) {
                        GeoCoordinate placesLocationCoordinate = placeLink.getPosition();
                        if (placesLocationCoordinate != null && (geoCoordinate == null || i2 <= 0 || geoCoordinate.distanceTo(placesLocationCoordinate) <= ((double) i2))) {
                            Coordinate coordinate = new Coordinate.Builder().latitude(Double.valueOf(placesLocationCoordinate.getLatitude())).longitude(Double.valueOf(placesLocationCoordinate.getLongitude())).build();
                            String address = placeLink.getVicinity();
                            String title = placeLink.getTitle();
                            String categoryStr = null;
                            double distance = placeLink.getDistance();
                            if (address != null) {
                                address = address.replace("<br/>", ", ");
                            }
                            Category category = placeLink.getCategory();
                            if (category != null) {
                                categoryStr = category.getName();
                            }
                            arrayList.add(new PlacesSearchResult(title, null, coordinate, address, categoryStr, Double.valueOf(distance)));
                            count++;
                            if (count == i) {
                                HerePlacesManager.sLogger.v("[search] max results reached:" + i);
                                break;
                            }
                        }
                    }
                    MapsEventHandler.getInstance().sendEventToClient(new Builder().searchQuery(str).status(RequestStatus.REQUEST_SUCCESS).statusDetail(null).results(arrayList).build());
                    HerePlacesManager.sLogger.v("[search] returned:" + arrayList.size());
                } catch (Throwable t) {
                    HerePlacesManager.sLogger.e(t);
                    HerePlacesManager.returnSearchErrorResponse(str, RequestStatus.REQUEST_UNKNOWN_ERROR, HerePlacesManager.context.getString(R.string.unknown_error));
                }
            }
        }, 2);
    }

    private static void returnAutoCompleteErrorResponse(String partialSearch, RequestStatus status, String errorText) {
        try {
            sLogger.e("[autocomplete]" + status + ": " + errorText);
            MapsEventHandler.getInstance().sendEventToClient(new AutoCompleteResponse(partialSearch, status, errorText, null));
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    private static void returnAutoCompleteResults(final String autoCompleteSearch, final int maxResults, final List<String> results) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    List<String> ret = results;
                    if (results.size() > maxResults) {
                        ret = results.subList(0, maxResults);
                    }
                    MapsEventHandler.getInstance().sendEventToClient(new AutoCompleteResponse(autoCompleteSearch, RequestStatus.REQUEST_SUCCESS, null, ret));
                    HerePlacesManager.sLogger.v("[autocomplete] returned:" + ret.size());
                } catch (Throwable t) {
                    HerePlacesManager.sLogger.e(t);
                    HerePlacesManager.returnSearchErrorResponse(autoCompleteSearch, RequestStatus.REQUEST_UNKNOWN_ERROR, HerePlacesManager.context.getString(R.string.unknown_error));
                }
            }
        }, 2);
    }

    public static void printSearchRequest(PlacesSearchRequest request) {
        try {
            sLogger.v("PlacesSearchRequest query[" + request.searchQuery + "] area[" + request.searchArea + "] maxResults[" + request.maxResults + "]");
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public static void printAutoCompleteRequest(AutoCompleteRequest request) {
        try {
            sLogger.v("AutoCompleteRequest query[" + request.partialSearch + "] maxResults[" + request.maxResults + "]");
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public static GeoCoordinate getPlaceEntry(Place place) {
        if (place.getLocation().getAccessPoints().size() > 0) {
            return ((NavigationPosition) place.getLocation().getAccessPoints().get(0)).getCoordinate();
        }
        return place.getLocation().getCoordinate();
    }
}
