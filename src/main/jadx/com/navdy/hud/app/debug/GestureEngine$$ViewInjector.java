package com.navdy.hud.app.debug;

import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class GestureEngine$$ViewInjector {
    public static void inject(Finder finder, final GestureEngine target, Object source) {
        ((CompoundButton) finder.findRequiredView(source, R.id.enable_gesture, "method 'onToggleGesture'")).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton p0, boolean p1) {
                target.onToggleGesture(p1);
            }
        });
        ((CompoundButton) finder.findRequiredView(source, R.id.enable_discrete_mode, "method 'onToggleDiscreteMode'")).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton p0, boolean p1) {
                target.onToggleDiscreteMode(p1);
            }
        });
        ((CompoundButton) finder.findRequiredView(source, R.id.enable_preview, "method 'onTogglePreview'")).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton p0, boolean p1) {
                target.onTogglePreview(p1);
            }
        });
    }

    public static void reset(GestureEngine target) {
    }
}
