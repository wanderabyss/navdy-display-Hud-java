package com.navdy.hud.app.debug;

public interface SerialExecutor {
    void execute(Runnable runnable);
}
