package com.navdy.hud.app.debug;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.view.KeyEvent;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated;
import com.navdy.hud.app.analytics.TelemetrySession;
import com.navdy.hud.app.debug.IRouteRecorder.Stub;
import com.navdy.hud.app.device.gps.CalibratedGForceData;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.MapEvents.MapEngineInitialize;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.obd.IPidListener;
import com.navdy.obd.Pid;
import com.navdy.obd.PidSet;
import com.navdy.service.library.events.debug.StartDrivePlaybackEvent;
import com.navdy.service.library.events.debug.StartDriveRecordingEvent;
import com.navdy.service.library.events.debug.StopDrivePlaybackEvent;
import com.navdy.service.library.events.debug.StopDriveRecordingEvent;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;

public class DriveRecorder implements ServiceConnection, SerialExecutor {
    private static final String AUTO_DRIVERECORD_PROP = "persist.sys.driverecord.auto";
    public static final String AUTO_RECORD_LABEL = "auto";
    public static final int BUFFER_SIZE = 500;
    public static final int BUFFER_SIZE_OBD_LOG = 250;
    public static final String DEMO_LOG_FILE_NAME = "demo_log.log";
    public static final String DRIVE_LOGS_FOLDER = "drive_logs";
    public static final int MAXIMUM_FILES_IN_THE_FOLDER = 20;
    public static final Logger sLogger = new Logger(DriveRecorder.class);
    Bus bus;
    private ConnectionHandler connectionHandler;
    private final Context context = HudApplication.getAppContext();
    private volatile int currentObdDataInjectionIndex;
    boolean demoObdLogFileExists = false;
    boolean demoRouteLogFileExists = false;
    AsyncBufferedFileWriter driveScoreDataBufferedFileWriter;
    private StringBuilder gforceDataBuilder = new StringBuilder(40);
    private final Handler handler = new Handler(Looper.getMainLooper());
    private final Runnable injectFakeObdData = new Runnable() {
        public void run() {
            if (DriveRecorder.this.recordedObdData.size() > DriveRecorder.this.currentObdDataInjectionIndex + 1) {
                Entry<Long, List<Pid>> data = (Entry) DriveRecorder.this.recordedObdData.get(DriveRecorder.this.currentObdDataInjectionIndex);
                long timeStamp = ((Long) data.getKey()).longValue();
                ObdManager.getInstance().injectObdData((List) data.getValue(), (List) data.getValue());
                if (DriveRecorder.this.recordedObdData.size() > DriveRecorder.this.currentObdDataInjectionIndex + 1) {
                    DriveRecorder.this.obdDataPlaybackHandler.postDelayed(DriveRecorder.this.injectFakeObdData, ((Long) ((Entry) DriveRecorder.this.recordedObdData.get(DriveRecorder.access$204(DriveRecorder.this))).getKey()).longValue() - timeStamp);
                }
            } else if (DriveRecorder.this.isLooping) {
                DriveRecorder.this.currentObdDataInjectionIndex = 0;
                DriveRecorder.this.obdDataPlaybackHandler.post(DriveRecorder.this.injectFakeObdData);
            } else {
                DriveRecorder.sLogger.d("Done playing the recorded data. Stopping the playback");
                DriveRecorder.this.stopPlayback();
            }
        }
    };
    private boolean isEngineInitialized;
    private volatile boolean isLooping = false;
    private volatile boolean isRecording;
    private volatile boolean isSupportedPidsWritten;
    AsyncBufferedFileWriter obdAsyncBufferedFileWriter;
    private Handler obdDataPlaybackHandler;
    private HandlerThread obdPlaybackThread;
    private final IPidListener pidListener = new IPidListener() {
        public void pidsRead(List<Pid> list, List<Pid> pidsChanged) throws RemoteException {
            if (pidsChanged != null && DriveRecorder.this.isRecording) {
                if (!DriveRecorder.this.isSupportedPidsWritten) {
                    ObdManager.getInstance().setRecordingPidListener(null);
                    try {
                        DriveRecorder.this.bRecordSupportedPids();
                    } catch (Throwable th) {
                        DriveRecorder.sLogger.d("Error while recording the supported PIDs");
                    }
                    ObdManager.getInstance().setRecordingPidListener(DriveRecorder.this.pidListener);
                }
                DriveRecorder.this.obdAsyncBufferedFileWriter.write(System.currentTimeMillis() + HereManeuverDisplayBuilder.COMMA);
                for (Pid pid : pidsChanged) {
                    DriveRecorder.this.obdAsyncBufferedFileWriter.write(pid.getId() + GlanceConstants.COLON_SEPARATOR + pid.getValue());
                    if (pid != pidsChanged.get(pidsChanged.size() - 1)) {
                        DriveRecorder.this.obdAsyncBufferedFileWriter.write(HereManeuverDisplayBuilder.COMMA);
                    } else {
                        DriveRecorder.this.obdAsyncBufferedFileWriter.write(GlanceConstants.NEWLINE);
                    }
                }
            }
        }

        public void pidsChanged(List<Pid> list) throws RemoteException {
        }

        public void onConnectionStateChange(int newState) throws RemoteException {
        }

        public IBinder asBinder() {
            return null;
        }
    };
    private volatile State playbackState = State.STOPPED;
    private volatile long preparedFileLastModifiedTime;
    private volatile String preparedFileName;
    volatile boolean realObdConnected = false;
    private List<Entry<Long, List<Pid>>> recordedObdData;
    private IRouteRecorder routeRecorder;

    public enum Action {
        PRELOAD,
        PLAY,
        PAUSE,
        RESUME,
        RESTART,
        STOP
    }

    public enum DemoPreference {
        NA,
        DISABLED,
        ENABLED
    }

    public static class FilesModifiedTimeComparator implements Comparator<File> {
        public int compare(File file, File t1) {
            return Long.compare(file.lastModified(), t1.lastModified());
        }
    }

    enum State {
        PLAYING,
        PAUSED,
        STOPPED
    }

    static /* synthetic */ int access$204(DriveRecorder x0) {
        int i = x0.currentObdDataInjectionIndex + 1;
        x0.currentObdDataInjectionIndex = i;
        return i;
    }

    public void onServiceConnected(ComponentName name, IBinder service) {
        this.routeRecorder = Stub.asInterface(service);
    }

    public void onServiceDisconnected(ComponentName name) {
        this.routeRecorder = null;
    }

    public void execute(Runnable runnable) {
        TaskManager.getInstance().execute(runnable, 9);
    }

    public void stopRecording() {
        if (this.isRecording) {
            sLogger.d("Stopping the Obd recording");
            this.handler.post(new Runnable() {
                public void run() {
                    DriveRecorder.this.isRecording = false;
                    DriveRecorder.this.isSupportedPidsWritten = false;
                    ObdManager.getInstance().setRecordingPidListener(null);
                    DriveRecorder.this.obdAsyncBufferedFileWriter.flushAndClose();
                    DriveRecorder.this.driveScoreDataBufferedFileWriter.flushAndClose();
                }
            });
            return;
        }
        sLogger.v("already stopped, no-op");
    }

    public boolean bPrepare(java.lang.String r29) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Unknown predecessor block by arg (r16_1 'reader' java.io.BufferedReader) in PHI: PHI: (r16_4 'reader' java.io.BufferedReader) = (r16_0 'reader' java.io.BufferedReader), (r16_0 'reader' java.io.BufferedReader), (r16_1 'reader' java.io.BufferedReader), (r16_1 'reader' java.io.BufferedReader), (r16_1 'reader' java.io.BufferedReader), (r16_1 'reader' java.io.BufferedReader), (r16_1 'reader' java.io.BufferedReader), (r16_1 'reader' java.io.BufferedReader) binds: {(r16_0 'reader' java.io.BufferedReader)=B:9:0x005f, (r16_0 'reader' java.io.BufferedReader)=B:10:?, (r16_1 'reader' java.io.BufferedReader)=B:13:0x009f, (r16_1 'reader' java.io.BufferedReader)=B:34:0x0150, (r16_1 'reader' java.io.BufferedReader)=B:69:0x017a, (r16_1 'reader' java.io.BufferedReader)=B:71:0x017a, (r16_1 'reader' java.io.BufferedReader)=B:62:0x017a, (r16_1 'reader' java.io.BufferedReader)=B:64:0x017a}
	at jadx.core.dex.instructions.PhiInsn.replaceArg(PhiInsn.java:78)
	at jadx.core.dex.visitors.ModVisitor.processInvoke(ModVisitor.java:222)
	at jadx.core.dex.visitors.ModVisitor.replaceStep(ModVisitor.java:83)
	at jadx.core.dex.visitors.ModVisitor.visit(ModVisitor.java:68)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
	at java.util.ArrayList.forEach(Unknown Source)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:34)
	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:59)
	at java.lang.Iterable.forEach(Unknown Source)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:39)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:282)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:200)
*/
        /*
        r28 = this;
        r2 = getDriveLogsDir(r29);
        r11 = new java.io.File;
        r24 = new java.lang.StringBuilder;
        r24.<init>();
        r0 = r24;
        r1 = r29;
        r24 = r0.append(r1);
        r25 = ".obd";
        r24 = r24.append(r25);
        r24 = r24.toString();
        r0 = r24;
        r11.<init>(r2, r0);
        r24 = r11.exists();
        if (r24 != 0) goto L_0x002b;
    L_0x0028:
        r24 = 0;
    L_0x002a:
        return r24;
    L_0x002b:
        r6 = r11.lastModified();
        r0 = r28;
        r0 = r0.preparedFileName;
        r24 = r0;
        r0 = r24;
        r1 = r29;
        r24 = android.text.TextUtils.equals(r0, r1);
        if (r24 == 0) goto L_0x0053;
    L_0x003f:
        r0 = r28;
        r0 = r0.preparedFileLastModifiedTime;
        r24 = r0;
        r24 = (r24 > r6 ? 1 : (r24 == r6 ? 0 : -1));
        if (r24 != 0) goto L_0x0053;
    L_0x0049:
        r24 = sLogger;
        r25 = "File already prepared";
        r24.d(r25);
        r24 = 1;
        goto L_0x002a;
    L_0x0053:
        r0 = r29;
        r1 = r28;
        r1.preparedFileName = r0;
        r0 = r28;
        r0.preparedFileLastModifiedTime = r6;
        r16 = 0;
        r17 = new java.io.BufferedReader;	 Catch:{ Throwable -> 0x017a }
        r24 = new java.io.InputStreamReader;	 Catch:{ Throwable -> 0x017a }
        r25 = new java.io.FileInputStream;	 Catch:{ Throwable -> 0x017a }
        r0 = r25;	 Catch:{ Throwable -> 0x017a }
        r0.<init>(r11);	 Catch:{ Throwable -> 0x017a }
        r26 = "utf-8";	 Catch:{ Throwable -> 0x017a }
        r24.<init>(r25, r26);	 Catch:{ Throwable -> 0x017a }
        r0 = r17;	 Catch:{ Throwable -> 0x017a }
        r1 = r24;	 Catch:{ Throwable -> 0x017a }
        r0.<init>(r1);	 Catch:{ Throwable -> 0x017a }
        r24 = sLogger;	 Catch:{ Throwable -> 0x01ac, all -> 0x01a8 }
        r25 = "startPlayback of Obd data";	 Catch:{ Throwable -> 0x01ac, all -> 0x01a8 }
        r24.v(r25);	 Catch:{ Throwable -> 0x01ac, all -> 0x01a8 }
        r24 = new java.util.ArrayList;	 Catch:{ Throwable -> 0x01ac, all -> 0x01a8 }
        r24.<init>();	 Catch:{ Throwable -> 0x01ac, all -> 0x01a8 }
        r0 = r24;	 Catch:{ Throwable -> 0x01ac, all -> 0x01a8 }
        r1 = r28;	 Catch:{ Throwable -> 0x01ac, all -> 0x01a8 }
        r1.recordedObdData = r0;	 Catch:{ Throwable -> 0x01ac, all -> 0x01a8 }
        r16 = new java.io.BufferedReader;	 Catch:{ Throwable -> 0x01ac, all -> 0x01a8 }
        r24 = new java.io.InputStreamReader;	 Catch:{ Throwable -> 0x01ac, all -> 0x01a8 }
        r25 = new java.io.FileInputStream;	 Catch:{ Throwable -> 0x01ac, all -> 0x01a8 }
        r0 = r25;	 Catch:{ Throwable -> 0x01ac, all -> 0x01a8 }
        r0.<init>(r11);	 Catch:{ Throwable -> 0x01ac, all -> 0x01a8 }
        r26 = "utf-8";	 Catch:{ Throwable -> 0x01ac, all -> 0x01a8 }
        r24.<init>(r25, r26);	 Catch:{ Throwable -> 0x01ac, all -> 0x01a8 }
        r0 = r16;	 Catch:{ Throwable -> 0x01ac, all -> 0x01a8 }
        r1 = r24;	 Catch:{ Throwable -> 0x01ac, all -> 0x01a8 }
        r0.<init>(r1);	 Catch:{ Throwable -> 0x01ac, all -> 0x01a8 }
        r8 = r16.readLine();	 Catch:{ Throwable -> 0x017a }
        r18 = new com.navdy.obd.PidSet;	 Catch:{ Throwable -> 0x017a }
        r18.<init>();	 Catch:{ Throwable -> 0x017a }
        if (r8 == 0) goto L_0x00d7;	 Catch:{ Throwable -> 0x017a }
    L_0x00aa:
        r24 = sLogger;	 Catch:{ Throwable -> 0x017a }
        r25 = new java.lang.StringBuilder;	 Catch:{ Throwable -> 0x017a }
        r25.<init>();	 Catch:{ Throwable -> 0x017a }
        r26 = "Supported pids recorded ";	 Catch:{ Throwable -> 0x017a }
        r25 = r25.append(r26);	 Catch:{ Throwable -> 0x017a }
        r0 = r25;	 Catch:{ Throwable -> 0x017a }
        r25 = r0.append(r8);	 Catch:{ Throwable -> 0x017a }
        r25 = r25.toString();	 Catch:{ Throwable -> 0x017a }
        r24.d(r25);	 Catch:{ Throwable -> 0x017a }
        r24 = r8.trim();	 Catch:{ Throwable -> 0x017a }
        r25 = "-1";	 Catch:{ Throwable -> 0x017a }
        r24 = r24.equals(r25);	 Catch:{ Throwable -> 0x017a }
        if (r24 == 0) goto L_0x0150;	 Catch:{ Throwable -> 0x017a }
    L_0x00d0:
        r24 = sLogger;	 Catch:{ Throwable -> 0x017a }
        r25 = "There are no supported pids when trying to playback the data";	 Catch:{ Throwable -> 0x017a }
        r24.d(r25);	 Catch:{ Throwable -> 0x017a }
    L_0x00d7:
        r24 = com.navdy.hud.app.obd.ObdManager.getInstance();	 Catch:{ Throwable -> 0x017a }
        r0 = r24;	 Catch:{ Throwable -> 0x017a }
        r1 = r18;	 Catch:{ Throwable -> 0x017a }
        r0.setSupportedPidSet(r1);	 Catch:{ Throwable -> 0x017a }
    L_0x00e2:
        r8 = r16.readLine();	 Catch:{ Throwable -> 0x017a }
        if (r8 == 0) goto L_0x01a1;	 Catch:{ Throwable -> 0x017a }
    L_0x00e8:
        r24 = ",";	 Catch:{ Throwable -> 0x017a }
        r0 = r24;	 Catch:{ Throwable -> 0x017a }
        r9 = r8.split(r0);	 Catch:{ Throwable -> 0x017a }
        if (r9 == 0) goto L_0x00e2;	 Catch:{ Throwable -> 0x017a }
    L_0x00f2:
        r0 = r9.length;	 Catch:{ Throwable -> 0x017a }
        r24 = r0;	 Catch:{ Throwable -> 0x017a }
        r25 = 1;
        r0 = r24;
        r1 = r25;
        if (r0 <= r1) goto L_0x00e2;
    L_0x00fd:
        r24 = 0;
        r24 = r9[r24];	 Catch:{ NumberFormatException -> 0x018e }
        r20 = java.lang.Long.parseLong(r24);	 Catch:{ NumberFormatException -> 0x018e }
        r15 = new java.util.ArrayList;	 Catch:{ NumberFormatException -> 0x018e }
        r15.<init>();	 Catch:{ NumberFormatException -> 0x018e }
        r4 = 1;	 Catch:{ NumberFormatException -> 0x018e }
    L_0x010b:
        r0 = r9.length;	 Catch:{ NumberFormatException -> 0x018e }
        r24 = r0;	 Catch:{ NumberFormatException -> 0x018e }
        r0 = r24;	 Catch:{ NumberFormatException -> 0x018e }
        if (r4 >= r0) goto L_0x00e2;	 Catch:{ NumberFormatException -> 0x018e }
    L_0x0112:
        r24 = r9[r4];	 Catch:{ NumberFormatException -> 0x018e }
        r25 = ":";	 Catch:{ NumberFormatException -> 0x018e }
        r13 = r24.split(r25);	 Catch:{ NumberFormatException -> 0x018e }
        r24 = 0;	 Catch:{ NumberFormatException -> 0x018e }
        r24 = r13[r24];	 Catch:{ NumberFormatException -> 0x018e }
        r5 = java.lang.Integer.parseInt(r24);	 Catch:{ NumberFormatException -> 0x018e }
        r24 = 1;	 Catch:{ NumberFormatException -> 0x018e }
        r24 = r13[r24];	 Catch:{ NumberFormatException -> 0x018e }
        r22 = java.lang.Double.parseDouble(r24);	 Catch:{ NumberFormatException -> 0x018e }
        r12 = new com.navdy.obd.Pid;	 Catch:{ NumberFormatException -> 0x018e }
        r12.<init>(r5);	 Catch:{ NumberFormatException -> 0x018e }
        r0 = r22;	 Catch:{ NumberFormatException -> 0x018e }
        r12.setValue(r0);	 Catch:{ NumberFormatException -> 0x018e }
        r15.add(r12);	 Catch:{ NumberFormatException -> 0x018e }
        r0 = r28;	 Catch:{ NumberFormatException -> 0x018e }
        r0 = r0.recordedObdData;	 Catch:{ NumberFormatException -> 0x018e }
        r24 = r0;	 Catch:{ NumberFormatException -> 0x018e }
        r25 = new java.util.AbstractMap$SimpleEntry;	 Catch:{ NumberFormatException -> 0x018e }
        r26 = java.lang.Long.valueOf(r20);	 Catch:{ NumberFormatException -> 0x018e }
        r0 = r25;	 Catch:{ NumberFormatException -> 0x018e }
        r1 = r26;	 Catch:{ NumberFormatException -> 0x018e }
        r0.<init>(r1, r15);	 Catch:{ NumberFormatException -> 0x018e }
        r24.add(r25);	 Catch:{ NumberFormatException -> 0x018e }
        r4 = r4 + 1;
        goto L_0x010b;
    L_0x0150:
        r24 = ",";	 Catch:{ Throwable -> 0x017a }
        r0 = r24;	 Catch:{ Throwable -> 0x017a }
        r14 = r8.split(r0);	 Catch:{ Throwable -> 0x017a }
        r0 = r14.length;	 Catch:{ Throwable -> 0x017a }
        r25 = r0;	 Catch:{ Throwable -> 0x017a }
        r24 = 0;	 Catch:{ Throwable -> 0x017a }
    L_0x015d:
        r0 = r24;	 Catch:{ Throwable -> 0x017a }
        r1 = r25;	 Catch:{ Throwable -> 0x017a }
        if (r0 >= r1) goto L_0x00d7;	 Catch:{ Throwable -> 0x017a }
    L_0x0163:
        r12 = r14[r24];	 Catch:{ Throwable -> 0x017a }
        r5 = java.lang.Integer.parseInt(r12);	 Catch:{ Exception -> 0x0171 }
        r0 = r18;	 Catch:{ Exception -> 0x0171 }
        r0.add(r5);	 Catch:{ Exception -> 0x0171 }
    L_0x016e:
        r24 = r24 + 1;
        goto L_0x015d;
    L_0x0171:
        r3 = move-exception;
        r26 = sLogger;	 Catch:{ Throwable -> 0x017a }
        r27 = "Exception while parsing the supported Pids";	 Catch:{ Throwable -> 0x017a }
        r26.e(r27);	 Catch:{ Throwable -> 0x017a }
        goto L_0x016e;
    L_0x017a:
        r19 = move-exception;
    L_0x017b:
        r28.stopPlayback();	 Catch:{ all -> 0x019c }
        r24 = sLogger;	 Catch:{ all -> 0x019c }
        r0 = r24;	 Catch:{ all -> 0x019c }
        r1 = r19;	 Catch:{ all -> 0x019c }
        r0.e(r1);	 Catch:{ all -> 0x019c }
        r24 = 0;
        com.navdy.service.library.util.IOUtils.closeStream(r16);
        goto L_0x002a;
    L_0x018e:
        r10 = move-exception;
        r24 = sLogger;	 Catch:{ Throwable -> 0x017a }
        r25 = "Error parsing the Obd data from the file ";	 Catch:{ Throwable -> 0x017a }
        r0 = r24;	 Catch:{ Throwable -> 0x017a }
        r1 = r25;	 Catch:{ Throwable -> 0x017a }
        r0.e(r1, r10);	 Catch:{ Throwable -> 0x017a }
        goto L_0x00e2;
    L_0x019c:
        r24 = move-exception;
    L_0x019d:
        com.navdy.service.library.util.IOUtils.closeStream(r16);
        throw r24;
    L_0x01a1:
        r24 = 1;
        com.navdy.service.library.util.IOUtils.closeStream(r16);
        goto L_0x002a;
    L_0x01a8:
        r24 = move-exception;
        r16 = r17;
        goto L_0x019d;
    L_0x01ac:
        r19 = move-exception;
        r16 = r17;
        goto L_0x017b;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.navdy.hud.app.debug.DriveRecorder.bPrepare(java.lang.String):boolean");
    }

    public void prepare(final String fileName) {
        if (isPlaying() || isPaused()) {
            sLogger.e("Cannot prepare for playback as the playback is in progress");
        } else {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    if (DriveRecorder.this.bPrepare(fileName)) {
                        DriveRecorder.sLogger.d("Prepare succeeded");
                    } else {
                        DriveRecorder.sLogger.e("Prepare failed");
                    }
                }
            }, 9);
        }
    }

    public void startPlayback(final String fileName, boolean loop) {
        if (isPlaying() || this.isRecording) {
            sLogger.v("already busy, no-op");
            return;
        }
        sLogger.d("Starting the playback");
        this.playbackState = State.PLAYING;
        this.isLooping = loop;
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (DriveRecorder.this.bPrepare(fileName)) {
                    DriveRecorder.sLogger.d("Prepare succeeded");
                    try {
                        DriveRecorder.this.currentObdDataInjectionIndex = 0;
                        DriveRecorder.this.obdPlaybackThread = new HandlerThread("ObdPlayback");
                        DriveRecorder.this.obdPlaybackThread.start();
                        DriveRecorder.this.obdDataPlaybackHandler = new Handler(DriveRecorder.this.obdPlaybackThread.getLooper());
                        DriveRecorder.this.obdDataPlaybackHandler.post(DriveRecorder.this.injectFakeObdData);
                        return;
                    } catch (Throwable t) {
                        DriveRecorder.this.stopPlayback();
                        DriveRecorder.sLogger.e(t);
                        return;
                    }
                }
                DriveRecorder.sLogger.e("Failed to prepare for playback , file : " + fileName);
                DriveRecorder.this.stopPlayback();
            }
        }, 9);
    }

    public void stopPlayback() {
        if (isPlaying()) {
            sLogger.d("Stopping the playback");
            this.currentObdDataInjectionIndex = 0;
            if (this.obdDataPlaybackHandler != null) {
                this.obdDataPlaybackHandler.removeCallbacks(this.injectFakeObdData);
            }
            if (this.obdPlaybackThread != null && this.obdPlaybackThread.isAlive()) {
                sLogger.v("obd handler thread quit:" + this.obdPlaybackThread.quit());
            }
            this.obdPlaybackThread = null;
            this.isLooping = false;
            this.playbackState = State.STOPPED;
            return;
        }
        sLogger.v("already stopped, no-op");
    }

    public void release() {
        stopPlayback();
        if (this.recordedObdData != null) {
            this.recordedObdData = null;
        }
        this.preparedFileName = null;
        this.preparedFileLastModifiedTime = -1;
    }

    public void pausePlayback() {
        if (isPlaying()) {
            sLogger.d("Pausing the playback");
            this.obdDataPlaybackHandler.removeCallbacks(this.injectFakeObdData);
            this.playbackState = State.PAUSED;
            return;
        }
        sLogger.d("Playback is not happening so not pausing");
    }

    public void resumePlayback() {
        if (isPaused()) {
            this.playbackState = State.PLAYING;
            this.obdDataPlaybackHandler.post(this.injectFakeObdData);
        }
    }

    public boolean restartPlayback() {
        if (!isPlaying() && !isPaused()) {
            return false;
        }
        this.playbackState = State.PLAYING;
        this.currentObdDataInjectionIndex = 0;
        this.obdDataPlaybackHandler.removeCallbacks(this.injectFakeObdData);
        this.obdDataPlaybackHandler.post(this.injectFakeObdData);
        return true;
    }

    public void flushRecordings() {
        this.handler.post(new Runnable() {
            public void run() {
                if (DriveRecorder.this.isRecording) {
                    DriveRecorder.this.obdAsyncBufferedFileWriter.flush();
                    DriveRecorder.this.driveScoreDataBufferedFileWriter.flush();
                }
            }
        });
    }

    public DriveRecorder(Bus bus, ConnectionHandler connectionHandler) {
        this.connectionHandler = connectionHandler;
        this.isRecording = false;
        this.playbackState = State.STOPPED;
        this.isSupportedPidsWritten = false;
        this.bus = bus;
        this.bus.register(this);
        ObdManager.getInstance().setDriveRecorder(this);
        this.context.bindService(new Intent(this.context, RouteRecorderService.class), this, 1);
    }

    public void startRecording(String label) {
        if (this.isRecording || isPlaying()) {
            sLogger.v("Obd data recorder is already busy");
            return;
        }
        sLogger.d("Starting the recording");
        this.isRecording = true;
        this.isSupportedPidsWritten = false;
        TaskManager.getInstance().execute(createDriveRecord(label), 9);
    }

    private Runnable createDriveRecord(final String fileName) {
        return new Runnable() {
            public void run() {
                File driveLogsDir = DriveRecorder.getDriveLogsDir(fileName);
                if (!driveLogsDir.exists()) {
                    driveLogsDir.mkdirs();
                }
                File driveLogFile = new File(driveLogsDir, fileName);
                File obdDataLogFile = new File(driveLogFile.getAbsolutePath() + ".obd");
                File gforceLogFile = new File(driveLogFile.getAbsolutePath() + ".drive");
                try {
                    if (obdDataLogFile.createNewFile()) {
                        DriveRecorder.this.obdAsyncBufferedFileWriter = new AsyncBufferedFileWriter(obdDataLogFile.getAbsolutePath(), DriveRecorder.this, 250);
                        ObdManager.getInstance().setRecordingPidListener(null);
                        DriveRecorder.this.bRecordSupportedPids();
                        ObdManager.getInstance().setRecordingPidListener(DriveRecorder.this.pidListener);
                    }
                    if (gforceLogFile.createNewFile()) {
                        DriveRecorder.this.driveScoreDataBufferedFileWriter = new AsyncBufferedFileWriter(gforceLogFile.getAbsolutePath(), DriveRecorder.this, 500);
                    }
                } catch (Throwable e) {
                    DriveRecorder.sLogger.e(e);
                    DriveRecorder.this.isRecording = false;
                    DriveRecorder.this.isSupportedPidsWritten = false;
                }
            }
        };
    }

    private void bRecordSupportedPids() throws IOException {
        ObdManager obdManager = ObdManager.getInstance();
        PidSet supportedPidSet = obdManager.getSupportedPids();
        if (supportedPidSet != null) {
            List<Pid> supportedPidsList = supportedPidSet.asList();
            if (supportedPidsList == null || supportedPidsList.size() <= 0) {
                this.obdAsyncBufferedFileWriter.write("-1\n");
            } else {
                for (Pid pid : supportedPidsList) {
                    if (pid != supportedPidsList.get(supportedPidsList.size() - 1)) {
                        this.obdAsyncBufferedFileWriter.write(pid.getId() + HereManeuverDisplayBuilder.COMMA);
                    } else {
                        this.obdAsyncBufferedFileWriter.write(pid.getId() + GlanceConstants.NEWLINE);
                    }
                }
            }
            this.obdAsyncBufferedFileWriter.write(System.currentTimeMillis() + HereManeuverDisplayBuilder.COMMA);
            this.obdAsyncBufferedFileWriter.write("47:" + obdManager.getFuelLevel() + HereManeuverDisplayBuilder.COMMA);
            this.obdAsyncBufferedFileWriter.write("13:" + SpeedManager.getInstance().getObdSpeed() + HereManeuverDisplayBuilder.COMMA);
            this.obdAsyncBufferedFileWriter.write("12:" + obdManager.getEngineRpm() + HereManeuverDisplayBuilder.COMMA);
            this.obdAsyncBufferedFileWriter.write("256:" + obdManager.getInstantFuelConsumption() + GlanceConstants.NEWLINE, true);
            this.isSupportedPidsWritten = true;
            sLogger.d("Obd data record created successfully and starting to listening to the PID changes");
        }
    }

    @Subscribe
    public void onKeyEvent(KeyEvent event) {
        if (event != null && event.getKeyCode() == 36 && event.getAction() == 1) {
            performDemoPlaybackAction(Action.STOP, false);
        } else if (event != null && event.getKeyCode() == 53 && event.getAction() == 1) {
            performDemoPlaybackAction(Action.RESTART, false);
        } else if (event != null && event.getKeyCode() == 44 && event.getAction() == 1) {
            performDemoPlaybackAction(Action.PRELOAD, false);
        }
    }

    @Subscribe
    public void onStartRecordingEvent(StartDriveRecordingEvent startRecordingEvent) {
        sLogger.d("Start recording event received");
        startRecording(startRecordingEvent.label);
    }

    @Subscribe
    public void onStopRecordingEvent(StopDriveRecordingEvent stopRecordingEvent) {
        sLogger.d("Stop recording event received");
        stopRecording();
    }

    @Subscribe
    public void onStartPlayback(StartDrivePlaybackEvent startPlayback) {
        sLogger.d("Start playback event received");
        startPlayback(startPlayback.label, false);
    }

    @Subscribe
    public void onStopPlayback(StopDrivePlaybackEvent stopDrivePlaybackEvent) {
        sLogger.d("Stop playback event received");
        stopPlayback();
    }

    @Subscribe
    public void onCalibratedGForceData(CalibratedGForceData calibratedGForceData) {
        if (this.isRecording && this.driveScoreDataBufferedFileWriter != null) {
            this.gforceDataBuilder.setLength(0);
            this.driveScoreDataBufferedFileWriter.write(this.gforceDataBuilder.append(System.currentTimeMillis()).append(HereManeuverDisplayBuilder.COMMA).append("G,").append(calibratedGForceData.getXAccel()).append(HereManeuverDisplayBuilder.COMMA).append(calibratedGForceData.getYAccel()).append(HereManeuverDisplayBuilder.COMMA).append(calibratedGForceData.getZAccel()).append(GlanceConstants.NEWLINE).toString());
        }
    }

    @Subscribe
    public void onDriveScoreUpdatedEvent(DriveScoreUpdated driveScoreUpdated) {
        if (this.isRecording && this.driveScoreDataBufferedFileWriter != null) {
            this.gforceDataBuilder.setLength(0);
            this.driveScoreDataBufferedFileWriter.write(this.gforceDataBuilder.append(System.currentTimeMillis()).append(HereManeuverDisplayBuilder.COMMA).append("D,").append("DS:").append(driveScoreUpdated.getDriveScore()).append(HereManeuverDisplayBuilder.COMMA).append("DE:").append(driveScoreUpdated.getInterestingEvent()).append(HereManeuverDisplayBuilder.COMMA).append("SD:").append(TelemetrySession.INSTANCE.getSessionDuration()).append(HereManeuverDisplayBuilder.COMMA).append("RD:").append(TelemetrySession.INSTANCE.getSessionRollingDuration()).append(HereManeuverDisplayBuilder.COMMA).append("SPD:").append(TelemetrySession.INSTANCE.getSpeedingDuration()).append(HereManeuverDisplayBuilder.COMMA).append("ESD:").append(TelemetrySession.INSTANCE.getExcessiveSpeedingDuration()).append(HereManeuverDisplayBuilder.COMMA).append("HA:").append(TelemetrySession.INSTANCE.getSessionHardAccelerationCount()).append(HereManeuverDisplayBuilder.COMMA).append("HB:").append(TelemetrySession.INSTANCE.getSessionHardBrakingCount()).append(HereManeuverDisplayBuilder.COMMA).append("HG:").append(TelemetrySession.INSTANCE.getSessionHighGCount()).append(HereManeuverDisplayBuilder.COMMA).append("LT:").append(getLatitude()).append(HereManeuverDisplayBuilder.COMMA).append("LN:").append(getLongitude()).append(GlanceConstants.NEWLINE).toString());
        }
    }

    private static double getLatitude() {
        if (HereMapsManager.getInstance().isInitialized()) {
            GeoCoordinate c = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
            if (c != null) {
                return c.getLatitude();
            }
        }
        return 0.0d;
    }

    private static double getLongitude() {
        if (HereMapsManager.getInstance().isInitialized()) {
            GeoCoordinate c = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
            if (c != null) {
                return c.getLongitude();
            }
        }
        return 0.0d;
    }

    @Subscribe
    public void onMapEngineInitialized(MapEngineInitialize mapEngineInitializedEvent) {
        sLogger.d("onMapEnigneInitialzed : message received. Initialized :" + mapEngineInitializedEvent.initialized);
        this.isEngineInitialized = mapEngineInitializedEvent.initialized;
        checkAndStartAutoRecording();
    }

    public void performDemoPlaybackAction(final Action action, final boolean loop) {
        sLogger.d(":performDemoPlaybackAction");
        if (this.isEngineInitialized) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    if (DriveRecorder.this.isDemoAvailable()) {
                        switch (action) {
                            case PLAY:
                                DriveRecorder.this.playDemo(loop);
                                return;
                            case PAUSE:
                                DriveRecorder.this.pauseDemo();
                                return;
                            case RESUME:
                                DriveRecorder.this.resumeDemo();
                                return;
                            case RESTART:
                                DriveRecorder.this.restartDemo(loop);
                                return;
                            case STOP:
                                DriveRecorder.this.stopDemo();
                                return;
                            case PRELOAD:
                                DriveRecorder.this.prepareDemo();
                                return;
                            default:
                                return;
                        }
                    }
                }
            }, 1);
        } else {
            sLogger.e("Engine is not initialized so can not start the playback");
        }
    }

    public void load() {
        sLogger.d("Loading the Obd Data recorder");
        this.demoRouteLogFileExists = new File(PathManager.getInstance().getMapsPartitionPath() + File.separator + DEMO_LOG_FILE_NAME).exists();
        sLogger.d("Demo file exists : " + this.demoRouteLogFileExists);
        this.demoObdLogFileExists = new File(PathManager.getInstance().getMapsPartitionPath() + File.separator + DEMO_LOG_FILE_NAME + ".obd").exists();
        sLogger.d("Demo obd file exists : " + this.demoObdLogFileExists);
    }

    public boolean isDemoAvailable() {
        if (this.demoRouteLogFileExists && !this.realObdConnected) {
            return true;
        }
        sLogger.d(!this.demoRouteLogFileExists ? "Demo log file does not exist in the map partition. So Demo preference : NA" : "Obd has been connected so skipping the demo");
        return false;
    }

    private void checkAndStartAutoRecording() {
        if (isAutoRecordingEnabled()) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    DriveRecorder.sLogger.d("Auto recording is enabled so starting the auto record");
                    File[] files = new File(PathManager.getInstance().getMapsPartitionPath() + File.separator + "drive_logs").listFiles();
                    if (files != null && files.length > 20) {
                        Arrays.sort(files, new FilesModifiedTimeComparator());
                        for (int i = 0; i < files.length - 20; i++) {
                            File lastFile = files[i];
                            DriveRecorder.sLogger.d("Deleting the drive record " + lastFile.getAbsolutePath() + " as its old ");
                            IOUtils.deleteFile(HudApplication.getAppContext(), lastFile.getAbsolutePath());
                        }
                    }
                    DriveRecorder.this.connectionHandler.sendLocalMessage(new StartDriveRecordingEvent("auto"));
                }
            }, 9);
        }
    }

    public void setRealObdConnected(boolean realObdConnected) {
        this.realObdConnected = realObdConnected;
        sLogger.d("setRealObdConnected : state " + realObdConnected);
        if (this.realObdConnected) {
            performDemoPlaybackAction(Action.STOP, true);
        }
    }

    public static File getDriveLogsDir(String fileName) {
        if (DEMO_LOG_FILE_NAME.equals(fileName)) {
            return new File(PathManager.getInstance().getMapsPartitionPath());
        }
        return new File(PathManager.getInstance().getMapsPartitionPath() + File.separator + "drive_logs");
    }

    public static boolean isAutoRecordingEnabled() {
        return SystemProperties.getBoolean(AUTO_DRIVERECORD_PROP, false) || !DeviceUtil.isUserBuild();
    }

    private boolean isPlaying() {
        return this.playbackState == State.PLAYING;
    }

    private boolean isPaused() {
        return this.playbackState == State.PAUSED;
    }

    private boolean isStopped() {
        return this.playbackState == State.STOPPED;
    }

    public boolean isDemoPlaying() {
        try {
            return isPlaying() || this.routeRecorder.isPlaying();
        } catch (Throwable e) {
            sLogger.e(e);
            return false;
        }
    }

    public boolean isDemoPaused() {
        try {
            return isPaused() || this.routeRecorder.isPaused();
        } catch (Throwable e) {
            sLogger.e(e);
            return false;
        }
    }

    public boolean isDemoStopped() {
        try {
            return isStopped() && this.routeRecorder.isStopped();
        } catch (Throwable e) {
            sLogger.e(e);
            return false;
        }
    }

    private void prepareDemo() {
        prepare(DEMO_LOG_FILE_NAME);
        try {
            this.routeRecorder.prepare(DEMO_LOG_FILE_NAME, false);
        } catch (Throwable e) {
            sLogger.e("Error preparing the route recorder for demo", e);
        }
    }

    private void playDemo(boolean loop) {
        stopRecordingBeforeDemo();
        if (!isPlaying()) {
            if (isPaused()) {
                resumePlayback();
            } else {
                startPlayback(DEMO_LOG_FILE_NAME, loop);
            }
        }
        try {
            if (this.routeRecorder.isPaused()) {
                this.routeRecorder.resumePlayback();
            } else if (!this.routeRecorder.isPlaying()) {
                this.routeRecorder.startPlayback(DEMO_LOG_FILE_NAME, false, loop);
            }
        } catch (Throwable e) {
            sLogger.e("Error playing the route playback", e);
        }
    }

    private void stopRecordingBeforeDemo() {
        if (this.isRecording) {
            stopRecording();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                sLogger.e("Interrupted ", e);
            }
        }
        try {
            if (this.routeRecorder.isRecording()) {
                this.routeRecorder.stopRecording();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e2) {
                    sLogger.e("Interrupted while stopping the recording");
                }
            }
        } catch (Exception e3) {
            sLogger.e("Exception while stopping the recording", e3);
        }
    }

    private void pauseDemo() {
        stopRecordingBeforeDemo();
        if (isPlaying()) {
            pausePlayback();
        }
        try {
            if (this.routeRecorder.isPlaying()) {
                this.routeRecorder.pausePlayback();
            }
        } catch (Exception e) {
            sLogger.e("Error pausing the route playback");
        }
    }

    private void stopDemo() {
        if (isPlaying()) {
            stopPlayback();
        }
        try {
            if (this.routeRecorder.isPlaying() || this.routeRecorder.isPaused()) {
                this.routeRecorder.stopPlayback();
            }
        } catch (Exception e) {
            sLogger.e("Error stopping the route playback", e);
        }
    }

    private void resumeDemo() {
        stopRecordingBeforeDemo();
        if (isPaused()) {
            resumePlayback();
        }
        try {
            if (this.routeRecorder.isPaused()) {
                this.routeRecorder.resumePlayback();
            }
        } catch (Exception e) {
            sLogger.e("Error resuming the route playback", e);
        }
    }

    private void restartDemo(boolean loop) {
        stopRecordingBeforeDemo();
        if (!restartPlayback()) {
            startPlayback(DEMO_LOG_FILE_NAME, loop);
        }
        try {
            if (!this.routeRecorder.restartPlayback()) {
                this.routeRecorder.startPlayback(DEMO_LOG_FILE_NAME, false, loop);
            }
        } catch (Exception e) {
            sLogger.e("Error restarting the route playback", e);
        }
    }
}
