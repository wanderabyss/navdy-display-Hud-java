package com.navdy.hud.app.profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.DriverProfileUpdated;
import com.navdy.hud.app.event.DriverProfileUpdated.State;
import com.navdy.hud.app.event.InitEvents.InitPhase;
import com.navdy.hud.app.event.InitEvents.Phase;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader.PhotoDownloadStatus;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Priority;
import com.navdy.hud.app.service.ConnectionHandler.DeviceSyncEvent;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.glances.CannedMessagesRequest;
import com.navdy.service.library.events.glances.CannedMessagesUpdate;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesUpdate;
import com.navdy.service.library.events.preferences.DriverProfilePreferences;
import com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest.Builder;
import com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate;
import com.navdy.service.library.events.preferences.InputPreferencesRequest;
import com.navdy.service.library.events.preferences.InputPreferencesUpdate;
import com.navdy.service.library.events.preferences.LocalPreferences;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.service.library.events.preferences.NavigationPreferencesRequest;
import com.navdy.service.library.events.preferences.NavigationPreferencesUpdate;
import com.navdy.service.library.events.preferences.NotificationPreferencesRequest;
import com.navdy.service.library.events.preferences.NotificationPreferencesUpdate;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import javax.inject.Inject;

public class DriverProfileManager {
    private static final String DEFAULT_PROFILE_NAME = "DefaultProfile";
    private static final DriverProfileChanged DRIVER_PROFILE_CHANGED = new DriverProfileChanged();
    private static final String LOCAL_PREFERENCES_SUFFIX = "_preferences";
    private static final NavdyDeviceId MOCK_PROFILE_DEVICE_ID = NavdyDeviceId.UNKNOWN_ID;
    private static final Logger sLogger = new Logger(DriverProfileManager.class);
    private String lastUserEmail;
    private String lastUserName;
    private HashMap<String, DriverProfile> mAllProfiles = new HashMap();
    private Bus mBus;
    private DriverProfile mCurrentProfile;
    private String mProfileDirectoryName;
    private Set<DriverProfile> mPublicProfiles = new HashSet();
    private DriverSessionPreferences mSessionPrefs;
    private DriverProfile theDefaultProfile;

    @Inject
    public DriverProfileManager(Bus bus, PathManager pathManager, TimeHelper timeHelper) {
        this.mProfileDirectoryName = pathManager.getDriverProfilesDir();
        this.mBus = bus;
        this.mBus.register(this);
        sLogger.i("initializing in " + this.mProfileDirectoryName);
        File[] profileDirectories = new File(this.mProfileDirectoryName).listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        });
        int length = profileDirectories.length;
        int i = 0;
        while (i < length) {
            File profileDirectory = profileDirectories[i];
            try {
                DriverProfile profile = new DriverProfile(profileDirectory);
                String profileName = profileDirectory.getName();
                this.mAllProfiles.put(profileName, profile);
                if (profile.isProfilePublic()) {
                    this.mPublicProfiles.add(profile);
                }
                if (profileName.equals(profileNameForId(MOCK_PROFILE_DEVICE_ID))) {
                    this.theDefaultProfile = profile;
                }
                i++;
            } catch (Exception e) {
                sLogger.e("could not load profile from " + profileDirectory, e);
            }
        }
        if (this.theDefaultProfile == null) {
            this.theDefaultProfile = createProfileForId(MOCK_PROFILE_DEVICE_ID);
        }
        NavigationPreferences defaultNavPrefs = this.theDefaultProfile.getNavigationPreferences();
        this.mSessionPrefs = new DriverSessionPreferences(this.mBus, this.theDefaultProfile, timeHelper);
        setCurrentProfile(this.theDefaultProfile);
    }

    public DriverProfile getCurrentProfile() {
        return this.mCurrentProfile;
    }

    public void loadProfileForId(final NavdyDeviceId deviceId) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                DriverProfileManager.sLogger.v("loading profile:" + deviceId);
                DriverProfile profile = DriverProfileManager.this.getProfileForId(deviceId);
                if (profile == null) {
                    DriverProfileManager.sLogger.w("profile not loaded:" + deviceId);
                    profile = DriverProfileManager.this.createProfileForId(deviceId);
                }
                DriverProfileManager.this.setCurrentProfile(profile);
            }
        }, 10);
    }

    public void setCurrentProfile(DriverProfile profile) {
        sLogger.i("setting current profile to " + profile);
        if (profile == null) {
            profile = this.theDefaultProfile;
        }
        if (this.mCurrentProfile != profile) {
            this.mCurrentProfile = profile;
            this.mSessionPrefs.setDefault(this.mCurrentProfile, isDefaultProfile(this.mCurrentProfile));
            this.theDefaultProfile.setTrafficEnabled(this.mCurrentProfile.isTrafficEnabled());
            copyCurrentProfileToDefault();
            this.mBus.post(DRIVER_PROFILE_CHANGED);
        }
        this.lastUserName = this.mCurrentProfile.getDriverName();
        this.lastUserEmail = this.mCurrentProfile.getDriverEmail();
        sLogger.v("lastUserEmail:" + this.lastUserEmail);
    }

    public DriverProfile getProfileForId(NavdyDeviceId deviceId) {
        return (DriverProfile) this.mAllProfiles.get(profileNameForId(deviceId));
    }

    public Collection<DriverProfile> getPublicProfiles() {
        return this.mPublicProfiles;
    }

    public DriverProfile createProfileForId(NavdyDeviceId deviceId) {
        String profileName = profileNameForId(deviceId);
        try {
            DriverProfile newProfile = DriverProfile.createProfileForId(profileName, this.mProfileDirectoryName);
            this.mAllProfiles.put(profileName, newProfile);
            return newProfile;
        } catch (IOException e) {
            sLogger.e("could not create new profile: " + e);
            return null;
        }
    }

    private String profileNameForId(NavdyDeviceId deviceId) {
        String profileName = deviceId.getBluetoothAddress();
        if (profileName == null) {
            profileName = DEFAULT_PROFILE_NAME;
        }
        return GenericUtil.normalizeToFilename(profileName);
    }

    private void requestPreferenceUpdates(DriverProfile profile) {
        sLogger.v("requestPreferenceUpdates:" + profile.getProfileName());
        sendEventToClient(new Builder().serial_number(profile.mDriverProfilePreferences.serial_number).build());
        sLogger.v("requestPreferenceUpdates: nav:" + profile.mNavigationPreferences.serial_number);
        sendEventToClient(new NavigationPreferencesRequest.Builder().serial_number(profile.mNavigationPreferences.serial_number).build());
        sLogger.v("requestPreferenceUpdates: notif:" + profile.mNotificationPreferences.serial_number);
        sendEventToClient(new NotificationPreferencesRequest.Builder().serial_number(profile.mNotificationPreferences.serial_number).build());
        sLogger.v("requestPreferenceUpdates: input:" + profile.mInputPreferences.serial_number);
        sendEventToClient(new InputPreferencesRequest.Builder().serial_number(profile.mInputPreferences.serial_number).build());
    }

    private void requestUpdates(DriverProfile profile) {
        sLogger.v("requestUpdates(canned-msgs) [" + profile.mCannedMessages.serial_number + "]");
        sendEventToClient(new CannedMessagesRequest.Builder().serial_number(profile.mCannedMessages.serial_number).build());
    }

    private void sendEventToClient(Message message) {
        this.mBus.post(new RemoteEvent(message));
    }

    @Subscribe
    public void onDriverProfileChange(DriverProfileChanged changed) {
        this.mBus.post(this.mCurrentProfile.getNavigationPreferences());
    }

    @Subscribe
    public void onDeviceSyncRequired(DeviceSyncEvent event) {
        sLogger.v("calling requestPreferenceUpdates");
        requestPreferenceUpdates(this.mCurrentProfile);
        sLogger.v("calling requestUpdates");
        requestUpdates(this.mCurrentProfile);
    }

    @Subscribe
    public void onDriverProfilePreferencesUpdate(DriverProfilePreferencesUpdate update) {
        if (this.mCurrentProfile != this.theDefaultProfile) {
            switch (update.status) {
                case REQUEST_VERSION_IS_CURRENT:
                    sLogger.i("prefs are up to date!");
                    TaskManager.getInstance().execute(new Runnable() {
                        public void run() {
                            DriverProfileManager.this.changeLocale(false);
                            DriverProfileManager.this.mBus.post(new DriverProfileUpdated(State.UP_TO_DATE));
                        }
                    }, 10);
                    return;
                case REQUEST_SUCCESS:
                    sLogger.i("Received profile update, applying");
                    if (this.mCurrentProfile != null) {
                        final DriverProfilePreferences preferences = update.preferences;
                        if (preferences != null) {
                            TaskManager.getInstance().execute(new Runnable() {
                                public void run() {
                                    DriverProfileManager.this.refreshProfileImageIfNeeded(preferences.photo_checksum);
                                }
                            }, 10);
                            if (((Boolean) Wire.get(preferences.profile_is_public, DriverProfilePreferences.DEFAULT_PROFILE_IS_PUBLIC)).booleanValue()) {
                                this.mPublicProfiles.add(this.mCurrentProfile);
                            } else {
                                this.mPublicProfiles.remove(this.mCurrentProfile);
                            }
                            TaskManager.getInstance().execute(new Runnable() {
                                public void run() {
                                    DriverProfilePreferences driverProfilePreferences = DriverProfileManager.this.findSupportedLocale(preferences);
                                    Locale oldLocale = DriverProfileManager.this.mCurrentProfile.getLocale();
                                    DriverProfileManager.this.mCurrentProfile.setDriverProfilePreferences(driverProfilePreferences);
                                    DriverProfileManager.this.theDefaultProfile.setDriverProfilePreferences(driverProfilePreferences);
                                    Locale newLocale = DriverProfileManager.this.mCurrentProfile.getLocale();
                                    boolean localeChanged = !oldLocale.equals(newLocale);
                                    DriverProfileManager.sLogger.i("[HUD-locale] changed[" + localeChanged + "] current[" + oldLocale + "] new[" + newLocale + "]");
                                    DriverProfileManager.this.changeLocale(localeChanged);
                                    DriverProfileManager.this.mBus.post(new DriverProfileUpdated(State.UPDATED));
                                }
                            }, 10);
                            return;
                        }
                        sLogger.i("preferences update with no preferences!");
                        return;
                    }
                    return;
                default:
                    sLogger.i("profile status was " + update.status + ", which I didn't expect; ignoring.");
                    return;
            }
        }
    }

    private DriverProfilePreferences findSupportedLocale(DriverProfilePreferences preferences) {
        DriverProfilePreferences driverProfilePreferences = preferences;
        if (HudLocale.isLocaleSupported(HudLocale.getLocaleForID(preferences.locale)) || driverProfilePreferences.additionalLocales == null) {
            return driverProfilePreferences;
        }
        Locale supportedAdditionalLocale = null;
        for (String additionalLocaleStr : driverProfilePreferences.additionalLocales) {
            if (!TextUtils.isEmpty(additionalLocaleStr)) {
                Locale additionalLocale = HudLocale.getLocaleForID(additionalLocaleStr);
                if (HudLocale.isLocaleSupported(additionalLocale)) {
                    sLogger.v("HUD-locale additional locale supported:" + additionalLocale);
                    supportedAdditionalLocale = additionalLocale;
                    break;
                }
                sLogger.v("HUD-locale additional locale not supported:" + additionalLocale);
            }
        }
        if (supportedAdditionalLocale != null) {
            return new DriverProfilePreferences.Builder(driverProfilePreferences).locale(supportedAdditionalLocale.toString()).build();
        }
        return driverProfilePreferences;
    }

    private void refreshProfileImageIfNeeded(String remoteChecksum) {
        PhoneImageDownloader downloader = PhoneImageDownloader.getInstance();
        String localChecksum = downloader.hashFor(DriverProfile.DRIVER_PROFILE_IMAGE, PhotoType.PHOTO_DRIVER_PROFILE);
        if (!Objects.equals(remoteChecksum, localChecksum)) {
            sLogger.d("asking for new photo - local:" + localChecksum + " remote:" + remoteChecksum);
            downloader.submitDownload(DriverProfile.DRIVER_PROFILE_IMAGE, Priority.NORMAL, PhotoType.PHOTO_DRIVER_PROFILE, null);
        }
    }

    @Subscribe
    public void onNavigationPreferencesUpdate(final NavigationPreferencesUpdate update) {
        handleUpdateInBackground(update, update.status, update.preferences, new Runnable() {
            public void run() {
                DriverProfileManager.this.mCurrentProfile.setNavigationPreferences(update.preferences);
                DriverProfileManager.this.theDefaultProfile.setNavigationPreferences(update.preferences);
                NavigationPreferences preferences = DriverProfileManager.this.mCurrentProfile.getNavigationPreferences();
                AnalyticsSupport.recordNavigationPreferenceChange(DriverProfileManager.this.mCurrentProfile);
                DriverProfileManager.this.mSessionPrefs.getNavigationSessionPreference().setDefault(preferences);
                DriverProfileManager.this.mBus.post(preferences);
            }
        });
    }

    @Subscribe
    public void onInputPreferencesUpdate(final InputPreferencesUpdate update) {
        handleUpdateInBackground(update, update.status, update.preferences, new Runnable() {
            public void run() {
                DriverProfileManager.this.mCurrentProfile.setInputPreferences(update.preferences);
                DriverProfileManager.this.theDefaultProfile.setInputPreferences(update.preferences);
                AnalyticsSupport.recordPreferenceChange(DriverProfileManager.this.mCurrentProfile);
                DriverProfileManager.this.mBus.post(DriverProfileManager.this.mCurrentProfile.getInputPreferences());
            }
        });
    }

    @Subscribe
    public void onDisplaySpeakerPreferencesUpdate(final DisplaySpeakerPreferencesUpdate update) {
        handleUpdateInBackground(update, update.status, update.preferences, new Runnable() {
            public void run() {
                DriverProfileManager.this.mCurrentProfile.setSpeakerPreferences(update.preferences);
                DriverProfileManager.this.theDefaultProfile.setSpeakerPreferences(update.preferences);
                AnalyticsSupport.recordPreferenceChange(DriverProfileManager.this.mCurrentProfile);
                DriverProfileManager.this.mBus.post(DriverProfileManager.this.mCurrentProfile.getSpeakerPreferences());
            }
        });
    }

    @Subscribe
    public void onNotificationPreferencesUpdate(final NotificationPreferencesUpdate update) {
        handleUpdateInBackground(update, update.status, update.preferences, new Runnable() {
            public void run() {
                DriverProfileManager.this.mCurrentProfile.setNotificationPreferences(update.preferences);
                DriverProfileManager.this.theDefaultProfile.setNotificationPreferences(update.preferences);
                AnalyticsSupport.recordPreferenceChange(DriverProfileManager.this.mCurrentProfile);
                DriverProfileManager.this.mBus.post(DriverProfileManager.this.mCurrentProfile.getNotificationPreferences());
            }
        });
    }

    @Subscribe
    public void onCannedMessagesUpdate(final CannedMessagesUpdate update) {
        handleUpdateInBackground(update, update.status, update, new Runnable() {
            public void run() {
                DriverProfileManager.this.mCurrentProfile.setCannedMessages(update);
            }
        });
    }

    public void updateLocalPreferences(final LocalPreferences preferences) {
        if (this.mCurrentProfile != null) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    DriverProfileManager.this.mCurrentProfile.setLocalPreferences(preferences);
                    DriverProfileManager.this.theDefaultProfile.setLocalPreferences(preferences);
                    AnalyticsSupport.recordPreferenceChange(DriverProfileManager.this.mCurrentProfile);
                    DriverProfileManager.this.mBus.post(preferences);
                }
            }, 10);
        }
    }

    public LocalPreferences getLocalPreferences() {
        return this.mCurrentProfile.getLocalPreferences();
    }

    private void handleUpdateInBackground(Message request, RequestStatus status, Message preferences, Runnable runnable) {
        if (this.mCurrentProfile != this.theDefaultProfile) {
            String requestType = request.getClass().getSimpleName();
            switch (status) {
                case REQUEST_VERSION_IS_CURRENT:
                    sLogger.i(requestType + " returned up to date!");
                    return;
                case REQUEST_SUCCESS:
                    sLogger.i("Received " + requestType + ", applying");
                    if (this.mCurrentProfile == null) {
                        return;
                    }
                    if (preferences != null) {
                        TaskManager.getInstance().execute(runnable, 10);
                        return;
                    } else {
                        sLogger.i(requestType + " with no preferences!");
                        return;
                    }
                default:
                    sLogger.i("profile status for " + requestType + " was " + status + ", which I didn't expect; ignoring.");
                    return;
            }
        }
    }

    @Subscribe
    public void onPhotoDownload(PhotoDownloadStatus photoStatus) {
        if (photoStatus.photoType == PhotoType.PHOTO_DRIVER_PROFILE && !photoStatus.alreadyDownloaded && this.mCurrentProfile != this.theDefaultProfile) {
            if (photoStatus.success) {
                sLogger.d("applying new photo");
            } else {
                sLogger.i("driver photo not downloaded");
            }
        }
    }

    boolean isDefaultProfile(DriverProfile profile) {
        return profile == this.theDefaultProfile;
    }

    public SharedPreferences getLocalPreferencesForDriverProfile(Context context, DriverProfile profile) {
        return context.getSharedPreferences(profile.getProfileName() + LOCAL_PREFERENCES_SUFFIX, 0);
    }

    public SharedPreferences getLocalPreferencesForCurrentDriverProfile(Context context) {
        return getLocalPreferencesForDriverProfile(context, getCurrentProfile());
    }

    public DriverSessionPreferences getSessionPreferences() {
        return this.mSessionPrefs;
    }

    public boolean isManualZoom() {
        LocalPreferences localPreferences = getLocalPreferences();
        if (localPreferences == null || !Boolean.TRUE.equals(localPreferences.manualZoom)) {
            return false;
        }
        return true;
    }

    public Locale getCurrentLocale() {
        return this.mCurrentProfile.getLocale();
    }

    public String getLastUserName() {
        return this.lastUserName;
    }

    public String getLastUserEmail() {
        return this.lastUserEmail;
    }

    public void enableTraffic() {
        this.mCurrentProfile.setTrafficEnabled(true);
        this.theDefaultProfile.setTrafficEnabled(true);
    }

    public void disableTraffic() {
        this.mCurrentProfile.setTrafficEnabled(false);
        this.theDefaultProfile.setTrafficEnabled(false);
    }

    public boolean isTrafficEnabled() {
        return this.mCurrentProfile.isTrafficEnabled();
    }

    private void copyCurrentProfileToDefault() {
        if (this.mCurrentProfile != this.theDefaultProfile) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    DriverProfileManager.sLogger.v("copyCurrentProfileToDefault");
                    DriverProfileManager.this.theDefaultProfile.copy(DriverProfileManager.this.mCurrentProfile);
                }
            }, 10);
        }
    }

    private void changeLocale(boolean showLanguageNotSupportedToast) {
        try {
            Locale locale = getCurrentLocale();
            sLogger.v("[HUD-locale] changeLocale current=" + locale);
            if (HudLocale.switchLocale(HudApplication.getAppContext(), locale)) {
                sLogger.v("[HUD-locale] change, restarting...");
                this.mBus.post(new InitPhase(Phase.SWITCHING_LOCALE));
                HudLocale.showLocaleChangeToast();
                return;
            }
            sLogger.v("[HUD-locale] not changed");
            this.mBus.post(new InitPhase(Phase.LOCALE_UP_TO_DATE));
            if (HudLocale.isLocaleSupported(locale)) {
                HudLocale.dismissToast();
            } else if (showLanguageNotSupportedToast) {
                HudLocale.showLocaleNotSupportedToast(locale.getDisplayLanguage());
            }
        } catch (Throwable t) {
            sLogger.e("[HUD-locale] changeLocale", t);
        }
    }
}
