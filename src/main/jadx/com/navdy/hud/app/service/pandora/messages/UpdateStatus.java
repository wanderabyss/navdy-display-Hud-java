package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import java.util.HashMap;
import java.util.Map;

public class UpdateStatus extends BaseIncomingMessage {
    private static int MESSAGE_LENGTH = 2;
    private static Map<Byte, UpdateStatus> SINGLETONS_MAP = new HashMap<Byte, UpdateStatus>() {
        {
            put(Byte.valueOf((byte) 1), new UpdateStatus(Value.PLAYING, null));
            put(Byte.valueOf((byte) 2), new UpdateStatus(Value.PAUSED, null));
            put(Byte.valueOf((byte) 3), new UpdateStatus(Value.INCOMPATIBLE_API_VERSION, null));
            put(Byte.valueOf((byte) 4), new UpdateStatus(Value.UNKNOWN_ERROR, null));
            put(Byte.valueOf((byte) 5), new UpdateStatus(Value.NO_STATIONS, null));
            put(Byte.valueOf((byte) 6), new UpdateStatus(Value.NO_STATION_ACTIVE, null));
            put(Byte.valueOf((byte) 7), new UpdateStatus(Value.INSUFFICIENT_CONNECTIVITY, null));
            put(Byte.valueOf((byte) 8), new UpdateStatus(Value.LISTENING_RESTRICTIONS, null));
            put(Byte.valueOf((byte) 9), new UpdateStatus(Value.INVALID_LOGIN, null));
        }
    };
    public Value value;

    public enum Value {
        PLAYING,
        PAUSED,
        INCOMPATIBLE_API_VERSION,
        UNKNOWN_ERROR,
        NO_STATIONS,
        NO_STATION_ACTIVE,
        INSUFFICIENT_CONNECTIVITY,
        LISTENING_RESTRICTIONS,
        INVALID_LOGIN
    }

    /* synthetic */ UpdateStatus(Value x0, AnonymousClass1 x1) {
        this(x0);
    }

    private UpdateStatus(Value value) {
        this.value = value;
    }

    protected static BaseIncomingMessage innerBuildFromPayload(byte[] payload) throws MessageWrongWayException, CorruptedPayloadException {
        if (payload.length != MESSAGE_LENGTH) {
            throw new CorruptedPayloadException();
        }
        UpdateStatus result = (UpdateStatus) SINGLETONS_MAP.get(Byte.valueOf(payload[1]));
        if (result != null) {
            return result;
        }
        throw new CorruptedPayloadException();
    }

    public String toString() {
        return "Update Status: " + this.value;
    }
}
