package com.navdy.hud.app.service;

import android.content.Context;
import android.content.Intent;
import android.os.Process;
import android.os.SystemClock;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.file.FileTransferData;
import com.navdy.service.library.events.file.FileTransferError;
import com.navdy.service.library.events.file.FileTransferRequest;
import com.navdy.service.library.events.file.FileTransferResponse.Builder;
import com.navdy.service.library.events.file.FileTransferStatus;
import com.navdy.service.library.events.file.FileType;
import com.navdy.service.library.file.FileTransferSessionManager;
import com.navdy.service.library.file.IFileTransferAuthority;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.wire.Message;
import java.io.File;

public class FileTransferHandler {
    public static final String ACTION_OTA_DOWNLOAD = "com.navdy.hud.app.service.OTA_DOWNLOAD";
    public static final String OTA_DOWLOADING_PROPERTY = "navdy.ota.downloading";
    static final Logger sLogger = new Logger(FileTransferHandler.class);
    private boolean closed;
    private Context context;
    IFileTransferAuthority fileTransferAuthority = PathManager.getInstance();
    FileTransferSessionManager fileTransferManager;
    private final Logger logger = new Logger(getClass());
    RemoteDevice remoteDevice;

    public FileTransferHandler(Context context) {
        this.fileTransferManager = new FileTransferSessionManager(context, this.fileTransferAuthority);
        this.context = context;
    }

    public void onFileTransferRequest(final FileTransferRequest request) {
        if (!this.closed) {
            if (request == null || request.fileType == null || request.fileType != FileType.FILE_TYPE_LOGS) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        FileTransferHandler.this.logger.v("onFileTransferRequest");
                        if (FileTransferHandler.this.remoteDevice.getLinkBandwidthLevel() <= 0) {
                            FileTransferHandler.sLogger.d("Link bandwidth is low");
                            if (request != null) {
                                FileTransferHandler.sLogger.d("FileTransferRequest (PUSH) : rejecting file transfer request to avoid link traffic");
                                FileTransferHandler.this.remoteDevice.postEvent(new Builder().success(Boolean.valueOf(false)).destinationFileName(request.destinationFileName).fileType(request.fileType).error(FileTransferError.FILE_TRANSFER_HOST_BUSY).build());
                                return;
                            }
                            return;
                        }
                        FileTransferHandler.this.remoteDevice.postEvent(FileTransferHandler.this.fileTransferManager.handleFileTransferRequest(request));
                    }
                }, 5);
            } else {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        if (FileTransferHandler.this.remoteDevice.getLinkBandwidthLevel() <= 0) {
                            FileTransferHandler.sLogger.d("Link bandwidth is low");
                            if (request != null) {
                                FileTransferHandler.sLogger.d("FileTransferRequest (pull) : rejecting file transfer request to avoid link traffic");
                                FileTransferHandler.this.remoteDevice.postEvent(new Builder().success(Boolean.valueOf(false)).destinationFileName(request.destinationFileName).fileType(request.fileType).error(FileTransferError.FILE_TRANSFER_HOST_BUSY).build());
                                return;
                            }
                            return;
                        }
                        Message fileTransferResponse = FileTransferHandler.this.fileTransferManager.handleFileTransferRequest(request);
                        FileTransferHandler.this.remoteDevice.postEvent(fileTransferResponse);
                        if (fileTransferResponse.success.booleanValue()) {
                            int mTransferId = fileTransferResponse.transferId.intValue();
                            try {
                                if (fileTransferResponse.supportsAcks.booleanValue()) {
                                    if (FileTransferHandler.this.sendNextChunk(mTransferId) != null) {
                                        FileTransferHandler.sLogger.d("Send the first chunk");
                                    } else {
                                        FileTransferHandler.sLogger.e("Error sending the first chunk because data was null");
                                    }
                                    FileTransferHandler.sLogger.d("Flow control enabled, waiting for the FileTransferStatus");
                                    return;
                                }
                                FileTransferData data;
                                do {
                                    data = FileTransferHandler.this.sendNextChunk(mTransferId);
                                    if (data == null) {
                                        FileTransferHandler.sLogger.e("Error sending the chunk because data was null");
                                        break;
                                    }
                                    Thread.sleep(1000);
                                    if (data == null) {
                                        break;
                                    }
                                } while (!data.lastChunk.booleanValue());
                                if (data != null && data.lastChunk.booleanValue()) {
                                    FileTransferHandler.this.fileTransferAuthority.onFileSent(request.fileType);
                                }
                            } catch (Throwable t) {
                                FileTransferHandler.sLogger.d("Exception while reading the data and sending it across", t);
                            }
                        }
                    }
                }, 5);
            }
        }
    }

    private FileTransferData sendNextChunk(int transferId) throws Throwable {
        Message data = this.fileTransferManager.getNextChunk(transferId);
        if (data != null) {
            this.remoteDevice.postEvent(data);
        }
        return data;
    }

    private void setDownloadingProperty(boolean downloading) {
        int time = 0;
        if (downloading) {
            time = (int) (SystemClock.elapsedRealtime() / 1000);
        }
        SystemProperties.set(OTA_DOWLOADING_PROPERTY, String.format("%d", new Object[]{Integer.valueOf(time)}));
    }

    public void onFileTransferData(final FileTransferData request) {
        if (!this.closed) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    boolean z = false;
                    FileTransferHandler.this.logger.v("onFileTransferData");
                    if (FileTransferHandler.this.remoteDevice.getLinkBandwidthLevel() <= 0) {
                        FileTransferHandler.this.remoteDevice.postEvent(new FileTransferStatus.Builder().success(Boolean.valueOf(false)).transferComplete(Boolean.valueOf(false)).error(FileTransferError.FILE_TRANSFER_HOST_BUSY).build());
                        return;
                    }
                    String absolutePath = FileTransferHandler.this.fileTransferManager.absolutePathForTransferId(request.transferId.intValue());
                    Message fileTransferStatus = FileTransferHandler.this.fileTransferManager.handleFileTransferData(request);
                    FileTransferHandler.this.remoteDevice.postEvent(fileTransferStatus);
                    FileTransferHandler fileTransferHandler = FileTransferHandler.this;
                    if (fileTransferStatus.success.booleanValue() && !fileTransferStatus.transferComplete.booleanValue()) {
                        z = true;
                    }
                    fileTransferHandler.setDownloadingProperty(z);
                    if (absolutePath != null && !absolutePath.equals("") && fileTransferStatus.transferComplete.booleanValue()) {
                        FileTransferHandler.this.onFileTransferDone(absolutePath);
                    }
                }
            }, 5);
        }
    }

    public void onFileTransferStatus(final FileTransferStatus fileTransferStatus) {
        if (!this.closed) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    if (fileTransferStatus == null) {
                        return;
                    }
                    if (fileTransferStatus.success.booleanValue() && fileTransferStatus.transferComplete.booleanValue()) {
                        FileTransferHandler.sLogger.d("File Transfer complete " + fileTransferStatus.transferId);
                        FileType fileType = FileTransferHandler.this.fileTransferManager.getFileType(fileTransferStatus.transferId.intValue());
                        FileTransferHandler.this.fileTransferManager.endFileTransferSession(fileTransferStatus.transferId.intValue(), true);
                        if (fileType != null) {
                            FileTransferHandler.this.fileTransferAuthority.onFileSent(fileType);
                            return;
                        } else {
                            FileTransferHandler.sLogger.e("Cannot find FileType associated with transfer ID :" + fileTransferStatus.transferId);
                            return;
                        }
                    }
                    try {
                        if (FileTransferHandler.this.remoteDevice.getLinkBandwidthLevel() <= 0) {
                            FileTransferHandler.sLogger.d("Link bandwidth is low, Not sending the next chunk. Ending the file transfer");
                            FileTransferHandler.this.fileTransferManager.endFileTransferSession(fileTransferStatus.transferId.intValue(), true);
                        } else if (!FileTransferHandler.this.fileTransferManager.handleFileTransferStatus(fileTransferStatus)) {
                            FileTransferHandler.this.fileTransferManager.endFileTransferSession(fileTransferStatus.transferId.intValue(), true);
                        } else if (FileTransferHandler.this.sendNextChunk(fileTransferStatus.transferId.intValue()) != null) {
                            FileTransferHandler.sLogger.d("Sent chunk");
                        } else {
                            FileTransferHandler.sLogger.e("Failed to get the data for the transfer session");
                        }
                    } catch (Throwable throwable) {
                        FileTransferHandler.sLogger.d("Exception while reading the data and sending it across", throwable);
                    }
                }
            }, 5);
        }
    }

    private synchronized void onFileTransferDone(final String absolutePath) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                FileTransferHandler.this.logger.v("onFileTransferDone");
                File file = new File(absolutePath);
                if (file.exists() && file.isFile() && file.canRead()) {
                    Intent intent = new Intent(FileTransferHandler.ACTION_OTA_DOWNLOAD);
                    intent.putExtra("path", file.getAbsolutePath());
                    HudApplication.getAppContext().sendBroadcastAsUser(intent, Process.myUserHandle());
                    return;
                }
                FileTransferHandler.this.logger.e("Cannot read from downloaded OTA file " + file.getAbsolutePath());
            }
        }, 5);
    }

    public void onDeviceConnected(RemoteDevice device) {
        this.remoteDevice = device;
    }

    public void onDeviceDisconnected() {
        this.remoteDevice = null;
    }

    public void close() {
        if (!this.closed) {
            this.closed = true;
            if (this.fileTransferManager != null) {
                this.fileTransferManager.stop();
            }
        }
    }
}
