package com.navdy.hud.app.service;

import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.manager.InputManager;
import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class ShutdownMonitor$$InjectAdapter extends Binding<ShutdownMonitor> implements MembersInjector<ShutdownMonitor> {
    private Binding<Bus> bus;
    private Binding<InputManager> mInputManager;
    private Binding<PowerManager> powerManager;

    public ShutdownMonitor$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.service.ShutdownMonitor", false, ShutdownMonitor.class);
    }

    public void attach(Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", ShutdownMonitor.class, getClass().getClassLoader());
        this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", ShutdownMonitor.class, getClass().getClassLoader());
        this.mInputManager = linker.requestBinding("com.navdy.hud.app.manager.InputManager", ShutdownMonitor.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.powerManager);
        injectMembersBindings.add(this.mInputManager);
    }

    public void injectMembers(ShutdownMonitor object) {
        object.bus = (Bus) this.bus.get();
        object.powerManager = (PowerManager) this.powerManager.get();
        object.mInputManager = (InputManager) this.mInputManager.get();
    }
}
