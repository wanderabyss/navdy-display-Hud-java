package com.navdy.hud.app.service.pandora;

import java.nio.ByteBuffer;

public class DataFrameMessage extends FrameMessage {
    public DataFrameMessage(boolean isSequence0, byte[] payload) throws IllegalArgumentException {
        super(isSequence0, payload);
        if (payload.length == 0) {
            throw new IllegalArgumentException("Empty payload in data frame is not allowed");
        }
    }

    public byte[] buildFrame() {
        byte b = (byte) 0;
        int length = this.payload.length;
        ByteBuffer put = ByteBuffer.allocate(length + 6).put((byte) 0);
        if (!this.isSequence0) {
            b = (byte) 1;
        }
        return FrameMessage.buildFrameFromCRCPart(put.put(b).putInt(length).put(this.payload));
    }

    protected static DataFrameMessage parseDataFrame(byte[] dataFrame) throws IllegalArgumentException {
        if (dataFrame[0] == (byte) 126 && dataFrame[dataFrame.length - 1] == (byte) 124 && dataFrame[1] == (byte) 0) {
            boolean isSequence0;
            ByteBuffer buffer = FrameMessage.unescapeBytes(dataFrame);
            buffer.position(2);
            byte sequence = buffer.get();
            if (sequence == (byte) 0) {
                isSequence0 = true;
            } else if (sequence == (byte) 1) {
                isSequence0 = false;
            } else {
                throw new IllegalArgumentException("Unknown message frame sequence: " + String.format("%02X", new Object[]{Byte.valueOf(sequence)}));
            }
            int length = buffer.getInt();
            if (length == 0) {
                throw new IllegalArgumentException("Data frame has 0 as payload's length");
            }
            byte[] payload = new byte[length];
            buffer.get(payload);
            byte[] crc = new byte[2];
            buffer.get(crc);
            if (buffer.remaining() != 1) {
                throw new IllegalArgumentException("Corrupted message frame");
            }
            byte[] crcData = new byte[(length + 6)];
            buffer.position(1);
            buffer.get(crcData);
            if (CRC16CCITT.checkCRC(crcData, crc)) {
                return new DataFrameMessage(isSequence0, payload);
            }
            throw new IllegalArgumentException("CRC fail");
        }
        throw new IllegalArgumentException("Data frame start/stop/type bytes not in place");
    }
}
