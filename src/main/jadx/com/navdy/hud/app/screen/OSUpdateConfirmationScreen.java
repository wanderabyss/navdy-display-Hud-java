package com.navdy.hud.app.screen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.manager.UpdateReminderManager;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.hud.app.view.UpdateConfirmationView;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import flow.Layout;
import javax.inject.Inject;
import javax.inject.Singleton;

@Layout(R.layout.screen_update_confirmation)
public class OSUpdateConfirmationScreen extends BaseScreen {
    private static final Logger sLogger = new Logger(OSUpdateConfirmationScreen.class);

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {UpdateConfirmationView.class})
    public class Module {
    }

    @Singleton
    public static class Presenter extends BasePresenter<UpdateConfirmationView> {
        private boolean isReminder;
        @Inject
        Bus mBus;
        @Inject
        SharedPreferences mPreferences;

        public void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (savedInstanceState != null) {
                this.isReminder = savedInstanceState.getBoolean(UpdateReminderManager.EXTRA_UPDATE_REMINDER, false);
                return;
            }
            OSUpdateConfirmationScreen.sLogger.e("null bundle passed to onLoad()");
            this.isReminder = false;
        }

        public void install() {
            Intent intent = new Intent(HudApplication.getAppContext(), OTAUpdateService.class);
            intent.putExtra("COMMAND", OTAUpdateService.COMMAND_INSTALL_UPDATE);
            HudApplication.getAppContext().startService(intent);
        }

        public void doNotPrompt() {
            Intent intent = new Intent(HudApplication.getAppContext(), OTAUpdateService.class);
            intent.putExtra("COMMAND", OTAUpdateService.COMMAND_DO_NOT_PROMPT);
            HudApplication.getAppContext().startService(intent);
        }

        public void finish() {
            this.mBus.post(new Builder().screen(Screen.SCREEN_BACK).build());
        }

        public String getUpdateVersion() {
            return OTAUpdateService.getSWUpdateVersion();
        }

        public String getCurrentVersion() {
            return OTAUpdateService.getCurrentVersion();
        }

        public boolean isReminder() {
            return this.isReminder;
        }
    }

    public String getMortarScopeName() {
        return OSUpdateConfirmationScreen.class.getName();
    }

    public Object getDaggerModule() {
        return new Module();
    }

    public Screen getScreen() {
        return Screen.SCREEN_OTA_CONFIRMATION;
    }
}
