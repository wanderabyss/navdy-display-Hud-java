package com.navdy.hud.app.screen;

import com.navdy.hud.app.screen.GestureLearningScreen.Module;
import dagger.internal.ModuleAdapter;

public final class GestureLearningScreen$Module$$ModuleAdapter extends ModuleAdapter<Module> {
    private static final Class<?>[] INCLUDES = new Class[0];
    private static final String[] INJECTS = new String[]{"members/com.navdy.hud.app.view.LearnGestureScreenLayout", "members/com.navdy.hud.app.view.GestureLearningView", "members/com.navdy.hud.app.view.ScrollableTextPresenterLayout", "members/com.navdy.hud.app.view.GestureVideoCaptureView"};
    private static final Class<?>[] STATIC_INJECTIONS = new Class[0];

    public GestureLearningScreen$Module$$ModuleAdapter() {
        super(Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
