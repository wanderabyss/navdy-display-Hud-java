package com.navdy.hud.app.screen;

import android.os.Bundle;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.hud.app.manager.UpdateReminderManager;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.view.DialUpdateConfirmationView;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import flow.Layout;
import javax.inject.Inject;
import javax.inject.Singleton;

@Layout(R.layout.screen_dial_update_confirmation)
public class DialUpdateConfirmationScreen extends BaseScreen {
    private static final Logger sLogger = new Logger(DialUpdateConfirmationScreen.class);

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {DialUpdateConfirmationView.class})
    public class Module {
    }

    @Singleton
    public static class Presenter extends BasePresenter<DialUpdateConfirmationView> {
        private boolean isReminder;
        @Inject
        Bus mBus;

        public void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (savedInstanceState != null) {
                this.isReminder = savedInstanceState.getBoolean(UpdateReminderManager.EXTRA_UPDATE_REMINDER, false);
                return;
            }
            DialUpdateConfirmationScreen.sLogger.e("null bundle passed to onLoad()");
            this.isReminder = false;
        }

        public void install() {
            Bundle args = new Bundle();
            args.putInt(DialUpdateProgressScreen.EXTRA_PROGRESS_CAUSE, 1);
            this.mBus.post(new ShowScreenWithArgs(Screen.SCREEN_DIAL_UPDATE_PROGRESS, args, false));
        }

        public void finish() {
            this.mBus.post(new Builder().screen(Screen.SCREEN_BACK).build());
        }

        public boolean isReminder() {
            return this.isReminder;
        }
    }

    public String getMortarScopeName() {
        return DialUpdateConfirmationScreen.class.getName();
    }

    public Object getDaggerModule() {
        return new Module();
    }

    public Screen getScreen() {
        return Screen.SCREEN_DIAL_UPDATE_CONFIRMATION;
    }
}
