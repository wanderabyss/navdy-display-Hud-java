package com.navdy.hud.app.storage;

import android.content.Intent;
import android.os.Environment;
import android.os.Process;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.device.gps.GpsConstants;
import com.navdy.hud.app.util.CrashReportService;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.util.ReportIssueService;
import com.navdy.hud.app.util.os.PropsFileUpdater;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.events.file.FileType;
import com.navdy.service.library.file.IFileTransferAuthority;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.LogUtils;
import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class PathManager implements IFileTransferAuthority {
    private static final String ACTIVE_ROUTE_DIR = ".activeroute";
    private static final String CAR_MD_RESPONSE_DISK_CACHE_FILES_DIR = "car_md_cache";
    private static String CRASH_INFO_TEXT_FILE_NAME = "info.txt";
    private static final String DATA_LOGS_DIR = "DataLogs";
    private static final String DRIVER_PROFILES_DIR = "DriverProfiles";
    private static final String GESTURE_VIDEOS_SYNC_FOLDER = "gesture_videos";
    private static final String HERE_MAPS_CONFIG_BASE_PATH = "/.here-maps";
    private static final Pattern HERE_MAPS_CONFIG_DIRS_PATTERN = Pattern.compile(HERE_MAPS_CONFIG_DIRS_REGEX);
    private static final String HERE_MAPS_CONFIG_DIRS_REGEX = "[0-9]{10}";
    private static final String HERE_MAP_DATA_DIR = ".here-maps";
    public static final String HERE_MAP_META_JSON_FILE = "meta.json";
    private static final String HUD_DATABASE_DIR = "/.db";
    private static final String IMAGE_DISK_CACHE_FILES_DIR = "img_disk_cache";
    private static final String KERNEL_CRASH_CONSOLE_RAMOOPS = "/sys/fs/pstore/console-ramoops";
    private static final String KERNEL_CRASH_CONSOLE_RAMOOPS_0 = "/sys/fs/pstore/console-ramoops-0";
    private static final String KERNEL_CRASH_DMESG_RAMOOPS = "/sys/fs/pstore/dmesg-ramoops-0";
    private static final String[] KERNEL_CRASH_FILES = new String[]{KERNEL_CRASH_CONSOLE_RAMOOPS, KERNEL_CRASH_CONSOLE_RAMOOPS_0, KERNEL_CRASH_DMESG_RAMOOPS};
    private static final String LOGS_FOLDER = "templogs";
    private static final String MUSIC_LIBRARY_DISK_CACHE_FILES_DIR = "music_disk_cache";
    private static final String NAVIGATION_FILES_DIR = "navigation_issues";
    private static final String NON_FATAL_CRASH_REPORT_DIR = "/sdcard/.logs/snapshot/";
    private static final String SYSTEM_CACHE_DIR = "/cache";
    private static final String TEMP_FILE_TIMESTAMP_FORMAT = "'display_log'_yyyy_MM_dd-HH_mm_ss'.zip'";
    private static final String TIMESTAMP_MWCONFIG_LATEST = DeviceUtil.getCurrentHereSdkTimestamp();
    private static SimpleDateFormat format = new SimpleDateFormat(TEMP_FILE_TIMESTAMP_FORMAT);
    private static final Logger sLogger = new Logger(PathManager.class);
    private static final PathManager sSingleton = new PathManager();
    private String activeRouteInfoDir;
    private String carMdResponseDiskCacheFolder;
    private String databaseDir;
    private String driverProfilesDir;
    private String gestureVideosSyncFolder;
    private File hereMapsConfigDirs;
    private String hereMapsDataDirectory;
    private String hereVoiceSkinsPath;
    private String imageDiskCacheFolder;
    private volatile boolean initialized;
    private final String mapsPartitionPath;
    private String musicDiskCacheFolder;
    private String navigationIssuesFolder;
    private String tempLogsFolder = (HudApplication.getAppContext().getCacheDir().getAbsolutePath() + File.separator + LOGS_FOLDER);

    public static PathManager getInstance() {
        return sSingleton;
    }

    private PathManager() {
        String externalPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        String internalAppPath = HudApplication.getAppContext().getFilesDir().getAbsolutePath();
        this.driverProfilesDir = internalAppPath + File.separator + DRIVER_PROFILES_DIR;
        this.mapsPartitionPath = SystemProperties.get("ro.maps_partition", externalPath);
        this.hereMapsConfigDirs = new File(this.mapsPartitionPath + File.separator + HERE_MAPS_CONFIG_BASE_PATH);
        this.databaseDir = internalAppPath + File.separator + HUD_DATABASE_DIR;
        this.activeRouteInfoDir = internalAppPath + File.separator + ACTIVE_ROUTE_DIR;
        this.navigationIssuesFolder = internalAppPath + File.separator + NAVIGATION_FILES_DIR;
        this.carMdResponseDiskCacheFolder = internalAppPath + File.separator + CAR_MD_RESPONSE_DISK_CACHE_FILES_DIR;
        this.imageDiskCacheFolder = internalAppPath + File.separator + IMAGE_DISK_CACHE_FILES_DIR;
        this.musicDiskCacheFolder = internalAppPath + File.separator + MUSIC_LIBRARY_DISK_CACHE_FILES_DIR;
        this.gestureVideosSyncFolder = this.mapsPartitionPath + File.separator + GESTURE_VIDEOS_SYNC_FOLDER;
        File voiceSkinsFilepath = new File(this.mapsPartitionPath + File.separator + ".here-maps/voices-download");
        sLogger.v("voiceSkins path=" + voiceSkinsFilepath);
        this.hereVoiceSkinsPath = voiceSkinsFilepath.getAbsolutePath();
        this.hereMapsDataDirectory = this.mapsPartitionPath + File.separator + HERE_MAP_DATA_DIR;
    }

    private void init() {
        if (!this.initialized) {
            this.initialized = true;
            new File(this.tempLogsFolder).mkdirs();
            new File(this.databaseDir).mkdirs();
            new File(this.driverProfilesDir).mkdirs();
            new File(this.activeRouteInfoDir).mkdirs();
            new File(this.navigationIssuesFolder).mkdirs();
            new File(this.carMdResponseDiskCacheFolder).mkdirs();
            new File(NON_FATAL_CRASH_REPORT_DIR).mkdirs();
            new File(this.imageDiskCacheFolder).mkdirs();
            new File(this.musicDiskCacheFolder).mkdirs();
            new File(this.hereVoiceSkinsPath).mkdirs();
            new File(this.gestureVideosSyncFolder).mkdirs();
        }
    }

    public List<String> getHereMapsConfigDirs() {
        if (!this.initialized) {
            init();
        }
        List<String> pathList = new ArrayList();
        File[] hereDirs = this.hereMapsConfigDirs.listFiles(new FileFilter() {
            public boolean accept(File file) {
                return file.isDirectory() && PathManager.HERE_MAPS_CONFIG_DIRS_PATTERN.matcher(file.getName()).matches();
            }
        });
        if (hereDirs != null) {
            for (File f : hereDirs) {
                pathList.add(f.getAbsolutePath());
            }
        }
        return pathList;
    }

    public String getLatestHereMapsConfigPath() {
        if (!this.initialized) {
            init();
        }
        return this.hereMapsConfigDirs.getAbsolutePath() + File.separator + TIMESTAMP_MWCONFIG_LATEST;
    }

    public String getHereVoiceSkinsPath() {
        if (!this.initialized) {
            init();
        }
        return this.hereVoiceSkinsPath;
    }

    public String[] getKernelCrashFiles() {
        if (!this.initialized) {
            init();
        }
        return KERNEL_CRASH_FILES;
    }

    public String getDriverProfilesDir() {
        if (!this.initialized) {
            init();
        }
        return this.driverProfilesDir;
    }

    public String getMapsPartitionPath() {
        if (!this.initialized) {
            init();
        }
        return this.mapsPartitionPath;
    }

    public String getHereMapsDataDirectory() {
        if (!this.initialized) {
            init();
        }
        return this.hereMapsDataDirectory;
    }

    public String getGestureVideosSyncFolder() {
        if (!this.initialized) {
            init();
        }
        return this.gestureVideosSyncFolder;
    }

    public boolean isFileTypeAllowed(FileType fileType) {
        switch (fileType) {
            case FILE_TYPE_OTA:
            case FILE_TYPE_LOGS:
            case FILE_TYPE_PERF_TEST:
                return true;
            default:
                return false;
        }
    }

    public String getDirectoryForFileType(FileType fileType) {
        if (!this.initialized) {
            init();
        }
        switch (fileType) {
            case FILE_TYPE_OTA:
                return SYSTEM_CACHE_DIR;
            case FILE_TYPE_LOGS:
                return this.tempLogsFolder;
            default:
                return null;
        }
    }

    public String getFileToSend(FileType fileType) {
        if (fileType == FileType.FILE_TYPE_LOGS) {
            String tempLogFolder = getDirectoryForFileType(fileType);
            String stagingPath = tempLogFolder + File.separator + "stage";
            File stagingDirectory = new File(stagingPath);
            IOUtils.deleteDirectory(HudApplication.getAppContext(), stagingDirectory);
            IOUtils.createDirectory(stagingDirectory);
            try {
                LogUtils.copyComprehensiveSystemLogs(stagingPath);
                collectEnvironmentInfo(stagingPath);
                collectGpsLog(stagingPath);
                DeviceUtil.copyHEREMapsDataInfo(stagingPath);
                DeviceUtil.takeDeviceScreenShot(stagingDirectory.getAbsolutePath() + File.separator + "HUDScreenShot.png");
                List<File> driveLogFiles = ReportIssueService.getLatestDriveLogFiles(1);
                if (driveLogFiles != null && driveLogFiles.size() > 0) {
                    for (File driveLogFile : driveLogFiles) {
                        IOUtils.copyFile(driveLogFile.getAbsolutePath(), stagingDirectory.getAbsolutePath() + File.separator + driveLogFile.getName());
                    }
                }
                File[] logFiles = stagingDirectory.listFiles();
                String absolutePath = tempLogFolder + File.separator + generateTempFileName();
                CrashReportService.compressCrashReportsToZip(logFiles, absolutePath);
                if (new File(absolutePath).exists()) {
                    return absolutePath;
                }
            } catch (Throwable th) {
                return null;
            }
        }
        return null;
    }

    public void onFileSent(FileType fileType) {
        if (fileType == FileType.FILE_TYPE_LOGS) {
            CrashReportService.clearCrashReports();
        }
    }

    public static synchronized String generateTempFileName() {
        String format;
        synchronized (PathManager.class) {
            format = format.format(Long.valueOf(System.currentTimeMillis()));
        }
        return format;
    }

    public String getDatabaseDir() {
        if (!this.initialized) {
            init();
        }
        return this.databaseDir;
    }

    public String getActiveRouteInfoDir() {
        if (!this.initialized) {
            init();
        }
        return this.activeRouteInfoDir;
    }

    public String getNavigationIssuesDir() {
        if (!this.initialized) {
            init();
        }
        return this.navigationIssuesFolder;
    }

    public String getCarMdResponseDiskCacheFolder() {
        if (!this.initialized) {
            init();
        }
        return this.carMdResponseDiskCacheFolder;
    }

    public String getNonFatalCrashReportDir() {
        if (!this.initialized) {
            init();
        }
        return NON_FATAL_CRASH_REPORT_DIR;
    }

    public String getImageDiskCacheFolder() {
        if (!this.initialized) {
            init();
        }
        return this.imageDiskCacheFolder;
    }

    public String getMusicDiskCacheFolder() {
        if (!this.initialized) {
            init();
        }
        return this.musicDiskCacheFolder;
    }

    private void collectGpsLog(String stagingPath) {
        try {
            Intent intent = new Intent(GpsConstants.GPS_COLLECT_LOGS);
            intent.putExtra(GpsConstants.GPS_EXTRA_LOG_PATH, stagingPath);
            HudApplication.getAppContext().sendBroadcastAsUser(intent, Process.myUserHandle());
            GenericUtil.sleep(3000);
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public void collectEnvironmentInfo(String stagingPath) {
        Throwable t;
        Throwable th;
        FileWriter fileWriter = null;
        try {
            FileWriter fileWriter2 = new FileWriter(new File(stagingPath + File.separator + CRASH_INFO_TEXT_FILE_NAME));
            try {
                fileWriter2.write(PropsFileUpdater.readProps());
                IOUtils.closeStream(fileWriter2);
                fileWriter = fileWriter2;
            } catch (Throwable th2) {
                th = th2;
                fileWriter = fileWriter2;
                IOUtils.closeStream(fileWriter);
                throw th;
            }
        } catch (Throwable th3) {
            t = th3;
            sLogger.e(t);
            IOUtils.closeStream(fileWriter);
        }
    }
}
