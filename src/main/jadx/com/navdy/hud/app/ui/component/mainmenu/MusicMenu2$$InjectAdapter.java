package com.navdy.hud.app.ui.component.mainmenu;

import com.navdy.hud.app.storage.cache.MessageCache;
import com.navdy.hud.app.util.MusicArtworkCache;
import com.navdy.service.library.events.audio.MusicCollectionResponse;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class MusicMenu2$$InjectAdapter extends Binding<MusicMenu2> implements MembersInjector<MusicMenu2> {
    private Binding<MusicArtworkCache> musicArtworkCache;
    private Binding<MessageCache<MusicCollectionResponse>> musicCollectionResponseMessageCache;

    public MusicMenu2$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.ui.component.mainmenu.MusicMenu2", false, MusicMenu2.class);
    }

    public void attach(Linker linker) {
        this.musicArtworkCache = linker.requestBinding("com.navdy.hud.app.util.MusicArtworkCache", MusicMenu2.class, getClass().getClassLoader());
        this.musicCollectionResponseMessageCache = linker.requestBinding("com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>", MusicMenu2.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.musicArtworkCache);
        injectMembersBindings.add(this.musicCollectionResponseMessageCache);
    }

    public void injectMembers(MusicMenu2 object) {
        object.musicArtworkCache = (MusicArtworkCache) this.musicArtworkCache.get();
        object.musicCollectionResponseMessageCache = (MessageCache) this.musicCollectionResponseMessageCache.get();
    }
}
