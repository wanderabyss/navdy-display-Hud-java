package com.navdy.hud.app.ui.activity;

import android.view.KeyEvent;
import android.view.Window;
import com.navdy.hud.app.common.BaseActivity;
import com.navdy.hud.app.manager.InputManager;
import javax.inject.Inject;

public abstract class HudBaseActivity extends BaseActivity {
    @Inject
    InputManager inputManager;

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        boolean handled = false;
        if (this.inputManager != null) {
            handled = this.inputManager.onKeyUp(keyCode, event);
        }
        if (handled) {
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean handled = false;
        this.logger.v("keydown:" + keyCode);
        if (this.inputManager != null) {
            handled = this.inputManager.onKeyDown(keyCode, event);
        }
        if (handled) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        boolean handled = false;
        if (this.inputManager != null) {
            handled = this.inputManager.onKeyLongPress(keyCode, event);
        }
        if (handled) {
            return true;
        }
        return super.onKeyLongPress(keyCode, event);
    }

    protected void makeImmersive(boolean keepScreenOn) {
        Window window = getWindow();
        int flags = 4718592;
        if (keepScreenOn) {
            flags = 4718592 | 128;
        }
        window.addFlags(flags);
        window.getDecorView().setSystemUiVisibility(5894);
        this.logger.v("setting immersive mode");
    }
}
