package com.navdy.hud.app.ui.component.homescreen;

import android.widget.ImageView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class TbtEarlyView$$ViewInjector {
    public static void inject(Finder finder, TbtEarlyView target, Object source) {
        target.earlyManeuverImage = (ImageView) finder.findRequiredView(source, R.id.earlyManeuverImage, "field 'earlyManeuverImage'");
    }

    public static void reset(TbtEarlyView target) {
        target.earlyManeuverImage = null;
    }
}
