package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils.TruncateAt;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.HaloView;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State;
import com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.service.library.log.Logger;

public abstract class IconBaseViewHolder extends VerticalViewHolder {
    public static final int FLUCTUATOR_OPACITY_ALPHA = 153;
    public static final int HALO_DELAY_START_DURATION = 100;
    private static final int[] location = new int[2];
    private static final Logger sLogger = VerticalViewHolder.sLogger;
    protected Builder animatorSetBuilder;
    protected CrossFadeImageView crossFadeImageView;
    private Runnable fluctuatorRunnable = new Runnable() {
        public void run() {
            IconBaseViewHolder.this.startFluctuator();
        }
    };
    private DefaultAnimationListener fluctuatorStartListener = new DefaultAnimationListener() {
        public void onAnimationEnd(Animator animation) {
            IconBaseViewHolder.this.itemAnimatorSet = null;
            IconBaseViewHolder.this.handler.removeCallbacks(IconBaseViewHolder.this.fluctuatorRunnable);
            IconBaseViewHolder.this.handler.postDelayed(IconBaseViewHolder.this.fluctuatorRunnable, 100);
        }
    };
    protected HaloView haloView;
    private boolean hasIconFluctuatorColor;
    private boolean hasSubTitle;
    private boolean hasSubTitle2;
    protected ViewGroup iconContainer;
    protected boolean iconScaleAnimationDisabled;
    protected ViewGroup imageContainer;
    protected TextView subTitle;
    protected TextView subTitle2;
    protected boolean textAnimationDisabled;
    protected TextView title;
    private float titleSelectedTopMargin;
    private float titleUnselectedScale;

    static ViewGroup getLayout(ViewGroup parent, int lytId, int subLytId) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewGroup layout = (ViewGroup) inflater.inflate(lytId, parent, false);
        ViewGroup iconContainer = (ViewGroup) layout.findViewById(R.id.iconContainer);
        CrossFadeImageView image = (CrossFadeImageView) inflater.inflate(subLytId, iconContainer, false);
        image.inject(Mode.SMALL);
        image.setId(R.id.vlist_image);
        iconContainer.addView(image);
        return layout;
    }

    public IconBaseViewHolder(ViewGroup layout, VerticalList vlist, Handler handler) {
        super(layout, vlist, handler);
        this.imageContainer = (ViewGroup) layout.findViewById(R.id.imageContainer);
        this.iconContainer = (ViewGroup) layout.findViewById(R.id.iconContainer);
        this.haloView = (HaloView) layout.findViewById(R.id.halo);
        this.haloView.setVisibility(8);
        this.crossFadeImageView = (CrossFadeImageView) layout.findViewById(R.id.vlist_image);
        this.title = (TextView) layout.findViewById(R.id.title);
        this.subTitle = (TextView) layout.findViewById(R.id.subTitle);
        this.subTitle2 = (TextView) layout.findViewById(R.id.subTitle2);
    }

    public void setItemState(State state, AnimationType animation, int duration, boolean startFluctuator) {
        PropertyValuesHolder p1;
        PropertyValuesHolder p2;
        PropertyValuesHolder p3;
        float titleScaleFactor = 0.0f;
        float titleTopMargin = 0.0f;
        float subTitleAlpha = 0.0f;
        float subTitleScaleFactor = 0.0f;
        float subTitle2Alpha = 0.0f;
        float subTitle2ScaleFactor = 0.0f;
        State itemState = state;
        this.animatorSetBuilder = null;
        if (this.textAnimationDisabled) {
            if (animation == AnimationType.MOVE) {
                this.itemAnimatorSet = new AnimatorSet();
                this.animatorSetBuilder = this.itemAnimatorSet.play(ValueAnimator.ofFloat(new float[]{0.0f, 1.0f}));
                return;
            }
            itemState = State.SELECTED;
        }
        switch (itemState) {
            case SELECTED:
                titleScaleFactor = 1.0f;
                titleTopMargin = this.titleSelectedTopMargin;
                subTitleAlpha = 1.0f;
                subTitleScaleFactor = 1.0f;
                subTitle2Alpha = 1.0f;
                subTitle2ScaleFactor = 1.0f;
                break;
            case UNSELECTED:
                titleScaleFactor = this.titleUnselectedScale;
                titleTopMargin = 0.0f;
                subTitleAlpha = 0.0f;
                subTitleScaleFactor = this.titleUnselectedScale;
                subTitle2Alpha = 0.0f;
                subTitle2ScaleFactor = this.titleUnselectedScale;
                break;
        }
        if (!this.hasSubTitle) {
            titleTopMargin = 0.0f;
            subTitleAlpha = 0.0f;
            subTitle2Alpha = 0.0f;
        }
        this.title.setPivotX(0.0f);
        this.title.setPivotY(titleHeight / 2.0f);
        switch (animation) {
            case INIT:
            case NONE:
                this.title.setScaleX(titleScaleFactor);
                this.title.setScaleY(titleScaleFactor);
                ((MarginLayoutParams) this.title.getLayoutParams()).topMargin = (int) titleTopMargin;
                this.title.requestLayout();
                break;
            case MOVE:
                this.itemAnimatorSet = new AnimatorSet();
                p1 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{titleScaleFactor});
                p2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{titleScaleFactor});
                this.animatorSetBuilder = this.itemAnimatorSet.play(ObjectAnimator.ofPropertyValuesHolder(this.title, new PropertyValuesHolder[]{p1, p2}));
                this.animatorSetBuilder.with(VerticalAnimationUtils.animateMargin(this.title, (int) titleTopMargin));
                break;
        }
        this.subTitle.setPivotX(0.0f);
        this.subTitle.setPivotY(subTitleHeight / 2.0f);
        AnimationType animation2 = animation;
        if (!this.hasSubTitle) {
            animation2 = AnimationType.NONE;
        }
        switch (animation2) {
            case INIT:
            case NONE:
                this.subTitle.setAlpha(subTitleAlpha);
                this.subTitle.setScaleX(subTitleScaleFactor);
                this.subTitle.setScaleY(subTitleScaleFactor);
                break;
            case MOVE:
                p1 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{subTitleScaleFactor});
                p2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{subTitleScaleFactor});
                p3 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{subTitleAlpha});
                this.animatorSetBuilder.with(ObjectAnimator.ofPropertyValuesHolder(this.subTitle, new PropertyValuesHolder[]{p1, p2, p3}));
                break;
        }
        this.subTitle2.setPivotX(0.0f);
        this.subTitle2.setPivotY(subTitleHeight / 2.0f);
        animation2 = animation;
        if (!this.hasSubTitle2) {
            animation2 = AnimationType.NONE;
        }
        switch (animation2) {
            case INIT:
            case NONE:
                this.subTitle2.setAlpha(subTitle2Alpha);
                this.subTitle2.setScaleX(subTitle2ScaleFactor);
                this.subTitle2.setScaleY(subTitle2ScaleFactor);
                break;
            case MOVE:
                p1 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{subTitle2ScaleFactor});
                p2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{subTitle2ScaleFactor});
                p3 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{subTitleAlpha});
                this.animatorSetBuilder.with(ObjectAnimator.ofPropertyValuesHolder(this.subTitle2, new PropertyValuesHolder[]{p1, p2, p3}));
                break;
        }
        if (itemState != State.SELECTED || !startFluctuator) {
            return;
        }
        if (this.itemAnimatorSet != null) {
            this.itemAnimatorSet.addListener(this.fluctuatorStartListener);
        } else {
            startFluctuator();
        }
    }

    public void clearAnimation() {
        stopFluctuator();
        stopAnimation();
        this.currentState = null;
        this.layout.setVisibility(0);
        this.layout.setAlpha(1.0f);
    }

    private void setTitle(String str) {
        this.title.setText(str);
    }

    private void setSubTitle(String str, boolean formatted) {
        if (str == null) {
            this.subTitle.setText("");
            this.hasSubTitle = false;
            return;
        }
        int c = getSubTitleColor();
        if (c == 0) {
            this.subTitle.setTextColor(subTitleColor);
        } else {
            this.subTitle.setTextColor(c);
        }
        if (formatted) {
            this.subTitle.setText(Html.fromHtml(str));
        } else {
            this.subTitle.setText(str);
        }
        this.hasSubTitle = true;
    }

    private void setSubTitle2(String str, boolean formatted) {
        if (str == null) {
            this.subTitle2.setText("");
            this.subTitle2.setVisibility(8);
            this.hasSubTitle2 = false;
            return;
        }
        int c = getSubTitle2Color();
        if (c == 0) {
            this.subTitle2.setTextColor(subTitle2Color);
        } else {
            this.subTitle2.setTextColor(c);
        }
        if (formatted) {
            this.subTitle2.setText(Html.fromHtml(str));
        } else {
            this.subTitle2.setText(str);
        }
        this.subTitle2.setVisibility(0);
        this.hasSubTitle2 = true;
    }

    private void setIconFluctuatorColor(int color) {
        if (color != 0) {
            int fluctuatorColor = Color.argb(FLUCTUATOR_OPACITY_ALPHA, Color.red(color), Color.green(color), Color.blue(color));
            this.hasIconFluctuatorColor = true;
            this.haloView.setStrokeColor(fluctuatorColor);
            return;
        }
        this.hasIconFluctuatorColor = false;
    }

    public void startFluctuator() {
        if (this.hasIconFluctuatorColor) {
            this.haloView.setVisibility(0);
            this.haloView.start();
        }
    }

    private void stopFluctuator() {
        if (this.hasIconFluctuatorColor) {
            this.handler.removeCallbacks(this.fluctuatorRunnable);
            this.haloView.setVisibility(8);
            this.haloView.stop();
        }
    }

    public void select(final Model model, final int pos, int duration) {
        if (this.hasIconFluctuatorColor) {
            this.haloView.setVisibility(4);
            this.haloView.stop();
        }
        VerticalAnimationUtils.performClick(this.iconContainer, duration, new Runnable() {
            public void run() {
                ItemSelectionState selectionState = IconBaseViewHolder.this.vlist.getItemSelectionState();
                selectionState.set(model, model.id, pos, -1, -1);
                IconBaseViewHolder.this.vlist.performSelectAction(selectionState);
            }
        });
    }

    private int getSubTitleColor() {
        if (this.extras == null) {
            return 0;
        }
        return getTextColor(Model.SUBTITLE_COLOR);
    }

    private int getSubTitle2Color() {
        if (this.extras == null) {
            return 0;
        }
        return getTextColor(Model.SUBTITLE_2_COLOR);
    }

    private int getTextColor(String property) {
        int i = 0;
        if (this.extras == null) {
            return i;
        }
        String str = (String) this.extras.get(property);
        if (str == null) {
            return i;
        }
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return i;
        }
    }

    public void copyAndPosition(ImageView imageC, TextView titleC, TextView subTitleC, TextView subTitle2C, boolean setImage) {
        View view = this.crossFadeImageView.getBig();
        if (setImage) {
            VerticalAnimationUtils.copyImage((ImageView) view, imageC);
        }
        imageC.setX((float) selectedImageX);
        imageC.setY((float) selectedImageY);
        titleC.setText(this.title.getText());
        titleC.setX((float) selectedTextX);
        this.title.getLocationOnScreen(location);
        titleC.setY((float) (location[1] - rootTopOffset));
        if (this.hasSubTitle) {
            subTitleC.setText(this.subTitle.getText());
            subTitleC.setX((float) selectedTextX);
            this.subTitle.getLocationOnScreen(location);
            subTitleC.setY((float) (location[1] - rootTopOffset));
        } else {
            subTitleC.setText("");
        }
        if (this.hasSubTitle2) {
            subTitle2C.setText(this.subTitle2.getText());
            subTitle2C.setX((float) selectedTextX);
            this.subTitle2.getLocationOnScreen(location);
            subTitle2C.setY((float) (location[1] - rootTopOffset));
            return;
        }
        subTitle2C.setText("");
    }

    public void preBind(Model model, ModelState modelState) {
        ((MarginLayoutParams) this.title.getLayoutParams()).topMargin = (int) model.fontInfo.titleFontTopMargin;
        this.title.setTextSize(model.fontInfo.titleFontSize);
        setMultiLineStyles(this.title, model.fontInfo.titleSingleLine, -1);
        this.title.setLineSpacing(0.0f, 0.9f);
        this.titleSelectedTopMargin = (float) ((int) model.fontInfo.titleFontTopMargin);
        ((MarginLayoutParams) this.subTitle.getLayoutParams()).topMargin = (int) model.fontInfo.subTitleFontTopMargin;
        setMultiLineStyles(this.subTitle, !model.subTitle_2Lines, model.subTitle_2Lines ? R.style.vlist_subtitle_2_line : R.style.vlist_subtitle);
        this.subTitle.setTextSize(model.fontInfo.subTitleFontSize);
        ((MarginLayoutParams) this.subTitle2.getLayoutParams()).topMargin = (int) model.fontInfo.subTitle2FontTopMargin;
        this.subTitle2.setTextSize(model.fontInfo.subTitle2FontSize);
        this.titleUnselectedScale = model.fontInfo.titleScale;
        this.crossFadeImageView.setSmallAlpha(-1.0f);
    }

    private void setMultiLineStyles(TextView view, boolean singleLine, int style) {
        if (style != -1) {
            view.setTextAppearance(view.getContext(), style);
        }
        view.setSingleLine(singleLine);
        if (singleLine) {
            view.setEllipsize(null);
            return;
        }
        view.setMaxLines(2);
        view.setEllipsize(TruncateAt.END);
    }

    public void bind(Model model, ModelState modelState) {
        if (modelState.updateTitle) {
            setTitle(model.title);
        }
        if (modelState.updateSubTitle) {
            if (model.subTitle != null) {
                setSubTitle(model.subTitle, model.subTitleFormatted);
            } else {
                setSubTitle(null, false);
            }
        }
        if (modelState.updateSubTitle2) {
            if (model.subTitle2 != null) {
                setSubTitle2(model.subTitle2, model.subTitle2Formatted);
            } else {
                setSubTitle2(null, false);
            }
        }
        this.textAnimationDisabled = model.noTextAnimation;
        this.iconScaleAnimationDisabled = model.noImageScaleAnimation;
        setIconFluctuatorColor(model.iconFluctuatorColor);
    }
}
