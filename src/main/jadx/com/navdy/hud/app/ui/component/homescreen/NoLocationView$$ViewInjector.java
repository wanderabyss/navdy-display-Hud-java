package com.navdy.hud.app.ui.component.homescreen;

import android.widget.ImageView;
import android.widget.LinearLayout;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class NoLocationView$$ViewInjector {
    public static void inject(Finder finder, NoLocationView target, Object source) {
        target.noLocationImage = (ImageView) finder.findRequiredView(source, R.id.noLocationImage, "field 'noLocationImage'");
        target.noLocationTextContainer = (LinearLayout) finder.findRequiredView(source, R.id.noLocationTextContainer, "field 'noLocationTextContainer'");
    }

    public static void reset(NoLocationView target) {
        target.noLocationImage = null;
        target.noLocationTextContainer = null;
    }
}
