package com.navdy.hud.app.ui.component.main;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereNavController.State;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.ui.component.homescreen.NavigationView;
import com.navdy.service.library.log.Logger;

public class MainLowerView extends FrameLayout {
    private static final Logger sLogger = new Logger(MainLowerView.class);
    private HomeScreenView homeScreenView;
    private NavigationView navigationView;

    public MainLowerView(Context context) {
        super(context);
    }

    public MainLowerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MainLowerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void injectView(View view) {
        sLogger.v("injectView");
        if (initView() && HereMapsManager.getInstance().isInitialized()) {
            removeAllViews();
            addView(view);
            setAlpha(0.0f);
            setVisibility(0);
            if (this.homeScreenView.isNavigationActive()) {
                sLogger.v("injectView: nav active already");
            } else {
                sLogger.v("injectView: set to nav mode");
                this.homeScreenView.setMode(NavigationMode.MAP_ON_ROUTE);
            }
            animate().alpha(1.0f).start();
        }
    }

    public void ejectView() {
        sLogger.v("ejectView");
        if (initView() && HereMapsManager.getInstance().isInitialized() && getVisibility() == 0) {
            animate().cancel();
            setVisibility(8);
            setAlpha(0.0f);
            removeAllViews();
            if (HereNavigationManager.getInstance().getNavController().getState() != State.NAVIGATING) {
                sLogger.v("ejectView: reset to open mode");
                this.homeScreenView.setMode(NavigationMode.MAP);
                return;
            }
            sLogger.v("ejectView: nav active already");
        }
    }

    private boolean initView() {
        if (this.navigationView == null) {
            this.navigationView = RemoteDeviceManager.getInstance().getUiStateManager().getNavigationView();
        }
        if (this.homeScreenView == null) {
            this.homeScreenView = RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView();
        }
        if (this.navigationView == null || this.homeScreenView == null) {
            return false;
        }
        return true;
    }
}
