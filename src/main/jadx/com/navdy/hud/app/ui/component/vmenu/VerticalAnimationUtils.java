package com.navdy.hud.app.ui.component.vmenu;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.squareup.otto.Bus;

public class VerticalAnimationUtils {

    public static class ActionRunnable implements Runnable {
        private Bundle args;
        private Bus bus;
        private boolean ignoreAnimation;
        private Screen screen;

        public ActionRunnable(Bus bus, Screen screen) {
            this(bus, screen, null, false);
        }

        public ActionRunnable(Bus bus, Screen screen, Bundle args, boolean ignoreAnimation) {
            this.bus = bus;
            this.screen = screen;
            this.args = args;
            this.ignoreAnimation = ignoreAnimation;
        }

        public void run() {
            if (this.args == null) {
                this.bus.post(new Builder().screen(this.screen).build());
            } else {
                this.bus.post(new ShowScreenWithArgs(this.screen, this.args, this.ignoreAnimation));
            }
        }
    }

    public static void performClick(View view, int duration, Runnable endAction) {
        performClickDown(view, duration / 2, endAction, true);
    }

    public static void performClickDown(final View view, final int duration, final Runnable endAction, final boolean clickup) {
        view.animate().scaleX(0.8f).scaleY(0.8f).setDuration((long) duration).withEndAction(new Runnable() {
            public void run() {
                if (clickup) {
                    VerticalAnimationUtils.performClickUp(view, duration, endAction);
                } else if (endAction != null) {
                    endAction.run();
                }
            }
        });
    }

    public static void performClickUp(View view, int duration, final Runnable endAction) {
        view.animate().scaleX(1.0f).scaleY(1.0f).setDuration((long) duration).withEndAction(new Runnable() {
            public void run() {
                if (endAction != null) {
                    endAction.run();
                }
            }
        }).start();
    }

    public static void copyImage(ImageView from, ImageView to) {
        Bitmap bitmap = getBitmapFromImageView(from);
        if (bitmap != null) {
            to.setImageBitmap(bitmap);
        }
    }

    public static Bitmap getBitmapFromImageView(ImageView imageView) {
        Drawable drawable = imageView.getDrawable();
        BitmapDrawable bitmapDrawable = null;
        if (drawable instanceof BitmapDrawable) {
            bitmapDrawable = (BitmapDrawable) drawable;
        }
        if (!(imageView instanceof IconColorImageView) && bitmapDrawable != null) {
            return bitmapDrawable.getBitmap();
        }
        Bitmap bitmap = Bitmap.createBitmap(imageView.getWidth(), imageView.getHeight(), Config.ARGB_8888);
        imageView.draw(new Canvas(bitmap));
        return bitmap;
    }

    public static Animator animateMargin(final View view, int toMargin) {
        final MarginLayoutParams layoutParams = (MarginLayoutParams) view.getLayoutParams();
        ValueAnimator marginAnimator = ValueAnimator.ofInt(new int[]{layoutParams.topMargin, toMargin});
        marginAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                layoutParams.topMargin = ((Integer) animation.getAnimatedValue()).intValue();
                view.requestLayout();
            }
        });
        return marginAnimator;
    }

    public static Animator animateDimension(final View view, int toDimension) {
        final MarginLayoutParams layoutParams = (MarginLayoutParams) view.getLayoutParams();
        ValueAnimator dimensionAnimator = ValueAnimator.ofInt(new int[]{layoutParams.width, toDimension});
        dimensionAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                int size = ((Integer) animation.getAnimatedValue()).intValue();
                layoutParams.width = size;
                layoutParams.height = size;
                view.requestLayout();
            }
        });
        return dimensionAnimator;
    }
}
