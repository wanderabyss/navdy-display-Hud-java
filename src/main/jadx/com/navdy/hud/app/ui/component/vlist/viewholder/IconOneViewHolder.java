package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView.Style;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State;
import com.navdy.service.library.log.Logger;

public class IconOneViewHolder extends IconBaseViewHolder {
    private static final Logger sLogger = VerticalViewHolder.sLogger;

    public static Model buildModel(int id, int icon, int iconFluctuatorColor, String title, String subTitle) {
        return buildModel(id, icon, iconFluctuatorColor, title, subTitle, null);
    }

    public static Model buildModel(int id, int icon, int iconFluctuatorColor, String title, String subTitle, String subTitle2) {
        Model model = new Model();
        model.type = ModelType.ICON;
        model.id = id;
        model.icon = icon;
        model.iconFluctuatorColor = iconFluctuatorColor;
        model.title = title;
        model.subTitle = subTitle;
        model.subTitle2 = subTitle2;
        return model;
    }

    public static IconOneViewHolder buildViewHolder(ViewGroup parent, VerticalList vlist, Handler handler) {
        return new IconOneViewHolder(IconBaseViewHolder.getLayout(parent, R.layout.vlist_item, R.layout.crossfade_image_lyt), vlist, handler);
    }

    private IconOneViewHolder(ViewGroup layout, VerticalList vlist, Handler handler) {
        super(layout, vlist, handler);
    }

    public ModelType getModelType() {
        return ModelType.ICON;
    }

    public void setItemState(State state, AnimationType animation, int duration, boolean startFluctuator) {
        super.setItemState(state, animation, duration, startFluctuator);
        float iconScaleFactor = 0.0f;
        float iconAlphaFactor = 0.0f;
        Mode mode = null;
        switch (state) {
            case SELECTED:
                iconScaleFactor = 1.0f;
                iconAlphaFactor = 1.0f;
                mode = Mode.BIG;
                break;
            case UNSELECTED:
                iconScaleFactor = 0.6f;
                iconAlphaFactor = 0.5f;
                mode = Mode.SMALL;
                break;
        }
        switch (animation) {
            case NONE:
            case INIT:
                this.imageContainer.setScaleX(iconScaleFactor);
                this.imageContainer.setScaleY(iconScaleFactor);
                this.imageContainer.setAlpha(iconAlphaFactor);
                this.crossFadeImageView.setMode(mode);
                return;
            case MOVE:
                PropertyValuesHolder p1 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{iconScaleFactor});
                PropertyValuesHolder p2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{iconScaleFactor});
                PropertyValuesHolder p3 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{iconAlphaFactor});
                this.animatorSetBuilder.with(ObjectAnimator.ofPropertyValuesHolder(this.imageContainer, new PropertyValuesHolder[]{p1, p2, p3}));
                if (this.crossFadeImageView.getMode() != mode) {
                    this.animatorSetBuilder.with(this.crossFadeImageView.getCrossFadeAnimator());
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void bind(Model model, ModelState modelState) {
        super.bind(model, modelState);
        setIcon(model.icon, modelState.updateImage, modelState.updateSmallImage);
    }

    private void setIcon(int icon, boolean updateImage, boolean updateSmallImage) {
        String initials = null;
        if (this.extras != null) {
            initials = (String) this.extras.get(Model.INITIALS);
        }
        if (updateImage) {
            Style bigStyle;
            if (initials != null) {
                bigStyle = Style.MEDIUM;
            } else {
                bigStyle = Style.DEFAULT;
            }
            ((InitialsImageView) this.crossFadeImageView.getBig()).setImage(icon, null, bigStyle);
        }
        if (updateSmallImage) {
            Style smallStyle;
            if (initials != null) {
                smallStyle = Style.TINY;
            } else {
                smallStyle = Style.DEFAULT;
            }
            InitialsImageView small = (InitialsImageView) this.crossFadeImageView.getSmall();
            small.setAlpha(0.5f);
            small.setImage(icon, initials, smallStyle);
        }
    }
}
