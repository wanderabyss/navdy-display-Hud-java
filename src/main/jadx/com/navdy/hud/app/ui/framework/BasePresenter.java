package com.navdy.hud.app.ui.framework;

import android.os.Bundle;
import android.view.View;
import mortar.ViewPresenter;

public class BasePresenter<V extends View> extends ViewPresenter<V> {
    protected boolean active;

    public void onLoad(Bundle savedInstanceState) {
        this.active = true;
        super.onLoad(savedInstanceState);
    }

    protected void onUnload() {
    }

    public void dropView(V view) {
        V v = (View) getView();
        if (v != null) {
            if (view == v) {
                this.active = false;
                onUnload();
            }
            super.dropView(v);
        }
    }

    public boolean isActive() {
        return this.active;
    }
}
