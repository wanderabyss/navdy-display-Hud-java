package com.navdy.hud.app.ui.component;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout.Mode;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import java.util.ArrayList;
import java.util.List;

public class ConfirmationLayout extends RelativeLayout {
    public ChoiceLayout choiceLayout;
    private IListener choiceListener;
    private IListener defaultChoiceListener;
    private boolean finished;
    public FluctuatorAnimatorView fluctuatorView;
    private Handler handler;
    public ImageView leftSwipe;
    private Listener listener;
    public ViewGroup mainSection;
    public ImageView rightSwipe;
    public InitialsImageView screenImage;
    public IconColorImageView screenImageIconBkColor;
    public TextView screenTitle;
    public ImageView sideImage;
    private int timeout;
    private Runnable timeoutRunnable;
    public TextView title1;
    public TextView title2;
    public TextView title3;
    public TextView title4;
    public ViewGroup titleContainer;

    public interface Listener {
        void timeout();
    }

    public ConfirmationLayout(Context context) {
        this(context, null);
    }

    public ConfirmationLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ConfirmationLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.handler = new Handler(Looper.getMainLooper());
        this.defaultChoiceListener = new IListener() {
            public void executeItem(int pos, int id) {
                ConfirmationLayout.this.clearTimeout();
                ConfirmationLayout.this.choiceListener.executeItem(pos, id);
            }

            public void itemSelected(int pos, int id) {
                ConfirmationLayout.this.choiceListener.itemSelected(pos, id);
            }
        };
        this.timeoutRunnable = new Runnable() {
            public void run() {
                ConfirmationLayout.this.finished = true;
                ConfirmationLayout.this.listener.timeout();
            }
        };
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.confirmation_lyt, this, true);
        this.screenTitle = (TextView) findViewById(R.id.mainTitle);
        this.screenImage = (InitialsImageView) findViewById(R.id.image);
        this.screenImageIconBkColor = (IconColorImageView) findViewById(R.id.imageIconBkColor);
        this.sideImage = (ImageView) findViewById(R.id.sideImage);
        this.titleContainer = (ViewGroup) findViewById(R.id.infoContainer);
        this.title1 = (TextView) findViewById(R.id.title1);
        this.title2 = (TextView) findViewById(R.id.title2);
        this.title3 = (TextView) findViewById(R.id.title3);
        this.title4 = (TextView) findViewById(R.id.title4);
        this.choiceLayout = (ChoiceLayout) findViewById(R.id.choiceLayout);
        this.fluctuatorView = (FluctuatorAnimatorView) findViewById(R.id.fluctuator);
        this.leftSwipe = (ImageView) findViewById(R.id.leftSwipe);
        this.rightSwipe = (ImageView) findViewById(R.id.rightSwipe);
        this.mainSection = (ViewGroup) findViewById(R.id.mainSection);
    }

    public void setTimeout(int millis, Listener listener) {
        if (!this.finished) {
            if (listener == null || millis <= 0) {
                throw new IllegalArgumentException();
            }
            this.timeout = millis;
            this.listener = listener;
            resetTimeout();
        }
    }

    private void resetTimeout() {
        if (this.timeout > 0) {
            this.handler.removeCallbacks(this.timeoutRunnable);
            this.handler.postDelayed(this.timeoutRunnable, (long) this.timeout);
        }
    }

    private void clearTimeout() {
        this.handler.removeCallbacks(this.timeoutRunnable);
    }

    public void setChoices(List<String> choiceLabels, int initialSelection, IListener listener) {
        this.finished = false;
        clearTimeout();
        if (choiceLabels == null || choiceLabels.size() == 0 || listener == null) {
            throw new IllegalArgumentException();
        }
        this.choiceListener = listener;
        List<Choice> list = new ArrayList();
        for (String str : choiceLabels) {
            list.add(new Choice(str, 0));
        }
        this.choiceLayout.setChoices(Mode.LABEL, list, initialSelection, this.defaultChoiceListener);
    }

    public void setChoicesList(List<Choice> choices, int initialSelection, IListener listener) {
        this.finished = false;
        clearTimeout();
        if (choices == null || choices.size() == 0 || listener == null) {
            throw new IllegalArgumentException();
        }
        this.choiceListener = listener;
        this.choiceLayout.setChoices(Mode.LABEL, choices, initialSelection, this.defaultChoiceListener);
    }

    public void moveSelectionLeft() {
        if (!this.finished) {
            resetTimeout();
            this.choiceLayout.moveSelectionLeft();
        }
    }

    public void moveSelectionRight() {
        if (!this.finished) {
            resetTimeout();
            this.choiceLayout.moveSelectionRight();
        }
    }

    public void executeSelectedItem(boolean animated) {
        if (!this.finished) {
            resetTimeout();
            this.choiceLayout.executeSelectedItem(animated);
        }
    }

    public boolean handleKey(CustomKeyEvent event) {
        switch (event) {
            case LEFT:
                moveSelectionLeft();
                return true;
            case RIGHT:
                moveSelectionRight();
                return true;
            case SELECT:
                executeSelectedItem(true);
                return true;
            default:
                return false;
        }
    }
}
