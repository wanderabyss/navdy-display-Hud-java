package com.navdy.hud.app.ui.component.tbt;

import android.animation.Animator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"}, d2 = {"com/navdy/hud/app/ui/component/tbt/TbtViewContainer$updateDisplay$1", "Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;", "(Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;)V", "onAnimationEnd", "", "animation", "Landroid/animation/Animator;", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: TbtViewContainer.kt */
public final class TbtViewContainer$updateDisplay$1 extends DefaultAnimationListener {
    final /* synthetic */ TbtViewContainer this$0;

    TbtViewContainer$updateDisplay$1(TbtViewContainer $outer) {
        this.this$0 = $outer;
    }

    public void onAnimationEnd(@NotNull Animator animation) {
        Intrinsics.checkParameterIsNotNull(animation, "animation");
        TbtView access$getInactiveView$p = this.this$0.inactiveView;
        if (access$getInactiveView$p != null) {
            access$getInactiveView$p.setTranslationY(-TbtViewContainer.Companion.getItemHeight());
        }
    }
}
