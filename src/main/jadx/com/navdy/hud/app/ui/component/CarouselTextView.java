package com.navdy.hud.app.ui.component;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.widget.TextView;
import com.navdy.hud.app.R;
import java.util.ArrayList;
import java.util.List;

public class CarouselTextView extends TextView {
    private static final long CAROUSEL_ANIM_DURATION = 500;
    private static final long CAROUSEL_VIEW_INTERVAL = 5000;
    private Runnable carouselChangeRunnable = new Runnable() {
        public void run() {
            if (CarouselTextView.this.textList != null) {
                int size = CarouselTextView.this.textList.size();
                if (size > 0) {
                    CarouselTextView.this.currentTextIndex = (CarouselTextView.this.currentTextIndex + 1) % size;
                    CarouselTextView.this.rotateWithAnimation((String) CarouselTextView.this.textList.get(CarouselTextView.this.currentTextIndex));
                    CarouselTextView.this.handler.postDelayed(this, 5000);
                }
            }
        }
    };
    private int currentTextIndex;
    private final Handler handler = new Handler(Looper.getMainLooper());
    private List<String> textList;
    private final float translationY = getResources().getDimension(R.dimen.text_fade_anim_translationy);

    public CarouselTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setCarousel(int... resIds) {
        this.handler.removeCallbacks(this.carouselChangeRunnable);
        this.textList = new ArrayList();
        for (int resId : resIds) {
            this.textList.add(getResources().getString(resId));
        }
        this.currentTextIndex = -1;
        this.handler.post(this.carouselChangeRunnable);
    }

    private void rotateWithAnimation(final String text) {
        animate().translationY(this.translationY).alpha(0.0f).setDuration(500).withEndAction(new Runnable() {
            public void run() {
                CarouselTextView.this.setText(text);
                CarouselTextView.this.animate().translationY(0.0f).alpha(1.0f).setDuration(500);
            }
        });
    }

    public void clear() {
        animate().cancel();
        this.handler.removeCallbacks(this.carouselChangeRunnable);
        this.textList = null;
        this.currentTextIndex = -1;
    }
}
