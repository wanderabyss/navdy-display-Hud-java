package com.navdy.hud.app.ui.component.mainmenu;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.hud.app.framework.contacts.Contact;
import com.navdy.hud.app.framework.contacts.ContactImageHelper;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.glympse.GlympseManager;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.ui.component.UISettings;
import com.navdy.hud.app.ui.component.destination.DestinationParcelable;
import com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType;
import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen;
import com.navdy.hud.app.ui.component.image.InitialsImageView.Style;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter;
import com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu.Callback;
import com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu.Mode;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.TitleViewHolder;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import java.util.ArrayList;
import java.util.List;
import org.jetbrains.annotations.NotNull;

class ContactOptionsMenu implements IMenu {
    private static final Model back;
    private static final int backColor = resources.getColor(R.color.mm_back);
    private static final int callColor = resources.getColor(R.color.mm_contacts);
    private static final Logger logger = new Logger(ContactOptionsMenu.class);
    private static final int messageColor = resources.getColor(R.color.share_location);
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private static final int shareLocationColor = resources.getColor(R.color.share_location);
    private static final int shareTripLocationColor = resources.getColor(R.color.mm_active_trip);
    private int backSelection;
    private int backSelectionId;
    private final Bus bus;
    private List<Model> cachedList;
    private final List<Contact> contacts;
    private boolean hasScrollModel;
    private MessagePickerMenu messagePickerMenu;
    private final String notifId;
    private Contact numberPicked;
    private final IMenu parent;
    private final Presenter presenter;
    private final VerticalMenuComponent vscrollComponent;

    static {
        String title = resources.getString(R.string.back);
        int fluctuatorColor = backColor;
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
    }

    ContactOptionsMenu(List<Contact> contacts, VerticalMenuComponent vscrollComponent, Presenter presenter, IMenu parent, Bus bus) {
        this(contacts, null, vscrollComponent, presenter, parent, bus);
    }

    ContactOptionsMenu(List<Contact> contacts, String notifId, VerticalMenuComponent vscrollComponent, Presenter presenter, IMenu parent, Bus bus) {
        this.contacts = contacts;
        this.notifId = notifId;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
        this.bus = bus;
    }

    public List<Model> getItems() {
        if (this.cachedList != null) {
            return this.cachedList;
        }
        List<Model> list = new ArrayList();
        if (this.notifId == null) {
            list.add(back);
        } else {
            list.add(TitleViewHolder.buildModel(resources.getString(R.string.pick_reply)));
        }
        list.add(IconBkColorViewHolder.buildModel(R.id.menu_call, R.drawable.icon_mm_contacts_2, callColor, MainMenu.bkColorUnselected, callColor, resources.getString(R.string.call), null));
        boolean glympseManagerSynced = GlympseManager.getInstance().isSynced();
        logger.v("glympse synced:" + glympseManagerSynced);
        if (HereMapsManager.getInstance().isInitialized() && glympseManagerSynced) {
            if (HereNavigationManager.getInstance().isNavigationModeOn()) {
                list.add(IconBkColorViewHolder.buildModel(R.id.menu_share_trip, R.drawable.icon_badge_active_trip, shareTripLocationColor, MainMenu.bkColorUnselected, shareTripLocationColor, resources.getString(R.string.share_trip), null));
            } else {
                list.add(IconBkColorViewHolder.buildModel(R.id.menu_share_location, R.drawable.icon_share_location, shareLocationColor, MainMenu.bkColorUnselected, shareLocationColor, resources.getString(R.string.share_location), null));
            }
        }
        boolean addMessage = false;
        DeviceInfo deviceInfo = RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
        if (deviceInfo != null) {
            switch (deviceInfo.platform) {
                case PLATFORM_Android:
                    addMessage = true;
                    break;
                case PLATFORM_iOS:
                    if (UISettings.supportsIosSms()) {
                        addMessage = true;
                        break;
                    }
                    break;
            }
        }
        if (addMessage) {
            list.add(IconBkColorViewHolder.buildModel(R.id.menu_message, R.drawable.icon_message, messageColor, MainMenu.bkColorUnselected, messageColor, resources.getString(R.string.message), null));
        }
        this.cachedList = list;
        return list;
    }

    public int getInitialSelection() {
        return (this.cachedList == null || this.cachedList.isEmpty() || this.notifId != null) ? 0 : 1;
    }

    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        Contact contact = (Contact) this.contacts.get(0);
        if (this.contacts.size() == 1) {
            Bitmap bitmap = PicassoUtil.getBitmapfromCache(PhoneImageDownloader.getInstance().getImagePath(contact.number, PhotoType.PHOTO_CONTACT));
            if (bitmap != null) {
                this.vscrollComponent.setSelectedIconImage(bitmap);
            } else if (TextUtils.isEmpty(contact.name)) {
                this.vscrollComponent.setSelectedIconImage(R.drawable.icon_user_bg_4, null, Style.LARGE);
            } else {
                this.vscrollComponent.setSelectedIconImage(ContactImageHelper.getInstance().getResourceId(contact.defaultImageIndex), contact.initials, Style.LARGE);
            }
        } else {
            ContactImageHelper contactImageHelper = ContactImageHelper.getInstance();
            this.vscrollComponent.setSelectedIconImage(contactImageHelper.getResourceId(contactImageHelper.getContactImageIndex(contact.name)), contact.initials, Style.LARGE);
        }
        if (TextUtils.isEmpty(contact.name)) {
            this.vscrollComponent.selectedText.setText(contact.formattedNumber);
        } else if (this.contacts.size() != 1 || TextUtils.isEmpty(contact.numberTypeStr)) {
            this.vscrollComponent.selectedText.setText(contact.name);
        } else {
            this.vscrollComponent.selectedText.setText(contact.name + GlanceConstants.NEWLINE + contact.numberTypeStr);
        }
    }

    public boolean selectItem(ItemSelectionState selection) {
        logger.v("select id:" + selection.id + " pos:" + selection.pos);
        int selectionPos = selection.pos;
        final Contact contact = (Contact) this.contacts.get(0);
        switch (selection.id) {
            case R.id.menu_back:
                logger.v("back");
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId, this.hasScrollModel);
                this.backSelectionId = 0;
                break;
            case R.id.menu_call:
                logger.v("call contact");
                if (this.contacts.size() != 1) {
                    this.presenter.loadMenu(new NumberPickerMenu(this.vscrollComponent, this.presenter, this, Mode.CALL, this.contacts, null, R.drawable.icon_mm_contacts_2, callColor, new Callback() {
                        public void selected(@NotNull Contact selectedContact) {
                            ContactOptionsMenu.this.makeCall(selectedContact);
                        }
                    }), MenuLevel.SUB_LEVEL, selection.pos, 0);
                    break;
                }
                AnalyticsSupport.recordMenuSelection("call_contact");
                this.presenter.performSelectionAnimation(new Runnable() {
                    public void run() {
                        ContactOptionsMenu.this.presenter.close(new Runnable() {
                            public void run() {
                                ContactOptionsMenu.this.makeCall(contact);
                            }
                        });
                    }
                });
                break;
            case R.id.menu_message:
                logger.v("send message");
                AnalyticsSupport.recordMenuSelection("send_message");
                if (this.contacts.size() != 1) {
                    this.presenter.loadMenu(new NumberPickerMenu(this.vscrollComponent, this.presenter, this, Mode.MESSAGE, this.contacts, this.notifId, R.drawable.icon_message, messageColor, new Callback() {
                        public void selected(@NotNull Contact selectedContact) {
                        }
                    }), MenuLevel.SUB_LEVEL, selection.pos, 0);
                    break;
                }
                if (this.messagePickerMenu == null) {
                    this.messagePickerMenu = new MessagePickerMenu(this.vscrollComponent, this.presenter, this, GlanceConstants.getCannedMessages(), contact, this.notifId);
                }
                this.presenter.loadMenu(this.messagePickerMenu, MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            case R.id.menu_share_location:
                logger.v("share location");
                AnalyticsSupport.recordMenuSelection("share_location");
                if (this.contacts.size() != 1) {
                    this.presenter.loadMenu(new NumberPickerMenu(this.vscrollComponent, this.presenter, this, Mode.SHARE_TRIP, this.contacts, null, R.drawable.icon_share_location, shareLocationColor, new Callback() {
                        public void selected(@NotNull Contact selectedContact) {
                            ContactOptionsMenu.this.launchShareTripMenu(selectedContact);
                        }
                    }), MenuLevel.SUB_LEVEL, selection.pos, 0);
                    break;
                }
                launchShareTripMenu(contact);
                break;
            case R.id.menu_share_trip:
                logger.v("share trip");
                AnalyticsSupport.recordMenuSelection("share_trip");
                if (this.contacts.size() != 1) {
                    this.presenter.loadMenu(new NumberPickerMenu(this.vscrollComponent, this.presenter, this, Mode.SHARE_TRIP, this.contacts, null, R.drawable.icon_badge_active_trip, shareTripLocationColor, new Callback() {
                        public void selected(@NotNull Contact selectedContact) {
                            ContactOptionsMenu.this.launchShareTripMenu(selectedContact);
                        }
                    }), MenuLevel.SUB_LEVEL, selection.pos, 0);
                    break;
                }
                launchShareTripMenu(contact);
                break;
        }
        return true;
    }

    public Menu getType() {
        return Menu.CONTACT_OPTIONS;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    public Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(Model model, View view, int pos, ModelState state) {
    }

    public IMenu getChildMenu(IMenu parent, String args, String path) {
        return null;
    }

    public void onUnload(MenuLevel level) {
    }

    public void onItemSelected(ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return this.notifId == null;
    }

    private void launchShareTripMenu(final Contact contact) {
        final Bundle args = new Bundle();
        args.putInt(DestinationPickerScreen.PICKER_DESTINATION_ICON, R.drawable.icon_pin_dot_destination_blue);
        args.putInt(DestinationPickerScreen.PICKER_LEFT_ICON, R.drawable.icon_message);
        args.putInt(DestinationPickerScreen.PICKER_LEFT_ICON_BKCOLOR, resources.getColor(R.color.share_location_trip_color));
        GeoCoordinate geoCoordinate = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
        Coordinate destination = null;
        NavigationRouteRequest request = HereNavigationManager.getInstance().getCurrentNavigationRouteRequest();
        if (request != null) {
            destination = request.destination;
        }
        args.putBoolean(DestinationPickerScreen.PICKER_SHOW_ROUTE_MAP, true);
        if (geoCoordinate != null) {
            args.putDouble(DestinationPickerScreen.PICKER_MAP_START_LAT, geoCoordinate.getLatitude());
            args.putDouble(DestinationPickerScreen.PICKER_MAP_START_LNG, geoCoordinate.getLongitude());
        }
        String title = getContactName(contact);
        if (destination != null) {
            args.putDouble(DestinationPickerScreen.PICKER_MAP_END_LAT, destination.latitude.doubleValue());
            args.putDouble(DestinationPickerScreen.PICKER_MAP_END_LNG, destination.longitude.doubleValue());
            args.putString(DestinationPickerScreen.PICKER_TITLE, resources.getString(R.string.share_your_trip_with, new Object[]{title}));
        } else {
            args.putString(DestinationPickerScreen.PICKER_TITLE, resources.getString(R.string.share_your_location_with, new Object[]{title}));
        }
        String[] messages = GlympseManager.getInstance().getMessages();
        DestinationParcelable[] destinationParcelables = new DestinationParcelable[messages.length];
        String currentRouteId = HereNavigationManager.getInstance().getCurrentRouteId();
        for (int i = 0; i < messages.length; i++) {
            destinationParcelables[i] = getDestinationParcelable(messages[i]);
            if (currentRouteId != null) {
                destinationParcelables[i].setRouteId(currentRouteId);
            }
        }
        args.putParcelableArray(DestinationPickerScreen.PICKER_DESTINATIONS, destinationParcelables);
        this.presenter.close(new Runnable() {
            public void run() {
                ContactOptionsMenu.this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_DESTINATION_PICKER, args, new ShareTripMenu(contact, ContactOptionsMenu.this.notifId), false));
            }
        });
    }

    private DestinationParcelable getDestinationParcelable(String message) {
        return new DestinationParcelable(0, message, null, false, null, false, null, 0.0d, 0.0d, 0.0d, 0.0d, R.drawable.icon_message, 0, shareTripLocationColor, MainMenu.bkColorUnselected, DestinationType.NONE, null);
    }

    private String getContactName(Contact contact) {
        if (!TextUtils.isEmpty(contact.name)) {
            return contact.name;
        }
        if (TextUtils.isEmpty(contact.formattedNumber)) {
            return "";
        }
        return contact.formattedNumber;
    }

    private void removeNotification() {
        if (this.notifId != null) {
            NotificationManager.getInstance().removeNotification(this.notifId);
        }
    }

    private void makeCall(Contact contact) {
        removeNotification();
        RemoteDeviceManager.getInstance().getCallManager().dial(contact.number, null, contact.name);
    }

    public void setScrollModel(boolean b) {
        this.hasScrollModel = b;
    }
}
