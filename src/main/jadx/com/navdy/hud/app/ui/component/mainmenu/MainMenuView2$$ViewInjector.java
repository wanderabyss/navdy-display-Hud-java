package com.navdy.hud.app.ui.component.mainmenu;

import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.ConfirmationLayout;

public class MainMenuView2$$ViewInjector {
    public static void inject(Finder finder, MainMenuView2 target, Object source) {
        target.rightBackground = finder.findRequiredView(source, R.id.rightBackground, "field 'rightBackground'");
        target.confirmationLayout = (ConfirmationLayout) finder.findRequiredView(source, R.id.confirmationLayout, "field 'confirmationLayout'");
    }

    public static void reset(MainMenuView2 target) {
        target.rightBackground = null;
        target.confirmationLayout = null;
    }
}
