package com.navdy.hud.app.ui.component.homescreen;

import com.navdy.hud.app.ui.component.homescreen.HomeScreen.Module;
import dagger.internal.ModuleAdapter;

public final class HomeScreen$Module$$ModuleAdapter extends ModuleAdapter<Module> {
    private static final Class<?>[] INCLUDES = new Class[0];
    private static final String[] INJECTS = new String[]{"members/com.navdy.hud.app.ui.component.homescreen.HomeScreenView"};
    private static final Class<?>[] STATIC_INJECTIONS = new Class[0];

    public HomeScreen$Module$$ModuleAdapter() {
        super(Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
