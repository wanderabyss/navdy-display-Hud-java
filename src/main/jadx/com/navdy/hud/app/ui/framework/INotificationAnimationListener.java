package com.navdy.hud.app.ui.framework;

import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;

public interface INotificationAnimationListener {
    void onStart(String str, NotificationType notificationType, Mode mode);

    void onStop(String str, NotificationType notificationType, Mode mode);
}
