package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.service.library.log.Logger;

public class ContentLoadingViewHolder extends VerticalViewHolder {
    private static final Logger sLogger = new Logger(ContentLoadingViewHolder.class);
    private Builder animatorSetBuilder;
    private CrossFadeImageView crossFadeImageView;
    private ViewGroup iconContainer;
    private ViewGroup imageContainer;
    private AnimationDrawable loopAnimation;
    private DefaultAnimationListener loopEnd = new DefaultAnimationListener() {
        public void onAnimationStart(Animator animation) {
            ContentLoadingViewHolder.this.unSelImageView.setVisibility(0);
            if (ContentLoadingViewHolder.this.loopAnimation != null && ContentLoadingViewHolder.this.loopAnimation.isRunning()) {
                ContentLoadingViewHolder.this.loopAnimation.stop();
            }
        }

        public void onAnimationEnd(Animator animation) {
            ContentLoadingViewHolder.this.loopImageView.setVisibility(8);
        }
    };
    private ImageView loopImageView;
    private DefaultAnimationListener loopStart = new DefaultAnimationListener() {
        public void onAnimationStart(Animator animation) {
            ContentLoadingViewHolder.this.loopImageView.setVisibility(0);
        }

        public void onAnimationEnd(Animator animation) {
            ContentLoadingViewHolder.this.unSelImageView.setVisibility(8);
            if (ContentLoadingViewHolder.this.loopAnimation != null && !ContentLoadingViewHolder.this.loopAnimation.isRunning()) {
                ContentLoadingViewHolder.this.loopAnimation.start();
            }
        }
    };
    private ImageView unSelImageView;

    public static Model buildModel(int icon, int iconSelectedColor, int iconDeselectedColor) {
        Model model = VerticalModelCache.getFromCache(ModelType.LOADING_CONTENT);
        if (model == null) {
            model = new Model();
        }
        model.type = ModelType.LOADING_CONTENT;
        model.id = R.id.vlist_content_loading;
        model.icon = icon;
        model.iconSelectedColor = iconSelectedColor;
        model.iconDeselectedColor = iconDeselectedColor;
        return model;
    }

    public static ContentLoadingViewHolder buildViewHolder(ViewGroup parent, VerticalList vlist, Handler handler) {
        ViewGroup layout = (ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.vlist_content_loading, parent, false);
        CrossFadeImageView image = (CrossFadeImageView) ((ViewGroup) layout.findViewById(R.id.iconContainer)).findViewById(R.id.crossFadeImageView);
        image.inject(Mode.SMALL);
        image.setId(R.id.vlist_image);
        return new ContentLoadingViewHolder(layout, vlist, handler);
    }

    private ContentLoadingViewHolder(ViewGroup layout, VerticalList vlist, Handler handler) {
        super(layout, vlist, handler);
        this.imageContainer = (ViewGroup) layout.findViewById(R.id.imageContainer);
        this.iconContainer = (ViewGroup) layout.findViewById(R.id.iconContainer);
        this.crossFadeImageView = (CrossFadeImageView) layout.findViewById(R.id.vlist_image);
        this.loopImageView = (ImageView) layout.findViewById(R.id.img_loop);
        this.unSelImageView = (ImageView) layout.findViewById(R.id.img);
        this.loopAnimation = (AnimationDrawable) this.loopImageView.getDrawable();
    }

    public ModelType getModelType() {
        return ModelType.LOADING_CONTENT;
    }

    public void clearAnimation() {
        if (this.loopAnimation != null && this.loopAnimation.isRunning()) {
            this.loopAnimation.stop();
        }
        stopAnimation();
        this.currentState = null;
        this.layout.setVisibility(0);
        this.layout.setAlpha(1.0f);
    }

    public void select(Model model, int pos, int duration) {
    }

    public void copyAndPosition(ImageView imageC, TextView titleC, TextView subTitleC, TextView subTitle2C, boolean setImage) {
    }

    public void setItemState(State state, AnimationType animation, int duration, boolean startFluctuator) {
        float iconScaleFactor = 0.0f;
        Mode mode = null;
        this.animatorSetBuilder = null;
        switch (state) {
            case SELECTED:
                iconScaleFactor = 1.0f;
                mode = Mode.BIG;
                break;
            case UNSELECTED:
                iconScaleFactor = 0.6f;
                mode = Mode.SMALL;
                break;
        }
        switch (animation) {
            case NONE:
            case INIT:
                this.imageContainer.setScaleX(iconScaleFactor);
                this.imageContainer.setScaleY(iconScaleFactor);
                this.crossFadeImageView.setMode(mode);
                if (state == State.SELECTED) {
                    this.loopImageView.setVisibility(0);
                    this.loopImageView.setAlpha(1.0f);
                    this.unSelImageView.setVisibility(8);
                    this.unSelImageView.setAlpha(0.0f);
                    if (this.loopAnimation != null && !this.loopAnimation.isRunning()) {
                        this.loopAnimation.start();
                        return;
                    }
                    return;
                }
                this.loopImageView.setVisibility(8);
                this.loopImageView.setAlpha(0.0f);
                this.unSelImageView.setVisibility(0);
                this.unSelImageView.setAlpha(1.0f);
                if (this.loopAnimation != null && this.loopAnimation.isRunning()) {
                    this.loopAnimation.stop();
                    return;
                }
                return;
            case MOVE:
                this.itemAnimatorSet = new AnimatorSet();
                PropertyValuesHolder p1 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{iconScaleFactor});
                PropertyValuesHolder p2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{iconScaleFactor});
                this.animatorSetBuilder = this.itemAnimatorSet.play(ObjectAnimator.ofPropertyValuesHolder(this.imageContainer, new PropertyValuesHolder[]{p1, p2}));
                if (this.crossFadeImageView.getMode() != mode) {
                    this.animatorSetBuilder.with(this.crossFadeImageView.getCrossFadeAnimator());
                }
                if (state == State.SELECTED) {
                    this.itemAnimatorSet.addListener(this.loopStart);
                    this.animatorSetBuilder.with(ObjectAnimator.ofFloat(this.loopImageView, View.ALPHA, new float[]{1.0f}));
                    this.animatorSetBuilder.with(ObjectAnimator.ofFloat(this.unSelImageView, View.ALPHA, new float[]{0.0f}));
                    return;
                }
                this.itemAnimatorSet.addListener(this.loopEnd);
                this.animatorSetBuilder.with(ObjectAnimator.ofFloat(this.unSelImageView, View.ALPHA, new float[]{1.0f}));
                this.animatorSetBuilder.with(ObjectAnimator.ofFloat(this.loopImageView, View.ALPHA, new float[]{0.0f}));
                return;
            default:
                return;
        }
    }

    public void preBind(Model model, ModelState modelState) {
        this.crossFadeImageView.setSmallAlpha(-1.0f);
        ((IconColorImageView) this.crossFadeImageView.getBig()).setIconShape(model.iconShape);
        ((IconColorImageView) this.crossFadeImageView.getSmall()).setIconShape(model.iconShape);
    }

    public void bind(Model model, ModelState modelState) {
        setIcon(model.icon, model.iconSelectedColor, model.iconDeselectedColor);
    }

    private void setIcon(int icon, int iconSelectedColor, int iconDeselectedColor) {
        IconColorImageView big = (IconColorImageView) this.crossFadeImageView.getBig();
        big.setDraw(true);
        big.setIcon(icon, iconSelectedColor, null, 0.83f);
        IconColorImageView small = (IconColorImageView) this.crossFadeImageView.getSmall();
        small.setDraw(true);
        small.setIcon(icon, iconDeselectedColor, null, 0.83f);
    }
}
