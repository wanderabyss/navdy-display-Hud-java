package com.navdy.hud.app.ui.component.homescreen;

import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class EtaView$$ViewInjector {
    public static void inject(Finder finder, EtaView target, Object source) {
        target.etaTextView = (TextView) finder.findRequiredView(source, R.id.etaTextView, "field 'etaTextView'");
        target.etaTimeLeft = (TextView) finder.findRequiredView(source, R.id.etaTimeLeft, "field 'etaTimeLeft'");
        target.etaTimeAmPm = (TextView) finder.findRequiredView(source, R.id.etaTimeAmPm, "field 'etaTimeAmPm'");
    }

    public static void reset(EtaView target) {
        target.etaTextView = null;
        target.etaTimeLeft = null;
        target.etaTimeAmPm = null;
    }
}
