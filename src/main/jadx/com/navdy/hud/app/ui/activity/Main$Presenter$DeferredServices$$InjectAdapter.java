package com.navdy.hud.app.ui.activity;

import com.navdy.hud.app.ancs.AncsServiceConnector;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.ui.activity.Main.Presenter.DeferredServices;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class Main$Presenter$DeferredServices$$InjectAdapter extends Binding<DeferredServices> implements MembersInjector<DeferredServices> {
    private Binding<AncsServiceConnector> ancsService;
    private Binding<GestureServiceConnector> gestureService;

    public Main$Presenter$DeferredServices$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.ui.activity.Main$Presenter$DeferredServices", false, DeferredServices.class);
    }

    public void attach(Linker linker) {
        this.gestureService = linker.requestBinding("com.navdy.hud.app.gesture.GestureServiceConnector", DeferredServices.class, getClass().getClassLoader());
        this.ancsService = linker.requestBinding("com.navdy.hud.app.ancs.AncsServiceConnector", DeferredServices.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.gestureService);
        injectMembersBindings.add(this.ancsService);
    }

    public void injectMembers(DeferredServices object) {
        object.gestureService = (GestureServiceConnector) this.gestureService.get();
        object.ancsService = (AncsServiceConnector) this.ancsService.get();
    }
}
