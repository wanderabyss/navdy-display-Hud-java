package com.navdy.hud.app.ui.component.homescreen;

import android.animation.AnimatorSet.Builder;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.FrameLayout;
import butterknife.ButterKnife;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.gesture.GestureDetector.GestureListener;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.manager.SpeedManager.SpeedDataExpired;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnit;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnitChanged;
import com.navdy.hud.app.maps.MapEvents.GPSSpeedEvent;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent;
import com.navdy.hud.app.obd.ObdManager.ObdPidChangeEvent;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode;
import com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants.Type;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

public class BaseSmartDashView extends FrameLayout implements IInputHandler, GestureListener, IHomeScreenLifecycle {
    protected Bus bus;
    private Handler handler;
    protected HomeScreenView homeScreenView;
    private int lastObdSpeed;
    private Logger logger;
    private SpeedManager speedManager;
    protected UIStateManager uiStateManager;

    public BaseSmartDashView(Context context) {
        this(context, null);
    }

    public BaseSmartDashView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseSmartDashView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.lastObdSpeed = -1;
        this.handler = new Handler();
    }

    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        if (!isInEditMode()) {
            ButterKnife.inject((View) this);
            this.speedManager = SpeedManager.getInstance();
            RemoteDeviceManager remoteDeviceManager = RemoteDeviceManager.getInstance();
            this.bus = remoteDeviceManager.getBus();
            this.uiStateManager = remoteDeviceManager.getUiStateManager();
            initViews();
        }
    }

    private void initViews() {
        if (this.uiStateManager != null) {
            if (this.uiStateManager.getCurrentDashboardType() == null) {
                this.logger.v("setting initial dashtype = " + Type.Smart);
                this.uiStateManager.setCurrentDashboardType(Type.Smart);
            }
            setDashboard(this.uiStateManager.getCurrentDashboardType());
        }
        if (TTSUtils.isDebugTTSEnabled()) {
            this.handler.postDelayed(new Runnable() {
                public void run() {
                    String str;
                    if (SpeedManager.getInstance().getObdSpeed() != -1) {
                        str = TTSUtils.DEBUG_TTS_USING_OBD_SPEED;
                    } else {
                        str = TTSUtils.DEBUG_TTS_USING_GPS_SPEED;
                    }
                    TTSUtils.debugShowSpeedInput(str);
                }
            }, 60000);
        }
    }

    public void setView(CustomAnimationMode mode) {
        setMiddleGaugeView(mode);
    }

    public void setMiddleGaugeView(CustomAnimationMode mode) {
    }

    public void setDashboard(Type dashBoardType) {
        switch (dashBoardType) {
            case Smart:
                this.uiStateManager.setCurrentDashboardType(dashBoardType);
                return;
            case Eco:
                this.uiStateManager.setCurrentDashboardType(dashBoardType);
                return;
            default:
                return;
        }
    }

    public Type getDashBoardType() {
        return this.uiStateManager.getCurrentDashboardType();
    }

    public void init(HomeScreenView homeScreenView) {
        this.homeScreenView = homeScreenView;
        initializeView();
        this.bus.register(new Object() {
            @Subscribe
            public void GPSSpeedChangeEvent(GPSSpeedEvent event) {
                BaseSmartDashView.this.updateSpeed(false);
            }

            @Subscribe
            public void onSpeedDataExpired(SpeedDataExpired speedDataExpired) {
                BaseSmartDashView.this.updateSpeed(true);
            }

            @Subscribe
            public void onMapEvent(ManeuverDisplay event) {
                BaseSmartDashView.this.updateSpeedLimitInternal(event.currentSpeedLimit);
            }

            @Subscribe
            public void onSpeedUnitChanged(SpeedUnitChanged event) {
                BaseSmartDashView.this.updateSpeed(true);
            }

            @Subscribe
            public void ObdPidChangeEvent(ObdPidChangeEvent event) {
                BaseSmartDashView.this.ObdPidChangeEvent(event);
            }
        });
    }

    protected void initializeView() {
        updateSpeedLimitInternal(HereMapUtil.getCurrentSpeedLimit());
        updateSpeed(true);
    }

    protected void adjustDashHeight(int height) {
        ((MarginLayoutParams) getLayoutParams()).height = height;
        invalidate();
        requestLayout();
        if (height == HomeScreenResourceValues.openMapHeight && this.homeScreenView.getDisplayMode() == DisplayMode.SMART_DASH) {
            animateToFullMode();
        }
    }

    public void updateLayoutForMode(NavigationMode navigationMode, boolean forced) {
        boolean navigationActive = this.homeScreenView.isNavigationActive();
        if (forced) {
            adjustDashHeight(navigationActive ? HomeScreenResourceValues.activeMapHeight : HomeScreenResourceValues.openMapHeight);
        }
    }

    public void getCustomAnimator(CustomAnimationMode mode, Builder mainBuilder) {
    }

    public void getTopAnimator(Builder builder, boolean out) {
    }

    public void resetTopViewsAnimator() {
        CustomAnimationMode mode = this.uiStateManager.getCustomAnimateMode();
        this.logger.v("mode-view:" + mode);
        setView(mode);
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        return false;
    }

    public IInputHandler nextHandler() {
        return null;
    }

    private void updateSpeedLimitInternal(float speedLimitMetersPerSec) {
        updateSpeedLimit(Math.round((float) SpeedManager.convert((double) speedLimitMetersPerSec, SpeedUnit.METERS_PER_SECOND, SpeedManager.getInstance().getSpeedUnit())));
    }

    protected void updateSpeed(boolean force) {
        int speed = this.speedManager.getCurrentSpeed();
        if (speed == -1) {
            speed = 0;
        }
        boolean changed = false;
        if (force || speed != this.lastObdSpeed) {
            this.lastObdSpeed = speed;
            updateSpeed(speed);
            changed = true;
        }
        if (changed && this.logger.isLoggable(2)) {
            this.logger.v("SPEED updated :" + speed);
        }
    }

    @Subscribe
    public void ObdStateChangeEvent(ObdConnectionStatusEvent event) {
        updateSpeed(true);
    }

    public void ObdPidChangeEvent(ObdPidChangeEvent event) {
        switch (event.pid.getId()) {
            case 13:
                updateSpeed(false);
                return;
            default:
                return;
        }
    }

    protected void animateToFullMode() {
    }

    protected void animateToFullModeInternal(Builder builder) {
    }

    public boolean shouldShowTopViews() {
        return true;
    }

    protected void updateSpeed(int speed) {
    }

    protected void updateSpeedLimit(int speedLimit) {
    }

    public void onPause() {
    }

    public void onResume() {
    }
}
