package com.navdy.hud.app.util;

import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;

public class DrawableLocator {
    private Drawable mDrawable;
    private View mParent;
    private int mResourceId = 0;

    public DrawableLocator(View parent, TypedArray array, int index, int defaultId) {
        this.mParent = parent;
        if (parent.isInEditMode()) {
            this.mDrawable = array.getDrawable(index);
            return;
        }
        this.mResourceId = array.getResourceId(index, -1);
        if (this.mResourceId == -1) {
            this.mResourceId = defaultId;
        }
    }

    public boolean isSet() {
        return (this.mDrawable == null && this.mResourceId == 0) ? false : true;
    }

    public Bitmap getBitmap() {
        if (this.mDrawable != null) {
            return drawableToBitmap(this.mDrawable);
        }
        if (this.mResourceId == 0) {
            return null;
        }
        return BitmapFactory.decodeResource(this.mParent.getResources(), this.mResourceId);
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }
}
