package com.navdy.hud.app.util;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class PackagedResource {
    private static final int BUFFER_SIZE = 16384;
    private static final String MD5_SUFFIX = ".md5";
    private static final Object lockObject = new Object();
    private static final Logger sLogger = new Logger(PackagedResource.class);
    private String basePath;
    private String destinationFilename;

    public PackagedResource(String destinationFilename, String basePath) {
        this.destinationFilename = destinationFilename;
        this.basePath = basePath;
    }

    public void updateFromResources(Context context, int resId) throws Throwable {
        updateFromResources(context, resId, -1);
    }

    public void updateFromResources(Context context, int resId, int hashResId) throws Throwable {
        updateFromResources(context, resId, hashResId, false);
    }

    public void updateFromResources(Context context, int resId, int hashResId, boolean unzip) throws Throwable {
        synchronized (lockObject) {
            boolean copyFile = true;
            InputStream md5InputStream = null;
            Resources resources = context.getResources();
            File md5File = new File(this.basePath, this.destinationFilename + MD5_SUFFIX);
            File configFile = new File(this.basePath, this.destinationFilename);
            sLogger.v("updateFromResources [" + this.destinationFilename + "]");
            String md5InApk = null;
            if (hashResId > 0) {
                try {
                    md5InputStream = resources.openRawResource(hashResId);
                    md5InApk = IOUtils.convertInputStreamToString(md5InputStream, "UTF-8");
                    IOUtils.closeStream(md5InputStream);
                    md5InputStream = null;
                } catch (Throwable th) {
                    IOUtils.closeStream(md5InputStream);
                }
            }
            if (md5File.exists() && (unzip || configFile.exists())) {
                String md5OnDevice = IOUtils.convertFileToString(md5File.getAbsolutePath());
                if (TextUtils.equals(md5OnDevice, md5InApk)) {
                    copyFile = false;
                    sLogger.d("no update required signature matches:" + md5OnDevice);
                } else {
                    sLogger.d("update required, device:" + md5OnDevice + " apk:" + md5InApk);
                }
            } else {
                sLogger.d("update required, no file");
            }
            if (copyFile) {
                IOUtils.copyFile(configFile.getAbsolutePath(), resources.openRawResource(resId));
                if (unzip) {
                    unpackZip(this.basePath, configFile.getName());
                    IOUtils.deleteFile(context, configFile.getAbsolutePath());
                }
                if (md5InApk != null) {
                    IOUtils.copyFile(md5File.getAbsolutePath(), md5InApk.getBytes());
                }
                IOUtils.closeStream(md5InputStream);
                return;
            }
            IOUtils.closeStream(md5InputStream);
        }
    }

    public static boolean unpackZip(String path, String zipname) {
        Throwable e;
        Throwable th;
        InputStream is = null;
        try {
            InputStream is2 = new FileInputStream(path + File.separator + zipname);
            try {
                boolean unpack = unpack(path, is2);
                IOUtils.closeStream(is2);
                is = is2;
                return unpack;
            } catch (Throwable th2) {
                th = th2;
                is = is2;
                IOUtils.closeStream(is);
                throw th;
            }
        } catch (Throwable th3) {
            e = th3;
            sLogger.e("Error reading resource", e);
            IOUtils.closeStream(is);
            return false;
        }
    }

    private static boolean unpack(String path, InputStream is) {
        File destDir = new File(path);
        byte[] buffer = new byte[16384];
        if (!destDir.exists()) {
            destDir.mkdirs();
        }
        ZipInputStream zis = new ZipInputStream(new BufferedInputStream(is));
        while (true) {
            ZipEntry ze = zis.getNextEntry();
            if (ze != null) {
                String filePath = path + File.separator + ze.getName();
                if (ze.isDirectory()) {
                    try {
                        new File(filePath).mkdirs();
                    } catch (Throwable th) {
                        IOUtils.closeStream(zis);
                        throw th;
                    }
                }
                extractFile(zis, filePath, buffer);
                zis.closeEntry();
            } else {
                IOUtils.closeStream(zis);
                return true;
            }
        }
    }

    private static void extractFile(ZipInputStream zis, String filePath, byte[] buffer) {
        Throwable t;
        Throwable th;
        FileOutputStream fout = null;
        try {
            FileOutputStream fout2 = new FileOutputStream(filePath);
            while (true) {
                try {
                    int count = zis.read(buffer);
                    if (count != -1) {
                        fout2.write(buffer, 0, count);
                    } else {
                        IOUtils.fileSync(fout2);
                        IOUtils.closeStream(fout2);
                        fout = fout2;
                        return;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fout = fout2;
                }
            }
        } catch (Throwable th3) {
            t = th3;
            try {
                sLogger.e("error extracting file", t);
                IOUtils.closeStream(fout);
            } catch (Throwable th4) {
                th = th4;
                IOUtils.closeStream(fout);
                throw th;
            }
        }
    }
}
