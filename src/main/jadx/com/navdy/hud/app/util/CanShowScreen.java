package com.navdy.hud.app.util;

import flow.Flow.Direction;
import mortar.Blueprint;

public interface CanShowScreen<S extends Blueprint> {
    void showScreen(S s, Direction direction, int i, int i2);
}
