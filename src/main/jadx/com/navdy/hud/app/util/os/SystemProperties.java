package com.navdy.hud.app.util.os;

import android.util.Log;

public class SystemProperties {
    private static Class<?> CLASS;
    public static final String TAG = SystemProperties.class.getName();

    static {
        try {
            CLASS = Class.forName("android.os.SystemProperties");
        } catch (ClassNotFoundException e) {
        }
    }

    public static String get(String key) {
        try {
            return (String) CLASS.getMethod("get", new Class[]{String.class}).invoke(null, new Object[]{key});
        } catch (Exception e) {
            return null;
        }
    }

    public static String get(String key, String def) {
        try {
            return (String) CLASS.getMethod("get", new Class[]{String.class, String.class}).invoke(null, new Object[]{key, def});
        } catch (Exception e) {
            return def;
        }
    }

    public static int getInt(String key, int def) {
        try {
            return ((Integer) CLASS.getMethod("getInt", new Class[]{String.class, Integer.TYPE}).invoke(null, new Object[]{key, Integer.valueOf(def)})).intValue();
        } catch (Exception e) {
            return def;
        }
    }

    public static long getLong(String key, long def) {
        try {
            return ((Long) CLASS.getMethod("getLong", new Class[]{String.class, Long.TYPE}).invoke(null, new Object[]{key, Long.valueOf(def)})).longValue();
        } catch (Exception e) {
            return def;
        }
    }

    public static boolean getBoolean(String key, boolean def) {
        try {
            return ((Boolean) CLASS.getMethod("getBoolean", new Class[]{String.class, Boolean.TYPE}).invoke(null, new Object[]{key, Boolean.valueOf(def)})).booleanValue();
        } catch (Exception e) {
            return def;
        }
    }

    public static void set(String key, String value) {
        try {
            CLASS.getMethod("set", new Class[]{String.class, String.class}).invoke(null, new Object[]{key, value});
        } catch (Exception e) {
            Log.e(TAG, "Unable to set prop " + key + " to value:" + value + " because of:" + e.getMessage());
        }
    }

    public static float getFloat(String key, float defaultValue) {
        float result = defaultValue;
        try {
            String value = get(key);
            if (value != null) {
                return Float.parseFloat(value);
            }
            return result;
        } catch (Exception e) {
            Log.w(TAG, "Failed to parse " + key + " as float - " + e.getMessage());
            return result;
        }
    }

    private SystemProperties() {
    }
}
