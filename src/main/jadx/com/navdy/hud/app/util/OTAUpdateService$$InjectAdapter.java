package com.navdy.hud.app.util;

import android.content.SharedPreferences;
import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class OTAUpdateService$$InjectAdapter extends Binding<OTAUpdateService> implements Provider<OTAUpdateService>, MembersInjector<OTAUpdateService> {
    private Binding<Bus> bus;
    private Binding<SharedPreferences> sharedPreferences;

    public OTAUpdateService$$InjectAdapter() {
        super("com.navdy.hud.app.util.OTAUpdateService", "members/com.navdy.hud.app.util.OTAUpdateService", false, OTAUpdateService.class);
    }

    public void attach(Linker linker) {
        this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", OTAUpdateService.class, getClass().getClassLoader());
        this.bus = linker.requestBinding("com.squareup.otto.Bus", OTAUpdateService.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.sharedPreferences);
        injectMembersBindings.add(this.bus);
    }

    public OTAUpdateService get() {
        OTAUpdateService result = new OTAUpdateService();
        injectMembers(result);
        return result;
    }

    public void injectMembers(OTAUpdateService object) {
        object.sharedPreferences = (SharedPreferences) this.sharedPreferences.get();
        object.bus = (Bus) this.bus.get();
    }
}
