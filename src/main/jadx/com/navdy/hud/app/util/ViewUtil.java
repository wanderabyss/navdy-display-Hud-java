package com.navdy.hud.app.util;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.support.annotation.ArrayRes;
import android.support.annotation.StyleRes;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.widget.TextView;

public class ViewUtil {
    private static final int LINE_SPACING_INDEX = 2;
    private static final int MAX_LINE_INDEX = 0;
    private static final int[] MULTI_LINE_ATTRS = new int[]{16843091, 16843101, 16843288};
    private static final int SINGLE_LINE_INDEX = 1;
    static SparseArray<float[]> TEXT_SIZES = new SparseArray();

    public static void autosize(TextView view, int maxLines, int maxWidth, @ArrayRes int arrayId) {
        autosize(view, maxLines, maxWidth, lookupSizes(view.getResources(), arrayId));
    }

    public static void adjustPadding(View v, int left, int top, int right, int bottom) {
        v.setPadding(v.getPaddingLeft() + left, v.getPaddingTop() + top, v.getPaddingRight() + right, v.getPaddingBottom() + bottom);
    }

    public static void setBottomPadding(View v, int bottom) {
        v.setPadding(v.getPaddingLeft(), v.getPaddingTop(), v.getPaddingRight(), bottom);
    }

    public static float autosize(TextView view, int maxLines, int maxWidth, float[] textSizes) {
        int[] maxLinesArray = new int[textSizes.length];
        for (int i = 0; i < textSizes.length; i++) {
            maxLinesArray[i] = maxLines;
        }
        return autosize(view, maxLinesArray, maxWidth, textSizes, null);
    }

    public static float autosize(TextView view, int[] maxLinesArray, int maxWidth, float[] textSizes, int[] indexHolder) {
        if (maxLinesArray.length != textSizes.length) {
            throw new IllegalArgumentException("maxLinesArray length must match textSizes length");
        }
        CharSequence sequence = view.getText();
        TextPaint p = view.getPaint();
        int minLines = -1;
        int index = textSizes.length - 1;
        float maxSize = textSizes[index];
        for (int i = 0; i < textSizes.length; i++) {
            int maxLines = maxLinesArray[i];
            view.setSingleLine(maxLines == 1);
            int limit = maxLines;
            float textSize = textSizes[i];
            p.setTextSize(textSize);
            int lines = new StaticLayout(sequence, p, maxWidth, Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false).getLineCount();
            if (lines <= limit && (minLines == -1 || lines < minLines)) {
                minLines = lines;
                maxSize = textSize;
                index = i;
            }
            if (lines == 1) {
                break;
            }
        }
        if (indexHolder != null) {
            indexHolder[0] = index;
            if (minLines == -1) {
                minLines = maxLinesArray[index];
            }
            indexHolder[1] = minLines;
        }
        view.setTextSize(maxSize);
        return maxSize;
    }

    private static float[] lookupSizes(Resources r, @ArrayRes int arrayId) {
        float[] cache = (float[]) TEXT_SIZES.get(arrayId);
        if (cache != null) {
            return cache;
        }
        int[] array = r.getIntArray(arrayId);
        float[] copy = new float[array.length];
        for (int i = 0; i < array.length; i++) {
            copy[i] = (float) array[i];
        }
        cache = copy;
        TEXT_SIZES.put(arrayId, cache);
        return cache;
    }

    public static int applyTextAndStyle(TextView view, CharSequence text, int maxLines, @StyleRes int style) {
        Context context = view.getContext();
        if (TextUtils.isEmpty(text)) {
            view.setVisibility(8);
            return 0;
        }
        boolean singleLine;
        view.setVisibility(0);
        if (maxLines == 1) {
            singleLine = true;
        } else {
            singleLine = false;
        }
        float lineSpacing = 1.0f;
        if (style != -1) {
            TypedArray ta = context.obtainStyledAttributes(style, MULTI_LINE_ATTRS);
            maxLines = ta.getInt(0, maxLines);
            singleLine = ta.hasValue(1) ? ta.getBoolean(1, true) : maxLines == 1;
            lineSpacing = ta.getFloat(2, 1.0f);
            ta.recycle();
            view.setTextAppearance(context, style);
        }
        view.setText(text);
        view.setLineSpacing(0.0f, lineSpacing);
        view.setSingleLine(singleLine);
        view.setMaxLines(maxLines);
        return maxLines;
    }
}
