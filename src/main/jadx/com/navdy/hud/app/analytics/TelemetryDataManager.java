package com.navdy.hud.app.analytics;

import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import com.navdy.hud.app.analytics.TelemetrySession.DataSource;
import com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.gps.CalibratedGForceData;
import com.navdy.hud.app.device.gps.GpsUtils.GpsSwitch;
import com.navdy.hud.app.event.InitEvents.InitPhase;
import com.navdy.hud.app.event.InitEvents.Phase;
import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.manager.SpeedManager.SpeedDataExpired;
import com.navdy.hud.app.maps.MapEvents.GPSSpeedEvent;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent;
import com.navdy.hud.app.obd.ObdManager.ObdPidChangeEvent;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.ConversionUtil;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.obd.PidSet;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.HashSet;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u00a4\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 D2\u00020\u0001:\u0002DEB-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\fJ\u0010\u0010&\u001a\u00020#2\u0006\u0010'\u001a\u00020(H\u0007J\u0010\u0010)\u001a\u00020#2\u0006\u0010'\u001a\u00020*H\u0007J\u0010\u0010+\u001a\u00020#2\u0006\u0010,\u001a\u00020-H\u0007J\u0010\u0010.\u001a\u00020#2\u0006\u0010/\u001a\u000200H\u0007J\u0010\u00101\u001a\u00020#2\u0006\u0010'\u001a\u000202H\u0007J\u0010\u00103\u001a\u00020#2\u0006\u0010'\u001a\u000204H\u0007J\u0010\u00105\u001a\u00020#2\u0006\u00106\u001a\u000207H\u0007J\u0010\u00108\u001a\u00020#2\u0006\u00109\u001a\u00020:H\u0007J\u0010\u0010;\u001a\u00020#2\u0006\u0010<\u001a\u00020=H\u0007J\b\u0010>\u001a\u00020#H\u0002J\u0006\u0010?\u001a\u00020#J\u0010\u0010@\u001a\u00020#2\b\b\u0002\u0010A\u001a\u00020BJ\b\u0010C\u001a\u00020#H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u00020\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0015\u001a\u00020\u0016X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u001a\u0010\u001b\u001a\u00020\u001cX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010!\u001a\b\u0012\u0004\u0012\u00020#0\"X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006F"}, d2 = {"Lcom/navdy/hud/app/analytics/TelemetryDataManager;", "", "uiStateManager", "Lcom/navdy/hud/app/ui/framework/UIStateManager;", "powerManager", "Lcom/navdy/hud/app/device/PowerManager;", "bus", "Lcom/squareup/otto/Bus;", "sharedPreferences", "Landroid/content/SharedPreferences;", "tripManager", "Lcom/navdy/hud/app/framework/trips/TripManager;", "(Lcom/navdy/hud/app/ui/framework/UIStateManager;Lcom/navdy/hud/app/device/PowerManager;Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Lcom/navdy/hud/app/framework/trips/TripManager;)V", "driveScoreUpdatedEvent", "Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;", "getDriveScoreUpdatedEvent", "()Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;", "setDriveScoreUpdatedEvent", "(Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;)V", "handler", "Landroid/os/Handler;", "highAccuracySpeedAvailable", "", "getHighAccuracySpeedAvailable", "()Z", "setHighAccuracySpeedAvailable", "(Z)V", "lastDriveScore", "", "getLastDriveScore", "()I", "setLastDriveScore", "(I)V", "reportRunnable", "Lkotlin/Function0;", "", "updateDriveScoreRunnable", "Ljava/lang/Runnable;", "GPSSpeedChangeEvent", "event", "Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;", "ObdPidChangeEvent", "Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;", "onCalibratedGForceData", "calibratedGForceData", "Lcom/navdy/hud/app/device/gps/CalibratedGForceData;", "onGpsSwitched", "gpsSwitch", "Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;", "onInit", "Lcom/navdy/hud/app/event/InitEvents$InitPhase;", "onMapEvent", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "onObdConnectionStateChanged", "connectionStateChangeEvent", "Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;", "onSpeedDataExpired", "speedDataExpired", "Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataExpired;", "onWakeup", "wakeUpEvent", "Lcom/navdy/hud/app/event/Wakeup;", "reportTelemetryData", "startTelemetrySession", "updateDriveScore", "interestingEvent", "Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;", "updateSpeed", "Companion", "DriveScoreUpdated", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: TelemetryDataManager.kt */
public final class TelemetryDataManager {
    private static final long ANALYTICS_REPORTING_INTERVAL = ANALYTICS_REPORTING_INTERVAL;
    public static final Companion Companion = new Companion();
    private static final long DRIVE_SCORE_PUBLISH_INTERVAL = 3000;
    private static final boolean LOG_TELEMETRY_DATA = SystemProperties.getBoolean("persist.sys.prop.log.telemetry", false);
    private static final float MAX_ACCEL = 2.0f;
    private static final String PREFERENCE_TROUBLE_CODES = PREFERENCE_TROUBLE_CODES;
    private static final Logger sLogger = new Logger("TelemetryDataManager");
    private final Bus bus;
    @NotNull
    private DriveScoreUpdated driveScoreUpdatedEvent;
    private Handler handler;
    private boolean highAccuracySpeedAvailable;
    private int lastDriveScore;
    private final PowerManager powerManager;
    private final Function0<Unit> reportRunnable = new TelemetryDataManager$reportRunnable$1(this);
    private final SharedPreferences sharedPreferences;
    private final TripManager tripManager;
    private final UIStateManager uiStateManager;
    private final Runnable updateDriveScoreRunnable;

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000-\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003*\u0001\u0000\b\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J\b\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\b\u0010\u000b\u001a\u00020\fH\u0016J\b\u0010\r\u001a\u00020\fH\u0016J\b\u0010\u000e\u001a\u00020\u0004H\u0016\u00a8\u0006\u000f"}, d2 = {"com/navdy/hud/app/analytics/TelemetryDataManager$1", "Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;", "(Lcom/navdy/hud/app/analytics/TelemetryDataManager;)V", "absoluteCurrentTime", "", "currentSpeed", "Lcom/navdy/hud/app/analytics/RawSpeed;", "interestingEventDetected", "", "event", "Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;", "isHighAccuracySpeedAvailable", "", "isVerboseLoggingNeeded", "totalDistanceTravelledWithMeters", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TelemetryDataManager.kt */
    /* renamed from: com.navdy.hud.app.analytics.TelemetryDataManager$1 */
    public static final class AnonymousClass1 implements DataSource {
        final /* synthetic */ TelemetryDataManager this$0;

        AnonymousClass1(TelemetryDataManager $outer) {
            this.this$0 = $outer;
        }

        public boolean isHighAccuracySpeedAvailable() {
            return this.this$0.getHighAccuracySpeedAvailable();
        }

        public long absoluteCurrentTime() {
            return SystemClock.elapsedRealtime();
        }

        public boolean isVerboseLoggingNeeded() {
            return !DeviceUtil.isUserBuild() && Intrinsics.areEqual(this.this$0.uiStateManager.getHomescreenView().getDisplayMode(), DisplayMode.SMART_DASH) && this.this$0.uiStateManager.getSmartDashView().isShowingDriveScoreGauge;
        }

        public void interestingEventDetected(@NotNull InterestingEvent event) {
            Intrinsics.checkParameterIsNotNull(event, "event");
            this.this$0.updateDriveScore(event);
        }

        public long totalDistanceTravelledWithMeters() {
            return this.this$0.tripManager.getTotalDistanceTravelledWithNavdy();
        }

        @NotNull
        public RawSpeed currentSpeed() {
            RawSpeed currentSpeedInMetersPerSecond = SpeedManager.getInstance().getCurrentSpeedInMetersPerSecond();
            Intrinsics.checkExpressionValueIsNotNull(currentSpeedInMetersPerSecond, "SpeedManager.getInstance\u2026entSpeedInMetersPerSecond");
            return currentSpeedInMetersPerSecond;
        }
    }

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J(\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00120\u001a2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00120\u001c2\u0006\u0010\u001d\u001a\u00020\u0012R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0082D\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u0014\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\u00020\u000eX\u0082D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u0012X\u0082D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0014\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018\u00a8\u0006\u001e"}, d2 = {"Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;", "", "()V", "ANALYTICS_REPORTING_INTERVAL", "", "getANALYTICS_REPORTING_INTERVAL", "()J", "DRIVE_SCORE_PUBLISH_INTERVAL", "getDRIVE_SCORE_PUBLISH_INTERVAL", "LOG_TELEMETRY_DATA", "", "getLOG_TELEMETRY_DATA", "()Z", "MAX_ACCEL", "", "getMAX_ACCEL", "()F", "PREFERENCE_TROUBLE_CODES", "", "getPREFERENCE_TROUBLE_CODES", "()Ljava/lang/String;", "sLogger", "Lcom/navdy/service/library/log/Logger;", "getSLogger", "()Lcom/navdy/service/library/log/Logger;", "serializeTroubleCodes", "Lkotlin/Pair;", "troubleCodes", "", "savedTroubleCodesStringFromPreviousSession", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TelemetryDataManager.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker $constructor_marker) {
            this();
        }

        private final Logger getSLogger() {
            return TelemetryDataManager.sLogger;
        }

        private final String getPREFERENCE_TROUBLE_CODES() {
            return TelemetryDataManager.PREFERENCE_TROUBLE_CODES;
        }

        private final long getANALYTICS_REPORTING_INTERVAL() {
            return TelemetryDataManager.ANALYTICS_REPORTING_INTERVAL;
        }

        private final long getDRIVE_SCORE_PUBLISH_INTERVAL() {
            return TelemetryDataManager.DRIVE_SCORE_PUBLISH_INTERVAL;
        }

        private final float getMAX_ACCEL() {
            return TelemetryDataManager.MAX_ACCEL;
        }

        private final boolean getLOG_TELEMETRY_DATA() {
            return TelemetryDataManager.LOG_TELEMETRY_DATA;
        }

        @NotNull
        public final Pair<String, String> serializeTroubleCodes(@NotNull List<String> troubleCodes, @NotNull String savedTroubleCodesStringFromPreviousSession) {
            Intrinsics.checkParameterIsNotNull(troubleCodes, "troubleCodes");
            Intrinsics.checkParameterIsNotNull(savedTroubleCodesStringFromPreviousSession, "savedTroubleCodesStringFromPreviousSession");
            HashSet hashSet = new HashSet();
            if (!TextUtils.isEmpty(savedTroubleCodesStringFromPreviousSession)) {
                for (String troubleCode : StringsKt__StringsKt.split$default((CharSequence) savedTroubleCodesStringFromPreviousSession, new String[]{" "}, false, 0, 6, null)) {
                    hashSet.add(troubleCode);
                }
            }
            StringBuilder updatedTroubleCodesString = new StringBuilder();
            StringBuilder troubleCodesDiffString = new StringBuilder();
            if (troubleCodes.size() > 0) {
                TelemetrySession.INSTANCE.setSessionTroubleCodeCount(troubleCodes.size());
                for (String newTroubleCode : troubleCodes) {
                    if (!hashSet.contains(newTroubleCode)) {
                        troubleCodesDiffString.append(newTroubleCode + " ");
                    }
                    updatedTroubleCodesString.append(newTroubleCode + " ");
                }
            }
            String stringBuilder = updatedTroubleCodesString.toString();
            if (stringBuilder == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
            }
            String obj = StringsKt__StringsKt.trim((CharSequence) stringBuilder).toString();
            stringBuilder = troubleCodesDiffString.toString();
            if (stringBuilder != null) {
                return new Pair(obj, StringsKt__StringsKt.trim((CharSequence) stringBuilder).toString());
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0018\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0018\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\tH\u00c6\u0003J;\u0010\u001d\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u001e\u001a\u00020\u00052\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010 \u001a\u00020\tH\u00d6\u0001J\t\u0010!\u001a\u00020\"H\u00d6\u0001R\u001a\u0010\b\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u001a\u0010\u0007\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001a\u0010\u0006\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0013\"\u0004\b\u0016\u0010\u0015R\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0004\u0010\u0013\"\u0004\b\u0017\u0010\u0015\u00a8\u0006#"}, d2 = {"Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;", "", "interestingEvent", "Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;", "isSpeeding", "", "isExcessiveSpeeding", "isDoingHighGManeuver", "driveScore", "", "(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;ZZZI)V", "getDriveScore", "()I", "setDriveScore", "(I)V", "getInterestingEvent", "()Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;", "setInterestingEvent", "(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V", "()Z", "setDoingHighGManeuver", "(Z)V", "setExcessiveSpeeding", "setSpeeding", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "other", "hashCode", "toString", "", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TelemetryDataManager.kt */
    public static final class DriveScoreUpdated {
        private int driveScore;
        @NotNull
        private InterestingEvent interestingEvent;
        private boolean isDoingHighGManeuver;
        private boolean isExcessiveSpeeding;
        private boolean isSpeeding;

        @NotNull
        public static /* bridge */ /* synthetic */ DriveScoreUpdated copy$default(DriveScoreUpdated driveScoreUpdated, InterestingEvent interestingEvent, boolean z, boolean z2, boolean z3, int i, int i2, Object obj) {
            return driveScoreUpdated.copy((i2 & 1) != 0 ? driveScoreUpdated.interestingEvent : interestingEvent, (i2 & 2) != 0 ? driveScoreUpdated.isSpeeding : z, (i2 & 4) != 0 ? driveScoreUpdated.isExcessiveSpeeding : z2, (i2 & 8) != 0 ? driveScoreUpdated.isDoingHighGManeuver : z3, (i2 & 16) != 0 ? driveScoreUpdated.driveScore : i);
        }

        @NotNull
        public final InterestingEvent component1() {
            return this.interestingEvent;
        }

        public final boolean component2() {
            return this.isSpeeding;
        }

        public final boolean component3() {
            return this.isExcessiveSpeeding;
        }

        public final boolean component4() {
            return this.isDoingHighGManeuver;
        }

        public final int component5() {
            return this.driveScore;
        }

        @NotNull
        public final DriveScoreUpdated copy(@NotNull InterestingEvent interestingEvent, boolean isSpeeding, boolean isExcessiveSpeeding, boolean isDoingHighGManeuver, int driveScore) {
            Intrinsics.checkParameterIsNotNull(interestingEvent, "interestingEvent");
            return new DriveScoreUpdated(interestingEvent, isSpeeding, isExcessiveSpeeding, isDoingHighGManeuver, driveScore);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                if (!(obj instanceof DriveScoreUpdated)) {
                    return false;
                }
                DriveScoreUpdated driveScoreUpdated = (DriveScoreUpdated) obj;
                if (!Intrinsics.areEqual(this.interestingEvent, driveScoreUpdated.interestingEvent)) {
                    return false;
                }
                if (!(this.isSpeeding == driveScoreUpdated.isSpeeding)) {
                    return false;
                }
                if (!(this.isExcessiveSpeeding == driveScoreUpdated.isExcessiveSpeeding)) {
                    return false;
                }
                if (!(this.isDoingHighGManeuver == driveScoreUpdated.isDoingHighGManeuver)) {
                    return false;
                }
                if (!(this.driveScore == driveScoreUpdated.driveScore)) {
                    return false;
                }
            }
            return true;
        }

        public int hashCode() {
            int i = 1;
            InterestingEvent interestingEvent = this.interestingEvent;
            int hashCode = (interestingEvent != null ? interestingEvent.hashCode() : 0) * 31;
            int i2 = this.isSpeeding;
            if (i2 != 0) {
                i2 = 1;
            }
            hashCode = (i2 + hashCode) * 31;
            i2 = this.isExcessiveSpeeding;
            if (i2 != 0) {
                i2 = 1;
            }
            hashCode = (i2 + hashCode) * 31;
            boolean z = this.isDoingHighGManeuver;
            if (!z) {
                boolean i3 = z;
            }
            return ((hashCode + i3) * 31) + this.driveScore;
        }

        public String toString() {
            return "DriveScoreUpdated(interestingEvent=" + this.interestingEvent + ", isSpeeding=" + this.isSpeeding + ", isExcessiveSpeeding=" + this.isExcessiveSpeeding + ", isDoingHighGManeuver=" + this.isDoingHighGManeuver + ", driveScore=" + this.driveScore + HereManeuverDisplayBuilder.CLOSE_BRACKET;
        }

        public DriveScoreUpdated(@NotNull InterestingEvent interestingEvent, boolean isSpeeding, boolean isExcessiveSpeeding, boolean isDoingHighGManeuver, int driveScore) {
            Intrinsics.checkParameterIsNotNull(interestingEvent, "interestingEvent");
            this.interestingEvent = interestingEvent;
            this.isSpeeding = isSpeeding;
            this.isExcessiveSpeeding = isExcessiveSpeeding;
            this.isDoingHighGManeuver = isDoingHighGManeuver;
            this.driveScore = driveScore;
        }

        public final int getDriveScore() {
            return this.driveScore;
        }

        @NotNull
        public final InterestingEvent getInterestingEvent() {
            return this.interestingEvent;
        }

        public final boolean isDoingHighGManeuver() {
            return this.isDoingHighGManeuver;
        }

        public final boolean isExcessiveSpeeding() {
            return this.isExcessiveSpeeding;
        }

        public final boolean isSpeeding() {
            return this.isSpeeding;
        }

        public final void setDoingHighGManeuver(boolean <set-?>) {
            this.isDoingHighGManeuver = <set-?>;
        }

        public final void setDriveScore(int <set-?>) {
            this.driveScore = <set-?>;
        }

        public final void setExcessiveSpeeding(boolean <set-?>) {
            this.isExcessiveSpeeding = <set-?>;
        }

        public final void setInterestingEvent(@NotNull InterestingEvent <set-?>) {
            Intrinsics.checkParameterIsNotNull(<set-?>, "<set-?>");
            this.interestingEvent = <set-?>;
        }

        public final void setSpeeding(boolean <set-?>) {
            this.isSpeeding = <set-?>;
        }
    }

    public TelemetryDataManager(@NotNull UIStateManager uiStateManager, @NotNull PowerManager powerManager, @NotNull Bus bus, @NotNull SharedPreferences sharedPreferences, @NotNull TripManager tripManager) {
        Intrinsics.checkParameterIsNotNull(uiStateManager, "uiStateManager");
        Intrinsics.checkParameterIsNotNull(powerManager, "powerManager");
        Intrinsics.checkParameterIsNotNull(bus, "bus");
        Intrinsics.checkParameterIsNotNull(sharedPreferences, "sharedPreferences");
        Intrinsics.checkParameterIsNotNull(tripManager, "tripManager");
        this.uiStateManager = uiStateManager;
        this.powerManager = powerManager;
        this.bus = bus;
        this.sharedPreferences = sharedPreferences;
        this.tripManager = tripManager;
        TelemetrySession.INSTANCE.setDataSource(new AnonymousClass1(this));
        this.bus.register(this);
        this.handler = new Handler(Looper.getMainLooper());
        this.updateDriveScoreRunnable = new TelemetryDataManager$updateDriveScoreRunnable$1(this);
        this.driveScoreUpdatedEvent = new DriveScoreUpdated(InterestingEvent.NONE, false, false, false, 100);
    }

    @NotNull
    public final DriveScoreUpdated getDriveScoreUpdatedEvent() {
        return this.driveScoreUpdatedEvent;
    }

    public final void setDriveScoreUpdatedEvent(@NotNull DriveScoreUpdated <set-?>) {
        Intrinsics.checkParameterIsNotNull(<set-?>, "<set-?>");
        this.driveScoreUpdatedEvent = <set-?>;
    }

    public final int getLastDriveScore() {
        return this.lastDriveScore;
    }

    public final void setLastDriveScore(int <set-?>) {
        this.lastDriveScore = <set-?>;
    }

    public final boolean getHighAccuracySpeedAvailable() {
        return this.highAccuracySpeedAvailable;
    }

    public final void setHighAccuracySpeedAvailable(boolean <set-?>) {
        this.highAccuracySpeedAvailable = <set-?>;
    }

    public static /* bridge */ /* synthetic */ void updateDriveScore$default(TelemetryDataManager telemetryDataManager, InterestingEvent interestingEvent, int i, Object obj) {
        if ((i & 1) != 0) {
            interestingEvent = InterestingEvent.NONE;
        }
        telemetryDataManager.updateDriveScore(interestingEvent);
    }

    public final void updateDriveScore(@NotNull InterestingEvent interestingEvent) {
        Intrinsics.checkParameterIsNotNull(interestingEvent, "interestingEvent");
        TelemetrySession $receiver = TelemetrySession.INSTANCE;
        double hardAccelerationFactor = $receiver.getSessionRollingDuration() > ((long) 0) ? (((double) (($receiver.getSessionHardAccelerationCount() + $receiver.getSessionHardBrakingCount()) + $receiver.getSessionHighGCount())) * 540.0d) / Math.sqrt((((double) $receiver.getSessionRollingDuration()) / ((double) 1000.0f)) + ((double) 60)) : 0.0d;
        float speedingFactor = $receiver.getSessionSpeedingPercentage() * ((float) 50);
        float excessiveSpeedingFactor = $receiver.getSessionExcessiveSpeedingPercentage() * ((float) 50);
        int drivingScore = (int) Math.ceil(Math.max(0.0d, ((double) 100) - ((((double) speedingFactor) + hardAccelerationFactor) + ((double) excessiveSpeedingFactor))));
        this.driveScoreUpdatedEvent = new DriveScoreUpdated(interestingEvent, $receiver.isSpeeding(), $receiver.isExcessiveSpeeding(), $receiver.isDoingHighGManeuver(), drivingScore);
        if (!((Intrinsics.areEqual( interestingEvent, InterestingEvent.NONE) ^ 1) == 0 && drivingScore == this.lastDriveScore)) {
            Companion.getSLogger().d("HA : " + $receiver.getSessionHardAccelerationCount() + " , HB : " + $receiver.getSessionHardBrakingCount() + " , HG : " + $receiver.getSessionHighGCount());
            Companion.getSLogger().d("Speeding percentage : " + $receiver.getSessionSpeedingPercentage());
            Companion.getSLogger().d("Excessive Speeding percentage : " + $receiver.getSessionExcessiveSpeedingPercentage());
            Companion.getSLogger().d("Hard Acceleration Factor : " + hardAccelerationFactor + " , Speeding Factor : " + speedingFactor + " , excessive Speeding factor " + excessiveSpeedingFactor);
            Companion.getSLogger().d("DriveScoreUpdate : " + this.driveScoreUpdatedEvent);
            this.bus.post(this.driveScoreUpdatedEvent);
        }
        this.lastDriveScore = drivingScore;
        this.handler.removeCallbacks(this.updateDriveScoreRunnable);
        this.handler.postDelayed(this.updateDriveScoreRunnable, Companion.getDRIVE_SCORE_PUBLISH_INTERVAL());
    }

    @Subscribe
    public final void onInit(@NotNull InitPhase event) {
        Intrinsics.checkParameterIsNotNull(event, "event");
        Phase phase = event.phase;
        if (phase != null) {
            switch (phase) {
                case POST_START:
                    Companion.getSLogger().i("Post Start , Quiet mode : " + this.powerManager.inQuietMode());
                    if (!this.powerManager.inQuietMode()) {
                        startTelemetrySession();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @Subscribe
    public final void onWakeup(@NotNull Wakeup wakeUpEvent) {
        Intrinsics.checkParameterIsNotNull(wakeUpEvent, "wakeUpEvent");
        startTelemetrySession();
    }

    public final void startTelemetrySession() {
        if (TelemetrySession.INSTANCE.getSessionStarted()) {
            Companion.getSLogger().d("Telemetry session is already started");
            return;
        }
        Companion.getSLogger().i("Starting telemetry session");
        TelemetrySession.INSTANCE.startSession();
        Handler handler = this.handler;
        Function0 function0 = this.reportRunnable;
        handler.removeCallbacks(function0 == null ? null : new TelemetryDataManagerKt$sam$Runnable$cc6381d8(function0));
        handler = this.handler;
        function0 = this.reportRunnable;
        handler.postDelayed(function0 == null ? null : new TelemetryDataManagerKt$sam$Runnable$cc6381d8(function0), Companion.getANALYTICS_REPORTING_INTERVAL());
        this.handler.postDelayed(this.updateDriveScoreRunnable, Companion.getDRIVE_SCORE_PUBLISH_INTERVAL());
    }

    @Subscribe
    public final void GPSSpeedChangeEvent(@NotNull GPSSpeedEvent event) {
        Intrinsics.checkParameterIsNotNull(event, "event");
        updateSpeed();
    }

    @Subscribe
    public final void onSpeedDataExpired(@NotNull SpeedDataExpired speedDataExpired) {
        Intrinsics.checkParameterIsNotNull(speedDataExpired, "speedDataExpired");
        TelemetrySession.INSTANCE.speedSourceChanged();
        updateSpeed();
    }

    @Subscribe
    public final void onGpsSwitched(@NotNull GpsSwitch gpsSwitch) {
        Intrinsics.checkParameterIsNotNull(gpsSwitch, "gpsSwitch");
        Companion.getSLogger().d("onGpsSwitched " + gpsSwitch);
        TelemetrySession.INSTANCE.speedSourceChanged();
    }

    @Subscribe
    public final void onMapEvent(@NotNull ManeuverDisplay event) {
        Intrinsics.checkParameterIsNotNull(event, "event");
        TelemetrySession.INSTANCE.setCurrentSpeedLimit(event.currentSpeedLimit);
    }

    @Subscribe
    public final void onObdConnectionStateChanged(@NotNull ObdConnectionStatusEvent connectionStateChangeEvent) {
        Intrinsics.checkParameterIsNotNull(connectionStateChangeEvent, "connectionStateChangeEvent");
        TelemetrySession.INSTANCE.speedSourceChanged();
        if (connectionStateChangeEvent.connected) {
            PidSet pidSet = ObdManager.getInstance().getSupportedPids();
            this.highAccuracySpeedAvailable = true;
            String troubleCodesString = this.sharedPreferences.getString(Companion.getPREFERENCE_TROUBLE_CODES(), "");
            Object troubleCodes = ObdManager.getInstance().getTroubleCodes();
            if (Intrinsics.areEqual(troubleCodes, null)) {
                this.sharedPreferences.edit().putString(Companion.getPREFERENCE_TROUBLE_CODES(), "").apply();
                TelemetrySession.INSTANCE.setSessionTroubleCodesReport("");
                return;
            }
            Companion companion = Companion;
            Intrinsics.checkExpressionValueIsNotNull(troubleCodesString, "troubleCodesString");
            Pair serializeTroubleCodes = companion.serializeTroubleCodes(troubleCodes, troubleCodesString);
            String str = (String) serializeTroubleCodes.component1();
            String str2 = (String) serializeTroubleCodes.component2();
            this.sharedPreferences.edit().putString(Companion.getPREFERENCE_TROUBLE_CODES(), str).apply();
            TelemetrySession.INSTANCE.setSessionTroubleCodesReport(str2);
            return;
        }
        this.highAccuracySpeedAvailable = false;
    }

    @Subscribe
    public final void ObdPidChangeEvent(@NotNull ObdPidChangeEvent event) {
        Intrinsics.checkParameterIsNotNull(event, "event");
        switch (event.pid.getId()) {
            case 12:
                TelemetrySession.INSTANCE.setCurrentRpm((int) event.pid.getValue());
                return;
            case 13:
                updateSpeed();
                return;
            case 256:
                TelemetrySession.INSTANCE.setCurrentMpg(ConversionUtil.convertLpHundredKmToMPG(event.pid.getValue()));
                return;
            default:
                return;
        }
    }

    @Subscribe
    public final void onCalibratedGForceData(@NotNull CalibratedGForceData calibratedGForceData) {
        Intrinsics.checkParameterIsNotNull(calibratedGForceData, "calibratedGForceData");
        if (TelemetrySession.INSTANCE.getSessionStarted()) {
            TelemetrySession.INSTANCE.setGForce(calibratedGForceData.getXAccel(), calibratedGForceData.getYAccel(), calibratedGForceData.getZAccel());
        }
    }

    private final void updateSpeed() {
        if (TelemetrySession.INSTANCE.getSessionStarted()) {
            TelemetrySession telemetrySession = TelemetrySession.INSTANCE;
            RawSpeed currentSpeedInMetersPerSecond = SpeedManager.getInstance().getCurrentSpeedInMetersPerSecond();
            Intrinsics.checkExpressionValueIsNotNull(currentSpeedInMetersPerSecond, "SpeedManager.getInstance\u2026entSpeedInMetersPerSecond");
            telemetrySession.setCurrentSpeed(currentSpeedInMetersPerSecond);
        }
    }

    private final void reportTelemetryData() {
        if (Companion.getLOG_TELEMETRY_DATA()) {
            Companion.getSLogger().d(("Distance = " + TelemetrySession.INSTANCE.getSessionDistance() + HereManeuverDisplayBuilder.COMMA) + ("Duration = " + TelemetrySessionKt.milliSecondsToSeconds(TelemetrySession.INSTANCE.getSessionDuration()) + ", ") + ("RollingDuration = " + TelemetrySessionKt.milliSecondsToSeconds(TelemetrySession.INSTANCE.getSessionRollingDuration()) + ", ") + ("AverageSpeed = " + TelemetrySession.INSTANCE.getSessionAverageSpeed() + ", ") + ("AverageRollingSpeed = " + TelemetrySession.INSTANCE.getSessionAverageRollingSpeed() + ", ") + ("MaxSpeed = " + TelemetrySession.INSTANCE.getSessionMaxSpeed() + ", ") + ("HardBrakingCount = " + TelemetrySession.INSTANCE.getSessionHardBrakingCount() + ", ") + ("HardAccelerationCount = " + TelemetrySession.INSTANCE.getSessionHardAccelerationCount() + ", ") + ("SpeedingPercentage = " + TelemetrySession.INSTANCE.getSessionSpeedingPercentage() + HereManeuverDisplayBuilder.COMMA) + ("TroubleCodeCount = " + TelemetrySession.INSTANCE.getSessionTroubleCodeCount() + HereManeuverDisplayBuilder.COMMA) + ("TroubleCodesReport = " + TelemetrySession.INSTANCE.getSessionTroubleCodesReport()) + ("MaxRPM = " + TelemetrySession.INSTANCE.getMaxRpm() + HereManeuverDisplayBuilder.COMMA));
            Companion.getSLogger().d(("EVENT , averageMpg = " + TelemetrySession.INSTANCE.getEventAverageMpg() + HereManeuverDisplayBuilder.COMMA) + ("averageRPM = " + TelemetrySession.INSTANCE.getEventAverageRpm() + ", ") + ("Fuel level = " + ObdManager.getInstance().getFuelLevel() + ", ") + ("Engine Temperature = " + ObdManager.getInstance().getPidValue(5) + ", "));
        }
        AnalyticsSupport.recordVehicleTelemetryData(TelemetrySession.INSTANCE.getSessionDistance(), TelemetrySessionKt.milliSecondsToSeconds(TelemetrySession.INSTANCE.getSessionDuration()), TelemetrySessionKt.milliSecondsToSeconds(TelemetrySession.INSTANCE.getSessionRollingDuration()), TelemetrySession.INSTANCE.getSessionAverageSpeed(), TelemetrySession.INSTANCE.getSessionAverageRollingSpeed(), TelemetrySession.INSTANCE.getSessionMaxSpeed(), TelemetrySession.INSTANCE.getSessionHardBrakingCount(), TelemetrySession.INSTANCE.getSessionHardAccelerationCount(), TelemetrySession.INSTANCE.getSessionSpeedingPercentage(), TelemetrySession.INSTANCE.getSessionExcessiveSpeedingPercentage(), TelemetrySession.INSTANCE.getSessionTroubleCodeCount(), TelemetrySession.INSTANCE.getSessionTroubleCodesReport(), (int) TelemetrySession.INSTANCE.getSessionAverageMpg(), TelemetrySession.INSTANCE.getMaxRpm(), (int) TelemetrySession.INSTANCE.getEventAverageMpg(), TelemetrySession.INSTANCE.getEventAverageRpm(), ObdManager.getInstance().getFuelLevel(), (float) ObdManager.getInstance().getPidValue(5), TelemetrySession.INSTANCE.getMaxG(), TelemetrySession.INSTANCE.getMaxGAngle(), TelemetrySession.INSTANCE.getSessionHighGCount());
        TelemetrySession.INSTANCE.eventReported();
        Handler handler = this.handler;
        Function0 function0 = this.reportRunnable;
        handler.postDelayed(function0 == null ? null : new TelemetryDataManagerKt$sam$Runnable$cc6381d8(function0), Companion.getANALYTICS_REPORTING_INTERVAL());
    }
}
