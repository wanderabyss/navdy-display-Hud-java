package com.navdy.hud.app;

import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class HudApplication$$InjectAdapter extends Binding<HudApplication> implements Provider<HudApplication>, MembersInjector<HudApplication> {
    private Binding<Bus> bus;

    public HudApplication$$InjectAdapter() {
        super("com.navdy.hud.app.HudApplication", "members/com.navdy.hud.app.HudApplication", false, HudApplication.class);
    }

    public void attach(Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", HudApplication.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
    }

    public HudApplication get() {
        HudApplication result = new HudApplication();
        injectMembers(result);
        return result;
    }

    public void injectMembers(HudApplication object) {
        object.bus = (Bus) this.bus.get();
    }
}
