package com.navdy.hud.device.connection;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.navdy.hud.mfi.CallStateUpdate;
import com.navdy.hud.mfi.CommunicationUpdate;
import com.navdy.hud.mfi.EASession;
import com.navdy.hud.mfi.IAPCommunicationsManager;
import com.navdy.hud.mfi.IAPCommunicationsUpdateListener;
import com.navdy.hud.mfi.IAPListener;
import com.navdy.hud.mfi.LinkLayer;
import com.navdy.hud.mfi.LinkLayer.PhysicalLayer;
import com.navdy.hud.mfi.LinkPacket;
import com.navdy.hud.mfi.Utils;
import com.navdy.hud.mfi.iAPProcessor;
import com.navdy.hud.mfi.iAPProcessor.StateListener;
import com.navdy.service.library.device.connection.Connection.DisconnectCause;
import com.navdy.service.library.device.connection.ConnectionType;
import com.navdy.service.library.device.link.EventRequest;
import com.navdy.service.library.device.link.IOThread;
import com.navdy.service.library.device.link.Link;
import com.navdy.service.library.device.link.LinkListener;
import com.navdy.service.library.device.link.ReaderThread;
import com.navdy.service.library.device.link.WriterThread;
import com.navdy.service.library.device.link.WriterThread.EventProcessor;
import com.navdy.service.library.events.Ext_NavdyEvent;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.NavdyEvent.MessageType;
import com.navdy.service.library.events.NavdyEventUtil;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.WireUtil;
import com.navdy.service.library.events.callcontrol.CallAction;
import com.navdy.service.library.events.callcontrol.PhoneEvent;
import com.navdy.service.library.events.callcontrol.PhoneStatusResponse;
import com.navdy.service.library.events.callcontrol.TelephonyRequest;
import com.navdy.service.library.events.callcontrol.TelephonyResponse;
import com.navdy.service.library.events.input.LaunchAppEvent;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.util.IOUtils;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingDeque;

public class iAP2Link implements Link, PhysicalLayer, StateListener, IAPCommunicationsUpdateListener {
    public static EASession proxyEASession;
    private static final Logger sLogger = new Logger(iAP2Link.class);
    private int bandwidthLevel = 1;
    private ConnectedThread connectedThread;
    private PhoneEvent currentPhoneEvent;
    private EventProcessor eventProcessor = new EventProcessor() {
        public byte[] processEvent(byte[] eventData) {
            MessageType type = WireUtil.getEventType(eventData);
            if (type == null || !iAP2Link.this.eventProcessors.containsKey(type)) {
                return eventData;
            }
            boolean handled = false;
            try {
                handled = ((NavdyEventProcessor) iAP2Link.this.eventProcessors.get(type)).processNavdyEvent((NavdyEvent) iAP2Link.this.wire.parseFrom(eventData, NavdyEvent.class));
            } catch (IOException e) {
                iAP2Link.sLogger.e("Error while parsing request", e);
            }
            return handled ? null : eventData;
        }
    };
    private Map<MessageType, NavdyEventProcessor> eventProcessors = new HashMap();
    private IAPCommunicationsManager iAPCommunicationsManager;
    private iAPProcessor iAPProcessor;
    private CallAction lastCallActionRequested;
    private LinkLayer linkLayer;
    private LinkListener linkListener;
    private final BluetoothAdapter mAdapter = BluetoothAdapter.getDefaultAdapter();
    private iAP2MusicHelper musicHelper;
    private ProxyStream proxyStream = new ProxyStream();
    private ReaderThread readerThread;
    private Wire wire;
    private WriterThread writerThread;

    interface NavdyEventProcessor {
        boolean processNavdyEvent(NavdyEvent navdyEvent);
    }

    private class ConnectedThread extends IOThread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private final SocketAdapter mmSocket;

        public ConnectedThread(SocketAdapter socket) {
            iAP2Link.sLogger.d("create ConnectedThread");
            this.mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                iAP2Link.sLogger.e("temp sockets not created", e);
            }
            this.mmInStream = tmpIn;
            this.mmOutStream = tmpOut;
        }

        public void run() {
            BluetoothDevice device = iAP2Link.this.getRemoteDevice(this.mmSocket);
            iAP2Link.sLogger.i("BEGIN mConnectedThread - " + device.getAddress() + " name:" + device.getName());
            DisconnectCause cause = DisconnectCause.NORMAL;
            iAP2Link.this.linkListener.linkEstablished(ConnectionType.BT_IAP2_LINK);
            iAP2Link.this.linkLayer.connectionStarted(device.getAddress(), device.getName());
            byte[] buffer = new byte[1024];
            while (!this.closing) {
                try {
                    iAP2Link.this.linkLayer.queue(new LinkPacket(Arrays.copyOf(buffer, this.mmInStream.read(buffer))));
                } catch (IOException e) {
                    if (!this.closing) {
                        iAP2Link.sLogger.e("disconnected: " + e.getMessage());
                        cause = DisconnectCause.ABORTED;
                    }
                }
            }
            IOUtils.closeStream(this.mmInStream);
            iAP2Link.this.linkLayer.connectionEnded();
            iAP2Link.this.linkListener.linkLost(ConnectionType.BT_IAP2_LINK, cause);
        }

        public void write(byte[] buffer) {
            try {
                this.mmOutStream.write(buffer);
            } catch (IOException e) {
                iAP2Link.sLogger.e("Exception during write", e);
            }
        }

        public void cancel() {
            super.cancel();
            IOUtils.closeStream(this.mmOutStream);
            IOUtils.closeStream(this.mmSocket);
        }
    }

    private class ProxyStream extends OutputStream {
        private OutputStream outputStream;

        private ProxyStream() {
        }

        /* synthetic */ ProxyStream(iAP2Link x0, AnonymousClass1 x1) {
            this();
        }

        public void setOutputStream(OutputStream stream) {
            this.outputStream = stream;
        }

        public void write(@NonNull byte[] buffer) throws IOException {
            if (this.outputStream != null) {
                this.outputStream.write(buffer);
            }
        }

        public void write(int oneByte) throws IOException {
            throw new IOException("Shouldn't be calling this method!");
        }
    }

    public iAP2Link(IAPListener listener, Context context) {
        this.iAPProcessor = new iAPProcessor(listener, context);
        this.iAPProcessor.setAccessoryName(getName());
        this.iAPCommunicationsManager = new IAPCommunicationsManager(this.iAPProcessor);
        this.musicHelper = new iAP2MusicHelper(this, this.iAPProcessor);
        this.linkLayer = new LinkLayer();
        this.iAPProcessor.connect(this.linkLayer);
        this.iAPProcessor.connect((StateListener) this);
        this.linkLayer.connect(this.iAPProcessor);
        this.linkLayer.connect((PhysicalLayer) this);
        this.wire = new Wire(Ext_NavdyEvent.class);
        addEventProcessors();
    }

    public String getName() {
        return this.mAdapter.getName();
    }

    void sendMessageAsNavdyEvent(Message message) {
        if (message != null && this.linkListener != null) {
            this.linkListener.onNavdyEventReceived(NavdyEventUtil.eventFromMessage(message).toByteArray());
        }
    }

    void addEventProcessor(MessageType type, NavdyEventProcessor processor) {
        if (this.eventProcessors.containsKey(type)) {
            sLogger.w("Already have a processor for that type!");
        }
        this.eventProcessors.put(type, processor);
    }

    private void addEventProcessors() {
        this.eventProcessors.put(MessageType.TelephonyRequest, new NavdyEventProcessor() {
            public boolean processNavdyEvent(NavdyEvent event) {
                TelephonyRequest request = (TelephonyRequest) event.getExtension(Ext_NavdyEvent.telephonyRequest);
                iAP2Link.this.lastCallActionRequested = request.action;
                iAP2Link.this.sendMessageAsNavdyEvent(new TelephonyResponse(request.action, IAPMessageUtility.performActionForTelephonyRequest(request, iAP2Link.this.iAPCommunicationsManager) ? RequestStatus.REQUEST_SUCCESS : RequestStatus.REQUEST_NOT_AVAILABLE, null));
                return true;
            }
        });
        this.eventProcessors.put(MessageType.PhoneStatusRequest, new NavdyEventProcessor() {
            public boolean processNavdyEvent(NavdyEvent event) {
                iAP2Link.this.sendMessageAsNavdyEvent(new PhoneStatusResponse(RequestStatus.REQUEST_SUCCESS, iAP2Link.this.currentPhoneEvent));
                return true;
            }
        });
        this.eventProcessors.put(MessageType.LaunchAppEvent, new NavdyEventProcessor() {
            public boolean processNavdyEvent(NavdyEvent event) {
                LaunchAppEvent launchAppRequest = (LaunchAppEvent) event.getExtension(Ext_NavdyEvent.launchAppEvent);
                if (TextUtils.isEmpty(launchAppRequest.appBundleID)) {
                    iAP2Link.this.iAPProcessor.launchApp(false);
                } else {
                    iAP2Link.this.iAPProcessor.launchApp(launchAppRequest.appBundleID, false);
                }
                return true;
            }
        });
        this.eventProcessors.put(MessageType.CallStateUpdateRequest, new NavdyEventProcessor() {
            public boolean processNavdyEvent(NavdyEvent event) {
                return true;
            }
        });
    }

    public void onCallStateUpdate(CallStateUpdate callStateUpdate) {
        PhoneEvent event = IAPMessageUtility.getPhoneEventForCallStateUpdate(callStateUpdate, this.lastCallActionRequested);
        if (event != null) {
            this.currentPhoneEvent = event;
            this.lastCallActionRequested = null;
            sendMessageAsNavdyEvent(this.currentPhoneEvent);
        }
    }

    public void onCommunicationUpdate(CommunicationUpdate communicationUpdate) {
        sLogger.e("CommunicationUpdate : " + communicationUpdate);
    }

    public boolean start(SocketAdapter socket, LinkedBlockingDeque<EventRequest> outboundEventQueue, LinkListener listener) {
        if ((this.writerThread == null || this.writerThread.isClosing()) && (this.readerThread == null || this.readerThread.isClosing())) {
            this.linkListener = listener;
            this.connectedThread = new ConnectedThread(socket);
            this.writerThread = new WriterThread(outboundEventQueue, this.proxyStream, this.eventProcessor);
            this.connectedThread.start();
            this.writerThread.start();
            return true;
        }
        throw new IllegalStateException("Must stop threads before calling startLink");
    }

    public void setBandwidthLevel(int level) {
        this.bandwidthLevel = level;
        this.musicHelper.setBandwidthLevel(level);
    }

    public int getBandWidthLevel() {
        return this.bandwidthLevel;
    }

    public void close() {
        if (this.musicHelper != null) {
            this.musicHelper.close();
        }
        if (this.connectedThread != null) {
            this.connectedThread.cancel();
            this.connectedThread = null;
        }
        if (this.writerThread != null) {
            this.writerThread.cancel();
            this.writerThread = null;
        }
        if (this.readerThread != null) {
            this.readerThread.cancel();
            this.readerThread = null;
        }
    }

    private boolean startProtobufLink(SocketAdapter socket) {
        InputStream inputStream = null;
        try {
            inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();
            this.readerThread = new ReaderThread(ConnectionType.EA_PROTOBUF, inputStream, this.linkListener, false);
            this.proxyStream.setOutputStream(outputStream);
            this.readerThread.start();
            return true;
        } catch (IOException e) {
            IOUtils.closeStream(inputStream);
            this.readerThread = null;
            return false;
        }
    }

    private void stopProtobufLink() {
        if (this.readerThread != null) {
            this.readerThread.cancel();
            this.readerThread = null;
        }
        this.proxyStream.setOutputStream(null);
    }

    private BluetoothDevice getRemoteDevice(SocketAdapter socket) {
        return this.mAdapter.getRemoteDevice(socket.getRemoteDevice().getBluetoothAddress());
    }

    public void onSessionStart(EASession session) {
        String proto = session.getProtocol();
        if (proto.equals(iAPProcessor.PROTOCOL_V1)) {
            startProtobufLink(new EASessionSocketAdapter(session));
        } else if (proto.equals(iAPProcessor.PROXY_V1)) {
            proxyEASession = session;
            if (this.linkListener != null) {
                this.linkListener.onNetworkLinkReady();
            }
        }
    }

    public void onSessionStop(EASession session) {
        String proto = session.getProtocol();
        if (proto.equals(iAPProcessor.PROTOCOL_V1)) {
            stopProtobufLink();
        } else if (proto.equals(iAPProcessor.PROXY_V1)) {
            proxyEASession = null;
        }
        session.close();
    }

    public void onError() {
        sLogger.e("Error in IAP session");
    }

    public void onReady() {
        try {
            this.iAPCommunicationsManager.setUpdatesListener(this);
            this.iAPCommunicationsManager.startUpdates();
            this.iAPCommunicationsManager.startCommunicationUpdates();
            this.musicHelper.onReady();
            this.iAPProcessor.startHIDSession();
        } catch (Throwable t) {
            sLogger.e("Error starting the call state updates ", t);
        }
    }

    public byte[] getLocalAddress() {
        return Utils.parseMACAddress(this.mAdapter.getAddress());
    }

    public void queue(LinkPacket pkt) {
        if (this.connectedThread != null) {
            this.connectedThread.write(pkt.data);
        }
    }
}
