package com.navdy.hud.mfi;

import com.navdy.hud.mfi.IAPCommunicationsManager.Service;

public class CallStateUpdate {
    public String addressBookID;
    public String callUUID;
    public int conferenceGroup;
    public Direction direction = Direction.Unknown;
    public DisconnectReason disconnectReason = DisconnectReason.Ended;
    public String displayName;
    public boolean isConferenced;
    public String label;
    public String remoteID;
    public Service service = Service.Unknown;
    public Status status = Status.Disconnected;

    public enum Direction {
        Unknown,
        Incoming,
        Outgoing
    }

    public enum DisconnectReason {
        Ended,
        Declined,
        Failed
    }

    public enum Status {
        Disconnected,
        Sending,
        Ringing,
        Connecting,
        Active,
        Held,
        Disconnecting
    }
}
