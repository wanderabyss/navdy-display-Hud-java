package com.navdy.hud.mfi;

public interface PacketQueue<T> {
    void queue(T t);
}
