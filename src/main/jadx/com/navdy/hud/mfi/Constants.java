package com.navdy.hud.mfi;

public interface Constants {
    public static final String DEVICE_NAME = "device_name";
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_EA_READ = 2;
    public static final int MESSAGE_EA_WRITE = 3;
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_TOAST = 5;
    public static final String TOAST = "toast";
}
