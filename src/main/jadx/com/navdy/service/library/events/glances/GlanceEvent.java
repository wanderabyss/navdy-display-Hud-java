package com.navdy.service.library.events.glances;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class GlanceEvent extends Message {
    public static final List<GlanceActions> DEFAULT_ACTIONS = Collections.emptyList();
    public static final List<KeyValue> DEFAULT_GLANCEDATA = Collections.emptyList();
    public static final GlanceType DEFAULT_GLANCETYPE = GlanceType.GLANCE_TYPE_CALENDAR;
    public static final String DEFAULT_ID = "";
    public static final String DEFAULT_LANGUAGE = "";
    public static final Long DEFAULT_POSTTIME = Long.valueOf(0);
    public static final String DEFAULT_PROVIDER = "";
    private static final long serialVersionUID = 0;
    @ProtoField(enumType = GlanceActions.class, label = Label.REPEATED, tag = 6, type = Datatype.ENUM)
    public final List<GlanceActions> actions;
    @ProtoField(label = Label.REPEATED, messageType = KeyValue.class, tag = 5)
    public final List<KeyValue> glanceData;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final GlanceType glanceType;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String id;
    @ProtoField(tag = 7, type = Datatype.STRING)
    public final String language;
    @ProtoField(tag = 4, type = Datatype.INT64)
    public final Long postTime;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String provider;

    public static final class Builder extends com.squareup.wire.Message.Builder<GlanceEvent> {
        public List<GlanceActions> actions;
        public List<KeyValue> glanceData;
        public GlanceType glanceType;
        public String id;
        public String language;
        public Long postTime;
        public String provider;

        public Builder(GlanceEvent message) {
            super(message);
            if (message != null) {
                this.glanceType = message.glanceType;
                this.provider = message.provider;
                this.id = message.id;
                this.postTime = message.postTime;
                this.glanceData = Message.copyOf(message.glanceData);
                this.actions = Message.copyOf(message.actions);
                this.language = message.language;
            }
        }

        public Builder glanceType(GlanceType glanceType) {
            this.glanceType = glanceType;
            return this;
        }

        public Builder provider(String provider) {
            this.provider = provider;
            return this;
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder postTime(Long postTime) {
            this.postTime = postTime;
            return this;
        }

        public Builder glanceData(List<KeyValue> glanceData) {
            this.glanceData = com.squareup.wire.Message.Builder.checkForNulls(glanceData);
            return this;
        }

        public Builder actions(List<GlanceActions> actions) {
            this.actions = com.squareup.wire.Message.Builder.checkForNulls(actions);
            return this;
        }

        public Builder language(String language) {
            this.language = language;
            return this;
        }

        public GlanceEvent build() {
            return new GlanceEvent();
        }
    }

    public enum GlanceActions implements ProtoEnum {
        REPLY(0);
        
        private final int value;

        private GlanceActions(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum GlanceType implements ProtoEnum {
        GLANCE_TYPE_CALENDAR(0),
        GLANCE_TYPE_EMAIL(1),
        GLANCE_TYPE_MESSAGE(2),
        GLANCE_TYPE_SOCIAL(3),
        GLANCE_TYPE_GENERIC(4),
        GLANCE_TYPE_FUEL(5);
        
        private final int value;

        private GlanceType(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public GlanceEvent(GlanceType glanceType, String provider, String id, Long postTime, List<KeyValue> glanceData, List<GlanceActions> actions, String language) {
        this.glanceType = glanceType;
        this.provider = provider;
        this.id = id;
        this.postTime = postTime;
        this.glanceData = Message.immutableCopyOf(glanceData);
        this.actions = Message.immutableCopyOf(actions);
        this.language = language;
    }

    private GlanceEvent(Builder builder) {
        this(builder.glanceType, builder.provider, builder.id, builder.postTime, builder.glanceData, builder.actions, builder.language);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof GlanceEvent)) {
            return false;
        }
        GlanceEvent o = (GlanceEvent) other;
        if (equals( this.glanceType,  o.glanceType) && equals( this.provider,  o.provider) && equals( this.id,  o.id) && equals( this.postTime,  o.postTime) && equals(this.glanceData, o.glanceData) && equals(this.actions, o.actions) && equals( this.language,  o.language)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 1;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.glanceType != null ? this.glanceType.hashCode() : 0) * 37;
        if (this.provider != null) {
            hashCode = this.provider.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.id != null) {
            hashCode = this.id.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.postTime != null) {
            hashCode = this.postTime.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.glanceData != null) {
            hashCode = this.glanceData.hashCode();
        } else {
            hashCode = 1;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.actions != null) {
            i = this.actions.hashCode();
        }
        hashCode = (hashCode + i) * 37;
        if (this.language != null) {
            i2 = this.language.hashCode();
        }
        result = hashCode + i2;
        this.hashCode = result;
        return result;
    }
}
