package com.navdy.service.library.events.photo;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class PhotoUpdateQuery extends Message {
    public static final String DEFAULT_ALBUM = "";
    public static final String DEFAULT_AUTHOR = "";
    public static final String DEFAULT_CHECKSUM = "";
    public static final String DEFAULT_IDENTIFIER = "";
    public static final PhotoType DEFAULT_PHOTOTYPE = PhotoType.PHOTO_CONTACT;
    public static final Long DEFAULT_SIZE = Long.valueOf(0);
    public static final String DEFAULT_TRACK = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String album;
    @ProtoField(tag = 7, type = Datatype.STRING)
    public final String author;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String checksum;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String identifier;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final PhotoType photoType;
    @ProtoField(tag = 3, type = Datatype.INT64)
    public final Long size;
    @ProtoField(tag = 6, type = Datatype.STRING)
    public final String track;

    public static final class Builder extends com.squareup.wire.Message.Builder<PhotoUpdateQuery> {
        public String album;
        public String author;
        public String checksum;
        public String identifier;
        public PhotoType photoType;
        public Long size;
        public String track;

        public Builder(PhotoUpdateQuery message) {
            super(message);
            if (message != null) {
                this.identifier = message.identifier;
                this.photoType = message.photoType;
                this.size = message.size;
                this.checksum = message.checksum;
                this.album = message.album;
                this.track = message.track;
                this.author = message.author;
            }
        }

        public Builder identifier(String identifier) {
            this.identifier = identifier;
            return this;
        }

        public Builder photoType(PhotoType photoType) {
            this.photoType = photoType;
            return this;
        }

        public Builder size(Long size) {
            this.size = size;
            return this;
        }

        public Builder checksum(String checksum) {
            this.checksum = checksum;
            return this;
        }

        public Builder album(String album) {
            this.album = album;
            return this;
        }

        public Builder track(String track) {
            this.track = track;
            return this;
        }

        public Builder author(String author) {
            this.author = author;
            return this;
        }

        public PhotoUpdateQuery build() {
            return new PhotoUpdateQuery();
        }
    }

    public PhotoUpdateQuery(String identifier, PhotoType photoType, Long size, String checksum, String album, String track, String author) {
        this.identifier = identifier;
        this.photoType = photoType;
        this.size = size;
        this.checksum = checksum;
        this.album = album;
        this.track = track;
        this.author = author;
    }

    private PhotoUpdateQuery(Builder builder) {
        this(builder.identifier, builder.photoType, builder.size, builder.checksum, builder.album, builder.track, builder.author);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof PhotoUpdateQuery)) {
            return false;
        }
        PhotoUpdateQuery o = (PhotoUpdateQuery) other;
        if (equals( this.identifier,  o.identifier) && equals( this.photoType,  o.photoType) && equals( this.size,  o.size) && equals( this.checksum,  o.checksum) && equals( this.album,  o.album) && equals( this.track,  o.track) && equals( this.author,  o.author)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.identifier != null ? this.identifier.hashCode() : 0) * 37;
        if (this.photoType != null) {
            hashCode = this.photoType.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.size != null) {
            hashCode = this.size.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.checksum != null) {
            hashCode = this.checksum.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.album != null) {
            hashCode = this.album.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.track != null) {
            hashCode = this.track.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.author != null) {
            i = this.author.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
