package com.navdy.service.library.events.location;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class TripUpdateAck extends Message {
    public static final Integer DEFAULT_SEQUENCE_NUMBER = Integer.valueOf(0);
    public static final Long DEFAULT_TRIP_NUMBER = Long.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.INT32)
    public final Integer sequence_number;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT64)
    public final Long trip_number;

    public static final class Builder extends com.squareup.wire.Message.Builder<TripUpdateAck> {
        public Integer sequence_number;
        public Long trip_number;

        public Builder(TripUpdateAck message) {
            super(message);
            if (message != null) {
                this.trip_number = message.trip_number;
                this.sequence_number = message.sequence_number;
            }
        }

        public Builder trip_number(Long trip_number) {
            this.trip_number = trip_number;
            return this;
        }

        public Builder sequence_number(Integer sequence_number) {
            this.sequence_number = sequence_number;
            return this;
        }

        public TripUpdateAck build() {
            checkRequiredFields();
            return new TripUpdateAck();
        }
    }

    public TripUpdateAck(Long trip_number, Integer sequence_number) {
        this.trip_number = trip_number;
        this.sequence_number = sequence_number;
    }

    private TripUpdateAck(Builder builder) {
        this(builder.trip_number, builder.sequence_number);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof TripUpdateAck)) {
            return false;
        }
        TripUpdateAck o = (TripUpdateAck) other;
        if (equals( this.trip_number,  o.trip_number) && equals( this.sequence_number,  o.sequence_number)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.trip_number != null) {
            result = this.trip_number.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.sequence_number != null) {
            i = this.sequence_number.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
