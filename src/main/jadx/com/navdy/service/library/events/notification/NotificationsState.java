package com.navdy.service.library.events.notification;

import com.squareup.wire.ProtoEnum;

public enum NotificationsState implements ProtoEnum {
    NOTIFICATIONS_ENABLED(1),
    NOTIFICATIONS_STOPPED(2),
    NOTIFICATIONS_CONNECTING(3),
    NOTIFICATIONS_PAIRING_FAILED(4),
    NOTIFICATIONS_SERVICE_UNAVAILABLE(5);
    
    private final int value;

    private NotificationsState(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
