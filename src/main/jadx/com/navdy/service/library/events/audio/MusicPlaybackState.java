package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoEnum;

public enum MusicPlaybackState implements ProtoEnum {
    PLAYBACK_NONE(1),
    PLAYBACK_BUFFERING(2),
    PLAYBACK_CONNECTING(3),
    PLAYBACK_ERROR(4),
    PLAYBACK_PLAYING(5),
    PLAYBACK_PAUSED(6),
    PLAYBACK_STOPPED(7),
    PLAYBACK_FAST_FORWARDING(8),
    PLAYBACK_REWINDING(9),
    PLAYBACK_SKIPPING_TO_NEXT(10),
    PLAYBACK_SKIPPING_TO_PREVIOUS(11);
    
    private final int value;

    private MusicPlaybackState(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
