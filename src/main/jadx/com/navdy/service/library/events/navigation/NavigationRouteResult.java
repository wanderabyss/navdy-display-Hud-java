package com.navdy.service.library.events.navigation;

import com.navdy.service.library.events.location.Coordinate;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class NavigationRouteResult extends Message {
    public static final String DEFAULT_ADDRESS = "";
    public static final Integer DEFAULT_DURATION = Integer.valueOf(0);
    public static final Integer DEFAULT_DURATION_TRAFFIC = Integer.valueOf(0);
    public static final List<Coordinate> DEFAULT_GEOPOINTS_OBSOLETE = Collections.emptyList();
    public static final String DEFAULT_LABEL = "";
    public static final Integer DEFAULT_LENGTH = Integer.valueOf(0);
    public static final String DEFAULT_ROUTEID = "";
    public static final List<Float> DEFAULT_ROUTELATLONGS = Collections.emptyList();
    public static final String DEFAULT_VIA = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 9, type = Datatype.STRING)
    public final String address;
    @ProtoField(label = Label.REQUIRED, tag = 5, type = Datatype.UINT32)
    public final Integer duration;
    @ProtoField(tag = 6, type = Datatype.UINT32)
    public final Integer duration_traffic;
    @ProtoField(label = Label.REPEATED, messageType = Coordinate.class, tag = 3)
    public final List<Coordinate> geoPoints_OBSOLETE;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String label;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.UINT32)
    public final Integer length;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String routeId;
    @ProtoField(label = Label.PACKED, tag = 7, type = Datatype.FLOAT)
    public final List<Float> routeLatLongs;
    @ProtoField(tag = 8, type = Datatype.STRING)
    public final String via;

    public static final class Builder extends com.squareup.wire.Message.Builder<NavigationRouteResult> {
        public String address;
        public Integer duration;
        public Integer duration_traffic;
        public List<Coordinate> geoPoints_OBSOLETE;
        public String label;
        public Integer length;
        public String routeId;
        public List<Float> routeLatLongs;
        public String via;

        public Builder(NavigationRouteResult message) {
            super(message);
            if (message != null) {
                this.routeId = message.routeId;
                this.label = message.label;
                this.geoPoints_OBSOLETE = Message.copyOf(message.geoPoints_OBSOLETE);
                this.length = message.length;
                this.duration = message.duration;
                this.duration_traffic = message.duration_traffic;
                this.routeLatLongs = Message.copyOf(message.routeLatLongs);
                this.via = message.via;
                this.address = message.address;
            }
        }

        public Builder routeId(String routeId) {
            this.routeId = routeId;
            return this;
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Builder geoPoints_OBSOLETE(List<Coordinate> geoPoints_OBSOLETE) {
            this.geoPoints_OBSOLETE = com.squareup.wire.Message.Builder.checkForNulls(geoPoints_OBSOLETE);
            return this;
        }

        public Builder length(Integer length) {
            this.length = length;
            return this;
        }

        public Builder duration(Integer duration) {
            this.duration = duration;
            return this;
        }

        public Builder duration_traffic(Integer duration_traffic) {
            this.duration_traffic = duration_traffic;
            return this;
        }

        public Builder routeLatLongs(List<Float> routeLatLongs) {
            this.routeLatLongs = com.squareup.wire.Message.Builder.checkForNulls(routeLatLongs);
            return this;
        }

        public Builder via(String via) {
            this.via = via;
            return this;
        }

        public Builder address(String address) {
            this.address = address;
            return this;
        }

        public NavigationRouteResult build() {
            checkRequiredFields();
            return new NavigationRouteResult();
        }
    }

    public NavigationRouteResult(String routeId, String label, List<Coordinate> geoPoints_OBSOLETE, Integer length, Integer duration, Integer duration_traffic, List<Float> routeLatLongs, String via, String address) {
        this.routeId = routeId;
        this.label = label;
        this.geoPoints_OBSOLETE = Message.immutableCopyOf(geoPoints_OBSOLETE);
        this.length = length;
        this.duration = duration;
        this.duration_traffic = duration_traffic;
        this.routeLatLongs = Message.immutableCopyOf(routeLatLongs);
        this.via = via;
        this.address = address;
    }

    private NavigationRouteResult(Builder builder) {
        this(builder.routeId, builder.label, builder.geoPoints_OBSOLETE, builder.length, builder.duration, builder.duration_traffic, builder.routeLatLongs, builder.via, builder.address);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NavigationRouteResult)) {
            return false;
        }
        NavigationRouteResult o = (NavigationRouteResult) other;
        if (equals( this.routeId,  o.routeId) && equals( this.label,  o.label) && equals(this.geoPoints_OBSOLETE, o.geoPoints_OBSOLETE) && equals( this.length,  o.length) && equals( this.duration,  o.duration) && equals( this.duration_traffic,  o.duration_traffic) && equals(this.routeLatLongs, o.routeLatLongs) && equals( this.via,  o.via) && equals( this.address,  o.address)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 1;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.routeId != null ? this.routeId.hashCode() : 0) * 37;
        if (this.label != null) {
            hashCode = this.label.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.geoPoints_OBSOLETE != null) {
            hashCode = this.geoPoints_OBSOLETE.hashCode();
        } else {
            hashCode = 1;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.length != null) {
            hashCode = this.length.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.duration != null) {
            hashCode = this.duration.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.duration_traffic != null) {
            hashCode = this.duration_traffic.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.routeLatLongs != null) {
            i = this.routeLatLongs.hashCode();
        }
        i = (hashCode + i) * 37;
        if (this.via != null) {
            hashCode = this.via.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (i + hashCode) * 37;
        if (this.address != null) {
            i2 = this.address.hashCode();
        }
        result = hashCode + i2;
        this.hashCode = result;
        return result;
    }
}
