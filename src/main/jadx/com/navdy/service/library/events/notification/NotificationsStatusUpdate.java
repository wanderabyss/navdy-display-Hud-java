package com.navdy.service.library.events.notification;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class NotificationsStatusUpdate extends Message {
    public static final NotificationsError DEFAULT_ERRORDETAILS = NotificationsError.NOTIFICATIONS_ERROR_UNKNOWN;
    public static final ServiceType DEFAULT_SERVICE = ServiceType.SERVICE_ANCS;
    public static final NotificationsState DEFAULT_STATE = NotificationsState.NOTIFICATIONS_ENABLED;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final NotificationsError errorDetails;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final ServiceType service;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final NotificationsState state;

    public static final class Builder extends com.squareup.wire.Message.Builder<NotificationsStatusUpdate> {
        public NotificationsError errorDetails;
        public ServiceType service;
        public NotificationsState state;

        public Builder(NotificationsStatusUpdate message) {
            super(message);
            if (message != null) {
                this.state = message.state;
                this.service = message.service;
                this.errorDetails = message.errorDetails;
            }
        }

        public Builder state(NotificationsState state) {
            this.state = state;
            return this;
        }

        public Builder service(ServiceType service) {
            this.service = service;
            return this;
        }

        public Builder errorDetails(NotificationsError errorDetails) {
            this.errorDetails = errorDetails;
            return this;
        }

        public NotificationsStatusUpdate build() {
            checkRequiredFields();
            return new NotificationsStatusUpdate();
        }
    }

    public NotificationsStatusUpdate(NotificationsState state, ServiceType service, NotificationsError errorDetails) {
        this.state = state;
        this.service = service;
        this.errorDetails = errorDetails;
    }

    private NotificationsStatusUpdate(Builder builder) {
        this(builder.state, builder.service, builder.errorDetails);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NotificationsStatusUpdate)) {
            return false;
        }
        NotificationsStatusUpdate o = (NotificationsStatusUpdate) other;
        if (equals( this.state,  o.state) && equals( this.service,  o.service) && equals( this.errorDetails,  o.errorDetails)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.state != null ? this.state.hashCode() : 0) * 37;
        if (this.service != null) {
            hashCode = this.service.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.errorDetails != null) {
            i = this.errorDetails.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
