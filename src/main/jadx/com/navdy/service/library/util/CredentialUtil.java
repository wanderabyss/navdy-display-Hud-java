package com.navdy.service.library.util;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import com.navdy.service.library.log.Logger;

public class CredentialUtil {
    public static final Logger logger = new Logger(CredentialUtil.class);

    public static String getCredentials(Context context, String metaName) {
        String credentials = "";
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString(metaName);
        } catch (NameNotFoundException e) {
            logger.e("Failed to load meta-data, NameNotFound: " + e.getMessage());
            return credentials;
        } catch (NullPointerException e2) {
            logger.e("Failed to load meta-data, NullPointer: " + e2.getMessage());
            return credentials;
        }
    }
}
