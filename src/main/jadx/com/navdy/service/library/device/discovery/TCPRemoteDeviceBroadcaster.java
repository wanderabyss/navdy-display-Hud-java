package com.navdy.service.library.device.discovery;

import android.content.Context;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.connection.TCPConnectionInfo;

public class TCPRemoteDeviceBroadcaster extends MDNSDeviceBroadcaster {
    public TCPRemoteDeviceBroadcaster(Context context) {
        super(context, TCPConnectionInfo.nsdServiceWithDeviceId(NavdyDeviceId.getThisDevice(context)));
    }
}
