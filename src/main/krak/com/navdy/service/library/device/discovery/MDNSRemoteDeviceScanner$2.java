package com.navdy.service.library.device.discovery;

class MDNSRemoteDeviceScanner$2 implements android.net.nsd.NsdManager.DiscoveryListener {
    final com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner this$0;
    
    MDNSRemoteDeviceScanner$2(com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner a) {
        super();
        this.this$0 = a;
    }
    
    public void onDiscoveryStarted(String s) {
        this.this$0.mDiscoveryRunning = true;
        this.this$0.discoveryTimer = null;
        com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.d("Service discovery started");
        this.this$0.dispatchOnScanStarted();
    }
    
    public void onDiscoveryStopped(String s) {
        this.this$0.mDiscoveryRunning = false;
        com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.i(new StringBuilder().append("Discovery stopped: ").append(s).toString());
        this.this$0.dispatchOnScanStopped();
    }
    
    public void onServiceFound(android.net.nsd.NsdServiceInfo a) {
        com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.d(new StringBuilder().append("Service discovery success ").append(a).toString());
        if (com.navdy.service.library.device.connection.ConnectionInfo.isValidNavdyServiceInfo(a)) {
            this.this$0.resolveService(a);
        }
    }
    
    public void onServiceLost(android.net.nsd.NsdServiceInfo a) {
        com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.e(new StringBuilder().append("service lost").append(a).toString());
    }
    
    public void onStartDiscoveryFailed(String s, int i) {
        com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.e(new StringBuilder().append("Start Discovery failed: Error code:").append(i).toString());
    }
    
    public void onStopDiscoveryFailed(String s, int i) {
        com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.e(new StringBuilder().append("Stop Discovery failed: Error code:").append(i).toString());
    }
}
