package com.navdy.service.library.device.link;

abstract public interface Link extends java.io.Closeable {
    final public static int BANDWIDTH_LEVEL_LOW = 0;
    final public static int BANDWIDTH_LEVEL_NORMAL = 1;
    
    abstract public void close();
    
    
    abstract public int getBandWidthLevel();
    
    
    abstract public void setBandwidthLevel(int arg);
    
    
    abstract public boolean start(com.navdy.service.library.network.SocketAdapter arg, java.util.concurrent.LinkedBlockingDeque arg0, com.navdy.service.library.device.link.LinkListener arg1);
}
