package com.navdy.service.library.device;

class RemoteDevice$1 implements com.navdy.service.library.device.RemoteDevice$EventDispatcher {
    final com.navdy.service.library.device.RemoteDevice this$0;
    
    RemoteDevice$1(com.navdy.service.library.device.RemoteDevice a) {
        super();
        this.this$0 = a;
    }
    
    public void dispatchEvent(com.navdy.service.library.device.RemoteDevice a, com.navdy.service.library.device.RemoteDevice$Listener a0) {
        a0.onDeviceConnecting(a);
    }
    
    public void dispatchEvent(com.navdy.service.library.util.Listenable a, com.navdy.service.library.util.Listenable$Listener a0) {
        this.dispatchEvent((com.navdy.service.library.device.RemoteDevice)a, (com.navdy.service.library.device.RemoteDevice$Listener)a0);
    }
}
