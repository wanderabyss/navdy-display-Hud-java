package com.navdy.service.library.device.connection;

class ConnectionService$3 extends android.content.BroadcastReceiver {
    final com.navdy.service.library.device.connection.ConnectionService this$0;
    
    ConnectionService$3(com.navdy.service.library.device.connection.ConnectionService a) {
        super();
        this.this$0 = a;
    }
    
    public void onReceive(android.content.Context a, android.content.Intent a0) {
        if (a0.getAction().equals("android.bluetooth.adapter.action.STATE_CHANGED") && a0.getIntExtra("android.bluetooth.adapter.extra.STATE", -2147483648) == 12) {
            if (this.this$0.state != com.navdy.service.library.device.connection.ConnectionService$State.START) {
                this.this$0.logger.i("bluetooth turned on - restarting listeners");
                this.this$0.serviceHandler.sendEmptyMessage(4);
            } else {
                this.this$0.logger.i("bluetooth turned on - exiting START state");
                this.this$0.setState(com.navdy.service.library.device.connection.ConnectionService$State.IDLE);
            }
        }
    }
}
