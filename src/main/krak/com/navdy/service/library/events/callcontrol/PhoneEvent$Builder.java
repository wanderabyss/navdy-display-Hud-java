package com.navdy.service.library.events.callcontrol;

final public class PhoneEvent$Builder extends com.squareup.wire.Message.Builder {
    public String callUUID;
    public String contact_name;
    public String label;
    public String number;
    public com.navdy.service.library.events.callcontrol.PhoneStatus status;
    
    public PhoneEvent$Builder() {
    }
    
    public PhoneEvent$Builder(com.navdy.service.library.events.callcontrol.PhoneEvent a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
            this.number = a.number;
            this.contact_name = a.contact_name;
            this.label = a.label;
            this.callUUID = a.callUUID;
        }
    }
    
    public com.navdy.service.library.events.callcontrol.PhoneEvent build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.callcontrol.PhoneEvent(this, (com.navdy.service.library.events.callcontrol.PhoneEvent$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.callcontrol.PhoneEvent$Builder callUUID(String s) {
        this.callUUID = s;
        return this;
    }
    
    public com.navdy.service.library.events.callcontrol.PhoneEvent$Builder contact_name(String s) {
        this.contact_name = s;
        return this;
    }
    
    public com.navdy.service.library.events.callcontrol.PhoneEvent$Builder label(String s) {
        this.label = s;
        return this;
    }
    
    public com.navdy.service.library.events.callcontrol.PhoneEvent$Builder number(String s) {
        this.number = s;
        return this;
    }
    
    public com.navdy.service.library.events.callcontrol.PhoneEvent$Builder status(com.navdy.service.library.events.callcontrol.PhoneStatus a) {
        this.status = a;
        return this;
    }
}
