package com.navdy.service.library.events.dial;

final public class DialBondResponse extends com.squareup.wire.Message {
    final public static String DEFAULT_MACADDRESS = "";
    final public static String DEFAULT_NAME = "";
    final public static com.navdy.service.library.events.dial.DialError DEFAULT_STATUS;
    final private static long serialVersionUID = 0L;
    final public String macAddress;
    final public String name;
    final public com.navdy.service.library.events.dial.DialError status;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.dial.DialError.DIAL_ERROR;
    }
    
    private DialBondResponse(com.navdy.service.library.events.dial.DialBondResponse$Builder a) {
        this(a.status, a.name, a.macAddress);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    DialBondResponse(com.navdy.service.library.events.dial.DialBondResponse$Builder a, com.navdy.service.library.events.dial.DialBondResponse$1 a0) {
        this(a);
    }
    
    public DialBondResponse(com.navdy.service.library.events.dial.DialError a, String s, String s0) {
        this.status = a;
        this.name = s;
        this.macAddress = s0;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.dial.DialBondResponse) {
                com.navdy.service.library.events.dial.DialBondResponse a0 = (com.navdy.service.library.events.dial.DialBondResponse)a;
                boolean b0 = this.equals(this.status, a0.status);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.name, a0.name)) {
                        break label1;
                    }
                    if (this.equals(this.macAddress, a0.macAddress)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.status == null) ? 0 : this.status.hashCode()) * 37 + ((this.name == null) ? 0 : this.name.hashCode())) * 37 + ((this.macAddress == null) ? 0 : this.macAddress.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
