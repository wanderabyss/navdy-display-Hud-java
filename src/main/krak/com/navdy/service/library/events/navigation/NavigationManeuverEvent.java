package com.navdy.service.library.events.navigation;

final public class NavigationManeuverEvent extends com.squareup.wire.Message {
    final public static String DEFAULT_CURRENTROAD = "";
    final public static String DEFAULT_DISTANCE_TO_PENDING_TURN = "";
    final public static String DEFAULT_ETA = "";
    final public static Long DEFAULT_ETAUTCTIME;
    final public static com.navdy.service.library.events.navigation.NavigationSessionState DEFAULT_NAVIGATIONSTATE;
    final public static String DEFAULT_PENDING_STREET = "";
    final public static com.navdy.service.library.events.navigation.NavigationTurn DEFAULT_PENDING_TURN;
    final public static String DEFAULT_SPEED = "";
    final private static long serialVersionUID = 0L;
    final public String currentRoad;
    final public String distance_to_pending_turn;
    final public String eta;
    final public Long etaUtcTime;
    final public com.navdy.service.library.events.navigation.NavigationSessionState navigationState;
    final public String pending_street;
    final public com.navdy.service.library.events.navigation.NavigationTurn pending_turn;
    final public String speed;
    
    static {
        DEFAULT_PENDING_TURN = com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_START;
        DEFAULT_NAVIGATIONSTATE = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
        DEFAULT_ETAUTCTIME = Long.valueOf(0L);
    }
    
    private NavigationManeuverEvent(com.navdy.service.library.events.navigation.NavigationManeuverEvent$Builder a) {
        this(a.currentRoad, a.pending_turn, a.distance_to_pending_turn, a.pending_street, a.eta, a.speed, a.navigationState, a.etaUtcTime);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NavigationManeuverEvent(com.navdy.service.library.events.navigation.NavigationManeuverEvent$Builder a, com.navdy.service.library.events.navigation.NavigationManeuverEvent$1 a0) {
        this(a);
    }
    
    public NavigationManeuverEvent(String s, com.navdy.service.library.events.navigation.NavigationTurn a, String s0, String s1, String s2, String s3, com.navdy.service.library.events.navigation.NavigationSessionState a0, Long a1) {
        this.currentRoad = s;
        this.pending_turn = a;
        this.distance_to_pending_turn = s0;
        this.pending_street = s1;
        this.eta = s2;
        this.speed = s3;
        this.navigationState = a0;
        this.etaUtcTime = a1;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.navigation.NavigationManeuverEvent) {
                com.navdy.service.library.events.navigation.NavigationManeuverEvent a0 = (com.navdy.service.library.events.navigation.NavigationManeuverEvent)a;
                boolean b0 = this.equals(this.currentRoad, a0.currentRoad);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.pending_turn, a0.pending_turn)) {
                        break label1;
                    }
                    if (!this.equals(this.distance_to_pending_turn, a0.distance_to_pending_turn)) {
                        break label1;
                    }
                    if (!this.equals(this.pending_street, a0.pending_street)) {
                        break label1;
                    }
                    if (!this.equals(this.eta, a0.eta)) {
                        break label1;
                    }
                    if (!this.equals(this.speed, a0.speed)) {
                        break label1;
                    }
                    if (!this.equals(this.navigationState, a0.navigationState)) {
                        break label1;
                    }
                    if (this.equals(this.etaUtcTime, a0.etaUtcTime)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((((((this.currentRoad == null) ? 0 : this.currentRoad.hashCode()) * 37 + ((this.pending_turn == null) ? 0 : this.pending_turn.hashCode())) * 37 + ((this.distance_to_pending_turn == null) ? 0 : this.distance_to_pending_turn.hashCode())) * 37 + ((this.pending_street == null) ? 0 : this.pending_street.hashCode())) * 37 + ((this.eta == null) ? 0 : this.eta.hashCode())) * 37 + ((this.speed == null) ? 0 : this.speed.hashCode())) * 37 + ((this.navigationState == null) ? 0 : this.navigationState.hashCode())) * 37 + ((this.etaUtcTime == null) ? 0 : this.etaUtcTime.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
