package com.navdy.service.library.events.callcontrol;

final public class PhoneBatteryStatus extends com.squareup.wire.Message {
    final public static Boolean DEFAULT_CHARGING;
    final public static Integer DEFAULT_LEVEL;
    final public static com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus DEFAULT_STATUS;
    final private static long serialVersionUID = 0L;
    final public Boolean charging;
    final public Integer level;
    final public com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus status;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus.BATTERY_OK;
        DEFAULT_LEVEL = Integer.valueOf(0);
        DEFAULT_CHARGING = Boolean.valueOf(false);
    }
    
    public PhoneBatteryStatus(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus a, Integer a0, Boolean a1) {
        this.status = a;
        this.level = a0;
        this.charging = a1;
    }
    
    private PhoneBatteryStatus(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$Builder a) {
        this(a.status, a.level, a.charging);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    PhoneBatteryStatus(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$Builder a, com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.callcontrol.PhoneBatteryStatus) {
                com.navdy.service.library.events.callcontrol.PhoneBatteryStatus a0 = (com.navdy.service.library.events.callcontrol.PhoneBatteryStatus)a;
                boolean b0 = this.equals(this.status, a0.status);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.level, a0.level)) {
                        break label1;
                    }
                    if (this.equals(this.charging, a0.charging)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.status == null) ? 0 : this.status.hashCode()) * 37 + ((this.level == null) ? 0 : this.level.hashCode())) * 37 + ((this.charging == null) ? 0 : this.charging.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
