package com.navdy.service.library.events.notification;

final public class NotificationAction extends com.squareup.wire.Message {
    final public static Integer DEFAULT_ACTIONID;
    final public static String DEFAULT_HANDLER = "";
    final public static Integer DEFAULT_ICONRESOURCE;
    final public static Integer DEFAULT_ICONRESOURCEFOCUSED;
    final public static Integer DEFAULT_ID;
    final public static String DEFAULT_LABEL = "";
    final public static Integer DEFAULT_LABELID;
    final public static Integer DEFAULT_NOTIFICATIONID;
    final private static long serialVersionUID = 0L;
    final public Integer actionId;
    final public String handler;
    final public Integer iconResource;
    final public Integer iconResourceFocused;
    final public Integer id;
    final public String label;
    final public Integer labelId;
    final public Integer notificationId;
    
    static {
        DEFAULT_ID = Integer.valueOf(0);
        DEFAULT_NOTIFICATIONID = Integer.valueOf(0);
        DEFAULT_ACTIONID = Integer.valueOf(0);
        DEFAULT_LABELID = Integer.valueOf(0);
        DEFAULT_ICONRESOURCE = Integer.valueOf(0);
        DEFAULT_ICONRESOURCEFOCUSED = Integer.valueOf(0);
    }
    
    private NotificationAction(com.navdy.service.library.events.notification.NotificationAction$Builder a) {
        this(a.handler, a.id, a.notificationId, a.actionId, a.labelId, a.label, a.iconResource, a.iconResourceFocused);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NotificationAction(com.navdy.service.library.events.notification.NotificationAction$Builder a, com.navdy.service.library.events.notification.NotificationAction$1 a0) {
        this(a);
    }
    
    public NotificationAction(String s, Integer a, Integer a0, Integer a1, Integer a2, String s0, Integer a3, Integer a4) {
        this.handler = s;
        this.id = a;
        this.notificationId = a0;
        this.actionId = a1;
        this.labelId = a2;
        this.label = s0;
        this.iconResource = a3;
        this.iconResourceFocused = a4;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.notification.NotificationAction) {
                com.navdy.service.library.events.notification.NotificationAction a0 = (com.navdy.service.library.events.notification.NotificationAction)a;
                boolean b0 = this.equals(this.handler, a0.handler);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.id, a0.id)) {
                        break label1;
                    }
                    if (!this.equals(this.notificationId, a0.notificationId)) {
                        break label1;
                    }
                    if (!this.equals(this.actionId, a0.actionId)) {
                        break label1;
                    }
                    if (!this.equals(this.labelId, a0.labelId)) {
                        break label1;
                    }
                    if (!this.equals(this.label, a0.label)) {
                        break label1;
                    }
                    if (!this.equals(this.iconResource, a0.iconResource)) {
                        break label1;
                    }
                    if (this.equals(this.iconResourceFocused, a0.iconResourceFocused)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((((((this.handler == null) ? 0 : this.handler.hashCode()) * 37 + ((this.id == null) ? 0 : this.id.hashCode())) * 37 + ((this.notificationId == null) ? 0 : this.notificationId.hashCode())) * 37 + ((this.actionId == null) ? 0 : this.actionId.hashCode())) * 37 + ((this.labelId == null) ? 0 : this.labelId.hashCode())) * 37 + ((this.label == null) ? 0 : this.label.hashCode())) * 37 + ((this.iconResource == null) ? 0 : this.iconResource.hashCode())) * 37 + ((this.iconResourceFocused == null) ? 0 : this.iconResourceFocused.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
