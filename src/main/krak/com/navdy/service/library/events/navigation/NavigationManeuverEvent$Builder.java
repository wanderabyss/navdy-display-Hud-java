package com.navdy.service.library.events.navigation;

final public class NavigationManeuverEvent$Builder extends com.squareup.wire.Message.Builder {
    public String currentRoad;
    public String distance_to_pending_turn;
    public String eta;
    public Long etaUtcTime;
    public com.navdy.service.library.events.navigation.NavigationSessionState navigationState;
    public String pending_street;
    public com.navdy.service.library.events.navigation.NavigationTurn pending_turn;
    public String speed;
    
    public NavigationManeuverEvent$Builder() {
    }
    
    public NavigationManeuverEvent$Builder(com.navdy.service.library.events.navigation.NavigationManeuverEvent a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.currentRoad = a.currentRoad;
            this.pending_turn = a.pending_turn;
            this.distance_to_pending_turn = a.distance_to_pending_turn;
            this.pending_street = a.pending_street;
            this.eta = a.eta;
            this.speed = a.speed;
            this.navigationState = a.navigationState;
            this.etaUtcTime = a.etaUtcTime;
        }
    }
    
    public com.navdy.service.library.events.navigation.NavigationManeuverEvent build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.navigation.NavigationManeuverEvent(this, (com.navdy.service.library.events.navigation.NavigationManeuverEvent$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.navigation.NavigationManeuverEvent$Builder currentRoad(String s) {
        this.currentRoad = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationManeuverEvent$Builder distance_to_pending_turn(String s) {
        this.distance_to_pending_turn = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationManeuverEvent$Builder eta(String s) {
        this.eta = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationManeuverEvent$Builder etaUtcTime(Long a) {
        this.etaUtcTime = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationManeuverEvent$Builder navigationState(com.navdy.service.library.events.navigation.NavigationSessionState a) {
        this.navigationState = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationManeuverEvent$Builder pending_street(String s) {
        this.pending_street = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationManeuverEvent$Builder pending_turn(com.navdy.service.library.events.navigation.NavigationTurn a) {
        this.pending_turn = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationManeuverEvent$Builder speed(String s) {
        this.speed = s;
        return this;
    }
}
