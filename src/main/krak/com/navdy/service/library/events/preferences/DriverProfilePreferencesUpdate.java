package com.navdy.service.library.events.preferences;

final public class DriverProfilePreferencesUpdate extends com.squareup.wire.Message {
    final public static Long DEFAULT_SERIAL_NUMBER;
    final public static com.navdy.service.library.events.RequestStatus DEFAULT_STATUS;
    final public static String DEFAULT_STATUSDETAIL = "";
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.preferences.DriverProfilePreferences preferences;
    final public Long serial_number;
    final public com.navdy.service.library.events.RequestStatus status;
    final public String statusDetail;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
        DEFAULT_SERIAL_NUMBER = Long.valueOf(0L);
    }
    
    public DriverProfilePreferencesUpdate(com.navdy.service.library.events.RequestStatus a, String s, Long a0, com.navdy.service.library.events.preferences.DriverProfilePreferences a1) {
        this.status = a;
        this.statusDetail = s;
        this.serial_number = a0;
        this.preferences = a1;
    }
    
    private DriverProfilePreferencesUpdate(com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate$Builder a) {
        this(a.status, a.statusDetail, a.serial_number, a.preferences);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    DriverProfilePreferencesUpdate(com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate$Builder a, com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate) {
                com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate a0 = (com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate)a;
                boolean b0 = this.equals(this.status, a0.status);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.statusDetail, a0.statusDetail)) {
                        break label1;
                    }
                    if (!this.equals(this.serial_number, a0.serial_number)) {
                        break label1;
                    }
                    if (this.equals(this.preferences, a0.preferences)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((this.status == null) ? 0 : this.status.hashCode()) * 37 + ((this.statusDetail == null) ? 0 : this.statusDetail.hashCode())) * 37 + ((this.serial_number == null) ? 0 : this.serial_number.hashCode())) * 37 + ((this.preferences == null) ? 0 : this.preferences.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
