package com.navdy.service.library.events.audio;

final public class AudioStatus$ProfileType extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.audio.AudioStatus$ProfileType[] $VALUES;
    final public static com.navdy.service.library.events.audio.AudioStatus$ProfileType AUDIO_PROFILE_A2DP;
    final public static com.navdy.service.library.events.audio.AudioStatus$ProfileType AUDIO_PROFILE_HFP;
    final public static com.navdy.service.library.events.audio.AudioStatus$ProfileType AUDIO_PROFILE_NONE;
    final private int value;
    
    static {
        AUDIO_PROFILE_NONE = new com.navdy.service.library.events.audio.AudioStatus$ProfileType("AUDIO_PROFILE_NONE", 0, 1);
        AUDIO_PROFILE_A2DP = new com.navdy.service.library.events.audio.AudioStatus$ProfileType("AUDIO_PROFILE_A2DP", 1, 2);
        AUDIO_PROFILE_HFP = new com.navdy.service.library.events.audio.AudioStatus$ProfileType("AUDIO_PROFILE_HFP", 2, 3);
        com.navdy.service.library.events.audio.AudioStatus$ProfileType[] a = new com.navdy.service.library.events.audio.AudioStatus$ProfileType[3];
        a[0] = AUDIO_PROFILE_NONE;
        a[1] = AUDIO_PROFILE_A2DP;
        a[2] = AUDIO_PROFILE_HFP;
        $VALUES = a;
    }
    
    private AudioStatus$ProfileType(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.audio.AudioStatus$ProfileType valueOf(String s) {
        return (com.navdy.service.library.events.audio.AudioStatus$ProfileType)Enum.valueOf(com.navdy.service.library.events.audio.AudioStatus$ProfileType.class, s);
    }
    
    public static com.navdy.service.library.events.audio.AudioStatus$ProfileType[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
