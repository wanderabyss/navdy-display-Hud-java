package com.navdy.service.library.events.photo;

final public class PhotoRequest extends com.squareup.wire.Message {
    final public static String DEFAULT_IDENTIFIER = "";
    final public static String DEFAULT_NAME = "";
    final public static String DEFAULT_PHOTOCHECKSUM = "";
    final public static com.navdy.service.library.events.photo.PhotoType DEFAULT_PHOTOTYPE;
    final private static long serialVersionUID = 0L;
    final public String identifier;
    final public String name;
    final public String photoChecksum;
    final public com.navdy.service.library.events.photo.PhotoType photoType;
    
    static {
        DEFAULT_PHOTOTYPE = com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT;
    }
    
    private PhotoRequest(com.navdy.service.library.events.photo.PhotoRequest$Builder a) {
        this(a.identifier, a.photoChecksum, a.photoType, a.name);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    PhotoRequest(com.navdy.service.library.events.photo.PhotoRequest$Builder a, com.navdy.service.library.events.photo.PhotoRequest$1 a0) {
        this(a);
    }
    
    public PhotoRequest(String s, String s0, com.navdy.service.library.events.photo.PhotoType a, String s1) {
        this.identifier = s;
        this.photoChecksum = s0;
        this.photoType = a;
        this.name = s1;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.photo.PhotoRequest) {
                com.navdy.service.library.events.photo.PhotoRequest a0 = (com.navdy.service.library.events.photo.PhotoRequest)a;
                boolean b0 = this.equals(this.identifier, a0.identifier);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.photoChecksum, a0.photoChecksum)) {
                        break label1;
                    }
                    if (!this.equals(this.photoType, a0.photoType)) {
                        break label1;
                    }
                    if (this.equals(this.name, a0.name)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((this.identifier == null) ? 0 : this.identifier.hashCode()) * 37 + ((this.photoChecksum == null) ? 0 : this.photoChecksum.hashCode())) * 37 + ((this.photoType == null) ? 0 : this.photoType.hashCode())) * 37 + ((this.name == null) ? 0 : this.name.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
