package com.navdy.service.library.events.input;

final public class MediaRemoteKey extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.input.MediaRemoteKey[] $VALUES;
    final public static com.navdy.service.library.events.input.MediaRemoteKey MEDIA_REMOTE_KEY_NEXT;
    final public static com.navdy.service.library.events.input.MediaRemoteKey MEDIA_REMOTE_KEY_PLAY;
    final public static com.navdy.service.library.events.input.MediaRemoteKey MEDIA_REMOTE_KEY_PREV;
    final public static com.navdy.service.library.events.input.MediaRemoteKey MEDIA_REMOTE_KEY_SIRI;
    final private int value;
    
    static {
        MEDIA_REMOTE_KEY_SIRI = new com.navdy.service.library.events.input.MediaRemoteKey("MEDIA_REMOTE_KEY_SIRI", 0, 1);
        MEDIA_REMOTE_KEY_PLAY = new com.navdy.service.library.events.input.MediaRemoteKey("MEDIA_REMOTE_KEY_PLAY", 1, 2);
        MEDIA_REMOTE_KEY_NEXT = new com.navdy.service.library.events.input.MediaRemoteKey("MEDIA_REMOTE_KEY_NEXT", 2, 3);
        MEDIA_REMOTE_KEY_PREV = new com.navdy.service.library.events.input.MediaRemoteKey("MEDIA_REMOTE_KEY_PREV", 3, 4);
        com.navdy.service.library.events.input.MediaRemoteKey[] a = new com.navdy.service.library.events.input.MediaRemoteKey[4];
        a[0] = MEDIA_REMOTE_KEY_SIRI;
        a[1] = MEDIA_REMOTE_KEY_PLAY;
        a[2] = MEDIA_REMOTE_KEY_NEXT;
        a[3] = MEDIA_REMOTE_KEY_PREV;
        $VALUES = a;
    }
    
    private MediaRemoteKey(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.input.MediaRemoteKey valueOf(String s) {
        return (com.navdy.service.library.events.input.MediaRemoteKey)Enum.valueOf(com.navdy.service.library.events.input.MediaRemoteKey.class, s);
    }
    
    public static com.navdy.service.library.events.input.MediaRemoteKey[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
