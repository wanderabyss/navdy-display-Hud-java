package com.navdy.service.library.events.preferences;

final public class LocalPreferences extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.settings.DateTimeConfiguration$Clock DEFAULT_CLOCKFORMAT;
    final public static Boolean DEFAULT_MANUALZOOM;
    final public static Float DEFAULT_MANUALZOOMLEVEL;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.settings.DateTimeConfiguration$Clock clockFormat;
    final public Boolean manualZoom;
    final public Float manualZoomLevel;
    
    static {
        DEFAULT_MANUALZOOM = Boolean.valueOf(false);
        DEFAULT_MANUALZOOMLEVEL = Float.valueOf(0.0f);
        DEFAULT_CLOCKFORMAT = com.navdy.service.library.events.settings.DateTimeConfiguration$Clock.CLOCK_24_HOUR;
    }
    
    private LocalPreferences(com.navdy.service.library.events.preferences.LocalPreferences$Builder a) {
        this(a.manualZoom, a.manualZoomLevel, a.clockFormat);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    LocalPreferences(com.navdy.service.library.events.preferences.LocalPreferences$Builder a, com.navdy.service.library.events.preferences.LocalPreferences$1 a0) {
        this(a);
    }
    
    public LocalPreferences(Boolean a, Float a0, com.navdy.service.library.events.settings.DateTimeConfiguration$Clock a1) {
        this.manualZoom = a;
        this.manualZoomLevel = a0;
        this.clockFormat = a1;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.preferences.LocalPreferences) {
                com.navdy.service.library.events.preferences.LocalPreferences a0 = (com.navdy.service.library.events.preferences.LocalPreferences)a;
                boolean b0 = this.equals(this.manualZoom, a0.manualZoom);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.manualZoomLevel, a0.manualZoomLevel)) {
                        break label1;
                    }
                    if (this.equals(this.clockFormat, a0.clockFormat)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.manualZoom == null) ? 0 : this.manualZoom.hashCode()) * 37 + ((this.manualZoomLevel == null) ? 0 : this.manualZoomLevel.hashCode())) * 37 + ((this.clockFormat == null) ? 0 : this.clockFormat.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
