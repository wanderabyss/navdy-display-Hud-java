package com.navdy.service.library.events.audio;

final public class MusicShuffleMode extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.audio.MusicShuffleMode[] $VALUES;
    final public static com.navdy.service.library.events.audio.MusicShuffleMode MUSIC_SHUFFLE_MODE_ALBUMS;
    final public static com.navdy.service.library.events.audio.MusicShuffleMode MUSIC_SHUFFLE_MODE_OFF;
    final public static com.navdy.service.library.events.audio.MusicShuffleMode MUSIC_SHUFFLE_MODE_SONGS;
    final public static com.navdy.service.library.events.audio.MusicShuffleMode MUSIC_SHUFFLE_MODE_UNKNOWN;
    final private int value;
    
    static {
        MUSIC_SHUFFLE_MODE_UNKNOWN = new com.navdy.service.library.events.audio.MusicShuffleMode("MUSIC_SHUFFLE_MODE_UNKNOWN", 0, 1);
        MUSIC_SHUFFLE_MODE_OFF = new com.navdy.service.library.events.audio.MusicShuffleMode("MUSIC_SHUFFLE_MODE_OFF", 1, 2);
        MUSIC_SHUFFLE_MODE_SONGS = new com.navdy.service.library.events.audio.MusicShuffleMode("MUSIC_SHUFFLE_MODE_SONGS", 2, 3);
        MUSIC_SHUFFLE_MODE_ALBUMS = new com.navdy.service.library.events.audio.MusicShuffleMode("MUSIC_SHUFFLE_MODE_ALBUMS", 3, 4);
        com.navdy.service.library.events.audio.MusicShuffleMode[] a = new com.navdy.service.library.events.audio.MusicShuffleMode[4];
        a[0] = MUSIC_SHUFFLE_MODE_UNKNOWN;
        a[1] = MUSIC_SHUFFLE_MODE_OFF;
        a[2] = MUSIC_SHUFFLE_MODE_SONGS;
        a[3] = MUSIC_SHUFFLE_MODE_ALBUMS;
        $VALUES = a;
    }
    
    private MusicShuffleMode(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.audio.MusicShuffleMode valueOf(String s) {
        return (com.navdy.service.library.events.audio.MusicShuffleMode)Enum.valueOf(com.navdy.service.library.events.audio.MusicShuffleMode.class, s);
    }
    
    public static com.navdy.service.library.events.audio.MusicShuffleMode[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
