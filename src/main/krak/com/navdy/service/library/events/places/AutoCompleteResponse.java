package com.navdy.service.library.events.places;

final public class AutoCompleteResponse extends com.squareup.wire.Message {
    final public static String DEFAULT_PARTIALSEARCH = "";
    final public static java.util.List DEFAULT_RESULTS;
    final public static com.navdy.service.library.events.RequestStatus DEFAULT_STATUS;
    final public static String DEFAULT_STATUSDETAIL = "";
    final private static long serialVersionUID = 0L;
    final public String partialSearch;
    final public java.util.List results;
    final public com.navdy.service.library.events.RequestStatus status;
    final public String statusDetail;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
        DEFAULT_RESULTS = java.util.Collections.emptyList();
    }
    
    private AutoCompleteResponse(com.navdy.service.library.events.places.AutoCompleteResponse$Builder a) {
        this(a.partialSearch, a.status, a.statusDetail, a.results);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    AutoCompleteResponse(com.navdy.service.library.events.places.AutoCompleteResponse$Builder a, com.navdy.service.library.events.places.AutoCompleteResponse$1 a0) {
        this(a);
    }
    
    public AutoCompleteResponse(String s, com.navdy.service.library.events.RequestStatus a, String s0, java.util.List a0) {
        this.partialSearch = s;
        this.status = a;
        this.statusDetail = s0;
        this.results = com.navdy.service.library.events.places.AutoCompleteResponse.immutableCopyOf(a0);
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.places.AutoCompleteResponse.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.places.AutoCompleteResponse) {
                com.navdy.service.library.events.places.AutoCompleteResponse a0 = (com.navdy.service.library.events.places.AutoCompleteResponse)a;
                boolean b0 = this.equals(this.partialSearch, a0.partialSearch);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.status, a0.status)) {
                        break label1;
                    }
                    if (!this.equals(this.statusDetail, a0.statusDetail)) {
                        break label1;
                    }
                    if (this.equals(this.results, a0.results)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((this.partialSearch == null) ? 0 : this.partialSearch.hashCode()) * 37 + ((this.status == null) ? 0 : this.status.hashCode())) * 37 + ((this.statusDetail == null) ? 0 : this.statusDetail.hashCode())) * 37 + ((this.results == null) ? 1 : this.results.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
