package com.navdy.service.library.events.file;

final public class FileTransferError extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.file.FileTransferError[] $VALUES;
    final public static com.navdy.service.library.events.file.FileTransferError FILE_TRANSFER_ABORTED;
    final public static com.navdy.service.library.events.file.FileTransferError FILE_TRANSFER_CHECKSUM_ERROR;
    final public static com.navdy.service.library.events.file.FileTransferError FILE_TRANSFER_HOST_BUSY;
    final public static com.navdy.service.library.events.file.FileTransferError FILE_TRANSFER_ILLEGAL_CHUNK;
    final public static com.navdy.service.library.events.file.FileTransferError FILE_TRANSFER_INSUFFICIENT_SPACE;
    final public static com.navdy.service.library.events.file.FileTransferError FILE_TRANSFER_IO_ERROR;
    final public static com.navdy.service.library.events.file.FileTransferError FILE_TRANSFER_NOT_INITIATED;
    final public static com.navdy.service.library.events.file.FileTransferError FILE_TRANSFER_NO_ERROR;
    final public static com.navdy.service.library.events.file.FileTransferError FILE_TRANSFER_PERMISSION_DENIED;
    final private int value;
    
    static {
        FILE_TRANSFER_NO_ERROR = new com.navdy.service.library.events.file.FileTransferError("FILE_TRANSFER_NO_ERROR", 0, 1);
        FILE_TRANSFER_INSUFFICIENT_SPACE = new com.navdy.service.library.events.file.FileTransferError("FILE_TRANSFER_INSUFFICIENT_SPACE", 1, 2);
        FILE_TRANSFER_ABORTED = new com.navdy.service.library.events.file.FileTransferError("FILE_TRANSFER_ABORTED", 2, 3);
        FILE_TRANSFER_IO_ERROR = new com.navdy.service.library.events.file.FileTransferError("FILE_TRANSFER_IO_ERROR", 3, 4);
        FILE_TRANSFER_ILLEGAL_CHUNK = new com.navdy.service.library.events.file.FileTransferError("FILE_TRANSFER_ILLEGAL_CHUNK", 4, 5);
        FILE_TRANSFER_NOT_INITIATED = new com.navdy.service.library.events.file.FileTransferError("FILE_TRANSFER_NOT_INITIATED", 5, 6);
        FILE_TRANSFER_PERMISSION_DENIED = new com.navdy.service.library.events.file.FileTransferError("FILE_TRANSFER_PERMISSION_DENIED", 6, 7);
        FILE_TRANSFER_CHECKSUM_ERROR = new com.navdy.service.library.events.file.FileTransferError("FILE_TRANSFER_CHECKSUM_ERROR", 7, 8);
        FILE_TRANSFER_HOST_BUSY = new com.navdy.service.library.events.file.FileTransferError("FILE_TRANSFER_HOST_BUSY", 8, 9);
        com.navdy.service.library.events.file.FileTransferError[] a = new com.navdy.service.library.events.file.FileTransferError[9];
        a[0] = FILE_TRANSFER_NO_ERROR;
        a[1] = FILE_TRANSFER_INSUFFICIENT_SPACE;
        a[2] = FILE_TRANSFER_ABORTED;
        a[3] = FILE_TRANSFER_IO_ERROR;
        a[4] = FILE_TRANSFER_ILLEGAL_CHUNK;
        a[5] = FILE_TRANSFER_NOT_INITIATED;
        a[6] = FILE_TRANSFER_PERMISSION_DENIED;
        a[7] = FILE_TRANSFER_CHECKSUM_ERROR;
        a[8] = FILE_TRANSFER_HOST_BUSY;
        $VALUES = a;
    }
    
    private FileTransferError(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.file.FileTransferError valueOf(String s) {
        return (com.navdy.service.library.events.file.FileTransferError)Enum.valueOf(com.navdy.service.library.events.file.FileTransferError.class, s);
    }
    
    public static com.navdy.service.library.events.file.FileTransferError[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
