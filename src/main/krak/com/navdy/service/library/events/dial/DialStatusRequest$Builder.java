package com.navdy.service.library.events.dial;

final public class DialStatusRequest$Builder extends com.squareup.wire.Message.Builder {
    public DialStatusRequest$Builder() {
    }
    
    public DialStatusRequest$Builder(com.navdy.service.library.events.dial.DialStatusRequest a) {
        super((com.squareup.wire.Message)a);
    }
    
    public com.navdy.service.library.events.dial.DialStatusRequest build() {
        return new com.navdy.service.library.events.dial.DialStatusRequest(this, (com.navdy.service.library.events.dial.DialStatusRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
}
