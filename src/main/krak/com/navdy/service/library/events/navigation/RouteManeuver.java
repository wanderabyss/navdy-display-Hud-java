package com.navdy.service.library.events.navigation;

final public class RouteManeuver extends com.squareup.wire.Message {
    final public static String DEFAULT_CURRENTROAD = "";
    final public static Float DEFAULT_DISTANCETOPENDINGROAD;
    final public static com.navdy.service.library.events.navigation.DistanceUnit DEFAULT_DISTANCETOPENDINGROADUNIT;
    final public static Integer DEFAULT_MANEUVERTIME;
    final public static String DEFAULT_PENDINGROAD = "";
    final public static com.navdy.service.library.events.navigation.NavigationTurn DEFAULT_PENDINGTURN;
    final private static long serialVersionUID = 0L;
    final public String currentRoad;
    final public Float distanceToPendingRoad;
    final public com.navdy.service.library.events.navigation.DistanceUnit distanceToPendingRoadUnit;
    final public Integer maneuverTime;
    final public String pendingRoad;
    final public com.navdy.service.library.events.navigation.NavigationTurn pendingTurn;
    
    static {
        DEFAULT_PENDINGTURN = com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_START;
        DEFAULT_DISTANCETOPENDINGROAD = Float.valueOf(0.0f);
        DEFAULT_DISTANCETOPENDINGROADUNIT = com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_MILES;
        DEFAULT_MANEUVERTIME = Integer.valueOf(0);
    }
    
    private RouteManeuver(com.navdy.service.library.events.navigation.RouteManeuver$Builder a) {
        this(a.currentRoad, a.pendingTurn, a.pendingRoad, a.distanceToPendingRoad, a.distanceToPendingRoadUnit, a.maneuverTime);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    RouteManeuver(com.navdy.service.library.events.navigation.RouteManeuver$Builder a, com.navdy.service.library.events.navigation.RouteManeuver$1 a0) {
        this(a);
    }
    
    public RouteManeuver(String s, com.navdy.service.library.events.navigation.NavigationTurn a, String s0, Float a0, com.navdy.service.library.events.navigation.DistanceUnit a1, Integer a2) {
        this.currentRoad = s;
        this.pendingTurn = a;
        this.pendingRoad = s0;
        this.distanceToPendingRoad = a0;
        this.distanceToPendingRoadUnit = a1;
        this.maneuverTime = a2;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.navigation.RouteManeuver) {
                com.navdy.service.library.events.navigation.RouteManeuver a0 = (com.navdy.service.library.events.navigation.RouteManeuver)a;
                boolean b0 = this.equals(this.currentRoad, a0.currentRoad);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.pendingTurn, a0.pendingTurn)) {
                        break label1;
                    }
                    if (!this.equals(this.pendingRoad, a0.pendingRoad)) {
                        break label1;
                    }
                    if (!this.equals(this.distanceToPendingRoad, a0.distanceToPendingRoad)) {
                        break label1;
                    }
                    if (!this.equals(this.distanceToPendingRoadUnit, a0.distanceToPendingRoadUnit)) {
                        break label1;
                    }
                    if (this.equals(this.maneuverTime, a0.maneuverTime)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((((this.currentRoad == null) ? 0 : this.currentRoad.hashCode()) * 37 + ((this.pendingTurn == null) ? 0 : this.pendingTurn.hashCode())) * 37 + ((this.pendingRoad == null) ? 0 : this.pendingRoad.hashCode())) * 37 + ((this.distanceToPendingRoad == null) ? 0 : this.distanceToPendingRoad.hashCode())) * 37 + ((this.distanceToPendingRoadUnit == null) ? 0 : this.distanceToPendingRoadUnit.hashCode())) * 37 + ((this.maneuverTime == null) ? 0 : this.maneuverTime.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
