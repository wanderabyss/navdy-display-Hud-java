package com.navdy.service.library.events.audio;

final public class MusicRepeatMode extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.audio.MusicRepeatMode[] $VALUES;
    final public static com.navdy.service.library.events.audio.MusicRepeatMode MUSIC_REPEAT_MODE_ALL;
    final public static com.navdy.service.library.events.audio.MusicRepeatMode MUSIC_REPEAT_MODE_OFF;
    final public static com.navdy.service.library.events.audio.MusicRepeatMode MUSIC_REPEAT_MODE_ONE;
    final public static com.navdy.service.library.events.audio.MusicRepeatMode MUSIC_REPEAT_MODE_UNKNOWN;
    final private int value;
    
    static {
        MUSIC_REPEAT_MODE_UNKNOWN = new com.navdy.service.library.events.audio.MusicRepeatMode("MUSIC_REPEAT_MODE_UNKNOWN", 0, 1);
        MUSIC_REPEAT_MODE_OFF = new com.navdy.service.library.events.audio.MusicRepeatMode("MUSIC_REPEAT_MODE_OFF", 1, 2);
        MUSIC_REPEAT_MODE_ONE = new com.navdy.service.library.events.audio.MusicRepeatMode("MUSIC_REPEAT_MODE_ONE", 2, 3);
        MUSIC_REPEAT_MODE_ALL = new com.navdy.service.library.events.audio.MusicRepeatMode("MUSIC_REPEAT_MODE_ALL", 3, 4);
        com.navdy.service.library.events.audio.MusicRepeatMode[] a = new com.navdy.service.library.events.audio.MusicRepeatMode[4];
        a[0] = MUSIC_REPEAT_MODE_UNKNOWN;
        a[1] = MUSIC_REPEAT_MODE_OFF;
        a[2] = MUSIC_REPEAT_MODE_ONE;
        a[3] = MUSIC_REPEAT_MODE_ALL;
        $VALUES = a;
    }
    
    private MusicRepeatMode(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.audio.MusicRepeatMode valueOf(String s) {
        return (com.navdy.service.library.events.audio.MusicRepeatMode)Enum.valueOf(com.navdy.service.library.events.audio.MusicRepeatMode.class, s);
    }
    
    public static com.navdy.service.library.events.audio.MusicRepeatMode[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
