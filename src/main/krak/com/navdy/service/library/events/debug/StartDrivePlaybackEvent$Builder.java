package com.navdy.service.library.events.debug;

final public class StartDrivePlaybackEvent$Builder extends com.squareup.wire.Message.Builder {
    public String label;
    public Boolean playSecondaryLocation;
    
    public StartDrivePlaybackEvent$Builder() {
    }
    
    public StartDrivePlaybackEvent$Builder(com.navdy.service.library.events.debug.StartDrivePlaybackEvent a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.label = a.label;
            this.playSecondaryLocation = a.playSecondaryLocation;
        }
    }
    
    public com.navdy.service.library.events.debug.StartDrivePlaybackEvent build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.debug.StartDrivePlaybackEvent(this, (com.navdy.service.library.events.debug.StartDrivePlaybackEvent$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.debug.StartDrivePlaybackEvent$Builder label(String s) {
        this.label = s;
        return this;
    }
    
    public com.navdy.service.library.events.debug.StartDrivePlaybackEvent$Builder playSecondaryLocation(Boolean a) {
        this.playSecondaryLocation = a;
        return this;
    }
}
