package com.navdy.service.library.events.audio;

final public class NowPlayingUpdateRequest extends com.squareup.wire.Message {
    final public static Boolean DEFAULT_START;
    final private static long serialVersionUID = 0L;
    final public Boolean start;
    
    static {
        DEFAULT_START = Boolean.valueOf(false);
    }
    
    private NowPlayingUpdateRequest(com.navdy.service.library.events.audio.NowPlayingUpdateRequest$Builder a) {
        this(a.start);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NowPlayingUpdateRequest(com.navdy.service.library.events.audio.NowPlayingUpdateRequest$Builder a, com.navdy.service.library.events.audio.NowPlayingUpdateRequest$1 a0) {
        this(a);
    }
    
    public NowPlayingUpdateRequest(Boolean a) {
        this.start = a;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.audio.NowPlayingUpdateRequest && this.equals(this.start, ((com.navdy.service.library.events.audio.NowPlayingUpdateRequest)a).start);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.start == null) ? 0 : this.start.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
