package com.navdy.service.library.events.debug;

final public class DriveRecordingsResponse$Builder extends com.squareup.wire.Message.Builder {
    public java.util.List recordings;
    
    public DriveRecordingsResponse$Builder() {
    }
    
    public DriveRecordingsResponse$Builder(com.navdy.service.library.events.debug.DriveRecordingsResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.recordings = com.navdy.service.library.events.debug.DriveRecordingsResponse.access$000(a.recordings);
        }
    }
    
    public com.navdy.service.library.events.debug.DriveRecordingsResponse build() {
        return new com.navdy.service.library.events.debug.DriveRecordingsResponse(this, (com.navdy.service.library.events.debug.DriveRecordingsResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.debug.DriveRecordingsResponse$Builder recordings(java.util.List a) {
        this.recordings = com.navdy.service.library.events.debug.DriveRecordingsResponse$Builder.checkForNulls(a);
        return this;
    }
}
