package com.navdy.service.library.events.audio;

final public class VoiceSearchResponse extends com.squareup.wire.Message {
    final public static Integer DEFAULT_CONFIDENCELEVEL;
    final public static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError DEFAULT_ERROR;
    final public static Boolean DEFAULT_LISTENINGOVERBLUETOOTH;
    final public static String DEFAULT_LOCALE = "";
    final public static String DEFAULT_PREFIX = "";
    final public static String DEFAULT_RECOGNIZEDWORDS = "";
    final public static java.util.List DEFAULT_RESULTS;
    final public static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState DEFAULT_STATE;
    final public static Integer DEFAULT_VOLUMELEVEL;
    final private static long serialVersionUID = 0L;
    final public Integer confidenceLevel;
    final public com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError error;
    final public Boolean listeningOverBluetooth;
    final public String locale;
    final public String prefix;
    final public String recognizedWords;
    final public java.util.List results;
    final public com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState state;
    final public Integer volumeLevel;
    
    static {
        DEFAULT_STATE = com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState.VOICE_SEARCH_ERROR;
        DEFAULT_VOLUMELEVEL = Integer.valueOf(0);
        DEFAULT_LISTENINGOVERBLUETOOTH = Boolean.valueOf(false);
        DEFAULT_ERROR = com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.NOT_AVAILABLE;
        DEFAULT_CONFIDENCELEVEL = Integer.valueOf(0);
        DEFAULT_RESULTS = java.util.Collections.emptyList();
    }
    
    private VoiceSearchResponse(com.navdy.service.library.events.audio.VoiceSearchResponse$Builder a) {
        this(a.state, a.recognizedWords, a.volumeLevel, a.listeningOverBluetooth, a.error, a.confidenceLevel, a.results, a.prefix, a.locale);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    VoiceSearchResponse(com.navdy.service.library.events.audio.VoiceSearchResponse$Builder a, com.navdy.service.library.events.audio.VoiceSearchResponse$1 a0) {
        this(a);
    }
    
    public VoiceSearchResponse(com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState a, String s, Integer a0, Boolean a1, com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError a2, Integer a3, java.util.List a4, String s0, String s1) {
        this.state = a;
        this.recognizedWords = s;
        this.volumeLevel = a0;
        this.listeningOverBluetooth = a1;
        this.error = a2;
        this.confidenceLevel = a3;
        this.results = com.navdy.service.library.events.audio.VoiceSearchResponse.immutableCopyOf(a4);
        this.prefix = s0;
        this.locale = s1;
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.audio.VoiceSearchResponse.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.audio.VoiceSearchResponse) {
                com.navdy.service.library.events.audio.VoiceSearchResponse a0 = (com.navdy.service.library.events.audio.VoiceSearchResponse)a;
                boolean b0 = this.equals(this.state, a0.state);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.recognizedWords, a0.recognizedWords)) {
                        break label1;
                    }
                    if (!this.equals(this.volumeLevel, a0.volumeLevel)) {
                        break label1;
                    }
                    if (!this.equals(this.listeningOverBluetooth, a0.listeningOverBluetooth)) {
                        break label1;
                    }
                    if (!this.equals(this.error, a0.error)) {
                        break label1;
                    }
                    if (!this.equals(this.confidenceLevel, a0.confidenceLevel)) {
                        break label1;
                    }
                    if (!this.equals(this.results, a0.results)) {
                        break label1;
                    }
                    if (!this.equals(this.prefix, a0.prefix)) {
                        break label1;
                    }
                    if (this.equals(this.locale, a0.locale)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((((((this.state == null) ? 0 : this.state.hashCode()) * 37 + ((this.recognizedWords == null) ? 0 : this.recognizedWords.hashCode())) * 37 + ((this.volumeLevel == null) ? 0 : this.volumeLevel.hashCode())) * 37 + ((this.listeningOverBluetooth == null) ? 0 : this.listeningOverBluetooth.hashCode())) * 37 + ((this.error == null) ? 0 : this.error.hashCode())) * 37 + ((this.confidenceLevel == null) ? 0 : this.confidenceLevel.hashCode())) * 37 + ((this.results == null) ? 1 : this.results.hashCode())) * 37 + ((this.prefix == null) ? 0 : this.prefix.hashCode())) * 37 + ((this.locale == null) ? 0 : this.locale.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
