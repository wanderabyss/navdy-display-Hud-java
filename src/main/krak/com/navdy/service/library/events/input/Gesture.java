package com.navdy.service.library.events.input;

final public class Gesture extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.input.Gesture[] $VALUES;
    final public static com.navdy.service.library.events.input.Gesture GESTURE_FINGER;
    final public static com.navdy.service.library.events.input.Gesture GESTURE_FINGER_DOWN;
    final public static com.navdy.service.library.events.input.Gesture GESTURE_FINGER_TO_PINCH;
    final public static com.navdy.service.library.events.input.Gesture GESTURE_FINGER_UP;
    final public static com.navdy.service.library.events.input.Gesture GESTURE_FIST;
    final public static com.navdy.service.library.events.input.Gesture GESTURE_FIST_TO_HAND;
    final public static com.navdy.service.library.events.input.Gesture GESTURE_HAND_DOWN;
    final public static com.navdy.service.library.events.input.Gesture GESTURE_HAND_TO_FIST;
    final public static com.navdy.service.library.events.input.Gesture GESTURE_HAND_UP;
    final public static com.navdy.service.library.events.input.Gesture GESTURE_PALM;
    final public static com.navdy.service.library.events.input.Gesture GESTURE_PINCH;
    final public static com.navdy.service.library.events.input.Gesture GESTURE_PINCH_TO_FINGER;
    final public static com.navdy.service.library.events.input.Gesture GESTURE_SWIPE_DOWN;
    final public static com.navdy.service.library.events.input.Gesture GESTURE_SWIPE_LEFT;
    final public static com.navdy.service.library.events.input.Gesture GESTURE_SWIPE_RIGHT;
    final public static com.navdy.service.library.events.input.Gesture GESTURE_SWIPE_UP;
    final private int value;
    
    static {
        GESTURE_SWIPE_LEFT = new com.navdy.service.library.events.input.Gesture("GESTURE_SWIPE_LEFT", 0, 1);
        GESTURE_SWIPE_RIGHT = new com.navdy.service.library.events.input.Gesture("GESTURE_SWIPE_RIGHT", 1, 2);
        GESTURE_SWIPE_UP = new com.navdy.service.library.events.input.Gesture("GESTURE_SWIPE_UP", 2, 3);
        GESTURE_SWIPE_DOWN = new com.navdy.service.library.events.input.Gesture("GESTURE_SWIPE_DOWN", 3, 4);
        GESTURE_FINGER_UP = new com.navdy.service.library.events.input.Gesture("GESTURE_FINGER_UP", 4, 5);
        GESTURE_FINGER = new com.navdy.service.library.events.input.Gesture("GESTURE_FINGER", 5, 6);
        GESTURE_FINGER_DOWN = new com.navdy.service.library.events.input.Gesture("GESTURE_FINGER_DOWN", 6, 7);
        GESTURE_FINGER_TO_PINCH = new com.navdy.service.library.events.input.Gesture("GESTURE_FINGER_TO_PINCH", 7, 8);
        GESTURE_PINCH_TO_FINGER = new com.navdy.service.library.events.input.Gesture("GESTURE_PINCH_TO_FINGER", 8, 9);
        GESTURE_PINCH = new com.navdy.service.library.events.input.Gesture("GESTURE_PINCH", 9, 10);
        GESTURE_HAND_UP = new com.navdy.service.library.events.input.Gesture("GESTURE_HAND_UP", 10, 11);
        GESTURE_PALM = new com.navdy.service.library.events.input.Gesture("GESTURE_PALM", 11, 12);
        GESTURE_HAND_TO_FIST = new com.navdy.service.library.events.input.Gesture("GESTURE_HAND_TO_FIST", 12, 13);
        GESTURE_FIST = new com.navdy.service.library.events.input.Gesture("GESTURE_FIST", 13, 14);
        GESTURE_FIST_TO_HAND = new com.navdy.service.library.events.input.Gesture("GESTURE_FIST_TO_HAND", 14, 15);
        GESTURE_HAND_DOWN = new com.navdy.service.library.events.input.Gesture("GESTURE_HAND_DOWN", 15, 16);
        com.navdy.service.library.events.input.Gesture[] a = new com.navdy.service.library.events.input.Gesture[16];
        a[0] = GESTURE_SWIPE_LEFT;
        a[1] = GESTURE_SWIPE_RIGHT;
        a[2] = GESTURE_SWIPE_UP;
        a[3] = GESTURE_SWIPE_DOWN;
        a[4] = GESTURE_FINGER_UP;
        a[5] = GESTURE_FINGER;
        a[6] = GESTURE_FINGER_DOWN;
        a[7] = GESTURE_FINGER_TO_PINCH;
        a[8] = GESTURE_PINCH_TO_FINGER;
        a[9] = GESTURE_PINCH;
        a[10] = GESTURE_HAND_UP;
        a[11] = GESTURE_PALM;
        a[12] = GESTURE_HAND_TO_FIST;
        a[13] = GESTURE_FIST;
        a[14] = GESTURE_FIST_TO_HAND;
        a[15] = GESTURE_HAND_DOWN;
        $VALUES = a;
    }
    
    private Gesture(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.input.Gesture valueOf(String s) {
        return (com.navdy.service.library.events.input.Gesture)Enum.valueOf(com.navdy.service.library.events.input.Gesture.class, s);
    }
    
    public static com.navdy.service.library.events.input.Gesture[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
