package com.navdy.service.library.events.audio;

final public class MusicEvent extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.audio.MusicEvent$Action DEFAULT_ACTION;
    final public static String DEFAULT_COLLECTIONID = "";
    final public static com.navdy.service.library.events.audio.MusicCollectionSource DEFAULT_COLLECTIONSOURCE;
    final public static com.navdy.service.library.events.audio.MusicCollectionType DEFAULT_COLLECTIONTYPE;
    final public static com.navdy.service.library.events.audio.MusicDataSource DEFAULT_DATASOURCE;
    final public static Integer DEFAULT_INDEX;
    final public static com.navdy.service.library.events.audio.MusicRepeatMode DEFAULT_REPEATMODE;
    final public static com.navdy.service.library.events.audio.MusicShuffleMode DEFAULT_SHUFFLEMODE;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.audio.MusicEvent$Action action;
    final public String collectionId;
    final public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    final public com.navdy.service.library.events.audio.MusicCollectionType collectionType;
    final public com.navdy.service.library.events.audio.MusicDataSource dataSource;
    final public Integer index;
    final public com.navdy.service.library.events.audio.MusicRepeatMode repeatMode;
    final public com.navdy.service.library.events.audio.MusicShuffleMode shuffleMode;
    
    static {
        DEFAULT_ACTION = com.navdy.service.library.events.audio.MusicEvent$Action.MUSIC_ACTION_PLAY;
        DEFAULT_DATASOURCE = com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_NONE;
        DEFAULT_COLLECTIONSOURCE = com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
        DEFAULT_COLLECTIONTYPE = com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
        DEFAULT_INDEX = Integer.valueOf(0);
        DEFAULT_SHUFFLEMODE = com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_UNKNOWN;
        DEFAULT_REPEATMODE = com.navdy.service.library.events.audio.MusicRepeatMode.MUSIC_REPEAT_MODE_UNKNOWN;
    }
    
    public MusicEvent(com.navdy.service.library.events.audio.MusicEvent$Action a, com.navdy.service.library.events.audio.MusicDataSource a0, com.navdy.service.library.events.audio.MusicCollectionSource a1, com.navdy.service.library.events.audio.MusicCollectionType a2, String s, Integer a3, com.navdy.service.library.events.audio.MusicShuffleMode a4, com.navdy.service.library.events.audio.MusicRepeatMode a5) {
        this.action = a;
        this.dataSource = a0;
        this.collectionSource = a1;
        this.collectionType = a2;
        this.collectionId = s;
        this.index = a3;
        this.shuffleMode = a4;
        this.repeatMode = a5;
    }
    
    private MusicEvent(com.navdy.service.library.events.audio.MusicEvent$Builder a) {
        this(a.action, a.dataSource, a.collectionSource, a.collectionType, a.collectionId, a.index, a.shuffleMode, a.repeatMode);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    MusicEvent(com.navdy.service.library.events.audio.MusicEvent$Builder a, com.navdy.service.library.events.audio.MusicEvent$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.audio.MusicEvent) {
                com.navdy.service.library.events.audio.MusicEvent a0 = (com.navdy.service.library.events.audio.MusicEvent)a;
                boolean b0 = this.equals(this.action, a0.action);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.dataSource, a0.dataSource)) {
                        break label1;
                    }
                    if (!this.equals(this.collectionSource, a0.collectionSource)) {
                        break label1;
                    }
                    if (!this.equals(this.collectionType, a0.collectionType)) {
                        break label1;
                    }
                    if (!this.equals(this.collectionId, a0.collectionId)) {
                        break label1;
                    }
                    if (!this.equals(this.index, a0.index)) {
                        break label1;
                    }
                    if (!this.equals(this.shuffleMode, a0.shuffleMode)) {
                        break label1;
                    }
                    if (this.equals(this.repeatMode, a0.repeatMode)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((((((this.action == null) ? 0 : this.action.hashCode()) * 37 + ((this.dataSource == null) ? 0 : this.dataSource.hashCode())) * 37 + ((this.collectionSource == null) ? 0 : this.collectionSource.hashCode())) * 37 + ((this.collectionType == null) ? 0 : this.collectionType.hashCode())) * 37 + ((this.collectionId == null) ? 0 : this.collectionId.hashCode())) * 37 + ((this.index == null) ? 0 : this.index.hashCode())) * 37 + ((this.shuffleMode == null) ? 0 : this.shuffleMode.hashCode())) * 37 + ((this.repeatMode == null) ? 0 : this.repeatMode.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
