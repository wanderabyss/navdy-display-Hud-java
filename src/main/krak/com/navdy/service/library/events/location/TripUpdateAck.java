package com.navdy.service.library.events.location;

final public class TripUpdateAck extends com.squareup.wire.Message {
    final public static Integer DEFAULT_SEQUENCE_NUMBER;
    final public static Long DEFAULT_TRIP_NUMBER;
    final private static long serialVersionUID = 0L;
    final public Integer sequence_number;
    final public Long trip_number;
    
    static {
        DEFAULT_TRIP_NUMBER = Long.valueOf(0L);
        DEFAULT_SEQUENCE_NUMBER = Integer.valueOf(0);
    }
    
    private TripUpdateAck(com.navdy.service.library.events.location.TripUpdateAck$Builder a) {
        this(a.trip_number, a.sequence_number);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    TripUpdateAck(com.navdy.service.library.events.location.TripUpdateAck$Builder a, com.navdy.service.library.events.location.TripUpdateAck$1 a0) {
        this(a);
    }
    
    public TripUpdateAck(Long a, Integer a0) {
        this.trip_number = a;
        this.sequence_number = a0;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.location.TripUpdateAck) {
                com.navdy.service.library.events.location.TripUpdateAck a0 = (com.navdy.service.library.events.location.TripUpdateAck)a;
                boolean b0 = this.equals(this.trip_number, a0.trip_number);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.sequence_number, a0.sequence_number)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.trip_number == null) ? 0 : this.trip_number.hashCode()) * 37 + ((this.sequence_number == null) ? 0 : this.sequence_number.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
