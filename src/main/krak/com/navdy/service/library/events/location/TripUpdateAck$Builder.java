package com.navdy.service.library.events.location;

final public class TripUpdateAck$Builder extends com.squareup.wire.Message.Builder {
    public Integer sequence_number;
    public Long trip_number;
    
    public TripUpdateAck$Builder() {
    }
    
    public TripUpdateAck$Builder(com.navdy.service.library.events.location.TripUpdateAck a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.trip_number = a.trip_number;
            this.sequence_number = a.sequence_number;
        }
    }
    
    public com.navdy.service.library.events.location.TripUpdateAck build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.location.TripUpdateAck(this, (com.navdy.service.library.events.location.TripUpdateAck$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.location.TripUpdateAck$Builder sequence_number(Integer a) {
        this.sequence_number = a;
        return this;
    }
    
    public com.navdy.service.library.events.location.TripUpdateAck$Builder trip_number(Long a) {
        this.trip_number = a;
        return this;
    }
}
