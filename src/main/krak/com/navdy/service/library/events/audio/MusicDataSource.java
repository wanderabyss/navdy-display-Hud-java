package com.navdy.service.library.events.audio;

final public class MusicDataSource extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.audio.MusicDataSource[] $VALUES;
    final public static com.navdy.service.library.events.audio.MusicDataSource MUSIC_SOURCE_ANDROID_INTENT;
    final public static com.navdy.service.library.events.audio.MusicDataSource MUSIC_SOURCE_MEDIA_CONTROLLER;
    final public static com.navdy.service.library.events.audio.MusicDataSource MUSIC_SOURCE_NONE;
    final public static com.navdy.service.library.events.audio.MusicDataSource MUSIC_SOURCE_OS_NOTIFICATION;
    final public static com.navdy.service.library.events.audio.MusicDataSource MUSIC_SOURCE_PANDORA_API;
    final public static com.navdy.service.library.events.audio.MusicDataSource MUSIC_SOURCE_SPOTIFY_INTENT;
    final private int value;
    
    static {
        MUSIC_SOURCE_NONE = new com.navdy.service.library.events.audio.MusicDataSource("MUSIC_SOURCE_NONE", 0, 1);
        MUSIC_SOURCE_ANDROID_INTENT = new com.navdy.service.library.events.audio.MusicDataSource("MUSIC_SOURCE_ANDROID_INTENT", 1, 2);
        MUSIC_SOURCE_MEDIA_CONTROLLER = new com.navdy.service.library.events.audio.MusicDataSource("MUSIC_SOURCE_MEDIA_CONTROLLER", 2, 3);
        MUSIC_SOURCE_OS_NOTIFICATION = new com.navdy.service.library.events.audio.MusicDataSource("MUSIC_SOURCE_OS_NOTIFICATION", 3, 4);
        MUSIC_SOURCE_SPOTIFY_INTENT = new com.navdy.service.library.events.audio.MusicDataSource("MUSIC_SOURCE_SPOTIFY_INTENT", 4, 5);
        MUSIC_SOURCE_PANDORA_API = new com.navdy.service.library.events.audio.MusicDataSource("MUSIC_SOURCE_PANDORA_API", 5, 6);
        com.navdy.service.library.events.audio.MusicDataSource[] a = new com.navdy.service.library.events.audio.MusicDataSource[6];
        a[0] = MUSIC_SOURCE_NONE;
        a[1] = MUSIC_SOURCE_ANDROID_INTENT;
        a[2] = MUSIC_SOURCE_MEDIA_CONTROLLER;
        a[3] = MUSIC_SOURCE_OS_NOTIFICATION;
        a[4] = MUSIC_SOURCE_SPOTIFY_INTENT;
        a[5] = MUSIC_SOURCE_PANDORA_API;
        $VALUES = a;
    }
    
    private MusicDataSource(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.audio.MusicDataSource valueOf(String s) {
        return (com.navdy.service.library.events.audio.MusicDataSource)Enum.valueOf(com.navdy.service.library.events.audio.MusicDataSource.class, s);
    }
    
    public static com.navdy.service.library.events.audio.MusicDataSource[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
