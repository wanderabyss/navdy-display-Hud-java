package com.navdy.service.library.events.glances;

final public class FuelConstants extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.glances.FuelConstants[] $VALUES;
    final public static com.navdy.service.library.events.glances.FuelConstants FUEL_LEVEL;
    final public static com.navdy.service.library.events.glances.FuelConstants GAS_STATION_ADDRESS;
    final public static com.navdy.service.library.events.glances.FuelConstants GAS_STATION_DISTANCE;
    final public static com.navdy.service.library.events.glances.FuelConstants GAS_STATION_NAME;
    final public static com.navdy.service.library.events.glances.FuelConstants NO_ROUTE;
    final private int value;
    
    static {
        FUEL_LEVEL = new com.navdy.service.library.events.glances.FuelConstants("FUEL_LEVEL", 0, 0);
        NO_ROUTE = new com.navdy.service.library.events.glances.FuelConstants("NO_ROUTE", 1, 1);
        GAS_STATION_NAME = new com.navdy.service.library.events.glances.FuelConstants("GAS_STATION_NAME", 2, 2);
        GAS_STATION_ADDRESS = new com.navdy.service.library.events.glances.FuelConstants("GAS_STATION_ADDRESS", 3, 3);
        GAS_STATION_DISTANCE = new com.navdy.service.library.events.glances.FuelConstants("GAS_STATION_DISTANCE", 4, 4);
        com.navdy.service.library.events.glances.FuelConstants[] a = new com.navdy.service.library.events.glances.FuelConstants[5];
        a[0] = FUEL_LEVEL;
        a[1] = NO_ROUTE;
        a[2] = GAS_STATION_NAME;
        a[3] = GAS_STATION_ADDRESS;
        a[4] = GAS_STATION_DISTANCE;
        $VALUES = a;
    }
    
    private FuelConstants(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.glances.FuelConstants valueOf(String s) {
        return (com.navdy.service.library.events.glances.FuelConstants)Enum.valueOf(com.navdy.service.library.events.glances.FuelConstants.class, s);
    }
    
    public static com.navdy.service.library.events.glances.FuelConstants[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
