package com.navdy.service.library.events.notification;

final public class NotificationsStatusUpdate extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.notification.NotificationsError DEFAULT_ERRORDETAILS;
    final public static com.navdy.service.library.events.notification.ServiceType DEFAULT_SERVICE;
    final public static com.navdy.service.library.events.notification.NotificationsState DEFAULT_STATE;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.notification.NotificationsError errorDetails;
    final public com.navdy.service.library.events.notification.ServiceType service;
    final public com.navdy.service.library.events.notification.NotificationsState state;
    
    static {
        DEFAULT_STATE = com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_ENABLED;
        DEFAULT_SERVICE = com.navdy.service.library.events.notification.ServiceType.SERVICE_ANCS;
        DEFAULT_ERRORDETAILS = com.navdy.service.library.events.notification.NotificationsError.NOTIFICATIONS_ERROR_UNKNOWN;
    }
    
    public NotificationsStatusUpdate(com.navdy.service.library.events.notification.NotificationsState a, com.navdy.service.library.events.notification.ServiceType a0, com.navdy.service.library.events.notification.NotificationsError a1) {
        this.state = a;
        this.service = a0;
        this.errorDetails = a1;
    }
    
    private NotificationsStatusUpdate(com.navdy.service.library.events.notification.NotificationsStatusUpdate$Builder a) {
        this(a.state, a.service, a.errorDetails);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NotificationsStatusUpdate(com.navdy.service.library.events.notification.NotificationsStatusUpdate$Builder a, com.navdy.service.library.events.notification.NotificationsStatusUpdate$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.notification.NotificationsStatusUpdate) {
                com.navdy.service.library.events.notification.NotificationsStatusUpdate a0 = (com.navdy.service.library.events.notification.NotificationsStatusUpdate)a;
                boolean b0 = this.equals(this.state, a0.state);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.service, a0.service)) {
                        break label1;
                    }
                    if (this.equals(this.errorDetails, a0.errorDetails)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.state == null) ? 0 : this.state.hashCode()) * 37 + ((this.service == null) ? 0 : this.service.hashCode())) * 37 + ((this.errorDetails == null) ? 0 : this.errorDetails.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
