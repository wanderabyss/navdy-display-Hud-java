package com.navdy.service.library.events;

final public class Notification extends com.squareup.wire.Message {
    final public static String DEFAULT_MESSAGE_DATA = "";
    final public static String DEFAULT_ORIGIN = "";
    final public static String DEFAULT_SENDER = "";
    final public static Integer DEFAULT_SMS_CLASS;
    final public static String DEFAULT_TARGET = "";
    final public static String DEFAULT_TYPE = "";
    final private static long serialVersionUID = 0L;
    final public String message_data;
    final public String origin;
    final public String sender;
    final public Integer sms_class;
    final public String target;
    final public String type;
    
    static {
        DEFAULT_SMS_CLASS = Integer.valueOf(0);
    }
    
    private Notification(com.navdy.service.library.events.Notification$Builder a) {
        this(a.sms_class, a.origin, a.sender, a.target, a.message_data, a.type);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    Notification(com.navdy.service.library.events.Notification$Builder a, com.navdy.service.library.events.Notification$1 a0) {
        this(a);
    }
    
    public Notification(Integer a, String s, String s0, String s1, String s2, String s3) {
        this.sms_class = a;
        this.origin = s;
        this.sender = s0;
        this.target = s1;
        this.message_data = s2;
        this.type = s3;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.Notification) {
                com.navdy.service.library.events.Notification a0 = (com.navdy.service.library.events.Notification)a;
                boolean b0 = this.equals(this.sms_class, a0.sms_class);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.origin, a0.origin)) {
                        break label1;
                    }
                    if (!this.equals(this.sender, a0.sender)) {
                        break label1;
                    }
                    if (!this.equals(this.target, a0.target)) {
                        break label1;
                    }
                    if (!this.equals(this.message_data, a0.message_data)) {
                        break label1;
                    }
                    if (this.equals(this.type, a0.type)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((((this.sms_class == null) ? 0 : this.sms_class.hashCode()) * 37 + ((this.origin == null) ? 0 : this.origin.hashCode())) * 37 + ((this.sender == null) ? 0 : this.sender.hashCode())) * 37 + ((this.target == null) ? 0 : this.target.hashCode())) * 37 + ((this.message_data == null) ? 0 : this.message_data.hashCode())) * 37 + ((this.type == null) ? 0 : this.type.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
