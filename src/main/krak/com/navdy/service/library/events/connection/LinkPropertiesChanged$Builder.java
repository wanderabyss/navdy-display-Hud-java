package com.navdy.service.library.events.connection;

final public class LinkPropertiesChanged$Builder extends com.squareup.wire.Message.Builder {
    public Integer bandwidthLevel;
    
    public LinkPropertiesChanged$Builder() {
    }
    
    public LinkPropertiesChanged$Builder(com.navdy.service.library.events.connection.LinkPropertiesChanged a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.bandwidthLevel = a.bandwidthLevel;
        }
    }
    
    public com.navdy.service.library.events.connection.LinkPropertiesChanged$Builder bandwidthLevel(Integer a) {
        this.bandwidthLevel = a;
        return this;
    }
    
    public com.navdy.service.library.events.connection.LinkPropertiesChanged build() {
        return new com.navdy.service.library.events.connection.LinkPropertiesChanged(this, (com.navdy.service.library.events.connection.LinkPropertiesChanged$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
}
