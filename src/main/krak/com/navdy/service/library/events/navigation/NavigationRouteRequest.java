package com.navdy.service.library.events.navigation;

final public class NavigationRouteRequest extends com.squareup.wire.Message {
    final public static Boolean DEFAULT_AUTONAVIGATE;
    final public static Boolean DEFAULT_CANCELCURRENT;
    final public static com.navdy.service.library.events.destination.Destination$FavoriteType DEFAULT_DESTINATIONTYPE;
    final public static String DEFAULT_DESTINATION_IDENTIFIER = "";
    final public static Boolean DEFAULT_GEOCODESTREETADDRESS;
    final public static String DEFAULT_LABEL = "";
    final public static Boolean DEFAULT_ORIGINDISPLAY;
    final public static String DEFAULT_REQUESTID = "";
    final public static java.util.List DEFAULT_ROUTEATTRIBUTES;
    final public static String DEFAULT_STREETADDRESS = "";
    final public static java.util.List DEFAULT_WAYPOINTS;
    final private static long serialVersionUID = 0L;
    final public Boolean autoNavigate;
    final public Boolean cancelCurrent;
    final public com.navdy.service.library.events.location.Coordinate destination;
    final public com.navdy.service.library.events.location.Coordinate destinationDisplay;
    final public com.navdy.service.library.events.destination.Destination$FavoriteType destinationType;
    final public String destination_identifier;
    final public Boolean geoCodeStreetAddress;
    final public String label;
    final public Boolean originDisplay;
    final public com.navdy.service.library.events.destination.Destination requestDestination;
    final public String requestId;
    final public java.util.List routeAttributes;
    final public String streetAddress;
    final public java.util.List waypoints;
    
    static {
        DEFAULT_WAYPOINTS = java.util.Collections.emptyList();
        DEFAULT_GEOCODESTREETADDRESS = Boolean.valueOf(false);
        DEFAULT_AUTONAVIGATE = Boolean.valueOf(false);
        DEFAULT_DESTINATIONTYPE = com.navdy.service.library.events.destination.Destination$FavoriteType.FAVORITE_NONE;
        DEFAULT_ORIGINDISPLAY = Boolean.valueOf(false);
        DEFAULT_CANCELCURRENT = Boolean.valueOf(false);
        DEFAULT_ROUTEATTRIBUTES = java.util.Collections.emptyList();
    }
    
    public NavigationRouteRequest(com.navdy.service.library.events.location.Coordinate a, String s, java.util.List a0, String s0, Boolean a1, Boolean a2, String s1, com.navdy.service.library.events.destination.Destination$FavoriteType a3, Boolean a4, Boolean a5, String s2, com.navdy.service.library.events.location.Coordinate a6, java.util.List a7, com.navdy.service.library.events.destination.Destination a8) {
        this.destination = a;
        this.label = s;
        this.waypoints = com.navdy.service.library.events.navigation.NavigationRouteRequest.immutableCopyOf(a0);
        this.streetAddress = s0;
        this.geoCodeStreetAddress = a1;
        this.autoNavigate = a2;
        this.destination_identifier = s1;
        this.destinationType = a3;
        this.originDisplay = a4;
        this.cancelCurrent = a5;
        this.requestId = s2;
        this.destinationDisplay = a6;
        this.routeAttributes = com.navdy.service.library.events.navigation.NavigationRouteRequest.immutableCopyOf(a7);
        this.requestDestination = a8;
    }
    
    private NavigationRouteRequest(com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder a) {
        this(a.destination, a.label, a.waypoints, a.streetAddress, a.geoCodeStreetAddress, a.autoNavigate, a.destination_identifier, a.destinationType, a.originDisplay, a.cancelCurrent, a.requestId, a.destinationDisplay, a.routeAttributes, a.requestDestination);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NavigationRouteRequest(com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder a, com.navdy.service.library.events.navigation.NavigationRouteRequest$1 a0) {
        this(a);
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.navigation.NavigationRouteRequest.copyOf(a);
    }
    
    static java.util.List access$100(java.util.List a) {
        return com.navdy.service.library.events.navigation.NavigationRouteRequest.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.navigation.NavigationRouteRequest) {
                com.navdy.service.library.events.navigation.NavigationRouteRequest a0 = (com.navdy.service.library.events.navigation.NavigationRouteRequest)a;
                boolean b0 = this.equals(this.destination, a0.destination);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.label, a0.label)) {
                        break label1;
                    }
                    if (!this.equals(this.waypoints, a0.waypoints)) {
                        break label1;
                    }
                    if (!this.equals(this.streetAddress, a0.streetAddress)) {
                        break label1;
                    }
                    if (!this.equals(this.geoCodeStreetAddress, a0.geoCodeStreetAddress)) {
                        break label1;
                    }
                    if (!this.equals(this.autoNavigate, a0.autoNavigate)) {
                        break label1;
                    }
                    if (!this.equals(this.destination_identifier, a0.destination_identifier)) {
                        break label1;
                    }
                    if (!this.equals(this.destinationType, a0.destinationType)) {
                        break label1;
                    }
                    if (!this.equals(this.originDisplay, a0.originDisplay)) {
                        break label1;
                    }
                    if (!this.equals(this.cancelCurrent, a0.cancelCurrent)) {
                        break label1;
                    }
                    if (!this.equals(this.requestId, a0.requestId)) {
                        break label1;
                    }
                    if (!this.equals(this.destinationDisplay, a0.destinationDisplay)) {
                        break label1;
                    }
                    if (!this.equals(this.routeAttributes, a0.routeAttributes)) {
                        break label1;
                    }
                    if (this.equals(this.requestDestination, a0.requestDestination)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((((((((((((this.destination == null) ? 0 : this.destination.hashCode()) * 37 + ((this.label == null) ? 0 : this.label.hashCode())) * 37 + ((this.waypoints == null) ? 1 : this.waypoints.hashCode())) * 37 + ((this.streetAddress == null) ? 0 : this.streetAddress.hashCode())) * 37 + ((this.geoCodeStreetAddress == null) ? 0 : this.geoCodeStreetAddress.hashCode())) * 37 + ((this.autoNavigate == null) ? 0 : this.autoNavigate.hashCode())) * 37 + ((this.destination_identifier == null) ? 0 : this.destination_identifier.hashCode())) * 37 + ((this.destinationType == null) ? 0 : this.destinationType.hashCode())) * 37 + ((this.originDisplay == null) ? 0 : this.originDisplay.hashCode())) * 37 + ((this.cancelCurrent == null) ? 0 : this.cancelCurrent.hashCode())) * 37 + ((this.requestId == null) ? 0 : this.requestId.hashCode())) * 37 + ((this.destinationDisplay == null) ? 0 : this.destinationDisplay.hashCode())) * 37 + ((this.routeAttributes == null) ? 1 : this.routeAttributes.hashCode())) * 37 + ((this.requestDestination == null) ? 0 : this.requestDestination.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
