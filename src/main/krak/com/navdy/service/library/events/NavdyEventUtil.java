package com.navdy.service.library.events;

public class NavdyEventUtil {
    final private static int EXTENSION_BASE = 101;
    private static java.util.HashMap classToExtensionMap;
    private static int minTag;
    final private static com.navdy.service.library.log.Logger sLogger;
    private static com.squareup.wire.Extension[] tagToExtensionMap;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.events.NavdyEventUtil.class);
        classToExtensionMap = new java.util.HashMap();
        tagToExtensionMap = com.navdy.service.library.events.NavdyEventUtil.cacheExtensions();
    }
    
    public NavdyEventUtil() {
    }
    
    public static com.squareup.wire.Message applyDefaults(com.squareup.wire.Message a) {
        return com.navdy.service.library.events.NavdyEventUtil.merge(a, com.navdy.service.library.events.NavdyEventUtil.getDefault((a).getClass()));
    }
    
    private static com.squareup.wire.Extension[] cacheExtensions() {
        java.util.ArrayList a = new java.util.ArrayList();
        minTag = 2147483647;
        java.lang.reflect.Field[] a0 = com.navdy.service.library.events.Ext_NavdyEvent.class.getDeclaredFields();
        int i = a0.length;
        int i0 = -1;
        int i1 = 0;
        while(i1 < i) {
            java.lang.reflect.Field a1 = a0[i1];
            boolean b = (a1.getType()).equals(com.squareup.wire.Extension.class);
            label0: {
                if (!b) {
                    break label0;
                }
                try {
                    com.squareup.wire.Extension a2 = (com.squareup.wire.Extension)a1.get(null);
                    classToExtensionMap.put(a2.getMessageType(), a2);
                    int i2 = a2.getTag();
                    if (i2 < minTag) {
                        minTag = i2;
                    }
                    if (i2 <= i0) {
                        i2 = i0;
                    }
                    i0 = i2;
                    ((java.util.List)a).add(a2);
                    i0 = i2;
                    break label0;
                } catch(IllegalAccessException ignoredException) {
                }
                sLogger.d(new StringBuilder().append("Skipping field ").append(a1.getName()).toString());
            }
            i1 = i1 + 1;
        }
        com.squareup.wire.Extension[] a3 = new com.squareup.wire.Extension[i0 - minTag + 1];
        Object a4 = ((java.util.List)a).iterator();
        while(((java.util.Iterator)a4).hasNext()) {
            com.squareup.wire.Extension a5 = (com.squareup.wire.Extension)((java.util.Iterator)a4).next();
            a3[a5.getTag() - minTag] = a5;
        }
        return a3;
    }
    
    public static com.navdy.service.library.events.NavdyEvent eventFromMessage(com.squareup.wire.Message a) {
        com.squareup.wire.Extension a0 = com.navdy.service.library.events.NavdyEventUtil.findExtension(a);
        if (a0 == null) {
            sLogger.e(new StringBuilder().append("Not a valid NavdyEvent protobuf message: ").append(a).toString());
            throw new IllegalArgumentException(new StringBuilder().append("Not a valid NavdyEvent protobuf message: ").append(a).toString());
        }
        com.navdy.service.library.events.NavdyEvent$MessageType a1 = (com.navdy.service.library.events.NavdyEvent$MessageType)com.squareup.wire.Message.enumFromInt(com.navdy.service.library.events.NavdyEvent$MessageType.class, a0.getTag() - 100);
        return new com.navdy.service.library.events.NavdyEvent$Builder().type(a1).setExtension(a0, a).build();
    }
    
    private static com.squareup.wire.Extension findExtension(com.navdy.service.library.events.NavdyEvent a) {
        com.navdy.service.library.events.NavdyEvent$MessageType a0 = a.type;
        com.squareup.wire.Extension a1 = null;
        if (a0 != null) {
            int i = a.type.ordinal() + 101 - minTag;
            a1 = null;
            if (i >= 0) {
                a1 = (i < tagToExtensionMap.length) ? tagToExtensionMap[i] : null;
            }
        }
        return a1;
    }
    
    private static com.squareup.wire.Extension findExtension(com.squareup.wire.Message a) {
        return (com.squareup.wire.Extension)classToExtensionMap.get((a).getClass());
    }
    
    private static Class getBuilder(Class a) {
        return (a != null) ? Class.forName(new StringBuilder().append(a.getName()).append("$Builder").toString()) : null;
    }
    
    public static com.squareup.wire.Message getDefault(Class a) {
        return com.navdy.service.library.events.NavdyEventUtil.getDefault(a, (com.navdy.service.library.events.NavdyEventUtil$Initializer)null);
    }
    
    public static com.squareup.wire.Message getDefault(Class a, com.navdy.service.library.events.NavdyEventUtil$Initializer a0) {
        com.squareup.wire.Message a1 = null;
        label3: if (a != null) {
            Class a2 = null;
            com.squareup.wire.Message.Builder a3 = null;
            label4: {
                Class a4 = null;
                IllegalAccessException a5 = null;
                label7: {
                    Class a6 = null;
                    InstantiationException a7 = null;
                    label6: {
                        Class a8 = null;
                        NoSuchMethodException a9 = null;
                        label5: {
                            Class a10 = null;
                            ClassNotFoundException a11 = null;
                            label8: try {
                                try {
                                    try {
                                        try {
                                            try {
                                                a4 = null;
                                                a6 = null;
                                                a8 = null;
                                                a2 = null;
                                                a10 = null;
                                                a8 = com.navdy.service.library.events.NavdyEventUtil.getBuilder(a);
                                                a4 = a8;
                                                a6 = a8;
                                                a2 = a8;
                                                a10 = a8;
                                                a3 = (com.squareup.wire.Message.Builder)a8.getConstructor(new Class[0]).newInstance(new Object[0]);
                                            } catch(ClassNotFoundException a12) {
                                                a11 = a12;
                                                break label8;
                                            }
                                        } catch(IllegalAccessException a13) {
                                            a5 = a13;
                                            break label7;
                                        }
                                    } catch(InstantiationException a14) {
                                        a7 = a14;
                                        break label6;
                                    }
                                } catch(NoSuchMethodException a15) {
                                    a9 = a15;
                                    break label5;
                                }
                                a2 = a8;
                                break label4;
                            } catch(java.lang.reflect.InvocationTargetException a16) {
                                sLogger.e(a16.getMessage(), (Throwable)a16);
                                a3 = null;
                                break label4;
                            }
                            sLogger.e(a11.getMessage(), (Throwable)a11);
                            a3 = null;
                            a2 = a10;
                            break label4;
                        }
                        sLogger.e(a9.getMessage(), (Throwable)a9);
                        a3 = null;
                        a2 = a8;
                        break label4;
                    }
                    sLogger.e(a7.getMessage(), (Throwable)a7);
                    a3 = null;
                    a2 = a6;
                    break label4;
                }
                sLogger.e(a5.getMessage(), (Throwable)a5);
                a3 = null;
                a2 = a4;
            }
            label1: {
                label2: {
                    if (a2 == null) {
                        break label2;
                    }
                    if (a3 != null) {
                        break label1;
                    }
                }
                a1 = null;
                break label3;
            }
            java.lang.reflect.Field[] a17 = a2.getDeclaredFields();
            int i = a17.length;
            int i0 = 0;
            while(i0 < i) {
                java.lang.reflect.Field a18 = a17[i0];
                label0: {
                    IllegalAccessException a19 = null;
                    try {
                        try {
                            Object a20 = null;
                            if (com.squareup.wire.Message.class.isAssignableFrom(a18.getType())) {
                                a20 = com.navdy.service.library.events.NavdyEventUtil.getDefault(a18.getType());
                            } else {
                                boolean b = java.lang.reflect.Modifier.isFinal(a18.getModifiers());
                                a20 = null;
                                if (!b) {
                                    boolean b0 = a18.getName().startsWith("$");
                                    a20 = null;
                                    if (!b0) {
                                        a20 = a.getField(new StringBuilder().append("DEFAULT_").append(a18.getName().toUpperCase()).toString()).get(null);
                                    }
                                }
                            }
                            if (a20 == null) {
                                break label0;
                            }
                            a18.set(a3, a20);
                            break label0;
                        } catch(IllegalAccessException a21) {
                            a19 = a21;
                        }
                    } catch(NoSuchFieldException a22) {
                        sLogger.e(a22.getMessage(), (Throwable)a22);
                        break label0;
                    }
                    sLogger.e(a19.getMessage(), (Throwable)a19);
                }
                i0 = i0 + 1;
            }
            a1 = (a0 == null) ? a3.build() : a0.build(a3);
        } else {
            a1 = null;
        }
        return a1;
    }
    
    public static com.squareup.wire.Message merge(com.squareup.wire.Message a, com.squareup.wire.Message a0) {
        com.squareup.wire.Message a1 = null;
        label2: {
            Class a2 = null;
            com.squareup.wire.Message.Builder a3 = null;
            com.squareup.wire.Message.Builder a4 = null;
            label8: {
                label9: {
                    if (a == null) {
                        break label9;
                    }
                    if (a0 != null) {
                        break label8;
                    }
                }
                a1 = null;
                break label2;
            }
            Class a5 = (a).getClass();
            label3: {
                Class a6 = null;
                com.squareup.wire.Message.Builder a7 = null;
                ClassNotFoundException a8 = null;
                label6: {
                    Class a9 = null;
                    com.squareup.wire.Message.Builder a10 = null;
                    NoSuchMethodException a11 = null;
                    label5: {
                        Class a12 = null;
                        com.squareup.wire.Message.Builder a13 = null;
                        java.lang.reflect.InvocationTargetException a14 = null;
                        label4: {
                            Class a15 = null;
                            com.squareup.wire.Message.Builder a16 = null;
                            InstantiationException a17 = null;
                            label7: try {
                                try {
                                    try {
                                        try {
                                            try {
                                                a6 = null;
                                                a7 = null;
                                                a9 = null;
                                                a10 = null;
                                                a12 = null;
                                                a13 = null;
                                                a2 = null;
                                                a3 = null;
                                                a15 = null;
                                                a16 = null;
                                                a12 = com.navdy.service.library.events.NavdyEventUtil.getBuilder(a5);
                                                a6 = a12;
                                                a7 = null;
                                                a9 = a12;
                                                a10 = null;
                                                a13 = null;
                                                a2 = a12;
                                                a3 = null;
                                                a15 = a12;
                                                a16 = null;
                                                Class[] a18 = new Class[1];
                                                a18[0] = a5;
                                                java.lang.reflect.Constructor a19 = a12.getConstructor(a18);
                                                Object[] a20 = new Object[1];
                                                a20[0] = a;
                                                a13 = (com.squareup.wire.Message.Builder)a19.newInstance(a20);
                                                a6 = a12;
                                                a7 = a13;
                                                a9 = a12;
                                                a10 = a13;
                                                a2 = a12;
                                                a3 = a13;
                                                a15 = a12;
                                                a16 = a13;
                                                Class[] a21 = new Class[1];
                                                a21[0] = a5;
                                                java.lang.reflect.Constructor a22 = a12.getConstructor(a21);
                                                Object[] a23 = new Object[1];
                                                a23[0] = a0;
                                                a4 = (com.squareup.wire.Message.Builder)a22.newInstance(a23);
                                            } catch(InstantiationException a24) {
                                                a17 = a24;
                                                break label7;
                                            }
                                        } catch(ClassNotFoundException a25) {
                                            a8 = a25;
                                            break label6;
                                        }
                                    } catch(NoSuchMethodException a26) {
                                        a11 = a26;
                                        break label5;
                                    }
                                } catch(java.lang.reflect.InvocationTargetException a27) {
                                    a14 = a27;
                                    break label4;
                                }
                                a3 = a13;
                                a2 = a12;
                                break label3;
                            } catch(IllegalAccessException a28) {
                                sLogger.e(a28.getMessage(), (Throwable)a28);
                                a4 = null;
                                break label3;
                            }
                            sLogger.e(a17.getMessage(), (Throwable)a17);
                            a3 = a16;
                            a4 = null;
                            a2 = a15;
                            break label3;
                        }
                        sLogger.e(a14.getMessage(), (Throwable)a14);
                        a3 = a13;
                        a4 = null;
                        a2 = a12;
                        break label3;
                    }
                    sLogger.e(a11.getMessage(), (Throwable)a11);
                    a3 = a10;
                    a4 = null;
                    a2 = a9;
                    break label3;
                }
                sLogger.e(a8.getMessage(), (Throwable)a8);
                a3 = a7;
                a4 = null;
                a2 = a6;
            }
            label0: {
                label1: {
                    if (a2 == null) {
                        break label1;
                    }
                    if (a3 == null) {
                        break label1;
                    }
                    if (a4 != null) {
                        break label0;
                    }
                }
                a1 = null;
                break label2;
            }
            java.lang.reflect.Field[] a29 = a2.getDeclaredFields();
            int i = a29.length;
            int i0 = 0;
            while(i0 < i) {
                java.lang.reflect.Field a30 = a29[i0];
                try {
                    Object a31 = null;
                    Object a32 = a30.get(a3);
                    Object a33 = a30.get(a4);
                    if (com.squareup.wire.Message.class.isAssignableFrom(a30.getType())) {
                        a31 = com.navdy.service.library.events.NavdyEventUtil.merge((com.squareup.wire.Message)a32, (com.squareup.wire.Message)a33);
                    } else {
                        boolean b = java.lang.reflect.Modifier.isFinal(a30.getModifiers());
                        a31 = null;
                        if (!b) {
                            a31 = com.squareup.wire.Wire.get(a32, a33);
                        }
                    }
                    if (a31 != null) {
                        a30.set(a3, a31);
                    }
                } catch(IllegalAccessException a34) {
                    sLogger.e(a34.getMessage(), (Throwable)a34);
                }
                i0 = i0 + 1;
            }
            a1 = a3.build();
        }
        return a1;
    }
    
    public static com.squareup.wire.Message messageFromEvent(com.navdy.service.library.events.NavdyEvent a) {
        com.squareup.wire.Message a0 = null;
        com.squareup.wire.Extension a1 = com.navdy.service.library.events.NavdyEventUtil.findExtension(a);
        if (a1 != null) {
            a0 = (com.squareup.wire.Message)a.getExtension(a1);
        } else {
            sLogger.e(new StringBuilder().append("Unable to find extension for event: ").append(a).toString());
            a0 = null;
        }
        return a0;
    }
}
