package com.navdy.service.library.events;

final public class Frame$Builder extends com.squareup.wire.Message.Builder {
    public Integer size;
    
    public Frame$Builder() {
    }
    
    public Frame$Builder(com.navdy.service.library.events.Frame a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.size = a.size;
        }
    }
    
    public com.navdy.service.library.events.Frame build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.Frame(this, (com.navdy.service.library.events.Frame$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.Frame$Builder size(Integer a) {
        this.size = a;
        return this;
    }
}
