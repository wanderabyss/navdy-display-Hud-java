package com.navdy.service.library.events.audio;

final public class VoiceSearchResponse$VoiceSearchState extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState[] $VALUES;
    final public static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState VOICE_SEARCH_ERROR;
    final public static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState VOICE_SEARCH_LISTENING;
    final public static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState VOICE_SEARCH_SEARCHING;
    final public static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState VOICE_SEARCH_STARTING;
    final public static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState VOICE_SEARCH_SUCCESS;
    final private int value;
    
    static {
        VOICE_SEARCH_ERROR = new com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState("VOICE_SEARCH_ERROR", 0, 0);
        VOICE_SEARCH_STARTING = new com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState("VOICE_SEARCH_STARTING", 1, 1);
        VOICE_SEARCH_LISTENING = new com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState("VOICE_SEARCH_LISTENING", 2, 2);
        VOICE_SEARCH_SEARCHING = new com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState("VOICE_SEARCH_SEARCHING", 3, 3);
        VOICE_SEARCH_SUCCESS = new com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState("VOICE_SEARCH_SUCCESS", 4, 4);
        com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState[] a = new com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState[5];
        a[0] = VOICE_SEARCH_ERROR;
        a[1] = VOICE_SEARCH_STARTING;
        a[2] = VOICE_SEARCH_LISTENING;
        a[3] = VOICE_SEARCH_SEARCHING;
        a[4] = VOICE_SEARCH_SUCCESS;
        $VALUES = a;
    }
    
    private VoiceSearchResponse$VoiceSearchState(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState valueOf(String s) {
        return (com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState)Enum.valueOf(com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState.class, s);
    }
    
    public static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
