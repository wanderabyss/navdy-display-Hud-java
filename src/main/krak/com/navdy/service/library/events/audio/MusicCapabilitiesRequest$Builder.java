package com.navdy.service.library.events.audio;

final public class MusicCapabilitiesRequest$Builder extends com.squareup.wire.Message.Builder {
    public MusicCapabilitiesRequest$Builder() {
    }
    
    public MusicCapabilitiesRequest$Builder(com.navdy.service.library.events.audio.MusicCapabilitiesRequest a) {
        super((com.squareup.wire.Message)a);
    }
    
    public com.navdy.service.library.events.audio.MusicCapabilitiesRequest build() {
        return new com.navdy.service.library.events.audio.MusicCapabilitiesRequest(this, (com.navdy.service.library.events.audio.MusicCapabilitiesRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
}
