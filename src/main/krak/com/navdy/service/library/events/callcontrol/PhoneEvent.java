package com.navdy.service.library.events.callcontrol;

final public class PhoneEvent extends com.squareup.wire.Message {
    final public static String DEFAULT_CALLUUID = "";
    final public static String DEFAULT_CONTACT_NAME = "";
    final public static String DEFAULT_LABEL = "";
    final public static String DEFAULT_NUMBER = "";
    final public static com.navdy.service.library.events.callcontrol.PhoneStatus DEFAULT_STATUS;
    final private static long serialVersionUID = 0L;
    final public String callUUID;
    final public String contact_name;
    final public String label;
    final public String number;
    final public com.navdy.service.library.events.callcontrol.PhoneStatus status;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_IDLE;
    }
    
    private PhoneEvent(com.navdy.service.library.events.callcontrol.PhoneEvent$Builder a) {
        this(a.status, a.number, a.contact_name, a.label, a.callUUID);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    PhoneEvent(com.navdy.service.library.events.callcontrol.PhoneEvent$Builder a, com.navdy.service.library.events.callcontrol.PhoneEvent$1 a0) {
        this(a);
    }
    
    public PhoneEvent(com.navdy.service.library.events.callcontrol.PhoneStatus a, String s, String s0, String s1, String s2) {
        this.status = a;
        this.number = s;
        this.contact_name = s0;
        this.label = s1;
        this.callUUID = s2;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.callcontrol.PhoneEvent) {
                com.navdy.service.library.events.callcontrol.PhoneEvent a0 = (com.navdy.service.library.events.callcontrol.PhoneEvent)a;
                boolean b0 = this.equals(this.status, a0.status);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.number, a0.number)) {
                        break label1;
                    }
                    if (!this.equals(this.contact_name, a0.contact_name)) {
                        break label1;
                    }
                    if (!this.equals(this.label, a0.label)) {
                        break label1;
                    }
                    if (this.equals(this.callUUID, a0.callUUID)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((this.status == null) ? 0 : this.status.hashCode()) * 37 + ((this.number == null) ? 0 : this.number.hashCode())) * 37 + ((this.contact_name == null) ? 0 : this.contact_name.hashCode())) * 37 + ((this.label == null) ? 0 : this.label.hashCode())) * 37 + ((this.callUUID == null) ? 0 : this.callUUID.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
