package com.navdy.service.library.events.connection;

final public class ConnectionRequest$Action extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.connection.ConnectionRequest$Action[] $VALUES;
    final public static com.navdy.service.library.events.connection.ConnectionRequest$Action CONNECTION_SELECT;
    final public static com.navdy.service.library.events.connection.ConnectionRequest$Action CONNECTION_START_SEARCH;
    final public static com.navdy.service.library.events.connection.ConnectionRequest$Action CONNECTION_STOP_SEARCH;
    final private int value;
    
    static {
        CONNECTION_START_SEARCH = new com.navdy.service.library.events.connection.ConnectionRequest$Action("CONNECTION_START_SEARCH", 0, 1);
        CONNECTION_STOP_SEARCH = new com.navdy.service.library.events.connection.ConnectionRequest$Action("CONNECTION_STOP_SEARCH", 1, 2);
        CONNECTION_SELECT = new com.navdy.service.library.events.connection.ConnectionRequest$Action("CONNECTION_SELECT", 2, 3);
        com.navdy.service.library.events.connection.ConnectionRequest$Action[] a = new com.navdy.service.library.events.connection.ConnectionRequest$Action[3];
        a[0] = CONNECTION_START_SEARCH;
        a[1] = CONNECTION_STOP_SEARCH;
        a[2] = CONNECTION_SELECT;
        $VALUES = a;
    }
    
    private ConnectionRequest$Action(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.connection.ConnectionRequest$Action valueOf(String s) {
        return (com.navdy.service.library.events.connection.ConnectionRequest$Action)Enum.valueOf(com.navdy.service.library.events.connection.ConnectionRequest$Action.class, s);
    }
    
    public static com.navdy.service.library.events.connection.ConnectionRequest$Action[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
