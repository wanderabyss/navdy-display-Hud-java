package com.navdy.service.library.events.debug;

final public class DriveRecordingsRequest$Builder extends com.squareup.wire.Message.Builder {
    public DriveRecordingsRequest$Builder() {
    }
    
    public DriveRecordingsRequest$Builder(com.navdy.service.library.events.debug.DriveRecordingsRequest a) {
        super((com.squareup.wire.Message)a);
    }
    
    public com.navdy.service.library.events.debug.DriveRecordingsRequest build() {
        return new com.navdy.service.library.events.debug.DriveRecordingsRequest(this, (com.navdy.service.library.events.debug.DriveRecordingsRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
}
