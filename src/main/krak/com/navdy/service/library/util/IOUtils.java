package com.navdy.service.library.util;

final public class IOUtils {
    final private static int BUFFER_SIZE = 16384;
    final private static int BUFFER_SIZE_FOR_DOWNLOADS = 1024;
    final private static String CHECKSUM_FILE_NAME = ".checksum";
    final private static boolean CHECKSUM_UPDATE_MODE = false;
    final private static int DIGEST_BUFFER_SIZE = 1048576;
    final private static int END_OF_STREAM = -1;
    final private static int INPUT_BUFFER_SIZE = 16384;
    final private static String TAG;
    final private static String TRASH_DIR_NAME = ".trash";
    final public static String UTF_8 = "UTF-8";
    final private static java.util.concurrent.atomic.AtomicLong sCounter;
    private static java.io.File sExternalTrashDir;
    final private static Object sLock;
    private static com.navdy.service.library.log.Logger sLogger;
    private static volatile java.io.File sTrashDir;
    
    static {
        TAG = com.navdy.service.library.util.IOUtils.class.getName();
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.util.IOUtils.class);
        sCounter = new java.util.concurrent.atomic.AtomicLong(1L);
        sLock = new Object();
    }
    
    public IOUtils() {
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public static byte[] bitmap2ByteBuffer(android.graphics.Bitmap a) {
        byte[] a0 = null;
        if (a != null) {
            java.io.ByteArrayOutputStream a1 = new java.io.ByteArrayOutputStream();
            a.compress(android.graphics.Bitmap.CompressFormat.PNG, 100, (java.io.OutputStream)a1);
            a0 = a1.toByteArray();
        } else {
            a0 = null;
        }
        return a0;
    }
    
    public static String bytesToHexString(byte[] a) {
        return com.navdy.service.library.util.IOUtils.bytesToHexString(a, 0, a.length);
    }
    
    public static String bytesToHexString(byte[] a, int i, int i0) {
        StringBuilder a0 = new StringBuilder();
        while(i < i0) {
            int i1 = a[i];
            String s = Integer.toHexString(i1 & 255);
            if (s.length() == 1) {
                a0.append((char)48);
            }
            a0.append(s);
            i = i + 1;
        }
        return a0.toString();
    }
    
    public static void checkIntegrity(android.content.Context a, String s, int i) {
        String s0 = null;
        String s1 = null;
        boolean b = false;
        sLogger.i(new StringBuilder().append("integrity check for ").append(s).append(" starting").toString());
        long j = android.os.SystemClock.elapsedRealtime();
        label10: {
            java.io.InputStream a0 = null;
            Throwable a1 = null;
            label9: {
                try {
                    a0 = null;
                    s0 = null;
                    a0 = a.getResources().openRawResource(i);
                    s0 = null;
                    s0 = com.navdy.service.library.util.IOUtils.convertInputStreamToString(a0, "UTF-8");
                    s1 = com.navdy.service.library.util.IOUtils.convertFileToString(new StringBuilder().append(s).append(java.io.File.separator).append(".checksum").toString());
                } catch(Throwable a2) {
                    a1 = a2;
                    break label9;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                break label10;
            }
            try {
                sLogger.i("error while retrieving integrity checksum from filesystem, might not be present", a1);
            } catch(Throwable a3) {
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                throw a3;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            s1 = null;
        }
        label8: {
            label6: {
                label7: {
                    if (s0 == null) {
                        break label7;
                    }
                    if (s1 == null) {
                        break label7;
                    }
                    if (s0.equals(s1)) {
                        break label6;
                    }
                }
                b = false;
                break label8;
            }
            b = true;
        }
        label0: {
            java.io.PrintWriter a4 = null;
            Throwable a5 = null;
            label1: {
                label2: if (b) {
                    sLogger.v(new StringBuilder().append("checksum for ").append(s).append(" is fine, no-op").toString());
                    break label0;
                } else {
                    boolean b0 = false;
                    String[] a6 = new String[1];
                    a6[0] = ".checksum";
                    String s2 = com.navdy.service.library.util.IOUtils.hashForPath(s, true, a6);
                    label5: {
                        label3: {
                            label4: {
                                if (s2 == null) {
                                    break label4;
                                }
                                if (s0 == null) {
                                    break label4;
                                }
                                if (s2.equals(s0)) {
                                    break label3;
                                }
                            }
                            b0 = false;
                            break label5;
                        }
                        b0 = true;
                    }
                    if (b0) {
                        sLogger.v(new StringBuilder().append("checksum for ").append(s).append(" is fine, writing integrity checksum on filesystem").toString());
                        try {
                            a4 = new java.io.PrintWriter(new StringBuilder().append(s).append(java.io.File.separator).append(".checksum").toString());
                        } catch(Throwable a7) {
                            a5 = a7;
                            break label2;
                        }
                        try {
                            a4.print(s2);
                        } catch(Throwable a8) {
                            a5 = a8;
                            break label1;
                        }
                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a4);
                        break label0;
                    } else {
                        sLogger.w(new StringBuilder().append("files on ").append(s).append(" are out of date or corrupted, redoing").toString());
                        com.navdy.service.library.util.IOUtils.deleteDirectory(a, new java.io.File(s));
                        com.navdy.service.library.util.IOUtils.createDirectory(s);
                        break label0;
                    }
                }
                a4 = null;
            }
            try {
                sLogger.e("could not write integrity checksum on filesystem", a5);
            } catch(Throwable a9) {
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a4);
                throw a9;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a4);
        }
        long j0 = android.os.SystemClock.elapsedRealtime();
        sLogger.i(new StringBuilder().append("integrity check for ").append(s).append(" took ").append(j0 - j).append(" ms").toString());
    }
    
    private static void cleanDirectory(android.content.Context a, java.io.File a0) {
        if (a0.exists() && a0.isDirectory()) {
            java.io.File[] a1 = a0.listFiles();
            if (a1 != null) {
                int i = a1.length;
                java.io.IOException a2 = null;
                int i0 = 0;
                while(i0 < i) {
                    java.io.File a3 = a1[i0];
                    try {
                        com.navdy.service.library.util.IOUtils.forceDelete(a, a3);
                    } catch(java.io.IOException a4) {
                        a2 = a4;
                    }
                    i0 = i0 + 1;
                }
                if (a2 != null) {
                    throw a2;
                }
            }
        }
    }
    
    public static void closeFD(int i) {
        label0: {
            Throwable a = null;
            if (i == -1) {
                break label0;
            }
            try {
                android.os.ParcelFileDescriptor.adoptFd(i).close();
                break label0;
            } catch(Throwable a0) {
                a = a0;
            }
            sLogger.e(a);
        }
    }
    
    public static void closeObject(java.io.Closeable a) {
        label0: {
            Throwable a0 = null;
            if (a == null) {
                break label0;
            }
            try {
                a.close();
                break label0;
            } catch(Throwable a1) {
                a0 = a1;
            }
            sLogger.e(a0);
        }
    }
    
    public static void closeStream(java.io.Closeable a) {
        com.navdy.service.library.util.IOUtils.closeObject(a);
    }
    
    public static void compressFilesToZip(android.content.Context a, java.io.File[] a0, String s) {
        java.io.File a1 = new java.io.File(s);
        if (a1.exists()) {
            com.navdy.service.library.util.IOUtils.deleteFile(a, s);
        }
        label6: {
            boolean b = false;
            java.io.FileOutputStream a2 = null;
            Throwable a3 = null;
            java.util.zip.ZipOutputStream a4 = null;
            java.io.FileInputStream a5 = null;
            label7: {
                try {
                    b = a1.createNewFile();
                    break label7;
                } catch(java.io.IOException ignoredException) {
                }
                sLogger.e("IO Exception while creating new file");
                break label6;
            }
            if (!b) {
                break label6;
            }
            if (a0 == null) {
                break label6;
            }
            label0: {
                label1: {
                    {
                        label4: {
                            label5: {
                                byte[] a6 = null;
                                int i = 0;
                                int i0 = 0;
                                try {
                                    a6 = new byte[16384];
                                    a2 = new java.io.FileOutputStream(s);
                                } catch(Throwable a7) {
                                    a3 = a7;
                                    break label5;
                                }
                                try {
                                    a4 = new java.util.zip.ZipOutputStream((java.io.OutputStream)a2);
                                } catch(Throwable a8) {
                                    a3 = a8;
                                    break label4;
                                }
                                try {
                                    a5 = null;
                                    i = a0.length;
                                    i0 = 0;
                                } catch(NullPointerException a9) {
                                    a3 = a9;
                                    break label0;
                                }
                                while(i0 < i) {
                                    java.io.File a10 = a0[i0];
                                    label2: {
                                        try {
                                            boolean b0 = a10.exists();
                                            label3: {
                                                if (b0) {
                                                    break label3;
                                                }
                                                break label2;
                                            }
                                            if (!a10.canRead()) {
                                                break label2;
                                            }
                                            a5 = new java.io.FileInputStream(a10);
                                        } catch(Throwable a11) {
                                            a3 = a11;
                                            break label1;
                                        }
                                        try {
                                            a4.putNextEntry(new java.util.zip.ZipEntry(a10.getName()));
                                            while(true) {
                                                int i1 = a5.read(a6);
                                                if (i1 <= 0) {
                                                    break;
                                                }
                                                a4.write(a6, 0, i1);
                                            }
                                            a4.closeEntry();
                                            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a5);
                                        } catch(Throwable a12) {
                                            a3 = a12;
                                            break label0;
                                        }
                                    }
                                    i0 = i0 + 1;
                                }
                                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a4);
                                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)null);
                                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                                break label6;
                            }
                            a2 = null;
                            a5 = null;
                            a4 = null;
                            break label0;
                        }
                        a5 = null;
                        a4 = null;
                    }
                    break label0;
                }
                a5 = null;
            }
            try {
                sLogger.e("Error while compressing files ", a3);
            } catch(Throwable a13) {
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a4);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a5);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                throw a13;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a4);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a5);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
        }
    }
    
    public static String convertFileToString(String s) {
        String s0 = null;
        java.io.FileInputStream a = new java.io.FileInputStream(s);
        try {
            s0 = com.navdy.service.library.util.IOUtils.convertInputStreamToString((java.io.InputStream)a, "UTF-8");
        } catch(Throwable a0) {
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
            throw a0;
        }
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
        return s0;
    }
    
    public static String convertInputStreamToString(java.io.InputStream a, String s) {
        java.io.ByteArrayOutputStream a0 = new java.io.ByteArrayOutputStream();
        byte[] a1 = new byte[16384];
        while(true) {
            int i = a.read(a1);
            if (i == -1) {
                return a0.toString(s);
            }
            a0.write(a1, 0, i);
        }
    }
    
    public static int copyFile(String s, java.io.InputStream a) {
        java.io.FileOutputStream a0 = null;
        int i = 0;
        label0: {
            Throwable a1 = null;
            label2: {
                label1: {
                    try {
                        a0 = new java.io.FileOutputStream(s);
                        break label1;
                    } catch(Throwable a2) {
                        a1 = a2;
                    }
                    a0 = null;
                    break label2;
                }
                try {
                    byte[] a3 = new byte[16384];
                    i = 0;
                    while(true) {
                        int i0 = a.read(a3);
                        if (i0 <= 0) {
                            break label0;
                        }
                        a0.write(a3, 0, i0);
                        i = i + i0;
                    }
                } catch(Throwable a4) {
                    a1 = a4;
                }
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
            com.navdy.service.library.util.IOUtils.fileSync(a0);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            throw a1;
        }
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
        com.navdy.service.library.util.IOUtils.fileSync(a0);
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
        return i;
    }
    
    public static int copyFile(String s, byte[] a) {
        return com.navdy.service.library.util.IOUtils.copyFile(s, a, true);
    }
    
    public static int copyFile(String s, byte[] a, boolean b) {
        java.io.FileOutputStream a0 = null;
        int i = 0;
        label0: {
            Throwable a1 = null;
            label2: {
                label1: {
                    try {
                        a0 = new java.io.FileOutputStream(s);
                        break label1;
                    } catch(Throwable a2) {
                        a1 = a2;
                    }
                    a0 = null;
                    break label2;
                }
                try {
                    a0.write(a, 0, a.length);
                    i = a.length;
                    break label0;
                } catch(Throwable a3) {
                    a1 = a3;
                }
            }
            if (b) {
                com.navdy.service.library.util.IOUtils.fileSync(a0);
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            throw a1;
        }
        if (b) {
            com.navdy.service.library.util.IOUtils.fileSync(a0);
        }
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
        return 0 + i;
    }
    
    public static boolean copyFile(String s, String s0) {
        boolean b = false;
        label4: {
            label1: {
                label0: {
                    java.io.FileInputStream a = null;
                    Throwable a0 = null;
                    label3: {
                        label2: {
                            try {
                                java.io.File a1 = new java.io.File(s);
                                if (!a1.exists()) {
                                    break label1;
                                }
                                a = new java.io.FileInputStream(a1);
                                break label2;
                            } catch(Throwable a2) {
                                a0 = a2;
                            }
                            a = null;
                            break label3;
                        }
                        try {
                            com.navdy.service.library.util.IOUtils.copyFile(s0, (java.io.InputStream)a);
                            break label0;
                        } catch(Throwable a3) {
                            a0 = a3;
                        }
                    }
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
                    throw a0;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)null);
                b = true;
                break label4;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)null);
            b = false;
        }
        return b;
    }
    
    public static boolean createDirectory(java.io.File a) {
        boolean b = false;
        label1: {
            label0: {
                if (a == null) {
                    break label0;
                }
                if (a.exists()) {
                    break label0;
                }
                b = a.mkdirs();
                break label1;
            }
            if (a == null) {
                b = false;
            } else if (a.isDirectory()) {
                sLogger.v(new StringBuilder().append("Directory already exists: ").append(a.getPath()).toString());
                b = true;
            } else {
                b = false;
            }
        }
        if (b) {
            sLogger.v(new StringBuilder().append("Successfully created directory: ").append(a.getPath()).toString());
        } else {
            sLogger.e(new StringBuilder().append("Unable to create directory: ").append((a == null) ? "null" : a.getPath()).toString());
        }
        return b;
    }
    
    public static boolean createDirectory(String s) {
        return com.navdy.service.library.util.IOUtils.createDirectory(new java.io.File(s));
    }
    
    public static void deCompressZipToDirectory(android.content.Context a, java.io.File a0, java.io.File a1) {
        byte[] a2 = new byte[1024];
        java.util.zip.ZipInputStream a3 = new java.util.zip.ZipInputStream((java.io.InputStream)new java.io.FileInputStream(a0));
        java.util.zip.ZipEntry a4 = a3.getNextEntry();
        while(a4 != null) {
            java.io.File a5 = new java.io.File(a1, a4.getName());
            new java.io.File(a5.getParent()).mkdirs();
            java.io.FileOutputStream a6 = new java.io.FileOutputStream(a5);
            while(true) {
                int i = a3.read(a2);
                if (i <= 0) {
                    a6.close();
                    a4 = a3.getNextEntry();
                    break;
                } else {
                    a6.write(a2, 0, i);
                }
            }
        }
        a3.closeEntry();
        a3.close();
    }
    
    public static void deleteDirectory(android.content.Context a, java.io.File a0) {
        label1: {
            java.io.IOException a1 = null;
            if (a0 == null) {
                break label1;
            }
            if (!a0.exists()) {
                break label1;
            }
            java.io.File a2 = (com.navdy.service.library.util.IOUtils.isAppsInternalFile(a, a0.getAbsolutePath())) ? new java.io.File(com.navdy.service.library.util.IOUtils.getNewTrashEntryPath(a)) : (com.navdy.service.library.util.IOUtils.isAppsExternalFile(a, a0.getAbsolutePath())) ? new java.io.File(com.navdy.service.library.util.IOUtils.getNewExternalTrashEntryPath(a)) : new java.io.File(com.navdy.service.library.util.IOUtils.getTrashEntryPathInSameFolder(a0));
            boolean b = a0.renameTo(a2);
            if (!b) {
                sLogger.e(new StringBuilder().append("Unable to rename dir ").append(a0).append(" to: ").append(a2).toString());
            }
            if (b) {
                a0 = a2;
            }
            label0: {
                try {
                    com.navdy.service.library.util.IOUtils.deleteDirectoryInternal(a, a0);
                } catch(java.io.IOException a3) {
                    a1 = a3;
                    break label0;
                }
                break label1;
            }
            sLogger.e((Throwable)a1);
        }
    }
    
    private static void deleteDirectoryInternal(android.content.Context a, java.io.File a0) {
        if (a0.exists()) {
            com.navdy.service.library.util.IOUtils.cleanDirectory(a, a0);
            if (!a0.delete()) {
                throw new java.io.IOException(new StringBuilder().append("Unable to delete directory ").append(a0).toString());
            }
        }
    }
    
    public static boolean deleteFile(android.content.Context a, String s) {
        boolean b = false;
        if (s != null) {
            java.io.File a0 = new java.io.File(s);
            if (a0.exists()) {
                java.io.File a1 = (com.navdy.service.library.util.IOUtils.isAppsInternalFile(a, s)) ? new java.io.File(com.navdy.service.library.util.IOUtils.getNewTrashEntryPath(a)) : (com.navdy.service.library.util.IOUtils.isAppsExternalFile(a, s)) ? new java.io.File(com.navdy.service.library.util.IOUtils.getNewExternalTrashEntryPath(a)) : new java.io.File(com.navdy.service.library.util.IOUtils.getTrashEntryPathInSameFolder(a0));
                boolean b0 = a0.renameTo(a1);
                sLogger.d(new StringBuilder().append("Deleted file: ").append(s).append(":").append(b0).toString());
                b = a1.delete();
            } else {
                b = false;
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public static byte[] downloadImage(String s) {
        byte[] a = null;
        label4: {
            java.net.HttpURLConnection a0 = null;
            java.io.BufferedInputStream a1 = null;
            java.io.ByteArrayOutputStream a2 = null;
            label0: {
                Throwable a3 = null;
                label2: {
                    label3: {
                        try {
                            a0 = null;
                            a0 = (java.net.HttpURLConnection)new java.net.URL(s).openConnection();
                            a1 = new java.io.BufferedInputStream(a0.getInputStream());
                            break label3;
                        } catch(Throwable a4) {
                            a3 = a4;
                        }
                        a1 = null;
                        a2 = null;
                        break label2;
                    }
                    label1: {
                        try {
                            a2 = new java.io.ByteArrayOutputStream();
                            break label1;
                        } catch(Throwable a5) {
                            a3 = a5;
                        }
                        a2 = null;
                        break label2;
                    }
                    try {
                        byte[] a6 = new byte[1024];
                        while(true) {
                            int i = a1.read(a6);
                            if (i < 0) {
                                break;
                            }
                            a2.write(a6, 0, i);
                        }
                        a = a2.toByteArray();
                        break label0;
                    } catch(Throwable a7) {
                        a3 = a7;
                    }
                }
                try {
                    sLogger.e("Exception while downloading binary resource", a3);
                } catch(Throwable a8) {
                    if (a0 != null) {
                        a0.disconnect();
                    }
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                    throw a8;
                }
                if (a0 != null) {
                    a0.disconnect();
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                a = null;
                break label4;
            }
            if (a0 != null) {
                a0.disconnect();
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
        }
        return a;
    }
    
    public static void fileSync(java.io.FileOutputStream a) {
        label0: {
            Throwable a0 = null;
            if (a == null) {
                break label0;
            }
            try {
                a.getFD().sync();
                break label0;
            } catch(Throwable a1) {
                a0 = a1;
            }
            sLogger.e(a0);
        }
    }
    
    private static void forceDelete(android.content.Context a, java.io.File a0) {
        if (a0.isDirectory()) {
            com.navdy.service.library.util.IOUtils.deleteDirectory(a, a0);
        } else if (!a0.delete()) {
            sLogger.e(new StringBuilder().append("Unable to delete kernel crash file: ").append(a0).toString());
        }
    }
    
    public static String getExternalStorageFolderPath() {
        String s = null;
        boolean b = android.os.Environment.getExternalStorageState().equals("mounted");
        label2: {
            java.io.File a = null;
            label0: {
                label1: {
                    if (!b) {
                        break label1;
                    }
                    a = android.os.Environment.getExternalStorageDirectory();
                    if (a == null) {
                        break label1;
                    }
                    if (a.exists()) {
                        break label0;
                    }
                }
                s = null;
                break label2;
            }
            s = a.getPath();
        }
        return s;
    }
    
    public static long getFreeSpace(String s) {
        long j = 0L;
        java.io.File a = new java.io.File(s);
        boolean b = a.exists();
        label2: {
            label0: {
                label1: {
                    if (!b) {
                        break label1;
                    }
                    if (a.isDirectory()) {
                        break label0;
                    }
                }
                j = -1L;
                break label2;
            }
            android.os.StatFs a0 = new android.os.StatFs(s);
            j = (long)a0.getFreeBlocks() * (long)a0.getBlockSize();
        }
        return j;
    }
    
    public static org.json.JSONObject getJSONFromURL(String s) {
        org.json.JSONObject a = null;
        label3: {
            java.net.HttpURLConnection a0 = null;
            java.io.InputStreamReader a1 = null;
            Throwable a2 = null;
            label1: {
                label2: {
                    try {
                        a0 = null;
                        a0 = (java.net.HttpURLConnection)new java.net.URL(s).openConnection();
                        a1 = new java.io.InputStreamReader(a0.getInputStream(), "UTF-8");
                        break label2;
                    } catch(Throwable a3) {
                        a2 = a3;
                    }
                    a1 = null;
                    break label1;
                }
                label0: {
                    try {
                        a = new org.json.JSONObject(com.navdy.service.library.util.IOUtils.convertInputStreamToString(a0.getInputStream(), "UTF-8"));
                        break label0;
                    } catch(Throwable a4) {
                        a2 = a4;
                    }
                    break label1;
                }
                if (a0 != null) {
                    a0.disconnect();
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
                break label3;
            }
            try {
                sLogger.e("Exception while downloading/parsing json", a2);
            } catch(Throwable a5) {
                if (a0 != null) {
                    a0.disconnect();
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
                throw a5;
            }
            if (a0 != null) {
                a0.disconnect();
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
            a = null;
        }
        return a;
    }
    
    public static String getNewExternalTrashEntryPath(android.content.Context a) {
        Object a0 = null;
        Throwable a1 = null;
        label0: {
            String s = null;
            label3: if (a != null) {
                synchronized(sLock) {
                    java.io.File a2 = sExternalTrashDir;
                    label1: {
                        label2: {
                            if (a2 != null) {
                                break label2;
                            }
                            java.io.File a3 = a.getExternalFilesDir((String)null);
                            if (a3 == null) {
                                break label1;
                            }
                            String s0 = a3.getAbsolutePath();
                            sExternalTrashDir = new java.io.File(new StringBuilder().append(s0).append(java.io.File.separator).append(".trash").toString());
                            com.navdy.service.library.util.IOUtils.createDirectory(sExternalTrashDir);
                        }
                        s = new StringBuilder().append(sExternalTrashDir.getAbsolutePath()).append(java.io.File.separator).append(System.currentTimeMillis()).append("_").append(sCounter.getAndIncrement()).toString();
                        /*monexit(a0)*/;
                        break label3;
                    }
                    /*monexit(a0)*/;
                    s = null;
                }
            } else {
                s = null;
            }
            return s;
        }
    }
    
    private static String getNewTrashEntryPath(android.content.Context a) {
        Object a0 = null;
        Throwable a1 = null;
        java.io.File a2 = sTrashDir;
        label0: {
            if (a2 == null) {
                synchronized(sLock) {
                    java.io.File a3 = sTrashDir;
                    if (a3 == null) {
                        sTrashDir = new java.io.File(new StringBuilder().append(a.getFilesDir().getAbsolutePath()).append(java.io.File.separator).append(".trash").toString());
                        com.navdy.service.library.util.IOUtils.createDirectory(sTrashDir);
                    }
                    /*monexit(a0)*/;
                }
            }
            return new StringBuilder().append(sTrashDir.getAbsolutePath()).append(java.io.File.separator).append(System.currentTimeMillis()).append("_").append(sCounter.getAndIncrement()).toString();
        }
    }
    
    public static int getSocketFD(android.bluetooth.BluetoothSocket a) {
        int i = 0;
        label1: {
            label0: {
                if (a == null) {
                    break label0;
                }
                try {
                    java.lang.reflect.Field a0 = android.bluetooth.BluetoothSocket.class.getDeclaredField("mSocket");
                    a0.setAccessible(true);
                    android.net.LocalSocket a1 = (android.net.LocalSocket)a0.get(a);
                    if (a1 == null) {
                        break label0;
                    }
                    java.lang.reflect.Field a2 = android.net.LocalSocket.class.getDeclaredField("impl");
                    a2.setAccessible(true);
                    Object a3 = a2.get(a1);
                    if (a3 == null) {
                        break label0;
                    }
                    java.lang.reflect.Field a4 = a3.getClass().getDeclaredField("fd");
                    a4.setAccessible(true);
                    java.io.FileDescriptor a5 = (java.io.FileDescriptor)a4.get(a3);
                    if (a5 == null) {
                        break label0;
                    }
                    java.lang.reflect.Field a6 = java.io.FileDescriptor.class.getDeclaredField("descriptor");
                    a6.setAccessible(true);
                    i = ((Integer)a6.get(a5)).intValue();
                    break label1;
                } catch(Throwable a7) {
                    sLogger.e(a7);
                }
            }
            i = -1;
        }
        return i;
    }
    
    public static java.io.File getTempFile(com.navdy.service.library.log.Logger a, String s) {
        java.io.File a0 = null;
        boolean b = android.os.Environment.getExternalStorageState().equals("mounted");
        label0: {
            java.io.IOException a1 = null;
            if (b) {
                a0 = (android.text.TextUtils.isEmpty((CharSequence)s)) ? new java.io.File(android.os.Environment.getExternalStorageDirectory(), com.navdy.service.library.util.IOUtils.getTempFilename()) : new java.io.File(android.os.Environment.getExternalStorageDirectory(), s);
                try {
                    if (!a0.createNewFile()) {
                        break label0;
                    }
                    if (a == null) {
                        android.util.Log.v(TAG, new StringBuilder().append("Created ").append(a0.getAbsolutePath()).toString());
                        break label0;
                    } else {
                        a.v(new StringBuilder().append("Created ").append(a0.getAbsolutePath()).toString());
                        break label0;
                    }
                } catch(java.io.IOException a2) {
                    a1 = a2;
                }
            } else {
                if (a == null) {
                    android.util.Log.e(TAG, "FileUtils:: External Storage not mounted!");
                } else {
                    a.e("FileUtils:: External Storage not mounted!");
                }
                a0 = null;
                break label0;
            }
            if (a == null) {
                android.util.Log.e(TAG, "FileUtils:: Unable to create file. ", (Throwable)a1);
            } else {
                a.e("FileUtils:: Unable to create file. ", (Throwable)a1);
            }
        }
        return a0;
    }
    
    public static String getTempFilename() {
        java.text.SimpleDateFormat a = new java.text.SimpleDateFormat("yyyy-MM-dd'_'HH:mm:ss.SSS", java.util.Locale.US);
        java.util.Date a0 = new java.util.Date(System.currentTimeMillis());
        return new StringBuilder().append(a.format(a0)).append(".jpg").toString();
    }
    
    public static String getTrashEntryPathInSameFolder(java.io.File a) {
        String s = null;
        if (a == null) {
            s = null;
        } else {
            String s0 = a.getParent();
            s = new StringBuilder().append(s0).append(java.io.File.separator).append(System.currentTimeMillis()).append("_").append(sCounter.getAndIncrement()).toString();
        }
        return s;
    }
    
    public static String hashForBitmap(android.graphics.Bitmap a) {
        String s = null;
        label0: {
            if (a == null) {
                break label0;
            }
            try {
                s = com.navdy.service.library.util.IOUtils.hashForBytes(com.navdy.service.library.util.IOUtils.bitmap2ByteBuffer(a));
                break label0;
            } catch(java.security.NoSuchAlgorithmException ignoredException) {
            }
            s = null;
        }
        return s;
    }
    
    public static String hashForBytes(byte[] a) {
        String s = null;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.length > 0) {
                        break label0;
                    }
                }
                s = null;
                break label2;
            }
            java.security.MessageDigest a0 = java.security.MessageDigest.getInstance("MD5");
            s = null;
            if (a0 != null) {
                a0.update(a);
                s = com.navdy.service.library.util.IOUtils.bytesToHexString(a0.digest());
            }
        }
        return s;
    }
    
    public static String hashForFile(java.io.File a) {
        return com.navdy.service.library.util.IOUtils.hashForFile(a, a.length());
    }
    
    public static String hashForFile(java.io.File a, long j) {
        java.io.FileInputStream a0 = null;
        Throwable a1 = null;
        sLogger.recordStartTime();
        label0: {
            if (a != null && a.exists() && a.canRead() && j <= a.length()) {
                String s = null;
                label4: {
                    Throwable a2 = null;
                    label2: {
                        java.security.MessageDigest a3 = null;
                        byte[] a4 = null;
                        long j0 = 0L;
                        label3: {
                            try {
                                a3 = java.security.MessageDigest.getInstance("MD5");
                                a4 = new byte[1048576];
                                a0 = new java.io.FileInputStream(a);
                                j0 = 0L;
                                break label3;
                            } catch(Throwable a5) {
                                a2 = a5;
                            }
                            a0 = null;
                            break label2;
                        }
                        label1: {
                            try {
                                while(true) {
                                    int i = a0.read(a4, 0, (int)Math.min(j - j0, 1048576L));
                                    a3.update(a4, 0, i);
                                    j0 = j0 + (long)i;
                                    if (j0 >= j) {
                                        break;
                                    }
                                    if (i == -1) {
                                        break;
                                    }
                                }
                                s = com.navdy.service.library.util.IOUtils.bytesToHexString(a3.digest());
                                sLogger.logTimeTaken(new StringBuilder().append("Time taken to calculate hash of the file of size :").append(a.length()).toString());
                                break label1;
                            } catch(Throwable a6) {
                                a2 = a6;
                            }
                            break label2;
                        }
                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                        break label4;
                    }
                    try {
                        sLogger.e("Exception while calculating md5 checksum for the file ", a2);
                    } catch(Throwable a7) {
                        a1 = a7;
                        break label0;
                    }
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                    s = null;
                }
                return s;
            }
            throw new IllegalArgumentException();
        }
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
        throw a1;
    }
    
    public static String hashForKey(String s) {
        String s0 = null;
        label0: {
            if (s == null) {
                s0 = null;
                break label0;
            } else {
                try {
                    s0 = com.navdy.service.library.util.IOUtils.hashForBytes(s.getBytes());
                    break label0;
                } catch(java.security.NoSuchAlgorithmException ignoredException) {
                }
            }
            s0 = String.valueOf(s.hashCode());
        }
        return s0;
    }
    
    public static String hashForPath(String s, boolean b, String[] a) {
        String s0 = null;
        sLogger.recordStartTime();
        try {
            java.security.MessageDigest a0 = java.security.MessageDigest.getInstance("MD5");
            byte[] a1 = new byte[1048576];
            com.navdy.service.library.util.IOUtils.traverseFiles(s, b, java.util.Arrays.asList((Object[])a), (com.navdy.service.library.util.IOUtils$OnFileTraversal)new com.navdy.service.library.util.IOUtils$1(a1, a0));
            s0 = com.navdy.service.library.util.IOUtils.bytesToHexString(a0.digest());
            sLogger.logTimeTaken(new StringBuilder().append("Time taken to calculate hash of all files on path:").append(s).toString());
        } catch(Throwable a2) {
            sLogger.e("Exception while calculating md5 checksum for a path ", a2);
            s0 = null;
        }
        return s0;
    }
    
    private static boolean isAppsExternalFile(android.content.Context a, String s) {
        return s.startsWith(a.getExternalFilesDir((String)null).getAbsolutePath());
    }
    
    private static boolean isAppsInternalFile(android.content.Context a, String s) {
        return s.startsWith(a.getFilesDir().getAbsolutePath());
    }
    
    public static boolean isExternalStorageReadable() {
        return "mounted".equals(android.os.Environment.getExternalStorageState());
    }
    
    public static byte[] readBinaryFile(String s) {
        java.io.File a = new java.io.File(s);
        byte[] a0 = new byte[(int)a.length()];
        java.io.FileInputStream a1 = new java.io.FileInputStream(a);
        try {
            a1.read(a0);
        } catch(Throwable a2) {
            a1.close();
            throw a2;
        }
        a1.close();
        return a0;
    }
    
    public static byte[] readInputStreamToByteArray(java.io.InputStream a) {
        java.io.ByteArrayOutputStream a0 = new java.io.ByteArrayOutputStream();
        byte[] a1 = new byte[16384];
        while(true) {
            int i = a.read(a1);
            if (i == -1) {
                return a0.toByteArray();
            }
            a0.write(a1, 0, i);
        }
    }
    
    private static void traverseFiles(String s, boolean b, java.util.List a, com.navdy.service.library.util.IOUtils$OnFileTraversal a0) {
        java.io.File a1 = new java.io.File(s);
        if (a1.isDirectory()) {
            java.io.File[] a2 = a1.listFiles();
            if (a2 != null) {
                java.util.Arrays.sort((Object[])a2);
                int i = a2.length;
                Object a3 = a;
                Object a4 = a0;
                int i0 = 0;
                while(i0 < i) {
                    java.io.File a5 = a2[i0];
                    boolean b0 = a5.isDirectory();
                    label1: {
                        label0: {
                            if (b0) {
                                break label0;
                            }
                            if (((java.util.List)a3).contains(a5.getName())) {
                                break label0;
                            }
                            ((com.navdy.service.library.util.IOUtils$OnFileTraversal)a4).onFileTraversal(a5);
                            break label1;
                        }
                        if (b) {
                            com.navdy.service.library.util.IOUtils.traverseFiles(a5.getPath(), true, (java.util.List)a3, (com.navdy.service.library.util.IOUtils$OnFileTraversal)a4);
                        }
                    }
                    i0 = i0 + 1;
                }
            }
        }
    }
}
