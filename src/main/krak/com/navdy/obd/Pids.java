package com.navdy.obd;

public class Pids {
    final public static int CUSTOM_PIDS = 64;
    final public static int CUSTOM_PID_START = 256;
    final public static int DISTANCE_TO_EMPTY = 257;
    final public static int DISTANCE_TRAVELLED = 49;
    final public static int ENGINE_COOLANT_TEMPERATURE = 5;
    final public static int ENGINE_OIL_PRESSURE = 258;
    final public static int ENGINE_RPM = 12;
    final public static int ENGINE_TRIP_FUEL = 259;
    final public static int FUEL_LEVEL = 47;
    final public static int FUEL_SYSTEM_STATUS = 3;
    final public static int INSTANTANEOUS_FUEL_CONSUMPTION = 256;
    final public static int INTAKE_AIR_TEMPERATURE = 15;
    final public static int MANIFOLD_AIR_PRESSURE = 11;
    final public static int MASS_AIRFLOW = 16;
    final public static int MAX_PID = 320;
    final public static int THROTTLE_POSITION = 17;
    final public static int TOTAL_VEHICLE_DISTANCE = 260;
    final public static int VEHICLE_SPEED = 13;
    
    public Pids() {
    }
}
