package com.navdy.obd;

public class Pid implements android.os.Parcelable {
    final public static android.os.Parcelable$Creator CREATOR;
    final public static int NO_DATA = -2147483648;
    protected com.navdy.obd.Pid$DataType dataType;
    protected int id;
    protected String name;
    protected long timeStamp;
    protected com.navdy.obd.Units units;
    protected double value;
    
    static {
        CREATOR = (android.os.Parcelable$Creator)new com.navdy.obd.Pid$1();
    }
    
    public Pid(int i) {
        this(i, (String)null, 0.0, com.navdy.obd.Pid$DataType.INT, com.navdy.obd.Units.NONE);
    }
    
    public Pid(int i, String s) {
        this(i, s, 0.0, com.navdy.obd.Pid$DataType.INT, com.navdy.obd.Units.NONE);
    }
    
    public Pid(int i, String s, double d, com.navdy.obd.Pid$DataType a, com.navdy.obd.Units a0) {
        this(i, s, 0.0, com.navdy.obd.Pid$DataType.INT, com.navdy.obd.Units.NONE, 0L);
    }
    
    public Pid(int i, String s, double d, com.navdy.obd.Pid$DataType a, com.navdy.obd.Units a0, long j) {
        this.id = i;
        this.name = s;
        this.value = d;
        this.dataType = a;
        this.units = a0;
        this.timeStamp = j;
    }
    
    public Pid(android.os.Parcel a) {
        this(a.readInt(), a.readString(), a.readDouble(), com.navdy.obd.Pid$DataType.valueOf(a.readString()), com.navdy.obd.Units.valueOf(a.readString()), a.readLong());
    }
    
    public int describeContents() {
        return 0;
    }
    
    public boolean equals(Object a) {
        return (a instanceof com.navdy.obd.Pid) ? ((com.navdy.obd.Pid)a).id == this.id : (this).equals(a);
    }
    
    public com.navdy.obd.Pid$DataType getDataType() {
        return this.dataType;
    }
    
    public int getId() {
        return this.id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public long getTimeStamp() {
        return this.timeStamp;
    }
    
    public double getValue() {
        return this.value;
    }
    
    public void setTimeStamp(long j) {
        this.timeStamp = j;
    }
    
    public void setValue(double d) {
        this.value = d;
    }
    
    public String toString() {
        return new StringBuilder().append("Pid{id=").append(Integer.toHexString(this.id)).append((char)125).toString();
    }
    
    public void writeToParcel(android.os.Parcel a, int i) {
        a.writeInt(this.id);
        a.writeString(this.name);
        a.writeDouble(this.value);
        a.writeString(this.dataType.name());
        a.writeString(this.units.name());
        a.writeLong(this.timeStamp);
    }
}
