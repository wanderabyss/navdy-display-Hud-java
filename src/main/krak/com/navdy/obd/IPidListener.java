package com.navdy.obd;

abstract public interface IPidListener extends android.os.IInterface {
    abstract public void onConnectionStateChange(int arg);
    
    
    abstract public void pidsChanged(java.util.List arg);
    
    
    abstract public void pidsRead(java.util.List arg, java.util.List arg0);
}
