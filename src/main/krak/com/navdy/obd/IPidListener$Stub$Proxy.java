package com.navdy.obd;

class IPidListener$Stub$Proxy implements com.navdy.obd.IPidListener {
    private android.os.IBinder mRemote;
    
    IPidListener$Stub$Proxy(android.os.IBinder a) {
        this.mRemote = a;
    }
    
    public android.os.IBinder asBinder() {
        return this.mRemote;
    }
    
    public String getInterfaceDescriptor() {
        return "com.navdy.obd.IPidListener";
    }
    
    public void onConnectionStateChange(int i) {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.IPidListener");
            a.writeInt(i);
            this.mRemote.transact(2, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void pidsChanged(java.util.List a) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.obd.IPidListener");
            a0.writeTypedList(a);
            this.mRemote.transact(1, a0, a1, 0);
            a1.readException();
        } catch(Throwable a2) {
            a1.recycle();
            a0.recycle();
            throw a2;
        }
        a1.recycle();
        a0.recycle();
    }
    
    public void pidsRead(java.util.List a, java.util.List a0) {
        android.os.Parcel a1 = android.os.Parcel.obtain();
        android.os.Parcel a2 = android.os.Parcel.obtain();
        try {
            a1.writeInterfaceToken("com.navdy.obd.IPidListener");
            a1.writeTypedList(a);
            a1.writeTypedList(a0);
            this.mRemote.transact(3, a1, a2, 0);
            a2.readException();
        } catch(Throwable a3) {
            a2.recycle();
            a1.recycle();
            throw a3;
        }
        a2.recycle();
        a1.recycle();
    }
}
