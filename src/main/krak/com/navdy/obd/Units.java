package com.navdy.obd;


public enum Units {
    NONE(0),
    MILES_PER_HOUR(1),
    KILOMETERS_PER_HOUR(2);

    private int value;
    Units(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class Units extends Enum {
//    final private static com.navdy.obd.Units[] $VALUES;
//    final public static com.navdy.obd.Units KILOMETERS_PER_HOUR;
//    final public static com.navdy.obd.Units MILES_PER_HOUR;
//    final public static com.navdy.obd.Units NONE;
//    protected String abbreviation;
//    protected String description;
//    protected com.navdy.obd.Units$System system;
//    
//    static {
//        NONE = new com.navdy.obd.Units("NONE", 0);
//        MILES_PER_HOUR = new com.navdy.obd.Units("MILES_PER_HOUR", 1, com.navdy.obd.Units$System.US);
//        KILOMETERS_PER_HOUR = new com.navdy.obd.Units("KILOMETERS_PER_HOUR", 2, com.navdy.obd.Units$System.Metric);
//        com.navdy.obd.Units[] a = new com.navdy.obd.Units[3];
//        a[0] = NONE;
//        a[1] = MILES_PER_HOUR;
//        a[2] = KILOMETERS_PER_HOUR;
//        $VALUES = a;
//    }
//    
//    private Units(String s, int i) {
//        this(s, i, com.navdy.obd.Units$System.None);
//    }
//    
//    private Units(String s, int i, com.navdy.obd.Units$System a) {
//        this(s, i, "", "", a);
//    }
//    
//    private Units(String s, int i, String s0, String s1, com.navdy.obd.Units$System a) {
//        super(s, i);
//        this.abbreviation = s0;
//        this.description = s1;
//        this.system = a;
//    }
//    
//    static void localize(android.content.res.XmlResourceParser a) {
//        StringBuilder a0 = new StringBuilder();
//        int i = a.getEventType();
//        Object a1 = a;
//        com.navdy.obd.Units a2 = null;
//        while(i != 1) {
//            String s = ((android.content.res.XmlResourceParser)a1).getName();
//            if (i != 2) {
//                if (i != 3) {
//                    if (i == 4) {
//                        a0.append(((android.content.res.XmlResourceParser)a1).getText());
//                    }
//                } else {
//                    if (a2 != null) {
//                        String s0 = a0.toString();
//                        if ("abbreviation".equals(s)) {
//                            a2.setAbbreviation(s0);
//                        } else if ("description".equals(s)) {
//                            a2.setDescription(s0);
//                        }
//                    }
//                    if ("unit".equals(s)) {
//                        a2 = null;
//                    }
//                    a0.setLength(0);
//                }
//            } else if ("unit".equals(s)) {
//                a2 = com.navdy.obd.Units.valueOf(((android.content.res.XmlResourceParser)a1).getAttributeValue((String)null, "id"));
//            }
//            i = ((android.content.res.XmlResourceParser)a1).next();
//        }
//    }
//    
//    public static com.navdy.obd.Units valueOf(String s) {
//        return (com.navdy.obd.Units)Enum.valueOf(com.navdy.obd.Units.class, s);
//    }
//    
//    public static com.navdy.obd.Units[] values() {
//        return $VALUES.clone();
//    }
//    
//    public String getAbbreviation() {
//        return this.abbreviation;
//    }
//    
//    public String getDescription() {
//        return this.description;
//    }
//    
//    public com.navdy.obd.Units$System getSystem() {
//        return this.system;
//    }
//    
//    public void setAbbreviation(String s) {
//        this.abbreviation = s;
//    }
//    
//    public void setDescription(String s) {
//        this.description = s;
//    }
//}
//