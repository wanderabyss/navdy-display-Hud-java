package com.navdy.obd;

abstract public class ICanBusMonitoringListener$Stub extends android.os.Binder implements com.navdy.obd.ICanBusMonitoringListener {
    final private static String DESCRIPTOR = "com.navdy.obd.ICanBusMonitoringListener";
    final static int TRANSACTION_getGpsSpeed = 4;
    final static int TRANSACTION_getLatitude = 5;
    final static int TRANSACTION_getLongitude = 6;
    final static int TRANSACTION_getMake = 8;
    final static int TRANSACTION_getModel = 9;
    final static int TRANSACTION_getYear = 10;
    final static int TRANSACTION_isMonitoringLimitReached = 7;
    final static int TRANSACTION_onCanBusMonitorSuccess = 3;
    final static int TRANSACTION_onCanBusMonitoringError = 2;
    final static int TRANSACTION_onNewDataAvailable = 1;
    
    public ICanBusMonitoringListener$Stub() {
        this.attachInterface((android.os.IInterface)this, "com.navdy.obd.ICanBusMonitoringListener");
    }
    
    public static com.navdy.obd.ICanBusMonitoringListener asInterface(android.os.IBinder a) {
        Object a0 = null;
        label1: if (a != null) {
            android.os.IInterface a1 = a.queryLocalInterface("com.navdy.obd.ICanBusMonitoringListener");
            label0: {
                if (a1 == null) {
                    break label0;
                }
                if (!(a1 instanceof com.navdy.obd.ICanBusMonitoringListener)) {
                    break label0;
                }
                a0 = a1;
                break label1;
            }
            a0 = new com.navdy.obd.ICanBusMonitoringListener$Stub$Proxy(a);
        } else {
            a0 = null;
        }
        return (com.navdy.obd.ICanBusMonitoringListener)a0;
    }
    
    public android.os.IBinder asBinder() {
        return (android.os.IBinder)this;
    }
    
    public boolean onTransact(int i, android.os.Parcel a, android.os.Parcel a0, int i0) {
        boolean b = false;
        switch(i) {
            case 1598968902: {
                a0.writeString("com.navdy.obd.ICanBusMonitoringListener");
                b = true;
                break;
            }
            case 10: {
                a.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                String s = this.getYear();
                a0.writeNoException();
                a0.writeString(s);
                b = true;
                break;
            }
            case 9: {
                a.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                String s0 = this.getModel();
                a0.writeNoException();
                a0.writeString(s0);
                b = true;
                break;
            }
            case 8: {
                a.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                String s1 = this.getMake();
                a0.writeNoException();
                a0.writeString(s1);
                b = true;
                break;
            }
            case 7: {
                a.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                boolean b0 = this.isMonitoringLimitReached();
                a0.writeNoException();
                a0.writeInt(b0 ? 1 : 0);
                b = true;
                break;
            }
            case 6: {
                a.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                double d = this.getLongitude();
                a0.writeNoException();
                a0.writeDouble(d);
                b = true;
                break;
            }
            case 5: {
                a.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                double d0 = this.getLatitude();
                a0.writeNoException();
                a0.writeDouble(d0);
                b = true;
                break;
            }
            case 4: {
                a.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                int i1 = this.getGpsSpeed();
                a0.writeNoException();
                a0.writeInt(i1);
                b = true;
                break;
            }
            case 3: {
                a.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                this.onCanBusMonitorSuccess(a.readInt());
                a0.writeNoException();
                b = true;
                break;
            }
            case 2: {
                a.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                this.onCanBusMonitoringError(a.readString());
                a0.writeNoException();
                b = true;
                break;
            }
            case 1: {
                a.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                this.onNewDataAvailable(a.readString());
                a0.writeNoException();
                b = true;
                break;
            }
            default: {
                b = super.onTransact(i, a, a0, i0);
            }
        }
        return b;
    }
}
