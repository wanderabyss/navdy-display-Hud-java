package com.navdy.hud.device.connection;

public class iAP2Link implements com.navdy.service.library.device.link.Link, com.navdy.hud.mfi.LinkLayer$PhysicalLayer, com.navdy.hud.mfi.iAPProcessor$StateListener, com.navdy.hud.mfi.IAPCommunicationsUpdateListener {
    public static com.navdy.hud.mfi.EASession proxyEASession;
    final private static com.navdy.service.library.log.Logger sLogger;
    private int bandwidthLevel;
    private com.navdy.hud.device.connection.iAP2Link$ConnectedThread connectedThread;
    private com.navdy.service.library.events.callcontrol.PhoneEvent currentPhoneEvent;
    private com.navdy.service.library.device.link.WriterThread$EventProcessor eventProcessor;
    private java.util.Map eventProcessors;
    private com.navdy.hud.mfi.IAPCommunicationsManager iAPCommunicationsManager;
    private com.navdy.hud.mfi.iAPProcessor iAPProcessor;
    private com.navdy.service.library.events.callcontrol.CallAction lastCallActionRequested;
    private com.navdy.hud.mfi.LinkLayer linkLayer;
    private com.navdy.service.library.device.link.LinkListener linkListener;
    final private android.bluetooth.BluetoothAdapter mAdapter;
    private com.navdy.hud.device.connection.iAP2MusicHelper musicHelper;
    private com.navdy.hud.device.connection.iAP2Link$ProxyStream proxyStream;
    private com.navdy.service.library.device.link.ReaderThread readerThread;
    private com.squareup.wire.Wire wire;
    private com.navdy.service.library.device.link.WriterThread writerThread;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.device.connection.iAP2Link.class);
    }
    
    public iAP2Link(com.navdy.hud.mfi.IAPListener a, android.content.Context a0) {
        this.bandwidthLevel = 1;
        this.eventProcessors = (java.util.Map)new java.util.HashMap();
        this.eventProcessor = (com.navdy.service.library.device.link.WriterThread$EventProcessor)new com.navdy.hud.device.connection.iAP2Link$1(this);
        this.proxyStream = new com.navdy.hud.device.connection.iAP2Link$ProxyStream(this, (com.navdy.hud.device.connection.iAP2Link$1)null);
        this.mAdapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
        this.iAPProcessor = new com.navdy.hud.mfi.iAPProcessor(a, a0);
        this.iAPProcessor.setAccessoryName(this.getName());
        this.iAPCommunicationsManager = new com.navdy.hud.mfi.IAPCommunicationsManager(this.iAPProcessor);
        this.musicHelper = new com.navdy.hud.device.connection.iAP2MusicHelper(this, this.iAPProcessor);
        this.linkLayer = new com.navdy.hud.mfi.LinkLayer();
        this.iAPProcessor.connect(this.linkLayer);
        this.iAPProcessor.connect((com.navdy.hud.mfi.iAPProcessor$StateListener)this);
        this.linkLayer.connect(this.iAPProcessor);
        this.linkLayer.connect((com.navdy.hud.mfi.LinkLayer$PhysicalLayer)this);
        Class[] a1 = new Class[1];
        a1[0] = com.navdy.service.library.events.Ext_NavdyEvent.class;
        this.wire = new com.squareup.wire.Wire(a1);
        this.addEventProcessors();
    }
    
    static java.util.Map access$000(com.navdy.hud.device.connection.iAP2Link a) {
        return a.eventProcessors;
    }
    
    static com.squareup.wire.Wire access$100(com.navdy.hud.device.connection.iAP2Link a) {
        return a.wire;
    }
    
    static com.navdy.hud.mfi.LinkLayer access$1000(com.navdy.hud.device.connection.iAP2Link a) {
        return a.linkLayer;
    }
    
    static com.navdy.service.library.log.Logger access$200() {
        return sLogger;
    }
    
    static com.navdy.service.library.events.callcontrol.CallAction access$302(com.navdy.hud.device.connection.iAP2Link a, com.navdy.service.library.events.callcontrol.CallAction a0) {
        a.lastCallActionRequested = a0;
        return a0;
    }
    
    static com.navdy.hud.mfi.IAPCommunicationsManager access$400(com.navdy.hud.device.connection.iAP2Link a) {
        return a.iAPCommunicationsManager;
    }
    
    static com.navdy.service.library.events.callcontrol.PhoneEvent access$500(com.navdy.hud.device.connection.iAP2Link a) {
        return a.currentPhoneEvent;
    }
    
    static com.navdy.hud.mfi.iAPProcessor access$600(com.navdy.hud.device.connection.iAP2Link a) {
        return a.iAPProcessor;
    }
    
    static android.bluetooth.BluetoothDevice access$800(com.navdy.hud.device.connection.iAP2Link a, com.navdy.service.library.network.SocketAdapter a0) {
        return a.getRemoteDevice(a0);
    }
    
    static com.navdy.service.library.device.link.LinkListener access$900(com.navdy.hud.device.connection.iAP2Link a) {
        return a.linkListener;
    }
    
    private void addEventProcessors() {
        this.eventProcessors.put(com.navdy.service.library.events.NavdyEvent$MessageType.TelephonyRequest, new com.navdy.hud.device.connection.iAP2Link$2(this));
        this.eventProcessors.put(com.navdy.service.library.events.NavdyEvent$MessageType.PhoneStatusRequest, new com.navdy.hud.device.connection.iAP2Link$3(this));
        this.eventProcessors.put(com.navdy.service.library.events.NavdyEvent$MessageType.LaunchAppEvent, new com.navdy.hud.device.connection.iAP2Link$4(this));
        this.eventProcessors.put(com.navdy.service.library.events.NavdyEvent$MessageType.CallStateUpdateRequest, new com.navdy.hud.device.connection.iAP2Link$5(this));
    }
    
    private android.bluetooth.BluetoothDevice getRemoteDevice(com.navdy.service.library.network.SocketAdapter a) {
        com.navdy.service.library.device.NavdyDeviceId a0 = a.getRemoteDevice();
        return this.mAdapter.getRemoteDevice(a0.getBluetoothAddress());
    }
    
    private boolean startProtobufLink(com.navdy.service.library.network.SocketAdapter a) {
        boolean b = false;
        label1: {
            java.io.InputStream a0 = null;
            label0: {
                try {
                    a0 = null;
                    a0 = a.getInputStream();
                    java.io.OutputStream a1 = a.getOutputStream();
                    this.readerThread = new com.navdy.service.library.device.link.ReaderThread(com.navdy.service.library.device.connection.ConnectionType.EA_PROTOBUF, a0, this.linkListener, false);
                    this.proxyStream.setOutputStream(a1);
                } catch(java.io.IOException ignoredException) {
                    break label0;
                }
                this.readerThread.start();
                b = true;
                break label1;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            this.readerThread = null;
            b = false;
        }
        return b;
    }
    
    private void stopProtobufLink() {
        if (this.readerThread != null) {
            this.readerThread.cancel();
            this.readerThread = null;
        }
        this.proxyStream.setOutputStream((java.io.OutputStream)null);
    }
    
    void addEventProcessor(com.navdy.service.library.events.NavdyEvent$MessageType a, com.navdy.hud.device.connection.iAP2Link$NavdyEventProcessor a0) {
        if (this.eventProcessors.containsKey(a)) {
            sLogger.w("Already have a processor for that type!");
        }
        this.eventProcessors.put(a, a0);
    }
    
    public void close() {
        if (this.musicHelper != null) {
            this.musicHelper.close();
        }
        if (this.connectedThread != null) {
            this.connectedThread.cancel();
            this.connectedThread = null;
        }
        if (this.writerThread != null) {
            this.writerThread.cancel();
            this.writerThread = null;
        }
        if (this.readerThread != null) {
            this.readerThread.cancel();
            this.readerThread = null;
        }
    }
    
    public int getBandWidthLevel() {
        return this.bandwidthLevel;
    }
    
    public byte[] getLocalAddress() {
        return com.navdy.hud.mfi.Utils.parseMACAddress(this.mAdapter.getAddress());
    }
    
    public String getName() {
        return this.mAdapter.getName();
    }
    
    public void onCallStateUpdate(com.navdy.hud.mfi.CallStateUpdate a) {
        com.navdy.service.library.events.callcontrol.PhoneEvent a0 = com.navdy.hud.device.connection.IAPMessageUtility.getPhoneEventForCallStateUpdate(a, this.lastCallActionRequested);
        if (a0 != null) {
            this.currentPhoneEvent = a0;
            this.lastCallActionRequested = null;
            this.sendMessageAsNavdyEvent((com.squareup.wire.Message)this.currentPhoneEvent);
        }
    }
    
    public void onCommunicationUpdate(com.navdy.hud.mfi.CommunicationUpdate a) {
        sLogger.e(new StringBuilder().append("CommunicationUpdate : ").append(a).toString());
    }
    
    public void onError() {
        sLogger.e("Error in IAP session");
    }
    
    public void onReady() {
        try {
            this.iAPCommunicationsManager.setUpdatesListener((com.navdy.hud.mfi.IAPCommunicationsUpdateListener)this);
            this.iAPCommunicationsManager.startUpdates();
            this.iAPCommunicationsManager.startCommunicationUpdates();
            this.musicHelper.onReady();
            this.iAPProcessor.startHIDSession();
        } catch(Throwable a) {
            sLogger.e("Error starting the call state updates ", a);
        }
    }
    
    public void onSessionStart(com.navdy.hud.mfi.EASession a) {
        String s = a.getProtocol();
        if (s.equals("com.navdy.hud.api.v1")) {
            this.startProtobufLink((com.navdy.service.library.network.SocketAdapter)new com.navdy.hud.device.connection.EASessionSocketAdapter(a));
        } else if (s.equals("com.navdy.hud.proxy.v1")) {
            proxyEASession = a;
            if (this.linkListener != null) {
                this.linkListener.onNetworkLinkReady();
            }
        }
    }
    
    public void onSessionStop(com.navdy.hud.mfi.EASession a) {
        String s = a.getProtocol();
        if (s.equals("com.navdy.hud.api.v1")) {
            this.stopProtobufLink();
        } else if (s.equals("com.navdy.hud.proxy.v1")) {
            proxyEASession = null;
        }
        a.close();
    }
    
    public void queue(com.navdy.hud.mfi.LinkPacket a) {
        if (this.connectedThread != null) {
            this.connectedThread.write(a.data);
        }
    }
    
    void sendMessageAsNavdyEvent(com.squareup.wire.Message a) {
        if (a != null && this.linkListener != null) {
            com.navdy.service.library.events.NavdyEvent a0 = com.navdy.service.library.events.NavdyEventUtil.eventFromMessage(a);
            this.linkListener.onNavdyEventReceived(a0.toByteArray());
        }
    }
    
    public void setBandwidthLevel(int i) {
        this.bandwidthLevel = i;
        this.musicHelper.setBandwidthLevel(i);
    }
    
    public boolean start(com.navdy.service.library.network.SocketAdapter a, java.util.concurrent.LinkedBlockingDeque a0, com.navdy.service.library.device.link.LinkListener a1) {
        com.navdy.service.library.device.link.WriterThread a2 = this.writerThread;
        label0: {
            label1: {
                label2: {
                    if (a2 == null) {
                        break label2;
                    }
                    if (!this.writerThread.isClosing()) {
                        break label1;
                    }
                }
                if (this.readerThread == null) {
                    break label0;
                }
                if (this.readerThread.isClosing()) {
                    break label0;
                }
            }
            throw new IllegalStateException("Must stop threads before calling startLink");
        }
        this.linkListener = a1;
        this.connectedThread = new com.navdy.hud.device.connection.iAP2Link$ConnectedThread(this, a);
        this.writerThread = new com.navdy.service.library.device.link.WriterThread(a0, (java.io.OutputStream)this.proxyStream, this.eventProcessor);
        this.connectedThread.start();
        this.writerThread.start();
        return true;
    }
}
