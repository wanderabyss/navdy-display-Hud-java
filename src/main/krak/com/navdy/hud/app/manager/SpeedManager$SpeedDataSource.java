package com.navdy.hud.app.manager;


public enum SpeedManager$SpeedDataSource {
    GPS(0),
    OBD(1);

    private int value;
    SpeedManager$SpeedDataSource(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class SpeedManager$SpeedDataSource extends Enum {
//    final private static com.navdy.hud.app.manager.SpeedManager$SpeedDataSource[] $VALUES;
//    final public static com.navdy.hud.app.manager.SpeedManager$SpeedDataSource GPS;
//    final public static com.navdy.hud.app.manager.SpeedManager$SpeedDataSource OBD;
//    
//    static {
//        GPS = new com.navdy.hud.app.manager.SpeedManager$SpeedDataSource("GPS", 0);
//        OBD = new com.navdy.hud.app.manager.SpeedManager$SpeedDataSource("OBD", 1);
//        com.navdy.hud.app.manager.SpeedManager$SpeedDataSource[] a = new com.navdy.hud.app.manager.SpeedManager$SpeedDataSource[2];
//        a[0] = GPS;
//        a[1] = OBD;
//        $VALUES = a;
//    }
//    
//    private SpeedManager$SpeedDataSource(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.manager.SpeedManager$SpeedDataSource valueOf(String s) {
//        return (com.navdy.hud.app.manager.SpeedManager$SpeedDataSource)Enum.valueOf(com.navdy.hud.app.manager.SpeedManager$SpeedDataSource.class, s);
//    }
//    
//    public static com.navdy.hud.app.manager.SpeedManager$SpeedDataSource[] values() {
//        return $VALUES.clone();
//    }
//}
//