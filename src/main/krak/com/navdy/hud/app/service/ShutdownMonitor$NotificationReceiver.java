package com.navdy.hud.app.service;
import com.squareup.otto.Subscribe;

class ShutdownMonitor$NotificationReceiver {
    final com.navdy.hud.app.service.ShutdownMonitor this$0;
    
    private ShutdownMonitor$NotificationReceiver(com.navdy.hud.app.service.ShutdownMonitor a) {
        super();
        this.this$0 = a;
    }
    
    ShutdownMonitor$NotificationReceiver(com.navdy.hud.app.service.ShutdownMonitor a, com.navdy.hud.app.service.ShutdownMonitor$1 a0) {
        this(a);
    }
    
    static void access$1300(com.navdy.hud.app.service.ShutdownMonitor$NotificationReceiver a, com.navdy.hud.app.event.Shutdown a0) {
        a.triggerShutdown(a0);
    }
    
    private void triggerShutdown(com.navdy.hud.app.event.Shutdown a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.service.ShutdownMonitor$NotificationReceiver$ShutdownRunnable(this, a), 1);
    }
    
    public void ObdStateChangeEvent(com.navdy.hud.app.obd.ObdManager$ObdConnectionStatusEvent a) {
        if (a.connected) {
            com.navdy.hud.app.service.ShutdownMonitor.access$1502(this.this$0, 0L);
            com.navdy.hud.app.service.ShutdownMonitor.access$1602(this.this$0, 0L);
        } else {
            com.navdy.hud.app.service.ShutdownMonitor.access$1502(this.this$0, com.navdy.hud.app.service.ShutdownMonitor.access$1700(this.this$0));
            com.navdy.hud.app.service.ShutdownMonitor.access$700(this.this$0).removeCallbacks((Runnable)this.this$0);
            com.navdy.hud.app.service.ShutdownMonitor.access$700(this.this$0).postDelayed((Runnable)this.this$0, com.navdy.hud.app.service.ShutdownMonitor.access$1800());
        }
    }
    
    @Subscribe
    public void onAccelerateShutdown(com.navdy.service.library.events.hudcontrol.AccelerateShutdown a) {
        com.navdy.service.library.log.Logger a0 = com.navdy.hud.app.service.ShutdownMonitor.access$100();
        Object[] a1 = new Object[1];
        a1[0] = a.reason;
        a0.v(String.format("received AccelerateShutdown, reason = %s", a1));
        com.navdy.hud.app.service.ShutdownMonitor.access$1602(this.this$0, com.navdy.hud.app.service.ShutdownMonitor.access$1700(this.this$0));
    }
    
    @Subscribe
    public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        if (a.state != com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_VERIFIED) {
            if (a.state == com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_DISCONNECTED) {
                com.navdy.hud.app.service.ShutdownMonitor.access$1902(this.this$0, 0L);
            }
        } else {
            com.navdy.hud.app.service.ShutdownMonitor.access$1902(this.this$0, com.navdy.hud.app.service.ShutdownMonitor.access$1700(this.this$0));
            com.navdy.hud.app.service.ShutdownMonitor.access$1602(this.this$0, 0L);
            com.navdy.hud.app.service.ShutdownMonitor.access$2000(this.this$0);
        }
    }
    
    @Subscribe
    public void onDrivingStateChange(com.navdy.hud.app.event.DrivingStateChange a) {
        com.navdy.hud.app.service.ShutdownMonitor.access$2102(this.this$0, a.driving);
        if (com.navdy.hud.app.service.ShutdownMonitor.access$2100(this.this$0)) {
            com.navdy.hud.app.service.ShutdownMonitor.access$2200(this.this$0);
        }
    }
    
    @Subscribe
    public void onShutdown(com.navdy.hud.app.event.Shutdown a) {
        switch(com.navdy.hud.app.service.ShutdownMonitor$4.$SwitchMap$com$navdy$hud$app$event$Shutdown$State[a.state.ordinal()]) {
            case 2: {
                com.navdy.hud.app.service.ShutdownMonitor.access$700(this.this$0).post((Runnable)new com.navdy.hud.app.service.ShutdownMonitor$NotificationReceiver$1(this, a));
                break;
            }
            case 1: {
                com.navdy.hud.app.service.ShutdownMonitor.access$1200(this.this$0);
                break;
            }
        }
    }
    
    @Subscribe
    public void onWakeup(com.navdy.hud.app.event.Wakeup a) {
        com.navdy.hud.app.service.ShutdownMonitor.access$1400(this.this$0);
    }
}
