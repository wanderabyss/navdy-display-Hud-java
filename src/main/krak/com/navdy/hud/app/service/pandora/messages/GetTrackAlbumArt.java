package com.navdy.hud.app.service.pandora.messages;

public class GetTrackAlbumArt extends com.navdy.hud.app.service.pandora.messages.BaseOutgoingConstantMessage {
    final public static com.navdy.hud.app.service.pandora.messages.GetTrackAlbumArt INSTANCE;
    
    static {
        INSTANCE = new com.navdy.hud.app.service.pandora.messages.GetTrackAlbumArt();
    }
    
    private GetTrackAlbumArt() {
    }
    
    public byte[] buildPayload() {
        return super.buildPayload();
    }
    
    protected java.io.ByteArrayOutputStream putThis(java.io.ByteArrayOutputStream a) {
        com.navdy.hud.app.service.pandora.messages.GetTrackAlbumArt.putByte(a, (byte)20);
        com.navdy.hud.app.service.pandora.messages.GetTrackAlbumArt.putInt(a, 9025);
        return a;
    }
    
    public String toString() {
        return "Requesting album artwork";
    }
}
