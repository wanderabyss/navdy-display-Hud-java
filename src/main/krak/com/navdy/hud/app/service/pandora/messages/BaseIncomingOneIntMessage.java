package com.navdy.hud.app.service.pandora.messages;

public class BaseIncomingOneIntMessage extends com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage {
    private static int MESSAGE_LENGTH;
    private static com.navdy.service.library.log.Logger sLogger;
    public int value;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.service.pandora.messages.BaseIncomingOneIntMessage.class);
        MESSAGE_LENGTH = 5;
    }
    
    public BaseIncomingOneIntMessage(int i) {
        this.value = i;
    }
    
    protected static int parseIntValue(byte[] a) {
        if (a.length != MESSAGE_LENGTH) {
            throw new com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException();
        }
        return java.nio.ByteBuffer.wrap(a).getInt(1);
    }
    
    public String toString() {
        sLogger.w("toString not overwritten in BaseIncomingOneIntMessage class");
        return new StringBuilder().append("One int message received with value: ").append(this.value).toString();
    }
}
