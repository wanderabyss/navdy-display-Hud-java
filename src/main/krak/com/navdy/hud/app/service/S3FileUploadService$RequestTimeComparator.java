package com.navdy.hud.app.service;

public class S3FileUploadService$RequestTimeComparator implements java.util.Comparator {
    public S3FileUploadService$RequestTimeComparator() {
    }
    
    public int compare(com.navdy.hud.app.service.S3FileUploadService$Request a, com.navdy.hud.app.service.S3FileUploadService$Request a0) {
        return Long.compare(a0.file.lastModified(), a.file.lastModified());
    }
    
    public int compare(Object a, Object a0) {
        return this.compare((com.navdy.hud.app.service.S3FileUploadService$Request)a, (com.navdy.hud.app.service.S3FileUploadService$Request)a0);
    }
}
