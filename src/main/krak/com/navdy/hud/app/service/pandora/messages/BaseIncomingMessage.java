package com.navdy.hud.app.service.pandora.messages;

abstract public class BaseIncomingMessage extends com.navdy.hud.app.service.pandora.messages.BaseMessage {
    public BaseIncomingMessage() {
    }
    
    public static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage buildFromPayload(byte[] a) {
        com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage a0 = null;
        int i = a[0];
        switch(i) {
            case -70: {
                a0 = com.navdy.hud.app.service.pandora.messages.UpdateStationActive.innerBuildFromPayload(a);
                break;
            }
            case -98: {
                a0 = com.navdy.hud.app.service.pandora.messages.UpdateTrackCompleted.innerBuildFromPayload(a);
                break;
            }
            case -99: {
                a0 = com.navdy.hud.app.service.pandora.messages.ReturnTrackInfoExtended.innerBuildFromPayload(a);
                break;
            }
            case -105: {
                a0 = com.navdy.hud.app.service.pandora.messages.UpdateTrackElapsed.innerBuildFromPayload(a);
                break;
            }
            case -106: {
                a0 = com.navdy.hud.app.service.pandora.messages.UpdateTrackAlbumArt.innerBuildFromPayload(a);
                break;
            }
            case -107: {
                a0 = com.navdy.hud.app.service.pandora.messages.ReturnTrackAlbumArtSegment.innerBuildFromPayload(a);
                break;
            }
            case -112: {
                a0 = com.navdy.hud.app.service.pandora.messages.UpdateTrack.innerBuildFromPayload(a);
                break;
            }
            case -127: {
                a0 = com.navdy.hud.app.service.pandora.messages.UpdateStatus.innerBuildFromPayload(a);
                break;
            }
            default: {
                int i0 = a[0];
                throw new com.navdy.hud.app.service.pandora.exceptions.UnsupportedMessageReceivedException((byte)i0);
            }
        }
        return a0;
    }
    
    protected static boolean getBoolean(java.nio.ByteBuffer a) {
        int i = a.get();
        return i != 0;
    }
    
    protected static String getFixedLengthASCIIString(java.nio.ByteBuffer a, int i) {
        java.io.ByteArrayOutputStream a0 = null;
        int i0 = 0;
        while(true) {
            String s = null;
            label0: if (i0 < i) {
                int i1 = a.get();
                label1: {
                    if (i1 != 0) {
                        break label1;
                    }
                    if (a0 != null) {
                        break label0;
                    }
                }
                if (i1 != 0) {
                    if (a0 == null) {
                        a0 = new java.io.ByteArrayOutputStream();
                    }
                    a0.write(i1);
                }
                i0 = i0 + 1;
                continue;
            }
            if (a0 == null) {
                s = "";
            } else {
                s = new String(a0.toByteArray(), FIXED_LENGTH_STRING_ENCODING);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            }
            return s;
        }
    }
    
    protected static String getString(java.nio.ByteBuffer a) {
        java.io.ByteArrayOutputStream a0 = new java.io.ByteArrayOutputStream();
        int i = a.get();
        int i0 = 0;
        while(i != 0) {
            if (i < 0) {
                throw new com.navdy.hud.app.service.pandora.exceptions.UnterminatedStringException();
            }
            a0.write(i);
            i0 = i0 + 1;
            if (i0 > 247) {
                throw new com.navdy.hud.app.service.pandora.exceptions.StringOverflowException();
            }
            i = a.get();
        }
        return new String(a0.toByteArray(), STRING_ENCODING);
    }
    
    protected static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage innerBuildFromPayload(byte[] a) {
        throw new com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException();
    }
}
