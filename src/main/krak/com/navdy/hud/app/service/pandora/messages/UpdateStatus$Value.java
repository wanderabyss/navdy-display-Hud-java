package com.navdy.hud.app.service.pandora.messages;


public enum UpdateStatus$Value {
    PLAYING(0),
    PAUSED(1),
    INCOMPATIBLE_API_VERSION(2),
    UNKNOWN_ERROR(3),
    NO_STATIONS(4),
    NO_STATION_ACTIVE(5),
    INSUFFICIENT_CONNECTIVITY(6),
    LISTENING_RESTRICTIONS(7),
    INVALID_LOGIN(8);

    private int value;
    UpdateStatus$Value(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class UpdateStatus$Value extends Enum {
//    final private static com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value[] $VALUES;
//    final public static com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value INCOMPATIBLE_API_VERSION;
//    final public static com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value INSUFFICIENT_CONNECTIVITY;
//    final public static com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value INVALID_LOGIN;
//    final public static com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value LISTENING_RESTRICTIONS;
//    final public static com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value NO_STATIONS;
//    final public static com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value NO_STATION_ACTIVE;
//    final public static com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value PAUSED;
//    final public static com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value PLAYING;
//    final public static com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value UNKNOWN_ERROR;
//    
//    static {
//        PLAYING = new com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value("PLAYING", 0);
//        PAUSED = new com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value("PAUSED", 1);
//        INCOMPATIBLE_API_VERSION = new com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value("INCOMPATIBLE_API_VERSION", 2);
//        UNKNOWN_ERROR = new com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value("UNKNOWN_ERROR", 3);
//        NO_STATIONS = new com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value("NO_STATIONS", 4);
//        NO_STATION_ACTIVE = new com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value("NO_STATION_ACTIVE", 5);
//        INSUFFICIENT_CONNECTIVITY = new com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value("INSUFFICIENT_CONNECTIVITY", 6);
//        LISTENING_RESTRICTIONS = new com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value("LISTENING_RESTRICTIONS", 7);
//        INVALID_LOGIN = new com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value("INVALID_LOGIN", 8);
//        com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value[] a = new com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value[9];
//        a[0] = PLAYING;
//        a[1] = PAUSED;
//        a[2] = INCOMPATIBLE_API_VERSION;
//        a[3] = UNKNOWN_ERROR;
//        a[4] = NO_STATIONS;
//        a[5] = NO_STATION_ACTIVE;
//        a[6] = INSUFFICIENT_CONNECTIVITY;
//        a[7] = LISTENING_RESTRICTIONS;
//        a[8] = INVALID_LOGIN;
//        $VALUES = a;
//    }
//    
//    private UpdateStatus$Value(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value valueOf(String s) {
//        return (com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value)Enum.valueOf(com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value.class, s);
//    }
//    
//    public static com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value[] values() {
//        return $VALUES.clone();
//    }
//}
//