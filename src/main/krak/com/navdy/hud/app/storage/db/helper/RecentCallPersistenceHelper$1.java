package com.navdy.hud.app.storage.db.helper;

final class RecentCallPersistenceHelper$1 implements java.util.Comparator {
    RecentCallPersistenceHelper$1() {
    }
    
    public int compare(com.navdy.hud.app.framework.recentcall.RecentCall a, com.navdy.hud.app.framework.recentcall.RecentCall a0) {
        return a0.compareTo(a);
    }
    
    public int compare(Object a, Object a0) {
        return this.compare((com.navdy.hud.app.framework.recentcall.RecentCall)a, (com.navdy.hud.app.framework.recentcall.RecentCall)a0);
    }
}
