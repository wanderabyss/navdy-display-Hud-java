package com.navdy.hud.app.storage.cache;

class DiskLruCache$1 implements java.util.Comparator {
    final com.navdy.hud.app.storage.cache.DiskLruCache this$0;
    
    DiskLruCache$1(com.navdy.hud.app.storage.cache.DiskLruCache a) {
        super();
        this.this$0 = a;
    }
    
    public int compare(java.io.File a, java.io.File a0) {
        return Long.compare(a.lastModified(), a0.lastModified());
    }
    
    public int compare(Object a, Object a0) {
        return this.compare((java.io.File)a, (java.io.File)a0);
    }
}
