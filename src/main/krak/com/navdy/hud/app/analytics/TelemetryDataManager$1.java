package com.navdy.hud.app.analytics;

final public class TelemetryDataManager$1 implements com.navdy.hud.app.analytics.TelemetrySession$DataSource {
    final com.navdy.hud.app.analytics.TelemetryDataManager this$0;
    
    TelemetryDataManager$1(com.navdy.hud.app.analytics.TelemetryDataManager a) {
        super();
        this.this$0 = a;
    }
    
    public long absoluteCurrentTime() {
        return android.os.SystemClock.elapsedRealtime();
    }
    
    public com.navdy.hud.app.analytics.RawSpeed currentSpeed() {
        com.navdy.hud.app.analytics.RawSpeed a = com.navdy.hud.app.manager.SpeedManager.getInstance().getCurrentSpeedInMetersPerSecond();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a, "SpeedManager.getInstance\u2026entSpeedInMetersPerSecond");
        return a;
    }
    
    public void interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "event");
        this.this$0.updateDriveScore(a);
    }
    
    public boolean isHighAccuracySpeedAvailable() {
        return this.this$0.getHighAccuracySpeedAvailable();
    }
    
    public boolean isVerboseLoggingNeeded() {
        boolean b = false;
        boolean b0 = com.navdy.hud.app.util.DeviceUtil.isUserBuild();
        label2: {
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if (!kotlin.jvm.internal.Intrinsics.areEqual(com.navdy.hud.app.analytics.TelemetryDataManager.access$getUiStateManager$p(this.this$0).getHomescreenView().getDisplayMode(), com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.SMART_DASH)) {
                        break label1;
                    }
                    if (com.navdy.hud.app.analytics.TelemetryDataManager.access$getUiStateManager$p(this.this$0).getSmartDashView().isShowingDriveScoreGauge) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public long totalDistanceTravelledWithMeters() {
        return com.navdy.hud.app.analytics.TelemetryDataManager.access$getTripManager$p(this.this$0).getTotalDistanceTravelledWithNavdy();
    }
}
