package com.navdy.hud.app.analytics;

final public class TelemetryDataManager$DriveScoreUpdated {
    private int driveScore;
    private com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent interestingEvent;
    private boolean isDoingHighGManeuver;
    private boolean isExcessiveSpeeding;
    private boolean isSpeeding;
    
    public TelemetryDataManager$DriveScoreUpdated(com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent a, boolean b, boolean b0, boolean b1, int i) {
        super();
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "interestingEvent");
        this.interestingEvent = a;
        this.isSpeeding = b;
        this.isExcessiveSpeeding = b0;
        this.isDoingHighGManeuver = b1;
        this.driveScore = i;
    }
    
    public static com.navdy.hud.app.analytics.TelemetryDataManager$DriveScoreUpdated copy$default(com.navdy.hud.app.analytics.TelemetryDataManager$DriveScoreUpdated a, com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent a0, boolean b, boolean b0, boolean b1, int i, int i0, Object a1) {
        if ((i0 & 1) != 0) {
            a0 = a.interestingEvent;
        }
        if ((i0 & 2) != 0) {
            b = a.isSpeeding;
        }
        if ((i0 & 4) != 0) {
            b0 = a.isExcessiveSpeeding;
        }
        if ((i0 & 8) != 0) {
            b1 = a.isDoingHighGManeuver;
        }
        if ((i0 & 16) != 0) {
            i = a.driveScore;
        }
        return a.copy(a0, b, b0, b1, i);
    }
    
    final public com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent component1() {
        return this.interestingEvent;
    }
    
    final public boolean component2() {
        return this.isSpeeding;
    }
    
    final public boolean component3() {
        return this.isExcessiveSpeeding;
    }
    
    final public boolean component4() {
        return this.isDoingHighGManeuver;
    }
    
    final public int component5() {
        return this.driveScore;
    }
    
    final public com.navdy.hud.app.analytics.TelemetryDataManager$DriveScoreUpdated copy(com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent a, boolean b, boolean b0, boolean b1, int i) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "interestingEvent");
        return new com.navdy.hud.app.analytics.TelemetryDataManager$DriveScoreUpdated(a, b, b0, b1, i);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: {
            label1: {
                if (this == a) {
                    break label1;
                }
                if (!(a instanceof com.navdy.hud.app.analytics.TelemetryDataManager$DriveScoreUpdated)) {
                    b = false;
                    break label0;
                }
                com.navdy.hud.app.analytics.TelemetryDataManager$DriveScoreUpdated a0 = (com.navdy.hud.app.analytics.TelemetryDataManager$DriveScoreUpdated)a;
                if (!kotlin.jvm.internal.Intrinsics.areEqual(this.interestingEvent, a0.interestingEvent)) {
                    b = false;
                    break label0;
                }
                if (!(this.isSpeeding == a0.isSpeeding)) {
                    b = false;
                    break label0;
                }
                if (!(this.isExcessiveSpeeding == a0.isExcessiveSpeeding)) {
                    b = false;
                    break label0;
                }
                if (!(this.isDoingHighGManeuver == a0.isDoingHighGManeuver)) {
                    b = false;
                    break label0;
                }
                if (!(this.driveScore == a0.driveScore)) {
                    b = false;
                    break label0;
                }
            }
            b = true;
        }
        return b;
    }
    
    final public int getDriveScore() {
        return this.driveScore;
    }
    
    final public com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent getInterestingEvent() {
        return this.interestingEvent;
    }
    
    public int hashCode() {
        com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent a = this.interestingEvent;
        int i = (a == null) ? 0 : (a).hashCode();
        boolean b = this.isSpeeding;
        if (b) {
            b = true;
        }
        boolean b0 = this.isExcessiveSpeeding;
        if (b0) {
            b0 = true;
        }
        boolean b1 = this.isDoingHighGManeuver;
        if (b1) {
            b1 = true;
        }
        return (((((b0 + ((b + i * 31) ? 1 : 0) * 31) ? 1 : 0) * 31 != 0) + b1) ? 1 : 0) * 31 + this.driveScore;
    }
    
    final public boolean isDoingHighGManeuver() {
        return this.isDoingHighGManeuver;
    }
    
    final public boolean isExcessiveSpeeding() {
        return this.isExcessiveSpeeding;
    }
    
    final public boolean isSpeeding() {
        return this.isSpeeding;
    }
    
    final public void setDoingHighGManeuver(boolean b) {
        this.isDoingHighGManeuver = b;
    }
    
    final public void setDriveScore(int i) {
        this.driveScore = i;
    }
    
    final public void setExcessiveSpeeding(boolean b) {
        this.isExcessiveSpeeding = b;
    }
    
    final public void setInterestingEvent(com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.interestingEvent = a;
    }
    
    final public void setSpeeding(boolean b) {
        this.isSpeeding = b;
    }
    
    public String toString() {
        return new StringBuilder().append("DriveScoreUpdated(interestingEvent=").append(this.interestingEvent).append(", isSpeeding=").append(this.isSpeeding).append(", isExcessiveSpeeding=").append(this.isExcessiveSpeeding).append(", isDoingHighGManeuver=").append(this.isDoingHighGManeuver).append(", driveScore=").append(this.driveScore).append(")").toString();
    }
}
