package com.navdy.hud.app.analytics;

public class AnalyticsSupport$NotificationReceiver implements com.navdy.hud.app.ui.framework.INotificationAnimationListener {
    @Inject
    public android.content.SharedPreferences appPreferences;
    private com.here.android.mpa.routing.Maneuver lastRequiredManeuver;
    private com.navdy.hud.app.maps.MapEvents$ManeuverDisplay lastWarningEvent;
    @Inject
    public com.navdy.hud.app.device.PowerManager powerManager;
    
    AnalyticsSupport$NotificationReceiver() {
        this.lastWarningEvent = null;
        this.lastRequiredManeuver = null;
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().register(this);
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().addNotificationAnimationListener((com.navdy.hud.app.ui.framework.INotificationAnimationListener)this);
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
    }
    
    private boolean objEquals(Object a, Object a0) {
        boolean b = false;
        label2: {
            label0: {
                label1: {
                    if (a == a0) {
                        break label1;
                    }
                    if (a == null) {
                        break label0;
                    }
                    if (!a.equals(a0)) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    public void ObdStateChangeEvent(com.navdy.hud.app.obd.ObdManager$ObdConnectionStatusEvent a) {
        if (a.connected) {
            com.navdy.hud.app.analytics.AnalyticsSupport.access$1900();
        }
    }
    
    @Subscribe
    public void onArrived(com.navdy.hud.app.maps.MapEvents$ArrivalEvent a) {
        com.navdy.hud.app.analytics.AnalyticsSupport.access$2100(a);
    }
    
    @Subscribe
    public void onArrived(com.navdy.hud.app.maps.MapEvents$ManeuverEvent a) {
        if (a.type == com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type.LAST) {
            com.navdy.hud.app.analytics.AnalyticsSupport.access$000().v("NAVIGATION_ARRIVED");
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Navigation_Arrived", new String[0]);
        }
    }
    
    @Subscribe
    public void onBandwidthSettingChanged(com.navdy.hud.app.framework.network.NetworkBandwidthController$UserBandwidthSettingChanged a) {
        long j = com.navdy.hud.app.analytics.AnalyticsSupport.access$100();
        com.navdy.hud.app.analytics.AnalyticsSupport.access$000().v(new StringBuilder().append("uploader changed to ").append(j).toString());
        com.navdy.hud.app.analytics.AnalyticsSupport.access$200().removeCallbacks(com.navdy.hud.app.analytics.AnalyticsSupport.access$1800());
        com.navdy.hud.app.analytics.AnalyticsSupport.access$200().postDelayed(com.navdy.hud.app.analytics.AnalyticsSupport.access$1800(), j);
    }
    
    @Subscribe
    public void onConnectionStatusChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        if (a.state == com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_DISCONNECTED) {
            String[] a0 = new String[4];
            a0[0] = "Device_Id";
            a0[1] = android.os.Build.SERIAL;
            a0[2] = "Remote_Device_Id";
            a0[3] = com.navdy.hud.app.analytics.AnalyticsSupport.access$1400();
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Mobile_App_Disconnect", a0);
            com.navdy.hud.app.analytics.AnalyticsSupport.access$1500();
        }
    }
    
    @Subscribe
    public void onDateTimeAvailable(com.navdy.hud.app.common.TimeHelper$DateTimeAvailableEvent a) {
        com.navdy.hud.app.analytics.AnalyticsSupport.access$700();
    }
    
    @Subscribe
    public void onDeviceInfoAvailable(com.navdy.hud.app.event.DeviceInfoAvailable a) {
        if (a.deviceInfo != null) {
            com.navdy.hud.app.analytics.AnalyticsSupport.access$402(a.deviceInfo);
            com.navdy.hud.app.analytics.AnalyticsSupport.access$500();
        }
    }
    
    @Subscribe
    public void onDismissScreen(com.navdy.service.library.events.ui.DismissScreen a) {
    }
    
    @Subscribe
    public void onDriverProfileChange(com.navdy.hud.app.event.DriverProfileChanged a) {
        com.navdy.hud.app.profile.DriverProfile a0 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
        if (!a0.isDefaultProfile()) {
            com.navdy.hud.app.analytics.AnalyticsSupport.access$902(a0);
            com.navdy.hud.app.analytics.AnalyticsSupport.access$000().d("Remembering cached preferences");
            com.navdy.hud.app.analytics.AnalyticsSupport.access$1002(new com.navdy.hud.app.analytics.AnalyticsSupport$PreferenceWatcher(a0, (com.navdy.hud.app.analytics.AnalyticsSupport$PreferenceAdapter)new com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver$2(this)));
            com.navdy.hud.app.analytics.AnalyticsSupport.access$1202(new com.navdy.hud.app.analytics.AnalyticsSupport$PreferenceWatcher(a0, (com.navdy.hud.app.analytics.AnalyticsSupport$PreferenceAdapter)new com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver$3(this)));
        }
    }
    
    @Subscribe
    public void onManeuverMissed(com.navdy.hud.app.maps.MapEvents$RerouteEvent a) {
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Navigation_Maneuver_Missed", new String[0]);
    }
    
    @Subscribe
    public void onManeuverRequired(com.navdy.hud.app.maps.MapEvents$ManeuverSoonEvent a) {
        if (a.maneuver != this.lastRequiredManeuver) {
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Navigation_Maneuver_Required", new String[0]);
            this.lastRequiredManeuver = a.maneuver;
        }
    }
    
    @Subscribe
    public void onManeuverWarning(com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a) {
        com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a0 = this.lastWarningEvent;
        label0: {
            label1: {
                if (a0 == null) {
                    break label1;
                }
                if (a.turnIconId != this.lastWarningEvent.turnIconId) {
                    break label1;
                }
                if (!this.objEquals(a.pendingTurn, this.lastWarningEvent.pendingTurn)) {
                    break label1;
                }
                if (!this.objEquals(a.pendingRoad, this.lastWarningEvent.pendingRoad)) {
                    break label1;
                }
                if (!this.objEquals(a.currentRoad, this.lastWarningEvent.currentRoad)) {
                    break label1;
                }
                if (this.objEquals(a.navigationTurn, this.lastWarningEvent.navigationTurn)) {
                    break label0;
                }
            }
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Navigation_Maneuver_Warning", new String[0]);
            this.lastWarningEvent = a;
        }
    }
    
    @Subscribe
    public void onMapEngineInitialized(com.navdy.hud.app.maps.MapEvents$MapEngineInitialize a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver$1(this), 1);
    }
    
    @Subscribe
    public void onRegionEvent(com.navdy.hud.app.maps.MapEvents$RegionEvent a) {
        String[] a0 = new String[4];
        a0[0] = "State";
        a0[1] = a.state;
        a0[2] = "Country";
        a0[3] = a.country;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Map_Region_Change", a0);
    }
    
    @Subscribe
    public void onRouteSearchRequest(com.navdy.service.library.events.navigation.NavigationRouteRequest a) {
        com.navdy.hud.app.analytics.AnalyticsSupport.access$2000(a);
    }
    
    @Subscribe
    public void onShowToast(com.navdy.hud.app.framework.toast.ToastManager$ShowToast a) {
        if (a.name.equals("disconnection#toast")) {
            com.navdy.hud.app.analytics.AnalyticsSupport.recordPhoneDisconnect(false, (String)null);
        }
    }
    
    @Subscribe
    public void onShutdown(com.navdy.hud.app.event.Shutdown a) {
        if (a.state == com.navdy.hud.app.event.Shutdown$State.SHUTTING_DOWN) {
            com.navdy.hud.app.analytics.AnalyticsSupport.access$800();
        }
    }
    
    public void onStart(String s, com.navdy.hud.app.framework.notifications.NotificationType a, com.navdy.hud.app.ui.framework.UIStateManager$Mode a0) {
        if (a0 == com.navdy.hud.app.ui.framework.UIStateManager$Mode.EXPAND) {
            com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction("Glance_Open_Mini", com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getCurrentNotification(), (com.navdy.hud.app.analytics.AnalyticsSupport.access$300() == null) ? "auto" : com.navdy.hud.app.analytics.AnalyticsSupport.access$300());
        }
        com.navdy.hud.app.analytics.AnalyticsSupport.access$302((String)null);
    }
    
    public void onStop(String s, com.navdy.hud.app.framework.notifications.NotificationType a, com.navdy.hud.app.ui.framework.UIStateManager$Mode a0) {
    }
    
    @Subscribe
    public void onWakeup(com.navdy.hud.app.event.Wakeup a) {
        com.navdy.hud.app.analytics.AnalyticsSupport.access$1600(a.reason);
        com.navdy.hud.app.analytics.AnalyticsSupport.access$1702(false);
        com.navdy.hud.app.analytics.AnalyticsSupport.access$700();
    }
}
