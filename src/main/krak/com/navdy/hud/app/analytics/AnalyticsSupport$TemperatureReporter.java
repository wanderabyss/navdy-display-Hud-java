package com.navdy.hud.app.analytics;

public class AnalyticsSupport$TemperatureReporter extends Thread {
    final private static String SOCKET_NAME = "tempstatus";
    com.navdy.hud.app.device.PowerManager powerManager;
    
    public AnalyticsSupport$TemperatureReporter() {
    }
    
    public void run() {
        label11: {
            android.net.LocalSocket a = null;
            java.io.InputStream a0 = null;
            java.io.BufferedReader a1 = null;
            label10: {
                Exception a2 = null;
                try {
                    a = new android.net.LocalSocket();
                    a.connect(new android.net.LocalSocketAddress("tempstatus", android.net.LocalSocketAddress$Namespace.RESERVED));
                    a0 = a.getInputStream();
                    a1 = new java.io.BufferedReader((java.io.Reader)new java.io.InputStreamReader(a0));
                    break label10;
                } catch(Exception a3) {
                    a2 = a3;
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.access$000().e("error connecting to temperature reporting socket", (Throwable)a2);
                break label11;
            }
            label0: {
                Throwable a4 = null;
                label1: {
                    com.navdy.hud.app.analytics.AnalyticsSupport$Threshold a5 = null;
                    com.navdy.hud.app.analytics.AnalyticsSupport$Threshold a6 = null;
                    int i = 0;
                    try {
                        a5 = new com.navdy.hud.app.analytics.AnalyticsSupport$Threshold(72, com.navdy.hud.app.analytics.AnalyticsSupport.access$2300());
                        a6 = new com.navdy.hud.app.analytics.AnalyticsSupport$Threshold(65, com.navdy.hud.app.analytics.AnalyticsSupport.access$2400());
                        i = 0;
                    } catch(Throwable a7) {
                        a4 = a7;
                        break label1;
                    }
                    label2: while(true) {
                        Exception a8 = null;
                        label9: {
                            String s = null;
                            String[] a9 = null;
                            String s0 = null;
                            int i0 = 0;
                            try {
                                try {
                                    String s1 = a1.readLine();
                                    if (s1 == null) {
                                        continue label2;
                                    }
                                    s = s1.trim();
                                    a9 = new String[13];
                                    s0 = s;
                                    i0 = 0;
                                } catch(Exception a10) {
                                    a8 = a10;
                                    break label9;
                                }
                            } catch(Throwable a11) {
                                a4 = a11;
                                break label1;
                            }
                            while(true) {
                                boolean b = false;
                                label8: {
                                    String[] a12 = null;
                                    try {
                                        try {
                                            if (i0 >= a9.length - 1) {
                                                break label8;
                                            }
                                            if (!s0.contains((CharSequence)" ")) {
                                                break label8;
                                            }
                                            a12 = s0.split(" +", 2);
                                        } catch(Exception a13) {
                                            a8 = a13;
                                            break;
                                        }
                                    } catch(Throwable a14) {
                                        a4 = a14;
                                        break label1;
                                    }
                                    a9[i0] = a12[0];
                                    s0 = a12[1];
                                    i0 = i0 + 1;
                                    continue;
                                }
                                int i1 = i0 + 1;
                                a9[i0] = s0;
                                try {
                                    try {
                                        if (i1 != a9.length) {
                                            com.navdy.service.library.log.Logger a15 = com.navdy.hud.app.analytics.AnalyticsSupport.access$000();
                                            java.util.Locale a16 = java.util.Locale.US;
                                            Object[] a17 = new Object[3];
                                            a17[0] = Integer.valueOf(i1);
                                            a17[1] = Integer.valueOf(a9.length);
                                            a17[2] = s;
                                            a15.e(String.format(a16, "missing fields in temperature reporting data, found = %d, expected = %d data: %s", a17));
                                            continue label2;
                                        }
                                        if (a9[i1 - 1].contains((CharSequence)" ")) {
                                            com.navdy.hud.app.analytics.AnalyticsSupport.access$000().w(new StringBuilder().append("too many fields in temperature reporting data: ").append(s).toString());
                                            a9[i1 - 1] = a9[i1 - 1].split(" ")[0];
                                        }
                                        int i2 = com.navdy.hud.app.analytics.AnalyticsSupport.access$2500(a9[5]);
                                        com.navdy.hud.app.analytics.AnalyticsSupport.access$2602(i2);
                                        int i3 = com.navdy.hud.app.analytics.AnalyticsSupport.access$2500(a9[2]);
                                        int i4 = com.navdy.hud.app.analytics.AnalyticsSupport.access$2500(a9[3]);
                                        long j = android.os.SystemClock.elapsedRealtime();
                                        a6.update(i4);
                                        boolean b0 = a6.isHit();
                                        if (this.powerManager.inQuietMode()) {
                                            b = false;
                                        } else {
                                            a5.update(i3);
                                            label7: {
                                                label5: {
                                                    label6: {
                                                        if (i2 >= 93) {
                                                            break label6;
                                                        }
                                                        if (!a5.isHit()) {
                                                            break label5;
                                                        }
                                                    }
                                                    b = true;
                                                    break label7;
                                                }
                                                b = false;
                                            }
                                            long j0 = com.navdy.hud.app.view.TemperatureWarningView.getLastDismissedTime();
                                            int i5 = (j0 < 0L) ? -1 : (j0 == 0L) ? 0 : 1;
                                            label3: {
                                                label4: {
                                                    if (i5 == 0) {
                                                        break label4;
                                                    }
                                                    if (j0 == -1L) {
                                                        break label3;
                                                    }
                                                    if (j - j0 > com.navdy.hud.app.analytics.AnalyticsSupport.access$2700()) {
                                                        break label3;
                                                    }
                                                }
                                                b = false;
                                            }
                                            if (b && com.navdy.hud.app.analytics.AnalyticsSupport.access$2800() < com.navdy.hud.app.analytics.AnalyticsSupport.access$2900()) {
                                                b = false;
                                            }
                                        }
                                        if (b) {
                                            com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_TEMPERATURE_WARNING).build());
                                        }
                                        if (this.powerManager.inQuietMode() && b0) {
                                            com.navdy.hud.app.analytics.AnalyticsSupport.access$000().d("Shutting down due to high DMD temperature");
                                            com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().post(new com.navdy.hud.app.event.Shutdown(com.navdy.hud.app.event.Shutdown$Reason.HIGH_TEMPERATURE));
                                        }
                                    } catch(Exception a18) {
                                        a8 = a18;
                                        break;
                                    }
                                } catch(Throwable a19) {
                                    a4 = a19;
                                    break label1;
                                }
                                String s2 = a9[0];
                                String s3 = a9[1];
                                String s4 = a9[2];
                                String s5 = a9[3];
                                String s6 = a9[4];
                                String s7 = a9[5];
                                String s8 = a9[6];
                                String s9 = a9[7];
                                String s10 = a9[8];
                                String s11 = a9[9];
                                String s12 = a9[10];
                                String s13 = a9[11];
                                String s14 = a9[12];
                                String s15 = b ? "true" : "false";
                                try {
                                    try {
                                        String[] a20 = new String[28];
                                        a20[0] = "Fan_Speed";
                                        a20[1] = s2;
                                        a20[2] = "CPU_Temperature";
                                        a20[3] = s3;
                                        a20[4] = "Inlet_Temperature";
                                        a20[5] = s4;
                                        a20[6] = "DMD_Temperature";
                                        a20[7] = s5;
                                        a20[8] = "LED_Temperature";
                                        a20[9] = s6;
                                        a20[10] = "CPU_Average";
                                        a20[11] = s7;
                                        a20[12] = "Inlet_Average";
                                        a20[13] = s8;
                                        a20[14] = "DMD_Average";
                                        a20[15] = s9;
                                        a20[16] = "LED_Average";
                                        a20[17] = s10;
                                        a20[18] = "CPU_Max";
                                        a20[19] = s11;
                                        a20[20] = "Inlet_Max";
                                        a20[21] = s12;
                                        a20[22] = "DMD_Max";
                                        a20[23] = s13;
                                        a20[24] = "LED_Max";
                                        a20[25] = s14;
                                        a20[26] = "Warning";
                                        a20[27] = s15;
                                        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Temperature_Change", a20);
                                        continue label2;
                                    } catch(Exception a21) {
                                        a8 = a21;
                                        break;
                                    }
                                } catch(Throwable a22) {
                                    a4 = a22;
                                    break label1;
                                }
                            }
                        }
                        try {
                            com.navdy.hud.app.analytics.AnalyticsSupport.access$000().e("error reading from temperature reporting socket", (Throwable)a8);
                            i = i + 1;
                        } catch(Throwable a23) {
                            a4 = a23;
                            break label1;
                        }
                        if (i > 3) {
                            break;
                        }
                    }
                    try {
                        com.navdy.hud.app.analytics.AnalyticsSupport.access$000().e("too many errors reading temperature reporting socket, giving up");
                        break label0;
                    } catch(Throwable a24) {
                        a4 = a24;
                    }
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
                throw a4;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
        }
    }
}
