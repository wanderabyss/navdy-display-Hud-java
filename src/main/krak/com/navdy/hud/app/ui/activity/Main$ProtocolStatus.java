package com.navdy.hud.app.ui.activity;


public enum Main$ProtocolStatus {
    PROTOCOL_VALID(0),
    PROTOCOL_REMOTE_NEEDS_UPDATE(1),
    PROTOCOL_LOCAL_NEEDS_UPDATE(2);

    private int value;
    Main$ProtocolStatus(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class Main$ProtocolStatus extends Enum {
//    final private static com.navdy.hud.app.ui.activity.Main$ProtocolStatus[] $VALUES;
//    final public static com.navdy.hud.app.ui.activity.Main$ProtocolStatus PROTOCOL_LOCAL_NEEDS_UPDATE;
//    final public static com.navdy.hud.app.ui.activity.Main$ProtocolStatus PROTOCOL_REMOTE_NEEDS_UPDATE;
//    final public static com.navdy.hud.app.ui.activity.Main$ProtocolStatus PROTOCOL_VALID;
//    
//    static {
//        PROTOCOL_VALID = new com.navdy.hud.app.ui.activity.Main$ProtocolStatus("PROTOCOL_VALID", 0);
//        PROTOCOL_REMOTE_NEEDS_UPDATE = new com.navdy.hud.app.ui.activity.Main$ProtocolStatus("PROTOCOL_REMOTE_NEEDS_UPDATE", 1);
//        PROTOCOL_LOCAL_NEEDS_UPDATE = new com.navdy.hud.app.ui.activity.Main$ProtocolStatus("PROTOCOL_LOCAL_NEEDS_UPDATE", 2);
//        com.navdy.hud.app.ui.activity.Main$ProtocolStatus[] a = new com.navdy.hud.app.ui.activity.Main$ProtocolStatus[3];
//        a[0] = PROTOCOL_VALID;
//        a[1] = PROTOCOL_REMOTE_NEEDS_UPDATE;
//        a[2] = PROTOCOL_LOCAL_NEEDS_UPDATE;
//        $VALUES = a;
//    }
//    
//    private Main$ProtocolStatus(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.activity.Main$ProtocolStatus valueOf(String s) {
//        return (com.navdy.hud.app.ui.activity.Main$ProtocolStatus)Enum.valueOf(com.navdy.hud.app.ui.activity.Main$ProtocolStatus.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.activity.Main$ProtocolStatus[] values() {
//        return $VALUES.clone();
//    }
//}
//