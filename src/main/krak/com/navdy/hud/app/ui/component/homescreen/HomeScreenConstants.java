package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class HomeScreenConstants {
    final public static String EMPTY = "";
    final public static int INITIAL_ROUTE_TIMEOUT = 5000;
    final public static int MINUTES_DAY = 1440;
    final public static int MINUTES_HOUR = 60;
    final public static int MIN_RENAVIGATION_DISTANCE_METERS = 500;
    final public static double MIN_RENAVIGATION_DISTANCE_METERS_GAS = 500.0;
    final public static int SELECTION_ROUTE_TIMEOUT = 10000;
    final public static String SPACE = " ";
    final public static String SPEED_KM;
    final public static String SPEED_METERS;
    final public static String SPEED_MPH;
    final public static int TOP_ANIMATION_INITIAL_OUT_INTERVAL = 30000;
    final public static int TOP_ANIMATION_OUT_INTERVAL = 10000;
    final public static com.here.android.mpa.common.ViewRect routeOverviewRect;
    final public static com.here.android.mpa.common.ViewRect routePickerViewRect;
    final public static android.graphics.PointF transformCenterLargeBottom;
    final public static android.graphics.PointF transformCenterLargeMiddle;
    final public static android.graphics.PointF transformCenterPicker;
    final public static android.graphics.PointF transformCenterSmallBottom;
    final public static android.graphics.PointF transformCenterSmallMiddle;
    
    static {
        routePickerViewRect = new com.here.android.mpa.common.ViewRect(com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.routePickerBoxX, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.routePickerBoxY, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.routePickerBoxWidth, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.routePickerBoxHeight);
        routeOverviewRect = new com.here.android.mpa.common.ViewRect(com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.routeOverviewBoxX, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.routeOverviewBoxY, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.routeOverviewBoxWidth, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.routeOverviewBoxHeight);
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        SPEED_MPH = a.getString(R.string.miles_per_hour);
        SPEED_KM = a.getString(R.string.kilometers_per_hour);
        SPEED_METERS = a.getString(R.string.meters_per_second);
        transformCenterSmallMiddle = new android.graphics.PointF((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterOverviewX, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterMapOnRouteOverviewY);
        transformCenterSmallBottom = new android.graphics.PointF((float)(com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterX + com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterIconWidth / 2), (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterMapOnRouteY);
        transformCenterLargeMiddle = new android.graphics.PointF((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterOverviewX, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterOverviewY);
        transformCenterLargeBottom = new android.graphics.PointF((float)(com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterX + com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterIconWidth / 2), (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterOverviewRouteY);
        transformCenterPicker = new android.graphics.PointF((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterOverviewX, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterPickerY);
    }
    
    public HomeScreenConstants() {
    }
}
