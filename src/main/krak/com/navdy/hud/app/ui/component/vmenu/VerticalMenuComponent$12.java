package com.navdy.hud.app.ui.component.vmenu;

class VerticalMenuComponent$12 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent this$0;
    final Runnable val$startAction;
    
    VerticalMenuComponent$12(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a, Runnable a0) {
        super();
        this.this$0 = a;
        this.val$startAction = a0;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        this.this$0.rightContainer.setAlpha(0.0f);
        this.this$0.rightContainer.setScaleX(1f);
        this.this$0.rightContainer.setScaleY(1f);
        if (this.val$startAction != null) {
            this.val$startAction.run();
        }
    }
}
