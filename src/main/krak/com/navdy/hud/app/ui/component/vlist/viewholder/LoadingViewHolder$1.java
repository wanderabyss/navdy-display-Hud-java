package com.navdy.hud.app.ui.component.vlist.viewholder;

class LoadingViewHolder$1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder this$0;
    
    LoadingViewHolder$1(com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        if (com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder.access$000(this.this$0) == null) {
            com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder.access$100().v("abandon loading animation");
        } else {
            com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder.access$000(this.this$0).setStartDelay(33L);
            com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder.access$000(this.this$0).start();
        }
    }
}
