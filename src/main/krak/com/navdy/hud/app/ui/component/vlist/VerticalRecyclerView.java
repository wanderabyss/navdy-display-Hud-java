package com.navdy.hud.app.ui.component.vlist;

public class VerticalRecyclerView extends android.support.v7.widget.RecyclerView {
    public VerticalRecyclerView(android.content.Context a) {
        super(a);
    }
    
    public VerticalRecyclerView(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
    }
    
    public VerticalRecyclerView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
    }
}
