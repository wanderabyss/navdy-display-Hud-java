package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

class NavigationView$7 implements Runnable {
    final com.navdy.hud.app.ui.component.homescreen.NavigationView this$0;
    final boolean val$showRawLocation;
    
    NavigationView$7(com.navdy.hud.app.ui.component.homescreen.NavigationView a, boolean b) {
        super();
        this.this$0 = a;
        this.val$showRawLocation = b;
    }
    
    public void run() {
        com.here.android.mpa.mapping.PositionIndicator a = com.navdy.hud.app.ui.component.homescreen.NavigationView.access$000(this.this$0).getPositionIndicator();
        try {
            if (this.val$showRawLocation) {
                com.here.android.mpa.common.Image a0 = new com.here.android.mpa.common.Image();
                a0.setImageResource(R.drawable.here_pos_indicator);
                a.setMarker(a0);
            }
            a.setVisible(this.val$showRawLocation);
        } catch(Throwable a1) {
            com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).e(a1);
        }
    }
}
