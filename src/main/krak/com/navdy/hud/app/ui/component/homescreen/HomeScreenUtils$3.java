package com.navdy.hud.app.ui.component.homescreen;

final class HomeScreenUtils$3 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final android.view.View val$view;
    
    HomeScreenUtils$3(android.view.View a) {
        super();
        this.val$view = a;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        int i = ((Integer)a.getAnimatedValue()).intValue();
        com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.setWidth(this.val$view, i);
    }
}
