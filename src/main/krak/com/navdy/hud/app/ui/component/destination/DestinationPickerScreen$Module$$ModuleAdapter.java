package com.navdy.hud.app.ui.component.destination;

final public class DestinationPickerScreen$Module$$ModuleAdapter extends dagger.internal.ModuleAdapter {
    final private static Class[] INCLUDES;
    final private static String[] INJECTS;
    final private static Class[] STATIC_INJECTIONS;
    
    static {
        String[] a = new String[1];
        a[0] = "members/com.navdy.hud.app.ui.component.destination.DestinationPickerView";
        INJECTS = a;
        STATIC_INJECTIONS = new Class[0];
        INCLUDES = new Class[0];
    }
    
    public DestinationPickerScreen$Module$$ModuleAdapter() {
        super(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
