package com.navdy.hud.app.ui.component.homescreen;

class NavigationView$9 implements com.here.android.mpa.mapping.Map$OnTransformListener {
    final com.navdy.hud.app.ui.component.homescreen.NavigationView this$0;
    final Runnable val$endAction;
    
    NavigationView$9(com.navdy.hud.app.ui.component.homescreen.NavigationView a, Runnable a0) {
        super();
        this.this$0 = a;
        this.val$endAction = a0;
    }
    
    public void onMapTransformEnd(com.here.android.mpa.mapping.MapState a) {
        com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v("animateBackfromOverview: transform end");
        com.navdy.hud.app.ui.component.homescreen.NavigationView.access$000(this.this$0).removeTransformListener((com.here.android.mpa.mapping.Map$OnTransformListener)this);
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.ui.component.homescreen.NavigationView$9$1(this), 17);
    }
    
    public void onMapTransformStart() {
    }
}
