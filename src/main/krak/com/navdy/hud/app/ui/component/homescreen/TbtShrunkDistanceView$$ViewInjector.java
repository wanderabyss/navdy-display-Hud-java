package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class TbtShrunkDistanceView$$ViewInjector {
    public TbtShrunkDistanceView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView a0, Object a1) {
        a0.tbtShrunkDistance = (android.widget.TextView)a.findRequiredView(a1, R.id.tbtShrunkDistance, "field 'tbtShrunkDistance'");
        a0.nowShrunkIcon = a.findRequiredView(a1, R.id.now_shrunk_icon, "field 'nowShrunkIcon'");
        a0.progressBarShrunk = (android.widget.ProgressBar)a.findRequiredView(a1, R.id.progress_bar_shrunk, "field 'progressBarShrunk'");
    }
    
    public static void reset(com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView a) {
        a.tbtShrunkDistance = null;
        a.nowShrunkIcon = null;
        a.progressBarShrunk = null;
    }
}
