package com.navdy.hud.app.ui.component.homescreen;

class NavigationView$8 implements Runnable {
    final com.navdy.hud.app.ui.component.homescreen.NavigationView this$0;
    final boolean val$animate;
    final boolean val$changeState;
    final boolean val$cleanupMapOverview;
    final com.here.android.mpa.common.GeoPosition val$geoPosition;
    final double val$tilt;
    final boolean val$useLastZoom;
    final double val$zoom;
    
    NavigationView$8(com.navdy.hud.app.ui.component.homescreen.NavigationView a, boolean b, boolean b0, boolean b1, com.here.android.mpa.common.GeoPosition a0, double d, double d0, boolean b2) {
        super();
        this.this$0 = a;
        this.val$animate = b;
        this.val$changeState = b0;
        this.val$useLastZoom = b1;
        this.val$geoPosition = a0;
        this.val$zoom = d;
        this.val$tilt = d0;
        this.val$cleanupMapOverview = b2;
    }
    
    public void run() {
        com.navdy.hud.app.ui.component.homescreen.NavigationView.access$800(this.this$0, this.val$animate, this.val$changeState, this.val$useLastZoom, this.val$geoPosition, this.val$zoom, (float)this.val$tilt, this.val$cleanupMapOverview);
    }
}
