package com.navdy.hud.app.ui.component.image;


public enum InitialsImageView$Style {
    TINY(0),
    SMALL(1),
    LARGE(2),
    MEDIUM(3),
    DEFAULT(4);

    private int value;
    InitialsImageView$Style(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class InitialsImageView$Style extends Enum {
//    final private static com.navdy.hud.app.ui.component.image.InitialsImageView$Style[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.image.InitialsImageView$Style DEFAULT;
//    final public static com.navdy.hud.app.ui.component.image.InitialsImageView$Style LARGE;
//    final public static com.navdy.hud.app.ui.component.image.InitialsImageView$Style MEDIUM;
//    final public static com.navdy.hud.app.ui.component.image.InitialsImageView$Style SMALL;
//    final public static com.navdy.hud.app.ui.component.image.InitialsImageView$Style TINY;
//    
//    static {
//        TINY = new com.navdy.hud.app.ui.component.image.InitialsImageView$Style("TINY", 0);
//        SMALL = new com.navdy.hud.app.ui.component.image.InitialsImageView$Style("SMALL", 1);
//        LARGE = new com.navdy.hud.app.ui.component.image.InitialsImageView$Style("LARGE", 2);
//        MEDIUM = new com.navdy.hud.app.ui.component.image.InitialsImageView$Style("MEDIUM", 3);
//        DEFAULT = new com.navdy.hud.app.ui.component.image.InitialsImageView$Style("DEFAULT", 4);
//        com.navdy.hud.app.ui.component.image.InitialsImageView$Style[] a = new com.navdy.hud.app.ui.component.image.InitialsImageView$Style[5];
//        a[0] = TINY;
//        a[1] = SMALL;
//        a[2] = LARGE;
//        a[3] = MEDIUM;
//        a[4] = DEFAULT;
//        $VALUES = a;
//    }
//    
//    private InitialsImageView$Style(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.image.InitialsImageView$Style valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.image.InitialsImageView$Style)Enum.valueOf(com.navdy.hud.app.ui.component.image.InitialsImageView$Style.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.image.InitialsImageView$Style[] values() {
//        return $VALUES.clone();
//    }
//}
//