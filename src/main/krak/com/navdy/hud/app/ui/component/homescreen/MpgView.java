package com.navdy.hud.app.ui.component.homescreen;

public class MpgView extends android.widget.LinearLayout {
    private com.navdy.service.library.log.Logger logger;
    @InjectView(R.id.txt_mpg_label)
    android.widget.TextView mpgLabelTextView;
    @InjectView(R.id.txt_mpg)
    android.widget.TextView mpgTextView;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    
    public MpgView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public MpgView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public MpgView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
    }
    
    public void getTopAnimator(android.animation.AnimatorSet$Builder a, boolean b) {
        if (b) {
            a.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this, (float)((int)this.getX() + com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.topViewSpeedOut)));
            android.util.Property a0 = android.view.View.ALPHA;
            float[] a1 = new float[2];
            a1[0] = 1f;
            a1[1] = 0.0f;
            a.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(this, a0, a1));
        } else {
            a.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this, (float)((int)this.getX() - com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.topViewSpeedOut)));
            android.util.Property a2 = android.view.View.ALPHA;
            float[] a3 = new float[2];
            a3[0] = 0.0f;
            a3[1] = 1f;
            a.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(this, a2, a3));
        }
    }
    
    protected void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        this.uiStateManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
    }
    
    public void resetTopViewsAnimator() {
        this.setAlpha(1f);
        this.setView(this.uiStateManager.getCustomAnimateMode());
    }
    
    public void setView(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        switch(com.navdy.hud.app.ui.component.homescreen.MpgView$1.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                this.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.speedShrinkLeftX);
                break;
            }
            case 1: {
                this.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.speedX);
                break;
            }
        }
    }
    
    void updateInstantaneousFuelConsumption(double d) {
        String s = null;
        double d0 = com.navdy.hud.app.util.ConversionUtil.convertLpHundredKmToMPG(d);
        if (d0 > 0.0) {
            Object[] a = new Object[1];
            a[0] = Integer.valueOf((int)d0);
            s = String.format("%d", a);
        } else {
            s = "- -";
        }
        this.mpgTextView.setText((CharSequence)s);
    }
}
