package com.navdy.hud.app.ui.component.carousel;

public class CarouselAnimator implements com.navdy.hud.app.ui.component.carousel.AnimationStrategy {
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.carousel.CarouselAnimator.class);
    }
    
    public CarouselAnimator() {
    }
    
    public android.animation.AnimatorSet buildLayoutAnimation(android.animation.Animator$AnimatorListener a, com.navdy.hud.app.ui.component.carousel.CarouselLayout a0, int i, int i0) {
        return null;
    }
    
    public android.animation.AnimatorSet createHiddenViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction a0) {
        android.view.View a1 = null;
        android.view.View a2 = null;
        android.animation.AnimatorSet a3 = new android.animation.AnimatorSet();
        if (a0 != com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.RIGHT) {
            a1 = a.newRightView;
            a2 = a.rightView;
        } else {
            a1 = a.newLeftView;
            a2 = a.leftView;
        }
        float[] a4 = new float[1];
        a4[0] = a2.getX();
        a3.play((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a1, "x", a4));
        return a3;
    }
    
    public android.animation.AnimatorSet createMiddleLeftViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction a0) {
        android.animation.AnimatorSet a1 = new android.animation.AnimatorSet();
        android.view.View a2 = (a0 != com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.LEFT) ? a.rightView : a.leftView;
        float f = (a.middleLeftView.getScaleX() != 1f) ? 1f : (float)a.sideImageSize / (float)a.mainImageSize;
        android.view.View a3 = a.middleLeftView;
        float[] a4 = new float[1];
        a4[0] = f;
        android.animation.ObjectAnimator a5 = android.animation.ObjectAnimator.ofFloat(a3, "scaleX", a4);
        android.view.View a6 = a.middleLeftView;
        float[] a7 = new float[1];
        a7[0] = f;
        android.animation.ObjectAnimator a8 = android.animation.ObjectAnimator.ofFloat(a6, "scaleY", a7);
        android.view.View a9 = a.middleLeftView;
        float[] a10 = new float[1];
        a10[0] = a2.getX();
        android.animation.ObjectAnimator a11 = android.animation.ObjectAnimator.ofFloat(a9, "x", a10);
        android.view.View a12 = a.middleLeftView;
        float[] a13 = new float[1];
        a13[0] = a2.getY();
        android.animation.ObjectAnimator a14 = android.animation.ObjectAnimator.ofFloat(a12, "y", a13);
        android.animation.AnimatorSet a15 = ((com.navdy.hud.app.ui.component.image.CrossFadeImageView)a.middleLeftView).getCrossFadeAnimator();
        a.middleLeftView.setPivotX(0.5f);
        a.middleLeftView.setPivotY(0.5f);
        android.animation.Animator[] a16 = new android.animation.Animator[5];
        a16[0] = a5;
        a16[1] = a8;
        a16[2] = a11;
        a16[3] = a14;
        a16[4] = a15;
        a1.playTogether(a16);
        return a1;
    }
    
    public android.animation.AnimatorSet createMiddleRightViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction a0) {
        android.animation.AnimatorSet a1 = new android.animation.AnimatorSet();
        float f = (float)a.sideImageSize / (float)a.middleRightView.getMeasuredHeight();
        android.view.View a2 = (a0 != com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.LEFT) ? a.rightView : a.leftView;
        android.animation.Animator[] a3 = new android.animation.Animator[5];
        android.view.View a4 = a.middleRightView;
        float[] a5 = new float[1];
        a5[0] = f;
        a3[0] = android.animation.ObjectAnimator.ofFloat(a4, "scaleX", a5);
        android.view.View a6 = a.middleRightView;
        float[] a7 = new float[1];
        a7[0] = f;
        a3[1] = android.animation.ObjectAnimator.ofFloat(a6, "scaleY", a7);
        android.view.View a8 = a.middleRightView;
        float[] a9 = new float[1];
        a9[0] = a2.getX() + (float)((a.mainViewDividerPadding + a.sideImageSize) / 2);
        a3[2] = android.animation.ObjectAnimator.ofFloat(a8, "x", a9);
        android.view.View a10 = a.middleRightView;
        float[] a11 = new float[1];
        a11[0] = a.middleRightView.getY();
        a3[3] = android.animation.ObjectAnimator.ofFloat(a10, "y", a11);
        android.view.View a12 = a.middleRightView;
        float[] a13 = new float[1];
        a13[0] = 0.0f;
        a3[4] = android.animation.ObjectAnimator.ofFloat(a12, "alpha", a13);
        a1.playTogether(a3);
        return a1;
    }
    
    public android.animation.AnimatorSet createNewMiddleRightViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction a0) {
        android.animation.AnimatorSet a1 = new android.animation.AnimatorSet();
        android.view.View a2 = a.newMiddleRightView;
        float[] a3 = new float[1];
        a3[0] = a.middleLeftView.getX() + (float)a.mainViewDividerPadding + (float)a.mainImageSize;
        android.animation.ObjectAnimator a4 = android.animation.ObjectAnimator.ofFloat(a2, "x", a3);
        if (a0 != com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.RIGHT) {
            android.animation.Animator[] a5 = new android.animation.Animator[1];
            a5[0] = a4;
            a1.playTogether(a5);
        } else {
            android.animation.Animator[] a6 = new android.animation.Animator[2];
            a6[0] = a4;
            android.view.View a7 = a.newMiddleRightView;
            float[] a8 = new float[1];
            a8[0] = 1f;
            a6[1] = android.animation.ObjectAnimator.ofFloat(a7, "alpha", a8);
            a1.playTogether(a6);
        }
        return a1;
    }
    
    public android.animation.AnimatorSet createSideViewToMiddleAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction a0) {
        android.animation.AnimatorSet a1 = new android.animation.AnimatorSet();
        android.view.View a2 = (a0 != com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.RIGHT) ? a.rightView : a.leftView;
        float f = (a2.getScaleX() != 1f) ? 1f : (float)a.mainImageSize / (float)a.sideImageSize;
        float[] a3 = new float[1];
        a3[0] = f;
        android.animation.ObjectAnimator a4 = android.animation.ObjectAnimator.ofFloat(a2, "scaleX", a3);
        float[] a5 = new float[1];
        a5[0] = f;
        android.animation.ObjectAnimator a6 = android.animation.ObjectAnimator.ofFloat(a2, "scaleY", a5);
        float[] a7 = new float[1];
        a7[0] = a.middleLeftView.getX();
        android.animation.ObjectAnimator a8 = android.animation.ObjectAnimator.ofFloat(a2, "x", a7);
        float[] a9 = new float[1];
        a9[0] = a.middleLeftView.getY();
        android.animation.ObjectAnimator a10 = android.animation.ObjectAnimator.ofFloat(a2, "y", a9);
        com.navdy.hud.app.ui.component.image.CrossFadeImageView a11 = (com.navdy.hud.app.ui.component.image.CrossFadeImageView)a2;
        android.view.View a12 = a11;
        android.animation.AnimatorSet a13 = a11.getCrossFadeAnimator();
        a12.setPivotX(0.5f);
        a12.setPivotY(0.5f);
        android.animation.Animator[] a14 = new android.animation.Animator[5];
        a14[0] = a4;
        a14[1] = a6;
        a14[2] = a8;
        a14[3] = a10;
        a14[4] = a13;
        a1.playTogether(a14);
        return a1;
    }
    
    public android.animation.Animator createViewOutAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction a0) {
        android.animation.ObjectAnimator a1 = null;
        if (a0 != com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.RIGHT) {
            android.view.View a2 = a.leftView;
            float[] a3 = new float[1];
            a3[0] = -(a.leftView.getX() + (float)a.leftView.getMeasuredWidth());
            a1 = android.animation.ObjectAnimator.ofFloat(a2, "x", a3);
        } else {
            android.view.View a4 = a.rightView;
            float[] a5 = new float[1];
            a5[0] = a.rightView.getX() + (float)a.getMeasuredWidth();
            a1 = android.animation.ObjectAnimator.ofFloat(a4, "x", a5);
        }
        return a1;
    }
}
