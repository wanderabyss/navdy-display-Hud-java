package com.navdy.hud.app.ui.component.vlist;

class VerticalModelCache$CacheEntry {
    java.util.ArrayList items;
    int maxItems;
    
    VerticalModelCache$CacheEntry(int i, int i0) {
        this.maxItems = i;
        this.items = new java.util.ArrayList(i);
        if (i0 > 0) {
            int i1 = 0;
            while(i1 < i0) {
                this.items.add(new com.navdy.hud.app.ui.component.vlist.VerticalList$Model());
                i1 = i1 + 1;
            }
        }
    }
}
