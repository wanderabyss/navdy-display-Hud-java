package com.navdy.hud.app.ui.component.homescreen;

class HomeScreenView$2 implements com.navdy.hud.app.ui.framework.IScreenAnimationListener {
    final com.navdy.hud.app.ui.component.homescreen.HomeScreenView this$0;
    
    HomeScreenView$2(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        super();
        this.this$0 = a;
    }
    
    public void onStart(com.navdy.hud.app.screen.BaseScreen a, com.navdy.hud.app.screen.BaseScreen a0) {
    }
    
    public void onStop(com.navdy.hud.app.screen.BaseScreen a, com.navdy.hud.app.screen.BaseScreen a0) {
        if (com.navdy.hud.app.ui.component.homescreen.HomeScreenView.access$000(this.this$0) && a != null && a.getScreen() == com.navdy.service.library.events.ui.Screen.SCREEN_HYBRID_MAP) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView.access$002(this.this$0, false);
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger.v("expanding collapse notifi");
            com.navdy.hud.app.framework.notifications.NotificationManager a1 = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
            if (a1.makeNotificationCurrent(false)) {
                a1.showNotification();
            }
        }
    }
}
