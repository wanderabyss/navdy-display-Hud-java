package com.navdy.hud.app.ui.component.image;

public class ColorImageView extends android.widget.ImageView {
    private int color;
    private android.graphics.Paint paint;
    
    public ColorImageView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null, 0);
    }
    
    public ColorImageView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public ColorImageView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.init();
    }
    
    private void init() {
        this.paint = new android.graphics.Paint();
        this.paint.setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
    }
    
    protected void onDraw(android.graphics.Canvas a) {
        super.onDraw(a);
        int i = this.getWidth();
        int i0 = this.getHeight();
        a.drawColor(0);
        this.paint.setColor(this.color);
        a.drawCircle((float)(i / 2), (float)(i0 / 2), (float)(i / 2), this.paint);
    }
    
    public void setColor(int i) {
        this.color = i;
        this.invalidate();
    }
}
