package com.navdy.hud.app.ui.component.vmenu;

class VerticalMenuComponent$11 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent this$0;
    final Runnable val$endAction;
    
    VerticalMenuComponent$11(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a, Runnable a0) {
        super();
        this.this$0 = a;
        this.val$endAction = a0;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        android.animation.AnimatorSet a0 = new android.animation.AnimatorSet();
        android.view.ViewGroup a1 = this.this$0.rightContainer;
        android.util.Property a2 = android.view.View.ALPHA;
        float[] a3 = new float[1];
        a3[0] = 1f;
        android.animation.AnimatorSet$Builder a4 = a0.play((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a1, a2, a3));
        android.view.ViewGroup a5 = this.this$0.selectedImage;
        android.util.Property a6 = android.view.View.ALPHA;
        float[] a7 = new float[1];
        a7[0] = 1f;
        a4.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a5, a6, a7));
        android.widget.TextView a8 = this.this$0.selectedText;
        android.util.Property a9 = android.view.View.ALPHA;
        float[] a10 = new float[1];
        a10[0] = 1f;
        a4.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a8, a9, a10));
        a0.setDuration(100L);
        a0.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$11$1(this));
        a0.start();
    }
}
