package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;
import javax.inject.Inject;
import javax.inject.Singleton;
import com.squareup.otto.Subscribe;

@Singleton
public class MainMenuScreen2$Presenter extends com.navdy.hud.app.ui.framework.BasePresenter {
    private boolean animateIn;
    @Inject
    com.squareup.otto.Bus bus;
    private boolean closed;
    private com.navdy.hud.app.ui.component.mainmenu.IMenu currentMenu;
    private boolean firstLoad;
    private boolean handledSelection;
    private android.view.View[] leftView;
    private com.navdy.hud.app.ui.component.mainmenu.MainMenu mainMenu;
    private boolean mapShown;
    private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode menuMode;
    private String menuPath;
    private boolean navEnded;
    private boolean registered;
    @Inject
    android.content.SharedPreferences sharedPreferences;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode switchBackMode;
    @Inject
    com.navdy.hud.app.framework.trips.TripManager tripManager;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    
    public MainMenuScreen2$Presenter() {
    }
    
    static Object access$100(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a) {
        return a.getView();
    }
    
    static com.navdy.hud.app.ui.component.mainmenu.IMenu access$200(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a) {
        return a.currentMenu;
    }
    
    static com.navdy.hud.app.ui.component.mainmenu.IMenu access$202(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a, com.navdy.hud.app.ui.component.mainmenu.IMenu a0) {
        a.currentMenu = a0;
        return a0;
    }
    
    static Object access$300(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a) {
        return a.getView();
    }
    
    private int findEntry(java.util.List a, int i) {
        int i0 = a.size();
        Object a0 = a;
        int i1 = 0;
        while(true) {
            if (i1 >= i0) {
                i1 = -1;
            } else if (((com.navdy.hud.app.ui.component.vlist.VerticalList$Model)((java.util.List)a0).get(i1)).id != i) {
                i1 = i1 + 1;
                continue;
            }
            return i1;
        }
    }
    
    com.navdy.hud.app.ui.component.mainmenu.IMenu buildMenuPath(com.navdy.hud.app.ui.component.mainmenu.MainMenu a, String s) {
        Object a0 = null;
        try {
            if (s.indexOf("/") == 0) {
                String s0 = null;
                String s1 = s.substring(1);
                int i = s1.indexOf("/");
                if (i < 0) {
                    s0 = null;
                } else {
                    s0 = s.substring(i + 1);
                    s1 = s1.substring(0, i);
                }
                com.navdy.hud.app.ui.component.mainmenu.IMenu a1 = a.getChildMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)null, s1, s0);
                a0 = (a1 == null) ? a : a1;
            } else {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().w("menu_path does not start with slash");
                a0 = a;
            }
        } catch(Throwable a2) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().e(a2);
            a0 = a;
        }
        return (com.navdy.hud.app.ui.component.mainmenu.IMenu)a0;
    }
    
    void cancelLoadingAnimation(int i) {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        if (a != null) {
            a.vmenuComponent.verticalList.cancelLoadingAnimation(i);
        }
    }
    
    public void cleanMapFluctuator() {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v("cleanMapFluctuator");
        com.navdy.hud.app.ui.component.homescreen.NavigationView a = this.uiStateManager.getNavigationView();
        if (a != null) {
            a.cleanupFluctuator();
        }
    }
    
    void close() {
        if (this.currentMenu != null && this.currentMenu.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.SEARCH) {
            com.navdy.hud.app.analytics.AnalyticsSupport.recordNearbySearchClosed();
        }
        this.close((Runnable)null);
    }
    
    void close(Runnable a) {
        if (this.closed) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v("already closed");
        } else {
            this.closed = true;
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v("close");
            com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("exit");
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a0 = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
            if (a0 != null) {
                a0.vmenuComponent.animateOut((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter$1(this, a));
            }
        }
    }
    
    public void disableMapViews() {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        if (a != null) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v("disableMapViews");
            a.setBackgroundColor(-16777216);
            a.vmenuComponent.leftContainer.removeAllViews();
            a.vmenuComponent.leftContainer.setBackgroundColor(-16777216);
            if (this.leftView != null) {
                int i = 0;
                while(i < this.leftView.length) {
                    a.vmenuComponent.leftContainer.addView(this.leftView[i]);
                    i = i + 1;
                }
                this.leftView = null;
            }
            a.vmenuComponent.rightContainer.setBackgroundColor(0);
            a.vmenuComponent.closeContainer.setBackgroundColor(0);
            a.rightBackground.setVisibility(8);
        }
    }
    
    public void enableMapViews() {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        if (a != null) {
            int i = a.vmenuComponent.leftContainer.getChildCount();
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("enableMapViews:").append(i).toString());
            this.leftView = new android.view.View[i];
            int i0 = 0;
            while(i0 < i) {
                this.leftView[i0] = a.vmenuComponent.leftContainer.getChildAt(i0);
                i0 = i0 + 1;
            }
            a.vmenuComponent.leftContainer.removeAllViews();
            a.vmenuComponent.leftContainer.setBackgroundColor(0);
            android.view.View a0 = android.view.LayoutInflater.from(a.getContext()).inflate(R.layout.active_trip_menu_mask_lyt, (android.view.ViewGroup)a, false);
            a.vmenuComponent.leftContainer.addView(a0);
            a.vmenuComponent.rightContainer.setBackgroundColor(-16777216);
            a.vmenuComponent.closeContainer.setBackgroundColor(-16777216);
            a.rightBackground.setVisibility(0);
        }
    }
    
    com.navdy.hud.app.ui.component.ConfirmationLayout getConfirmationLayout() {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        com.navdy.hud.app.ui.component.ConfirmationLayout a0 = (a == null) ? null : a.confirmationLayout;
        return a0;
    }
    
    com.navdy.hud.app.ui.component.mainmenu.IMenu getCurrentMenu() {
        return this.currentMenu;
    }
    
    public com.here.android.mpa.common.GeoBoundingBox getCurrentRouteBoundingBox() {
        com.here.android.mpa.common.GeoBoundingBox a = null;
        String s = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentRouteId();
        label2: {
            com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a0 = null;
            label0: {
                label1: {
                    if (s == null) {
                        break label1;
                    }
                    a0 = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(s);
                    if (a0 != null) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = a0.route.getBoundingBox();
        }
        return a;
    }
    
    int getCurrentSelection() {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        return (a == null) ? 0 : a.vmenuComponent.verticalList.getCurrentPosition();
    }
    
    public int getCurrentSubSelectionId() {
        int i = 0;
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        label2: {
            com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a0 = null;
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    a0 = a.vmenuComponent.verticalList.getCurrentViewHolder();
                    if (a0 instanceof com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder) {
                        break label0;
                    }
                }
                i = -1;
                break label2;
            }
            i = ((com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder)a0).getCurrentSelectionId();
        }
        return i;
    }
    
    public int getCurrentSubSelectionPos() {
        int i = 0;
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        label2: {
            com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a0 = null;
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    a0 = a.vmenuComponent.verticalList.getCurrentViewHolder();
                    if (a0 instanceof com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder) {
                        break label0;
                    }
                }
                i = -1;
                break label2;
            }
            i = ((com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder)a0).getCurrentSelection();
        }
        return i;
    }
    
    public void hideMap() {
        if (this.mapShown) {
            this.mapShown = false;
            if ((com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView() != null) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v("hideMap");
                this.disableMapViews();
                if (this.switchBackMode != null) {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("hideMap switchbackmode: ").append(this.switchBackMode).toString());
                    this.uiStateManager.getHomescreenView().setDisplayMode(this.switchBackMode);
                }
                com.navdy.hud.app.ui.component.homescreen.NavigationView a = this.uiStateManager.getNavigationView();
                if (!this.navEnded) {
                    com.navdy.hud.app.maps.here.HereNavigationManager a0 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
                    if (a0.isNavigationModeOn()) {
                        if (a0.hasArrived()) {
                            this.navEnded = true;
                        }
                    } else {
                        this.navEnded = true;
                    }
                }
                a.switchBackfromRouteSearchMode(!this.navEnded);
            }
        }
    }
    
    public void hideScrimCover() {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        if (a != null) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v("scrim-hide");
            a.scrimCover.setVisibility(8);
        }
    }
    
    public void hideToolTip() {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        if (a != null) {
            a.vmenuComponent.hideToolTip();
        }
    }
    
    void init(com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a, android.os.Bundle a0) {
        switch(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$1.$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainMenuScreen2$MenuMode[this.menuMode.ordinal()]) {
            case 3: {
                this.currentMenu = (com.navdy.hud.app.ui.component.mainmenu.IMenu)new com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu(a.vmenuComponent, this, (com.navdy.hud.app.ui.component.mainmenu.IMenu)null, com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType.SNAP_SHOT);
                this.loadMenu(this.currentMenu, (com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel)null, 0, 0);
                break;
            }
            case 2: {
                java.util.ArrayList a1 = a0.getParcelableArrayList("CONTACTS");
                String s = a0.getString("NOTIF_ID");
                java.util.ArrayList a2 = new java.util.ArrayList();
                Object a3 = a1.iterator();
                while(((java.util.Iterator)a3).hasNext()) {
                    ((java.util.List)a2).add((com.navdy.hud.app.framework.contacts.Contact)((java.util.Iterator)a3).next());
                }
                this.currentMenu = (com.navdy.hud.app.ui.component.mainmenu.IMenu)new com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu((java.util.List)a2, s, a.vmenuComponent, this, (com.navdy.hud.app.ui.component.mainmenu.IMenu)null, this.bus);
                this.loadMenu(this.currentMenu, (com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel)null, 0, 0);
                break;
            }
            case 1: {
                this.mainMenu = new com.navdy.hud.app.ui.component.mainmenu.MainMenu(this.bus, a.vmenuComponent, this);
                if (android.text.TextUtils.isEmpty((CharSequence)this.menuPath)) {
                    this.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.mainMenu, (com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel)null, 0, 0);
                    break;
                } else {
                    this.loadMenu(this.buildMenuPath(this.mainMenu, this.menuPath), (com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel)null, 0, 0);
                    break;
                }
            }
        }
    }
    
    boolean isClosed() {
        return this.closed;
    }
    
    boolean isItemClickable(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        return this.currentMenu != null && this.currentMenu.isItemClickable(a.id, a.pos);
    }
    
    public boolean isMapShown() {
        return this.mapShown;
    }
    
    void loadMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu a, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a0, int i, int i0) {
        this.loadMenu(a, a0, i, i0, false);
    }
    
    void loadMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu a, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a0, int i, int i0, boolean b) {
        this.loadMenu(a, a0, i, i0, b, 0);
    }
    
    void loadMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu a, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a0, int i, int i0, boolean b, int i1) {
        this.hideToolTip();
        if (a0 == com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.ROOT && this.isMapShown()) {
            this.disableMapViews();
        }
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a1 = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        if (a1 != null) {
            if (this.firstLoad) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("Loading menu:").append(a.getType()).toString());
                this.currentMenu = a;
                this.currentMenu.setSelectedIcon();
                this.firstLoad = false;
                a1.vmenuComponent.verticalList.setBindCallbacks(this.currentMenu.isBindCallsEnabled());
                a1.vmenuComponent.updateView(this.currentMenu.getItems(), this.currentMenu.getInitialSelection(), this.currentMenu.isFirstItemEmpty());
            } else {
                switch(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$1.$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel[a0.ordinal()]) {
                    case 4: {
                        com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("refresh root menu:").append(a.getType()).toString());
                        this.currentMenu = a;
                        this.currentMenu.setSelectedIcon();
                        a1.vmenuComponent.updateView(a.getItems(), a.getInitialSelection(), this.currentMenu.isFirstItemEmpty());
                        break;
                    }
                    case 3: {
                        if (this.currentMenu != a) {
                            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().w(new StringBuilder().append("refresh current menu different cur=").append(this.currentMenu.getType()).append(" passed:").append(a.getType()).toString());
                            break;
                        } else {
                            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("refresh current menu:").append(this.currentMenu.getType()).toString());
                            a1.vmenuComponent.updateView(this.currentMenu.getItems(), this.currentMenu.getInitialSelection(), true, b, a1.vmenuComponent.getFastScrollIndex());
                            break;
                        }
                    }
                    case 2: {
                        com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("backSelectionId:").append(i0).toString());
                        java.util.List a2 = a.getItems();
                        if (i0 != 0) {
                            int i2 = this.findEntry(a2, i0);
                            if (i2 != -1) {
                                i = i2;
                            }
                            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("backSelectionId:").append(i0).append(" index=").append(i2).toString());
                        }
                        int i3 = a2.size();
                        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a3 = null;
                        if (i < i3) {
                            a3 = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)a2.get(i);
                        }
                        a1.vmenuComponent.performBackAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter$3(this, a, a2, i, b), (Runnable)null, a3, i1);
                        break;
                    }
                    case 1: {
                        com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("Loading menu:").append(a.getType()).toString());
                        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a4 = this.currentMenu.getModelfromPos(i);
                        if (this.currentMenu != null) {
                            this.currentMenu.onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL);
                        }
                        this.currentMenu = a;
                        this.currentMenu.setBackSelectionPos(i);
                        this.currentMenu.setSelectedIcon();
                        a1.vmenuComponent.performEnterAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter$2(this, b), (Runnable)null, a4);
                        break;
                    }
                }
            }
        }
    }
    
    @Subscribe
    public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        label0: if (this.currentMenu != null) {
            switch(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$1.$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[a.state.ordinal()]) {
                case 2: {
                    switch(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$1.$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainMenuScreen2$MenuMode[this.menuMode.ordinal()]) {
                        case 2: {
                            this.close();
                            break label0;
                        }
                        case 1: {
                            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("disconnected:").append(this.currentMenu.getType()).toString());
                            if (this.mainMenu != null) {
                                this.mainMenu.clearState();
                            }
                            if (this.currentMenu.getType() != com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MAIN) {
                                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v("disconnected: refresh menu,back to root");
                                this.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.mainMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.ROOT, 0, 0);
                                break label0;
                            } else {
                                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v("disconnected: refresh main menu");
                                this.loadMenu(this.currentMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.REFRESH_CURRENT, 0, 0);
                                break label0;
                            }
                        }
                        default: {
                            break label0;
                        }
                    }
                }
                default: {
                    break;
                }
                case 1: {
                }
            }
        }
    }
    
    @Subscribe
    public void onDeviceInfoAvailable(com.navdy.hud.app.event.DeviceInfoAvailable a) {
        if (this.currentMenu != null && this.menuMode == com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode.MAIN_MENU) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("onDeviceInfoAvailable:").append(this.currentMenu.getType()).toString());
            if (this.currentMenu.getType() == com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MAIN && this.currentMenu.getModelfromPos(this.currentMenu.getInitialSelection()).id == R.id.main_menu_settings_connect_phone) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v("connected: refresh main menu");
                this.loadMenu(this.currentMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.REFRESH_CURRENT, 0, 0);
            }
        }
    }
    
    public void onLoad(android.os.Bundle a) {
        this.reset();
        this.uiStateManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
        this.bus.register(this);
        this.registered = true;
        this.firstLoad = true;
        this.menuPath = null;
        this.menuMode = com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode.MAIN_MENU;
        if (a != null) {
            this.menuPath = a.getString("MENU_PATH");
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("menu_path:").append(this.menuPath).toString());
            int i = a.getInt("MENU_MODE", -1);
            if (i != -1) {
                this.menuMode = com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode.values()[i];
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("menu_mode:").append(this.menuMode).toString());
            }
        }
        this.updateView(a);
        super.onLoad(a);
    }
    
    @Subscribe
    public void onMapEngineReady(com.navdy.hud.app.maps.MapEvents$MapEngineReady a) {
        if (this.currentMenu != null && this.menuMode == com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode.MAIN_MENU) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v("onMapEngineReady");
            if (com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$1.$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu[this.currentMenu.getType().ordinal()] != 0) {
                this.loadMenu(this.currentMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.REFRESH_CURRENT, 0, 0);
            }
        }
    }
    
    @Subscribe
    public void onNavigationModeChanged(com.navdy.hud.app.maps.MapEvents$NavigationModeChange a) {
        if (this.currentMenu != null && this.menuMode == com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode.MAIN_MENU) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("onNavigationModeChanged:").append(a.navigationMode).toString());
            switch(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$1.$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu[this.currentMenu.getType().ordinal()]) {
                case 2: {
                    this.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.mainMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.ROOT, 0, 0);
                    break;
                }
                case 1: {
                    this.loadMenu(this.currentMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.REFRESH_CURRENT, 0, 0);
                    break;
                }
            }
        }
    }
    
    public void onUnload() {
        if (this.registered) {
            this.registered = false;
            this.bus.unregister(this);
        }
        this.reset();
        this.mainMenu = null;
        this.currentMenu = null;
        super.onUnload();
    }
    
    void performSelectionAnimation(Runnable a) {
        this.performSelectionAnimation(a, 0);
    }
    
    void performSelectionAnimation(Runnable a, int i) {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a0 = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        if (a0 != null) {
            a0.performSelectionAnimation(a, i);
        }
    }
    
    void refreshData(int i, com.navdy.hud.app.ui.component.vlist.VerticalList$Model[] a, com.navdy.hud.app.ui.component.mainmenu.IMenu a0) {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a1 = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        if (a1 != null) {
            if (this.closed) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v("refreshData(s) already closed");
            } else if (this.currentMenu != a0) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("refreshData(s) update menu:").append(a0.getType()).append(" different from current:").append(this.currentMenu.getType()).toString());
            } else {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("refreshData(s):").append(a0.getType()).toString());
                a1.vmenuComponent.verticalList.refreshData(i, a);
            }
        }
    }
    
    void refreshDataforPos(int i) {
        this.refreshDataforPos(i, true);
    }
    
    void refreshDataforPos(int i, boolean b) {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        if (a != null) {
            a.vmenuComponent.verticalList.refreshData(i, (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)null, !b);
        }
    }
    
    void reset() {
        this.closed = false;
        this.handledSelection = false;
        this.animateIn = false;
        this.firstLoad = false;
        this.mapShown = false;
        this.navEnded = false;
    }
    
    void resetSelectedItem() {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v("resetSelectedItem");
        this.handledSelection = false;
        if (!this.animateIn) {
            this.animateIn = true;
            com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
            if (a != null) {
                a.vmenuComponent.animateIn((android.animation.Animator$AnimatorListener)null);
            }
        }
    }
    
    boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        boolean b = false;
        if (this.currentMenu != null) {
            if (this.handledSelection) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("already handled [").append(a.id).append("], ").append(a.pos).toString());
                b = true;
            } else {
                this.handledSelection = this.currentMenu.selectItem(a);
                b = this.handledSelection;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    void sendCloseEvent() {
        this.hideMap();
        if (this.currentMenu != this.mainMenu) {
            this.currentMenu.onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.CLOSE);
        }
        if (this.mainMenu != null) {
            this.mainMenu.onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.CLOSE);
        }
    }
    
    void setInitialItemState(boolean b) {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        if (a != null) {
            a.vmenuComponent.verticalList.adapter.setInitialState(b);
        }
    }
    
    public void setNavEnded() {
        this.navEnded = true;
    }
    
    void setScrollIdleEvents(boolean b) {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        if (a != null) {
            a.vmenuComponent.verticalList.setScrollIdleEvent(b);
        }
    }
    
    public void setViewBackgroundColor(int i) {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        if (a != null) {
            a.setBackgroundColor(i);
        }
    }
    
    public void showBoundingBox(com.here.android.mpa.common.GeoBoundingBox a) {
        if ((com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView() != null && this.mapShown && a != null) {
            this.uiStateManager.getNavigationView().zoomToBoundBox(a, (com.here.android.mpa.routing.Route)null, false, false);
        }
    }
    
    public void showMap() {
        if (this.mapShown) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v("showMap: already shown");
        } else {
            this.mapShown = true;
            if ((com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView() != null) {
                com.here.android.mpa.routing.Route a = null;
                com.here.android.mpa.common.GeoBoundingBox a0 = null;
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v("showMap");
                com.navdy.hud.app.ui.component.homescreen.HomeScreenView a1 = this.uiStateManager.getHomescreenView();
                com.navdy.hud.app.ui.component.homescreen.NavigationView a2 = this.uiStateManager.getNavigationView();
                this.switchBackMode = a1.getDisplayMode();
                if (this.switchBackMode != com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP) {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append(" showMap switchbackmode: ").append(this.switchBackMode).toString());
                    a1.setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP);
                } else {
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v("showMap: switchbackmode null");
                    this.switchBackMode = null;
                }
                com.navdy.hud.app.maps.here.HereNavigationManager a3 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
                com.here.android.mpa.common.GeoCoordinate a4 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                com.navdy.service.library.events.navigation.NavigationRouteRequest a5 = a3.getCurrentNavigationRouteRequest();
                com.here.android.mpa.common.GeoCoordinate a6 = null;
                label5: if (a5 != null) {
                    double d = a5.destination.latitude.doubleValue();
                    int i = (d > 0.0) ? 1 : (d == 0.0) ? 0 : -1;
                    label4: {
                        if (i == 0) {
                            break label4;
                        }
                        if (a5.destination.longitude.doubleValue() == 0.0) {
                            break label4;
                        }
                        a6 = new com.here.android.mpa.common.GeoCoordinate(a5.destination.latitude.doubleValue(), a5.destination.longitude.doubleValue());
                        break label5;
                    }
                    com.navdy.service.library.events.location.Coordinate a7 = a5.destinationDisplay;
                    a6 = null;
                    if (a7 != null) {
                        double d0 = a5.destinationDisplay.latitude.doubleValue();
                        int i0 = (d0 > 0.0) ? 1 : (d0 == 0.0) ? 0 : -1;
                        a6 = null;
                        if (i0 != 0) {
                            double d1 = a5.destinationDisplay.longitude.doubleValue();
                            int i1 = (d1 > 0.0) ? 1 : (d1 == 0.0) ? 0 : -1;
                            a6 = null;
                            if (i1 != 0) {
                                a6 = new com.here.android.mpa.common.GeoCoordinate(a5.destinationDisplay.latitude.doubleValue(), a5.destinationDisplay.longitude.doubleValue());
                            }
                        }
                    }
                }
                String s = a3.getCurrentRouteId();
                label3: {
                    com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a8 = null;
                    label1: {
                        label2: {
                            if (s == null) {
                                break label2;
                            }
                            a8 = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(s);
                            if (a8 != null) {
                                break label1;
                            }
                        }
                        a = null;
                        a0 = null;
                        break label3;
                    }
                    a = a8.route;
                    a0 = a8.route.getBoundingBox();
                }
                label0: {
                    Throwable a9 = null;
                    if (a0 != null) {
                        break label0;
                    }
                    if (a4 == null) {
                        break label0;
                    }
                    try {
                        a0 = new com.here.android.mpa.common.GeoBoundingBox(a4, 5000f, 5000f);
                        break label0;
                    } catch(Throwable a10) {
                        a9 = a10;
                    }
                    com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().e(a9);
                }
                a2.switchToRouteSearchMode(a4, a6, a0, a, false, (java.util.List)null, -1);
                this.enableMapViews();
            }
        }
    }
    
    public void showScrimCover() {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        if (a != null) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v("scrim-show");
            a.scrimCover.setVisibility(0);
        }
    }
    
    public void showToolTip(int i, String s) {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        if (a != null) {
            a.vmenuComponent.showToolTip(i, s);
        }
    }
    
    void updateCurrentMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu a) {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a0 = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        if (a0 != null) {
            if (this.closed) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v("updateCurrentMenu already closed");
            } else if (this.currentMenu != a) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("updateCurrentMenu update menu:").append(a.getType()).append(" different from current:").append(this.currentMenu.getType()).toString());
            } else {
                com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("updateCurrentMenu:").append(a.getType()).toString());
                a0.vmenuComponent.unlock(false);
                a0.vmenuComponent.verticalList.setBindCallbacks(this.currentMenu.isBindCallsEnabled());
                a0.vmenuComponent.updateView(this.currentMenu.getItems(), this.currentMenu.getInitialSelection(), true, false, this.currentMenu.getScrollIndex());
            }
        }
    }
    
    protected void updateView(android.os.Bundle a) {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a0 = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)this.getView();
        if (a0 != null) {
            this.init(a0, a);
        }
    }
}
