package com.navdy.hud.app.ui.component.tbt;

final class TbtTextInstructionView$WidthInfo {
    private int lineWidth1;
    private int lineWidth2;
    private int numLines;
    
    public TbtTextInstructionView$WidthInfo() {
    }
    
    final public void clear() {
        this.numLines = 0;
        this.lineWidth1 = 0;
        this.lineWidth2 = 0;
    }
    
    final public int getLineWidth1$app_hudRelease() {
        return this.lineWidth1;
    }
    
    final public int getLineWidth2$app_hudRelease() {
        return this.lineWidth2;
    }
    
    final public int getNumLines$app_hudRelease() {
        return this.numLines;
    }
    
    final public void setLineWidth1$app_hudRelease(int i) {
        this.lineWidth1 = i;
    }
    
    final public void setLineWidth2$app_hudRelease(int i) {
        this.lineWidth2 = i;
    }
    
    final public void setNumLines$app_hudRelease(int i) {
        this.numLines = i;
    }
}
