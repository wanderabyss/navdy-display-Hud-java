package com.navdy.hud.app.ui.component;
import com.navdy.hud.app.R;

public class ChoiceLayout extends android.widget.FrameLayout {
    final private static int ANIMATION_EXECUTE_DURATION = 200;
    final private static int ANIMATION_SLIDE_DURATION = 100;
    final private static Object ANIMATION_TAG;
    final private static String EMPTY = "";
    final private static int KEY_UP_DOWN_DURATION = 50;
    private static int MIN_HIGHLIGHT_VIEW_WIDTH;
    private static int choiceHighlightColor;
    private static int choiceHighlightMargin;
    private static int choiceTextPaddingBottomDefault;
    private static int choiceTextPaddingDefault;
    private static int choiceTextSizeDefault;
    private static int textDeselectionColor;
    private static int textSelectionColor;
    private android.view.ViewTreeObserver$OnGlobalLayoutListener animationLayoutListener;
    private android.animation.AnimatorSet animatorSet;
    protected android.widget.LinearLayout choiceContainer;
    private int choiceTextPaddingBottom;
    private int choiceTextPaddingLeft;
    private int choiceTextPaddingRight;
    private int choiceTextPaddingTop;
    private int choiceTextSize;
    protected java.util.List choices;
    private com.navdy.hud.app.ui.component.ChoiceLayout$Mode currentMode;
    private android.view.View currentSelectionView;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener executeAnimationListener;
    private android.animation.ValueAnimator$AnimatorUpdateListener executeUpdateListener;
    private android.view.View executeView;
    private boolean executing;
    private android.os.Handler handler;
    private boolean highlightPersistent;
    protected android.view.View highlightView;
    private android.animation.Animator$AnimatorListener highlightViewListener;
    private int highlightVisibility;
    private boolean initialized;
    private com.navdy.hud.app.ui.component.ChoiceLayout$IListener listener;
    private android.view.View oldSelectionView;
    private java.util.Queue operationQueue;
    private boolean operationRunning;
    private boolean pressedDown;
    private int selectedItem;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener upDownAnimationListener;
    private android.animation.ValueAnimator$AnimatorUpdateListener upDownUpdateListener;
    
    static {
        ANIMATION_TAG = new Object();
        textSelectionColor = -1;
    }
    
    public ChoiceLayout(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public ChoiceLayout(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public ChoiceLayout(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.highlightPersistent = false;
        this.highlightVisibility = 0;
        this.listener = null;
        this.selectedItem = -1;
        this.handler = new android.os.Handler();
        this.operationQueue = (java.util.Queue)new java.util.LinkedList();
        this.upDownAnimationListener = new com.navdy.hud.app.ui.component.ChoiceLayout$1(this);
        this.upDownUpdateListener = (android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.ChoiceLayout$2(this);
        this.executeAnimationListener = new com.navdy.hud.app.ui.component.ChoiceLayout$3(this);
        this.executeUpdateListener = (android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.ChoiceLayout$4(this);
        this.animationLayoutListener = (android.view.ViewTreeObserver$OnGlobalLayoutListener)new com.navdy.hud.app.ui.component.ChoiceLayout$5(this);
        this.highlightViewListener = (android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.ChoiceLayout$6(this);
        this.init();
        android.content.res.TypedArray a1 = a.obtainStyledAttributes(a0, com.navdy.hud.app.R$styleable.ChoiceLayout, i, 0);
        if (a1 != null) {
            this.choiceTextPaddingLeft = (int)a1.getDimension(0, (float)choiceTextPaddingDefault);
            this.choiceTextPaddingRight = (int)a1.getDimension(1, (float)choiceTextPaddingDefault);
            this.choiceTextPaddingBottom = (int)a1.getDimension(3, (float)choiceTextPaddingBottomDefault);
            this.choiceTextPaddingTop = (int)a1.getDimension(2, (float)choiceTextPaddingDefault);
            this.choiceTextSize = (int)a1.getDimension(4, (float)choiceTextSizeDefault);
            a1.recycle();
        }
    }
    
    static void access$000(com.navdy.hud.app.ui.component.ChoiceLayout a) {
        a.runQueuedOperation();
    }
    
    static android.view.View access$100(com.navdy.hud.app.ui.component.ChoiceLayout a) {
        return a.executeView;
    }
    
    static int access$1000() {
        return textSelectionColor;
    }
    
    static android.view.View access$1100(com.navdy.hud.app.ui.component.ChoiceLayout a) {
        return a.currentSelectionView;
    }
    
    static void access$1200(com.navdy.hud.app.ui.component.ChoiceLayout a, int i) {
        a.selectInitialItem(i);
    }
    
    static android.view.View access$1300(com.navdy.hud.app.ui.component.ChoiceLayout a) {
        return a.oldSelectionView;
    }
    
    static com.navdy.hud.app.ui.component.ChoiceLayout$Mode access$1400(com.navdy.hud.app.ui.component.ChoiceLayout a) {
        return a.currentMode;
    }
    
    static int access$1500() {
        return textDeselectionColor;
    }
    
    static void access$1600(com.navdy.hud.app.ui.component.ChoiceLayout a, int i) {
        a.callSelectListener(i);
    }
    
    static void access$1700(com.navdy.hud.app.ui.component.ChoiceLayout a, com.navdy.hud.app.ui.component.ChoiceLayout$Operation a0, boolean b) {
        a.runOperation(a0, b);
    }
    
    static int access$200() {
        return choiceHighlightColor;
    }
    
    static int access$300() {
        return choiceHighlightMargin;
    }
    
    static android.animation.ValueAnimator$AnimatorUpdateListener access$400(com.navdy.hud.app.ui.component.ChoiceLayout a) {
        return a.executeUpdateListener;
    }
    
    static com.navdy.hud.app.ui.framework.DefaultAnimationListener access$500(com.navdy.hud.app.ui.component.ChoiceLayout a) {
        return a.executeAnimationListener;
    }
    
    static android.animation.AnimatorSet access$600(com.navdy.hud.app.ui.component.ChoiceLayout a) {
        return a.animatorSet;
    }
    
    static android.animation.AnimatorSet access$602(com.navdy.hud.app.ui.component.ChoiceLayout a, android.animation.AnimatorSet a0) {
        a.animatorSet = a0;
        return a0;
    }
    
    static boolean access$702(com.navdy.hud.app.ui.component.ChoiceLayout a, boolean b) {
        a.executing = b;
        return b;
    }
    
    static int access$800(com.navdy.hud.app.ui.component.ChoiceLayout a) {
        return a.selectedItem;
    }
    
    static void access$900(com.navdy.hud.app.ui.component.ChoiceLayout a, int i) {
        a.callListener(i);
    }
    
    private android.widget.TextView buildChoice(String s, int i) {
        android.widget.TextView a = new android.widget.TextView(this.getContext());
        a.setGravity(17);
        a.setSingleLine(true);
        a.setText((CharSequence)s);
        a.setIncludeFontPadding(false);
        a.setTextSize(0, (float)this.choiceTextSize);
        a.setTypeface((android.graphics.Typeface)null, 1);
        a.setTextColor(textDeselectionColor);
        return a;
    }
    
    private void buildChoices() {
        this.choiceContainer.removeAllViews();
        this.currentSelectionView = null;
        this.oldSelectionView = null;
        this.highlightView.setVisibility(4);
        if (this.choices != null) {
            int i = this.choices.size();
            int i0 = 0;
            while(i0 < i) {
                android.view.View a = null;
                com.navdy.hud.app.ui.component.ChoiceLayout$Choice a0 = (com.navdy.hud.app.ui.component.ChoiceLayout$Choice)this.choices.get(i0);
                if (this.currentMode != com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL) {
                    android.widget.ImageView a1 = new android.widget.ImageView(this.getContext());
                    a1.setScaleType(android.widget.ImageView$ScaleType.FIT_CENTER);
                    a1.setImageResource(a0.icon.resIdUnSelected);
                    a1.setColorFilter(textDeselectionColor);
                    a = a1;
                } else {
                    android.widget.TextView a2 = this.buildChoice("", i0);
                    a2.setText((CharSequence)a0.label);
                    a = a2;
                }
                android.widget.LinearLayout$LayoutParams a3 = new android.widget.LinearLayout$LayoutParams(-2, -2);
                a3.setMargins(this.choiceTextPaddingLeft, 0, this.choiceTextPaddingRight, 0);
                this.choiceContainer.addView(a, (android.view.ViewGroup$LayoutParams)a3);
                i0 = i0 + 1;
            }
        }
        this.invalidate();
        this.requestLayout();
    }
    
    private void callListener(int i) {
        if (this.listener != null) {
            com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils$Sound.MENU_SELECT);
            if (this.choices != null && i >= 0 && i < this.choices.size()) {
                this.listener.executeItem(i, ((com.navdy.hud.app.ui.component.ChoiceLayout$Choice)this.choices.get(i)).id);
            }
        }
    }
    
    private void callSelectListener(int i) {
        if (this.listener != null && this.choices != null && i >= 0 && i < this.choices.size()) {
            this.listener.itemSelected(i, ((com.navdy.hud.app.ui.component.ChoiceLayout$Choice)this.choices.get(i)).id);
        }
    }
    
    private boolean executeSelectedItemInternal() {
        boolean b = false;
        boolean b0 = this.initialized;
        label3: {
            label4: {
                label5: {
                    if (!b0) {
                        break label5;
                    }
                    if (!this.executing) {
                        break label4;
                    }
                }
                b = false;
                break label3;
            }
            int i = this.selectedItem;
            label0: {
                label2: {
                    if (i == -1) {
                        break label2;
                    }
                    if (this.getItemCount() <= 0) {
                        break label2;
                    }
                    this.executing = true;
                    int i0 = this.highlightVisibility;
                    label1: {
                        if (i0 == 0) {
                            break label1;
                        }
                        this.callListener(this.selectedItem);
                        this.executing = false;
                        break label2;
                    }
                    this.executeView = this.choiceContainer.getChildAt(this.selectedItem);
                    if (this.executeView != null) {
                        break label0;
                    }
                    this.callListener(this.selectedItem);
                    this.executing = false;
                }
                b = false;
                break label3;
            }
            this.executeView.setTag(ANIMATION_TAG);
            int[] a = new int[2];
            a[0] = choiceHighlightMargin;
            a[1] = 0;
            android.animation.ValueAnimator a0 = android.animation.ValueAnimator.ofInt(a);
            a0.addListener((android.animation.Animator$AnimatorListener)this.executeAnimationListener);
            a0.addUpdateListener(this.executeUpdateListener);
            this.animatorSet = new android.animation.AnimatorSet();
            this.animatorSet.setDuration(200L);
            this.animatorSet.play((android.animation.Animator)a0);
            this.animatorSet.start();
            b = true;
        }
        return b;
    }
    
    private int getItemCount() {
        return (this.choices == null) ? 0 : this.choices.size();
    }
    
    private float getXPositionForHighlight(android.view.View a) {
        int i = a.getMeasuredWidth() - (a.getPaddingLeft() + a.getPaddingRight());
        int i0 = a.getLeft() + a.getPaddingLeft();
        return (i < MIN_HIGHLIGHT_VIEW_WIDTH) ? (float)(i0 - (MIN_HIGHLIGHT_VIEW_WIDTH - i) / 2) : (float)i0;
    }
    
    private void init() {
        android.view.LayoutInflater.from(this.getContext()).inflate(R.layout.choices_lyt, (android.view.ViewGroup)this, true);
        this.invalidate();
        this.choiceContainer = (android.widget.LinearLayout)this.findViewById(R.id.choiceContainer);
        this.highlightView = this.findViewById(R.id.choiceHighlight);
        if (textSelectionColor == -1) {
            android.content.res.Resources a = this.getResources();
            MIN_HIGHLIGHT_VIEW_WIDTH = a.getDimensionPixelSize(R.dimen.min_highlight_view_width);
            textSelectionColor = a.getColor(R.color.choice_selection);
            textDeselectionColor = a.getColor(R.color.choice_deselection);
            choiceTextPaddingDefault = (int)a.getDimension(R.dimen.choices_lyt_item_padding);
            choiceTextPaddingBottomDefault = (int)a.getDimension(R.dimen.choices_lyt_item_padding_bottom);
            choiceTextSizeDefault = (int)a.getDimension(R.dimen.choices_lyt_text_size);
            choiceHighlightColor = a.getColor(R.color.cyan);
            choiceHighlightMargin = (int)a.getDimension(R.dimen.choices_lyt_highlight_margin);
        }
    }
    
    private boolean keyDownSelectedItemInternal() {
        boolean b = false;
        boolean b0 = this.initialized;
        label2: {
            label3: {
                label4: {
                    if (!b0) {
                        break label4;
                    }
                    if (!this.pressedDown) {
                        break label3;
                    }
                }
                b = false;
                break label2;
            }
            int i = this.selectedItem;
            label0: {
                label1: {
                    if (i == -1) {
                        break label1;
                    }
                    if (this.getItemCount() <= 0) {
                        break label1;
                    }
                    if (this.currentMode != com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL) {
                        ((android.widget.ImageView)this.currentSelectionView).setColorFilter(choiceHighlightColor);
                    } else {
                        ((android.widget.TextView)this.currentSelectionView).setTextColor(choiceHighlightColor);
                    }
                    if (this.highlightVisibility != 0) {
                        break label1;
                    }
                    this.pressedDown = true;
                    if (this.choiceContainer.getChildAt(this.selectedItem) != null) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            int[] a = new int[2];
            a[0] = choiceHighlightMargin;
            a[1] = 0;
            android.animation.ValueAnimator a0 = android.animation.ValueAnimator.ofInt(a);
            a0.addListener((android.animation.Animator$AnimatorListener)this.upDownAnimationListener);
            a0.addUpdateListener(this.upDownUpdateListener);
            this.animatorSet = new android.animation.AnimatorSet();
            this.animatorSet.setDuration(50L);
            this.animatorSet.play((android.animation.Animator)a0);
            this.animatorSet.start();
            b = true;
        }
        return b;
    }
    
    private boolean keyUpSelectedItemInternal() {
        boolean b = false;
        boolean b0 = this.initialized;
        label2: {
            label3: {
                label4: {
                    if (!b0) {
                        break label4;
                    }
                    if (this.pressedDown) {
                        break label3;
                    }
                }
                b = false;
                break label2;
            }
            int i = this.selectedItem;
            label0: {
                label1: {
                    if (i == -1) {
                        break label1;
                    }
                    if (this.getItemCount() <= 0) {
                        break label1;
                    }
                    if (this.currentMode != com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL) {
                        ((android.widget.ImageView)this.currentSelectionView).setColorFilter(textSelectionColor);
                    } else {
                        ((android.widget.TextView)this.currentSelectionView).setTextColor(textSelectionColor);
                    }
                    if (this.highlightVisibility != 0) {
                        break label1;
                    }
                    this.pressedDown = false;
                    if (this.choiceContainer.getChildAt(this.selectedItem) != null) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            int[] a = new int[2];
            a[0] = 0;
            a[1] = choiceHighlightMargin;
            android.animation.ValueAnimator a0 = android.animation.ValueAnimator.ofInt(a);
            a0.addListener((android.animation.Animator$AnimatorListener)this.upDownAnimationListener);
            a0.addUpdateListener(this.upDownUpdateListener);
            this.animatorSet = new android.animation.AnimatorSet();
            this.animatorSet.setDuration(50L);
            this.animatorSet.play((android.animation.Animator)a0);
            this.animatorSet.start();
            b = true;
        }
        return b;
    }
    
    private boolean moveSelectionLeftInternal() {
        return this.initialized && !this.executing && this.selectedItem != -1 && this.setSelectedItem(this.selectedItem - 1);
    }
    
    private boolean moveSelectionRightInternal() {
        return this.initialized && !this.executing && this.selectedItem != -1 && this.setSelectedItem(this.selectedItem + 1);
    }
    
    private void runOperation(com.navdy.hud.app.ui.component.ChoiceLayout$Operation a, boolean b) {
        label1: {
            label0: {
                if (b) {
                    break label0;
                }
                if (!this.operationRunning) {
                    break label0;
                }
                this.operationQueue.add(a);
                break label1;
            }
            this.operationRunning = true;
            switch(com.navdy.hud.app.ui.component.ChoiceLayout$9.$SwitchMap$com$navdy$hud$app$ui$component$ChoiceLayout$Operation[a.ordinal()]) {
                case 5: {
                    if (this.keyUpSelectedItemInternal()) {
                        break;
                    }
                    this.runQueuedOperation();
                    break;
                }
                case 4: {
                    if (this.keyDownSelectedItemInternal()) {
                        break;
                    }
                    this.runQueuedOperation();
                    break;
                }
                case 3: {
                    if (this.executeSelectedItemInternal()) {
                        break;
                    }
                    this.runQueuedOperation();
                    break;
                }
                case 2: {
                    if (this.moveSelectionRightInternal()) {
                        break;
                    }
                    this.runQueuedOperation();
                    break;
                }
                case 1: {
                    if (this.moveSelectionLeftInternal()) {
                        break;
                    }
                    this.runQueuedOperation();
                    break;
                }
            }
        }
    }
    
    private void runQueuedOperation() {
        if (this.operationQueue.size() <= 0) {
            this.operationRunning = false;
        } else {
            com.navdy.hud.app.ui.component.ChoiceLayout$Operation a = (com.navdy.hud.app.ui.component.ChoiceLayout$Operation)this.operationQueue.remove();
            this.handler.post((Runnable)new com.navdy.hud.app.ui.component.ChoiceLayout$8(this, a));
        }
    }
    
    private void selectInitialItem(int i) {
        if (i != -1) {
            android.view.View a = this.choiceContainer.getChildAt(i);
            if (a.getMeasuredWidth() != 0) {
                int i0 = (int)this.getXPositionForHighlight(a);
                ((android.widget.FrameLayout$LayoutParams)this.highlightView.getLayoutParams()).width = Math.max(a.getMeasuredWidth() - (a.getPaddingLeft() + a.getPaddingRight()), MIN_HIGHLIGHT_VIEW_WIDTH);
                this.highlightView.setX((float)i0);
                this.highlightView.setVisibility(this.highlightVisibility);
                this.highlightView.invalidate();
                this.highlightView.requestLayout();
                this.currentSelectionView = a;
                this.currentSelectionView.setTag(Integer.valueOf(i));
                if (this.currentMode != com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL) {
                    ((android.widget.ImageView)this.currentSelectionView).setColorFilter(textSelectionColor);
                } else {
                    ((android.widget.TextView)this.currentSelectionView).setTextColor(textSelectionColor);
                }
                this.initialized = true;
            } else {
                a.getViewTreeObserver().addOnGlobalLayoutListener(this.animationLayoutListener);
            }
        }
    }
    
    public void clearOperationQueue() {
        if (this.animatorSet != null && this.animatorSet.isRunning()) {
            this.animatorSet.removeAllListeners();
            this.animatorSet.cancel();
            this.animatorSet = null;
        }
        this.operationQueue.clear();
        this.operationRunning = false;
    }
    
    public void executeSelectedItem(boolean b) {
        this.runOperation(com.navdy.hud.app.ui.component.ChoiceLayout$Operation.EXECUTE, false);
    }
    
    public java.util.List getChoices() {
        return this.choices;
    }
    
    public com.navdy.hud.app.ui.component.ChoiceLayout$Choice getSelectedItem() {
        com.navdy.hud.app.ui.component.ChoiceLayout$Choice a = null;
        java.util.List a0 = this.choices;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.selectedItem < 0) {
                        break label1;
                    }
                    if (this.selectedItem < this.choices.size()) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = (com.navdy.hud.app.ui.component.ChoiceLayout$Choice)this.choices.get(this.selectedItem);
        }
        return a;
    }
    
    public int getSelectedItemIndex() {
        return this.selectedItem;
    }
    
    public boolean hasChoiceId(int i) {
        Object a = this.choices.iterator();
        while(true) {
            boolean b = false;
            if (((java.util.Iterator)a).hasNext()) {
                if (((com.navdy.hud.app.ui.component.ChoiceLayout$Choice)((java.util.Iterator)a).next()).id != i) {
                    continue;
                }
                b = true;
            } else {
                b = false;
            }
            return b;
        }
    }
    
    protected boolean isHighlightPersistent() {
        return this.highlightPersistent;
    }
    
    public void keyDownSelectedItem() {
        this.runOperation(com.navdy.hud.app.ui.component.ChoiceLayout$Operation.KEY_DOWN, false);
    }
    
    public void keyUpSelectedItem() {
        this.runOperation(com.navdy.hud.app.ui.component.ChoiceLayout$Operation.KEY_UP, false);
    }
    
    public void moveSelectionLeft() {
        this.runOperation(com.navdy.hud.app.ui.component.ChoiceLayout$Operation.MOVE_LEFT, false);
    }
    
    public void moveSelectionRight() {
        this.runOperation(com.navdy.hud.app.ui.component.ChoiceLayout$Operation.MOVE_RIGHT, false);
    }
    
    public void resetAnimationElements() {
        if (this.highlightVisibility == 0 && this.highlightView != null) {
            this.highlightView.setVisibility(0);
        }
        if (this.executeView != null) {
            if (this.executeView instanceof android.widget.TextView) {
                ((android.widget.TextView)this.executeView).setTextColor(textSelectionColor);
                this.executeView = null;
            } else if (this.executeView instanceof android.widget.ImageView) {
                ((android.widget.ImageView)this.executeView).setColorFilter(textSelectionColor);
                this.executeView = null;
            }
        }
    }
    
    public void setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode a, java.util.List a0, int i, com.navdy.hud.app.ui.component.ChoiceLayout$IListener a1) {
        this.currentMode = a;
        this.choices = a0;
        this.listener = a1;
        this.initialized = false;
        com.navdy.hud.app.ui.component.ChoiceLayout$Operation a2 = (com.navdy.hud.app.ui.component.ChoiceLayout$Operation)this.operationQueue.peek();
        this.executeView = null;
        this.clearOperationQueue();
        this.executing = false;
        this.selectedItem = -1;
        label1: if (a0 != null && a0.size() > 0) {
            label0: {
                if (i < 0) {
                    break label0;
                }
                if (i >= a0.size()) {
                    break label0;
                }
                this.selectedItem = i;
                break label1;
            }
            this.selectedItem = 0;
        }
        this.buildChoices();
        this.selectInitialItem(this.selectedItem);
        if (a2 == com.navdy.hud.app.ui.component.ChoiceLayout$Operation.KEY_UP) {
            ((android.widget.FrameLayout$LayoutParams)this.highlightView.getLayoutParams()).topMargin = choiceHighlightMargin;
        }
    }
    
    public void setHighlightPersistent(boolean b) {
        this.highlightPersistent = b;
    }
    
    public void setHighlightVisibility(int i) {
        this.highlightVisibility = i;
        this.invalidate();
    }
    
    public void setItemPadding(int i) {
        this.choiceTextPaddingLeft = i;
        this.choiceTextPaddingRight = i;
    }
    
    public void setLabelTextSize(int i) {
        this.choiceTextSize = i;
    }
    
    protected boolean setSelectedItem(int i) {
        boolean b = false;
        if (this.selectedItem != i) {
            int i0 = this.getItemCount();
            if (i < 0) {
                b = false;
            } else if (i >= i0) {
                b = false;
            } else {
                this.selectedItem = i;
                android.view.View a = this.choiceContainer.getChildAt(i);
                this.oldSelectionView = this.currentSelectionView;
                this.currentSelectionView = a;
                this.currentSelectionView.setTag(Integer.valueOf(i));
                int i1 = Math.max(a.getMeasuredWidth(), MIN_HIGHLIGHT_VIEW_WIDTH);
                if (i1 == 0) {
                    b = false;
                } else {
                    int i2 = i1 - (a.getPaddingLeft() + a.getPaddingRight());
                    int i3 = (this.oldSelectionView == null) ? i2 : Math.max(this.oldSelectionView.getMeasuredWidth() - (this.oldSelectionView.getPaddingLeft() + this.oldSelectionView.getPaddingRight()), MIN_HIGHLIGHT_VIEW_WIDTH);
                    this.animatorSet = new android.animation.AnimatorSet();
                    android.view.View a0 = this.highlightView;
                    float[] a1 = new float[1];
                    a1[0] = this.getXPositionForHighlight(a);
                    android.animation.ObjectAnimator a2 = android.animation.ObjectAnimator.ofFloat(a0, "x", a1);
                    int[] a3 = new int[2];
                    a3[0] = i3;
                    a3[1] = i2;
                    android.animation.ValueAnimator a4 = android.animation.ValueAnimator.ofInt(a3);
                    a4.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.ChoiceLayout$7(this));
                    this.highlightView.setVisibility(this.highlightVisibility);
                    this.highlightView.invalidate();
                    this.highlightView.requestLayout();
                    android.animation.AnimatorSet a5 = this.animatorSet;
                    android.animation.Animator[] a6 = new android.animation.Animator[2];
                    a6[0] = a2;
                    a6[1] = a4;
                    a5.playTogether(a6);
                    this.animatorSet.setDuration(100L);
                    this.animatorSet.addListener(this.highlightViewListener);
                    this.animatorSet.start();
                    com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils$Sound.MENU_MOVE);
                    b = true;
                }
            }
        } else {
            b = false;
        }
        return b;
    }
}
