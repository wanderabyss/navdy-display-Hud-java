package com.navdy.hud.app.ui.component.vlist;

public class VerticalModelCache {
    final private static java.util.Map map;
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.vlist.VerticalModelCache.class);
        map = (java.util.Map)new java.util.HashMap();
        map.put(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON_BKCOLOR, new com.navdy.hud.app.ui.component.vlist.VerticalModelCache$CacheEntry(500, 100));
        map.put(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.TWO_ICONS, new com.navdy.hud.app.ui.component.vlist.VerticalModelCache$CacheEntry(70, 10));
        map.put(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.LOADING_CONTENT, new com.navdy.hud.app.ui.component.vlist.VerticalModelCache$CacheEntry(500, 100));
    }
    
    public VerticalModelCache() {
    }
    
    public static void addToCache(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType a, com.navdy.hud.app.ui.component.vlist.VerticalList$Model a0) {
        if (a0 != null && a != null) {
            com.navdy.hud.app.ui.component.vlist.VerticalModelCache$CacheEntry a1 = (com.navdy.hud.app.ui.component.vlist.VerticalModelCache$CacheEntry)map.get(a);
            if (a1 != null && a1.items.size() < a1.maxItems) {
                a0.clear();
                a1.items.add(a0);
            }
        }
    }
    
    public static void addToCache(java.util.List a) {
        if (a != null) {
            Object a0 = a.iterator();
            while(((java.util.Iterator)a0).hasNext()) {
                com.navdy.hud.app.ui.component.vlist.VerticalList$Model a1 = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)((java.util.Iterator)a0).next();
                com.navdy.hud.app.ui.component.vlist.VerticalModelCache.addToCache(a1.type, a1);
            }
        }
    }
    
    public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model getFromCache(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType a) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a0 = null;
        if (a != null) {
            com.navdy.hud.app.ui.component.vlist.VerticalModelCache$CacheEntry a1 = (com.navdy.hud.app.ui.component.vlist.VerticalModelCache$CacheEntry)map.get(a);
            a0 = null;
            if (a1 != null) {
                int i = a1.items.size();
                a0 = null;
                if (i > 0) {
                    a0 = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)a1.items.remove(0);
                }
            }
        } else {
            a0 = null;
        }
        return a0;
    }
}
