package com.navdy.hud.app.ui.component.carousel;

class FastScrollAnimator$2 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.carousel.FastScrollAnimator this$0;
    final android.animation.Animator$AnimatorListener val$listener;
    final int val$newPos;
    final boolean val$skipEnd;
    
    FastScrollAnimator$2(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator a, boolean b, int i, android.animation.Animator$AnimatorListener a0) {
        super();
        this.this$0 = a;
        this.val$skipEnd = b;
        this.val$newPos = i;
        this.val$listener = a0;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$002(this.this$0, false);
        if (!this.val$skipEnd) {
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).currentItem = this.val$newPos;
            android.view.View a0 = com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$100(this.this$0);
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$102(this.this$0, com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).middleLeftView);
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$100(this.this$0).setVisibility(4);
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$100(this.this$0).setAlpha(0.0f);
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).middleLeftView = a0;
            if (this.val$listener != null) {
                this.val$listener.onAnimationEnd(a);
            }
            com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).selectedItemView = com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).middleLeftView;
            if (com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).carouselIndicator != null) {
                com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).carouselIndicator.setCurrentItem(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).currentItem);
            }
            if (com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).itemChangeListener != null) {
                com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).itemChangeListener.onCurrentItemChanged(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).currentItem, ((com.navdy.hud.app.ui.component.carousel.Carousel$Model)com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).model.get(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).currentItem)).id);
            }
        }
        com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.access$200(this.this$0).runQueuedOperation();
    }
}
