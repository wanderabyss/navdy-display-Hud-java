package com.navdy.hud.app.ui.component.carousel;
import com.navdy.hud.app.R;

public class CarouselIndicator extends android.widget.FrameLayout {
    final public static int PROGRESS_INDICATOR_HORIZONTAL_THRESHOLD = 12;
    final public static int PROGRESS_INDICATOR_VERTICAL_THRESHOLD = 8;
    private int barParentSize;
    private int barSize;
    private int circleFocusSize;
    private int circleMargin;
    private int circleSize;
    private int currentItemPaddingRadius;
    private boolean fullBackground;
    private int itemCount;
    private int itemPadding;
    private int itemRadius;
    private com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation orientation;
    private com.navdy.hud.app.ui.component.carousel.IProgressIndicator progressIndicator;
    private int roundRadius;
    private int viewPadding;
    
    public CarouselIndicator(android.content.Context a) {
        super(a, (android.util.AttributeSet)null);
        this.barSize = -1;
        this.barParentSize = -1;
        this.orientation = com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL;
        this.init(a, (android.util.AttributeSet)null);
    }
    
    public CarouselIndicator(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
        this.barSize = -1;
        this.barParentSize = -1;
        this.orientation = com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL;
        this.init(a, a0);
    }
    
    public CarouselIndicator(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.barSize = -1;
        this.barParentSize = -1;
        this.orientation = com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL;
        this.init(a, a0);
    }
    
    private void init(android.content.Context a, android.util.AttributeSet a0) {
        android.content.res.Resources a1 = a.getResources();
        this.circleSize = (int)a1.getDimension(R.dimen.carousel_circle_indicator_size);
        this.circleFocusSize = (int)a1.getDimension(R.dimen.carousel_circle_indicator_focus_size);
        this.circleMargin = (int)a1.getDimension(R.dimen.carousel_circle_indicator_margin);
        this.roundRadius = (int)a1.getDimension(R.dimen.carousel_progress_round_radius);
        this.itemRadius = (int)a1.getDimension(R.dimen.carousel_progress_item_radius);
        this.itemPadding = (int)a1.getDimension(R.dimen.carousel_progress_item_padding);
        this.currentItemPaddingRadius = (int)a1.getDimension(R.dimen.carousel_progress_currentitem_padding_radius);
        if (a0 != null) {
            android.content.res.TypedArray a2 = a.obtainStyledAttributes(a0, com.navdy.hud.app.R$styleable.CarouselIndicator);
            this.circleSize = (int)a2.getDimension(0, (float)this.circleSize);
            this.circleFocusSize = (int)a2.getDimension(1, (float)this.circleFocusSize);
            this.circleMargin = (int)a2.getDimension(2, (float)this.circleMargin);
            this.roundRadius = (int)a2.getDimension(3, (float)this.roundRadius);
            this.itemRadius = (int)a2.getDimension(4, (float)this.itemRadius);
            this.itemPadding = (int)a2.getDimension(5, (float)this.itemPadding);
            this.currentItemPaddingRadius = (int)a2.getDimension(6, (float)this.currentItemPaddingRadius);
            this.fullBackground = a2.getBoolean(7, false);
            this.viewPadding = (int)a2.getDimension(8, 0.0f);
            this.barSize = (int)a2.getDimension(9, -1f);
            this.barParentSize = (int)a2.getDimension(10, -1f);
            a2.recycle();
        }
    }
    
    public int getCurrentItem() {
        return (this.progressIndicator == null) ? -1 : this.progressIndicator.getCurrentItem();
    }
    
    public int getItemCount() {
        return this.itemCount;
    }
    
    public android.animation.AnimatorSet getItemMoveAnimator(int i, int i0) {
        android.animation.AnimatorSet a = (this.progressIndicator == null) ? null : this.progressIndicator.getItemMoveAnimator(i, i0);
        return a;
    }
    
    public android.graphics.RectF getItemPos(int i) {
        android.graphics.RectF a = (this.progressIndicator == null) ? null : this.progressIndicator.getItemPos(i);
        return a;
    }
    
    public void setCurrentItem(int i) {
        if (this.progressIndicator != null) {
            this.progressIndicator.setCurrentItem(i);
        }
    }
    
    public void setCurrentItem(int i, int i0) {
        if (this.progressIndicator != null) {
            this.progressIndicator.setCurrentItem(i, i0);
        }
    }
    
    public void setItemCount(int i) {
        if (i >= 0) {
            this.itemCount = i;
            int i0 = com.navdy.hud.app.HudApplication.getAppContext().getResources().getColor(17170443);
            if (i <= ((this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) ? 8 : 12)) {
                com.navdy.hud.app.ui.component.carousel.IProgressIndicator a = this.progressIndicator;
                label2: {
                    label3: {
                        if (a == null) {
                            break label3;
                        }
                        if (this.progressIndicator instanceof com.navdy.hud.app.ui.component.carousel.CircleIndicator) {
                            break label2;
                        }
                    }
                    this.removeAllViews();
                    this.progressIndicator = (com.navdy.hud.app.ui.component.carousel.IProgressIndicator)new com.navdy.hud.app.ui.component.carousel.CircleIndicator(this.getContext());
                    ((com.navdy.hud.app.ui.component.carousel.CircleIndicator)this.progressIndicator).setProperties(this.circleSize, this.circleFocusSize, this.circleMargin, i0);
                    this.progressIndicator.setOrientation(this.orientation);
                    android.widget.FrameLayout$LayoutParams a0 = new android.widget.FrameLayout$LayoutParams(-2, -2);
                    a0.gravity = 17;
                    this.addView((android.view.View)this.progressIndicator, (android.view.ViewGroup$LayoutParams)a0);
                }
                this.progressIndicator.setItemCount(i);
            } else {
                com.navdy.hud.app.ui.component.carousel.IProgressIndicator a1 = this.progressIndicator;
                label0: {
                    android.widget.FrameLayout$LayoutParams a2 = null;
                    label1: {
                        if (a1 == null) {
                            break label1;
                        }
                        if (this.progressIndicator instanceof com.navdy.hud.app.ui.component.carousel.ProgressIndicator) {
                            break label0;
                        }
                    }
                    this.removeAllViews();
                    this.progressIndicator = (com.navdy.hud.app.ui.component.carousel.IProgressIndicator)new com.navdy.hud.app.ui.component.carousel.ProgressIndicator(this.getContext());
                    ((com.navdy.hud.app.ui.component.carousel.ProgressIndicator)this.progressIndicator).setProperties(this.roundRadius, this.itemRadius, this.itemPadding, this.currentItemPaddingRadius, i0, -1, this.fullBackground, this.viewPadding, this.barSize, this.barParentSize);
                    this.progressIndicator.setOrientation(this.orientation);
                    android.content.res.Resources a3 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
                    if (this.barSize != -1) {
                        a2 = new android.widget.FrameLayout$LayoutParams(-1, -1);
                    } else {
                        int i1 = 0;
                        int i2 = 0;
                        if (this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) {
                            i1 = (int)a3.getDimension(R.dimen.carousel_progress_indicator_vertical_w);
                            i2 = (int)a3.getDimension(R.dimen.carousel_progress_indicator_vertical_h);
                        } else {
                            i2 = (int)a3.getDimension(R.dimen.carousel_progress_indicator_h);
                            i1 = (int)a3.getDimension(R.dimen.carousel_progress_indicator_w);
                        }
                        a2 = new android.widget.FrameLayout$LayoutParams(i1, i2);
                    }
                    a2.gravity = 17;
                    this.addView((android.view.View)this.progressIndicator, (android.view.ViewGroup$LayoutParams)a2);
                }
                this.progressIndicator.setItemCount(i);
            }
        }
    }
    
    public void setOrientation(com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation a) {
        this.orientation = a;
    }
}
