package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class TimeView$$ViewInjector {
    public TimeView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.ui.component.homescreen.TimeView a0, Object a1) {
        a0.dayTextView = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_day, "field 'dayTextView'");
        a0.timeTextView = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_time, "field 'timeTextView'");
        a0.ampmTextView = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_ampm, "field 'ampmTextView'");
    }
    
    public static void reset(com.navdy.hud.app.ui.component.homescreen.TimeView a) {
        a.dayTextView = null;
        a.timeTextView = null;
        a.ampmTextView = null;
    }
}
