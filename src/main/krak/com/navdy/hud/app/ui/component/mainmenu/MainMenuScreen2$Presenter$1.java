package com.navdy.hud.app.ui.component.mainmenu;

class MainMenuScreen2$Presenter$1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter this$0;
    final Runnable val$endAction;
    
    MainMenuScreen2$Presenter$1(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a, Runnable a0) {
        super();
        this.this$0 = a;
        this.val$endAction = a0;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v("post back");
        this.this$0.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
        if (this.val$endAction != null) {
            this.val$endAction.run();
        }
    }
}
