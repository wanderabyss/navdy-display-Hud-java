package com.navdy.hud.app.ui.component.bluetooth;

public class BluetoothPairing$BondStateChange {
    final public android.bluetooth.BluetoothDevice device;
    final public int newState;
    final public int oldState;
    
    public BluetoothPairing$BondStateChange(android.bluetooth.BluetoothDevice a, int i, int i0) {
        this.device = a;
        this.oldState = i;
        this.newState = i0;
    }
}
