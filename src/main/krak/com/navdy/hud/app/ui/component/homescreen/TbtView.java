package com.navdy.hud.app.ui.component.homescreen;

public class TbtView extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.homescreen.IHomeScreenLifecycle {
    final static long MANEUVER_PROGRESS_ANIMATION_DURATION = 250L;
    final private static long MANEUVER_SWAP_DURATION = 500L;
    final private static android.animation.TimeInterpolator maneuverSwapInterpolator;
    @InjectView(R.id.activeMapDistance)
    android.widget.TextView activeMapDistance;
    @InjectView(R.id.activeMapIcon)
    android.widget.ImageView activeMapIcon;
    @InjectView(R.id.activeMapInstruction)
    android.widget.TextView activeMapInstruction;
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private long initialProgressBarDistance;
    private String lastDistance;
    private com.navdy.hud.app.maps.MapEvents$ManeuverDisplay lastEvent;
    private com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState lastManeuverState;
    private com.navdy.hud.app.view.MainView$CustomAnimationMode lastMode;
    private String lastRoadText;
    private int lastTurnIconId;
    private String lastTurnText;
    private com.navdy.service.library.log.Logger logger;
    @InjectView(R.id.nextMapIcon)
    android.widget.ImageView nextMapIcon;
    @InjectView(R.id.nextMapIconContainer)
    android.view.View nextMapIconContainer;
    @InjectView(R.id.nextMapInstruction)
    android.widget.TextView nextMapInstruction;
    @InjectView(R.id.nextMapInstructionContainer)
    android.view.View nextMapInstructionContainer;
    @InjectView(R.id.now_icon)
    android.view.View nowIcon;
    private boolean paused;
    @InjectView(R.id.progress_bar)
    android.widget.ProgressBar progressBar;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    
    static {
        maneuverSwapInterpolator = (android.animation.TimeInterpolator)new android.view.animation.AccelerateDecelerateInterpolator();
    }
    
    public TbtView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public TbtView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public TbtView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.lastTurnIconId = -1;
    }
    
    static com.navdy.hud.app.ui.component.homescreen.HomeScreenView access$000(com.navdy.hud.app.ui.component.homescreen.TbtView a) {
        return a.homeScreenView;
    }
    
    static com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState access$100(com.navdy.hud.app.ui.component.homescreen.TbtView a) {
        return a.lastManeuverState;
    }
    
    private void animateSetDistance(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState a) {
        boolean b = false;
        boolean b0 = false;
        com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState a0 = this.lastManeuverState;
        com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState a1 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.NOW;
        label5: {
            label3: {
                label4: {
                    if (a0 == a1) {
                        break label4;
                    }
                    if (a == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.NOW) {
                        break label3;
                    }
                }
                b = false;
                break label5;
            }
            b = true;
        }
        com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState a2 = this.lastManeuverState;
        com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState a3 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.NOW;
        label2: {
            label0: {
                label1: {
                    if (a2 != a3) {
                        break label1;
                    }
                    if (a != com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.NOW) {
                        break label0;
                    }
                }
                b0 = false;
                break label2;
            }
            b0 = true;
        }
        if (b) {
            android.animation.AnimatorSet a4 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getFadeInAndScaleUpAnimator(this.nowIcon);
            android.animation.ObjectAnimator a5 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getAlphaAnimator((android.view.View)this.activeMapDistance, 0);
            android.animation.AnimatorSet a6 = new android.animation.AnimatorSet();
            a6.play((android.animation.Animator)a4).with((android.animation.Animator)a5);
            a6.setDuration(250L).start();
        } else if (b0) {
            android.animation.AnimatorSet a7 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getFadeOutAndScaleDownAnimator(this.nowIcon);
            android.animation.ObjectAnimator a8 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getAlphaAnimator((android.view.View)this.activeMapDistance, 1);
            android.animation.AnimatorSet a9 = new android.animation.AnimatorSet();
            a9.play((android.animation.Animator)a7).with((android.animation.Animator)a8);
            a9.setDuration(250L).start();
        }
    }
    
    private android.animation.AnimatorSet animateSetIcon(int i) {
        this.nextMapIconContainer.setVisibility(0);
        android.animation.ObjectAnimator a = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getTranslationYPositionAnimator((android.view.View)this.activeMapIcon, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionTranslationYAnimation);
        android.animation.ObjectAnimator a0 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getTranslationYPositionAnimator(this.nextMapIconContainer, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionTranslationYAnimation);
        a.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.homescreen.TbtView$3(this, i));
        android.animation.AnimatorSet a1 = new android.animation.AnimatorSet();
        a1.play((android.animation.Animator)a).with((android.animation.Animator)a0);
        return a1;
    }
    
    private android.animation.AnimatorSet animateSetInstruction(android.text.SpannableStringBuilder a) {
        this.nextMapInstructionContainer.setVisibility(0);
        this.nextMapInstruction.setText((CharSequence)a);
        android.animation.ObjectAnimator a0 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getTranslationYPositionAnimator((android.view.View)this.activeMapInstruction, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionTranslationYAnimation);
        android.animation.ObjectAnimator a1 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getTranslationYPositionAnimator(this.nextMapInstructionContainer, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionTranslationYAnimation);
        a0.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.homescreen.TbtView$4(this, a));
        android.animation.AnimatorSet a2 = new android.animation.AnimatorSet();
        a2.play((android.animation.Animator)a0).with((android.animation.Animator)a1);
        return a2;
    }
    
    private void cleanInstructions() {
        this.activeMapDistance.setText((CharSequence)"");
        com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getFadeOutAndScaleDownAnimator(this.nowIcon).setDuration(250L).start();
        com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.setWidth((android.view.View)this.progressBar, 0);
        this.activeMapIcon.setImageDrawable((android.graphics.drawable.Drawable)null);
        this.nextMapIcon.setImageDrawable((android.graphics.drawable.Drawable)null);
        this.activeMapInstruction.setText((CharSequence)"");
        this.nextMapInstruction.setText((CharSequence)"");
    }
    
    private void setDistance(String s, com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState a) {
        if (!android.text.TextUtils.equals((CharSequence)s, (CharSequence)this.lastDistance)) {
            this.lastDistance = s;
            this.activeMapDistance.setText((CharSequence)s);
            if (this.activeMapDistance.getAlpha() == 0.0f && this.nowIcon.getAlpha() == 0.0f && !this.uiStateManager.isMainUIShrunk()) {
                this.activeMapDistance.setAlpha(1f);
            }
            this.animateSetDistance(a);
        }
    }
    
    private android.animation.AnimatorSet setIcon(int i, boolean b) {
        int i0 = this.lastTurnIconId;
        android.animation.AnimatorSet a = null;
        if (i0 != i) {
            this.lastTurnIconId = i;
            if (i != -1) {
                this.nextMapIcon.setImageResource(i);
                if (b) {
                    this.activeMapIcon.setImageResource(i);
                    a = null;
                } else {
                    a = this.animateSetIcon(i);
                }
            } else {
                this.activeMapIcon.setImageDrawable((android.graphics.drawable.Drawable)null);
                a = null;
            }
        }
        return a;
    }
    
    private android.animation.AnimatorSet setInstruction(String s, String s0) {
        android.animation.AnimatorSet a = null;
        boolean b = android.text.TextUtils.equals((CharSequence)s, (CharSequence)this.lastTurnText);
        label1: {
            label0: {
                if (!b) {
                    break label0;
                }
                if (!android.text.TextUtils.equals((CharSequence)s0, (CharSequence)this.lastRoadText)) {
                    break label0;
                }
                a = null;
                break label1;
            }
            this.lastTurnText = s;
            this.lastRoadText = s0;
            android.text.SpannableStringBuilder a0 = new android.text.SpannableStringBuilder();
            if (this.lastTurnText != null && com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.canShowTurnText(this.lastTurnText)) {
                a0.append((CharSequence)this.lastTurnText);
                a0.append((CharSequence)" ");
            }
            if (this.lastRoadText != null) {
                a0.setSpan(new android.text.style.StyleSpan(1), 0, a0.length(), 33);
                a0.append((CharSequence)this.lastRoadText);
            }
            a = this.animateSetInstruction(a0);
        }
        return a;
    }
    
    private void setProgressBar(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState a, long j) {
        boolean b = false;
        boolean b0 = false;
        com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState a0 = this.lastManeuverState;
        com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState a1 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.SOON;
        label5: {
            label3: {
                label4: {
                    if (a0 == a1) {
                        break label4;
                    }
                    if (a == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.SOON) {
                        break label3;
                    }
                }
                b = false;
                break label5;
            }
            b = true;
        }
        com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState a2 = this.lastManeuverState;
        com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState a3 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.SOON;
        label2: {
            label0: {
                label1: {
                    if (a2 != a3) {
                        break label1;
                    }
                    if (a != com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.SOON) {
                        break label0;
                    }
                }
                b0 = false;
                break label2;
            }
            b0 = true;
        }
        if (b0) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.setWidth((android.view.View)this.progressBar, 0);
            this.initialProgressBarDistance = 0L;
        } else if (b) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.setWidth((android.view.View)this.progressBar, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadProgressBarWidth);
            this.initialProgressBarDistance = j;
        } else if (a == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.SOON) {
            double d = (double)j / (double)this.initialProgressBarDistance;
            com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getProgressBarAnimator(this.progressBar, (int)(100.0 * (1.0 - d))).setDuration(250L).start();
        }
    }
    
    public void clearState() {
        this.lastTurnIconId = -1;
        this.lastDistance = null;
        this.lastTurnText = null;
        this.lastRoadText = null;
        this.cleanInstructions();
    }
    
    public void getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode a, android.animation.AnimatorSet$Builder a0) {
        this.lastMode = a;
        switch(com.navdy.hud.app.ui.component.homescreen.TbtView$5.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                android.animation.AnimatorSet a1 = new android.animation.AnimatorSet();
                android.animation.AnimatorSet$Builder a2 = a1.play((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.activeMapIcon, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoIconShrinkLeftX));
                a2.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.nextMapIconContainer, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoIconShrinkLeftX));
                android.animation.ObjectAnimator a3 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.activeMapInstruction, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionShrinkLeftX);
                android.animation.ValueAnimator a4 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getWidthAnimator((android.view.View)this.activeMapInstruction, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionShrinkWidth);
                a2.with((android.animation.Animator)a3);
                a2.with((android.animation.Animator)a4);
                android.animation.ObjectAnimator a5 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.nextMapInstructionContainer, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionShrinkLeftX);
                android.animation.ValueAnimator a6 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getWidthAnimator(this.nextMapInstructionContainer, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionShrinkWidth);
                a2.with((android.animation.Animator)a5);
                a2.with((android.animation.Animator)a6);
                if (this.lastManeuverState != com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.NOW) {
                    a2.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getAlphaAnimator((android.view.View)this.activeMapDistance, 0));
                } else {
                    a2.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getFadeOutAndScaleDownAnimator(this.nowIcon));
                }
                a1.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.homescreen.TbtView$1(this));
                a0.with((android.animation.Animator)a1);
                break;
            }
            case 1: {
                android.animation.AnimatorSet a7 = new android.animation.AnimatorSet();
                android.animation.AnimatorSet$Builder a8 = a7.play((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.activeMapIcon, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoIconX));
                a8.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.nextMapIconContainer, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoIconX));
                android.animation.ObjectAnimator a9 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.activeMapInstruction, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionX);
                android.animation.ValueAnimator a10 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getWidthAnimator((android.view.View)this.activeMapInstruction, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionWidth);
                a8.with((android.animation.Animator)a9);
                a8.with((android.animation.Animator)a10);
                android.animation.ObjectAnimator a11 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator(this.nextMapInstructionContainer, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionX);
                android.animation.ValueAnimator a12 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getWidthAnimator(this.nextMapInstructionContainer, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionWidth);
                a8.with((android.animation.Animator)a11);
                a8.with((android.animation.Animator)a12);
                if (this.lastManeuverState != com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.NOW) {
                    a8.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getAlphaAnimator((android.view.View)this.activeMapDistance, 1));
                } else {
                    a8.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getFadeInAndScaleUpAnimator(this.nowIcon));
                }
                a7.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.homescreen.TbtView$2(this));
                a0.with((android.animation.Animator)a7);
                break;
            }
        }
    }
    
    public void init(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        this.homeScreenView = a;
        this.bus.register(this);
    }
    
    protected void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        com.navdy.hud.app.manager.RemoteDeviceManager a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        this.bus = a.getBus();
        this.uiStateManager = a.getUiStateManager();
        this.setLayoutTransition(new android.animation.LayoutTransition());
    }
    
    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.logger.v("::onPause:tbt");
        }
    }
    
    public void onResume() {
        if (this.paused) {
            this.paused = false;
            this.logger.v("::onResume:tbt");
            com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a = this.lastEvent;
            if (a != null) {
                this.lastEvent = null;
                this.logger.v("::onResume:tbt updated last event");
                this.updateDisplay(a);
            }
        }
    }
    
    public void setView(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        this.lastMode = a;
        android.view.ViewGroup$MarginLayoutParams a0 = (android.view.ViewGroup$MarginLayoutParams)this.activeMapInstruction.getLayoutParams();
        android.view.ViewGroup$MarginLayoutParams a1 = (android.view.ViewGroup$MarginLayoutParams)this.nextMapInstructionContainer.getLayoutParams();
        switch(com.navdy.hud.app.ui.component.homescreen.TbtView$5.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                this.nowIcon.setScaleX(0.9f);
                this.nowIcon.setScaleY(0.9f);
                this.nowIcon.setAlpha(0.0f);
                this.activeMapDistance.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoDistanceX);
                this.activeMapIcon.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoIconShrinkLeftX);
                this.nextMapIconContainer.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoIconShrinkLeftX);
                this.activeMapInstruction.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionShrinkLeftX);
                this.nextMapInstructionContainer.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionShrinkLeftX);
                android.view.ViewGroup$MarginLayoutParams a2 = (android.view.ViewGroup$MarginLayoutParams)this.activeMapInstruction.getLayoutParams();
                android.view.ViewGroup$MarginLayoutParams a3 = (android.view.ViewGroup$MarginLayoutParams)this.nextMapInstructionContainer.getLayoutParams();
                a2.width = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionShrinkWidth;
                a3.width = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionShrinkWidth;
                break;
            }
            case 1: {
                if (this.lastManeuverState == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.NOW) {
                    this.nowIcon.setScaleX(1f);
                    this.nowIcon.setScaleY(1f);
                    this.nowIcon.setAlpha(1f);
                }
                this.activeMapDistance.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoDistanceX);
                this.activeMapIcon.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoIconX);
                this.nextMapIconContainer.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoIconX);
                this.activeMapInstruction.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionX);
                this.nextMapInstructionContainer.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionX);
                a0.width = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionWidth;
                a1.width = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadInfoInstructionWidth;
                break;
            }
        }
    }
    
    public void showHideActiveDistance() {
        if (this.uiStateManager.isMainUIShrunk()) {
            this.activeMapDistance.setVisibility(4);
            this.progressBar.setVisibility(4);
        } else {
            this.activeMapDistance.setVisibility(0);
            this.progressBar.setVisibility(0);
        }
    }
    
    public void updateDisplay(com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a) {
        if (this.homeScreenView.isNavigationActive()) {
            if (a.empty) {
                this.clearState();
            } else if (a.pendingTurn != null) {
                this.lastEvent = a;
                if (!this.paused) {
                    boolean b = false;
                    android.animation.AnimatorSet a0 = null;
                    boolean b0 = android.text.TextUtils.equals((CharSequence)a.pendingTurn, (CharSequence)this.lastTurnText);
                    label4: {
                        label2: {
                            label3: {
                                if (!b0) {
                                    break label3;
                                }
                                if (android.text.TextUtils.equals((CharSequence)a.pendingRoad, (CharSequence)this.lastRoadText)) {
                                    break label2;
                                }
                            }
                            b = false;
                            break label4;
                        }
                        b = true;
                    }
                    this.setDistance(a.distanceToPendingRoadText, a.maneuverState);
                    this.setProgressBar(a.maneuverState, a.distanceInMeters);
                    android.animation.AnimatorSet a1 = this.setIcon(a.turnIconId, b);
                    android.animation.AnimatorSet a2 = this.setInstruction(a.pendingTurn, a.pendingRoad);
                    label1: {
                        label0: {
                            if (a1 == null) {
                                break label0;
                            }
                            if (a2 == null) {
                                break label0;
                            }
                            a0 = new android.animation.AnimatorSet();
                            a0.play((android.animation.Animator)a1).with((android.animation.Animator)a2);
                            break label1;
                        }
                        if (a1 == null) {
                            a0 = null;
                            if (a2 != null) {
                                a0 = a2;
                            }
                        } else {
                            a0 = a1;
                        }
                    }
                    if (a0 != null) {
                        a0.setDuration(500L).setInterpolator(maneuverSwapInterpolator);
                        a0.start();
                    }
                    this.lastManeuverState = a.maneuverState;
                }
            } else {
                this.logger.v("null pending turn");
                this.lastEvent = null;
            }
        }
    }
}
