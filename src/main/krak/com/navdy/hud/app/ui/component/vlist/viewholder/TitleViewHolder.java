package com.navdy.hud.app.ui.component.vlist.viewholder;
import com.navdy.hud.app.R;

public class TitleViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder {
    final private static com.navdy.service.library.log.Logger sLogger;
    private android.widget.TextView title;
    
    static {
        sLogger = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.sLogger;
    }
    
    public TitleViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        super(a, a0, a1);
        this.title = (android.widget.TextView)a.findViewById(R.id.title);
    }
    
    public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model buildModel(String s) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = new com.navdy.hud.app.ui.component.vlist.VerticalList$Model();
        a.type = com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.TITLE;
        a.id = R.id.vlist_title;
        a.title = s;
        return a;
    }
    
    public static com.navdy.hud.app.ui.component.vlist.viewholder.TitleViewHolder buildViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        return new com.navdy.hud.app.ui.component.vlist.viewholder.TitleViewHolder((android.view.ViewGroup)android.view.LayoutInflater.from(a.getContext()).inflate(R.layout.vlist_title, a, false), a0, a1);
    }
    
    public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a0) {
        this.title.setText((CharSequence)a.title);
    }
    
    public void clearAnimation() {
    }
    
    public void copyAndPosition(android.widget.ImageView a, android.widget.TextView a0, android.widget.TextView a1, android.widget.TextView a2, boolean b) {
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType getModelType() {
        return com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.TITLE;
    }
    
    public void preBind(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a0) {
        this.title.setTextSize(a.fontInfo.titleFontSize);
    }
    
    public void select(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, int i, int i0) {
    }
    
    public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State a, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType a0, int i, boolean b) {
    }
}
