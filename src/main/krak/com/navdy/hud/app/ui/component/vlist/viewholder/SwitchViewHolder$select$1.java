package com.navdy.hud.app.ui.component.vlist.viewholder;

final public class SwitchViewHolder$select$1 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final android.graphics.drawable.ClipDrawable $clipDrawable;
    final com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder this$0;
    
    SwitchViewHolder$select$1(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder a, android.graphics.drawable.ClipDrawable a0) {
        super();
        this.this$0 = a;
        this.$clipDrawable = a0;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        Object a0 = (a == null) ? null : a.getAnimatedValue();
        if (a0 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
        int i = (int)((Float)a0).floatValue();
        this.$clipDrawable.setLevel(i);
        this.this$0.getSwitchBackground().invalidate();
    }
}
