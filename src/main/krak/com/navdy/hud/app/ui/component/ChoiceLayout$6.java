package com.navdy.hud.app.ui.component;

class ChoiceLayout$6 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.ChoiceLayout this$0;
    
    ChoiceLayout$6(com.navdy.hud.app.ui.component.ChoiceLayout a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        if (com.navdy.hud.app.ui.component.ChoiceLayout.access$1300(this.this$0) != null) {
            if (com.navdy.hud.app.ui.component.ChoiceLayout.access$1400(this.this$0) != com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL) {
                ((android.widget.ImageView)com.navdy.hud.app.ui.component.ChoiceLayout.access$1300(this.this$0)).setColorFilter(com.navdy.hud.app.ui.component.ChoiceLayout.access$1500());
            } else {
                ((android.widget.TextView)com.navdy.hud.app.ui.component.ChoiceLayout.access$1300(this.this$0)).setTextColor(com.navdy.hud.app.ui.component.ChoiceLayout.access$1500());
            }
        }
        android.view.View a0 = com.navdy.hud.app.ui.component.ChoiceLayout.access$1100(this.this$0);
        Integer a1 = null;
        if (a0 != null) {
            Object a2 = com.navdy.hud.app.ui.component.ChoiceLayout.access$1100(this.this$0).getTag();
            boolean b = a2 instanceof Integer;
            a1 = null;
            if (b) {
                a1 = (Integer)a2;
            }
            if (com.navdy.hud.app.ui.component.ChoiceLayout.access$1400(this.this$0) != com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL) {
                ((android.widget.ImageView)com.navdy.hud.app.ui.component.ChoiceLayout.access$1100(this.this$0)).setColorFilter(com.navdy.hud.app.ui.component.ChoiceLayout.access$1000());
            } else {
                ((android.widget.TextView)com.navdy.hud.app.ui.component.ChoiceLayout.access$1100(this.this$0)).setTextColor(com.navdy.hud.app.ui.component.ChoiceLayout.access$1000());
            }
        }
        com.navdy.hud.app.ui.component.ChoiceLayout.access$602(this.this$0, (android.animation.AnimatorSet)null);
        if (a1 != null) {
            com.navdy.hud.app.ui.component.ChoiceLayout.access$1600(this.this$0, a1.intValue());
        }
        com.navdy.hud.app.ui.component.ChoiceLayout.access$000(this.this$0);
    }
}
