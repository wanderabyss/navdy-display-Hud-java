package com.navdy.hud.app.ui.component.carousel;

public class Carousel$ViewCacheManager {
    private int maxCacheCount;
    private java.util.ArrayList middleLeftViewCache;
    private java.util.ArrayList middleRightViewCache;
    private java.util.ArrayList sideViewCache;
    
    public Carousel$ViewCacheManager(int i) {
        this.sideViewCache = new java.util.ArrayList();
        this.middleLeftViewCache = new java.util.ArrayList();
        this.middleRightViewCache = new java.util.ArrayList();
        this.maxCacheCount = i;
    }
    
    public void clearCache() {
        this.sideViewCache.clear();
        this.middleLeftViewCache.clear();
        this.middleRightViewCache.clear();
    }
    
    public android.view.View getView(com.navdy.hud.app.ui.component.carousel.Carousel$ViewType a) {
        android.view.View a0 = null;
        switch(com.navdy.hud.app.ui.component.carousel.Carousel$1.$SwitchMap$com$navdy$hud$app$ui$component$carousel$Carousel$ViewType[a.ordinal()]) {
            case 3: {
                int i = this.middleRightViewCache.size();
                a0 = null;
                if (i <= 0) {
                    break;
                }
                a0 = (android.view.View)this.middleRightViewCache.remove(0);
                break;
            }
            case 2: {
                int i0 = this.middleLeftViewCache.size();
                a0 = null;
                if (i0 <= 0) {
                    break;
                }
                a0 = (android.view.View)this.middleLeftViewCache.remove(0);
                break;
            }
            case 1: {
                int i1 = this.sideViewCache.size();
                a0 = null;
                if (i1 <= 0) {
                    break;
                }
                a0 = (android.view.View)this.sideViewCache.remove(0);
                break;
            }
            default: {
                a0 = null;
            }
        }
        if (a0 != null && a0.getParent() != null) {
            android.util.Log.e("CAROUSEL", "uhoh");
        }
        return a0;
    }
    
    public void putView(com.navdy.hud.app.ui.component.carousel.Carousel$ViewType a, android.view.View a0) {
        label0: {
            label1: {
                if (a0 == null) {
                    break label1;
                }
                if (a0.getParent() == null) {
                    break label0;
                }
            }
            android.util.Log.e("CAROUSEL", "uhoh");
        }
        switch(com.navdy.hud.app.ui.component.carousel.Carousel$1.$SwitchMap$com$navdy$hud$app$ui$component$carousel$Carousel$ViewType[a.ordinal()]) {
            case 3: {
                if (this.middleRightViewCache.size() >= this.maxCacheCount) {
                    break;
                }
                this.middleRightViewCache.add(a0);
                break;
            }
            case 2: {
                if (this.middleLeftViewCache.size() >= this.maxCacheCount) {
                    break;
                }
                this.middleLeftViewCache.add(a0);
                break;
            }
            case 1: {
                if (this.sideViewCache.size() >= this.maxCacheCount) {
                    break;
                }
                this.sideViewCache.add(a0);
                break;
            }
        }
    }
}
