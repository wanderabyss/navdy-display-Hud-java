package com.navdy.hud.app.ui.component.carousel;

abstract public interface Carousel$ViewProcessor {
    abstract public void processInfoView(com.navdy.hud.app.ui.component.carousel.Carousel$Model arg, android.view.View arg0, int arg1);
    
    
    abstract public void processLargeImageView(com.navdy.hud.app.ui.component.carousel.Carousel$Model arg, android.view.View arg0, int arg1, int arg2, int arg3);
    
    
    abstract public void processSmallImageView(com.navdy.hud.app.ui.component.carousel.Carousel$Model arg, android.view.View arg0, int arg1, int arg2, int arg3);
}
