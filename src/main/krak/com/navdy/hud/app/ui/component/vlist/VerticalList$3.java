package com.navdy.hud.app.ui.component.vlist;

class VerticalList$3 extends android.support.v7.widget.RecyclerView$OnScrollListener {
    final com.navdy.hud.app.ui.component.vlist.VerticalList this$0;
    
    VerticalList$3(com.navdy.hud.app.ui.component.vlist.VerticalList a) {
        super();
        this.this$0 = a;
    }
    
    private void moveItemToSelectedState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a, int i) {
        label0: if (a != null) {
            int i0 = com.navdy.hud.app.ui.component.vlist.VerticalList.access$1400(this.this$0);
            label1: {
                label2: {
                    if (i != i0) {
                        break label2;
                    }
                    if (a.getState() != com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.UNSELECTED) {
                        break label1;
                    }
                }
                com.navdy.hud.app.ui.component.vlist.VerticalList.access$1402(this.this$0, i);
                if (com.navdy.hud.app.ui.component.vlist.VerticalList.access$1800(this.this$0)) {
                    com.navdy.hud.app.ui.component.vlist.VerticalList.access$500().v(new StringBuilder().append("onScrollStateChanged idle-select-un pos:").append(i).toString());
                    a.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.UNSELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.NONE, 230);
                    break label0;
                } else {
                    com.navdy.hud.app.ui.component.vlist.VerticalList.access$500().v(new StringBuilder().append("onScrollStateChanged idle-select pos:").append(i).toString());
                    a.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.NONE, 230);
                    break label0;
                }
            }
            if (i == com.navdy.hud.app.ui.component.vlist.VerticalList.access$1400(this.this$0) && com.navdy.hud.app.ui.component.vlist.VerticalList.access$1800(this.this$0) && a.getState() == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED) {
                com.navdy.hud.app.ui.component.vlist.VerticalList.access$500().v(new StringBuilder().append("onScrollStateChanged idle-select-un-2 pos:").append(i).toString());
                a.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.UNSELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.NONE, 230);
            }
        }
    }
    
    private void moveItemToUnSelectedState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a, int i) {
        if (a != null && a.getModelType() != com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.BLANK && a.getState() == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED) {
            com.navdy.hud.app.ui.component.vlist.VerticalList.access$500().v(new StringBuilder().append("onScrollStateChanged idle-unselect pos:").append(i).toString());
            a.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.UNSELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.NONE, 230);
        }
    }
    
    public void onScrollStateChanged(android.support.v7.widget.RecyclerView a, int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList.access$202(this.this$0, true);
        com.navdy.hud.app.ui.component.vlist.VerticalList.access$702(this.this$0, i);
        ((android.support.v7.widget.RecyclerView$OnScrollListener)this).onScrollStateChanged(a, i);
        if (i == 0) {
            if (this.this$0.reCalcScrollPos) {
                com.navdy.hud.app.ui.component.vlist.VerticalList.access$800(this.this$0);
            }
            com.navdy.hud.app.ui.component.vlist.VerticalList.access$902(this.this$0, false);
            com.navdy.hud.app.ui.component.vlist.VerticalList.access$1002(this.this$0, false);
            com.navdy.hud.app.ui.component.vlist.VerticalList.access$1102(this.this$0, false);
            com.navdy.hud.app.ui.component.vlist.VerticalList.access$1202(this.this$0, 0);
            int i0 = com.navdy.hud.app.ui.component.vlist.VerticalList.access$1300(this.this$0, com.navdy.hud.app.ui.component.vlist.VerticalList.access$400(this.this$0));
            com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a0 = (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)a.findViewHolderForAdapterPosition(i0);
            com.navdy.hud.app.ui.component.vlist.VerticalList.access$500().v(new StringBuilder().append("onScrollStateChanged idle pos:").append(i0).append(" current=").append(com.navdy.hud.app.ui.component.vlist.VerticalList.access$1400(this.this$0)).append(" scrollY=").append(com.navdy.hud.app.ui.component.vlist.VerticalList.access$400(this.this$0)).append(" vh=").append(a0 != null).toString());
            this.moveItemToSelectedState(a0, i0);
            int i1 = com.navdy.hud.app.ui.component.vlist.VerticalList.access$1400(this.this$0) - 1;
            if (i1 >= 0) {
                this.moveItemToUnSelectedState((com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)a.findViewHolderForAdapterPosition(i1), i1);
            }
            int i2 = com.navdy.hud.app.ui.component.vlist.VerticalList.access$1400(this.this$0) + 1;
            this.moveItemToUnSelectedState((com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)a.findViewHolderForAdapterPosition(i2), i2);
            com.navdy.hud.app.ui.component.vlist.VerticalList.access$600(this.this$0);
            if (com.navdy.hud.app.ui.component.vlist.VerticalList.access$1500(this.this$0)) {
                com.navdy.hud.app.ui.component.vlist.VerticalList.access$1502(this.this$0, false);
                com.navdy.hud.app.ui.component.vlist.VerticalList.access$1600(this.this$0, true);
            } else if (com.navdy.hud.app.ui.component.vlist.VerticalList.access$1700(this.this$0) != -1) {
                int i3 = com.navdy.hud.app.ui.component.vlist.VerticalList.access$1700(this.this$0);
                com.navdy.hud.app.ui.component.vlist.VerticalList.access$1702(this.this$0, -1);
                this.this$0.scrollToPosition(i3);
                com.navdy.hud.app.ui.component.vlist.VerticalList.access$500().v(new StringBuilder().append("onScrolled idle scrollToPos:").append(i3).toString());
            }
            if (this.this$0.sendScrollIdleEvent) {
                this.this$0.callback.onScrollIdle();
            }
        }
    }
    
    public void onScrolled(android.support.v7.widget.RecyclerView a, int i, int i0) {
        ((android.support.v7.widget.RecyclerView$OnScrollListener)this).onScrolled(a, i, i0);
        com.navdy.hud.app.ui.component.vlist.VerticalList.access$402(this.this$0, com.navdy.hud.app.ui.component.vlist.VerticalList.access$400(this.this$0) + i0);
        if (this.this$0.waitForTarget) {
            this.this$0.targetFound(0);
        }
    }
}
