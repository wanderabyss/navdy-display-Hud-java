package com.navdy.hud.app.ui.component.homescreen;

class NavigationView$12 {
    final static int[] $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
    final static int[] $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State;
    final static int[] $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode;
    
    static {
        $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
        com.navdy.hud.app.manager.InputManager$CustomKeyEvent a0 = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.RIGHT;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LEFT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode = new int[com.navdy.hud.app.view.MainView$CustomAnimationMode.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode;
        com.navdy.hud.app.view.MainView$CustomAnimationMode a2 = com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_MODE.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException4) {
        }
        $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State = new int[com.navdy.hud.app.maps.here.HereMapController$State.values().length];
        int[] a3 = $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State;
        com.navdy.hud.app.maps.here.HereMapController$State a4 = com.navdy.hud.app.maps.here.HereMapController$State.OVERVIEW;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State[com.navdy.hud.app.maps.here.HereMapController$State.ROUTE_PICKER.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State[com.navdy.hud.app.maps.here.HereMapController$State.AR_MODE.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException7) {
        }
    }
}
