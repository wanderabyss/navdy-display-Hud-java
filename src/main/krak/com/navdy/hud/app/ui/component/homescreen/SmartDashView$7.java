package com.navdy.hud.app.ui.component.homescreen;

class SmartDashView$7 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final com.navdy.hud.app.ui.component.homescreen.SmartDashView this$0;
    final android.view.ViewGroup$MarginLayoutParams val$marginParams;
    
    SmartDashView$7(com.navdy.hud.app.ui.component.homescreen.SmartDashView a, android.view.ViewGroup$MarginLayoutParams a0) {
        super();
        this.this$0 = a;
        this.val$marginParams = a0;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        this.val$marginParams.leftMargin = ((Integer)a.getAnimatedValue()).intValue();
        this.this$0.mMiddleGaugeView.setLayoutParams((android.view.ViewGroup$LayoutParams)this.val$marginParams);
    }
}
