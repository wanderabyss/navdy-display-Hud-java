package com.navdy.hud.app.ui.component;
import com.navdy.hud.app.R;

public class SystemTrayView$$ViewInjector {
    public SystemTrayView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.ui.component.SystemTrayView a0, Object a1) {
        a0.dialImageView = (android.widget.ImageView)a.findRequiredView(a1, R.id.dial, "field 'dialImageView'");
        a0.phoneImageView = (android.widget.ImageView)a.findRequiredView(a1, R.id.phone, "field 'phoneImageView'");
        a0.phoneNetworkImageView = (android.widget.ImageView)a.findRequiredView(a1, R.id.phoneNetwork, "field 'phoneNetworkImageView'");
        a0.locationImageView = (android.widget.ImageView)a.findRequiredView(a1, R.id.gps, "field 'locationImageView'");
    }
    
    public static void reset(com.navdy.hud.app.ui.component.SystemTrayView a) {
        a.dialImageView = null;
        a.phoneImageView = null;
        a.phoneNetworkImageView = null;
        a.locationImageView = null;
    }
}
