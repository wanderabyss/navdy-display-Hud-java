package com.navdy.hud.app.ui.component.carousel;

public class Carousel$InitParams {
    public com.navdy.hud.app.ui.component.carousel.AnimationStrategy animator;
    public com.navdy.hud.app.ui.component.carousel.CarouselIndicator carouselIndicator;
    public boolean exitOnDoubleClick;
    public boolean fastScrollAnimation;
    public int imageLytResourceId;
    public int infoLayoutResourceId;
    public java.util.List model;
    public android.view.View rootContainer;
    public com.navdy.hud.app.ui.component.carousel.Carousel$ViewProcessor viewProcessor;
    
    public Carousel$InitParams() {
    }
}
