package com.navdy.hud.app.ui.component.tbt;
import com.navdy.hud.app.R;

final public class TbtTextInstructionView extends android.widget.TextView {
    final public static com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion Companion;
    final private static int fullWidth;
    final private static com.navdy.service.library.log.Logger logger;
    final private static int mediumWidth;
    final private static android.content.res.Resources resources;
    final private static int singleLineMinWidth;
    final private static float size18;
    final private static float size20;
    final private static float size22;
    final private static float size24;
    final private static float size26;
    final private static int smallWidth;
    private java.util.HashMap _$_findViewCache;
    private com.navdy.hud.app.view.MainView$CustomAnimationMode currentMode;
    private boolean instructionWidthIncludesNext;
    private String lastInstruction;
    private int lastInstructionWidth;
    private com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState lastManeuverState;
    private String lastRoadText;
    private String lastTurnText;
    final private android.widget.TextView sizeCalculationTextView;
    final private StringBuilder stringBuilder;
    final private com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$WidthInfo widthInfo;
    
    static {
        Companion = new com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion((kotlin.jvm.internal.DefaultConstructorMarker)null);
        logger = new com.navdy.service.library.log.Logger("TbtTextInstructionView");
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a, "HudApplication.getAppContext().resources");
        resources = a;
        fullWidth = com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getResources$p(Companion).getDimensionPixelSize(R.dimen.tbt_instruction_full_w);
        mediumWidth = com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getResources$p(Companion).getDimensionPixelSize(R.dimen.tbt_instruction_medium_w);
        smallWidth = com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getResources$p(Companion).getDimensionPixelSize(R.dimen.tbt_instruction_small_w);
        singleLineMinWidth = com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getResources$p(Companion).getDimensionPixelSize(R.dimen.tbt_instruction_min_w);
        size26 = com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getResources$p(Companion).getDimension(R.dimen.tbt_instruction_26);
        size24 = com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getResources$p(Companion).getDimension(R.dimen.tbt_instruction_24);
        size22 = com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getResources$p(Companion).getDimension(R.dimen.tbt_instruction_22);
        size20 = com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getResources$p(Companion).getDimension(R.dimen.tbt_instruction_20);
        size18 = com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getResources$p(Companion).getDimension(R.dimen.tbt_instruction_18);
    }
    
    public TbtTextInstructionView(android.content.Context a) {
        super(a);
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        this.lastInstructionWidth = -1;
        this.stringBuilder = new StringBuilder();
        this.widthInfo = new com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$WidthInfo();
        this.sizeCalculationTextView = new android.widget.TextView(com.navdy.hud.app.HudApplication.getAppContext());
    }
    
    public TbtTextInstructionView(android.content.Context a, android.util.AttributeSet a0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "attrs");
        super(a, a0);
        this.lastInstructionWidth = -1;
        this.stringBuilder = new StringBuilder();
        this.widthInfo = new com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$WidthInfo();
        this.sizeCalculationTextView = new android.widget.TextView(com.navdy.hud.app.HudApplication.getAppContext());
    }
    
    public TbtTextInstructionView(android.content.Context a, android.util.AttributeSet a0, int i) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "attrs");
        super(a, a0, i);
        this.lastInstructionWidth = -1;
        this.stringBuilder = new StringBuilder();
        this.widthInfo = new com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$WidthInfo();
        this.sizeCalculationTextView = new android.widget.TextView(com.navdy.hud.app.HudApplication.getAppContext());
    }
    
    final public static int access$getFullWidth$cp() {
        return fullWidth;
    }
    
    final public static com.navdy.service.library.log.Logger access$getLogger$cp() {
        return logger;
    }
    
    final public static int access$getMediumWidth$cp() {
        return mediumWidth;
    }
    
    final public static android.content.res.Resources access$getResources$cp() {
        return resources;
    }
    
    final public static int access$getSingleLineMinWidth$cp() {
        return singleLineMinWidth;
    }
    
    final public static float access$getSize18$cp() {
        return size18;
    }
    
    final public static float access$getSize20$cp() {
        return size20;
    }
    
    final public static float access$getSize22$cp() {
        return size22;
    }
    
    final public static float access$getSize24$cp() {
        return size24;
    }
    
    final public static float access$getSize26$cp() {
        return size26;
    }
    
    final public static int access$getSmallWidth$cp() {
        return smallWidth;
    }
    
    final private void calculateTextWidth(String s, float f, com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$WidthInfo a, int i) {
        a.clear();
        this.sizeCalculationTextView.setTextSize(f);
        android.text.StaticLayout a0 = new android.text.StaticLayout((CharSequence)s, this.sizeCalculationTextView.getPaint(), i, android.text.Layout$Alignment.ALIGN_NORMAL, 1f, 0.0f, false);
        a.setNumLines$app_hudRelease(a0.getLineCount());
        a.setLineWidth1$app_hudRelease((int)a0.getLineMax(0));
        if (a.getNumLines$app_hudRelease() > 1) {
            a.setLineWidth2$app_hudRelease((int)a0.getLineMax(1));
        }
    }
    
    final private android.text.SpannableStringBuilder getBoldText(String s) {
        android.text.SpannableStringBuilder a = new android.text.SpannableStringBuilder();
        a.append((CharSequence)s);
        a.setSpan(new android.text.style.StyleSpan(1), 0, a.length(), 33);
        return a;
    }
    
    final private void setInstruction(com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a, boolean b, boolean b0) {
        boolean b1 = android.text.TextUtils.equals((CharSequence)a.pendingTurn, (CharSequence)this.lastTurnText);
        label0: {
            label1: {
                if (!b1) {
                    break label1;
                }
                if (!android.text.TextUtils.equals((CharSequence)a.pendingRoad, (CharSequence)this.lastRoadText)) {
                    break label1;
                }
                if (!b) {
                    break label0;
                }
            }
            this.lastTurnText = a.pendingTurn;
            this.lastRoadText = a.pendingRoad;
            this.stringBuilder.setLength(0);
            if (this.lastTurnText != null && com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.canShowTurnText(this.lastTurnText)) {
                this.stringBuilder.append(this.lastTurnText);
                this.stringBuilder.append(" ");
            }
            if (this.lastRoadText != null) {
                this.stringBuilder.append(this.lastRoadText);
            }
            String s = this.stringBuilder.toString();
            this.lastInstruction = s;
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(s, "str");
            this.setWidth(s, b0);
        }
    }
    
    final private void setViewWidth(int i) {
        android.view.ViewGroup$LayoutParams a = this.getLayoutParams();
        if (a == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }
        ((android.view.ViewGroup$MarginLayoutParams)a).width = i;
        this.requestLayout();
    }
    
    final private void setWidth(String s, boolean b) {
        int i = 0;
        this.lastInstructionWidth = -1;
        com.navdy.hud.app.view.MainView$CustomAnimationMode a = this.currentMode;
        if (a != null) {
            switch(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$WhenMappings.$EnumSwitchMapping$1[a.ordinal()]) {
                case 2: {
                    i = com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getSmallWidth$p(Companion);
                    break;
                }
                case 1: {
                    i = b ? com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getMediumWidth$p(Companion) : com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getFullWidth$p(Companion);
                    break;
                }
                default: {
                    i = 0;
                }
            }
        } else {
            i = 0;
        }
        if (!this.tryTextSize(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getSize26$p(Companion), i, s, this.widthInfo, false, false) && !this.tryTextSize(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getSize24$p(Companion), i, s, this.widthInfo, true, false) && !this.tryTextSize(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getSize22$p(Companion), i, s, this.widthInfo, true, false) && !this.tryTextSize(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getSize20$p(Companion), i, s, this.widthInfo, true, true)) {
            this.setTextSize(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getSize18$p(Companion));
            this.setText((CharSequence)this.getBoldText(s));
            this.setViewWidth(i);
            this.lastInstructionWidth = i;
        }
    }
    
    final private boolean tryTextSize(float f, int i, String s, com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$WidthInfo a, boolean b, boolean b0) {
        boolean b1 = false;
        this.calculateTextWidth(s, f, a, i);
        if (a.getNumLines$app_hudRelease() != 1) {
            if (b) {
                if (a.getNumLines$app_hudRelease() != 2) {
                    b1 = false;
                } else {
                    this.setTextSize(f);
                    Object a0 = b0 ? this.getBoldText(s) : s;
                    this.setText((CharSequence)a0);
                    this.setViewWidth(i);
                    this.lastInstructionWidth = i;
                    b1 = true;
                }
            } else {
                b1 = false;
            }
        } else {
            this.setTextSize(f);
            Object a1 = b0 ? this.getBoldText(s) : s;
            this.setText((CharSequence)a1);
            int i0 = a.getLineWidth1$app_hudRelease();
            if (i0 < com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getSingleLineMinWidth$p(Companion)) {
                i0 = com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getSingleLineMinWidth$p(Companion);
            }
            this.setViewWidth(i0);
            this.lastInstructionWidth = i0;
            b1 = true;
        }
        return b1;
    }
    
    public static void updateDisplay$default(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView a, com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a0, boolean b, boolean b0, int i, Object a1) {
        if ((i & 2) != 0) {
            b = false;
        }
        if ((i & 4) != 0) {
            b0 = false;
        }
        a.updateDisplay(a0, b, b0);
    }
    
    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }
    
    public android.view.View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new java.util.HashMap();
        }
        android.view.View a = (android.view.View)this._$_findViewCache.get(Integer.valueOf(i));
        if (a == null) {
            a = this.findViewById(i);
            this._$_findViewCache.put(Integer.valueOf(i), a);
        }
        return a;
    }
    
    final public void clear() {
        this.setText((CharSequence)"");
        this.lastTurnText = null;
        this.lastRoadText = null;
        this.lastInstruction = null;
        this.lastInstructionWidth = -1;
        this.instructionWidthIncludesNext = false;
    }
    
    final public void getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode a, android.animation.AnimatorSet$Builder a0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "mode");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "mainBuilder");
    }
    
    final public void setMode(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "mode");
        if (!kotlin.jvm.internal.Intrinsics.areEqual(this.currentMode, a)) {
            if (this.lastInstructionWidth != -1) {
                boolean b = false;
                android.view.ViewGroup$LayoutParams a0 = this.getLayoutParams();
                if (a0 == null) {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
                }
                android.view.ViewGroup$MarginLayoutParams a1 = (android.view.ViewGroup$MarginLayoutParams)a0;
                switch(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$WhenMappings.$EnumSwitchMapping$0[a.ordinal()]) {
                    case 2: {
                        if (kotlin.jvm.internal.Intrinsics.areEqual(this.currentMode, com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND)) {
                            b = this.lastInstructionWidth > com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getMediumWidth$p(Companion);
                            break;
                        } else {
                            b = false;
                            break;
                        }
                    }
                    case 1: {
                        if (kotlin.jvm.internal.Intrinsics.areEqual(this.currentMode, com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT)) {
                            b = a1.width == com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getMediumWidth$p(Companion);
                            break;
                        } else {
                            b = false;
                            break;
                        }
                    }
                    default: {
                        b = false;
                    }
                }
                if (b) {
                    String s = this.lastInstruction;
                    if (s != null) {
                        this.setWidth(s, true);
                    }
                }
            } else if (kotlin.jvm.internal.Intrinsics.areEqual(a, com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND)) {
                this.setViewWidth(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getFullWidth$p(Companion));
            } else {
                this.setViewWidth(com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView$Companion.access$getMediumWidth$p(Companion));
            }
            this.currentMode = a;
        }
    }
    
    final public void updateDisplay(com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a, boolean b, boolean b0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "event");
        this.setInstruction(a, b, b0);
        this.lastManeuverState = a.maneuverState;
    }
}
