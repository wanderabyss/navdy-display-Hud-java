package com.navdy.hud.app.ui.component.homescreen;

final public class HomeScreenView$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding globalPreferences;
    private dagger.internal.Binding presenter;
    
    public HomeScreenView$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.ui.component.homescreen.HomeScreenView", false, com.navdy.hud.app.ui.component.homescreen.HomeScreenView.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.presenter = a.requestBinding("com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter", com.navdy.hud.app.ui.component.homescreen.HomeScreenView.class, (this).getClass().getClassLoader());
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.ui.component.homescreen.HomeScreenView.class, (this).getClass().getClassLoader());
        this.globalPreferences = a.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.ui.component.homescreen.HomeScreenView.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.presenter);
        a0.add(this.bus);
        a0.add(this.globalPreferences);
    }
    
    public void injectMembers(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        a.presenter = (com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter)this.presenter.get();
        a.bus = (com.squareup.otto.Bus)this.bus.get();
        a.globalPreferences = (android.content.SharedPreferences)this.globalPreferences.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.ui.component.homescreen.HomeScreenView)a);
    }
}
