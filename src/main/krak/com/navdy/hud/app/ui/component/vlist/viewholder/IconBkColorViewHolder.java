package com.navdy.hud.app.ui.component.vlist.viewholder;
import com.navdy.hud.app.R;

public class IconBkColorViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder {
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.sLogger;
    }
    
    private IconBkColorViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        super(a, a0, a1);
    }
    
    public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model buildModel(int i, int i0, int i1, int i2, int i3, String s, String s0) {
        return com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(i, i0, i1, i2, i3, s, s0, (String)null, com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape.CIRCLE);
    }
    
    public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model buildModel(int i, int i0, int i1, int i2, int i3, String s, String s0, String s1) {
        return com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(i, i0, i1, i2, i3, s, s0, s1, com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape.CIRCLE);
    }
    
    public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model buildModel(int i, int i0, int i1, int i2, int i3, String s, String s0, String s1, com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape a) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a0 = com.navdy.hud.app.ui.component.vlist.VerticalModelCache.getFromCache(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON_BKCOLOR);
        if (a0 == null) {
            a0 = new com.navdy.hud.app.ui.component.vlist.VerticalList$Model();
        }
        a0.type = com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON_BKCOLOR;
        a0.id = i;
        a0.icon = i0;
        a0.iconSelectedColor = i1;
        a0.iconDeselectedColor = i2;
        a0.iconFluctuatorColor = i3;
        a0.title = s;
        a0.subTitle = s0;
        a0.subTitle2 = s1;
        a0.iconShape = a;
        return a0;
    }
    
    public static com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder buildViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        return new com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.getLayout(a, R.layout.vlist_item, R.layout.crossfade_image_bkcolor_lyt), a0, a1);
    }
    
    private void setIcon(int i, int i0, int i1, boolean b, boolean b0) {
        com.navdy.hud.app.ui.component.image.IconColorImageView a = (com.navdy.hud.app.ui.component.image.IconColorImageView)this.crossFadeImageView.getBig();
        if (b) {
            a.setDraw(true);
            a.setIcon(i, i0, (android.graphics.Shader)null, 0.83f);
        } else {
            a.setDraw(false);
        }
        com.navdy.hud.app.ui.component.image.IconColorImageView a0 = (com.navdy.hud.app.ui.component.image.IconColorImageView)this.crossFadeImageView.getSmall();
        if (b0) {
            a0.setDraw(true);
            a0.setIcon(i, i1, (android.graphics.Shader)null, 0.83f);
        } else {
            a0.setDraw(false);
        }
    }
    
    public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a0) {
        super.bind(a, a0);
        this.setIcon(a.icon, a.iconSelectedColor, a.iconDeselectedColor, a0.updateImage, a0.updateSmallImage);
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType getModelType() {
        return com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON_BKCOLOR;
    }
    
    public void preBind(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a0) {
        super.preBind(a, a0);
        ((com.navdy.hud.app.ui.component.image.IconColorImageView)this.crossFadeImageView.getBig()).setIconShape(a.iconShape);
        ((com.navdy.hud.app.ui.component.image.IconColorImageView)this.crossFadeImageView.getSmall()).setIconShape(a.iconShape);
    }
    
    public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State a, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType a0, int i, boolean b) {
        com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode a1 = null;
        float f = 0.0f;
        super.setItemState(a, a0, i, b);
        switch(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder$1.$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$State[a.ordinal()]) {
            case 2: {
                a1 = com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.SMALL;
                f = 0.6f;
                break;
            }
            case 1: {
                a1 = com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.BIG;
                f = 1f;
                break;
            }
            default: {
                a1 = null;
                f = 0.0f;
            }
        }
        switch(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder$1.$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType[a0.ordinal()]) {
            case 3: {
                android.util.Property a2 = android.view.View.SCALE_X;
                float[] a3 = new float[1];
                a3[0] = f;
                android.animation.PropertyValuesHolder a4 = android.animation.PropertyValuesHolder.ofFloat(a2, a3);
                android.util.Property a5 = android.view.View.SCALE_Y;
                float[] a6 = new float[1];
                a6[0] = f;
                android.animation.PropertyValuesHolder a7 = android.animation.PropertyValuesHolder.ofFloat(a5, a6);
                android.animation.AnimatorSet$Builder a8 = this.animatorSetBuilder;
                android.view.ViewGroup a9 = this.imageContainer;
                android.animation.PropertyValuesHolder[] a10 = new android.animation.PropertyValuesHolder[2];
                a10[0] = a4;
                a10[1] = a7;
                a8.with((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a9, a10));
                if (this.crossFadeImageView.getMode() == a1) {
                    break;
                }
                this.animatorSetBuilder.with((android.animation.Animator)this.crossFadeImageView.getCrossFadeAnimator());
                break;
            }
            case 1: case 2: {
                this.imageContainer.setScaleX(f);
                this.imageContainer.setScaleY(f);
                this.crossFadeImageView.setMode(a1);
                break;
            }
        }
    }
}
