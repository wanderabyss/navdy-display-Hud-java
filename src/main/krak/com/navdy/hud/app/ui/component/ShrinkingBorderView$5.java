package com.navdy.hud.app.ui.component;

class ShrinkingBorderView$5 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final com.navdy.hud.app.ui.component.ShrinkingBorderView this$0;
    
    ShrinkingBorderView$5(com.navdy.hud.app.ui.component.ShrinkingBorderView a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        int i = ((Integer)a.getAnimatedValue()).intValue();
        ((android.view.ViewGroup$MarginLayoutParams)this.this$0.getLayoutParams()).height = i;
        this.this$0.requestLayout();
    }
}
