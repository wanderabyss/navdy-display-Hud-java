package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter$ProvideArtworkCacheProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    final private com.navdy.hud.app.common.ProdModule module;
    
    public ProdModule$$ModuleAdapter$ProvideArtworkCacheProvidesAdapter(com.navdy.hud.app.common.ProdModule a) {
        super("com.navdy.hud.app.util.MusicArtworkCache", true, "com.navdy.hud.app.common.ProdModule", "provideArtworkCache");
        this.module = a;
        this.setLibrary(true);
    }
    
    public com.navdy.hud.app.util.MusicArtworkCache get() {
        return this.module.provideArtworkCache();
    }
    
    public Object get() {
        return this.get();
    }
}
