package com.navdy.hud.app.common;

class ServiceReconnector$1 implements Runnable {
    final com.navdy.hud.app.common.ServiceReconnector this$0;
    
    ServiceReconnector$1(com.navdy.hud.app.common.ServiceReconnector a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        try {
            android.content.ComponentName a = this.this$0.serviceIntent.getComponent();
            if (a == null) {
                a = com.navdy.hud.app.common.ServiceReconnector.access$000(this.this$0, this.this$0.serviceIntent);
            }
            if (a != null) {
                this.this$0.serviceIntent.setComponent(a);
                com.navdy.hud.app.common.ServiceReconnector.access$200(this.this$0).i("Trying to start service");
                if (com.navdy.hud.app.common.ServiceReconnector.access$300(this.this$0).startService(this.this$0.serviceIntent) == null) {
                    com.navdy.hud.app.common.ServiceReconnector.access$200(this.this$0).e("Service doesn't exist (uninstalled?) - retrying");
                    com.navdy.hud.app.common.ServiceReconnector.access$100(this.this$0).postDelayed((Runnable)this, 60000L);
                } else {
                    this.this$0.serviceIntent.setAction(com.navdy.hud.app.common.ServiceReconnector.access$400(this.this$0));
                    if (!com.navdy.hud.app.common.ServiceReconnector.access$300(this.this$0).bindService(this.this$0.serviceIntent, com.navdy.hud.app.common.ServiceReconnector.access$500(this.this$0), 0)) {
                        com.navdy.hud.app.common.ServiceReconnector.access$200(this.this$0).e("Unable to bind to service - aborting");
                    }
                }
            } else {
                com.navdy.hud.app.common.ServiceReconnector.access$100(this.this$0).postDelayed((Runnable)this, 60000L);
            }
        } catch(SecurityException a0) {
            com.navdy.hud.app.common.ServiceReconnector.access$200(this.this$0).e("Security exception connecting to service - aborting", (Throwable)a0);
        }
    }
}
