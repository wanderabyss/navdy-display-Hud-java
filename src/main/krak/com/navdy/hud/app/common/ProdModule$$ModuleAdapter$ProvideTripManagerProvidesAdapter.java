package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter$ProvideTripManagerProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    private dagger.internal.Binding bus;
    final private com.navdy.hud.app.common.ProdModule module;
    private dagger.internal.Binding preferences;
    
    public ProdModule$$ModuleAdapter$ProvideTripManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule a) {
        super("com.navdy.hud.app.framework.trips.TripManager", true, "com.navdy.hud.app.common.ProdModule", "provideTripManager");
        this.module = a;
        this.setLibrary(true);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
        this.preferences = a.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.framework.trips.TripManager get() {
        return this.module.provideTripManager((com.squareup.otto.Bus)this.bus.get(), (android.content.SharedPreferences)this.preferences.get());
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a.add(this.bus);
        a.add(this.preferences);
    }
}
