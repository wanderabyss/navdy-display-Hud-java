package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    private dagger.internal.Binding bus;
    final private com.navdy.hud.app.common.ProdModule module;
    private dagger.internal.Binding pathManager;
    private dagger.internal.Binding timeHelper;
    
    public ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule a) {
        super("com.navdy.hud.app.profile.DriverProfileManager", true, "com.navdy.hud.app.common.ProdModule", "provideDriverProfileManager");
        this.module = a;
        this.setLibrary(true);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
        this.pathManager = a.requestBinding("com.navdy.hud.app.storage.PathManager", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
        this.timeHelper = a.requestBinding("com.navdy.hud.app.common.TimeHelper", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.profile.DriverProfileManager get() {
        return this.module.provideDriverProfileManager((com.squareup.otto.Bus)this.bus.get(), (com.navdy.hud.app.storage.PathManager)this.pathManager.get(), (com.navdy.hud.app.common.TimeHelper)this.timeHelper.get());
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a.add(this.bus);
        a.add(this.pathManager);
        a.add(this.timeHelper);
    }
}
