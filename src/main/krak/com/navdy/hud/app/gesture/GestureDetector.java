package com.navdy.hud.app.gesture;

public class GestureDetector {
    final private static float FOV_RANGE = 240f;
    final private static float FULL_RANGE = 640f;
    public float defaultOffset;
    public float defaultSensitivity;
    protected com.navdy.service.library.events.input.GestureEvent lastEvent;
    private com.navdy.hud.app.gesture.GestureDetector$GestureListener listener;
    protected float offset;
    protected int origin;
    protected int pinchDuration;
    protected com.navdy.service.library.events.input.GestureEvent startEvent;
    boolean useRelativeOrigin;
    protected boolean useRelativePosition;
    
    public GestureDetector(com.navdy.hud.app.gesture.GestureDetector$GestureListener a) {
        this.useRelativePosition = true;
        this.useRelativeOrigin = false;
        this.offset = -1f;
        this.defaultOffset = 0.5f;
        this.defaultSensitivity = 1f;
        this.pinchDuration = 0;
        this.listener = a;
    }
    
    private float calculatePosition(com.navdy.service.library.events.input.GestureEvent a) {
        float f = 0.0f;
        if (a != null) {
            int i = a.x.intValue();
            f = (this.useRelativePosition) ? this.clamp(this.offset + (float)(i - this.origin) / (this.defaultSensitivity * 120f), 0.0f, 1f) : this.clamp(((float)i - 200f) / 240f, 0.0f, 1f);
        } else {
            f = -1f;
        }
        return f;
    }
    
    private float clamp(float f, float f0, float f1) {
        return Math.min(Math.max(f0, f), f1);
    }
    
    private boolean hasPosition(com.navdy.service.library.events.input.GestureEvent a) {
        boolean b = false;
        Integer a0 = a.x;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (a.y != null) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private void trackHand(com.navdy.service.library.events.input.GestureEvent a) {
        if (this.hasPosition(a)) {
            if (this.startEvent == null) {
                this.startEvent = a;
                int i = a.x.intValue();
                if (this.offset < 0.0f) {
                    this.offset = this.defaultOffset;
                }
                this.origin = i;
            }
            float f = this.calculatePosition(a);
            this.listener.onTrackHand(f);
        }
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        com.navdy.service.library.events.input.Gesture a0 = a.gesture;
        switch(com.navdy.hud.app.gesture.GestureDetector$1.$SwitchMap$com$navdy$service$library$events$input$Gesture[a0.ordinal()]) {
            case 11: case 12: {
                this.trackHand(a);
                break;
            }
            case 7: case 8: {
                this.pinchDuration = this.pinchDuration + 1;
                if (this.pinchDuration != 1) {
                    break;
                }
                this.listener.onClick();
                break;
            }
            case 5: case 6: {
                this.pinchDuration = 0;
                break;
            }
            case 3: case 4: {
                boolean b = this.useRelativeOrigin;
                label1: {
                    label0: {
                        if (!b) {
                            break label0;
                        }
                        if (!this.hasPosition(this.lastEvent)) {
                            break label0;
                        }
                        this.offset = this.calculatePosition(this.lastEvent);
                        break label1;
                    }
                    this.offset = -1f;
                }
                this.startEvent = null;
                break;
            }
        }
        this.lastEvent = a;
        return true;
    }
}
