package com.navdy.hud.app.gesture;

class GestureServiceConnector$7 {
    final static int[] $SwitchMap$com$navdy$hud$app$gesture$GestureServiceConnector$Error;
    
    static {
        $SwitchMap$com$navdy$hud$app$gesture$GestureServiceConnector$Error = new int[com.navdy.hud.app.gesture.GestureServiceConnector$Error.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$gesture$GestureServiceConnector$Error;
        com.navdy.hud.app.gesture.GestureServiceConnector$Error a0 = com.navdy.hud.app.gesture.GestureServiceConnector$Error.CANNOT_CONNECT;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$gesture$GestureServiceConnector$Error[com.navdy.hud.app.gesture.GestureServiceConnector$Error.COMMUNICATION_LOST.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$gesture$GestureServiceConnector$Error[com.navdy.hud.app.gesture.GestureServiceConnector$Error.SWIPED_RESTARTED.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
    }
}
