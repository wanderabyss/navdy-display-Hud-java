package com.navdy.hud.app.debug;

class GestureEngine$1 implements Runnable {
    final com.navdy.hud.app.debug.GestureEngine this$0;
    final boolean val$checked;
    
    GestureEngine$1(com.navdy.hud.app.debug.GestureEngine a, boolean b) {
        super();
        this.this$0 = a;
        this.val$checked = b;
    }
    
    public void run() {
        if (this.val$checked) {
            this.this$0.gestureServiceConnector.start();
        } else {
            this.this$0.gestureServiceConnector.stop();
        }
    }
}
