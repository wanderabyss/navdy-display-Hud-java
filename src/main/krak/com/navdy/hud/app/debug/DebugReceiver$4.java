package com.navdy.hud.app.debug;

class DebugReceiver$4 implements Runnable {
    final com.navdy.hud.app.debug.DebugReceiver this$0;
    final String val$address;
    final int val$size;
    
    DebugReceiver$4(com.navdy.hud.app.debug.DebugReceiver a, int i, String s) {
        super();
        this.this$0 = a;
        this.val$size = i;
        this.val$address = s;
    }
    
    public void run() {
        label6: {
            java.io.InputStream a = null;
            long j = 0L;
            long j0 = 0L;
            label5: {
                java.io.InputStream a0 = null;
                long j1 = 0L;
                long j2 = 0L;
                label4: {
                    java.io.InputStream a1 = null;
                    long j3 = 0L;
                    long j4 = 0L;
                    label3: {
                        java.io.InputStream a2 = null;
                        long j5 = 0L;
                        long j6 = 0L;
                        label2: try {
                            Exception a3 = null;
                            label0: {
                                java.io.IOException a4 = null;
                                label1: {
                                    java.net.SocketTimeoutException a5 = null;
                                    try {
                                        try {
                                            try {
                                                com.navdy.service.library.log.Logger a6 = com.navdy.hud.app.debug.DebugReceiver.sLogger;
                                                a2 = null;
                                                j5 = 0L;
                                                j6 = 0L;
                                                a = null;
                                                j = 0L;
                                                j0 = 0L;
                                                a0 = null;
                                                j1 = 0L;
                                                j2 = 0L;
                                                a1 = null;
                                                j3 = 0L;
                                                j4 = 0L;
                                                a6.d(new StringBuilder().append("Intended Size to be dowloaded is ").append(this.val$size).toString());
                                                java.net.HttpURLConnection a7 = (java.net.HttpURLConnection)new java.net.URL(this.val$address).openConnection();
                                                a2 = null;
                                                j5 = 0L;
                                                j6 = 0L;
                                                a = null;
                                                j = 0L;
                                                j0 = 0L;
                                                a0 = null;
                                                j1 = 0L;
                                                j2 = 0L;
                                                a1 = null;
                                                j3 = 0L;
                                                j4 = 0L;
                                                a7.setRequestMethod("GET");
                                                a7.setConnectTimeout(30000);
                                                a7.setReadTimeout(30000);
                                                int i = a7.getResponseCode();
                                                if (i != 200) {
                                                    com.navdy.service.library.log.Logger a8 = com.navdy.hud.app.debug.DebugReceiver.sLogger;
                                                    a2 = null;
                                                    j5 = 0L;
                                                    j6 = 0L;
                                                    a = null;
                                                    j = 0L;
                                                    j0 = 0L;
                                                    a0 = null;
                                                    j1 = 0L;
                                                    j2 = 0L;
                                                    a1 = null;
                                                    j3 = 0L;
                                                    j4 = 0L;
                                                    a8.e(new StringBuilder().append("Response code not OK ").append(i).toString());
                                                    a2 = null;
                                                    j = 0L;
                                                    j0 = 0L;
                                                    break label2;
                                                } else {
                                                    com.navdy.service.library.log.Logger a9 = com.navdy.hud.app.debug.DebugReceiver.sLogger;
                                                    a2 = null;
                                                    j5 = 0L;
                                                    j6 = 0L;
                                                    a = null;
                                                    j = 0L;
                                                    j0 = 0L;
                                                    a0 = null;
                                                    j1 = 0L;
                                                    j2 = 0L;
                                                    a1 = null;
                                                    j3 = 0L;
                                                    j4 = 0L;
                                                    a9.d("Starting to download");
                                                    a2 = a7.getInputStream();
                                                    j5 = 0L;
                                                    j6 = 0L;
                                                    byte[] a10 = new byte[102400];
                                                    j5 = 0L;
                                                    j6 = 0L;
                                                    a = a2;
                                                    j = 0L;
                                                    j0 = 0L;
                                                    a0 = a2;
                                                    j1 = 0L;
                                                    j2 = 0L;
                                                    a1 = a2;
                                                    j3 = 0L;
                                                    j4 = 0L;
                                                    j = android.os.SystemClock.elapsedRealtime();
                                                    j0 = 0L;
                                                    while(true) {
                                                        j5 = j;
                                                        j6 = j0;
                                                        a = a2;
                                                        a0 = a2;
                                                        j1 = j;
                                                        j2 = j0;
                                                        a1 = a2;
                                                        j3 = j;
                                                        j4 = j0;
                                                        long j7 = (long)a2.read(a10);
                                                        if (j7 == -1L) {
                                                            break label2;
                                                        }
                                                        j0 = j0 + j7;
                                                    }
                                                }
                                            } catch(java.net.SocketTimeoutException a11) {
                                                a5 = a11;
                                            }
                                        } catch(java.io.IOException a12) {
                                            a4 = a12;
                                            break label1;
                                        }
                                    } catch(Exception a13) {
                                        a3 = a13;
                                        break label0;
                                    }
                                    com.navdy.service.library.log.Logger a14 = com.navdy.hud.app.debug.DebugReceiver.sLogger;
                                    a2 = a1;
                                    j5 = j3;
                                    j6 = j4;
                                    a14.e("Socket timed out Exception ", (Throwable)a5);
                                    break label3;
                                }
                                com.navdy.service.library.log.Logger a15 = com.navdy.hud.app.debug.DebugReceiver.sLogger;
                                a2 = a0;
                                j5 = j1;
                                j6 = j2;
                                a15.e("IOException ", (Throwable)a4);
                                break label4;
                            }
                            com.navdy.service.library.log.Logger a16 = com.navdy.hud.app.debug.DebugReceiver.sLogger;
                            a2 = a;
                            j5 = j;
                            j6 = j0;
                            a16.e("Exception ", (Throwable)a3);
                            break label5;
                        } catch(Throwable a17) {
                            double d = (double)(android.os.SystemClock.elapsedRealtime() - j5);
                            float f = (float)((double)((float)j6 / 1000f) / (d / 1000.0));
                            com.navdy.hud.app.debug.DebugReceiver.sLogger.e(new StringBuilder().append("Download finished, Size : ").append(j6 / 1000L).append(", Time Taken :").append((long)d).append(" milliseconds , RawSpeed :").append(f).append(" kBps").toString());
                            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                            throw a17;
                        }
                        double d0 = (double)(android.os.SystemClock.elapsedRealtime() - j);
                        float f0 = (float)((double)((float)j0 / 1000f) / (d0 / 1000.0));
                        com.navdy.hud.app.debug.DebugReceiver.sLogger.e(new StringBuilder().append("Download finished, Size : ").append(j0 / 1000L).append(", Time Taken :").append((long)d0).append(" milliseconds , RawSpeed :").append(f0).append(" kBps").toString());
                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                        break label6;
                    }
                    double d1 = (double)(android.os.SystemClock.elapsedRealtime() - j3);
                    float f1 = (float)((double)((float)j4 / 1000f) / (d1 / 1000.0));
                    com.navdy.hud.app.debug.DebugReceiver.sLogger.e(new StringBuilder().append("Download finished, Size : ").append(j4 / 1000L).append(", Time Taken :").append((long)d1).append(" milliseconds , RawSpeed :").append(f1).append(" kBps").toString());
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
                    break label6;
                }
                double d2 = (double)(android.os.SystemClock.elapsedRealtime() - j1);
                float f2 = (float)((double)((float)j2 / 1000f) / (d2 / 1000.0));
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e(new StringBuilder().append("Download finished, Size : ").append(j2 / 1000L).append(", Time Taken :").append((long)d2).append(" milliseconds , RawSpeed :").append(f2).append(" kBps").toString());
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                break label6;
            }
            double d3 = (double)(android.os.SystemClock.elapsedRealtime() - j);
            float f3 = (float)((double)((float)j0 / 1000f) / (d3 / 1000.0));
            com.navdy.hud.app.debug.DebugReceiver.sLogger.e(new StringBuilder().append("Download finished, Size : ").append(j0 / 1000L).append(", Time Taken :").append((long)d3).append(" milliseconds , RawSpeed :").append(f3).append(" kBps").toString());
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
        }
    }
}
