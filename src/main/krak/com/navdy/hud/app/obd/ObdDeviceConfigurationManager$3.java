package com.navdy.hud.app.obd;

class ObdDeviceConfigurationManager$3 implements Runnable {
    final com.navdy.hud.app.obd.ObdDeviceConfigurationManager this$0;
    
    ObdDeviceConfigurationManager$3(com.navdy.hud.app.obd.ObdDeviceConfigurationManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        if (com.navdy.hud.app.obd.ObdDeviceConfigurationManager.isValidVin(com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$600(this.this$0)) && com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$100(this.this$0)) {
            if (com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$700() == null) {
                com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$702(com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$500(this.this$0, "vin_configuration_mapping.csv"));
            }
            com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration a = com.navdy.hud.app.obd.ObdDeviceConfigurationManager.pickConfiguration(com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$700(), com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$600(this.this$0));
            if (a == null) {
                com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$302(this.this$0, false);
            } else {
                com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$200().d(new StringBuilder().append("decodeVin : got the matching configuration :").append(a.configurationName).append(", Vin :").append(com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$600(this.this$0)).toString());
                com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$302(this.this$0, true);
                com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$000(this.this$0, a.configurationName);
            }
        }
    }
}
