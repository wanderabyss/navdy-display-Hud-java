package com.navdy.hud.app.obd;

class ObdManager$PidCheck {
    private android.os.Handler handler;
    volatile boolean hasIncorrectData;
    private Runnable invalidPidRunnable;
    int pid;
    final com.navdy.hud.app.obd.ObdManager this$0;
    volatile boolean waitingForValidData;
    
    public ObdManager$PidCheck(com.navdy.hud.app.obd.ObdManager a, int i, android.os.Handler a0) {
        super();
        this.this$0 = a;
        this.hasIncorrectData = false;
        this.waitingForValidData = false;
        this.pid = i;
        this.handler = a0;
        this.invalidPidRunnable = (Runnable)new com.navdy.hud.app.obd.ObdManager$PidCheck$1(this, a, i);
    }
    
    public void checkPid(double d) {
        if (this.isPidValueValid(d)) {
            this.reset();
        } else if (!this.hasIncorrectData) {
            if (this.waitingForValidData) {
                com.navdy.hud.app.obd.ObdManager.access$700().d(new StringBuilder().append("Already waiting for valid PID data, PID : ").append(this.pid).append(" ,  Value : ").append(d).toString());
            } else {
                this.handler.removeCallbacks(this.invalidPidRunnable);
                this.handler.postDelayed(this.invalidPidRunnable, this.getWaitForValidDataTimeout());
                this.waitingForValidData = true;
            }
        }
    }
    
    public long getWaitForValidDataTimeout() {
        return 0L;
    }
    
    public boolean hasIncorrectData() {
        return this.hasIncorrectData;
    }
    
    public void invalidatePid(int i) {
    }
    
    public boolean isPidValueValid(double d) {
        return true;
    }
    
    public boolean isWaitingForValidData() {
        return this.waitingForValidData;
    }
    
    public void reset() {
        this.hasIncorrectData = false;
        this.handler.removeCallbacks(this.invalidPidRunnable);
        this.waitingForValidData = false;
    }
}
