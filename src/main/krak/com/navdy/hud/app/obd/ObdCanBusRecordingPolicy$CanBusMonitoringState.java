package com.navdy.hud.app.obd;


    public enum ObdCanBusRecordingPolicy$CanBusMonitoringState {
        UNKNOWN(0),
        SUCCESS(1),
        FAILURE(2);

        private int value;
        ObdCanBusRecordingPolicy$CanBusMonitoringState(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class ObdCanBusRecordingPolicy$CanBusMonitoringState extends Enum {
//    final private static com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState[] $VALUES;
//    final public static com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState FAILURE;
//    final public static com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState SUCCESS;
//    final public static com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState UNKNOWN;
//    
//    static {
//        UNKNOWN = new com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState("UNKNOWN", 0);
//        SUCCESS = new com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState("SUCCESS", 1);
//        FAILURE = new com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState("FAILURE", 2);
//        com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState[] a = new com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState[3];
//        a[0] = UNKNOWN;
//        a[1] = SUCCESS;
//        a[2] = FAILURE;
//        $VALUES = a;
//    }
//    
//    private ObdCanBusRecordingPolicy$CanBusMonitoringState(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState valueOf(String s) {
//        return (com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState)Enum.valueOf(com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState.class, s);
//    }
//    
//    public static com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState[] values() {
//        return $VALUES.clone();
//    }
//}
//