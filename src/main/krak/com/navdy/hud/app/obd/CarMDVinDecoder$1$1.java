package com.navdy.hud.app.obd;

class CarMDVinDecoder$1$1 implements okhttp3.Callback {
    final com.navdy.hud.app.obd.CarMDVinDecoder$1 this$1;
    
    CarMDVinDecoder$1$1(com.navdy.hud.app.obd.CarMDVinDecoder$1 a) {
        super();
        this.this$1 = a;
    }
    
    public void onFailure(okhttp3.Call a, java.io.IOException a0) {
        com.navdy.hud.app.obd.CarMDVinDecoder.access$100().e("onFailure : Decoding failed ", (Throwable)a0);
        com.navdy.hud.app.obd.CarMDVinDecoder.access$302(this.this$1.this$0, false);
    }
    
    public void onResponse(okhttp3.Call a, okhttp3.Response a0) {
        com.navdy.hud.app.obd.CarMDVinDecoder.access$100().d("onResponse : Decoding response received");
        com.navdy.hud.app.obd.CarMDVinDecoder.access$800(this.this$1.this$0, a0.body());
        com.navdy.hud.app.obd.CarMDVinDecoder.access$302(this.this$1.this$0, false);
    }
}
