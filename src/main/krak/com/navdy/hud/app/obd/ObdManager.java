package com.navdy.hud.app.obd;

final public class ObdManager {
    final public static float BATTERY_CHARGING_VOLTAGE = 13.1f;
    final public static double BATTERY_NO_OBD = -1.0;
    final private static String BLACKLIST_MODIFICATION_TIME = "blacklist_modification_time";
    final public static double CRITICAL_BATTERY_VOLTAGE;
    final private static String CRITICAL_VOLTAGE_OVERRIDE_PROPERTY = "persist.sys.voltage.crit";
    final private static float DEFAULT_CRITICAL_BATTERY_VOLTAGE = 12f;
    final private static long DEFAULT_LOW_VOLTAGE_SHUTOFF_DURATION;
    final private static String DEFAULT_SET = "default_set";
    final private static int DISTANCE_UPDATE_INTERVAL = 60000;
    final private static int ENGINE_TEMP_UPDATE_INTERVAL = 2000;
    final public static String FIRMWARE_VERSION = "4.2.1";
    final public static String FIRST_ODOMETER_READING_KM = "first_odometer_reading";
    final private static int FLUSH_INTERVAL_MS = 30000;
    final private static int FUEL_UPDATE_INTERVAL = 15000;
    final private static int GENERIC_UPDATE_INTERVAL = 1000;
    final public static String LAST_ODOMETER_READING_KM = "last_odometer_reading";
    final private static int LOW_BATTERY_COUNT_THRESHOLD;
    final public static double LOW_BATTERY_VOLTAGE;
    final public static int LOW_FUEL_LEVEL = 10;
    final private static String LOW_VOLTAGE_DURATION_OVERRIDE_PROPERTY = "persist.sys.voltage.duration";
    final private static String LOW_VOLTAGE_OVERRIDE_PROPERTY = "persist.sys.voltage.low";
    final private static long LOW_VOLTAGE_SHUTOFF_DURATION;
    final public static long MINIMUM_TIME_BETWEEN_MILEAGE_REPORTING;
    final private static int MPG_UPDATE_INTERVAL = 1000;
    final public static int NOT_AVAILABLE = -1;
    final private static com.navdy.hud.app.obd.ObdManager$ObdConnectionStatusEvent OBD_CONNECTED;
    final private static com.navdy.hud.app.obd.ObdManager$ObdConnectionStatusEvent OBD_NOT_CONNECTED;
    final private static int RPM_UPDATE_INTERVAL = 250;
    final public static int SAFE_TO_SLEEP_BATTERY_OBSERVED_COUNT_THRESHOLD = 5;
    final private static String SCAN_SETTING = "scan_setting";
    final private static int SLOW_RPM_UPDATE_INTERVAL = 2500;
    final private static int SPEED_UPDATE_INTERVAL = 100;
    final private static int STATE_CONNECTED = 2;
    final private static int STATE_CONNECTING = 1;
    final private static int STATE_DISCONNECTED = 0;
    final private static int STATE_IDLE = 4;
    final private static int STATE_SLEEPING = 5;
    final public static String TELEMETRY_TAG = "Telemetry";
    final public static String TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS = "total_distance_travelled_with_navdy";
    final public static int VALID_FUEL_DATA_WAIT_TIME = 45000;
    final public static String VEHICLE_VIN = "vehicle_vin";
    final public static int VOLTAGE_REPORT_INTERVAL = 2000;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.service.library.log.Logger sTelemetryLogger;
    final private static com.navdy.hud.app.obd.ObdManager singleton;
    private double batteryVoltage;
    @Inject
    com.squareup.otto.Bus bus;
    private com.navdy.obd.ICanBusMonitoringListener canBusMonitoringListener;
    private com.navdy.hud.app.obd.CarServiceConnector carServiceConnector;
    private Runnable checkVoltage;
    private com.navdy.hud.app.obd.ObdManager$ConnectionType connectionType;
    private com.navdy.hud.app.debug.DriveRecorder driveRecorder;
    @Inject
    com.navdy.hud.app.profile.DriverProfileManager driverProfileManager;
    private java.util.List ecus;
    private boolean firstScan;
    com.navdy.hud.app.obd.ObdManager$PidCheck fuelPidCheck;
    private com.navdy.obd.ScanSchedule fullScan;
    private android.os.Handler handler;
    private boolean isCheckEngineLightOn;
    private int lowBatteryCount;
    private double maxBatteryVoltage;
    private long mileageEventLastReportTime;
    private double minBatteryVoltage;
    private com.navdy.obd.ScanSchedule minimalScan;
    private com.navdy.hud.app.obd.ObdCanBusRecordingPolicy obdCanBusRecordingPolicy;
    private String obdChipFirmwareVersion;
    private volatile boolean obdConnected;
    private com.navdy.hud.app.obd.ObdDeviceConfigurationManager obdDeviceConfigurationManager;
    private java.util.HashMap obdPidsCache;
    private boolean odoMeterReadingValidated;
    private Runnable periodicCheckRunnable;
    private com.navdy.obd.IPidListener pidListener;
    @Inject
    com.navdy.hud.app.device.PowerManager powerManager;
    private String protocol;
    private com.navdy.obd.IPidListener recordingPidListener;
    private int safeToSleepBatteryLevelObservedCount;
    private com.navdy.obd.ScanSchedule schedule;
    @Inject
    android.content.SharedPreferences sharedPreferences;
    private com.navdy.hud.app.manager.SpeedManager speedManager;
    private com.navdy.obd.PidSet supportedPidSet;
    @Inject
    public com.navdy.hud.app.analytics.TelemetryDataManager telemetryDataManager;
    private long totalDistanceInCurrentSessionPersisted;
    @Inject
    com.navdy.hud.app.framework.trips.TripManager tripManager;
    private java.util.List troubleCodes;
    private String vin;
    
    static {
        DEFAULT_LOW_VOLTAGE_SHUTOFF_DURATION = java.util.concurrent.TimeUnit.MINUTES.toMillis(15L);
        LOW_BATTERY_VOLTAGE = (double)com.navdy.hud.app.util.os.SystemProperties.getFloat("persist.sys.voltage.low", 12.2f);
        CRITICAL_BATTERY_VOLTAGE = (double)com.navdy.hud.app.util.os.SystemProperties.getFloat("persist.sys.voltage.crit", 12f);
        LOW_VOLTAGE_SHUTOFF_DURATION = com.navdy.hud.app.util.os.SystemProperties.getLong("persist.sys.voltage.duration", DEFAULT_LOW_VOLTAGE_SHUTOFF_DURATION);
        MINIMUM_TIME_BETWEEN_MILEAGE_REPORTING = java.util.concurrent.TimeUnit.MINUTES.toMillis(10L);
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.obd.ObdManager.class);
        sTelemetryLogger = new com.navdy.service.library.log.Logger("Telemetry");
        OBD_CONNECTED = new com.navdy.hud.app.obd.ObdManager$ObdConnectionStatusEvent(true);
        OBD_NOT_CONNECTED = new com.navdy.hud.app.obd.ObdManager$ObdConnectionStatusEvent(false);
        LOW_BATTERY_COUNT_THRESHOLD = (int)Math.ceil((double)LOW_VOLTAGE_SHUTOFF_DURATION / 2000.0);
        singleton = new com.navdy.hud.app.obd.ObdManager();
    }
    
    private ObdManager() {
        this.odoMeterReadingValidated = false;
        this.totalDistanceInCurrentSessionPersisted = 0L;
        this.mileageEventLastReportTime = 0L;
        this.connectionType = com.navdy.hud.app.obd.ObdManager$ConnectionType.POWER_ONLY;
        this.supportedPidSet = new com.navdy.obd.PidSet();
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
        this.obdPidsCache = new java.util.HashMap();
        this.batteryVoltage = -1.0;
        this.minBatteryVoltage = -1.0;
        this.maxBatteryVoltage = -1.0;
        this.safeToSleepBatteryLevelObservedCount = 0;
        this.isCheckEngineLightOn = false;
        this.periodicCheckRunnable = (Runnable)new com.navdy.hud.app.obd.ObdManager$4(this);
        this.checkVoltage = (Runnable)new com.navdy.hud.app.obd.ObdManager$5(this);
        this.pidListener = (com.navdy.obd.IPidListener)new com.navdy.hud.app.obd.ObdManager$6(this);
        this.canBusMonitoringListener = (com.navdy.obd.ICanBusMonitoringListener)new com.navdy.hud.app.obd.ObdManager$7(this);
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        this.obdCanBusRecordingPolicy = new com.navdy.hud.app.obd.ObdCanBusRecordingPolicy(com.navdy.hud.app.HudApplication.getAppContext(), this.sharedPreferences, this, this.tripManager);
        this.carServiceConnector = new com.navdy.hud.app.obd.CarServiceConnector(com.navdy.hud.app.HudApplication.getAppContext());
        this.obdDeviceConfigurationManager = new com.navdy.hud.app.obd.ObdDeviceConfigurationManager(this.carServiceConnector);
        this.minimalScan = new com.navdy.obd.ScanSchedule();
        this.minimalScan.addPid(13, 100);
        this.minimalScan.addPid(47, 15000);
        this.minimalScan.addPid(49, 60000);
        this.fullScan = new com.navdy.obd.ScanSchedule(this.minimalScan);
        this.fullScan.addPid(12, 250);
        this.fullScan.addPid(16, 1000);
        this.fullScan.addPid(256, 1000);
        this.fullScan.addPid(5, 2000);
        this.fullScan.addPid(258, 1000);
        this.fullScan.addPid(259, 1000);
        this.fullScan.addPid(260, 1000);
        this.minimalScan.addPid(12, 2500);
        this.bus.register(this);
        this.bus.register(this.obdDeviceConfigurationManager);
        this.bus.register(this.obdCanBusRecordingPolicy);
        sTelemetryLogger.v("SESSION started");
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.fuelPidCheck = new com.navdy.hud.app.obd.ObdManager$1(this, 47, this.handler);
        com.navdy.hud.app.obd.ObdManager$2 a = new com.navdy.hud.app.obd.ObdManager$2(this);
        this.handler.post((Runnable)a);
    }
    
    static java.util.HashMap access$000(com.navdy.hud.app.obd.ObdManager a) {
        return a.obdPidsCache;
    }
    
    static com.navdy.obd.PidSet access$100(com.navdy.hud.app.obd.ObdManager a) {
        return a.supportedPidSet;
    }
    
    static int access$1000() {
        return LOW_BATTERY_COUNT_THRESHOLD;
    }
    
    static com.navdy.obd.PidSet access$102(com.navdy.hud.app.obd.ObdManager a, com.navdy.obd.PidSet a0) {
        a.supportedPidSet = a0;
        return a0;
    }
    
    static Runnable access$1100(com.navdy.hud.app.obd.ObdManager a) {
        return a.periodicCheckRunnable;
    }
    
    static boolean access$1200(com.navdy.hud.app.obd.ObdManager a) {
        return a.firstScan;
    }
    
    static boolean access$1202(com.navdy.hud.app.obd.ObdManager a, boolean b) {
        a.firstScan = b;
        return b;
    }
    
    static com.navdy.hud.app.obd.CarServiceConnector access$1300(com.navdy.hud.app.obd.ObdManager a) {
        return a.carServiceConnector;
    }
    
    static String access$1400(com.navdy.hud.app.obd.ObdManager a) {
        return a.vin;
    }
    
    static String access$1402(com.navdy.hud.app.obd.ObdManager a, String s) {
        a.vin = s;
        return s;
    }
    
    static void access$1500(com.navdy.hud.app.obd.ObdManager a, String s) {
        a.onVinRead(s);
    }
    
    static java.util.List access$1602(com.navdy.hud.app.obd.ObdManager a, java.util.List a0) {
        a.ecus = a0;
        return a0;
    }
    
    static String access$1702(com.navdy.hud.app.obd.ObdManager a, String s) {
        a.protocol = s;
        return s;
    }
    
    static com.navdy.service.library.log.Logger access$1800() {
        return sTelemetryLogger;
    }
    
    static com.navdy.hud.app.manager.SpeedManager access$1900(com.navdy.hud.app.obd.ObdManager a) {
        return a.speedManager;
    }
    
    static android.os.Handler access$200(com.navdy.hud.app.obd.ObdManager a) {
        return a.handler;
    }
    
    static void access$2000(com.navdy.hud.app.obd.ObdManager a, long j) {
        a.updateCumulativeVehicleDistanceTravelled(j);
    }
    
    static com.navdy.obd.IPidListener access$2100(com.navdy.hud.app.obd.ObdManager a) {
        return a.recordingPidListener;
    }
    
    static void access$2200(com.navdy.hud.app.obd.ObdManager a, int i) {
        a.updateConnectionState(i);
    }
    
    static com.navdy.hud.app.obd.ObdCanBusRecordingPolicy access$2300(com.navdy.hud.app.obd.ObdManager a) {
        return a.obdCanBusRecordingPolicy;
    }
    
    static Runnable access$300(com.navdy.hud.app.obd.ObdManager a) {
        return a.checkVoltage;
    }
    
    static double access$400(com.navdy.hud.app.obd.ObdManager a) {
        return a.batteryVoltage;
    }
    
    static double access$402(com.navdy.hud.app.obd.ObdManager a, double d) {
        a.batteryVoltage = d;
        return d;
    }
    
    static double access$500(com.navdy.hud.app.obd.ObdManager a) {
        return a.minBatteryVoltage;
    }
    
    static double access$502(com.navdy.hud.app.obd.ObdManager a, double d) {
        a.minBatteryVoltage = d;
        return d;
    }
    
    static int access$600(com.navdy.hud.app.obd.ObdManager a) {
        return a.lowBatteryCount;
    }
    
    static int access$602(com.navdy.hud.app.obd.ObdManager a, int i) {
        a.lowBatteryCount = i;
        return i;
    }
    
    static int access$608(com.navdy.hud.app.obd.ObdManager a) {
        int i = a.lowBatteryCount;
        a.lowBatteryCount = i + 1;
        return i;
    }
    
    static com.navdy.service.library.log.Logger access$700() {
        return sLogger;
    }
    
    static int access$800(com.navdy.hud.app.obd.ObdManager a) {
        return a.safeToSleepBatteryLevelObservedCount;
    }
    
    static int access$802(com.navdy.hud.app.obd.ObdManager a, int i) {
        a.safeToSleepBatteryLevelObservedCount = i;
        return i;
    }
    
    static int access$808(com.navdy.hud.app.obd.ObdManager a) {
        int i = a.safeToSleepBatteryLevelObservedCount;
        a.safeToSleepBatteryLevelObservedCount = i + 1;
        return i;
    }
    
    static double access$900(com.navdy.hud.app.obd.ObdManager a) {
        return a.maxBatteryVoltage;
    }
    
    static double access$902(com.navdy.hud.app.obd.ObdManager a, double d) {
        a.maxBatteryVoltage = d;
        return d;
    }
    
    public static com.navdy.hud.app.obd.ObdManager getInstance() {
        return singleton;
    }
    
    private com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting getScanSetting(android.content.SharedPreferences a) {
        com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting a0 = null;
        try {
            a0 = com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting.SCAN_DEFAULT_ON;
            a0 = com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting.valueOf(a.getString("scan_setting", a0.name()));
        } catch(Exception a1) {
            sLogger.e("Failed to parse saved scan setting ", (Throwable)a1);
        }
        return a0;
    }
    
    private void onVinRead(String s) {
        synchronized(this) {
            android.content.SharedPreferences a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
            String s0 = a.getString("vehicle_vin", (String)null);
            if (android.text.TextUtils.equals((CharSequence)s, (CharSequence)s0)) {
                sLogger.d(new StringBuilder().append("Vin has not changed , Vin ").append(s).toString());
            } else {
                sLogger.d(new StringBuilder().append("Vin changed , Old :").append(s0).append(", New Vin :").append(s).toString());
                if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                    a.edit().remove("vehicle_vin").apply();
                } else {
                    a.edit().putString("vehicle_vin", s).apply();
                }
                this.resetCumulativeMileageData();
            }
        }
        /*monexit(this)*/;
    }
    
    private void reportMileageEvent() {
        long j = android.os.SystemClock.elapsedRealtime();
        long j0 = this.mileageEventLastReportTime;
        int i = (j0 < 0L) ? -1 : (j0 == 0L) ? 0 : 1;
        label0: {
            label1: {
                if (i == 0) {
                    break label1;
                }
                if (j - this.mileageEventLastReportTime <= MINIMUM_TIME_BETWEEN_MILEAGE_REPORTING) {
                    break label0;
                }
            }
            android.content.SharedPreferences a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
            long j1 = a.getLong("first_odometer_reading", 0L);
            long j2 = a.getLong("last_odometer_reading", 0L);
            double d = (double)(j2 - j1);
            sLogger.d(new StringBuilder().append("Initial reading (KM) : ").append(j1).append(", Current reading (KM) : ").append(j2).append(" , Total (KM) : ").append(d).toString());
            double d0 = (double)a.getLong("total_distance_travelled_with_navdy", 0L);
            sLogger.d(new StringBuilder().append("Distance travelled with Navdy(Meters) : ").append(d0).toString());
            double d1 = d0 / 1000.0;
            if (d > 0.0 && d1 > 0.0) {
                double d2 = d / d;
                sLogger.d(new StringBuilder().append("Reporting the Navdy Mileage Vehicle (KMs) : ").append(d).append(" , Navdy (KMs) : ").append(d).append(" , Navdy Usage rate ").append(d2).toString());
                com.navdy.hud.app.analytics.AnalyticsSupport.recordNavdyMileageEvent(d, d1, d2);
                this.mileageEventLastReportTime = j;
            }
        }
    }
    
    private void resetCumulativeMileageData() {
        android.content.SharedPreferences a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
        sLogger.d("Resetting the cumulative mileage statistics");
        a.edit().putLong("first_odometer_reading", 0L).putLong("last_odometer_reading", 0L).putLong("total_distance_travelled_with_navdy", 0L).apply();
        this.totalDistanceInCurrentSessionPersisted = 0L;
        this.odoMeterReadingValidated = false;
    }
    
    private void setConnected(boolean b) {
        if (this.obdConnected != b) {
            if (this.driveRecorder != null) {
                this.driveRecorder.setRealObdConnected(b);
            }
            this.fuelPidCheck.reset();
            this.obdDeviceConfigurationManager.setConnectionState(b);
            this.obdConnected = b;
            label0: {
                Throwable a = null;
                if (b) {
                    try {
                        com.navdy.obd.ICarService a0 = this.carServiceConnector.getCarApi();
                        this.vin = a0.getVIN();
                        this.onVinRead(this.vin);
                        this.ecus = a0.getEcus();
                        this.protocol = a0.getProtocol();
                        this.isCheckEngineLightOn = a0.isCheckEngineLightOn();
                        this.troubleCodes = a0.getTroubleCodes();
                        this.obdChipFirmwareVersion = a0.getObdChipFirmwareVersion();
                        sLogger.d(new StringBuilder().append("Connected, Obd chip firmware version ").append(this.obdChipFirmwareVersion).toString());
                        break label0;
                    } catch(Throwable a1) {
                        a = a1;
                    }
                } else {
                    this.firstScan = true;
                    this.vin = null;
                    this.ecus = null;
                    this.protocol = null;
                    this.speedManager.setObdSpeed(-1, 0L);
                    if (this.supportedPidSet != null) {
                        this.supportedPidSet.clear();
                    }
                    this.obdPidsCache.clear();
                    break label0;
                }
                sLogger.d("Failed to read vin/ecu/protocol after connecting", a);
            }
            this.bus.post((b ? OBD_CONNECTED : OBD_NOT_CONNECTED));
            this.obdCanBusRecordingPolicy.onObdConnectionStateChanged(b);
        }
    }
    
    private void setScanSchedule(com.navdy.obd.ScanSchedule a) {
        if (a != this.schedule) {
            this.schedule = a;
            this.updateListener();
        }
    }
    
    private void updateConnectionState(int i) {
        if (i != 2) {
            this.setConnected(false);
        } else {
            this.setConnected(true);
        }
        if (this.connectionType == com.navdy.hud.app.obd.ObdManager$ConnectionType.POWER_ONLY && i > 1) {
            this.connectionType = com.navdy.hud.app.obd.ObdManager$ConnectionType.OBD;
        }
    }
    
    private void updateCumulativeVehicleDistanceTravelled(long j) {
        label0: synchronized(this) {
        int i = (j < 0L) ? -1 : (j == 0L) ? 0 : 1;
            //Throwable a = null;
            if (i <= 0) {
                break label0;
            }
            //try {
                sLogger.d(new StringBuilder().append("New odometer reading (KMs) : ").append(j).toString());
                android.content.SharedPreferences a0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
                if (!this.odoMeterReadingValidated) {
                    sLogger.d("Validating the Cumulative mileage data");
                    double d = (double)a0.getLong("last_odometer_reading", 0L);
                    if (d != 0.0) {
                        if ((double)j < d) {
                            sLogger.d(new StringBuilder().append("Odometer reading mismatch with saved reading, resetting, Saved :").append(d).append(", New : ").append(j).toString());
                            this.resetCumulativeMileageData();
                            a0.edit().putLong("first_odometer_reading", j).apply();
                        }
                    } else {
                        sLogger.d("No saved odo meter reading, starting fresh");
                        this.resetCumulativeMileageData();
                        a0.edit().putLong("first_odometer_reading", j).apply();
                    }
                    this.odoMeterReadingValidated = true;
                }
                a0.edit().putLong("last_odometer_reading", j).apply();
                com.navdy.hud.app.framework.trips.TripManager a1 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTripManager();
                long j0 = a0.getLong("total_distance_travelled_with_navdy", 0L);
                long j1 = (long)a1.getTotalDistanceTravelled();
                sLogger.d(new StringBuilder().append("Saving total distance travelled in current session, before :").append(j0).append(", Current Trip mileage : ").append(a1.getTotalDistanceTravelled()).toString());
                long j2 = j1 - this.totalDistanceInCurrentSessionPersisted;
                if (j2 <= 0L) {
                    break label0;
                }
                a0.edit().putLong("total_distance_travelled_with_navdy", j0 + j2).apply();
                this.totalDistanceInCurrentSessionPersisted = j1;
                this.reportMileageEvent();
                break label0;
            //} catch(Throwable a2) {
            //    a = a2;
            //}
            /*monexit(this)*/;
            //throw a;
        }
        /*monexit(this)*/;
    }
    
    private void updateObdScanSetting() {
        if (this.driverProfileManager != null) {
            com.navdy.hud.app.profile.DriverProfile a = this.driverProfileManager.getCurrentProfile();
            if (a != null) {
                label3: if (a.isAutoOnEnabled()) {
                    boolean b = android.text.TextUtils.isEmpty((CharSequence)a.getCarMake());
                    label4: {
                        if (!b) {
                            break label4;
                        }
                        if (!android.text.TextUtils.isEmpty((CharSequence)a.getCarModel())) {
                            break label4;
                        }
                        if (android.text.TextUtils.isEmpty((CharSequence)a.getCarYear())) {
                            break label3;
                        }
                    }
                    this.obdCanBusRecordingPolicy.onCarDetailsAvailable(a.getCarMake(), a.getCarModel(), a.getCarYear());
                    this.obdDeviceConfigurationManager.setCarDetails(a.getCarMake(), a.getCarModel(), a.getCarYear());
                } else {
                    this.obdDeviceConfigurationManager.setAutoOnEnabled(false);
                }
                com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting a0 = a.getObdScanSetting();
                sLogger.d(new StringBuilder().append("ObdScanSetting received : ").append(a0).toString());
                if (a0 != null) {
                    com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting a1 = null;
                    boolean b0 = false;
                    boolean b1 = false;
                    android.content.SharedPreferences a2 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
                    if (a2.getBoolean("default_set", false)) {
                        a1 = (this.isObdScanningEnabled()) ? com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting.SCAN_DEFAULT_ON : com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting.SCAN_DEFAULT_OFF;
                        a2.edit().remove("default_set").apply();
                        b0 = true;
                    } else {
                        a1 = this.getScanSetting(a2);
                        b0 = false;
                    }
                    long j = a.getObdBlacklistModificationTime();
                    long j0 = a2.getLong("blacklist_modification_time", 0L);
                    boolean b2 = j > j0;
                    sLogger.d(new StringBuilder().append("saved setting:").append(a1).append(" blacklist updated: (").append(j).append("/").append(j0).append(") (phone/local)").toString());
                    sLogger.d(new StringBuilder().append("Is Obd enabled :").append(this.isObdScanningEnabled()).toString());
                    com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting a3 = com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting.SCAN_DEFAULT_OFF;
                    label2: {
                        label0: {
                            label1: {
                                if (a1 == a3) {
                                    break label1;
                                }
                                if (a1 != com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting.SCAN_DEFAULT_ON) {
                                    break label0;
                                }
                            }
                            b1 = true;
                            break label2;
                        }
                        b1 = false;
                    }
                    switch(com.navdy.hud.app.obd.ObdManager$8.$SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$ObdScanSetting[a0.ordinal()]) {
                        case 4: {
                            if (!b2) {
                                break;
                            }
                            if (!b1) {
                                break;
                            }
                            this.enableObdScanning(false);
                            b0 = true;
                            break;
                        }
                        case 3: {
                            this.enableObdScanning(false);
                            b0 = true;
                            break;
                        }
                        case 2: {
                            this.enableObdScanning(true);
                            b0 = true;
                            break;
                        }
                        case 1: {
                            if (!b2) {
                                break;
                            }
                            if (!b1) {
                                break;
                            }
                            sLogger.d("Setting default to on");
                            this.enableObdScanning(true);
                            b0 = true;
                            break;
                        }
                    }
                    if (b0) {
                        sLogger.i(new StringBuilder().append("Writing scan setting:").append(a0).append(" modTime:").append(j).toString());
                        a2.edit().putString("scan_setting", a0.name()).putLong("blacklist_modification_time", j).apply();
                    }
                }
            }
        }
    }
    
    public void enableInstantaneousMode(boolean b) {
        this.obdCanBusRecordingPolicy.onInstantaneousModeChanged(b);
        if (b) {
            this.setScanSchedule(this.fullScan);
        } else if (com.navdy.hud.app.debug.DriveRecorder.isAutoRecordingEnabled()) {
            this.setScanSchedule(this.fullScan);
        } else {
            this.setScanSchedule(this.minimalScan);
        }
    }
    
    public void enableObdScanning(boolean b) {
        com.navdy.obd.ICarService a = this.carServiceConnector.getCarApi();
        label0: {
            android.os.RemoteException a0 = null;
            if (a == null) {
                break label0;
            }
            try {
                sLogger.i(new StringBuilder().append("setting obd scanning enabled:").append(b).toString());
                a.setObdPidsScanningEnabled(b);
                break label0;
            } catch(android.os.RemoteException a1) {
                a0 = a1;
            }
            sLogger.e(new StringBuilder().append("Error while ").append(b ? "enabling " : "disabling ").append("Obd PIDs scanning ").toString(), (Throwable)a0);
        }
    }
    
    public double getBatteryVoltage() {
        com.navdy.obd.ICarService a = this.carServiceConnector.getCarApi();
        label0: {
            Throwable a0 = null;
            if (a == null) {
                break label0;
            }
            try {
                this.batteryVoltage = a.getBatteryVoltage();
                break label0;
            } catch(Throwable a1) {
                a0 = a1;
            }
            sLogger.e(a0);
        }
        return this.batteryVoltage;
    }
    
    public com.navdy.obd.ICarService getCarService() {
        com.navdy.obd.ICarService a = (this.carServiceConnector == null) ? null : this.carServiceConnector.getCarApi();
        return a;
    }
    
    public com.navdy.hud.app.obd.ObdManager$ConnectionType getConnectionType() {
        return this.connectionType;
    }
    
    public int getDistanceTravelled() {
        return (int)this.getPidValue(49);
    }
    
    public long getDistanceTravelledWithNavdy() {
        return com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences().getLong("total_distance_travelled_with_navdy", -1L);
    }
    
    public com.navdy.hud.app.debug.DriveRecorder getDriveRecorder() {
        return this.driveRecorder;
    }
    
    public java.util.List getEcus() {
        return this.ecus;
    }
    
    public int getEngineRpm() {
        return (int)this.getPidValue(12);
    }
    
    public int getFuelLevel() {
        int i = 0;
        boolean b = this.fuelPidCheck.hasIncorrectData;
        label2: {
            label0: {
                label1: {
                    if (b) {
                        break label1;
                    }
                    if (!this.fuelPidCheck.isWaitingForValidData()) {
                        break label0;
                    }
                }
                i = -1;
                break label2;
            }
            i = (int)this.getPidValue(47);
        }
        return i;
    }
    
    public double getInstantFuelConsumption() {
        return (double)(int)this.getPidValue(256);
    }
    
    public double getMaxBatteryVoltage() {
        return this.maxBatteryVoltage;
    }
    
    public double getMinBatteryVoltage() {
        return this.minBatteryVoltage;
    }
    
    public com.navdy.hud.app.obd.ObdCanBusRecordingPolicy getObdCanBusRecordingPolicy() {
        return this.obdCanBusRecordingPolicy;
    }
    
    public String getObdChipFirmwareVersion() {
        return this.obdChipFirmwareVersion;
    }
    
    public com.navdy.hud.app.obd.ObdDeviceConfigurationManager getObdDeviceConfigurationManager() {
        return this.obdDeviceConfigurationManager;
    }
    
    public double getPidValue(int i) {
        return (this.obdPidsCache.containsKey(Integer.valueOf(i))) ? ((Double)this.obdPidsCache.get(Integer.valueOf(i))).doubleValue() : -1.0;
    }
    
    public String getProtocol() {
        return this.protocol;
    }
    
    public com.navdy.obd.PidSet getSupportedPids() {
        return this.supportedPidSet;
    }
    
    public java.util.List getTroubleCodes() {
        return this.troubleCodes;
    }
    
    public String getVin() {
        return this.vin;
    }
    
    public void injectObdData(java.util.List a, java.util.List a0) {
        try {
            this.pidListener.pidsRead(a, a0);
        } catch(android.os.RemoteException ignoredException) {
            sLogger.e("Remote exception when injecting fake obd data");
        }
    }
    
    public boolean isCheckEngineLightOn() {
        return this.isCheckEngineLightOn;
    }
    
    public boolean isConnected() {
        return this.obdConnected;
    }
    
    public boolean isObdScanningEnabled() {
        boolean b = false;
        com.navdy.obd.ICarService a = this.carServiceConnector.getCarApi();
        label0: {
            label1: {
                android.os.RemoteException a0 = null;
                if (a == null) {
                    break label1;
                }
                try {
                    b = a.isObdPidsScanningEnabled();
                    break label0;
                } catch(android.os.RemoteException a1) {
                    a0 = a1;
                }
                sLogger.e("RemoteException ", (Throwable)a0);
            }
            b = false;
        }
        return b;
    }
    
    public boolean isSleeping() {
        boolean b = false;
        com.navdy.obd.ICarService a = this.carServiceConnector.getCarApi();
        label1: {
            label0: if (a == null) {
                b = false;
                break label1;
            } else {
                int i = 0;
                try {
                    i = a.getConnectionState();
                } catch(android.os.RemoteException ignoredException) {
                    break label0;
                }
                b = i == 5;
                break label1;
            }
            b = false;
        }
        return b;
    }
    
    public boolean isSpeedPidAvailable() {
        return this.supportedPidSet != null && this.supportedPidSet.contains(13);
    }
    
    public void monitorBatteryVoltage() {
        sLogger.i("monitorBatteryVoltage()");
        this.handler.removeCallbacks(this.periodicCheckRunnable);
        this.handler.postDelayed(this.periodicCheckRunnable, 4000L);
    }
    
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged a) {
        this.updateObdScanSetting();
    }
    
    public void onDriverProfileUpdated(com.navdy.hud.app.event.DriverProfileUpdated a) {
        this.updateObdScanSetting();
    }
    
    public void onDrivingStateChange(com.navdy.hud.app.event.DrivingStateChange a) {
        boolean b = a.driving;
        label0: {
            if (!b) {
                break label0;
            }
            if (this.isConnected()) {
                break label0;
            }
            com.navdy.obd.ICarService a0 = this.carServiceConnector.getCarApi();
            if (a0 == null) {
                break label0;
            }
            try {
                sLogger.i("Driving started - triggering obd scan");
                a0.rescan();
                break label0;
            } catch(android.os.RemoteException ignoredException) {
            }
            sLogger.e("Failed to trigger OBD rescan");
        }
    }
    
    public void onObdStatusRequest(com.navdy.service.library.events.obd.ObdStatusRequest a) {
        int i = this.speedManager.getObdSpeed();
        com.squareup.otto.Bus a0 = this.bus;
        com.navdy.service.library.events.RequestStatus a1 = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
        boolean b = this.isConnected();
        String s = this.vin;
        Integer a2 = (i == -1) ? null : Integer.valueOf(i);
        a0.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.obd.ObdStatusResponse(a1, Boolean.valueOf(b), s, a2)));
    }
    
    public void onShutdown(com.navdy.hud.app.event.Shutdown a) {
        if (a.state == com.navdy.hud.app.event.Shutdown$State.SHUTTING_DOWN) {
            this.carServiceConnector.shutdown();
        }
    }
    
    public void onWakeUp(com.navdy.hud.app.event.Wakeup a) {
        sLogger.d("onWakeUp");
        this.updateListener();
    }
    
    void serviceConnected() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.obd.ObdManager$3(this), 6);
    }
    
    void serviceDisconnected() {
        this.setConnected(false);
    }
    
    public void setDriveRecorder(com.navdy.hud.app.debug.DriveRecorder a) {
        this.driveRecorder = a;
    }
    
    public void setMode(int i, boolean b) {
        switch(i) {
            case 0: case 1: {
                com.navdy.obd.ICarService a = this.carServiceConnector.getCarApi();
                label1: {
                    android.os.RemoteException a0 = null;
                    if (a == null) {
                        break label1;
                    }
                    label0: {
                        try {
                            a.setMode(i, b);
                        } catch(android.os.RemoteException a1) {
                            a0 = a1;
                            break label0;
                        }
                        break label1;
                    }
                    sLogger.e(new StringBuilder().append("RemoteException :").append(a0).toString());
                }
            }
            default: {
                return;
            }
        }
    }
    
    public void setRecordingPidListener(com.navdy.obd.IPidListener a) {
        this.recordingPidListener = a;
    }
    
    public void setSupportedPidSet(com.navdy.obd.PidSet a) {
        if (this.firstScan) {
            this.firstScan = true;
            this.supportedPidSet = a;
        } else if (this.supportedPidSet == null) {
            this.supportedPidSet = a;
        } else {
            this.supportedPidSet.merge(a);
        }
        this.bus.post(new com.navdy.hud.app.obd.ObdManager$ObdSupportedPidsChangedEvent());
    }
    
    public void sleep(boolean b) {
        com.navdy.obd.ICarService a = this.carServiceConnector.getCarApi();
        label0: {
            android.os.RemoteException a0 = null;
            if (a == null) {
                break label0;
            }
            try {
                a.sleep(b);
                break label0;
            } catch(android.os.RemoteException a1) {
                a0 = a1;
            }
            sLogger.e(new StringBuilder().append("Error during call to sleep Remote Exception ").append(a0.getCause()).toString());
        }
    }
    
    public void updateFirmware(com.navdy.hud.app.obd.ObdManager$Firmware a) {
        sLogger.d(new StringBuilder().append("Updating the firmware of the OBD chip , Version : ").append((a != com.navdy.hud.app.obd.ObdManager$Firmware.OLD_320) ? "4.2.1" : "3.2.0").toString());
        com.navdy.obd.ICarService a0 = this.carServiceConnector.getCarApi();
        java.io.InputStream a1 = com.navdy.hud.app.HudApplication.getAppContext().getResources().openRawResource(a.resource);
        String s = new StringBuilder().append(android.os.Environment.getExternalStorageDirectory().getAbsolutePath()).append(java.io.File.separator).append("update.bin").toString();
        java.io.File a2 = new java.io.File(s);
        if (a2.exists()) {
            com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), a2.getAbsolutePath());
        }
        label1: {
            label0: {
                java.io.IOException a3 = null;
                try {
                    com.navdy.service.library.util.IOUtils.copyFile(a2.getAbsolutePath(), a1);
                    break label0;
                } catch(java.io.IOException a4) {
                    a3 = a4;
                }
                sLogger.e("Error writing to the file ", (Throwable)a3);
                break label1;
            }
            try {
                a0.updateFirmware("4.2.1", s);
            } catch(android.os.RemoteException a5) {
                sLogger.e("Remote Exception while updating firmware ", (Throwable)a5);
            }
        }
    }
    
    public void updateListener() {
        com.navdy.obd.ICarService a = this.carServiceConnector.getCarApi();
        label0: {
            Throwable a0 = null;
            if (a == null) {
                break label0;
            }
            try {
                a.setCANBusMonitoringListener(this.canBusMonitoringListener);
            } catch(android.os.RemoteException a1) {
                sLogger.e("RemoteException ", (Throwable)a1);
            }
            try {
                this.firstScan = true;
                int i = a.getConnectionState();
                switch(i) {
                    case 5: {
                        sLogger.d("ObdService is in Sleeping state, waking it up");
                        a.wakeup();
                        break;
                    }
                    case 2: case 4: {
                        this.obdChipFirmwareVersion = a.getObdChipFirmwareVersion();
                        sLogger.d(new StringBuilder().append("Obd chip firmware version ").append(this.obdChipFirmwareVersion).append(", Connection State :").append(i).toString());
                        break;
                    }
                }
                this.updateConnectionState(a.getConnectionState());
                this.monitorBatteryVoltage();
                sLogger.i(new StringBuilder().append("adding listener for ").append(this.schedule).toString());
                if (this.schedule != null) {
                    a.updateScan(this.schedule, this.pidListener);
                }
                if (this.obdCanBusRecordingPolicy.isCanBusMonitoringNeeded()) {
                    sLogger.d("Start CAN bus monitoring");
                    a.startCanBusMonitoring();
                    break label0;
                } else {
                    sLogger.d("stop CAN bus monitoring");
                    a.stopCanBusMonitoring();
                    break label0;
                }
            } catch(Throwable a2) {
                a0 = a2;
            }
            sLogger.e(a0);
        }
    }
    
    public void wakeup() {
        com.navdy.obd.ICarService a = this.carServiceConnector.getCarApi();
        label0: {
            android.os.RemoteException a0 = null;
            if (a == null) {
                break label0;
            }
            try {
                a.wakeup();
                break label0;
            } catch(android.os.RemoteException a1) {
                a0 = a1;
            }
            sLogger.e(new StringBuilder().append("Error during call to sleep Remote Exception ").append(a0.getCause()).toString());
        }
    }
}
