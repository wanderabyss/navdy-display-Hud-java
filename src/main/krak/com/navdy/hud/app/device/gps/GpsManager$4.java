package com.navdy.hud.app.device.gps;

class GpsManager$4 implements android.location.GpsStatus$Listener {
    final com.navdy.hud.app.device.gps.GpsManager this$0;
    
    GpsManager$4(com.navdy.hud.app.device.gps.GpsManager a) {
        super();
        this.this$0 = a;
    }
    
    public void onGpsStatusChanged(int i) {
        switch(i) {
            case 3: {
                com.navdy.hud.app.device.gps.GpsManager.access$000().i("[Gps-stat] gps got first fix");
                break;
            }
            case 2: {
                com.navdy.hud.app.device.gps.GpsManager.access$000().w("[Gps-stat] gps stopped searching");
                break;
            }
            case 1: {
                com.navdy.hud.app.device.gps.GpsManager.access$000().i("[Gps-stat] gps searching...");
                break;
            }
        }
    }
}
