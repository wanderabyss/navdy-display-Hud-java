package com.navdy.hud.app.device.dial;

class DialManager$13 extends android.bluetooth.BluetoothGattCallback {
    final com.navdy.hud.app.device.dial.DialManager this$0;
    
    DialManager$13(com.navdy.hud.app.device.dial.DialManager a) {
        super();
        this.this$0 = a;
    }
    
    public void onCharacteristicChanged(android.bluetooth.BluetoothGatt a, android.bluetooth.BluetoothGattCharacteristic a0) {
        com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onCharacteristicChanged");
        this.print(a0);
    }
    
    public void onCharacteristicRead(android.bluetooth.BluetoothGatt a, android.bluetooth.BluetoothGattCharacteristic a0, int i) {
        com.navdy.hud.app.device.dial.DialManager.access$3100(this.this$0).onCharacteristicRead(a0, i);
        if (i != 0) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("[Dial]onCharacteristicRead fail:").append(i).toString());
        } else {
            this.print(a0);
        }
        com.navdy.hud.app.device.dial.DialManager.sLogger.d("onCharacteristicRead finished");
        if (com.navdy.hud.app.device.dial.DialManager.access$2700(this.this$0) != null) {
            com.navdy.hud.app.device.dial.DialManager.access$2700(this.this$0).commandFinished();
        }
    }
    
    public void onCharacteristicWrite(android.bluetooth.BluetoothGatt a, android.bluetooth.BluetoothGattCharacteristic a0, int i) {
        com.navdy.hud.app.device.dial.DialManager.access$3100(this.this$0).onCharacteristicWrite(a, a0, i);
        if (a0 == com.navdy.hud.app.device.dial.DialManager.access$2800(this.this$0)) {
            android.bluetooth.BluetoothDevice a1 = a.getDevice();
            this.this$0.forgetDial(a1);
        }
        if (a0 == com.navdy.hud.app.device.dial.DialManager.access$2900(this.this$0)) {
            boolean b = i == 0;
            if (b) {
                com.navdy.hud.app.device.dial.DialManager.access$3200(this.this$0).edit().putLong("last_dial_reboot", System.currentTimeMillis()).apply();
            }
            com.navdy.hud.app.device.dial.DialManagerHelper.sendRebootLocalyticsEvent(com.navdy.hud.app.device.dial.DialManager.access$400(this.this$0), b, a.getDevice(), (String)null);
        }
        if (com.navdy.hud.app.device.dial.DialManager.access$2700(this.this$0) != null) {
            com.navdy.hud.app.device.dial.DialManager.access$2700(this.this$0).commandFinished();
        }
    }
    
    public void onConnectionStateChange(android.bluetooth.BluetoothGatt a, int i, int i0) {
        try {
            com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("[Dial]gatt state change:").append(i).append(" newState=").append(i0).toString());
            if (i0 != 2) {
                if (i0 == 0) {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]gatt disconnected");
                    if (com.navdy.hud.app.device.dial.DialManager.access$2700(this.this$0) != null) {
                        com.navdy.hud.app.device.dial.DialManager.access$2700(this.this$0).release();
                        com.navdy.hud.app.device.dial.DialManager.access$2702(this.this$0, (com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor)null);
                    }
                    com.navdy.hud.app.device.dial.DialManager.access$1800(this.this$0);
                }
            } else if (a == null) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]gatt is null, cannot discover");
            } else {
                if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
                    String s = com.navdy.hud.app.util.os.SystemProperties.get("persist.sys.bt.dial.pwr");
                    Integer a0 = null;
                    if (s != null) {
                        a0 = (Integer)com.navdy.hud.app.device.dial.DialManager.access$2600().get(s);
                    }
                    if (a0 != null) {
                        com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("[Dial] setting BT power to ").append(s).toString());
                        a.requestConnectionPriority(a0.intValue());
                    }
                }
                com.navdy.hud.app.device.dial.DialManager.access$2702(this.this$0, new com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor(a, com.navdy.hud.app.device.dial.DialManager.access$400(this.this$0)));
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]gatt connected, discover");
                a.discoverServices();
            }
        } catch(Throwable a1) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.e("[Dial]", a1);
        }
    }
    
    public void onDescriptorRead(android.bluetooth.BluetoothGatt a, android.bluetooth.BluetoothGattDescriptor a0, int i) {
        com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onDescriptorRead");
    }
    
    public void onDescriptorWrite(android.bluetooth.BluetoothGatt a, android.bluetooth.BluetoothGattDescriptor a0, int i) {
        com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("[Dial]onDescriptorWrite : ").append((i != 0) ? "fail" : "success").toString());
    }
    
    public void onServicesDiscovered(android.bluetooth.BluetoothGatt a, int i) {
        com.navdy.hud.app.device.dial.DialManager.access$2302(this.this$0, (android.bluetooth.BluetoothGattCharacteristic)null);
        com.navdy.hud.app.device.dial.DialManager.access$2202(this.this$0, (android.bluetooth.BluetoothGattCharacteristic)null);
        com.navdy.hud.app.device.dial.DialManager.access$2402(this.this$0, (android.bluetooth.BluetoothGattCharacteristic)null);
        com.navdy.hud.app.device.dial.DialManager.access$2802(this.this$0, (android.bluetooth.BluetoothGattCharacteristic)null);
        com.navdy.hud.app.device.dial.DialManager.access$2902(this.this$0, (android.bluetooth.BluetoothGattCharacteristic)null);
        com.navdy.hud.app.device.dial.DialManager.access$1902(this.this$0, (android.bluetooth.BluetoothGattCharacteristic)null);
        com.navdy.hud.app.device.dial.DialManager.access$2002(this.this$0, (android.bluetooth.BluetoothGattCharacteristic)null);
        com.navdy.hud.app.device.dial.DialManager.access$2102(this.this$0, (android.bluetooth.BluetoothGattCharacteristic)null);
        try {
            if (com.navdy.hud.app.device.dial.DialManager.access$3000(this.this$0)) {
                if (i != 0) {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.e(new StringBuilder().append("[Dial]onServicesDiscovered error:").append(i).toString());
                } else {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.i("[Dial]onServicesDiscovered");
                    com.navdy.hud.app.device.dial.DialManager.access$3100(this.this$0).onServicesDiscovered(a);
                    Object a0 = a.getServices().iterator();
                    while(((java.util.Iterator)a0).hasNext()) {
                        android.bluetooth.BluetoothGattService a1 = (android.bluetooth.BluetoothGattService)((java.util.Iterator)a0).next();
                        java.util.UUID a2 = a1.getUuid();
                        boolean b = com.navdy.hud.app.device.dial.DialConstants.HID_SERVICE_UUID.equals(a2);
                        {
                            if (b) {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.d(new StringBuilder().append("Hid service found ").append(a2).toString());
                                com.navdy.hud.app.device.dial.DialManager.access$2402(this.this$0, a1.getCharacteristic(com.navdy.hud.app.device.dial.DialConstants.HID_HOST_READY_UUID));
                                if (com.navdy.hud.app.device.dial.DialManager.access$2400(this.this$0) != null) {
                                    com.navdy.hud.app.device.dial.DialManager.sLogger.v("Found Hid host ready Characteristic");
                                    com.navdy.hud.app.device.dial.DialManager.access$3200(this.this$0).edit().putBoolean("first_run_dial_video_shown", true).apply();
                                } else {
                                    com.navdy.hud.app.device.dial.DialManager.sLogger.e("Hid host ready Characteristic is null");
                                }
                                com.navdy.hud.app.device.dial.DialManager.access$400(this.this$0).post(com.navdy.hud.app.device.dial.DialManager.access$3300(this.this$0));
                                continue;
                            }
                            if (!com.navdy.hud.app.device.dial.DialConstants.BATTERY_SERVICE_UUID.equals(a2)) {
                                if (com.navdy.hud.app.device.dial.DialConstants.DEVICE_INFO_SERVICE_UUID.equals(a2)) {
                                    com.navdy.hud.app.device.dial.DialManager.sLogger.d(new StringBuilder().append("Device info service found ").append(a2).toString());
                                    com.navdy.hud.app.device.dial.DialManager.access$2202(this.this$0, a1.getCharacteristic(com.navdy.hud.app.device.dial.DialConstants.DEVICE_INFO_FIRMWARE_VERSION_UUID));
                                    if (com.navdy.hud.app.device.dial.DialManager.access$2200(this.this$0) == null) {
                                        com.navdy.hud.app.device.dial.DialManager.sLogger.e("Firmware Version Characteristic is null");
                                    }
                                    com.navdy.hud.app.device.dial.DialManager.access$2302(this.this$0, a1.getCharacteristic(com.navdy.hud.app.device.dial.DialConstants.DEVICE_INFO_HARDWARE_VERSION_UUID));
                                    if (com.navdy.hud.app.device.dial.DialManager.access$2300(this.this$0) == null) {
                                        com.navdy.hud.app.device.dial.DialManager.sLogger.e("Hardware Version Characteristic is null");
                                    }
                                    com.navdy.hud.app.device.dial.DialManager.access$400(this.this$0).postDelayed(com.navdy.hud.app.device.dial.DialManager.access$3600(this.this$0), 5000L);
                                    continue;
                                } else if (com.navdy.hud.app.device.dial.DialConstants.OTA_SERVICE_UUID.equals(a2)) {
                                    com.navdy.hud.app.device.dial.DialManager.access$2802(this.this$0, a1.getCharacteristic(com.navdy.hud.app.device.dial.DialConstants.DIAL_FORGET_KEYS_UUID));
                                    if (com.navdy.hud.app.device.dial.DialManager.access$2800(this.this$0) == null) {
                                        com.navdy.hud.app.device.dial.DialManager.sLogger.d("Dial key forget is null");
                                    }
                                    com.navdy.hud.app.device.dial.DialManager.access$2902(this.this$0, a1.getCharacteristic(com.navdy.hud.app.device.dial.DialConstants.DIAL_REBOOT_UUID));
                                    if (com.navdy.hud.app.device.dial.DialManager.access$2900(this.this$0) != null) {
                                        continue;
                                    }
                                    com.navdy.hud.app.device.dial.DialManager.sLogger.d("Dial reboot is null");
                                    continue;
                                } else {
                                    com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("[Dial]Ignoring service:").append(a2).toString());
                                    continue;
                                }
                            }
                            String s = a1.getUuid().toString();
                            com.navdy.hud.app.device.dial.DialManager.sLogger.i(new StringBuilder().append("[Dial]Service: ").append(s).toString());
                            java.util.List a3 = a1.getCharacteristics();
                            if (a3 != null) {
                                Object a4 = a3.iterator();
                                while(((java.util.Iterator)a4).hasNext()) {
                                    android.bluetooth.BluetoothGattCharacteristic a5 = (android.bluetooth.BluetoothGattCharacteristic)((java.util.Iterator)a4).next();
                                    if (com.navdy.hud.app.device.dial.DialConstants.BATTERY_LEVEL_UUID.equals(a5.getUuid())) {
                                        com.navdy.hud.app.device.dial.DialManager.access$1902(this.this$0, a5);
                                    } else if (com.navdy.hud.app.device.dial.DialConstants.RAW_BATTERY_LEVEL_UUID.equals(a5.getUuid())) {
                                        com.navdy.hud.app.device.dial.DialManager.access$2002(this.this$0, a5);
                                    } else if (com.navdy.hud.app.device.dial.DialConstants.SYSTEM_TEMPERATURE_UUID.equals(a5.getUuid())) {
                                        com.navdy.hud.app.device.dial.DialManager.access$2102(this.this$0, a5);
                                    }
                                }
                                if (com.navdy.hud.app.device.dial.DialManager.access$1900(this.this$0) == null) {
                                    com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial] battery characteristic not found");
                                    continue;
                                } else {
                                    com.navdy.hud.app.device.dial.DialManager.access$400(this.this$0).postDelayed(com.navdy.hud.app.device.dial.DialManager.access$3400(this.this$0), 5000L);
                                    com.navdy.hud.app.device.dial.DialManager.access$400(this.this$0).postDelayed(com.navdy.hud.app.device.dial.DialManager.access$3500(this.this$0), 10000L);
                                    continue;
                                }
                            }
                            com.navdy.hud.app.device.dial.DialManager.sLogger.i("[Dial]No characteristic");
                            break;
                        }
                    }
                }
            } else {
                com.navdy.hud.app.device.dial.DialManager.sLogger.w("gatt is not on but onServicesDiscovered called");
            }
        } catch(Throwable a6) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.e("[Dial]", a6);
        }
    }
    
    void print(android.bluetooth.BluetoothGattCharacteristic a) {
        if (com.navdy.hud.app.device.dial.DialConstants.HID_HOST_READY_UUID.equals(a.getUuid())) {
            byte[] a0 = a.getValue();
            com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("[Dial]onCharacteristicRead hid host ready, length ").append(a0.length).toString());
        } else if (com.navdy.hud.app.device.dial.DialConstants.BATTERY_LEVEL_UUID.equals(a.getUuid())) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onCharacteristicRead battery");
            Integer a1 = a.getIntValue(33, 0);
            if (a1 == null) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]battery level no data");
            } else {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("[Dial]battery level is [").append(a1).append("] %").toString());
                com.navdy.hud.app.device.dial.DialManager.access$3700(this.this$0, a1.intValue());
            }
        } else if (com.navdy.hud.app.device.dial.DialConstants.RAW_BATTERY_LEVEL_UUID.equals(a.getUuid())) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onCharacteristicRead rawBattery");
            Integer a2 = a.getIntValue(34, 0);
            if (a2 == null) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]raw battery level no data");
            } else {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("[Dial]raw battery level is [").append(a2).append("]").toString());
                com.navdy.hud.app.device.dial.DialManager.access$3800(this.this$0, a2);
            }
        } else if (com.navdy.hud.app.device.dial.DialConstants.SYSTEM_TEMPERATURE_UUID.equals(a.getUuid())) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onCharacteristicRead temperature");
            Integer a3 = a.getIntValue(34, 0);
            if (a3 == null) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]system temperature no data");
            } else {
                com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("[Dial]system temperature is [").append(a3).append("]").toString());
                com.navdy.hud.app.device.dial.DialManager.access$3900(this.this$0, a3);
            }
        } else if (com.navdy.hud.app.device.dial.DialConstants.DEVICE_INFO_HARDWARE_VERSION_UUID.equals(a.getUuid())) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onCharacteristicRead Hardware version");
            String s = a.getStringValue(0);
            if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                com.navdy.hud.app.device.dial.DialManager.access$4002(this.this$0, s.trim());
                com.navdy.hud.app.device.dial.DialManager.sLogger.d(new StringBuilder().append("Device Info hardware version received: ").append(com.navdy.hud.app.device.dial.DialManager.access$4000(this.this$0)).toString());
            }
        } else if (com.navdy.hud.app.device.dial.DialConstants.DEVICE_INFO_FIRMWARE_VERSION_UUID.equals(a.getUuid())) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onCharacteristicRead Firmware version");
            String s0 = a.getStringValue(0);
            if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                com.navdy.hud.app.device.dial.DialManager.access$4102(this.this$0, s0.trim());
                com.navdy.hud.app.device.dial.DialManager.sLogger.d(new StringBuilder().append("Device Info firmware version received: ").append(com.navdy.hud.app.device.dial.DialManager.access$4100(this.this$0)).toString());
            }
        } else {
            com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("[Dial]onCharacteristicRead:").append(a.getUuid()).toString());
        }
    }
}
