package com.navdy.hud.app.device.dial;

final class DialManagerHelper$10 implements Runnable {
    final android.bluetooth.BluetoothDevice val$device;
    final String val$result;
    final boolean val$successful;
    
    DialManagerHelper$10(String s, boolean b, android.bluetooth.BluetoothDevice a) {
        super();
        this.val$result = s;
        this.val$successful = b;
        this.val$device = a;
    }
    
    public void run() {
        try {
            java.util.HashMap a = new java.util.HashMap();
            ((java.util.Map)a).put("Type", "Reboot");
            ((java.util.Map)a).put("Result", this.val$result);
            ((java.util.Map)a).put("Success", ((this.val$successful) ? "true" : "false"));
            android.bluetooth.BluetoothDevice a0 = this.val$device;
            String s = null;
            if (a0 != null) {
                s = this.val$device.getAddress();
            }
            ((java.util.Map)a).put("Dial_Address", s);
            com.navdy.hud.app.device.dial.DialManager a1 = com.navdy.hud.app.device.dial.DialManager.getInstance();
            String s0 = String.valueOf(a1.getLastKnownBatteryLevel());
            String s1 = String.valueOf(a1.getLastKnownRawBatteryLevel());
            String s2 = String.valueOf(a1.getLastKnownSystemTemperature());
            String s3 = a1.getFirmWareVersion();
            String s4 = a1.getHardwareVersion();
            int i = a1.getDialFirmwareUpdater().getVersions().dial.incrementalVersion;
            ((java.util.Map)a).put("Battery_Level", s0);
            ((java.util.Map)a).put("Raw_Battery_Level", s1);
            ((java.util.Map)a).put("System_Temperature", s2);
            ((java.util.Map)a).put("FW_Version", s3);
            ((java.util.Map)a).put("HW_Version", s4);
            ((java.util.Map)a).put("Incremental_Version", Integer.toString(i));
            com.navdy.hud.app.device.dial.DialManagerHelper.access$000().v(new StringBuilder().append("sending foundDial[").append(this.val$successful).append("] battery[").append(s0).append("] rawBattery[").append(s1).append("] temperature[").append(s2).append("]").append(" fw[").append(s3).append("] hw[").append(s4).append("] incremental[").append(i).append("] type[").append("Reboot").append("] result [").append(this.val$result).append("] address [").append(s).append("]").toString());
            com.localytics.android.Localytics.tagEvent("Dial_Pairing", (java.util.Map)a);
        } catch(Throwable a2) {
            com.navdy.hud.app.device.dial.DialManagerHelper.access$000().e(a2);
        }
    }
}
