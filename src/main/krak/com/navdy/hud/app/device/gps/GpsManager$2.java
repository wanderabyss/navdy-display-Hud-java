package com.navdy.hud.app.device.gps;

class GpsManager$2 implements android.location.LocationListener {
    final com.navdy.hud.app.device.gps.GpsManager this$0;
    
    GpsManager$2(com.navdy.hud.app.device.gps.GpsManager a) {
        super();
        this.this$0 = a;
    }
    
    public void onLocationChanged(android.location.Location a) {
        label1: {
            Throwable a0 = null;
            label0: {
                long j = 0L;
                float f = 0.0f;
                int i = 0;
                com.navdy.service.library.log.Logger a1 = null;
                StringBuilder a2 = null;
                int i0 = 0;
                try {
                    if (com.navdy.hud.app.device.gps.GpsManager.access$000().isLoggable(2)) {
                        com.navdy.hud.app.device.gps.GpsManager.access$000().d(new StringBuilder().append("[Gps-loc] ").append((a.isFromMockProvider()) ? "[Mock] " : "").append(a).toString());
                    }
                    if (!com.navdy.hud.app.device.gps.GpsManager.access$200(this.this$0)) {
                        break label1;
                    }
                    if (a.isFromMockProvider()) {
                        break label1;
                    }
                    if (com.navdy.hud.app.device.gps.GpsManager.access$300(this.this$0) == com.navdy.hud.app.device.gps.GpsManager$LocationSource.UBLOX) {
                        break label1;
                    }
                    com.navdy.hud.app.device.gps.GpsManager$LocationSource a3 = com.navdy.hud.app.device.gps.GpsManager.access$300(this.this$0);
                    com.navdy.hud.app.device.gps.GpsManager.access$302(this.this$0, com.navdy.hud.app.device.gps.GpsManager$LocationSource.UBLOX);
                    com.navdy.hud.app.device.gps.GpsManager.access$500(this.this$0).removeCallbacks(com.navdy.hud.app.device.gps.GpsManager.access$400(this.this$0));
                    com.navdy.hud.app.device.gps.GpsManager.access$500(this.this$0).removeCallbacks(com.navdy.hud.app.device.gps.GpsManager.access$600(this.this$0));
                    com.navdy.hud.app.device.gps.GpsManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.device.gps.GpsManager.access$600(this.this$0), 1000L);
                    com.navdy.hud.app.device.gps.GpsManager.access$700(this.this$0);
                    int i1 = (int)(android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.device.gps.GpsManager.access$800(this.this$0)) / 1000;
                    j = android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.device.gps.GpsManager.access$900(this.this$0);
                    f = (com.navdy.hud.app.device.gps.GpsManager.access$1000(this.this$0) == null) ? -1f : (j >= 3000L) ? -1f : com.navdy.hud.app.device.gps.GpsManager.access$1000(this.this$0).accuracy.floatValue();
                    i = (int)((com.navdy.hud.app.device.gps.GpsManager.access$1100(this.this$0) == null) ? -1f : com.navdy.hud.app.device.gps.GpsManager.access$1100(this.this$0).getAccuracy());
                    com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.recordGpsAcquireLocation(String.valueOf(i1), String.valueOf(i));
                    com.navdy.hud.app.device.gps.GpsManager.access$802(this.this$0, android.os.SystemClock.elapsedRealtime());
                    com.navdy.hud.app.device.gps.GpsManager a4 = this.this$0;
                    StringBuilder a5 = new StringBuilder().append("Phone =");
                    Object a6 = (f != -1f) ? Float.valueOf(f) : "n/a";
                    com.navdy.hud.app.device.gps.GpsManager.access$1200(a4, "Switched to Ublox Gps", a5.append(a6).append(" ublox =").append(i).toString(), false, true);
                    a1 = com.navdy.hud.app.device.gps.GpsManager.access$000();
                    a2 = new StringBuilder().append("[Gps-loc] [LocationProvider] switched from [").append(a3).append("] to [").append(com.navdy.hud.app.device.gps.GpsManager$LocationSource.UBLOX).append("] ").append("Phone =");
                    i0 = (f > -1f) ? 1 : (f == -1f) ? 0 : -1;
                } catch(Throwable a7) {
                    a0 = a7;
                    break label0;
                }
                Object a8 = (i0 != 0) ? Float.valueOf(f) : "n/a";
                try {
                    a1.v(a2.append(a8).append(" ublox =").append(i).append(" time=").append(j).toString());
                } catch(Throwable a9) {
                    a0 = a9;
                    break label0;
                }
                break label1;
            }
            com.navdy.hud.app.device.gps.GpsManager.access$000().e(a0);
        }
    }
    
    public void onProviderDisabled(String s) {
        com.navdy.hud.app.device.gps.GpsManager.access$000().i(new StringBuilder().append("[Gps-stat] ").append(s).append("[disabled]").toString());
    }
    
    public void onProviderEnabled(String s) {
        com.navdy.hud.app.device.gps.GpsManager.access$000().i(new StringBuilder().append("[Gps-stat] ").append(s).append("[enabled]").toString());
    }
    
    public void onStatusChanged(String s, int i, android.os.Bundle a) {
        if (com.navdy.hud.app.device.PowerManager.isAwake()) {
            com.navdy.hud.app.device.gps.GpsManager.access$000().i(new StringBuilder().append("[Gps-stat] ").append(s).append(",").append(i).toString());
        }
    }
}
