package com.navdy.hud.app.device.dial;

class DialManager$CharacteristicCommandProcessor$WriteCommand extends com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor$Command {
    byte[] data;
    
    public DialManager$CharacteristicCommandProcessor$WriteCommand(android.bluetooth.BluetoothGattCharacteristic a, byte[] a0) {
        super(a);
        this.data = a0;
    }
    
    public void process(android.bluetooth.BluetoothGatt a) {
        this.characteristic.setValue(this.data);
        if (a.writeCharacteristic(this.characteristic)) {
            if (!this.characteristic.getUuid().equals(com.navdy.hud.app.device.dial.DialConstants.OTA_DATA_CHARACTERISTIC_UUID)) {
                com.navdy.hud.app.device.dial.DialManager.sLogger.d(new StringBuilder().append("Processing GATT write succeeded ").append(this.characteristic.getUuid().toString()).toString());
            }
        } else {
            com.navdy.hud.app.device.dial.DialManager.sLogger.d(new StringBuilder().append("Processing GATT write failed ").append(this.characteristic.getUuid().toString()).toString());
        }
    }
}
