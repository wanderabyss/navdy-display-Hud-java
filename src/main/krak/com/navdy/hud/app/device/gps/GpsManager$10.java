package com.navdy.hud.app.device.gps;

class GpsManager$10 implements Runnable {
    final com.navdy.hud.app.device.gps.GpsManager this$0;
    
    GpsManager$10(com.navdy.hud.app.device.gps.GpsManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        try {
            android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
            android.os.UserHandle a0 = android.os.Process.myUserHandle();
            int i = com.navdy.hud.app.device.gps.GpsManager.access$2800(this.this$0).size();
            if (i > 0) {
                int i0 = 0;
                while(i0 < i) {
                    android.content.Intent a1 = (android.content.Intent)com.navdy.hud.app.device.gps.GpsManager.access$2800(this.this$0).get(i0);
                    a.sendBroadcastAsUser(a1, a0);
                    com.navdy.hud.app.device.gps.GpsManager.access$000().v(new StringBuilder().append("sent-queue gps event user:").append(a1.getAction()).toString());
                    i0 = i0 + 1;
                }
                com.navdy.hud.app.device.gps.GpsManager.access$2800(this.this$0).clear();
            }
        } catch(Throwable a2) {
            com.navdy.hud.app.device.gps.GpsManager.access$000().e(a2);
        }
    }
}
