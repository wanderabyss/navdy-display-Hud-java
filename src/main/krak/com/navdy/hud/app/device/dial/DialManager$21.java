package com.navdy.hud.app.device.dial;

class DialManager$21 implements Runnable {
    final com.navdy.hud.app.device.dial.DialManager this$0;
    
    DialManager$21(com.navdy.hud.app.device.dial.DialManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        java.net.DatagramSocket a = null;
        Throwable a0 = null;
        label1: {
            label0: {
                try {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.v("DialEventsListener thread enter");
                    a = new java.net.DatagramSocket(23654, java.net.InetAddress.getByName("127.0.0.1"));
                    break label0;
                } catch(Throwable a1) {
                    a0 = a1;
                }
                a = null;
                break label1;
            }
            try {
                byte[] a2 = new byte[56];
                java.net.DatagramPacket a3 = new java.net.DatagramPacket(a2, a2.length);
                while(true) {
                    a.receive(a3);
                    String s = new String(a2, 0, a3.getLength());
                    com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("[DialEventsListener] [").append(s).append("]").toString());
                    try {
                        int i = s.indexOf(",");
                        if (i >= 0) {
                            String s0 = s.substring(0, i);
                            String s1 = s.substring(i + 1).toUpperCase();
                            com.navdy.hud.app.device.dial.DialManager.access$800(this.this$0);
                            if (android.bluetooth.BluetoothAdapter.checkBluetoothAddress(s1)) {
                                android.bluetooth.BluetoothDevice a4 = com.navdy.hud.app.device.dial.DialManager.access$800(this.this$0).getRemoteDevice(s1);
                                com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("[DialEventsListener] event for [").append(a4).append("]").toString());
                                if (com.navdy.hud.app.device.dial.DialManager.access$4600(this.this$0, a4)) {
                                    int i0 = 0;
                                    switch(s0.hashCode()) {
                                        case 1669334218: {
                                            i0 = (s0.equals("CONNECT")) ? 0 : -1;
                                            break;
                                        }
                                        case 1015497884: {
                                            i0 = (s0.equals("DISCONNECT")) ? 1 : -1;
                                            break;
                                        }
                                        case -1324212103: {
                                            i0 = (s0.equals("ENCRYPTION_FAILED")) ? 2 : -1;
                                            break;
                                        }
                                        case -1425556143: {
                                            i0 = (s0.equals("INPUTDESCRIPTOR_WRITTEN")) ? 3 : -1;
                                            break;
                                        }
                                        default: {
                                            i0 = -1;
                                        }
                                    }
                                    switch(i0) {
                                        case 3: {
                                            com.navdy.hud.app.device.dial.DialManager.access$5000(this.this$0, a4);
                                            break;
                                        }
                                        case 2: {
                                            com.navdy.hud.app.device.dial.DialManager.access$4900(this.this$0, a4);
                                            break;
                                        }
                                        case 1: {
                                            com.navdy.hud.app.device.dial.DialManager.access$4800(this.this$0, a4);
                                            break;
                                        }
                                        case 0: {
                                            com.navdy.hud.app.device.dial.DialManager.access$4700(this.this$0, a4);
                                            break;
                                        }
                                    }
                                } else {
                                    com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("[DialEventsListener] ").append(s1).append(" not present in bond list, ignore").toString());
                                }
                            } else {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.e("[DialEventsListener] not a valid bluetooth address");
                            }
                        } else {
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("[DialEventsListener] invalid data");
                        }
                    } catch(Throwable a5) {
                        com.navdy.hud.app.device.dial.DialManager.sLogger.e("[DialEventsListener]", a5);
                    }
                }
            } catch(Throwable a6) {
                a0 = a6;
            }
        }
        com.navdy.hud.app.device.dial.DialManager.sLogger.e(a0);
        if (a != null) {
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
        }
        com.navdy.hud.app.device.dial.DialManager.sLogger.v("DialEventsListener thread exit");
    }
}
