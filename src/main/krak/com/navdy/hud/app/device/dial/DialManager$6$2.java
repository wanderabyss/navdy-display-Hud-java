package com.navdy.hud.app.device.dial;

class DialManager$6$2 implements Runnable {
    final com.navdy.hud.app.device.dial.DialManager$6 this$1;
    final android.bluetooth.BluetoothDevice val$device;
    
    DialManager$6$2(com.navdy.hud.app.device.dial.DialManager$6 a, android.bluetooth.BluetoothDevice a0) {
        super();
        this.this$1 = a;
        this.val$device = a0;
    }
    
    public void run() {
        com.navdy.hud.app.device.dial.DialManager.sLogger.v("btBondRecvr removed runnable");
        com.navdy.hud.app.device.dial.DialManager.access$400(this.this$1.this$0).removeCallbacks(com.navdy.hud.app.device.dial.DialManager.access$600(this.this$1.this$0));
        this.val$device.createBond();
        com.navdy.hud.app.device.dial.DialManager.access$400(this.this$1.this$0).postDelayed(com.navdy.hud.app.device.dial.DialManager.access$600(this.this$1.this$0), 30000L);
        com.navdy.hud.app.device.dial.DialManager.sLogger.v("btBondRecvr posted runnable");
    }
}
