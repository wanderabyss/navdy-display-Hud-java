package com.navdy.hud.app.framework.calendar;

final public class CalendarManager$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider {
    private dagger.internal.Binding bus;
    
    public CalendarManager$$InjectAdapter() {
        super("com.navdy.hud.app.framework.calendar.CalendarManager", "members/com.navdy.hud.app.framework.calendar.CalendarManager", false, com.navdy.hud.app.framework.calendar.CalendarManager.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.framework.calendar.CalendarManager.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.framework.calendar.CalendarManager get() {
        return new com.navdy.hud.app.framework.calendar.CalendarManager((com.squareup.otto.Bus)this.bus.get());
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a.add(this.bus);
    }
}
