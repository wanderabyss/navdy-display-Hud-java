package com.navdy.hud.app.framework.contacts;

class PhoneImageDownloader$Info {
    String displayName;
    String fileName;
    com.navdy.service.library.events.photo.PhotoType photoType;
    int priority;
    String sourceIdentifier;
    
    PhoneImageDownloader$Info(String s, com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority a, com.navdy.service.library.events.photo.PhotoType a0, String s0, String s1) {
        this.fileName = s;
        this.priority = a.getValue();
        this.photoType = a0;
        this.sourceIdentifier = s0;
        this.displayName = s1;
    }
}
