package com.navdy.hud.app.framework.toast;

public class ToastManager$ToastParams {
    com.navdy.hud.app.framework.toast.IToastCallback cb;
    android.os.Bundle data;
    String id;
    boolean lock;
    boolean makeCurrent;
    boolean removeOnDisable;
    
    public ToastManager$ToastParams(String s, android.os.Bundle a, com.navdy.hud.app.framework.toast.IToastCallback a0, boolean b, boolean b0) {
        this(s, a, a0, b, b0, false);
    }
    
    public ToastManager$ToastParams(String s, android.os.Bundle a, com.navdy.hud.app.framework.toast.IToastCallback a0, boolean b, boolean b0, boolean b1) {
        this.id = s;
        this.data = a;
        this.cb = a0;
        this.removeOnDisable = b;
        this.makeCurrent = b0;
        this.lock = b1;
    }
}
