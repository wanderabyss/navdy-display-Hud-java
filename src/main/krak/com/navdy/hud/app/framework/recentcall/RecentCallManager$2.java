package com.navdy.hud.app.framework.recentcall;

class RecentCallManager$2 implements Runnable {
    final com.navdy.hud.app.framework.recentcall.RecentCallManager this$0;
    final com.navdy.service.library.events.contacts.ContactResponse val$event;
    
    RecentCallManager$2(com.navdy.hud.app.framework.recentcall.RecentCallManager a, com.navdy.service.library.events.contacts.ContactResponse a0) {
        super();
        this.this$0 = a;
        this.val$event = a0;
    }
    
    public void run() {
        label0: try {
            java.util.HashMap a = null;
            Throwable a0 = null;
            boolean b = android.text.TextUtils.isEmpty((CharSequence)this.val$event.identifier);
            label1: {
                Throwable a1 = null;
                label12: {
                    java.util.HashMap a2 = null;
                    label2: if (b) {
                        com.navdy.hud.app.framework.recentcall.RecentCallManager.access$100().w("invalid contact repsonse");
                        break label0;
                    } else {
                        java.util.ArrayList a3 = null;
                        java.util.ArrayList a4 = null;                        label11: synchronized(com.navdy.hud.app.framework.recentcall.RecentCallManager.access$200(this.this$0)) {
                            com.navdy.hud.app.framework.recentcall.RecentCall a5 = (com.navdy.hud.app.framework.recentcall.RecentCall)com.navdy.hud.app.framework.recentcall.RecentCallManager.access$200(this.this$0).remove(this.val$event.identifier);
                            label13: {
                                if (a5 != null) {
                                    break label13;
                                }
                                com.navdy.hud.app.framework.recentcall.RecentCallManager.access$100().i(new StringBuilder().append("identifer not found[").append(this.val$event.identifier).append("]").toString());
                                /*monexit(a2)*/;
                                break label0;
                            }
                            com.navdy.service.library.events.RequestStatus a6 = this.val$event.status;
                            com.navdy.service.library.events.RequestStatus a7 = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
                            label8: {
                                label3: {
                                    label10: {
                                        label9: {
                                            if (a6 == a7) {
                                                break label9;
                                            }
                                            com.navdy.hud.app.framework.recentcall.RecentCallManager.access$100().i(new StringBuilder().append("request failed for [").append(this.val$event.identifier).append("] status[").append(this.val$event.status).append("]").toString());
                                            a3 = null;
                                            a4 = null;
                                            break label10;
                                        }
                                        if (this.val$event.contacts == null) {
                                            break label8;
                                        }
                                        if (this.val$event.contacts.size() == 0) {
                                            break label8;
                                        }
                                        com.navdy.hud.app.framework.recentcall.RecentCallManager.access$100().v(new StringBuilder().append("contacts returned [").append(this.val$event.contacts.size()).append("]").toString());
                                        java.util.Iterator a8 = this.val$event.contacts.iterator();
                                        a3 = null;
                                        a4 = null;
                                        Object a9 = a8;
                                        while(true) {
                                            {
                                                {
                                                    com.navdy.service.library.events.contacts.Contact a10 = null;
                                                    com.navdy.hud.app.framework.recentcall.RecentCall a11 = null;
                                                    label4: {
                                                        label7: {
                                                            {
                                                                try {
                                                                    if (!((java.util.Iterator)a9).hasNext()) {
                                                                        break;
                                                                    }
                                                                    a10 = (com.navdy.service.library.events.contacts.Contact)((java.util.Iterator)a9).next();
                                                                    boolean b0 = android.text.TextUtils.isEmpty((CharSequence)a10.number);
                                                                    label5: {
                                                                        label6: {
                                                                            if (b0) {
                                                                                break label6;
                                                                            }
                                                                            if (com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(a10.number)) {
                                                                                break label5;
                                                                            }
                                                                        }
                                                                        com.navdy.hud.app.framework.recentcall.RecentCallManager.access$100().i(new StringBuilder().append("no number for  [").append(this.val$event.identifier).append("] status[").append(this.val$event.status).append("]").toString());
                                                                        continue;
                                                                    }
                                                                    a11 = new com.navdy.hud.app.framework.recentcall.RecentCall(a5);
                                                                    if (com.navdy.hud.app.framework.contacts.ContactUtil.isDisplayNameValid(a10.name, a10.number, com.navdy.hud.app.framework.contacts.ContactUtil.getPhoneNumber(a10.number))) {
                                                                        a11.name = a10.name;
                                                                    } else {
                                                                        a11.name = null;
                                                                    }
                                                                    a11.number = a10.number;
                                                                    a11.numberType = com.navdy.hud.app.framework.contacts.ContactUtil.getNumberType(a10.numberType);
                                                                    a11.validateArguments();
                                                                    if (a3 != null) {
                                                                        break label4;
                                                                    }
                                                                    a3 = new java.util.ArrayList();
                                                                    break label7;
                                                                } catch(Throwable a12) {
                                                                    a1 = a12;
                                                                }
                                                                break label3;
                                                            }
                                                        }
                                                        try {
                                                            a4 = new java.util.ArrayList();
                                                        } catch(Throwable a13) {
                                                            a1 = a13;
                                                            break label3;
                                                        }
                                                    }
                                                    ((java.util.List)a3).add(a11);
                                                    ((java.util.List)a4).add(a10);
                                                }
                                                continue;
                                            }
                                        }
                                    }
                                    /*monexit(a2)*/;
                                    break label11;
                                }
                                /*monexit(a2)*/;
                                break label12;
                            }
                            com.navdy.hud.app.framework.recentcall.RecentCallManager.access$100().i(new StringBuilder().append("no contact returned for [").append(this.val$event.identifier).append("] status[").append(this.val$event.status).append("]").toString());
                            /*monexit(a2)*/;
                            break label0;
                        }
                        if (a3 == null) {
                            break label0;
                        }
                        com.navdy.hud.app.framework.recentcall.RecentCallManager.access$100().v(new StringBuilder().append("contact found [").append(this.val$event.identifier).append("] downloading photo").toString());
                        synchronized(com.navdy.hud.app.framework.recentcall.RecentCallManager.access$300(this.this$0)) {
                            com.navdy.hud.app.framework.recentcall.RecentCallManager.access$300(this.this$0).put(this.val$event.identifier, a4);
                            /*monexit(a)*/;
                        }
                        com.navdy.hud.app.framework.recentcall.RecentCallManager.access$400(this.this$0).post(new com.navdy.hud.app.framework.recentcall.RecentCallManager$ContactFound(this.val$event.identifier, (java.util.List)a4));
                        com.navdy.hud.app.framework.contacts.PhoneImageDownloader a16 = com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance();
                        Object a17 = ((java.util.List)a3).iterator();
                        while(((java.util.Iterator)a17).hasNext()) {
                            com.navdy.hud.app.framework.recentcall.RecentCall a18 = (com.navdy.hud.app.framework.recentcall.RecentCall)((java.util.Iterator)a17).next();
                            a16.clearPhotoCheckEntry(a18.number, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT);
                            a16.submitDownload(a18.number, com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority.HIGH, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT, a18.name);
                        }
                        if (((java.util.List)a3).size() != 1) {
                            break label0;
                        }
                        com.navdy.hud.app.framework.recentcall.RecentCallManager.access$500(this.this$0, (java.util.List)a3);
                        break label0;
                    }
                    while(true) {
                        try {
                            /*monexit(a2)*/;
                            break;
                        } catch(IllegalMonitorStateException | NullPointerException a19) {
                            a1 = a19;
                        }
                    }
                }
                throw a1;
            }
            while(true) {
                try {
                    /*monexit(a)*/;
                } catch(IllegalMonitorStateException | NullPointerException a20) {
                    Throwable a21 = a20;
                    a0 = a21;
                    continue;
                }
                throw a0;
            }
        } catch(Throwable a22) {
            com.navdy.hud.app.framework.recentcall.RecentCallManager.access$100().e(a22);
        }
    }
}
