package com.navdy.hud.app.framework.fuel;

class FuelRoutingManager$RouteCacheItem {
    com.here.android.mpa.search.Place gasStation;
    com.navdy.service.library.events.navigation.NavigationRouteResult route;
    
    public FuelRoutingManager$RouteCacheItem(com.navdy.service.library.events.navigation.NavigationRouteResult a, com.here.android.mpa.search.Place a0) {
        this.route = a;
        this.gasStation = a0;
    }
}
