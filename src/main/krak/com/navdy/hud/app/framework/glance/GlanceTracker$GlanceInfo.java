package com.navdy.hud.app.framework.glance;

class GlanceTracker$GlanceInfo {
    java.util.Map data;
    com.navdy.service.library.events.glances.GlanceEvent event;
    long time;
    
    GlanceTracker$GlanceInfo(long j, com.navdy.service.library.events.glances.GlanceEvent a, java.util.Map a0) {
        this.time = j;
        this.event = a;
        this.data = a0;
    }
}
