package com.navdy.hud.app.framework.destinations;


public enum Destination$DestinationType {
    DEFAULT(0),
    FIND_GAS(1);

    private int value;
    Destination$DestinationType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class Destination$DestinationType extends Enum {
//    final private static com.navdy.hud.app.framework.destinations.Destination$DestinationType[] $VALUES;
//    final public static com.navdy.hud.app.framework.destinations.Destination$DestinationType DEFAULT;
//    final public static com.navdy.hud.app.framework.destinations.Destination$DestinationType FIND_GAS;
//    
//    static {
//        DEFAULT = new com.navdy.hud.app.framework.destinations.Destination$DestinationType("DEFAULT", 0);
//        FIND_GAS = new com.navdy.hud.app.framework.destinations.Destination$DestinationType("FIND_GAS", 1);
//        com.navdy.hud.app.framework.destinations.Destination$DestinationType[] a = new com.navdy.hud.app.framework.destinations.Destination$DestinationType[2];
//        a[0] = DEFAULT;
//        a[1] = FIND_GAS;
//        $VALUES = a;
//    }
//    
//    private Destination$DestinationType(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.framework.destinations.Destination$DestinationType valueOf(String s) {
//        return (com.navdy.hud.app.framework.destinations.Destination$DestinationType)Enum.valueOf(com.navdy.hud.app.framework.destinations.Destination$DestinationType.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.destinations.Destination$DestinationType[] values() {
//        return $VALUES.clone();
//    }
//}
//