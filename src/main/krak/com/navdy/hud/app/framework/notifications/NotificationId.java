package com.navdy.hud.app.framework.notifications;

final public class NotificationId {
    final public static String BRIGHTNESS_NOTIFICATION_ID = "navdy#brightness#notif";
    final public static String GLYMPSE_NOTIFICATION_ID = "navdy#glympse#notif";
    final public static String MUSIC_NOTIFICATION_ID = "navdy#music#notif";
    final public static String PHONE_CALL_NOTIFICATION_ID = "navdy#phone#call#notif";
    final public static String PLACE_TYPE_SEARCH_NOTIFICATION_ID = "navdy#place#type#search#notif";
    final public static String ROUTE_CALC_NOTIFICATION_ID = "navdy#route#calc#notif";
    final public static String SMS_NOTIFICATION_ID = "navdy#sms#notif#";
    final public static String TRAFFIC_DELAY_NOTIFICATION_ID = "navdy#traffic#delay#notif";
    final public static String TRAFFIC_EVENT_NOTIFICATION_ID = "navdy#traffic#event#notif";
    final public static String TRAFFIC_JAM_NOTIFICATION_ID = "navdy#traffic#jam#notif";
    final public static String TRAFFIC_REROUTE_NOTIFICATION_ID = "navdy#traffic#reroute#notif";
    final public static String VOICE_ASSIST_NOTIFICATION_ID = "navdy#voiceassist#notif";
    final public static String VOICE_SEARCH_NOTIFICATION_ID = "navdy#voicesearch#notif";
    
    public NotificationId() {
    }
}
