package com.navdy.hud.app.framework.contacts;

public class FavoriteContactsManager {
    final private static com.navdy.hud.app.framework.contacts.FavoriteContactsManager$FavoriteContactsChanged FAVORITE_CONTACTS_CHANGED;
    final private static int MAX_FAV_CONTACTS = 30;
    final private static com.navdy.hud.app.framework.contacts.FavoriteContactsManager sInstance;
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.squareup.otto.Bus bus;
    private volatile java.util.List favContacts;
    private boolean fullSync;
    private boolean isAllowedToReceiveContacts;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.contacts.FavoriteContactsManager.class);
        FAVORITE_CONTACTS_CHANGED = new com.navdy.hud.app.framework.contacts.FavoriteContactsManager$FavoriteContactsChanged((com.navdy.hud.app.framework.contacts.FavoriteContactsManager$1)null);
        sInstance = new com.navdy.hud.app.framework.contacts.FavoriteContactsManager();
    }
    
    private FavoriteContactsManager() {
        this.isAllowedToReceiveContacts = false;
        this.fullSync = true;
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        this.bus.register(this);
    }
    
    static void access$100(com.navdy.hud.app.framework.contacts.FavoriteContactsManager a) {
        a.load();
    }
    
    static boolean access$202(com.navdy.hud.app.framework.contacts.FavoriteContactsManager a, boolean b) {
        a.isAllowedToReceiveContacts = b;
        return b;
    }
    
    static com.navdy.service.library.log.Logger access$300() {
        return sLogger;
    }
    
    static boolean access$400(com.navdy.hud.app.framework.contacts.FavoriteContactsManager a) {
        return a.fullSync;
    }
    
    public static com.navdy.hud.app.framework.contacts.FavoriteContactsManager getInstance() {
        return sInstance;
    }
    
    private void load() {
        label1: {
            Throwable a = null;
            label0: {
                try {
                    String s = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getProfileName();
                    this.favContacts = com.navdy.hud.app.storage.db.helper.FavoriteContactsPersistenceHelper.getFavoriteContacts(s);
                    sLogger.v(new StringBuilder().append("load favcontacts id[").append(s).append("] number of contacts[").append(this.favContacts.size()).append("]").toString());
                } catch(Throwable a0) {
                    a = a0;
                    break label0;
                }
                this.bus.post(FAVORITE_CONTACTS_CHANGED);
                break label1;
            }
            try {
                this.favContacts = null;
                sLogger.e(a);
            } catch(Throwable a1) {
                this.bus.post(FAVORITE_CONTACTS_CHANGED);
                throw a1;
            }
            this.bus.post(FAVORITE_CONTACTS_CHANGED);
        }
    }
    
    public void buildFavoriteContacts() {
        if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.contacts.FavoriteContactsManager$1(this), 1);
        } else {
            this.load();
        }
    }
    
    public void clearFavoriteContacts() {
        this.setFavoriteContacts((java.util.List)null);
    }
    
    public java.util.List getFavoriteContacts() {
        return this.favContacts;
    }
    
    public boolean isAllowedToReceiveContacts() {
        return this.isAllowedToReceiveContacts;
    }
    
    public void onFavoriteContactResponse(com.navdy.service.library.events.contacts.FavoriteContactsResponse a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.contacts.FavoriteContactsManager$2(this, a), 1);
    }
    
    public void setFavoriteContacts(java.util.List a) {
        this.favContacts = a;
        this.bus.post(FAVORITE_CONTACTS_CHANGED);
    }
    
    public void syncFavoriteContacts(boolean b) {
        try {
            this.fullSync = b;
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.contacts.FavoriteContactsRequest(Integer.valueOf(30))));
            sLogger.v("sent fav-contact request");
        } catch(Throwable a) {
            sLogger.e(a);
        }
    }
}
