package com.navdy.hud.app.framework.fuel;

class FuelRoutingManager$2 implements Runnable {
    final com.navdy.hud.app.framework.fuel.FuelRoutingManager this$0;
    final com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnNearestGasStationCallback val$callback;
    
    FuelRoutingManager$2(com.navdy.hud.app.framework.fuel.FuelRoutingManager a, com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnNearestGasStationCallback a0) {
        super();
        this.this$0 = a;
        this.val$callback = a0;
    }
    
    public void run() {
        if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$100(this.this$0)) {
            if (this.val$callback == null) {
                throw new IllegalArgumentException();
            }
            if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$600(this.this$0)) {
                this.val$callback.onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error.INVALID_STATE);
            } else {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$602(this.this$0, true);
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$700(this.this$0, (com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback)new com.navdy.hud.app.framework.fuel.FuelRoutingManager$2$1(this));
            }
        } else {
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().w("Fuel manager not available");
        }
    }
}
