package com.navdy.hud.app.framework.network;

class NetworkBandwidthController$1 implements Runnable {
    final com.navdy.hud.app.framework.network.NetworkBandwidthController this$0;
    
    NetworkBandwidthController$1(com.navdy.hud.app.framework.network.NetworkBandwidthController a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        java.net.DatagramSocket a = null;
        Throwable a0 = null;
        label2: {
            label1: {
                try {
                    com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().v("networkStat thread enter");
                    a = new java.net.DatagramSocket(23655, java.net.InetAddress.getByName("127.0.0.1"));
                    break label1;
                } catch(Throwable a1) {
                    a0 = a1;
                }
                a = null;
                break label2;
            }
            try {
                a.setReceiveBufferSize(16384);
                byte[] a2 = new byte[2048];
                java.net.DatagramPacket a3 = new java.net.DatagramPacket(a2, a2.length);
                label0: while(true) {
                    try {
                        int i = 0;
                        com.navdy.hud.app.framework.network.NetworkBandwidthController$ComponentInfo a4 = null;
                        Throwable a5 = null;
                        a.receive(a3);
                        org.json.JSONObject a6 = new org.json.JSONObject(new String(a2, 0, a3.getLength()));
                        String s = a6.getString("event");
                        switch(s.hashCode()) {
                            case 1843370353: {
                                i = (s.equals("netstat")) ? 1 : -1;
                                break;
                            }
                            case 260369379: {
                                i = (s.equals("dnslookup")) ? 0 : -1;
                                break;
                            }
                            default: {
                                i = -1;
                            }
                        }
                        switch(i) {
                            case 1: {
                                org.json.JSONArray a7 = a6.getJSONArray("data");
                                if (a7.length() == 0) {
                                    continue label0;
                                }
                                org.json.JSONObject a8 = a7.getJSONObject(0);
                                String s0 = a8.getString("from");
                                String s1 = a8.getString("to");
                                int i0 = a8.getInt("tx");
                                int i1 = a8.getInt("rx");
                                int i2 = a8.getInt("fd");
                                if (com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().isLoggable(2)) {
                                    com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().v(new StringBuilder().append("[").append(i2).append("] tx[").append(i0).append("] rx[").append(i1).append("] from[").append(s0).append("] to[").append(s1).append("]").toString());
                                }
                                if (!android.text.TextUtils.isEmpty((CharSequence)s1)) {
                                    s0 = s1;
                                }
                                if ("8.8.4.4".equals(s0)) {
                                    continue label0;
                                }
                                com.navdy.hud.app.framework.network.NetworkBandwidthController.access$200(this.this$0).addStat(s0, i0, i1, i2);
                                if (i0 <= 0 && i1 <= 0) {
                                    continue label0;
                                }
                                String s2 = com.navdy.hud.app.framework.network.NetworkBandwidthController.access$100(this.this$0).getHostnamefromIP(s0);
                                if (s2 == null) {
                                    continue label0;
                                }
                                com.navdy.hud.app.framework.network.NetworkBandwidthController$Component a9 = (com.navdy.hud.app.framework.network.NetworkBandwidthController$Component)com.navdy.hud.app.framework.network.NetworkBandwidthController.access$300(this.this$0).get(s2);
                                if (a9 == null && s2.contains((CharSequence)"route.hybrid.api.here.com")) {
                                    a9 = com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.HERE_ROUTE;
                                }
                                if (a9 == null) {
                                    continue label0;
                                }
                                if (!com.navdy.hud.app.framework.network.NetworkBandwidthController.access$400(this.this$0) && a9 == com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.HERE_TRAFFIC && i1 > 0) {
                                    com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().i("traffic data downloaded once");
                                    com.navdy.hud.app.framework.network.NetworkBandwidthController.access$402(this.this$0, true);
                                }
                                synchronized((com.navdy.hud.app.framework.network.NetworkBandwidthController$ComponentInfo)com.navdy.hud.app.framework.network.NetworkBandwidthController.access$500(this.this$0).get(a9)) {
                                    a4.lastActivity = android.os.SystemClock.elapsedRealtime();
                                    /*monexit(a4)*/;
                                    continue label0;
                                }
                            }
                            case 0: {
                                String s3 = a6.getString("host");
                                if ("localhost".equals(s3)) {
                                    continue label0;
                                }
                                org.json.JSONArray a11 = a6.getJSONArray("ip");
                                int i3 = a11.length();
                                if (i3 == 0) {
                                    continue label0;
                                }
                                int i4 = 0;
                                while(i4 < i3) {
                                    String s4 = a11.getString(i4);
                                    com.navdy.hud.app.framework.network.NetworkBandwidthController.access$100(this.this$0).addEntry(s4, s3);
                                    if (com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().isLoggable(2)) {
                                        com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().v(new StringBuilder().append("dnslookup ").append(s3).append(" : ").append(s4).toString());
                                    }
                                    i4 = i4 + 1;
                                }
                                continue label0;
                            }
                            default: {
                                com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().v(new StringBuilder().append("invalid command:").append(s).toString());
                                continue label0;
                            }
                        }
                        while(true) {
                            try {
                                /*monexit(a4)*/;
                            } catch(IllegalMonitorStateException | NullPointerException a12) {
                                Throwable a13 = a12;
                                a5 = a13;
                                continue;
                            }
                            throw a5;
                        }
                    } catch(Throwable a14) {
                        com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().e("[networkStat]", a14);
                    }
                }
            } catch(Throwable a15) {
                a0 = a15;
            }
        }
        com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().e(a0);
        if (a != null) {
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
        }
        com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().v("networkStat thread exit");
    }
}
