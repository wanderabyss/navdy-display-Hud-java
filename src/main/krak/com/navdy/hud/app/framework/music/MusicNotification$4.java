package com.navdy.hud.app.framework.music;

class MusicNotification$4 {
    final static int[] $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
    final static int[] $SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl;
    
    static {
        $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
        com.navdy.hud.app.manager.InputManager$CustomKeyEvent a0 = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LEFT;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LONG_PRESS.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        $SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl = new int[com.navdy.hud.app.manager.MusicManager$MediaControl.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl;
        com.navdy.hud.app.manager.MusicManager$MediaControl a2 = com.navdy.hud.app.manager.MusicManager$MediaControl.PLAY;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl[com.navdy.hud.app.manager.MusicManager$MediaControl.PAUSE.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl[com.navdy.hud.app.manager.MusicManager$MediaControl.MUSIC_MENU.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException5) {
        }
    }
}
