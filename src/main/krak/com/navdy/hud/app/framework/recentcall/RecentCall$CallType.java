package com.navdy.hud.app.framework.recentcall;


public enum RecentCall$CallType {
    UNNKNOWN(0),
    INCOMING(1),
    OUTGOING(2),
    MISSED(3);

    private int value;
    RecentCall$CallType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class RecentCall$CallType extends Enum {
//    final private static com.navdy.hud.app.framework.recentcall.RecentCall$CallType[] $VALUES;
//    final public static com.navdy.hud.app.framework.recentcall.RecentCall$CallType INCOMING;
//    final public static com.navdy.hud.app.framework.recentcall.RecentCall$CallType MISSED;
//    final public static com.navdy.hud.app.framework.recentcall.RecentCall$CallType OUTGOING;
//    final public static com.navdy.hud.app.framework.recentcall.RecentCall$CallType UNNKNOWN;
//    int value;
//    
//    static {
//        UNNKNOWN = new com.navdy.hud.app.framework.recentcall.RecentCall$CallType("UNNKNOWN", 0, 0);
//        INCOMING = new com.navdy.hud.app.framework.recentcall.RecentCall$CallType("INCOMING", 1, 1);
//        OUTGOING = new com.navdy.hud.app.framework.recentcall.RecentCall$CallType("OUTGOING", 2, 2);
//        MISSED = new com.navdy.hud.app.framework.recentcall.RecentCall$CallType("MISSED", 3, 3);
//        com.navdy.hud.app.framework.recentcall.RecentCall$CallType[] a = new com.navdy.hud.app.framework.recentcall.RecentCall$CallType[4];
//        a[0] = UNNKNOWN;
//        a[1] = INCOMING;
//        a[2] = OUTGOING;
//        a[3] = MISSED;
//        $VALUES = a;
//    }
//    
//    private RecentCall$CallType(String s, int i, int i0) {
//        super(s, i);
//        this.value = i0;
//    }
//    
//    public static com.navdy.hud.app.framework.recentcall.RecentCall$CallType buildFromValue(int i) {
//        com.navdy.hud.app.framework.recentcall.RecentCall$CallType a = null;
//        switch(i) {
//            case 3: {
//                a = MISSED;
//                break;
//            }
//            case 2: {
//                a = OUTGOING;
//                break;
//            }
//            case 1: {
//                a = INCOMING;
//                break;
//            }
//            default: {
//                a = UNNKNOWN;
//            }
//        }
//        return a;
//    }
//    
//    public static com.navdy.hud.app.framework.recentcall.RecentCall$CallType valueOf(String s) {
//        return (com.navdy.hud.app.framework.recentcall.RecentCall$CallType)Enum.valueOf(com.navdy.hud.app.framework.recentcall.RecentCall$CallType.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.recentcall.RecentCall$CallType[] values() {
//        return $VALUES.clone();
//    }
//    
//    public int getValue() {
//        return this.value;
//    }
//}
//