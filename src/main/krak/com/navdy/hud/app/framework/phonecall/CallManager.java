package com.navdy.hud.app.framework.phonecall;

public class CallManager {
    final private static com.navdy.hud.app.framework.phonecall.CallManager$CallAccepted CALL_ACCEPTED;
    final private static com.navdy.hud.app.framework.phonecall.CallManager$CallEnded CALL_ENDED;
    final private static int NO_RESPONSE_THRESHOLD = 10000;
    final private static com.navdy.service.library.events.callcontrol.PhoneStatusRequest PHONE_STATUS_REQUEST;
    final private static com.navdy.service.library.log.Logger sLogger;
    boolean answered;
    private com.squareup.otto.Bus bus;
    boolean callMuted;
    private com.navdy.hud.app.framework.phonecall.CallNotification callNotification;
    java.util.Stack callStack;
    long callStartMs;
    String callUUID;
    String contact;
    boolean dialSet;
    long duration;
    private android.os.Handler handler;
    boolean incomingCall;
    com.navdy.service.library.events.callcontrol.CallAction lastCallAction;
    private Runnable noResponseCheckRunnable;
    String normalizedNumber;
    String number;
    com.navdy.service.library.events.callcontrol.PhoneStatus phoneStatus;
    boolean putOnStack;
    com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState state;
    boolean userCancelledCall;
    boolean userRejectedCall;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.phonecall.CallManager.class);
        CALL_ENDED = new com.navdy.hud.app.framework.phonecall.CallManager$CallEnded();
        CALL_ACCEPTED = new com.navdy.hud.app.framework.phonecall.CallManager$CallAccepted();
        PHONE_STATUS_REQUEST = new com.navdy.service.library.events.callcontrol.PhoneStatusRequest();
    }
    
    public CallManager(com.squareup.otto.Bus a, android.content.Context a0) {
        this.state = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.IDLE;
        this.noResponseCheckRunnable = (Runnable)new com.navdy.hud.app.framework.phonecall.CallManager$1(this);
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.phoneStatus = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_IDLE;
        this.number = null;
        this.normalizedNumber = null;
        this.contact = null;
        this.incomingCall = false;
        this.answered = false;
        this.putOnStack = false;
        this.callStack = new java.util.Stack();
        this.bus = a;
        this.bus.register(this);
        this.callNotification = new com.navdy.hud.app.framework.phonecall.CallNotification(this, a);
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static boolean access$100(com.navdy.hud.app.framework.phonecall.CallManager a, com.navdy.service.library.events.callcontrol.CallAction a0) {
        return a.isCallActionResponseRequired(a0);
    }
    
    static boolean access$200(com.navdy.hud.app.framework.phonecall.CallManager a) {
        return a.isNotificationAllowed();
    }
    
    static com.navdy.service.library.events.callcontrol.PhoneStatusRequest access$300() {
        return PHONE_STATUS_REQUEST;
    }
    
    static com.squareup.otto.Bus access$400(com.navdy.hud.app.framework.phonecall.CallManager a) {
        return a.bus;
    }
    
    private boolean isCallActionResponseRequired(com.navdy.service.library.events.callcontrol.CallAction a) {
        boolean b = false;
        if (a != null) {
            switch(com.navdy.hud.app.framework.phonecall.CallManager$3.$SwitchMap$com$navdy$service$library$events$callcontrol$CallAction[a.ordinal()]) {
                case 1: case 2: case 3: {
                    b = true;
                    break;
                }
                default: {
                    b = false;
                }
            }
        } else {
            b = false;
        }
        return b;
    }
    
    private boolean isIos() {
        com.navdy.hud.app.manager.RemoteDeviceManager a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        com.navdy.service.library.events.DeviceInfo$Platform a0 = (a.isRemoteDeviceConnected()) ? a.getRemoteDevicePlatform() : null;
        return com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS.equals(a0);
    }
    
    private boolean isNotificationAllowed() {
        boolean b = false;
        boolean b0 = this.incomingCall;
        label3: {
            label0: {
                label1: {
                    label2: {
                        if (!b0) {
                            break label2;
                        }
                        if (!this.incomingCall) {
                            break label1;
                        }
                        if (!com.navdy.hud.app.framework.glance.GlanceHelper.isPhoneNotificationEnabled()) {
                            break label1;
                        }
                    }
                    if (com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().isPhoneNotificationsEnabled()) {
                        break label0;
                    }
                }
                b = false;
                break label3;
            }
            b = true;
        }
        return b;
    }
    
    private long toSeconds(long j) {
        return (500L + j) / 1000L;
    }
    
    public void clearCallStack() {
        this.callStack.clear();
    }
    
    public void dial(String s, String s0, String s1) {
        if (s != null) {
            this.state = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.DIALING;
            this.phoneStatus = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_DIALING;
            this.dialSet = true;
            this.incomingCall = false;
            this.number = s;
            this.normalizedNumber = com.navdy.hud.app.util.PhoneUtil.normalizeNumber(s);
            this.contact = s1;
            this.sendNotification();
            this.sendCallAction(com.navdy.service.library.events.callcontrol.CallAction.CALL_DIAL, s);
        } else {
            sLogger.e(new StringBuilder().append("null number passed contact[").append(s1).append("]").toString());
        }
    }
    
    public void disconnect() {
        sLogger.v("got disconnect event, setting to IDLE");
        this.phoneStatus = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_IDLE;
        this.state = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.IDLE;
        this.incomingCall = false;
        this.number = null;
        this.normalizedNumber = null;
        this.contact = null;
    }
    
    public int getCurrentCallDuration() {
        return (this.callStartMs > 0L) ? (int)((android.os.SystemClock.elapsedRealtime() - this.callStartMs) / 1000L) : -1;
    }
    
    public com.navdy.service.library.events.callcontrol.PhoneStatus getPhoneStatus() {
        return this.phoneStatus;
    }
    
    public boolean isCallInProgress() {
        boolean b = false;
        com.navdy.service.library.events.callcontrol.PhoneStatus a = this.phoneStatus;
        com.navdy.service.library.events.callcontrol.PhoneStatus a0 = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_OFFHOOK;
        label2: {
            label0: {
                label1: {
                    if (a == a0) {
                        break label1;
                    }
                    if (this.phoneStatus == com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_DIALING) {
                        break label1;
                    }
                    if (this.phoneStatus != com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_RINGING) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    public void onDeviceInfoAvailable(com.navdy.hud.app.event.DeviceInfoAvailable a) {
        if (a.deviceInfo != null && a.deviceInfo.platform == com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_Android) {
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.callcontrol.PhoneStatusRequest()));
        }
    }
    
    public void onPhoneBatteryEvent(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus a) {
        sLogger.v(new StringBuilder().append("[Battery-phone] status[").append(a.status).append("] level[").append(a.level).append("] charging[").append(a.charging).append("]").toString());
        com.navdy.hud.app.framework.phonecall.PhoneBatteryNotification.showBatteryToast(a);
    }
    
    public void onPhoneEvent(com.navdy.service.library.events.callcontrol.PhoneEvent a) {
        this.handler.removeCallbacks(this.noResponseCheckRunnable);
        this.lastCallAction = null;
        com.navdy.service.library.events.callcontrol.PhoneStatus a0 = a.status;
        com.navdy.service.library.events.callcontrol.PhoneStatus a1 = this.phoneStatus;
        label4: {
            label12: {
                if (a0 != a1) {
                    break label12;
                }
                boolean b = this.dialSet;
                label13: {
                    if (!b) {
                        break label13;
                    }
                    if (a.status != com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_DIALING) {
                        break label13;
                    }
                    this.dialSet = false;
                    break label12;
                }
                if (!android.text.TextUtils.equals((CharSequence)com.navdy.hud.app.util.PhoneUtil.normalizeNumber(a.number), (CharSequence)this.normalizedNumber)) {
                    break label12;
                }
                break label4;
            }
            sLogger.v(new StringBuilder().append("phoneEvent:").append(a).toString());
            com.navdy.service.library.events.DeviceInfo a2 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
            com.navdy.service.library.events.callcontrol.PhoneStatus a3 = this.phoneStatus;
            this.phoneStatus = a.status;
            long j = android.os.SystemClock.elapsedRealtime();
            switch(com.navdy.hud.app.framework.phonecall.CallManager$3.$SwitchMap$com$navdy$service$library$events$callcontrol$PhoneStatus[a.status.ordinal()]) {
                case 5: {
                    if (this.state != com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.DIALING) {
                        break;
                    }
                    this.state = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.FAILED;
                    if (!this.isNotificationAllowed()) {
                        break;
                    }
                    this.sendNotification();
                    break;
                }
                case 4: {
                    com.navdy.hud.app.framework.phonecall.CallNotification.clearToast();
                    this.callStartMs = j;
                    this.answered = true;
                    if (!android.text.TextUtils.isEmpty((CharSequence)a.callUUID)) {
                        this.callUUID = a.callUUID;
                    }
                    if (android.text.TextUtils.isEmpty((CharSequence)this.normalizedNumber)) {
                        this.normalizedNumber = com.navdy.hud.app.util.PhoneUtil.normalizeNumber(a.number);
                    }
                    if (android.text.TextUtils.isEmpty((CharSequence)this.contact)) {
                        this.contact = a.contact_name;
                    }
                    if (android.text.TextUtils.isEmpty((CharSequence)this.number)) {
                        this.number = a.number;
                    }
                    this.state = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.IN_PROGRESS;
                    if (!this.isNotificationAllowed()) {
                        break;
                    }
                    this.sendNotification();
                    this.bus.post(CALL_ACCEPTED);
                    break;
                }
                case 3: {
                    this.callStartMs = j;
                    this.incomingCall = false;
                    this.answered = false;
                    this.number = a.number;
                    this.normalizedNumber = com.navdy.hud.app.util.PhoneUtil.normalizeNumber(a.number);
                    this.contact = a.contact_name;
                    if (!android.text.TextUtils.isEmpty((CharSequence)a.callUUID)) {
                        this.callUUID = a.callUUID;
                    }
                    this.state = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.DIALING;
                    if (!this.isNotificationAllowed()) {
                        break;
                    }
                    this.sendNotification();
                    break;
                }
                case 2: {
                    com.navdy.service.library.events.callcontrol.PhoneStatus a4 = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_OFFHOOK;
                    label11: {
                        if (a3 != a4) {
                            break label11;
                        }
                        sLogger.v("we are in a call");
                        label10: {
                            if (a2 == null) {
                                break label10;
                            }
                            if (a2.platform != com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS) {
                                break label10;
                            }
                            com.navdy.hud.app.framework.phonecall.CallManager$Info a5 = new com.navdy.hud.app.framework.phonecall.CallManager$Info(this.number, this.normalizedNumber, this.contact, this.incomingCall, this.answered, this.callStartMs, this.callUUID, this.duration, this.userRejectedCall, this.userCancelledCall, true, this.callMuted);
                            this.callStack.push(a5);
                            sLogger.v(new StringBuilder().append("stack pushed [").append(this.contact).append(" , ").append(this.number).append(",").append(this.normalizedNumber).append("]").toString());
                            break label11;
                        }
                        sLogger.w("stack not implemented on android");
                        break label4;
                    }
                    this.callStartMs = j;
                    this.incomingCall = true;
                    this.answered = false;
                    this.number = a.number;
                    this.normalizedNumber = com.navdy.hud.app.util.PhoneUtil.normalizeNumber(a.number);
                    this.contact = a.contact_name;
                    this.callMuted = false;
                    if (!android.text.TextUtils.isEmpty((CharSequence)a.callUUID)) {
                        this.callUUID = a.callUUID;
                    }
                    this.state = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.RINGING;
                    if (this.isNotificationAllowed()) {
                        this.callNotification.showIncomingCallToast();
                        break;
                    } else {
                        sLogger.v("incoming phone call disabled");
                        break;
                    }
                }
                case 1: {
                    sLogger.v(new StringBuilder().append("IDLE: putOnStack=").append(this.putOnStack).append(" lastStatus=").append(a3).toString());
                    this.callUUID = null;
                    this.callMuted = false;
                    com.navdy.service.library.events.callcontrol.PhoneStatus a6 = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_RINGING;
                    label5: {
                        label8: {
                            boolean b0 = false;
                            label9: {
                                if (a3 == a6) {
                                    break label9;
                                }
                                if (a3 != com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_DIALING) {
                                    break label8;
                                }
                            }
                            this.duration = 0L;
                            if (this.userRejectedCall) {
                                this.state = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.REJECTED;
                                b0 = true;
                            } else if (this.userCancelledCall) {
                                this.state = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.CANCELLED;
                                b0 = false;
                            } else if (this.answered) {
                                this.state = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.ENDED;
                                b0 = false;
                            } else {
                                this.state = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.MISSED;
                                b0 = false;
                            }
                            com.navdy.hud.app.framework.phonecall.CallNotification.clearToast();
                            this.userRejectedCall = false;
                            this.userCancelledCall = false;
                            if (this.putOnStack) {
                                this.removeNotification();
                                break label5;
                            } else {
                                if (b0) {
                                    break label5;
                                }
                                com.navdy.hud.app.framework.notifications.INotification a7 = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getCurrentNotification();
                                label6: {
                                    label7: {
                                        if (a7 == null) {
                                            break label7;
                                        }
                                        if (!android.text.TextUtils.equals((CharSequence)a7.getId(), (CharSequence)"navdy#phone#call#notif")) {
                                            break label6;
                                        }
                                    }
                                    this.removeNotification();
                                    break label5;
                                }
                                if (!this.isNotificationAllowed()) {
                                    break label5;
                                }
                                this.sendNotification();
                                break label5;
                            }
                        }
                        if (a3 != com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_OFFHOOK) {
                            break label5;
                        }
                        label3: {
                            if (a2 == null) {
                                break label3;
                            }
                            if (a2.platform != com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS) {
                                break label3;
                            }
                            if (android.text.TextUtils.isEmpty((CharSequence)a.number)) {
                                break label3;
                            }
                            if (android.text.TextUtils.equals((CharSequence)com.navdy.hud.app.util.PhoneUtil.normalizeNumber(a.number), (CharSequence)this.normalizedNumber)) {
                                break label3;
                            }
                            sLogger.d("Received end for a different call than the current one");
                            break label4;
                        }
                        this.duration = this.toSeconds(j - this.callStartMs);
                        this.state = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.ENDED;
                        boolean b1 = this.userRejectedCall;
                        label2: {
                            label0: {
                                label1: {
                                    if (b1) {
                                        break label1;
                                    }
                                    if (this.userCancelledCall) {
                                        break label1;
                                    }
                                    if (!this.putOnStack) {
                                        break label0;
                                    }
                                }
                                this.removeNotification();
                                break label2;
                            }
                            if (this.isNotificationAllowed()) {
                                this.sendNotification();
                            }
                        }
                        this.userRejectedCall = false;
                        this.userCancelledCall = false;
                    }
                    this.putOnStack = false;
                    this.bus.post(CALL_ENDED);
                    if (!this.isNotificationAllowed()) {
                        break;
                    }
                    if (this.number == null) {
                        break;
                    }
                    this.bus.post(new com.navdy.service.library.events.callcontrol.CallEvent(Boolean.valueOf(this.incomingCall), Boolean.valueOf(this.answered), this.number, this.contact, Long.valueOf(this.toSeconds(this.callStartMs)), Long.valueOf(this.duration)));
                    break;
                }
            }
            sLogger.v(new StringBuilder().append("state:").append(this.state).toString());
        }
    }
    
    public void onPhoneStatusResponse(com.navdy.service.library.events.callcontrol.PhoneStatusResponse a) {
        sLogger.v(new StringBuilder().append("onPhoneStatusResponse [").append(a.status).append("] [").append(a.callStatus).append("]").toString());
        label0: if (a.status == com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS && a.callStatus != null) {
            com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState a0 = this.state;
            com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState a1 = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.IDLE;
            label1: {
                if (a0 == a1) {
                    break label1;
                }
                if (this.state != com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.IN_PROGRESS) {
                    break label0;
                }
            }
            sLogger.v("onPhoneStatusResponse fwding event");
            this.onPhoneEvent(a.callStatus);
        }
    }
    
    void removeNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.callNotification.getId());
    }
    
    void restoreInfo(com.navdy.hud.app.framework.phonecall.CallManager$Info a) {
        this.bus.post(new com.navdy.service.library.events.callcontrol.CallEvent(Boolean.valueOf(this.incomingCall), Boolean.valueOf(this.answered), this.number, this.contact, Long.valueOf(this.toSeconds(this.callStartMs)), Long.valueOf(this.duration)));
        this.number = a.number;
        this.normalizedNumber = a.normalizedNumber;
        this.contact = a.contact;
        this.incomingCall = a.incomingCall;
        this.answered = a.answered;
        this.callStartMs = a.callStartMs;
        this.callUUID = a.callUUID;
        this.duration = a.duration;
        this.userRejectedCall = a.userRejectedCall;
        this.userCancelledCall = a.userCancelledCall;
        this.putOnStack = a.putOnStack;
        this.callMuted = a.callMuted;
        sLogger.v(new StringBuilder().append("stack restored [").append(this.contact).append(" , ").append(this.number).append(" , ").append(this.normalizedNumber).append("]").toString());
        if (this.isIos()) {
            if (this.answered) {
                this.phoneStatus = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_OFFHOOK;
            }
        } else {
            this.handler.postDelayed((Runnable)new com.navdy.hud.app.framework.phonecall.CallManager$2(this), 1000L);
        }
    }
    
    void sendCallAction(com.navdy.service.library.events.callcontrol.CallAction a, String s) {
        try {
            com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState a0 = this.state;
            com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState a1 = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.RINGING;
            label0: {
                label2: {
                    if (a0 != a1) {
                        break label2;
                    }
                    if (a != com.navdy.service.library.events.callcontrol.CallAction.CALL_REJECT) {
                        break label2;
                    }
                    this.userRejectedCall = true;
                    break label0;
                }
                com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState a2 = this.state;
                com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState a3 = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.DIALING;
                label1: {
                    if (a2 == a3) {
                        break label1;
                    }
                    if (this.state != com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.IN_PROGRESS) {
                        break label0;
                    }
                }
                if (a == com.navdy.service.library.events.callcontrol.CallAction.CALL_END) {
                    this.userCancelledCall = true;
                }
            }
            switch(com.navdy.hud.app.framework.phonecall.CallManager$3.$SwitchMap$com$navdy$service$library$events$callcontrol$CallAction[a.ordinal()]) {
                case 5: {
                    this.callMuted = false;
                    break;
                }
                case 4: {
                    this.callMuted = true;
                    break;
                }
            }
            if (this.isCallActionResponseRequired(a)) {
                this.handler.removeCallbacks(this.noResponseCheckRunnable);
                this.handler.postDelayed(this.noResponseCheckRunnable, 10000L);
            }
            this.lastCallAction = a;
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.callcontrol.TelephonyRequest(a, s, this.callUUID)));
            int i = this.callStack.size();
            sLogger.v(new StringBuilder().append("call stack size=").append(i).append(" action=").append(a.name()).toString());
            if (i > 0) {
                if (a != com.navdy.service.library.events.callcontrol.CallAction.CALL_REJECT) {
                    if (a == com.navdy.service.library.events.callcontrol.CallAction.CALL_END) {
                        this.restoreInfo((com.navdy.hud.app.framework.phonecall.CallManager$Info)this.callStack.pop());
                    }
                } else {
                    this.restoreInfo((com.navdy.hud.app.framework.phonecall.CallManager$Info)this.callStack.pop());
                }
            }
        } catch(Throwable a4) {
            sLogger.e(a4);
        }
    }
    
    void sendNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification((com.navdy.hud.app.framework.notifications.INotification)this.callNotification);
    }
}
