package com.navdy.hud.app.framework.glympse;

class GlympseManager$1$1 implements Runnable {
    final com.navdy.hud.app.framework.glympse.GlympseManager$1 this$1;
    
    GlympseManager$1$1(com.navdy.hud.app.framework.glympse.GlympseManager$1 a) {
        super();
        this.this$1 = a;
    }
    
    public void run() {
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            com.navdy.hud.app.maps.here.HereNavigationManager a = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
            com.navdy.hud.app.maps.here.HereNavController a0 = a.getNavController();
            if (a0.getState() == com.navdy.hud.app.maps.here.HereNavController$State.NAVIGATING) {
                java.util.Date a1 = a0.getEta(true, com.here.android.mpa.routing.Route$TrafficPenaltyMode.OPTIMAL);
                if (com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(a1)) {
                    Object a2 = null;
                    long j = android.os.SystemClock.elapsedRealtime();
                    com.glympse.android.api.GTrackBuilder a3 = com.glympse.android.api.GlympseFactory.createTrackBuilder();
                    a3.setSource(0);
                    a3.setDistance((int)a0.getDestinationDistance());
                    com.here.android.mpa.routing.Route a4 = a.getCurrentRoute();
                    StringBuilder a5 = new StringBuilder();
                    if (a4 == null) {
                        a2 = a3;
                    } else {
                        java.util.List a6 = a4.getManeuvers();
                        if (a6 == null) {
                            a2 = a3;
                        } else {
                            Object a7 = a6.iterator();
                            a2 = a3;
                            while(((java.util.Iterator)a7).hasNext()) {
                                com.here.android.mpa.common.GeoCoordinate a8 = ((com.here.android.mpa.routing.Maneuver)((java.util.Iterator)a7).next()).getCoordinate();
                                if (a8 != null) {
                                    double d = a8.getLatitude();
                                    double d0 = a8.getLongitude();
                                    ((com.glympse.android.api.GTrackBuilder)a2).addLocation(com.glympse.android.core.CoreFactory.createLocation(d, d0));
                                    a5.append(new StringBuilder().append("{").append(d).append(",").append(d0).append("} ").toString());
                                }
                            }
                        }
                    }
                    com.glympse.android.api.GTrack a9 = ((com.glympse.android.api.GTrackBuilder)a2).getTrack();
                    com.navdy.hud.app.framework.glympse.GlympseManager.access$200().v(new StringBuilder().append("update ETA track = ").append(a5.toString()).toString());
                    com.navdy.hud.app.framework.glympse.GlympseManager.access$200().v(new StringBuilder().append("update ETA time to generate track:").append(android.os.SystemClock.elapsedRealtime() - j).toString());
                    com.navdy.hud.app.framework.glympse.GlympseManager.access$400().post((Runnable)new com.navdy.hud.app.framework.glympse.GlympseManager$1$1$1(this, a1, a9));
                }
            }
        }
    }
}
