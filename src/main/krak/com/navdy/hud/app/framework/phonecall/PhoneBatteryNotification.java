package com.navdy.hud.app.framework.phonecall;
import com.navdy.hud.app.R;

public class PhoneBatteryNotification {
    final private static String EMPTY = "";
    final private static String ID_BATTERY_EXTREMELY_LOW = "phone-bat-exlow";
    final private static String ID_BATTERY_LOW = "phone-bat-low";
    final private static String ID_NO_NETWORK = "phone-no-network";
    final private static int PHONE_BATTERY_TIMEOUT = 2000;
    final private static int PHONE_BATTERY_TIMEOUT_EXTREMELY_LOW = 20000;
    final private static int PHONE_NO_NETWORK_TIMEOUT = 2000;
    final private static String noNetwork;
    final private static android.content.res.Resources resources;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static String unknown;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.phonecall.PhoneBatteryNotification.class);
        resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        unknown = resources.getString(R.string.question_mark);
        noNetwork = resources.getString(R.string.phone_no_network);
    }
    
    public PhoneBatteryNotification() {
    }
    
    private static void dismissAllBatteryToasts() {
        com.navdy.hud.app.framework.toast.ToastManager a = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        String[] a0 = new String[2];
        a0[0] = "phone-bat-low";
        a0[1] = "phone-bat-exlow";
        a.dismissCurrentToast(a0);
        String[] a1 = new String[2];
        a1[0] = "phone-bat-low";
        a1[1] = "phone-bat-exlow";
        a.clearPendingToast(a1);
    }
    
    public static void dismissAllToasts() {
        com.navdy.hud.app.framework.phonecall.PhoneBatteryNotification.dismissAllBatteryToasts();
        com.navdy.hud.app.framework.toast.ToastManager a = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        a.dismissCurrentToast("phone-no-network");
        a.clearPendingToast("phone-no-network");
    }
    
    public static void showBatteryToast(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus a) {
        sLogger.v(new StringBuilder().append("[PhoneBatteryStatus] status[").append(a.status).append("] charging[").append(a.charging).append("] level[").append(a.level).append("]").toString());
        com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus a0 = a.status;
        com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus a1 = com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus.BATTERY_OK;
        label2: {
            String s = null;
            String s0 = null;
            String s1 = null;
            int i = 0;
            label0: {
                label1: {
                    if (a0 == a1) {
                        break label1;
                    }
                    if (!Boolean.TRUE.equals(a.charging)) {
                        break label0;
                    }
                }
                sLogger.v("phone battery is ok/charging");
                com.navdy.hud.app.framework.phonecall.PhoneBatteryNotification.dismissAllBatteryToasts();
                break label2;
            }
            android.os.Bundle a2 = new android.os.Bundle();
            Object a3 = (a.level == null) ? unknown : a.level;
            String s2 = String.valueOf(a3);
            com.navdy.hud.app.framework.toast.ToastManager a4 = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
            switch(com.navdy.hud.app.framework.phonecall.PhoneBatteryNotification$1.$SwitchMap$com$navdy$service$library$events$callcontrol$PhoneBatteryStatus$BatteryStatus[a.status.ordinal()]) {
                case 2: {
                    android.content.res.Resources a5 = resources;
                    Object[] a6 = new Object[1];
                    a6[0] = s2;
                    s = a5.getString(R.string.phone_low_battery, a6);
                    a2.putInt("13", 2000);
                    s0 = com.navdy.hud.app.framework.voice.TTSUtils.TTS_PHONE_BATTERY_LOW;
                    a4.dismissCurrentToast("phone-bat-exlow");
                    String[] a7 = new String[2];
                    a7[0] = "phone-bat-low";
                    a7[1] = "phone-bat-exlow";
                    a4.clearPendingToast(a7);
                    s1 = "phone-bat-low";
                    i = R.drawable.icon_toast_phone_battery_low;
                    break;
                }
                case 1: {
                    android.content.res.Resources a8 = resources;
                    Object[] a9 = new Object[1];
                    a9[0] = s2;
                    s = a8.getString(R.string.phone_ex_low_battery, a9);
                    a2.putInt("13", 20000);
                    a2.putBoolean("12", true);
                    s0 = com.navdy.hud.app.framework.voice.TTSUtils.TTS_PHONE_BATTERY_VERY_LOW;
                    a4.dismissCurrentToast("phone-bat-low");
                    String[] a10 = new String[2];
                    a10[0] = "phone-bat-low";
                    a10[1] = "phone-bat-exlow";
                    a4.clearPendingToast(a10);
                    s1 = "phone-bat-exlow";
                    i = R.drawable.icon_toast_phone_battery_very_low;
                    break;
                }
                default: {
                    s1 = null;
                    s = "";
                    i = 0;
                    s0 = null;
                }
            }
            a2.putInt("8", i);
            a2.putString("4", s);
            a2.putInt("5", R.style.Glances_1);
            a2.putString("17", s0);
            if (!android.text.TextUtils.equals((CharSequence)s1, (CharSequence)a4.getCurrentToastId())) {
                com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams(s1, a2, (com.navdy.hud.app.framework.toast.IToastCallback)null, false, false));
            }
        }
    }
}
