package com.navdy.hud.app.framework.recentcall;

public class RecentCallManager {
    final private static com.navdy.hud.app.framework.recentcall.RecentCallManager$RecentCallChanged RECENT_CALL_CHANGED;
    final private static com.navdy.hud.app.framework.recentcall.RecentCallManager sInstance;
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.squareup.otto.Bus bus;
    private java.util.HashMap contactRequestMap;
    private java.util.Map numberMap;
    private volatile java.util.List recentCalls;
    private java.util.HashMap tempContactLookupMap;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.recentcall.RecentCallManager.class);
        RECENT_CALL_CHANGED = new com.navdy.hud.app.framework.recentcall.RecentCallManager$RecentCallChanged();
        sInstance = new com.navdy.hud.app.framework.recentcall.RecentCallManager();
    }
    
    private RecentCallManager() {
        this.numberMap = (java.util.Map)new java.util.HashMap();
        this.tempContactLookupMap = new java.util.HashMap();
        this.contactRequestMap = new java.util.HashMap();
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        this.bus.register(this);
    }
    
    static void access$000(com.navdy.hud.app.framework.recentcall.RecentCallManager a) {
        a.load();
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static java.util.HashMap access$200(com.navdy.hud.app.framework.recentcall.RecentCallManager a) {
        return a.contactRequestMap;
    }
    
    static java.util.HashMap access$300(com.navdy.hud.app.framework.recentcall.RecentCallManager a) {
        return a.tempContactLookupMap;
    }
    
    static com.squareup.otto.Bus access$400(com.navdy.hud.app.framework.recentcall.RecentCallManager a) {
        return a.bus;
    }
    
    static void access$500(com.navdy.hud.app.framework.recentcall.RecentCallManager a, java.util.List a0) {
        a.storeContactInfo(a0);
    }
    
    private void buildMap() {
        this.numberMap.clear();
        if (this.recentCalls != null && this.recentCalls.size() != 0) {
            Object a = this.recentCalls.iterator();
            while(((java.util.Iterator)a).hasNext()) {
                com.navdy.hud.app.framework.recentcall.RecentCall a0 = (com.navdy.hud.app.framework.recentcall.RecentCall)((java.util.Iterator)a).next();
                this.numberMap.put(Long.valueOf(a0.numericNumber), a0);
            }
        }
    }
    
    public static com.navdy.hud.app.framework.recentcall.RecentCallManager getInstance() {
        return sInstance;
    }
    
    private void load() {
        label1: {
            Throwable a = null;
            label0: {
                try {
                    String s = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getProfileName();
                    this.recentCalls = com.navdy.hud.app.storage.db.helper.RecentCallPersistenceHelper.getRecentsCalls(s);
                    sLogger.v(new StringBuilder().append("load recentCall id[").append(s).append("] calls[").append(this.recentCalls.size()).append("]").toString());
                } catch(Throwable a0) {
                    a = a0;
                    break label0;
                }
                this.buildMap();
                this.bus.post(RECENT_CALL_CHANGED);
                break label1;
            }
            try {
                this.recentCalls = null;
                sLogger.e(a);
            } catch(Throwable a1) {
                this.buildMap();
                this.bus.post(RECENT_CALL_CHANGED);
                throw a1;
            }
            this.buildMap();
            this.bus.post(RECENT_CALL_CHANGED);
        }
    }
    
    private void requestContactInfo(com.navdy.hud.app.framework.recentcall.RecentCall a) {
        sLogger.v(new StringBuilder().append("number not available for [").append(a.number).append("]").toString());
        synchronized(this.contactRequestMap) {
            if (this.contactRequestMap.containsKey(a.number)) {
                sLogger.v(new StringBuilder().append("contact [").append(a.number).append("] request already pending").toString());
                /*monexit(a0)*/;
            } else {
                this.contactRequestMap.put(a.number, a);
                sLogger.v(new StringBuilder().append("contact [").append(a.number).append("] request sent").toString());
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.contacts.ContactRequest(a.number)));
                /*monexit(a0)*/;
            }
        }
    }
    
    private void storeContactInfo(com.navdy.hud.app.framework.recentcall.RecentCall a) {
        java.util.ArrayList a0 = new java.util.ArrayList();
        ((java.util.List)a0).add(a);
        this.storeContactInfo((java.util.List)a0);
    }
    
    private void storeContactInfo(java.util.List a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.recentcall.RecentCallManager$3(this, a), 1);
    }
    
    public void buildRecentCalls() {
        synchronized(this.contactRequestMap) {
            this.contactRequestMap.clear();
            /*monexit(a)*/;
        }
        if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.recentcall.RecentCallManager$1(this), 1);
        } else {
            this.load();
        }
    }
    
    public void clearContactLookupMap() {
        synchronized(this.tempContactLookupMap) {
            this.tempContactLookupMap.clear();
            /*monexit(a)*/;
        }
    }
    
    public void clearRecentCalls() {
        this.setRecentCalls((java.util.List)null);
    }
    
    public java.util.List getContactsFromId(String s) {
        Object a = null;
        synchronized(this.tempContactLookupMap) {
            a = this.tempContactLookupMap.get(s);
            /*monexit(a0)*/;
        }
        return (java.util.List)a;
    }
    
    public int getDefaultContactImage(long j) {
        com.navdy.hud.app.framework.recentcall.RecentCall a = (com.navdy.hud.app.framework.recentcall.RecentCall)this.numberMap.get(Long.valueOf(j));
        return (a != null) ? a.defaultImageIndex : -1;
    }
    
    public java.util.List getRecentCalls() {
        return this.recentCalls;
    }
    
    public int getRecentCallsSize() {
        return (this.recentCalls == null) ? 0 : this.recentCalls.size();
    }
    
    public boolean handleNewCall(com.navdy.hud.app.framework.recentcall.RecentCall a) {
        java.util.HashMap a0 = null;
        Throwable a1 = null;
        sLogger.v(new StringBuilder().append("handle new call:").append(a.number).toString());
        boolean b = com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(a.number);
        label0: {
            boolean b0 = false;
            label4: if (b) {
                this.storeContactInfo(a);
                b0 = true;
            } else {
                String s = null;                label3: synchronized(this.tempContactLookupMap) {
                    Object a2 = this.tempContactLookupMap.get(a.number);
                    s = null;
                    label1: {
                        label2: {
                            if (a2 == null) {
                                break label2;
                            }
                            if (((java.util.List)a2).size() > 1) {
                                break label1;
                            }
                            s = ((com.navdy.service.library.events.contacts.Contact)((java.util.List)a2).get(0)).number;
                        }
                        /*monexit(a0)*/;
                        break label3;
                    }
                    /*monexit(a0)*/;
                    b0 = false;
                    break label4;
                }
                if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                    this.requestContactInfo(a);
                    b0 = false;
                } else {
                    a.number = s;
                    this.storeContactInfo(a);
                    b0 = true;
                }
            }
            return b0;
        }
        while(true) {
            try {
                /*monexit(a0)*/;
            } catch(IllegalMonitorStateException | NullPointerException a4) {
                Throwable a5 = a4;
                a1 = a5;
                continue;
            }
            throw a1;
        }
    }
    
    public void onCallEvent(com.navdy.service.library.events.callcontrol.CallEvent a) {
        if (android.text.TextUtils.isEmpty((CharSequence)a.number)) {
            sLogger.e(new StringBuilder().append("call event does not have a number, ").append(a.contact_name).toString());
        } else {
            com.navdy.hud.app.framework.recentcall.RecentCall$CallType a0 = (a.incoming.booleanValue()) ? (a.answered.booleanValue()) ? com.navdy.hud.app.framework.recentcall.RecentCall$CallType.INCOMING : com.navdy.hud.app.framework.recentcall.RecentCall$CallType.MISSED : com.navdy.hud.app.framework.recentcall.RecentCall$CallType.OUTGOING;
            this.handleNewCall(new com.navdy.hud.app.framework.recentcall.RecentCall(a.contact_name, com.navdy.hud.app.framework.recentcall.RecentCall$Category.PHONE_CALL, a.number, com.navdy.hud.app.framework.contacts.NumberType.OTHER, new java.util.Date(), a0, -1, 0L));
        }
    }
    
    public void onContactResponse(com.navdy.service.library.events.contacts.ContactResponse a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.recentcall.RecentCallManager$2(this, a), 1);
    }
    
    public void setRecentCalls(java.util.List a) {
        this.recentCalls = a;
        this.buildMap();
        this.bus.post(RECENT_CALL_CHANGED);
    }
}
