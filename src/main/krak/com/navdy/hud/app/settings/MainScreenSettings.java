package com.navdy.hud.app.settings;

public class MainScreenSettings implements android.os.Parcelable {
    final public static android.os.Parcelable$Creator CREATOR;
    protected com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration landscapeConfiguration;
    protected com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration portraitConfiguration;
    
    static {
        CREATOR = (android.os.Parcelable$Creator)new com.navdy.hud.app.settings.MainScreenSettings$1();
    }
    
    public MainScreenSettings() {
        this(new com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration(), new com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration());
    }
    
    public MainScreenSettings(android.os.Parcel a) {
        this((com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration)a.readParcelable(com.navdy.hud.app.settings.MainScreenSettings.class.getClassLoader()), (com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration)a.readParcelable(com.navdy.hud.app.settings.MainScreenSettings.class.getClassLoader()));
    }
    
    public MainScreenSettings(com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration a, com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration a0) {
        this.portraitConfiguration = a;
        this.landscapeConfiguration = a0;
    }
    
    public int describeContents() {
        return 0;
    }
    
    public com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration getLandscapeConfiguration() {
        return this.landscapeConfiguration;
    }
    
    public com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration getPortraitConfiguration() {
        return this.portraitConfiguration;
    }
    
    public void setLandscapeConfiguration(com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration a) {
        this.landscapeConfiguration = a;
    }
    
    public void setPortraitConfiguration(com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration a) {
        this.portraitConfiguration = a;
    }
    
    public void writeToParcel(android.os.Parcel a, int i) {
        a.writeParcelable((android.os.Parcelable)this.portraitConfiguration, 0);
        a.writeParcelable((android.os.Parcelable)this.landscapeConfiguration, 0);
    }
}
