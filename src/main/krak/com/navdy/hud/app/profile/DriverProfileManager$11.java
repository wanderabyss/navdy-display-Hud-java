package com.navdy.hud.app.profile;

class DriverProfileManager$11 implements Runnable {
    final com.navdy.hud.app.profile.DriverProfileManager this$0;
    final com.navdy.service.library.events.preferences.LocalPreferences val$preferences;
    
    DriverProfileManager$11(com.navdy.hud.app.profile.DriverProfileManager a, com.navdy.service.library.events.preferences.LocalPreferences a0) {
        super();
        this.this$0 = a;
        this.val$preferences = a0;
    }
    
    public void run() {
        com.navdy.hud.app.profile.DriverProfileManager.access$500(this.this$0).setLocalPreferences(this.val$preferences);
        com.navdy.hud.app.profile.DriverProfileManager.access$600(this.this$0).setLocalPreferences(this.val$preferences);
        com.navdy.hud.app.analytics.AnalyticsSupport.recordPreferenceChange(com.navdy.hud.app.profile.DriverProfileManager.access$500(this.this$0));
        com.navdy.hud.app.profile.DriverProfileManager.access$200(this.this$0).post(this.val$preferences);
    }
}
