package com.navdy.hud.app.bluetooth.vcard;

class VCardBuilder$PostalStruct {
    final String addressData;
    final boolean appendCharset;
    final boolean reallyUseQuotedPrintable;
    
    public VCardBuilder$PostalStruct(boolean b, boolean b0, String s) {
        this.reallyUseQuotedPrintable = b;
        this.appendCharset = b0;
        this.addressData = s;
    }
}
