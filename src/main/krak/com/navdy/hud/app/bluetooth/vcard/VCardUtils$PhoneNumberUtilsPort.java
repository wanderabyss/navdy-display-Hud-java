package com.navdy.hud.app.bluetooth.vcard;

public class VCardUtils$PhoneNumberUtilsPort {
    public VCardUtils$PhoneNumberUtilsPort() {
    }
    
    public static String formatNumber(String s, int i) {
        android.text.SpannableStringBuilder a = new android.text.SpannableStringBuilder((CharSequence)s);
        android.telephony.PhoneNumberUtils.formatNumber((android.text.Editable)a, i);
        return a.toString();
    }
}
