package com.navdy.hud.app.bluetooth.pbap;

class BluetoothPbapVcardList {
    final private java.util.ArrayList mCards;
    
    public BluetoothPbapVcardList(java.io.InputStream a, byte a0) {
        this.mCards = new java.util.ArrayList();
        this.parse(a, a0);
    }
    
    static java.util.ArrayList access$000(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapVcardList a) {
        return a.mCards;
    }
    
    private void parse(java.io.InputStream a, byte a0) {
        com.navdy.hud.app.bluetooth.vcard.VCardParser a1 = (a0 != 1) ? new com.navdy.hud.app.bluetooth.vcard.VCardParser_V21() : new com.navdy.hud.app.bluetooth.vcard.VCardParser_V30();
        com.navdy.hud.app.bluetooth.vcard.VCardEntryConstructor a2 = new com.navdy.hud.app.bluetooth.vcard.VCardEntryConstructor();
        com.navdy.hud.app.bluetooth.vcard.VCardEntryCounter a3 = new com.navdy.hud.app.bluetooth.vcard.VCardEntryCounter();
        a2.addEntryHandler((com.navdy.hud.app.bluetooth.vcard.VCardEntryHandler)new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapVcardList$CardEntryHandler(this));
        a1.addInterpreter((com.navdy.hud.app.bluetooth.vcard.VCardInterpreter)a2);
        a1.addInterpreter((com.navdy.hud.app.bluetooth.vcard.VCardInterpreter)a3);
        try {
            a1.parse(a);
        } catch(com.navdy.hud.app.bluetooth.vcard.exception.VCardException a4) {
            a4.printStackTrace();
        }
    }
    
    public int getCount() {
        return this.mCards.size();
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardEntry getFirst() {
        return (com.navdy.hud.app.bluetooth.vcard.VCardEntry)this.mCards.get(0);
    }
    
    public java.util.ArrayList getList() {
        return this.mCards;
    }
}
