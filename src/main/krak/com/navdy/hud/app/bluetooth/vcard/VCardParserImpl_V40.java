package com.navdy.hud.app.bluetooth.vcard;

class VCardParserImpl_V40 extends com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V30 {
    public VCardParserImpl_V40() {
    }
    
    public VCardParserImpl_V40(int i) {
        super(i);
    }
    
    public static String unescapeCharacter(char a) {
        String s = null;
        int i = a;
        label2: {
            label0: {
                label1: {
                    if (a == 110) {
                        break label1;
                    }
                    if (i != 78) {
                        break label0;
                    }
                }
                s = "\n";
                break label2;
            }
            s = String.valueOf((char)i);
        }
        return s;
    }
    
    public static String unescapeText(String s) {
        StringBuilder a = new StringBuilder();
        int i = s.length();
        int i0 = 0;
        while(i0 < i) {
            int i1 = s.charAt(i0);
            label2: {
                label3: {
                    label4: {
                        if (i1 != 92) {
                            break label4;
                        }
                        if (i0 < i - 1) {
                            break label3;
                        }
                    }
                    a.append((char)i1);
                    break label2;
                }
                i0 = i0 + 1;
                int i2 = s.charAt(i0);
                label0: {
                    label1: {
                        if (i2 == 110) {
                            break label1;
                        }
                        if (i2 != 78) {
                            break label0;
                        }
                    }
                    a.append("\n");
                    break label2;
                }
                a.append((char)i2);
            }
            i0 = i0 + 1;
        }
        return a.toString();
    }
    
    protected java.util.Set getKnownPropertyNameSet() {
        return com.navdy.hud.app.bluetooth.vcard.VCardParser_V40.sKnownPropertyNameSet;
    }
    
    protected int getVersion() {
        return 2;
    }
    
    protected String getVersionString() {
        return "4.0";
    }
    
    protected String maybeUnescapeText(String s) {
        return com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V40.unescapeText(s);
    }
}
