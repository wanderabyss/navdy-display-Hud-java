package com.navdy.hud.app.bluetooth.obex;

final public class ClientSession extends com.navdy.hud.app.bluetooth.obex.ObexSession {
    final private static String TAG = "ClientSession";
    private byte[] mConnectionId;
    final private java.io.InputStream mInput;
    final private boolean mLocalSrmSupported;
    private int mMaxTxPacketSize;
    private boolean mObexConnected;
    private boolean mOpen;
    final private java.io.OutputStream mOutput;
    private boolean mRequestActive;
    final private com.navdy.hud.app.bluetooth.obex.ObexTransport mTransport;
    
    public ClientSession(com.navdy.hud.app.bluetooth.obex.ObexTransport a) {
        this.mConnectionId = null;
        this.mMaxTxPacketSize = 255;
        this.mInput = a.openInputStream();
        this.mOutput = a.openOutputStream();
        this.mOpen = true;
        this.mRequestActive = false;
        this.mLocalSrmSupported = a.isSrmSupported();
        this.mTransport = a;
    }
    
    public ClientSession(com.navdy.hud.app.bluetooth.obex.ObexTransport a, boolean b) {
        this.mConnectionId = null;
        this.mMaxTxPacketSize = 255;
        this.mInput = a.openInputStream();
        this.mOutput = a.openOutputStream();
        this.mOpen = true;
        this.mRequestActive = false;
        this.mLocalSrmSupported = b;
        this.mTransport = a;
    }
    
    private void setRequestActive() {
        synchronized(this) {
            if (this.mRequestActive) {
                throw new java.io.IOException("OBEX request is already being performed");
            }
            this.mRequestActive = true;
        }
        /*monexit(this)*/;
    }
    
    public void close() {
        this.mOpen = false;
        this.mInput.close();
        this.mOutput.close();
    }
    
    public com.navdy.hud.app.bluetooth.obex.HeaderSet connect(com.navdy.hud.app.bluetooth.obex.HeaderSet a) {
        int i = 0;
        this.ensureOpen();
        if (this.mObexConnected) {
            throw new java.io.IOException("Already connected to server");
        }
        this.setRequestActive();
        byte[] a0 = null;
        if (a == null) {
            i = 4;
        } else {
            if (a.nonce != null) {
                this.mChallengeDigest = new byte[16];
                System.arraycopy(a.nonce, 0, this.mChallengeDigest, 0, 16);
            }
            a0 = com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(a, false);
            i = 4 + a0.length;
        }
        byte[] a1 = new byte[i];
        int i0 = com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport);
        a1[0] = (byte)16;
        a1[1] = (byte)0;
        int i1 = (byte)(i0 >> 8);
        int i2 = (byte)i1;
        a1[2] = (byte)i2;
        int i3 = (byte)(i0 & 255);
        int i4 = (byte)i3;
        a1[3] = (byte)i4;
        if (a0 != null) {
            System.arraycopy(a0, 0, a1, 4, a0.length);
        }
        if (a1.length + 3 > 65534) {
            throw new java.io.IOException("Packet size exceeds max packet size for connect");
        }
        com.navdy.hud.app.bluetooth.obex.HeaderSet a2 = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        this.sendRequest(128, a1, a2, (com.navdy.hud.app.bluetooth.obex.PrivateInputStream)null, false);
        if (a2.responseCode == 160) {
            this.mObexConnected = true;
        }
        this.setRequestInactive();
        return a2;
    }
    
    public com.navdy.hud.app.bluetooth.obex.HeaderSet delete(com.navdy.hud.app.bluetooth.obex.HeaderSet a) {
        com.navdy.hud.app.bluetooth.obex.Operation a0 = this.put(a);
        a0.getResponseCode();
        com.navdy.hud.app.bluetooth.obex.HeaderSet a1 = a0.getReceivedHeader();
        a0.close();
        return a1;
    }
    
    public com.navdy.hud.app.bluetooth.obex.HeaderSet disconnect(com.navdy.hud.app.bluetooth.obex.HeaderSet a) {
        byte[] a0 = null;
        Throwable a1 = null;
        if (!this.mObexConnected) {
            throw new java.io.IOException("Not connected to the server");
        }
        this.setRequestActive();
        this.ensureOpen();
        if (a == null) {
            byte[] a2 = this.mConnectionId;
            a0 = null;
            if (a2 != null) {
                a0 = new byte[5];
                a0[0] = (byte)(-53);
                System.arraycopy(this.mConnectionId, 0, a0, 1, 4);
            }
        } else {
            if (a.nonce != null) {
                this.mChallengeDigest = new byte[16];
                System.arraycopy(a.nonce, 0, this.mChallengeDigest, 0, 16);
            }
            if (this.mConnectionId != null) {
                a.mConnectionID = new byte[4];
                System.arraycopy(this.mConnectionId, 0, a.mConnectionID, 0, 4);
            }
            a0 = com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(a, false);
            if (a0.length + 3 > this.mMaxTxPacketSize) {
                throw new java.io.IOException("Packet size exceeds max packet size");
            }
        }
        com.navdy.hud.app.bluetooth.obex.HeaderSet a3 = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        this.sendRequest(129, a0, a3, (com.navdy.hud.app.bluetooth.obex.PrivateInputStream)null, false);
        label0: synchronized(this) {
           {
                this.mObexConnected = false;
                this.setRequestInactive();
                }
            return a3;
        }
    }
    
    public void ensureOpen() {
        synchronized(this) {
            if (!this.mOpen) {
                throw new java.io.IOException("Connection closed");
            }
        }
        /*monexit(this)*/;
    }
    
    public com.navdy.hud.app.bluetooth.obex.Operation get(com.navdy.hud.app.bluetooth.obex.HeaderSet a) {
        if (!this.mObexConnected) {
            throw new java.io.IOException("Not connected to the server");
        }
        this.setRequestActive();
        this.ensureOpen();
        if (a != null) {
            if (a.nonce != null) {
                this.mChallengeDigest = new byte[16];
                System.arraycopy(a.nonce, 0, this.mChallengeDigest, 0, 16);
            }
        } else {
            a = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        }
        if (this.mConnectionId != null) {
            a.mConnectionID = new byte[4];
            System.arraycopy(this.mConnectionId, 0, a.mConnectionID, 0, 4);
        }
        if (this.mLocalSrmSupported) {
            a.setHeader(151, Byte.valueOf((byte)1));
        }
        return (com.navdy.hud.app.bluetooth.obex.Operation)new com.navdy.hud.app.bluetooth.obex.ClientOperation(this.mMaxTxPacketSize, this, a, true);
    }
    
    public long getConnectionID() {
        return (this.mConnectionId != null) ? com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToLong(this.mConnectionId) : -1L;
    }
    
    public boolean isSrmSupported() {
        return this.mLocalSrmSupported;
    }
    
    public com.navdy.hud.app.bluetooth.obex.Operation put(com.navdy.hud.app.bluetooth.obex.HeaderSet a) {
        if (!this.mObexConnected) {
            throw new java.io.IOException("Not connected to the server");
        }
        this.setRequestActive();
        this.ensureOpen();
        if (a != null) {
            if (a.nonce != null) {
                this.mChallengeDigest = new byte[16];
                System.arraycopy(a.nonce, 0, this.mChallengeDigest, 0, 16);
            }
        } else {
            a = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        }
        if (this.mConnectionId != null) {
            a.mConnectionID = new byte[4];
            System.arraycopy(this.mConnectionId, 0, a.mConnectionID, 0, 4);
        }
        if (this.mLocalSrmSupported) {
            a.setHeader(151, Byte.valueOf((byte)1));
        }
        return (com.navdy.hud.app.bluetooth.obex.Operation)new com.navdy.hud.app.bluetooth.obex.ClientOperation(this.mMaxTxPacketSize, this, a, false);
    }
    
    public boolean sendRequest(int i, byte[] a, com.navdy.hud.app.bluetooth.obex.HeaderSet a0, com.navdy.hud.app.bluetooth.obex.PrivateInputStream a1, boolean b) {
        boolean b0 = false;
        boolean b1 = false;
        boolean b2 = false;
        if (a != null && a.length + 3 > 65534) {
            throw new java.io.IOException("header too large ");
        }
        if (b) {
            if (i != 2) {
                if (i != 3) {
                    if (i != 131) {
                        b0 = false;
                        b1 = false;
                    } else {
                        b0 = false;
                        b1 = true;
                    }
                } else {
                    b0 = true;
                    b1 = false;
                }
            } else {
                b0 = true;
                b1 = false;
            }
        } else {
            b0 = false;
            b1 = false;
        }
        java.io.ByteArrayOutputStream a2 = new java.io.ByteArrayOutputStream();
        int i0 = (byte)i;
        a2.write(i0);
        if (a != null) {
            int i1 = (byte)(a.length + 3 >> 8);
            a2.write(i1);
            int i2 = (byte)(a.length + 3);
            a2.write(i2);
            a2.write(a);
        } else {
            a2.write(0);
            a2.write(3);
        }
        if (!b1) {
            this.mOutput.write(a2.toByteArray());
            this.mOutput.flush();
        }
        label6: {
            label0: {
                label2: {
                    label3: {
                        label1: {
                            byte[] a3 = null;
                            if (b0) {
                                break label1;
                            }
                            a0.responseCode = this.mInput.read();
                            int i3 = this.mInput.read() << 8 | this.mInput.read();
                            if (i3 > com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport)) {
                                throw new java.io.IOException("Packet received exceeds packet size limit");
                            }
                            if (i3 <= 3) {
                                break label1;
                            }
                            label5: {
                                label4: {
                                    if (i == 128) {
                                        break label4;
                                    }
                                    a3 = new byte[i3 - 3];
                                    int i4 = this.mInput.read(a3);
                                    while(i4 != i3 - 3) {
                                        i4 = i4 + this.mInput.read(a3, i4, a3.length - i4);
                                    }
                                    if (i == 255) {
                                        break label3;
                                    }
                                    break label5;
                                }
                                this.mInput.read();
                                this.mInput.read();
                                this.mMaxTxPacketSize = (this.mInput.read() << 8) + this.mInput.read();
                                if (this.mMaxTxPacketSize > 64512) {
                                    this.mMaxTxPacketSize = 64512;
                                }
                                if (this.mMaxTxPacketSize > com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxTxPacketSize(this.mTransport)) {
                                    android.util.Log.w("ClientSession", new StringBuilder().append("An OBEX packet size of ").append(this.mMaxTxPacketSize).append("was").append(" requested. Transport only allows: ").append(com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxTxPacketSize(this.mTransport)).append(" Lowering limit to this value.").toString());
                                    this.mMaxTxPacketSize = com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxTxPacketSize(this.mTransport);
                                }
                                if (i3 <= 7) {
                                    break label2;
                                }
                                a3 = new byte[i3 - 7];
                                int i5 = this.mInput.read(a3);
                                while(i5 != i3 - 7) {
                                    i5 = i5 + this.mInput.read(a3, i5, a3.length - i5);
                                }
                            }
                            byte[] a4 = com.navdy.hud.app.bluetooth.obex.ObexHelper.updateHeaderSet(a0, a3);
                            if (a1 != null && a4 != null) {
                                a1.writeBytes(a4, 1);
                            }
                            if (a0.mConnectionID != null) {
                                this.mConnectionId = new byte[4];
                                System.arraycopy(a0.mConnectionID, 0, this.mConnectionId, 0, 4);
                            }
                            if (a0.mAuthResp != null && !this.handleAuthResp(a0.mAuthResp)) {
                                this.setRequestInactive();
                                throw new java.io.IOException("Authentication Failed");
                            }
                            if (a0.responseCode != 193) {
                                break label1;
                            }
                            if (a0.mAuthChall == null) {
                                break label1;
                            }
                            if (this.handleAuthChall(a0)) {
                                break label0;
                            }
                        }
                        b2 = true;
                        break label6;
                    }
                    b2 = true;
                    break label6;
                }
                b2 = true;
                break label6;
            }
            a2.write(78);
            int i6 = (byte)(a0.mAuthResp.length + 3 >> 8);
            a2.write(i6);
            int i7 = (byte)(a0.mAuthResp.length + 3);
            a2.write(i7);
            a2.write(a0.mAuthResp);
            a0.mAuthChall = null;
            a0.mAuthResp = null;
            byte[] a5 = new byte[a2.size() - 3];
            System.arraycopy(a2.toByteArray(), 3, a5, 0, a5.length);
            b2 = this.sendRequest(i, a5, a0, a1, false);
        }
        return b2;
    }
    
    public void setAuthenticator(com.navdy.hud.app.bluetooth.obex.Authenticator a) {
        if (a == null) {
            throw new java.io.IOException("Authenticator may not be null");
        }
        this.mAuthenticator = a;
    }
    
    public void setConnectionID(long j) {
        if (j >= 0L && j <= 4294967295L) {
            this.mConnectionId = com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToByteArray(j);
            return;
        }
        throw new IllegalArgumentException("Connection ID is not in a valid range");
    }
    
    public com.navdy.hud.app.bluetooth.obex.HeaderSet setPath(com.navdy.hud.app.bluetooth.obex.HeaderSet a, boolean b, boolean b0) {
        if (!this.mObexConnected) {
            throw new java.io.IOException("Not connected to the server");
        }
        this.setRequestActive();
        this.ensureOpen();
        if (a != null) {
            if (a.nonce != null) {
                this.mChallengeDigest = new byte[16];
                System.arraycopy(a.nonce, 0, this.mChallengeDigest, 0, 16);
            }
        } else {
            a = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        }
        if (a.nonce != null) {
            this.mChallengeDigest = new byte[16];
            System.arraycopy(a.nonce, 0, this.mChallengeDigest, 0, 16);
        }
        if (this.mConnectionId != null) {
            a.mConnectionID = new byte[4];
            System.arraycopy(this.mConnectionId, 0, a.mConnectionID, 0, 4);
        }
        byte[] a0 = com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(a, false);
        int i = 2 + a0.length;
        if (i > this.mMaxTxPacketSize) {
            throw new java.io.IOException("Packet size exceeds max packet size");
        }
        int i0 = b ? 1 : 0;
        if (!b0) {
            i0 = i0 | 2;
        }
        byte[] a1 = new byte[i];
        int i1 = (byte)i0;
        int i2 = (byte)i1;
        a1[0] = (byte)i2;
        a1[1] = (byte)0;
        if (a != null) {
            System.arraycopy(a0, 0, a1, 2, a0.length);
        }
        com.navdy.hud.app.bluetooth.obex.HeaderSet a2 = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        this.sendRequest(133, a1, a2, (com.navdy.hud.app.bluetooth.obex.PrivateInputStream)null, false);
        this.setRequestInactive();
        return a2;
    }
    
    void setRequestInactive() {
        synchronized(this) {
            this.mRequestActive = false;
        }
        /*monexit(this)*/;
    }
}
