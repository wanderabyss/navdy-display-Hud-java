package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntry$AndroidCustomData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement {
    final private java.util.List mDataList;
    final private String mMimeType;
    
    public VCardEntry$AndroidCustomData(String s, java.util.List a) {
        this.mMimeType = s;
        this.mDataList = a;
    }
    
    public static com.navdy.hud.app.bluetooth.vcard.VCardEntry$AndroidCustomData constructAndroidCustomData(java.util.List a) {
        String s = null;
        java.util.List a0 = null;
        if (a != null) {
            if (a.size() >= 2) {
                int i = (a.size() >= 16) ? 16 : a.size();
                s = (String)a.get(0);
                a0 = a.subList(1, i);
            } else {
                s = (String)a.get(0);
                a0 = null;
            }
        } else {
            a0 = null;
            s = null;
        }
        return new com.navdy.hud.app.bluetooth.vcard.VCardEntry$AndroidCustomData(s, a0);
    }
    
    public void constructInsertOperation(java.util.List a, int i) {
        android.content.ContentProviderOperation$Builder a0 = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract$Data.CONTENT_URI);
        a0.withValueBackReference("raw_contact_id", i);
        a0.withValue("mimetype", this.mMimeType);
        Object a1 = a;
        int i0 = 0;
        while(i0 < this.mDataList.size()) {
            String s = (String)this.mDataList.get(i0);
            if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                a0.withValue(new StringBuilder().append("data").append(i0 + 1).toString(), s);
            }
            i0 = i0 + 1;
        }
        ((java.util.List)a1).add(a0.build());
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        if (this != a) {
            if (a instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry$AndroidCustomData) {
                com.navdy.hud.app.bluetooth.vcard.VCardEntry$AndroidCustomData a0 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry$AndroidCustomData)a;
                if (android.text.TextUtils.equals((CharSequence)this.mMimeType, (CharSequence)a0.mMimeType)) {
                    if (this.mDataList != null) {
                        int i = this.mDataList.size();
                        if (i != a0.mDataList.size()) {
                            b = false;
                        } else {
                            int i0 = 0;
                            while(true) {
                                if (i0 >= i) {
                                    b = true;
                                    break;
                                } else if (android.text.TextUtils.equals((CharSequence)this.mDataList.get(i0), (CharSequence)a0.mDataList.get(i0))) {
                                    i0 = i0 + 1;
                                } else {
                                    b = false;
                                    break;
                                }
                            }
                        }
                    } else {
                        b = a0.mDataList == null;
                    }
                } else {
                    b = false;
                }
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public java.util.List getDataList() {
        return this.mDataList;
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel getEntryLabel() {
        return com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel.ANDROID_CUSTOM;
    }
    
    public String getMimeType() {
        return this.mMimeType;
    }
    
    public int hashCode() {
        int i = (this.mMimeType == null) ? 0 : this.mMimeType.hashCode();
        if (this.mDataList != null) {
            Object a = this.mDataList.iterator();
            while(((java.util.Iterator)a).hasNext()) {
                String s = (String)((java.util.Iterator)a).next();
                i = i * 31 + ((s == null) ? 0 : s.hashCode());
            }
        }
        return i;
    }
    
    public boolean isEmpty() {
        boolean b = false;
        boolean b0 = android.text.TextUtils.isEmpty((CharSequence)this.mMimeType);
        label2: {
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if (this.mDataList == null) {
                        break label1;
                    }
                    if (this.mDataList.size() != 0) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    public String toString() {
        StringBuilder a = new StringBuilder();
        a.append(new StringBuilder().append("android-custom: ").append(this.mMimeType).append(", data: ").toString());
        a.append((this.mDataList != null) ? java.util.Arrays.toString(this.mDataList.toArray()) : "null");
        return a.toString();
    }
}
