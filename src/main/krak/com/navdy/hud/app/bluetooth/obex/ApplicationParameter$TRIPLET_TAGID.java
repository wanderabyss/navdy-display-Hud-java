package com.navdy.hud.app.bluetooth.obex;

public class ApplicationParameter$TRIPLET_TAGID {
    final public static byte FILTER_TAGID = (byte)6;
    final public static byte FORMAT_TAGID = (byte)7;
    final public static byte LISTSTARTOFFSET_TAGID = (byte)5;
    final public static byte MAXLISTCOUNT_TAGID = (byte)4;
    final public static byte NEWMISSEDCALLS_TAGID = (byte)9;
    final public static byte ORDER_TAGID = (byte)1;
    final public static byte PHONEBOOKSIZE_TAGID = (byte)8;
    final public static byte SEARCH_ATTRIBUTE_TAGID = (byte)3;
    final public static byte SEARCH_VALUE_TAGID = (byte)2;
    
    public ApplicationParameter$TRIPLET_TAGID() {
    }
}
