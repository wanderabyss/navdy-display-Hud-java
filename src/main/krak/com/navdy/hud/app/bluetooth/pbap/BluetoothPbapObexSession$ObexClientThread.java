package com.navdy.hud.app.bluetooth.pbap;

class BluetoothPbapObexSession$ObexClientThread extends Thread {
    final private static String TAG = "ObexClientThread";
    private com.navdy.hud.app.bluetooth.obex.ClientSession mClientSession;
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest mRequest;
    private volatile boolean mRunning;
    final com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession this$0;
    
    public BluetoothPbapObexSession$ObexClientThread(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession a) {
        super();
        this.this$0 = a;
        this.mRunning = true;
        this.mClientSession = null;
        this.mRequest = null;
    }
    
    static com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest access$000(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession$ObexClientThread a) {
        return a.mRequest;
    }
    
    private boolean connect() {
        boolean b = false;
        android.util.Log.d("ObexClientThread", "connect");
        label1: {
            label0: {
                try {
                    this.mClientSession = new com.navdy.hud.app.bluetooth.obex.ClientSession(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.access$300(this.this$0));
                    this.mClientSession.setAuthenticator((com.navdy.hud.app.bluetooth.obex.Authenticator)com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.access$400(this.this$0));
                    break label0;
                } catch(Throwable ignoredException) {
                }
                b = false;
                break label1;
            }
            com.navdy.hud.app.bluetooth.obex.HeaderSet a = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
            a.setHeader(70, com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.access$500());
            try {
                if (this.mClientSession.connect(a).getResponseCode() == 160) {
                    b = true;
                } else {
                    this.disconnect();
                    b = false;
                }
            } catch(Throwable ignoredException0) {
                b = false;
            }
        }
        return b;
    }
    
    private void disconnect() {
        android.util.Log.d("ObexClientThread", "disconnect");
        if (this.mClientSession != null) {
            try {
                this.mClientSession.disconnect((com.navdy.hud.app.bluetooth.obex.HeaderSet)null);
                this.mClientSession.close();
            } catch(Throwable ignoredException) {
            }
        }
    }
    
    public boolean isRunning() {
        return this.mRunning;
    }
    
    public void run() {
        Throwable a = null;
        super.run();
        boolean b = this.connect();
        label0: {
            if (b) {
                com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.access$200(this.this$0).obtainMessage(100).sendToTarget();
                while(this.mRunning) {
                    label1: synchronized(this) {
                            if (this.mRequest == null) {
                                try {
                                    (this).wait();
                                } catch(InterruptedException ignoredException) {
                                    break label1;
                                }
                            }
                        if (this.mRunning && this.mRequest != null) {
                            try {
                                this.mRequest.execute(this.mClientSession);
                            } catch(Throwable ignoredException0) {
                                this.mRunning = false;
                            }
                            if (this.mRequest.isSuccess()) {
                                com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.access$200(this.this$0).obtainMessage(103, this.mRequest).sendToTarget();
                            } else {
                                com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.access$200(this.this$0).obtainMessage(104, this.mRequest).sendToTarget();
                            }
                        }
                        this.mRequest = null;
                        continue;
                    }
                    try {
                        this.mRunning = false;
                        /*monexit(this)*/;
                        break;
                    } catch(IllegalMonitorStateException | NullPointerException a1) {
                        a = a1;
                        break label0;
                    }
                }
                this.disconnect();
                com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.access$200(this.this$0).obtainMessage(102).sendToTarget();
            } else {
                com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession.access$200(this.this$0).obtainMessage(101).sendToTarget();
            }
            return;
        }
        while(true) {
            try {
                /*monexit(this)*/;
            } catch(IllegalMonitorStateException | NullPointerException a2) {
                Throwable a3 = a2;
                a = a3;
                continue;
            }
            throw a;
        }
    }
    
    public boolean schedule(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest a) {
        boolean b = false;
        synchronized(this) {
            android.util.Log.d("ObexClientThread", new StringBuilder().append("schedule: ").append((a).getClass().getSimpleName()).toString());
            if (this.mRequest == null) {
                this.mRequest = a;
                (this).notify();
                b = true;
            } else {
                b = false;
            }
        }
        /*monexit(this)*/;
        return b;
    }
}
