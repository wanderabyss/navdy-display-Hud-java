package com.navdy.hud.app.bluetooth.vcard;

abstract public interface VCardPhoneNumberTranslationCallback {
    abstract public String onValueReceived(String arg, int arg0, String arg1, boolean arg2);
}
