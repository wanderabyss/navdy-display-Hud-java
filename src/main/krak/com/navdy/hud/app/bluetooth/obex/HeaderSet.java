package com.navdy.hud.app.bluetooth.obex;

final public class HeaderSet {
    final public static int APPLICATION_PARAMETER = 76;
    final public static int AUTH_CHALLENGE = 77;
    final public static int AUTH_RESPONSE = 78;
    final public static int BODY = 72;
    final public static int CONNECTION_ID = 203;
    final public static int COUNT = 192;
    final public static int DESCRIPTION = 5;
    final public static int END_OF_BODY = 73;
    final public static int HTTP = 71;
    final public static int LENGTH = 195;
    final public static int NAME = 1;
    final public static int OBJECT_CLASS = 79;
    final public static int SINGLE_RESPONSE_MODE = 151;
    final public static int SINGLE_RESPONSE_MODE_PARAMETER = 152;
    final public static int TARGET = 70;
    final public static int TIME_4_BYTE = 196;
    final public static int TIME_ISO_8601 = 68;
    final public static int TYPE = 66;
    final public static int WHO = 74;
    private byte[] mAppParam;
    public byte[] mAuthChall;
    public byte[] mAuthResp;
    private java.util.Calendar mByteTime;
    private Byte[] mByteUserDefined;
    public byte[] mConnectionID;
    private Long mCount;
    private String mDescription;
    private boolean mEmptyName;
    private byte[] mHttpHeader;
    private Long[] mIntegerUserDefined;
    private java.util.Calendar mIsoTime;
    private Long mLength;
    private String mName;
    private byte[] mObjectClass;
    private java.security.SecureRandom mRandom;
    private byte[][] mSequenceUserDefined;
    private Byte mSingleResponseMode;
    private Byte mSrmParam;
    private byte[] mTarget;
    private String mType;
    private String[] mUnicodeUserDefined;
    private byte[] mWho;
    byte[] nonce;
    public int responseCode;
    
    public HeaderSet() {
        this.mRandom = null;
        this.mUnicodeUserDefined = new String[16];
        this.mSequenceUserDefined = new byte[16][];
        this.mByteUserDefined = new Byte[16];
        this.mIntegerUserDefined = new Long[16];
        this.responseCode = -1;
    }
    
    public void createAuthenticationChallenge(String s, boolean b, boolean b0) {
        this.nonce = new byte[16];
        if (this.mRandom == null) {
            this.mRandom = new java.security.SecureRandom();
        }
        int i = 0;
        while(i < 16) {
            byte[] a = this.nonce;
            int i0 = (byte)this.mRandom.nextInt();
            int i1 = (byte)i0;
            a[i] = (byte)i1;
            i = i + 1;
        }
        this.mAuthChall = com.navdy.hud.app.bluetooth.obex.ObexHelper.computeAuthenticationChallenge(this.nonce, s, b0, b);
    }
    
    public boolean getEmptyNameHeader() {
        return this.mEmptyName;
    }
    
    public Object getHeader(int i) {
        Object a = null;
        switch(i) {
            case 203: {
                a = this.mConnectionID;
                break;
            }
            case 196: {
                a = this.mByteTime;
                break;
            }
            case 195: {
                a = this.mLength;
                break;
            }
            case 192: {
                a = this.mCount;
                break;
            }
            case 152: {
                a = this.mSrmParam;
                break;
            }
            case 151: {
                a = this.mSingleResponseMode;
                break;
            }
            case 79: {
                a = this.mObjectClass;
                break;
            }
            case 76: {
                a = this.mAppParam;
                break;
            }
            case 74: {
                a = this.mWho;
                break;
            }
            case 71: {
                a = this.mHttpHeader;
                break;
            }
            case 70: {
                a = this.mTarget;
                break;
            }
            case 68: {
                a = this.mIsoTime;
                break;
            }
            case 66: {
                a = this.mType;
                break;
            }
            case 5: {
                a = this.mDescription;
                break;
            }
            case 1: {
                a = this.mName;
                break;
            }
            default: {
                {
                    label4: {
                        if (i < 48) {
                            break label4;
                        }
                        if (i > 63) {
                            break label4;
                        }
                        a = this.mUnicodeUserDefined[i - 48];
                        break;
                    }
                    label3: {
                        if (i < 112) {
                            break label3;
                        }
                        if (i > 127) {
                            break label3;
                        }
                        a = this.mSequenceUserDefined[i - 112];
                        break;
                    }
                    label2: {
                        if (i < 176) {
                            break label2;
                        }
                        if (i > 191) {
                            break label2;
                        }
                        a = this.mByteUserDefined[i - 176];
                        break;
                    }
                    label0: {
                        label1: {
                            if (i < 240) {
                                break label1;
                            }
                            if (i <= 255) {
                                break label0;
                            }
                        }
                        throw new IllegalArgumentException("Invalid Header Identifier");
                    }
                    a = this.mIntegerUserDefined[i - 240];
                }
            }
        }
        return a;
    }
    
    public int[] getHeaderList() {
        int[] a = null;
        java.io.ByteArrayOutputStream a0 = new java.io.ByteArrayOutputStream();
        if (this.mCount != null) {
            a0.write(192);
        }
        if (this.mName != null) {
            a0.write(1);
        }
        if (this.mType != null) {
            a0.write(66);
        }
        if (this.mLength != null) {
            a0.write(195);
        }
        if (this.mIsoTime != null) {
            a0.write(68);
        }
        if (this.mByteTime != null) {
            a0.write(196);
        }
        if (this.mDescription != null) {
            a0.write(5);
        }
        if (this.mTarget != null) {
            a0.write(70);
        }
        if (this.mHttpHeader != null) {
            a0.write(71);
        }
        if (this.mWho != null) {
            a0.write(74);
        }
        if (this.mAppParam != null) {
            a0.write(76);
        }
        if (this.mObjectClass != null) {
            a0.write(79);
        }
        if (this.mSingleResponseMode != null) {
            a0.write(151);
        }
        if (this.mSrmParam != null) {
            a0.write(152);
        }
        int i = 48;
        while(i < 64) {
            if (this.mUnicodeUserDefined[i - 48] != null) {
                a0.write(i);
            }
            i = i + 1;
        }
        int i0 = 112;
        while(i0 < 128) {
            if (this.mSequenceUserDefined[i0 - 112] != null) {
                a0.write(i0);
            }
            i0 = i0 + 1;
        }
        int i1 = 176;
        while(i1 < 192) {
            if (this.mByteUserDefined[i1 - 176] != null) {
                a0.write(i1);
            }
            i1 = i1 + 1;
        }
        int i2 = 240;
        while(i2 < 256) {
            if (this.mIntegerUserDefined[i2 - 240] != null) {
                a0.write(i2);
            }
            i2 = i2 + 1;
        }
        byte[] a1 = a0.toByteArray();
        a0.close();
        label2: {
            label0: {
                label1: {
                    if (a1 == null) {
                        break label1;
                    }
                    if (a1.length != 0) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = new int[a1.length];
            int i3 = 0;
            while(i3 < a1.length) {
                int i4 = a1[i3];
                a[i3] = i4 & 255;
                i3 = i3 + 1;
            }
        }
        return a;
    }
    
    public int getResponseCode() {
        if (this.responseCode == -1) {
            throw new java.io.IOException("May not be called on a server");
        }
        return this.responseCode;
    }
    
    public void setEmptyNameHeader() {
        this.mName = null;
        this.mEmptyName = true;
    }
    
    public void setHeader(int i, Object a) {
        switch(i) {
            case 196: {
                if (a != null && !(a instanceof java.util.Calendar)) {
                    throw new IllegalArgumentException("Time 4 Byte must be a Calendar");
                }
                this.mByteTime = (java.util.Calendar)a;
                break;
            }
            case 195: {
                if (a instanceof Long) {
                    Long a0 = (Long)a;
                    long j = a0.longValue();
                    int i0 = (j < 0L) ? -1 : (j == 0L) ? 0 : 1;
                    label10: {
                        label11: {
                            if (i0 < 0) {
                                break label11;
                            }
                            if (j <= 4294967295L) {
                                break label10;
                            }
                        }
                        throw new IllegalArgumentException("Length must be between 0 and 0xFFFFFFFF");
                    }
                    this.mLength = (Long)a0;
                    break;
                } else {
                    if (a != null) {
                        throw new IllegalArgumentException("Length must be a Long");
                    }
                    this.mLength = null;
                    break;
                }
            }
            case 192: {
                if (a instanceof Long) {
                    Long a1 = (Long)a;
                    long j0 = a1.longValue();
                    int i1 = (j0 < 0L) ? -1 : (j0 == 0L) ? 0 : 1;
                    label8: {
                        label9: {
                            if (i1 < 0) {
                                break label9;
                            }
                            if (j0 <= 4294967295L) {
                                break label8;
                            }
                        }
                        throw new IllegalArgumentException("Count must be between 0 and 0xFFFFFFFF");
                    }
                    this.mCount = (Long)a1;
                    break;
                } else {
                    if (a != null) {
                        throw new IllegalArgumentException("Count must be a Long");
                    }
                    this.mCount = null;
                    break;
                }
            }
            case 152: {
                if (a != null) {
                    if (!(a instanceof Byte)) {
                        throw new IllegalArgumentException("Single Response Mode Parameter must be a Byte");
                    }
                    this.mSrmParam = (Byte)a;
                    break;
                } else {
                    this.mSrmParam = null;
                    break;
                }
            }
            case 151: {
                if (a != null) {
                    if (!(a instanceof Byte)) {
                        throw new IllegalArgumentException("Single Response Mode must be a Byte");
                    }
                    this.mSingleResponseMode = (Byte)a;
                    break;
                } else {
                    this.mSingleResponseMode = null;
                    break;
                }
            }
            case 79: {
                if (a != null) {
                    if (!(a instanceof byte[])) {
                        throw new IllegalArgumentException("Object Class must be a byte array");
                    }
                    byte[] a2 = (byte[])a;
                    this.mObjectClass = new byte[a2.length];
                    System.arraycopy(a2, 0, this.mObjectClass, 0, this.mObjectClass.length);
                    break;
                } else {
                    this.mObjectClass = null;
                    break;
                }
            }
            case 76: {
                if (a != null) {
                    if (!(a instanceof byte[])) {
                        throw new IllegalArgumentException("Application Parameter must be a byte array");
                    }
                    byte[] a3 = (byte[])a;
                    this.mAppParam = new byte[a3.length];
                    System.arraycopy(a3, 0, this.mAppParam, 0, this.mAppParam.length);
                    break;
                } else {
                    this.mAppParam = null;
                    break;
                }
            }
            case 74: {
                if (a != null) {
                    if (!(a instanceof byte[])) {
                        throw new IllegalArgumentException("WHO must be a byte array");
                    }
                    byte[] a4 = (byte[])a;
                    this.mWho = new byte[a4.length];
                    System.arraycopy(a4, 0, this.mWho, 0, this.mWho.length);
                    break;
                } else {
                    this.mWho = null;
                    break;
                }
            }
            case 71: {
                if (a != null) {
                    if (!(a instanceof byte[])) {
                        throw new IllegalArgumentException("HTTP must be a byte array");
                    }
                    byte[] a5 = (byte[])a;
                    this.mHttpHeader = new byte[a5.length];
                    System.arraycopy(a5, 0, this.mHttpHeader, 0, this.mHttpHeader.length);
                    break;
                } else {
                    this.mHttpHeader = null;
                    break;
                }
            }
            case 70: {
                if (a != null) {
                    if (!(a instanceof byte[])) {
                        throw new IllegalArgumentException("Target must be a byte array");
                    }
                    byte[] a6 = (byte[])a;
                    this.mTarget = new byte[a6.length];
                    System.arraycopy(a6, 0, this.mTarget, 0, this.mTarget.length);
                    break;
                } else {
                    this.mTarget = null;
                    break;
                }
            }
            case 68: {
                if (a != null && !(a instanceof java.util.Calendar)) {
                    throw new IllegalArgumentException("Time ISO 8601 must be a Calendar");
                }
                this.mIsoTime = (java.util.Calendar)a;
                break;
            }
            case 66: {
                if (a != null && !(a instanceof String)) {
                    throw new IllegalArgumentException("Type must be a String");
                }
                this.mType = (String)a;
                break;
            }
            case 5: {
                if (a != null && !(a instanceof String)) {
                    throw new IllegalArgumentException("Description must be a String");
                }
                this.mDescription = (String)a;
                break;
            }
            case 1: {
                if (a != null && !(a instanceof String)) {
                    throw new IllegalArgumentException("Name must be a String");
                }
                this.mEmptyName = false;
                this.mName = (String)a;
                break;
            }
            default: {
                {
                    label7: {
                        if (i < 48) {
                            break label7;
                        }
                        if (i > 63) {
                            break label7;
                        }
                        if (a != null && !(a instanceof String)) {
                            throw new IllegalArgumentException("Unicode String User Defined must be a String");
                        }
                        this.mUnicodeUserDefined[i - 48] = (String)a;
                        break;
                    }
                    label6: {
                        if (i < 112) {
                            break label6;
                        }
                        if (i > 127) {
                            break label6;
                        }
                        if (a != null) {
                            if (!(a instanceof byte[])) {
                                throw new IllegalArgumentException("Byte Sequence User Defined must be a byte array");
                            }
                            byte[][] a7 = this.mSequenceUserDefined;
                            int i2 = i - 112;
                            byte[] a8 = (byte[])a;
                            a7[i2] = new byte[a8.length];
                            System.arraycopy(a8, 0, this.mSequenceUserDefined[i - 112], 0, this.mSequenceUserDefined[i - 112].length);
                            break;
                        } else {
                            this.mSequenceUserDefined[i - 112] = null;
                            break;
                        }
                    }
                    label4: {
                        label5: {
                            if (i < 176) {
                                break label5;
                            }
                            if (i <= 191) {
                                break label4;
                            }
                        }
                        label2: {
                            label3: {
                                if (i < 240) {
                                    break label3;
                                }
                                if (i <= 255) {
                                    break label2;
                                }
                            }
                            throw new IllegalArgumentException("Invalid Header Identifier");
                        }
                        if (a instanceof Long) {
                            Long a9 = (Long)a;
                            long j1 = a9.longValue();
                            int i3 = (j1 < 0L) ? -1 : (j1 == 0L) ? 0 : 1;
                            label0: {
                                label1: {
                                    if (i3 < 0) {
                                        break label1;
                                    }
                                    if (j1 <= 4294967295L) {
                                        break label0;
                                    }
                                }
                                throw new IllegalArgumentException("Integer User Defined must be between 0 and 0xFFFFFFFF");
                            }
                            this.mIntegerUserDefined[i - 240] = (Long)a9;
                            break;
                        } else {
                            if (a != null) {
                                throw new IllegalArgumentException("Integer User Defined must be a Long");
                            }
                            this.mIntegerUserDefined[i - 240] = null;
                            break;
                        }
                    }
                    if (a != null && !(a instanceof Byte)) {
                        throw new IllegalArgumentException("ByteUser Defined must be a Byte");
                    }
                    this.mByteUserDefined[i - 176] = (Byte)a;
                }
            }
        }
    }
}
