package com.navdy.hud.app.bluetooth.pbap.utils;

final public class ObexTime {
    private java.util.Date mDate;
    
    public ObexTime(String s) {
        java.util.regex.Matcher a = java.util.regex.Pattern.compile("(\\d{4})(\\d{2})(\\d{2})T(\\d{2})(\\d{2})(\\d{2})(([+-])(\\d{2})(\\d{2}))?").matcher((CharSequence)s);
        if (a.matches()) {
            java.util.Calendar a0 = java.util.Calendar.getInstance();
            a0.set(Integer.parseInt(a.group(1)), Integer.parseInt(a.group(2)) - 1, Integer.parseInt(a.group(3)), Integer.parseInt(a.group(4)), Integer.parseInt(a.group(5)), Integer.parseInt(a.group(6)));
            if (a.group(7) != null) {
                int i = (Integer.parseInt(a.group(9)) * 60 + Integer.parseInt(a.group(10))) * 60 * 1000;
                if (a.group(8).equals("-")) {
                    i = -i;
                }
                java.util.TimeZone a1 = java.util.TimeZone.getTimeZone("UTC");
                a1.setRawOffset(i);
                a0.setTimeZone(a1);
            }
            this.mDate = a0.getTime();
        }
    }
    
    public ObexTime(java.util.Date a) {
        this.mDate = a;
    }
    
    public java.util.Date getTime() {
        return this.mDate;
    }
    
    public String toString() {
        String s = null;
        if (this.mDate != null) {
            java.util.Calendar a = java.util.Calendar.getInstance();
            a.setTime(this.mDate);
            java.util.Locale a0 = java.util.Locale.US;
            Object[] a1 = new Object[6];
            a1[0] = Integer.valueOf(a.get(1));
            a1[1] = Integer.valueOf(a.get(2) + 1);
            a1[2] = Integer.valueOf(a.get(5));
            a1[3] = Integer.valueOf(a.get(11));
            a1[4] = Integer.valueOf(a.get(12));
            a1[5] = Integer.valueOf(a.get(13));
            s = String.format(a0, "%04d%02d%02dT%02d%02d%02d", a1);
        } else {
            s = null;
        }
        return s;
    }
}
