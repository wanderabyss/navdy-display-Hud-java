package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;

final class HerePlacesManager$10 implements Runnable {
    final String val$autoCompleteSearch;
    final int val$maxResults;
    final java.util.List val$results;
    
    HerePlacesManager$10(java.util.List a, int i, String s) {
        super();
        this.val$results = a;
        this.val$maxResults = i;
        this.val$autoCompleteSearch = s;
    }
    
    public void run() {
        try {
            java.util.List a = this.val$results;
            if (this.val$results.size() > this.val$maxResults) {
                a = this.val$results.subList(0, this.val$maxResults);
            }
            com.navdy.service.library.events.places.AutoCompleteResponse a0 = new com.navdy.service.library.events.places.AutoCompleteResponse(this.val$autoCompleteSearch, com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, (String)null, a);
            com.navdy.hud.app.maps.MapsEventHandler.getInstance().sendEventToClient((com.squareup.wire.Message)a0);
            com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("[autocomplete] returned:").append(a.size()).toString());
        } catch(Throwable a1) {
            com.navdy.hud.app.maps.here.HerePlacesManager.access$000().e(a1);
            com.navdy.hud.app.maps.here.HerePlacesManager.access$300(this.val$autoCompleteSearch, com.navdy.service.library.events.RequestStatus.REQUEST_UNKNOWN_ERROR, com.navdy.hud.app.maps.here.HerePlacesManager.access$200().getString(R.string.autoCompleteSearch));
        }
    }
}
