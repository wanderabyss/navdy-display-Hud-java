package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;

class HereRouteCalculator$1$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereRouteCalculator$1 this$1;
    final java.util.List val$results;
    final com.here.android.mpa.routing.RoutingError val$routingError;
    
    HereRouteCalculator$1$1(com.navdy.hud.app.maps.here.HereRouteCalculator$1 a, com.here.android.mpa.routing.RoutingError a0, java.util.List a1) {
        super();
        this.this$1 = a;
        this.val$routingError = a0;
        this.val$results = a1;
    }
    
    public void run() {
        label1: {
            Throwable a = null;
            label0: {
                long j = 0L;
                label7: {
                    java.util.ArrayList a0 = null;
                    String[] a1 = null;
                    int i = 0;
                    Object a2 = null;
                    label8: {
                        try {
                            j = android.os.SystemClock.elapsedRealtime();
                            com.here.android.mpa.routing.RoutingError a3 = this.val$routingError;
                            com.here.android.mpa.routing.RoutingError a4 = com.here.android.mpa.routing.RoutingError.NONE;
                            label4: {
                                label5: {
                                    label6: {
                                        if (a3 == a4) {
                                            break label6;
                                        }
                                        if (this.val$routingError != com.here.android.mpa.routing.RoutingError.VIOLATES_OPTIONS) {
                                            break label5;
                                        }
                                    }
                                    if (this.val$results.size() > 0) {
                                        break label4;
                                    }
                                }
                                int i0 = (this.val$results == null) ? -1 : this.val$results.size();
                                com.navdy.hud.app.maps.here.HereRouteCalculator.access$200(this.this$1.this$0).e(new StringBuilder().append(com.navdy.hud.app.maps.here.HereRouteCalculator.access$100(this.this$1.this$0)).append("got error:").append(this.val$routingError).append(" results=").append(i0).toString());
                                this.this$1.val$listener.error(this.val$routingError, (Throwable)null);
                                break label7;
                            }
                            this.this$1.val$listener.preSuccess();
                            a0 = new java.util.ArrayList(this.val$results.size());
                            boolean b = this.this$1.val$allowCancellation;
                            String s = null;
                            if (b) {
                                com.navdy.service.library.events.navigation.NavigationRouteRequest a5 = this.this$1.val$request;
                                s = null;
                                if (a5 != null) {
                                    s = this.this$1.val$request.requestId;
                                }
                            }
                            a1 = com.navdy.hud.app.maps.here.HereRouteViaGenerator.generateVia(this.val$results, s);
                            java.util.Iterator a6 = this.val$results.iterator();
                            i = 0;
                            a2 = a6;
                            break label8;
                        } catch(Throwable a7) {
                            a = a7;
                            break label0;
                        }
                    }
                    while(true) {
                        com.here.android.mpa.routing.RouteResult a8 = null;
                        com.here.android.mpa.routing.Route a9 = null;
                        String s0 = null;
                        int i1 = 0;
                        try {
                            if (!((java.util.Iterator)a2).hasNext()) {
                                break;
                            }
                            a8 = (com.here.android.mpa.routing.RouteResult)((java.util.Iterator)a2).next();
                            boolean b0 = this.this$1.val$allowCancellation;
                            label3: {
                                if (!b0) {
                                    break label3;
                                }
                                if (com.navdy.hud.app.maps.here.HereRouteCalculator.access$000(this.this$1.this$0, this.this$1.val$request)) {
                                    break label1;
                                }
                            }
                            a9 = a8.getRoute();
                            s0 = java.util.UUID.randomUUID().toString();
                            i1 = i + 1;
                        } catch(Throwable a10) {
                            a = a10;
                            break label0;
                        }
                        String s1 = a1[i];
                        try {
                            if (android.text.TextUtils.isEmpty((CharSequence)s1)) {
                                android.content.res.Resources a11 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
                                Object[] a12 = new Object[1];
                                a12[0] = Integer.valueOf(i1);
                                s1 = a11.getString(R.string.route_num, a12);
                            }
                            java.util.EnumSet a13 = a8.getViolatedOptions();
                            if (a13 != null && a13.size() > 0) {
                                Object a14 = a13.iterator();
                                while(((java.util.Iterator)a14).hasNext()) {
                                    com.here.android.mpa.routing.RouteResult$ViolatedOption a15 = (com.here.android.mpa.routing.RouteResult$ViolatedOption)((java.util.Iterator)a14).next();
                                    com.navdy.hud.app.maps.here.HereRouteCalculator.access$200(this.this$1.this$0).v(new StringBuilder().append(com.navdy.hud.app.maps.here.HereRouteCalculator.access$100(this.this$1.this$0)).append(" violated options [").append(a15.name()).append("]").toString());
                                }
                            }
                            com.navdy.service.library.events.navigation.NavigationRouteResult a16 = (this.this$1.val$request == null) ? this.this$1.this$0.getRouteResultFromRoute(s0, a9, s1, (String)null, (String)null, this.this$1.val$calculatePolyline) : this.this$1.this$0.getRouteResultFromRoute(s0, a9, s1, this.this$1.val$request.label, this.this$1.val$request.streetAddress, this.this$1.val$calculatePolyline);
                            a0.add(a16);
                            if (this.this$1.val$cache) {
                                com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a17 = new com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo(a9, this.this$1.val$request, a16, this.this$1.val$factoringInTraffic, this.this$1.val$startPoint);
                                com.navdy.hud.app.maps.here.HereRouteCache.getInstance().addRoute(s0, a17);
                            }
                            if (com.navdy.hud.app.maps.here.HereRouteCalculator.access$200(this.this$1.this$0).isLoggable(2)) {
                                com.navdy.hud.app.maps.here.HereMapUtil.printRouteDetails(a9, (this.this$1.val$request != null) ? this.this$1.val$request.streetAddress : "", this.this$1.val$request, (StringBuilder)null);
                            }
                            if (com.navdy.hud.app.maps.MapSettings.isGenerateRouteIcons()) {
                                com.navdy.hud.app.maps.here.HereMapUtil.generateRouteIcons(s0, this.this$1.val$request.label, a9);
                            }
                            i = i1;
                        } catch(Throwable a18) {
                            a = a18;
                            break label0;
                        }
                    }
                    try {
                        boolean b1 = this.this$1.val$allowCancellation;
                        label2: {
                            if (!b1) {
                                break label2;
                            }
                            if (com.navdy.hud.app.maps.here.HereRouteCalculator.access$000(this.this$1.this$0, this.this$1.val$request)) {
                                break label1;
                            }
                        }
                        this.this$1.val$listener.postSuccess(a0);
                    } catch(Throwable a19) {
                        a = a19;
                        break label0;
                    }
                }
                try {
                    long j0 = android.os.SystemClock.elapsedRealtime();
                    com.navdy.hud.app.maps.here.HereRouteCalculator.access$200(this.this$1.this$0).v(new StringBuilder().append(com.navdy.hud.app.maps.here.HereRouteCalculator.access$100(this.this$1.this$0)).append("handleSearchRequest- time-2=").append(j0 - j).toString());
                } catch(Throwable a20) {
                    a = a20;
                    break label0;
                }
                break label1;
            }
            com.navdy.hud.app.maps.here.HereRouteCalculator.access$200(this.this$1.this$0).e(com.navdy.hud.app.maps.here.HereRouteCalculator.access$100(this.this$1.this$0), a);
            this.this$1.val$listener.error((com.here.android.mpa.routing.RoutingError)null, a);
        }
    }
}
