package com.navdy.hud.app.maps.here;

class HereMapsManager$3 implements com.here.android.mpa.common.PositioningManager$OnPositionChangedListener {
    final com.navdy.hud.app.maps.here.HereMapsManager this$0;
    
    HereMapsManager$3(com.navdy.hud.app.maps.here.HereMapsManager a) {
        super();
        this.this$0 = a;
    }
    
    public void onPositionFixChanged(com.here.android.mpa.common.PositioningManager$LocationMethod a, com.here.android.mpa.common.PositioningManager$LocationStatus a0) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereMapsManager$3$2(this, a, a0), 18);
    }
    
    public void onPositionUpdated(com.here.android.mpa.common.PositioningManager$LocationMethod a, com.here.android.mpa.common.GeoPosition a0, boolean b) {
        if (a0 != null) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereMapsManager$3$1(this, a0, a, b), 18);
        }
    }
}
