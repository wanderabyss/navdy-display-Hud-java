package com.navdy.hud.app.maps.here;

class HereLocationFixManager$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereLocationFixManager this$0;
    
    HereLocationFixManager$1(com.navdy.hud.app.maps.here.HereLocationFixManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        label3: {
            Throwable a = null;
            label0: {
                label2: try {
                    long j = android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HereLocationFixManager.access$000(this.this$0);
                    if (com.navdy.hud.app.maps.here.HereLocationFixManager.access$100(this.this$0)) {
                        if (j < 3000L) {
                            if (com.navdy.hud.app.maps.here.HereLocationFixManager.access$200().isLoggable(2)) {
                                com.navdy.hud.app.maps.here.HereLocationFixManager.access$200().v(new StringBuilder().append("still has location fix [").append(j).append("]").toString());
                            }
                        } else {
                            com.navdy.hud.app.maps.here.HereLocationFixManager.access$200().i(new StringBuilder().append("lost location fix[").append(j).append("]").toString());
                            com.navdy.hud.app.maps.here.HereLocationFixManager.access$102(this.this$0, false);
                            com.navdy.hud.app.maps.here.HereLocationFixManager.access$300(this.this$0).post(new com.navdy.hud.app.maps.MapEvents$LocationFix(false, false, false));
                        }
                    } else {
                        android.location.Location a0 = com.navdy.hud.app.maps.here.HereLocationFixManager.access$400(this.this$0);
                        label1: {
                            if (a0 == null) {
                                break label1;
                            }
                            if (j > 3000L) {
                                break label1;
                            }
                            com.navdy.hud.app.maps.here.HereLocationFixManager.access$200().v(new StringBuilder().append("got location fix [").append(j).append("] ").append(com.navdy.hud.app.maps.here.HereLocationFixManager.access$400(this.this$0)).toString());
                            com.navdy.hud.app.maps.here.HereLocationFixManager.access$102(this.this$0, true);
                            com.navdy.hud.app.maps.MapEvents$LocationFix a1 = this.this$0.getLocationFixEvent();
                            com.navdy.hud.app.maps.here.HereLocationFixManager.access$300(this.this$0).post(a1);
                            break label2;
                        }
                        if (com.navdy.hud.app.maps.here.HereLocationFixManager.access$200().isLoggable(2)) {
                            com.navdy.hud.app.maps.here.HereLocationFixManager.access$200().v(new StringBuilder().append("still don't have location fix [").append(j).append("]").toString());
                        }
                        if (com.navdy.hud.app.maps.here.HereLocationFixManager.access$500(this.this$0)) {
                            com.navdy.hud.app.maps.here.HereLocationFixManager.access$300(this.this$0).post(new com.navdy.hud.app.maps.MapEvents$LocationFix(false, false, false));
                        }
                    }
                } catch(Throwable a2) {
                    a = a2;
                    break label0;
                }
                com.navdy.hud.app.maps.here.HereLocationFixManager.access$700(this.this$0).postDelayed(com.navdy.hud.app.maps.here.HereLocationFixManager.access$600(this.this$0), 1000L);
                com.navdy.hud.app.maps.here.HereLocationFixManager.access$502(this.this$0, false);
                break label3;
            }
            try {
                com.navdy.hud.app.maps.here.HereLocationFixManager.access$200().e(a);
            } catch(Throwable a3) {
                com.navdy.hud.app.maps.here.HereLocationFixManager.access$700(this.this$0).postDelayed(com.navdy.hud.app.maps.here.HereLocationFixManager.access$600(this.this$0), 1000L);
                com.navdy.hud.app.maps.here.HereLocationFixManager.access$502(this.this$0, false);
                throw a3;
            }
            com.navdy.hud.app.maps.here.HereLocationFixManager.access$700(this.this$0).postDelayed(com.navdy.hud.app.maps.here.HereLocationFixManager.access$600(this.this$0), 1000L);
            com.navdy.hud.app.maps.here.HereLocationFixManager.access$502(this.this$0, false);
        }
    }
}
