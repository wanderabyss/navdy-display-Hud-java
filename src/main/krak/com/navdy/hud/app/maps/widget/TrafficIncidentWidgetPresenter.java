package com.navdy.hud.app.maps.widget;
import com.navdy.hud.app.R;

public class TrafficIncidentWidgetPresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    private static android.os.Handler handler;
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.squareup.otto.Bus bus;
    private boolean registered;
    private android.widget.TextView textView;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger("TrafficIncidentWidget");
        handler = new android.os.Handler(android.os.Looper.getMainLooper());
    }
    
    public TrafficIncidentWidgetPresenter() {
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    }
    
    static android.widget.TextView access$000(com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter a) {
        return a.textView;
    }
    
    static android.os.Handler access$100() {
        return handler;
    }
    
    private void setText() {
        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.incident_1);
        boolean b = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized();
        label1: {
            label0: {
                if (!b) {
                    break label0;
                }
                if (!com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isTrafficUpdaterRunning()) {
                    break label0;
                }
                this.textView.setText((CharSequence)"NORMAL");
                break label1;
            }
            this.textView.setText((CharSequence)"INACTIVE");
        }
    }
    
    private void unregister() {
        if (this.registered) {
            sLogger.v("unregister");
            this.bus.unregister(this);
            this.registered = false;
        }
    }
    
    public android.graphics.drawable.Drawable getDrawable() {
        return null;
    }
    
    public String getWidgetIdentifier() {
        return "TRAFFIC_INCIDENT_GAUGE_ID";
    }
    
    public String getWidgetName() {
        return null;
    }
    
    public void onDisplayTrafficIncident(com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident a) {
        try {
            if (this.textView != null) {
                sLogger.v(new StringBuilder().append("DisplayTrafficIncident type:").append(a.type).append(", category:").append(a.category).append(", title:").append(a.title).append(", description:").append(a.description).append(", affectedStreet:").append(a.affectedStreet).append(", distanceToIncident:").append(a.distanceToIncident).append(", reported:").append(a.reported).append(", updated:").append(a.updated).append(", icon:").append(a.icon).toString());
                switch(com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter$2.$SwitchMap$com$navdy$hud$app$maps$MapEvents$DisplayTrafficIncident$Type[a.type.ordinal()]) {
                    case 3: {
                        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.incident_1);
                        this.textView.setText((CharSequence)"FAILED");
                        break;
                    }
                    case 2: {
                        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.incident_1);
                        this.textView.setText((CharSequence)"INACTIVE");
                        break;
                    }
                    case 1: {
                        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.incident_1);
                        this.textView.setText((CharSequence)"NORMAL");
                        break;
                    }
                    default: {
                        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter$1(this, a), 1);
                    }
                }
            }
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
    }
    
    public void setView(com.navdy.hud.app.view.DashboardWidgetView a, android.os.Bundle a0) {
        if (a == null) {
            this.unregister();
            this.textView = null;
            super.setView(a, a0);
        } else {
            boolean b = a0 != null && a0.getBoolean("EXTRA_IS_ACTIVE", false);
            a.setContentView(R.layout.maps_traffic_incident_widget);
            this.textView = (android.widget.TextView)a.findViewById(R.id.incidentInfo);
            this.setText();
            super.setView(a, a0);
            if (b) {
                if (this.registered) {
                    sLogger.v("already register");
                } else {
                    this.bus.register(this);
                    this.registered = true;
                    sLogger.v("register");
                }
            } else {
                sLogger.v("not active");
                this.unregister();
            }
        }
    }
    
    protected void updateGauge() {
    }
}
