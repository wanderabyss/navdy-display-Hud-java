package com.navdy.hud.app.maps.here;

class HereRerouteListener$2 implements Runnable {
    final com.navdy.hud.app.maps.here.HereRerouteListener this$0;
    final com.here.android.mpa.routing.Route val$route;
    
    HereRerouteListener$2(com.navdy.hud.app.maps.here.HereRerouteListener a, com.here.android.mpa.routing.Route a0) {
        super();
        this.this$0 = a;
        this.val$route = a0;
    }
    
    public void run() {
        String s = com.navdy.hud.app.maps.here.HereRerouteListener.access$100(this.this$0).getCurrentRouteId();
        String s0 = com.navdy.hud.app.maps.here.HereRerouteListener.access$000(this.this$0);
        com.navdy.hud.app.maps.here.HereRerouteListener.access$300(this.this$0).i(new StringBuilder().append(com.navdy.hud.app.maps.here.HereRerouteListener.access$200(this.this$0)).append(" reroute-end: ").append(this.val$route).append(" id=").append(com.navdy.hud.app.maps.here.HereRerouteListener.access$000(this.this$0)).append(" current=").append(s).toString());
        com.navdy.hud.app.maps.here.HereRerouteListener.access$002(this.this$0, (String)null);
        com.navdy.hud.app.maps.here.HereRerouteListener.access$402(this.this$0, false);
        com.navdy.hud.app.maps.here.HereRerouteListener.access$500(this.this$0).post(com.navdy.hud.app.maps.here.HereRerouteListener.REROUTE_FINISHED);
        if (this.val$route != null) {
            if (com.navdy.hud.app.maps.here.HereRerouteListener.access$100(this.this$0).isNavigationModeOn()) {
                if (android.text.TextUtils.equals((CharSequence)s0, (CharSequence)s)) {
                    boolean b = com.navdy.hud.app.maps.here.HereRerouteListener.access$100(this.this$0).isTrafficConsidered(s);
                    com.navdy.hud.app.maps.here.HereRerouteListener.access$100(this.this$0).setReroute(this.val$route, com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason.NAV_SESSION_OFF_REROUTE, (String)null, (String)null, false, b);
                    com.navdy.hud.app.maps.here.HereRerouteListener.access$600(this.this$0).trackTripRecalculated();
                } else {
                    com.navdy.hud.app.maps.here.HereRerouteListener.access$300(this.this$0).i("route change before recalc completed: cannot use");
                }
            } else {
                com.navdy.hud.app.maps.here.HereRerouteListener.access$300(this.this$0).i(new StringBuilder().append(com.navdy.hud.app.maps.here.HereRerouteListener.access$200(this.this$0)).append("reroute-end: navigation already ended, not adding route").toString());
            }
        }
    }
}
