package com.navdy.hud.app.maps;

public class MapSchemeController implements android.hardware.SensorEventListener {
    final private static int AVERAGING_PERIOD = 300;
    final private static long DELAY_MILLIS = 10000L;
    final private static float HI_THRESHOLD = 600f;
    final private static float LO_THRESHOLD = 200f;
    final public static String MAP_SCHEME_CARNAV_DAY = "carnav.day";
    final public static String MAP_SCHEME_CARNAV_NIGHT = "carnav.day";
    final public static int MESSAGE_PERIODIC = 0;
    final public static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.maps.MapSchemeController singleton;
    private android.content.Context context;
    private int currentIdx;
    private float lastSensorValue;
    private android.hardware.Sensor light;
    private android.os.Handler myHandler;
    private boolean ringBufferFull;
    private android.hardware.SensorManager sensorManager;
    private float[] sensorValues;
    @Inject
    android.content.SharedPreferences sharedPreferences;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.MapSchemeController.class);
        singleton = new com.navdy.hud.app.maps.MapSchemeController();
    }
    
    public MapSchemeController() {
        this.lastSensorValue = 400f;
        this.sensorValues = new float[30];
        this.currentIdx = 0;
        this.ringBufferFull = false;
        this.context = com.navdy.hud.app.HudApplication.getAppContext();
        mortar.Mortar.inject(this.context, this);
        this.sensorManager = (android.hardware.SensorManager)this.context.getSystemService("sensor");
        Object a = this.sensorManager.getSensorList(-1);
        int i = 0;
        while(i < ((java.util.List)a).size()) {
            android.hardware.Sensor a0 = (android.hardware.Sensor)((java.util.List)a).get(i);
            sLogger.d(new StringBuilder().append("sensor: ").append(a0.toString()).toString());
            if (a0.getName().equals("tsl2572")) {
                this.light = a0;
            }
            i = i + 1;
        }
        sLogger.d(new StringBuilder().append("light sensor selected: ").append(this.light).toString());
        this.sensorManager.registerListener((android.hardware.SensorEventListener)this, this.light, 3);
        this.myHandler = new com.navdy.hud.app.maps.MapSchemeController$1(this, android.os.Looper.getMainLooper());
        this.myHandler.sendEmptyMessageDelayed(0, 10000L);
    }
    
    static void access$000(com.navdy.hud.app.maps.MapSchemeController a) {
        a.collectLightSensorSample();
    }
    
    static void access$100(com.navdy.hud.app.maps.MapSchemeController a) {
        a.evaluateCollectedSamples();
    }
    
    private void collectLightSensorSample() {
        this.sensorValues[this.currentIdx] = this.lastSensorValue;
        if (this.currentIdx == this.sensorValues.length - 1) {
            this.ringBufferFull = true;
        }
        this.currentIdx = (this.currentIdx + 1) % this.sensorValues.length;
    }
    
    private void evaluateCollectedSamples() {
        int i = (this.ringBufferFull) ? this.sensorValues.length : this.currentIdx;
        float f = 0.0f;
        int i0 = 0;
        while(i0 < i) {
            f = f + this.sensorValues[i0];
            i0 = i0 + 1;
        }
        float f0 = f / (float)i;
        sLogger.d(new StringBuilder().append("evaluateCollectedSamples: avg lux: ").append(f0).toString());
        com.navdy.hud.app.maps.MapSchemeController$MapScheme a = this.getActiveScheme();
        com.navdy.hud.app.maps.MapSchemeController$MapScheme a0 = com.navdy.hud.app.maps.MapSchemeController$MapScheme.DAY;
        label1: {
            label0: {
                if (a != a0) {
                    break label0;
                }
                if (!(f0 < 200f)) {
                    break label0;
                }
                this.switchMapScheme(com.navdy.hud.app.maps.MapSchemeController$MapScheme.NIGHT);
                break label1;
            }
            if (this.getActiveScheme() == com.navdy.hud.app.maps.MapSchemeController$MapScheme.NIGHT && f0 > 600f) {
                this.switchMapScheme(com.navdy.hud.app.maps.MapSchemeController$MapScheme.DAY);
            }
        }
    }
    
    public static com.navdy.hud.app.maps.MapSchemeController getInstance() {
        return singleton;
    }
    
    private void switchMapScheme(com.navdy.hud.app.maps.MapSchemeController$MapScheme a) {
        String s = this.sharedPreferences.getString("map.scheme", "");
        boolean b = s.equals("carnav.day");
        label1: {
            label0: {
                if (!b) {
                    break label0;
                }
                if (a != com.navdy.hud.app.maps.MapSchemeController$MapScheme.NIGHT) {
                    break label0;
                }
                s = "carnav.day";
                break label1;
            }
            if (s.equals("carnav.day") && a == com.navdy.hud.app.maps.MapSchemeController$MapScheme.DAY) {
                s = "carnav.day";
            }
        }
        sLogger.i(new StringBuilder().append("changing map scheme to ").append(s).toString());
        this.sharedPreferences.edit().putString("map.scheme", s).apply();
    }
    
    com.navdy.hud.app.maps.MapSchemeController$MapScheme getActiveScheme() {
        return (this.sharedPreferences.getString("map.scheme", "").equals("carnav.day")) ? com.navdy.hud.app.maps.MapSchemeController$MapScheme.DAY : com.navdy.hud.app.maps.MapSchemeController$MapScheme.NIGHT;
    }
    
    public void onAccuracyChanged(android.hardware.Sensor a, int i) {
        sLogger.d(new StringBuilder().append("onAccuracyChanged: accuracy: ").append(i).toString());
    }
    
    final public void onSensorChanged(android.hardware.SensorEvent a) {
        float f = a.values[0];
        if (sLogger.isLoggable(2)) {
            sLogger.d(new StringBuilder().append("onSensorChanged: lux: ").append(f).toString());
        }
        this.lastSensorValue = f;
    }
}
