package com.navdy.hud.app.maps.here;

public class HereRealisticViewListener extends com.here.android.mpa.guidance.NavigationManager$RealisticViewListener {
    final private static com.navdy.hud.app.maps.MapEvents$HideSignPostJunction HIDE_SIGN_POST_JUNCTION;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private com.squareup.otto.Bus bus;
    final private com.navdy.hud.app.maps.here.HereNavController navController;
    private boolean running;
    private Runnable start;
    private Runnable stop;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereRealisticViewListener.class);
        HIDE_SIGN_POST_JUNCTION = new com.navdy.hud.app.maps.MapEvents$HideSignPostJunction();
    }
    
    HereRealisticViewListener(com.squareup.otto.Bus a, com.navdy.hud.app.maps.here.HereNavController a0) {
        this.start = (Runnable)new com.navdy.hud.app.maps.here.HereRealisticViewListener$1(this);
        this.stop = (Runnable)new com.navdy.hud.app.maps.here.HereRealisticViewListener$2(this);
        this.navController = a0;
        this.bus = a;
    }
    
    static com.navdy.hud.app.maps.here.HereNavController access$000(com.navdy.hud.app.maps.here.HereRealisticViewListener a) {
        return a.navController;
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    private void clearDisplayEvent() {
        this.bus.post(HIDE_SIGN_POST_JUNCTION);
    }
    
    public void onRealisticViewHide() {
        this.clearDisplayEvent();
    }
    
    public void onRealisticViewNextManeuver(com.here.android.mpa.guidance.NavigationManager$AspectRatio a, com.here.android.mpa.common.Image a0, com.here.android.mpa.common.Image a1) {
    }
    
    public void onRealisticViewShow(com.here.android.mpa.guidance.NavigationManager$AspectRatio a, com.here.android.mpa.common.Image a0, com.here.android.mpa.common.Image a1) {
        this.bus.post(new com.navdy.hud.app.maps.MapEvents$DisplayJunction(a0, a1));
    }
    
    void start() {
        synchronized(this) {
            if (!this.running) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(this.start, 15);
                this.running = true;
                sLogger.v("start:added realistic view listener");
            }
        }
        /*monexit(this)*/;
    }
    
    void stop() {
        synchronized(this) {
            if (this.running) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(this.stop, 15);
                this.running = false;
                this.clearDisplayEvent();
                sLogger.v("stop:remove realistic view listener");
            }
        }
        /*monexit(this)*/;
    }
}
