package com.navdy.hud.app.maps;

public class MapEvents$RegionEvent {
    final public String country;
    final public String state;
    
    public MapEvents$RegionEvent(String s, String s0) {
        this.state = s;
        this.country = s0;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.hud.app.maps.MapEvents$RegionEvent) {
                com.navdy.hud.app.maps.MapEvents$RegionEvent a0 = (com.navdy.hud.app.maps.MapEvents$RegionEvent)a;
                boolean b0 = (this.state != null) ? this.state.equals(a0.state) : a0.state == null;
                label3: {
                    label1: {
                        if (!b0) {
                            break label1;
                        }
                        String s = this.country;
                        label2: {
                            if (s == null) {
                                break label2;
                            }
                            if (!this.country.equals(a0.country)) {
                                break label1;
                            }
                            break label3;
                        }
                        if (a0.country == null) {
                            b = true;
                            break label0;
                        }
                    }
                    b = false;
                    break label0;
                }
                b = true;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        return (((this.state != null) ? this.state.hashCode() : 0) + 527) * 31 + ((this.country != null) ? this.country.hashCode() : 0);
    }
    
    public String toString() {
        return new StringBuilder().append("RegionEvent: ").append(this.state).append(", ").append(this.country).toString();
    }
}
