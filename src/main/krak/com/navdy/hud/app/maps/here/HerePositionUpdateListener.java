package com.navdy.hud.app.maps.here;

public class HerePositionUpdateListener extends com.here.android.mpa.guidance.NavigationManager$PositionListener {
    final private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    private com.here.android.mpa.common.GeoPosition lastGeoPosition;
    private long lastGeoPositionTime;
    private com.navdy.service.library.log.Logger logger;
    private com.navdy.hud.app.maps.here.HereNavController navController;
    private String tag;
    
    HerePositionUpdateListener(String s, com.navdy.hud.app.maps.here.HereNavController a, com.navdy.hud.app.maps.here.HereNavigationManager a0, com.squareup.otto.Bus a1) {
        this.logger = new com.navdy.service.library.log.Logger("HerePositionListener");
        this.tag = s;
        this.navController = a;
        this.hereNavigationManager = a0;
        this.bus = a1;
        a1.register(this);
    }
    
    static void access$000(com.navdy.hud.app.maps.here.HerePositionUpdateListener a, com.here.android.mpa.common.GeoPosition a0) {
        a.handlePositionUpdate(a0);
    }
    
    private void handlePositionUpdate(com.here.android.mpa.common.GeoPosition a) {
        label0: try {
            if (this.navController.isInitialized() && a != null) {
                com.here.android.mpa.common.GeoPosition a0 = this.lastGeoPosition;
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    long j = android.os.SystemClock.elapsedRealtime() - this.lastGeoPositionTime;
                    if (j >= 500L) {
                        break label1;
                    }
                    if (!com.navdy.hud.app.maps.here.HereMapUtil.isCoordinateEqual(this.lastGeoPosition.getCoordinate(), a.getCoordinate())) {
                        break label1;
                    }
                    if (!this.logger.isLoggable(2)) {
                        break label0;
                    }
                    this.logger.v(new StringBuilder().append(this.tag).append("GEO-Here-Nav same pos as last:").append(j).toString());
                    break label0;
                }
                this.lastGeoPosition = a;
                this.lastGeoPositionTime = android.os.SystemClock.elapsedRealtime();
                if (this.logger.isLoggable(2)) {
                    String s = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadName();
                    this.logger.v(new StringBuilder().append(this.tag).append(" onPosUpdated: road name =").append(s).toString());
                }
                this.postTrackingEvent(a);
                this.hereNavigationManager.refreshNavigationInfo();
            }
        } catch(Throwable a1) {
            this.logger.e(a1);
            com.navdy.hud.app.util.CrashReporter.getInstance().reportNonFatalException(a1);
        }
    }
    
    private void postTrackingEvent(com.here.android.mpa.common.GeoPosition a) {
        if (this.hereNavigationManager.isNavigationModeOn()) {
            com.here.android.mpa.routing.RouteTta a0 = this.navController.getTta(com.here.android.mpa.routing.Route$TrafficPenaltyMode.OPTIMAL, true);
            if (a0 == null) {
                this.bus.post(new com.navdy.hud.app.framework.trips.TripManager$TrackingEvent(a));
            } else {
                this.bus.post(new com.navdy.hud.app.framework.trips.TripManager$TrackingEvent(a, this.hereNavigationManager.getCurrentDestinationIdentifier(), a0.getDuration(), (int)this.navController.getDestinationDistance()));
            }
        } else {
            this.bus.post(new com.navdy.hud.app.framework.trips.TripManager$TrackingEvent(a));
        }
    }
    
    public void onPositionUpdated(com.here.android.mpa.common.GeoPosition a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HerePositionUpdateListener$1(this, a), 4);
    }
}
