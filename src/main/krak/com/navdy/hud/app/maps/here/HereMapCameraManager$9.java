package com.navdy.hud.app.maps.here;

class HereMapCameraManager$9 {
    final static int[] $SwitchMap$com$navdy$hud$app$maps$MapEvents$DialMapZoom$Type;
    final static int[] $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State;
    
    static {
        $SwitchMap$com$navdy$hud$app$maps$MapEvents$DialMapZoom$Type = new int[com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$maps$MapEvents$DialMapZoom$Type;
        com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type a0 = com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type.IN;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$MapEvents$DialMapZoom$Type[com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type.OUT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State = new int[com.navdy.hud.app.maps.here.HereMapController$State.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State;
        com.navdy.hud.app.maps.here.HereMapController$State a2 = com.navdy.hud.app.maps.here.HereMapController$State.OVERVIEW;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State[com.navdy.hud.app.maps.here.HereMapController$State.AR_MODE.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException2) {
        }
    }
}
