package com.navdy.hud.app.maps.here;

public class HereTrafficUpdater2 {
    final private static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident FAILED_EVENT;
    final private static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident INACTIVE_EVENT;
    final private static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident NORMAL_EVENT;
    final private static int STALE_REQUEST_TIME;
    final private static String TAG = "HereTrafficUpdater2";
    final private static int TRAFFIC_DATA_CHECK_TIME_INTERVAL;
    final private static int TRAFFIC_DATA_REFRESH_INTERVAL;
    final private static int TRAFFIC_DATA_REFRESH_INTERVAL_LIMITED_BANDWIDTH;
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.squareup.otto.Bus bus;
    private Runnable checkRunnable;
    private com.here.android.mpa.guidance.TrafficUpdater$RequestInfo currentRequestInfo;
    private android.os.Handler handler;
    private long lastRequestTime;
    private com.here.android.mpa.guidance.TrafficUpdater$GetEventsListener onGetEventsListener;
    private com.here.android.mpa.guidance.TrafficUpdater$Listener onRequestListener;
    private Runnable refreshRunnable;
    private com.here.android.mpa.routing.Route route;
    private com.here.android.mpa.guidance.TrafficUpdater trafficUpdater;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger("HereTrafficUpdater2");
        TRAFFIC_DATA_REFRESH_INTERVAL = (int)java.util.concurrent.TimeUnit.MINUTES.toMillis(5L);
        TRAFFIC_DATA_REFRESH_INTERVAL_LIMITED_BANDWIDTH = (int)java.util.concurrent.TimeUnit.MINUTES.toMillis(15L);
        TRAFFIC_DATA_CHECK_TIME_INTERVAL = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(30L);
        STALE_REQUEST_TIME = (int)java.util.concurrent.TimeUnit.MINUTES.toMillis(2L);
        NORMAL_EVENT = new com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident(com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type.NORMAL, com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category.UNDEFINED);
        FAILED_EVENT = new com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident(com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type.FAILED, com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category.UNDEFINED);
        INACTIVE_EVENT = new com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident(com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type.INACTIVE, com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category.UNDEFINED);
    }
    
    public HereTrafficUpdater2(com.squareup.otto.Bus a) {
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.onRequestListener = (com.here.android.mpa.guidance.TrafficUpdater$Listener)new com.navdy.hud.app.maps.here.HereTrafficUpdater2$1(this);
        this.onGetEventsListener = (com.here.android.mpa.guidance.TrafficUpdater$GetEventsListener)new com.navdy.hud.app.maps.here.HereTrafficUpdater2$2(this);
        this.refreshRunnable = (Runnable)new com.navdy.hud.app.maps.here.HereTrafficUpdater2$3(this);
        this.checkRunnable = (Runnable)new com.navdy.hud.app.maps.here.HereTrafficUpdater2$4(this);
        sLogger.i("ctor");
        this.bus = a;
        this.trafficUpdater = com.here.android.mpa.guidance.TrafficUpdater.getInstance();
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static com.here.android.mpa.routing.Route access$100(com.navdy.hud.app.maps.here.HereTrafficUpdater2 a) {
        return a.route;
    }
    
    static void access$1000(com.navdy.hud.app.maps.here.HereTrafficUpdater2 a) {
        a.startRequest();
    }
    
    static int access$1100() {
        return STALE_REQUEST_TIME;
    }
    
    static int access$1200() {
        return TRAFFIC_DATA_CHECK_TIME_INTERVAL;
    }
    
    static android.os.Handler access$1300(com.navdy.hud.app.maps.here.HereTrafficUpdater2 a) {
        return a.handler;
    }
    
    static com.here.android.mpa.guidance.TrafficUpdater$RequestInfo access$200(com.navdy.hud.app.maps.here.HereTrafficUpdater2 a) {
        return a.currentRequestInfo;
    }
    
    static com.here.android.mpa.guidance.TrafficUpdater$RequestInfo access$202(com.navdy.hud.app.maps.here.HereTrafficUpdater2 a, com.here.android.mpa.guidance.TrafficUpdater$RequestInfo a0) {
        a.currentRequestInfo = a0;
        return a0;
    }
    
    static long access$300(com.navdy.hud.app.maps.here.HereTrafficUpdater2 a) {
        return a.lastRequestTime;
    }
    
    static long access$302(com.navdy.hud.app.maps.here.HereTrafficUpdater2 a, long j) {
        a.lastRequestTime = j;
        return j;
    }
    
    static void access$400(com.navdy.hud.app.maps.here.HereTrafficUpdater2 a, int i) {
        a.reset(i);
    }
    
    static com.here.android.mpa.guidance.TrafficUpdater$GetEventsListener access$500(com.navdy.hud.app.maps.here.HereTrafficUpdater2 a) {
        return a.onGetEventsListener;
    }
    
    static com.here.android.mpa.guidance.TrafficUpdater access$600(com.navdy.hud.app.maps.here.HereTrafficUpdater2 a) {
        return a.trafficUpdater;
    }
    
    static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident access$700() {
        return FAILED_EVENT;
    }
    
    static com.squareup.otto.Bus access$800(com.navdy.hud.app.maps.here.HereTrafficUpdater2 a) {
        return a.bus;
    }
    
    static void access$900(com.navdy.hud.app.maps.here.HereTrafficUpdater2 a, com.here.android.mpa.routing.Route a0, java.util.List a1, long j) {
        a.processEvents(a0, a1, j);
    }
    
    private com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident buildEvent(com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type a, java.util.List a0) {
        java.util.Iterator a1 = a0.iterator();
        com.navdy.hud.app.maps.here.HereTrafficUpdater2$HereTrafficEvent a2 = null;
        com.navdy.hud.app.maps.here.HereTrafficUpdater2$HereTrafficEvent a3 = null;
        com.navdy.hud.app.maps.here.HereTrafficUpdater2$HereTrafficEvent a4 = null;
        com.navdy.hud.app.maps.here.HereTrafficUpdater2$HereTrafficEvent a5 = null;
        com.navdy.hud.app.maps.here.HereTrafficUpdater2$HereTrafficEvent a6 = null;
        com.navdy.hud.app.maps.here.HereTrafficUpdater2$HereTrafficEvent a7 = null;
        com.navdy.hud.app.maps.here.HereTrafficUpdater2$HereTrafficEvent a8 = null;
        Object a9 = a1;
        while(true) {
            boolean b = ((java.util.Iterator)a9).hasNext();
            com.navdy.hud.app.maps.here.HereTrafficUpdater2$HereTrafficEvent a10 = a8;
            if (b) {
                int i = 0;
                a8 = (com.navdy.hud.app.maps.here.HereTrafficUpdater2$HereTrafficEvent)((java.util.Iterator)a9).next();
                String s = a8.trafficEvent.getShortText();
                switch(s.hashCode()) {
                    case 2101625895: {
                        i = (s.equals("CONGESTION")) ? 3 : -1;
                        break;
                    }
                    case 1943412802: {
                        i = (s.equals("ROADWORKS")) ? 2 : -1;
                        break;
                    }
                    case 1748463920: {
                        i = (s.equals("UNDEFINED")) ? 6 : -1;
                        break;
                    }
                    case 1584535067: {
                        i = (s.equals("CLOSURE")) ? 1 : -1;
                        break;
                    }
                    case 75532016: {
                        i = (s.equals("OTHER")) ? 5 : -1;
                        break;
                    }
                    case 2160942: {
                        i = (s.equals("FLOW")) ? 4 : -1;
                        break;
                    }
                    case -1360575985: {
                        i = (s.equals("ACCIDENT")) ? 0 : -1;
                        break;
                    }
                    default: {
                        i = -1;
                    }
                }
                switch(i) {
                    case 5: {
                        if (a7 != null) {
                            if (a7.distance <= a8.distance) {
                                a8 = a10;
                                break;
                            } else {
                                a7 = a8;
                                a8 = a10;
                                break;
                            }
                        } else {
                            a7 = a8;
                            a8 = a10;
                            break;
                        }
                    }
                    case 4: {
                        if (a6 != null) {
                            if (a6.distance <= a8.distance) {
                                a8 = a10;
                                break;
                            } else {
                                a6 = a8;
                                a8 = a10;
                                break;
                            }
                        } else {
                            a6 = a8;
                            a8 = a10;
                            break;
                        }
                    }
                    case 3: {
                        if (a5 != null) {
                            if (a5.distance <= a8.distance) {
                                a8 = a10;
                                break;
                            } else {
                                a5 = a8;
                                a8 = a10;
                                break;
                            }
                        } else {
                            a5 = a8;
                            a8 = a10;
                            break;
                        }
                    }
                    case 2: {
                        if (a4 != null) {
                            if (a4.distance <= a8.distance) {
                                a8 = a10;
                                break;
                            } else {
                                a4 = a8;
                                a8 = a10;
                                break;
                            }
                        } else {
                            a4 = a8;
                            a8 = a10;
                            break;
                        }
                    }
                    case 1: {
                        if (a3 != null) {
                            if (a3.distance <= a8.distance) {
                                a8 = a10;
                                break;
                            } else {
                                a3 = a8;
                                a8 = a10;
                                break;
                            }
                        } else {
                            a3 = a8;
                            a8 = a10;
                            break;
                        }
                    }
                    case 0: {
                        if (a2 != null) {
                            if (a2.distance <= a8.distance) {
                                a8 = a10;
                                break;
                            } else {
                                a2 = a8;
                                a8 = a10;
                                break;
                            }
                        } else {
                            a2 = a8;
                            a8 = a10;
                            break;
                        }
                    }
                    default: {
                        if (a10 != null && a10.distance <= a8.distance) {
                            a8 = a10;
                        }
                    }
                }
            } else {
                int i0 = 0;
                String s0 = null;
                com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category a11 = null;
                String s1 = null;
                if (a2 == null) {
                    if (a3 == null) {
                        if (a4 == null) {
                            if (a5 == null) {
                                if (a6 == null) {
                                    if (a7 == null) {
                                        a2 = null;
                                        if (a8 != null) {
                                            a2 = a8;
                                        }
                                    } else {
                                        a2 = a7;
                                    }
                                } else {
                                    a2 = a6;
                                }
                            } else {
                                a2 = a5;
                            }
                        } else {
                            a2 = a4;
                        }
                    } else {
                        a2 = a3;
                    }
                }
                String s2 = a2.trafficEvent.getFirstAffectedStreet();
                String s3 = a2.trafficEvent.getShortText();
                switch(s3.hashCode()) {
                    case 2101625895: {
                        i0 = (s3.equals("CONGESTION")) ? 3 : -1;
                        break;
                    }
                    case 1943412802: {
                        i0 = (s3.equals("ROADWORKS")) ? 2 : -1;
                        break;
                    }
                    case 1748463920: {
                        i0 = (s3.equals("UNDEFINED")) ? 6 : -1;
                        break;
                    }
                    case 1584535067: {
                        i0 = (s3.equals("CLOSURE")) ? 1 : -1;
                        break;
                    }
                    case 75532016: {
                        i0 = (s3.equals("OTHER")) ? 5 : -1;
                        break;
                    }
                    case 2160942: {
                        i0 = (s3.equals("FLOW")) ? 4 : -1;
                        break;
                    }
                    case -1360575985: {
                        i0 = (s3.equals("ACCIDENT")) ? 0 : -1;
                        break;
                    }
                    default: {
                        i0 = -1;
                    }
                }
                switch(i0) {
                    case 5: {
                        s0 = a2.trafficEvent.getEventText();
                        a11 = com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category.OTHER;
                        s1 = "Other";
                        break;
                    }
                    case 4: {
                        a11 = com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category.FLOW;
                        s1 = "Congestion";
                        s0 = "";
                        break;
                    }
                    case 3: {
                        s0 = a2.trafficEvent.getEventText();
                        a11 = com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category.CONGESTION;
                        s1 = "Congestion";
                        break;
                    }
                    case 2: {
                        s0 = a2.trafficEvent.getEventText();
                        a11 = com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category.ROADWORKS;
                        s1 = "RoadWork";
                        break;
                    }
                    case 1: {
                        s0 = a2.trafficEvent.getEventText();
                        a11 = com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category.CLOSURE;
                        s1 = "Closure";
                        break;
                    }
                    case 0: {
                        s0 = a2.trafficEvent.getEventText();
                        a11 = com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category.ACCIDENT;
                        s1 = "Accident";
                        break;
                    }
                    default: {
                        s1 = new StringBuilder().append("Undefined:").append(a2.trafficEvent.getShortText()).toString();
                        s0 = a2.trafficEvent.getEventText();
                        a11 = com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category.UNDEFINED;
                    }
                }
                return new com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident(a, a11, s1, s0, s2, a2.distance, a2.trafficEvent.getActivationDate(), a2.trafficEvent.getActivationDate(), a2.trafficEvent.getIconOnRoute());
            }
        }
    }
    
    private void cancelRequest() {
        label0: {
            try {
                if (this.currentRequestInfo == null) {
                    break label0;
                }
                if (this.currentRequestInfo.getError() == com.here.android.mpa.guidance.TrafficUpdater$Error.NONE) {
                    this.trafficUpdater.cancelRequest(this.currentRequestInfo.getRequestId());
                    this.lastRequestTime = 0L;
                    sLogger.v("cancelRequest: called cancel");
                }
            } catch(Throwable a) {
                sLogger.e(a);
            }
            this.currentRequestInfo = null;
        }
    }
    
    private void processEvents(com.here.android.mpa.routing.Route a, java.util.List a0, long j) {
        long j0 = android.os.SystemClock.elapsedRealtime();
        sLogger.v(new StringBuilder().append("processEvents size:").append(a0.size()).toString());
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        a.getDestination();
        java.util.ArrayList a1 = new java.util.ArrayList();
        java.util.ArrayList a2 = new java.util.ArrayList();
        java.util.ArrayList a3 = new java.util.ArrayList();
        com.here.android.mpa.common.RoadElement a4 = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadElement();
        if (a4 != null) {
            com.here.android.mpa.common.GeoCoordinate a5 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
            if (a5 != null) {
                java.util.List a6 = a.getManeuvers();
                com.here.android.mpa.routing.Maneuver a7 = com.navdy.hud.app.maps.here.HereMapUtil.getLastManeuver(a6);
                com.here.android.mpa.common.RoadElement a8 = null;
                if (a7 != null) {
                    java.util.List a9 = a7.getRoadElements();
                    a8 = null;
                    if (a9 != null) {
                        int i = a9.size();
                        a8 = null;
                        if (i > 0) {
                            a8 = (com.here.android.mpa.common.RoadElement)a9.get(a9.size() - 1);
                        }
                    }
                }
                Object a10 = a0.iterator();
                int i0 = 0;
                int i1 = 0;
                int i2 = 0;
                Object a11 = a6;
                while(((java.util.Iterator)a10).hasNext()) {
                    com.here.android.mpa.mapping.TrafficEvent a12 = (com.here.android.mpa.mapping.TrafficEvent)((java.util.Iterator)a10).next();
                    if (a12.isOnRoute(a)) {
                        java.util.List a13 = a12.getAffectedRoadElements();
                        if (a13 != null && a13.size() != 0) {
                            com.here.android.mpa.mapping.TrafficEvent$Severity a14 = a12.getSeverity();
                            int i3 = com.navdy.hud.app.maps.here.HereTrafficUpdater2$5.$SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity[a14.ordinal()];
                            boolean b = false;
                            switch(i3) {
                                case 1: case 2: case 3: {
                                    b = true;
                                }
                                default: {
                                    long j1 = 0L;
                                    if (b) {
                                        j1 = com.navdy.hud.app.maps.here.HereMapUtil.getDistanceToRoadElement(a4, (com.here.android.mpa.common.RoadElement)a13.get(0), (java.util.List)a11);
                                        long j2 = com.navdy.hud.app.maps.here.HereMapUtil.getDistanceToRoadElement((com.here.android.mpa.common.RoadElement)a13.get(0), a8, (java.util.List)a11);
                                        if (j2 == -1L) {
                                            sLogger.v("could not get distance");
                                            continue;
                                        }
                                        long j3 = (long)a12.getDistanceTo(a5);
                                        sLogger.v(new StringBuilder().append("[EVENT] distanceToEvent:").append(j1).append(" eventToDestinationDistance:").append(j2).append(" distanceToDest:").append(j).append(" total:").append(j1 + j2).append(" distanceToEventHaversine:").append(j3).toString());
                                    } else {
                                        sLogger.v(new StringBuilder().append("[EVENT] n/a severity = ").append(a14).toString());
                                        j1 = -1L;
                                    }
                                    switch(com.navdy.hud.app.maps.here.HereTrafficUpdater2$5.$SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity[a14.ordinal()]) {
                                        case 3: {
                                            ((java.util.List)a3).add(new com.navdy.hud.app.maps.here.HereTrafficUpdater2$HereTrafficEvent(a12, j1));
                                            i2 = i2 + 1;
                                            continue;
                                        }
                                        case 2: {
                                            ((java.util.List)a2).add(new com.navdy.hud.app.maps.here.HereTrafficUpdater2$HereTrafficEvent(a12, j1));
                                            i1 = i1 + 1;
                                            continue;
                                        }
                                        case 1: {
                                            ((java.util.List)a1).add(new com.navdy.hud.app.maps.here.HereTrafficUpdater2$HereTrafficEvent(a12, j1));
                                            i0 = i0 + 1;
                                            continue;
                                        }
                                        default: {
                                            continue;
                                        }
                                    }
                                }
                            }
                        }
                        sLogger.v("no road elements for event");
                    } else {
                        sLogger.v("[NOT_ON_ROUTE] event is not on route");
                    }
                }
                sLogger.v(new StringBuilder().append("blocking:").append(i0).append(" very high:").append(i1).append(" high:").append(i2).toString());
                if (i0 <= 0) {
                    if (i1 <= 0) {
                        if (i2 <= 0) {
                            this.bus.post(NORMAL_EVENT);
                        } else {
                            this.bus.post(this.buildEvent(com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type.HIGH, (java.util.List)a3));
                        }
                    } else {
                        this.bus.post(this.buildEvent(com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type.VERY_HIGH, (java.util.List)a2));
                    }
                } else {
                    this.bus.post(this.buildEvent(com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type.BLOCKING, (java.util.List)a1));
                }
                long j4 = android.os.SystemClock.elapsedRealtime();
                sLogger.v(new StringBuilder().append("processEvents took [").append(j4 - j0).append("]").toString());
            } else {
                sLogger.w("no current coordinate");
            }
        } else {
            sLogger.w("no current road element");
        }
    }
    
    private void reset(int i) {
        sLogger.v("reset");
        this.cancelRequest();
        this.handler.removeCallbacks(this.refreshRunnable);
        this.handler.postDelayed(this.refreshRunnable, (long)i);
        this.handler.removeCallbacks(this.checkRunnable);
    }
    
    private void startRequest() {
        label3: try {
            if (com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext())) {
                if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().hasArrived()) {
                    sLogger.i("startRequest: arrived, not making request");
                    this.bus.post(INACTIVE_EVENT);
                    this.lastRequestTime = 0L;
                } else {
                    com.here.android.mpa.routing.Route a = this.route;
                    label1: {
                        label2: {
                            if (a == null) {
                                break label2;
                            }
                            sLogger.i("startRequest: getting route traffic");
                            this.currentRequestInfo = this.trafficUpdater.request(this.route, this.onRequestListener);
                            break label1;
                        }
                        com.here.android.mpa.common.GeoCoordinate a0 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                        label0: {
                            if (a0 == null) {
                                break label0;
                            }
                            sLogger.i("startRequest: getting area traffic");
                            this.currentRequestInfo = this.trafficUpdater.request(a0, this.onRequestListener);
                            break label1;
                        }
                        sLogger.i("startRequest: getting area traffic, no current coordinate");
                        this.lastRequestTime = 0L;
                        this.bus.post(FAILED_EVENT);
                        this.reset(this.getRefereshInterval() / 2);
                        break label3;
                    }
                    com.here.android.mpa.guidance.TrafficUpdater$Error a1 = this.currentRequestInfo.getError();
                    sLogger.i(new StringBuilder().append("startRequest: returned:").append(a1).toString());
                    if (com.navdy.hud.app.maps.here.HereTrafficUpdater2$5.$SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error[a1.ordinal()] != 0) {
                        this.handler.removeCallbacks(this.checkRunnable);
                        this.handler.postDelayed(this.checkRunnable, (long)TRAFFIC_DATA_CHECK_TIME_INTERVAL);
                        this.lastRequestTime = android.os.SystemClock.elapsedRealtime();
                    } else {
                        this.lastRequestTime = 0L;
                        this.bus.post(FAILED_EVENT);
                        this.reset(this.getRefereshInterval() / 2);
                    }
                }
            } else {
                sLogger.i("startRequest: not connected to n/w, retry");
                this.reset(this.getRefereshInterval() / 2);
                this.bus.post(FAILED_EVENT);
                this.lastRequestTime = 0L;
            }
        } catch(Throwable a2) {
            sLogger.e(a2);
            this.reset(this.getRefereshInterval() / 2);
        }
    }
    
    int getRefereshInterval() {
        return (com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance().isLimitBandwidthModeOn()) ? TRAFFIC_DATA_REFRESH_INTERVAL_LIMITED_BANDWIDTH : TRAFFIC_DATA_REFRESH_INTERVAL;
    }
    
    boolean isRunning() {
        return this.route != null;
    }
    
    public void setRoute(com.here.android.mpa.routing.Route a) {
        sLogger.i(new StringBuilder().append("setRoute:").append(a).append(" id=").append(System.identityHashCode(a)).toString());
        this.cancelRequest();
        this.route = a;
        this.startRequest();
    }
    
    public void start() {
        sLogger.i("start");
        this.startRequest();
    }
}
