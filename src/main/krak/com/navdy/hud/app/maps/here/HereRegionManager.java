package com.navdy.hud.app.maps.here;

public class HereRegionManager {
    final private static long REGION_CHECK_INTERVAL = 900000L;
    final private static com.navdy.hud.app.maps.here.HereRegionManager sInstance;
    final private static com.navdy.service.library.log.Logger sLogger;
    @Inject
    com.squareup.otto.Bus bus;
    final private Runnable checkRegionRunnable;
    private com.navdy.hud.app.maps.MapEvents$RegionEvent currentRegionEvent;
    final private android.os.Handler handler;
    final private com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereRegionManager.class);
        sInstance = new com.navdy.hud.app.maps.here.HereRegionManager();
    }
    
    private HereRegionManager() {
        this.checkRegionRunnable = (Runnable)new com.navdy.hud.app.maps.here.HereRegionManager$1(this);
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        this.hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.handler.postDelayed(this.checkRegionRunnable, 900000L);
    }
    
    static void access$000(com.navdy.hud.app.maps.here.HereRegionManager a) {
        a.checkRegion();
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static com.navdy.hud.app.maps.MapEvents$RegionEvent access$200(com.navdy.hud.app.maps.here.HereRegionManager a) {
        return a.currentRegionEvent;
    }
    
    static com.navdy.hud.app.maps.MapEvents$RegionEvent access$202(com.navdy.hud.app.maps.here.HereRegionManager a, com.navdy.hud.app.maps.MapEvents$RegionEvent a0) {
        a.currentRegionEvent = a0;
        return a0;
    }
    
    private void checkRegion() {
        label4: {
            Throwable a = null;
            label0: {
                label1: {
                    label3: try {
                        com.here.android.mpa.common.GeoPosition a0 = this.hereMapsManager.getLastGeoPosition();
                        label2: {
                            if (a0 != null) {
                                break label2;
                            }
                            sLogger.v("invalid checkRegion call with unknown geoPosition");
                            break label3;
                        }
                        com.here.android.mpa.search.ErrorCode a1 = new com.here.android.mpa.search.ReverseGeocodeRequest2(a0.getCoordinate()).execute((com.here.android.mpa.search.ResultListener)new com.navdy.hud.app.maps.here.HereRegionManager$2(this));
                        if (a1 == com.here.android.mpa.search.ErrorCode.NONE) {
                            break label1;
                        }
                        sLogger.e(new StringBuilder().append("reverseGeoCode:execute: Error[").append(a1.name()).append("]").toString());
                        break label1;
                    } catch(Throwable a2) {
                        a = a2;
                        break label0;
                    }
                    this.handler.postDelayed(this.checkRegionRunnable, 900000L);
                    break label4;
                }
                this.handler.postDelayed(this.checkRegionRunnable, 900000L);
                break label4;
            }
            try {
                sLogger.e(a);
                com.navdy.hud.app.util.CrashReporter.getInstance().reportNonFatalException(a);
            } catch(Throwable a3) {
                this.handler.postDelayed(this.checkRegionRunnable, 900000L);
                throw a3;
            }
            this.handler.postDelayed(this.checkRegionRunnable, 900000L);
        }
    }
    
    public static com.navdy.hud.app.maps.here.HereRegionManager getInstance() {
        return sInstance;
    }
}
