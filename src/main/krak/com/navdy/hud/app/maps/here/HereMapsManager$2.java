package com.navdy.hud.app.maps.here;

class HereMapsManager$2 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapsManager this$0;
    
    HereMapsManager$2(com.navdy.hud.app.maps.here.HereMapsManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        long j = android.os.SystemClock.elapsedRealtime();
        com.navdy.hud.app.maps.here.HereMapsManager.access$2302(this.this$0, com.here.android.mpa.odml.MapLoader.getInstance());
        long j0 = android.os.SystemClock.elapsedRealtime();
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v(new StringBuilder().append("initMapLoader took [").append(j0 - j).append("]").toString());
        com.navdy.hud.app.maps.here.HereMapsManager.access$2300(this.this$0).addListener((com.here.android.mpa.odml.MapLoader$Listener)new com.navdy.hud.app.maps.here.HereMapsManager$2$1(this));
        com.navdy.hud.app.maps.here.HereMapsManager.access$2502(this.this$0, 0);
        com.navdy.hud.app.maps.here.HereMapsManager.access$2700(this.this$0);
    }
}
