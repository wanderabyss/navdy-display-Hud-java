package com.navdy.hud.app.maps.util;
import com.navdy.hud.app.R;

final public class MapUtils {
    final public static float INVALID_DISTANCE = -1f;
    final private static double MAX_ALTITUDE = 10000.0;
    final private static double MAX_LATITUDE = 90.0;
    final private static double MAX_LONGITUDE = 180.0;
    final private static double MIN_ALTITUDE = -10000.0;
    final private static double MIN_LATITUDE = -90.0;
    final private static double MIN_LONGITUDE = -180.0;
    final public static int SECONDS_PER_HOUR = 3600;
    final public static int SECONDS_PER_MINUTE = 60;
    
    private MapUtils() {
    }
    
    public static float distanceBetween(com.navdy.service.library.events.location.LatLong a, com.navdy.service.library.events.location.LatLong a0) {
        float f = 0.0f;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a0 != null) {
                        break label0;
                    }
                }
                f = -1f;
                break label2;
            }
            float[] a1 = new float[1];
            android.location.Location.distanceBetween(a.latitude.doubleValue(), a.longitude.doubleValue(), a0.latitude.doubleValue(), a0.longitude.doubleValue(), a1);
            f = a1[0];
        }
        return f;
    }
    
    public static String formatTime(int i) {
        String s = null;
        if (i < 3600) {
            Object[] a = new Object[1];
            a[0] = Integer.valueOf(i / 60);
            s = String.format("%d", a);
        } else {
            Object[] a0 = new Object[2];
            a0[0] = Integer.valueOf(i / 3600);
            a0[1] = Integer.valueOf(i / 60);
            s = String.format("%d:%02d", a0);
        }
        return s;
    }
    
    public static int getIconForDestination(com.navdy.service.library.events.destination.Destination$FavoriteType a) {
        return R.drawable.icon_pin_route_circle;
    }
    
    public static int getMinuteCeiling(int i) {
        if (i % 60 != 0) {
            i = i + 60 - i % 60;
        }
        return i;
    }
    
    public static com.here.android.mpa.common.GeoCoordinate sanitizeCoords(com.here.android.mpa.common.GeoCoordinate a) {
        double d = a.getLatitude();
        double d0 = a.getLongitude();
        double d1 = a.getAltitude();
        label0: if (com.navdy.hud.app.maps.util.MapUtils.sanitizeCoords(d, d0)) {
            int i = (d1 < -10000.0) ? -1 : (d1 == -10000.0) ? 0 : 1;
            label1: {
                if (i < 0) {
                    break label1;
                }
                if (!(d1 > 10000.0)) {
                    break label0;
                }
            }
            a.setAltitude(0.0);
        } else {
            a = null;
        }
        return a;
    }
    
    public static boolean sanitizeCoords(double d, double d0) {
        boolean b = false;
        int i = (d < 90.0) ? -1 : (d == 90.0) ? 0 : 1;
        label2: {
            label0: {
                label1: {
                    if (i > 0) {
                        break label1;
                    }
                    if (!(d >= -90.0)) {
                        break label1;
                    }
                    if (!(d0 <= 180.0)) {
                        break label1;
                    }
                    if (d0 >= -180.0) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
}
