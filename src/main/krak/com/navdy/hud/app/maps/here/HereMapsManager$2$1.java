package com.navdy.hud.app.maps.here;

class HereMapsManager$2$1 implements com.here.android.mpa.odml.MapLoader$Listener {
    final com.navdy.hud.app.maps.here.HereMapsManager$2 this$1;
    
    HereMapsManager$2$1(com.navdy.hud.app.maps.here.HereMapsManager$2 a) {
        super();
        this.this$1 = a;
    }
    
    public void onCheckForUpdateComplete(boolean b, String s, String s0, com.here.android.mpa.odml.MapLoader$ResultCode a) {
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v(new StringBuilder().append("MapLoader: updateAvailable[").append(b).append("] currentMapVersion[").append(s).append("] newestMapVersion[").append(s0).append("] resultCode[").append(a).append("]").toString());
    }
    
    public void onGetMapPackagesComplete(com.here.android.mpa.odml.MapPackage a, com.here.android.mpa.odml.MapLoader$ResultCode a0) {
        if (a0 == com.here.android.mpa.odml.MapLoader$ResultCode.OPERATION_SUCCESSFUL) {
            com.navdy.hud.app.maps.here.HereMapsManager.access$100().v("initMapLoader: get map package success");
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereMapsManager$2$1$1(this, a), 2);
        } else {
            com.navdy.hud.app.maps.here.HereMapsManager.access$100().e(new StringBuilder().append("initMapLoader: get map package failed:").append(a0.name()).toString());
        }
    }
    
    public void onInstallMapPackagesComplete(com.here.android.mpa.odml.MapPackage a, com.here.android.mpa.odml.MapLoader$ResultCode a0) {
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v(new StringBuilder().append("MapLoader: onInstallMapPackagesComplete result[").append(a0.name()).append("] package[").append((a == null) ? "null" : a.getTitle()).append("]").toString());
    }
    
    public void onInstallationSize(long j, long j0) {
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v(new StringBuilder().append("MapLoader: onInstallationSize disk[").append(j).append("] network[").append(j0).append("]").toString());
    }
    
    public void onPerformMapDataUpdateComplete(com.here.android.mpa.odml.MapPackage a, com.here.android.mpa.odml.MapLoader$ResultCode a0) {
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v(new StringBuilder().append("MapLoader: onPerformMapDataUpdateComplete result[").append(a0.name()).append("] package[").append((a == null) ? "null" : a.getTitle()).append("]").toString());
    }
    
    public void onProgress(int i) {
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v(new StringBuilder().append("MapLoader: progress[").append(i).append("]").toString());
    }
    
    public void onUninstallMapPackagesComplete(com.here.android.mpa.odml.MapPackage a, com.here.android.mpa.odml.MapLoader$ResultCode a0) {
    }
}
