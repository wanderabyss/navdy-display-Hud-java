package com.navdy.hud.app.util;

public class CrashReporter$KernelCrashException extends com.navdy.hud.app.util.CrashReporter$CrashException {
    final private static java.util.regex.Pattern stackMatcher;
    
    static {
        stackMatcher = java.util.regex.Pattern.compile("PC is at\\s+([^\n]+\n)");
    }
    
    public CrashReporter$KernelCrashException(String s) {
        super(s);
    }
    
    public java.util.regex.Pattern getLocationPattern() {
        return stackMatcher;
    }
}
