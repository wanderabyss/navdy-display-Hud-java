package com.navdy.hud.app.util;

public class CrashReporter$NavdyCrashListener extends net.hockeyapp.android.CrashManagerListener {
    public Throwable exception;
    
    public CrashReporter$NavdyCrashListener() {
        this.exception = null;
    }
    
    public String getContact() {
        return com.navdy.hud.app.util.CrashReporter.access$100().getLastUserEmail();
    }
    
    public String getDescription() {
        String s = null;
        if (this.exception instanceof com.navdy.hud.app.util.CrashReporter$TombstoneException) {
            s = ((com.navdy.hud.app.util.CrashReporter$TombstoneException)this.exception).description;
        } else if (this.exception instanceof com.navdy.hud.app.util.CrashReporter$AnrException) {
            s = com.navdy.service.library.util.LogUtils.systemLogStr(32768, "Wrote stack traces to '/data/anr/traces.txt'");
        } else if (this.exception instanceof com.navdy.hud.app.util.CrashReporter$KernelCrashException) {
            s = com.navdy.service.library.util.LogUtils.systemLogStr(32768, "--------- beginning of main");
        } else if (this.exception instanceof com.navdy.hud.app.util.OTAUpdateService$OTAFailedException) {
            com.navdy.hud.app.util.OTAUpdateService$OTAFailedException a = (com.navdy.hud.app.util.OTAUpdateService$OTAFailedException)this.exception;
            s = new StringBuilder().append(a.last_install).append("\n").append(a.last_log).toString();
        } else {
            s = com.navdy.service.library.util.LogUtils.systemLogStr(32768, (String)null);
        }
        return s;
    }
    
    public String getUserID() {
        return com.navdy.hud.app.util.CrashReporter.access$000();
    }
    
    public void saveException(Throwable a) {
        this.exception = a;
        net.hockeyapp.android.ExceptionHandler.saveException(a, (net.hockeyapp.android.CrashManagerListener)this);
        this.exception = null;
    }
    
    public boolean shouldAutoUploadCrashes() {
        return true;
    }
}
