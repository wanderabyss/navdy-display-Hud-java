package com.navdy.hud.app.view;


public enum MainView$AnimationMode {
    MOVE_LEFT_SHRINK(0),
    RIGHT_EXPAND(1);

    private int value;
    MainView$AnimationMode(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class MainView$AnimationMode extends Enum {
//    final private static com.navdy.hud.app.view.MainView$AnimationMode[] $VALUES;
//    final public static com.navdy.hud.app.view.MainView$AnimationMode MOVE_LEFT_SHRINK;
//    final public static com.navdy.hud.app.view.MainView$AnimationMode RIGHT_EXPAND;
//    
//    static {
//        MOVE_LEFT_SHRINK = new com.navdy.hud.app.view.MainView$AnimationMode("MOVE_LEFT_SHRINK", 0);
//        RIGHT_EXPAND = new com.navdy.hud.app.view.MainView$AnimationMode("RIGHT_EXPAND", 1);
//        com.navdy.hud.app.view.MainView$AnimationMode[] a = new com.navdy.hud.app.view.MainView$AnimationMode[2];
//        a[0] = MOVE_LEFT_SHRINK;
//        a[1] = RIGHT_EXPAND;
//        $VALUES = a;
//    }
//    
//    private MainView$AnimationMode(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.view.MainView$AnimationMode valueOf(String s) {
//        return (com.navdy.hud.app.view.MainView$AnimationMode)Enum.valueOf(com.navdy.hud.app.view.MainView$AnimationMode.class, s);
//    }
//    
//    public static com.navdy.hud.app.view.MainView$AnimationMode[] values() {
//        return $VALUES.clone();
//    }
//}
//