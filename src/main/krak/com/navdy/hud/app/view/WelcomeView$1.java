package com.navdy.hud.app.view;

class WelcomeView$1 implements Runnable {
    final com.navdy.hud.app.view.WelcomeView this$0;
    
    WelcomeView$1(com.navdy.hud.app.view.WelcomeView a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        if (com.navdy.hud.app.view.WelcomeView.access$000(this.this$0) != -1) {
            int i = com.navdy.hud.app.view.WelcomeView.access$100(this.this$0);
            android.widget.TextView a = null;
            android.widget.TextView a0 = null;
            android.widget.ImageView a1 = null;
            android.widget.ImageView a2 = null;
            android.widget.ImageView a3 = null;
            android.widget.ImageView a4 = null;
            switch(i) {
                case 1: {
                    a = this.this$0.downloadAppTextView2;
                    a0 = this.this$0.downloadAppTextView1;
                    a1 = this.this$0.appStoreImage2;
                    a2 = this.this$0.appStoreImage1;
                    a3 = this.this$0.playStoreImage2;
                    a4 = this.this$0.playStoreImage1;
                    com.navdy.hud.app.view.WelcomeView.access$102(this.this$0, 0);
                    break;
                }
                case 0: {
                    a = this.this$0.downloadAppTextView1;
                    a0 = this.this$0.downloadAppTextView2;
                    a1 = this.this$0.appStoreImage1;
                    a2 = this.this$0.appStoreImage2;
                    a3 = this.this$0.playStoreImage1;
                    a4 = this.this$0.playStoreImage2;
                    com.navdy.hud.app.view.WelcomeView.access$102(this.this$0, 1);
                    break;
                }
            }
            a.animate().alpha(0.0f).setDuration((long)com.navdy.hud.app.view.WelcomeView.access$200());
            a0.animate().alpha(1f).setDuration((long)com.navdy.hud.app.view.WelcomeView.access$200()).withEndAction((Runnable)new com.navdy.hud.app.view.WelcomeView$1$1(this, a, a1, a3));
            a1.animate().alpha(0.0f).setDuration((long)com.navdy.hud.app.view.WelcomeView.access$200());
            a2.animate().alpha(1f).setDuration((long)com.navdy.hud.app.view.WelcomeView.access$200());
            a3.animate().alpha(0.0f).setDuration((long)com.navdy.hud.app.view.WelcomeView.access$200());
            a4.animate().alpha(1f).setDuration((long)com.navdy.hud.app.view.WelcomeView.access$200());
        }
    }
}
