package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class GaugeView$$ViewInjector {
    public GaugeView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.GaugeView a0, Object a1) {
        com.navdy.hud.app.view.DashboardWidgetView$$ViewInjector.inject(a, (com.navdy.hud.app.view.DashboardWidgetView)a0, a1);
        a0.mTvValue = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_value, "field 'mTvValue'");
        a0.mTvUnit = (android.widget.TextView)a.findOptionalView(a1, R.id.txt_unit);
    }
    
    public static void reset(com.navdy.hud.app.view.GaugeView a) {
        com.navdy.hud.app.view.DashboardWidgetView$$ViewInjector.reset((com.navdy.hud.app.view.DashboardWidgetView)a);
        a.mTvValue = null;
        a.mTvUnit = null;
    }
}
