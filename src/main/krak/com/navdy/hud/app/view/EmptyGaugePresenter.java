package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class EmptyGaugePresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    final public static int ALPHA_ANIMATION_DURATION = 2000;
    final public static int ANIMATION_DELAY = 1000;
    private String emptyGaugeName;
    private Runnable mAnimationRunnable;
    @InjectView(R.id.txt_empty_gauge)
    android.widget.TextView mEmptyGaugeText;
    private android.os.Handler mHandler;
    private boolean mPlayedAnimation;
    private android.animation.ValueAnimator mTextAlphaAnimator;
    
    public EmptyGaugePresenter(android.content.Context a) {
        this.mPlayedAnimation = false;
        this.mHandler = new android.os.Handler();
        this.mTextAlphaAnimator = new android.animation.ValueAnimator();
        android.animation.ValueAnimator a0 = this.mTextAlphaAnimator;
        float[] a1 = new float[2];
        a1[0] = 1f;
        a1[1] = 0.0f;
        a0.setFloatValues(a1);
        this.mTextAlphaAnimator.setDuration(2000L);
        this.emptyGaugeName = a.getString(R.string.widget_empty);
        this.mTextAlphaAnimator.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.view.EmptyGaugePresenter$1(this));
        this.mAnimationRunnable = (Runnable)new com.navdy.hud.app.view.EmptyGaugePresenter$2(this);
    }
    
    static android.animation.ValueAnimator access$000(com.navdy.hud.app.view.EmptyGaugePresenter a) {
        return a.mTextAlphaAnimator;
    }
    
    public android.graphics.drawable.Drawable getDrawable() {
        return null;
    }
    
    public String getWidgetIdentifier() {
        return "EMPTY_WIDGET";
    }
    
    public String getWidgetName() {
        return this.emptyGaugeName;
    }
    
    public void setView(com.navdy.hud.app.view.DashboardWidgetView a, android.os.Bundle a0) {
        if (a == null) {
            this.mPlayedAnimation = false;
            super.setView(a, a0);
        } else {
            a.setContentView(R.layout.empty_gauge_layout);
            butterknife.ButterKnife.inject(this, (android.view.View)a);
            if (a0 != null) {
                if (a0.getBoolean("EXTRA_IS_ACTIVE", false)) {
                    if (!this.mPlayedAnimation) {
                        this.mPlayedAnimation = true;
                        this.mTextAlphaAnimator.end();
                        this.mHandler.removeCallbacks(this.mAnimationRunnable);
                        this.mEmptyGaugeText.setAlpha(1f);
                        this.mHandler.postDelayed(this.mAnimationRunnable, 1000L);
                    }
                } else {
                    this.mPlayedAnimation = false;
                    this.mTextAlphaAnimator.end();
                    this.mHandler.removeCallbacks(this.mAnimationRunnable);
                    this.mEmptyGaugeText.setAlpha(1f);
                }
            }
            super.setView(a, a0);
        }
    }
    
    protected void updateGauge() {
    }
}
