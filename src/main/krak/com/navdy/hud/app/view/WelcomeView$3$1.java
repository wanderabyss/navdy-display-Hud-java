package com.navdy.hud.app.view;

class WelcomeView$3$1 implements Runnable {
    final com.navdy.hud.app.view.WelcomeView$3 this$1;
    final com.navdy.service.library.device.NavdyDeviceId val$deviceId;
    final java.io.File val$imagePath;
    final String val$initials;
    final com.navdy.hud.app.ui.component.image.InitialsImageView val$initialsImageView;
    final android.widget.ImageView val$view;
    
    WelcomeView$3$1(com.navdy.hud.app.view.WelcomeView$3 a, android.widget.ImageView a0, com.navdy.service.library.device.NavdyDeviceId a1, java.io.File a2, com.navdy.hud.app.ui.component.image.InitialsImageView a3, String s) {
        super();
        this.this$1 = a;
        this.val$view = a0;
        this.val$deviceId = a1;
        this.val$imagePath = a2;
        this.val$initialsImageView = a3;
        this.val$initials = s;
    }
    
    public void run() {
        if (this.val$view.getTag() == this.val$deviceId) {
            boolean b = this.val$imagePath.exists();
            com.navdy.hud.app.view.WelcomeView.access$500(this.this$1.this$0).post((Runnable)new com.navdy.hud.app.view.WelcomeView$3$1$1(this, b));
        }
    }
}
