package com.navdy.hud.app.view;

public class MapMask extends android.view.View {
    final private static String TAG;
    private int bottom;
    private android.graphics.Paint fadePaint;
    private android.graphics.Path fadePath;
    private int left;
    private int mHorizontalRadius;
    private com.navdy.hud.app.util.CustomDimension mHorizontalRadiusAttribute;
    private com.navdy.hud.app.view.IndicatorView mIndicator;
    private int mMaskColor;
    private int mVerticalRadius;
    private com.navdy.hud.app.util.CustomDimension mVerticalRadiusAttribute;
    private android.graphics.Paint maskPaint;
    private android.graphics.Path maskPath;
    private int right;
    private int top;
    
    static {
        TAG = com.navdy.hud.app.view.MapMask.class.getSimpleName();
    }
    
    public MapMask(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public MapMask(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public MapMask(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.initFromAttributes(a, a0);
        this.mIndicator = new com.navdy.hud.app.view.IndicatorView(a, a0);
        this.initDrawingTools();
    }
    
    private void evaluateDimensions(int i, int i0) {
        this.mHorizontalRadius = (int)this.mHorizontalRadiusAttribute.getSize((android.view.View)this, (float)i, 0.0f);
        this.mVerticalRadius = (int)this.mVerticalRadiusAttribute.getSize((android.view.View)this, (float)i0, 0.0f);
        this.mIndicator.evaluateDimensions(this.mHorizontalRadius * 2, i0);
    }
    
    private void initDrawingTools() {
        this.maskPath = new android.graphics.Path();
        this.maskPaint = new android.graphics.Paint();
        this.maskPaint.setColor(this.mMaskColor);
        this.maskPaint.setAntiAlias(true);
        this.maskPaint.setStyle(android.graphics.Paint$Style.FILL_AND_STROKE);
        int i = this.getWidth();
        int i0 = this.getHeight();
        this.maskPath.moveTo(0.0f, 0.0f);
        this.maskPath.lineTo(0.0f, (float)i0);
        this.maskPath.lineTo((float)i, (float)i0);
        this.maskPath.lineTo((float)i, 0.0f);
        this.maskPath.close();
        this.top = (i0 - this.mVerticalRadius) / 2;
        this.bottom = this.top + this.mVerticalRadius;
        int i1 = this.bottom;
        int i2 = this.mVerticalRadius;
        this.left = (i - this.mHorizontalRadius * 2) / 2;
        this.right = this.left + this.mHorizontalRadius * 2;
        this.maskPath.moveTo((float)this.left, (float)this.bottom);
        android.graphics.RectF a = new android.graphics.RectF((float)this.left, (float)this.top, (float)this.right, (float)(i1 + i2));
        this.maskPath.arcTo(a, 180f, 180f, false);
        com.navdy.hud.app.view.IndicatorView.drawIndicatorPath(this.maskPath, (float)this.left, (float)this.bottom, this.mIndicator.getCurveRadius(), (float)this.mIndicator.getIndicatorWidth(), (float)this.mIndicator.getIndicatorHeight(), (float)(this.mHorizontalRadius * 2));
        this.maskPath.setFillType(android.graphics.Path$FillType.EVEN_ODD);
        float f = this.mIndicator.getStrokeWidth();
        float f0 = (float)this.mHorizontalRadius;
        if (f0 < 60f) {
            f0 = 60f;
        }
        this.fadePaint = new android.graphics.Paint();
        this.fadePaint.setStrokeWidth(60f);
        this.fadePaint.setAntiAlias(true);
        this.fadePaint.setStrokeCap(android.graphics.Paint$Cap.BUTT);
        this.fadePaint.setStyle(android.graphics.Paint$Style.STROKE);
        float f1 = (f0 - 60f) / f0;
        android.graphics.Paint a0 = this.fadePaint;
        float f2 = (float)this.left;
        float f3 = (float)this.bottom;
        android.graphics.Shader$TileMode a1 = android.graphics.Shader$TileMode.CLAMP;
        float f4 = f2 + f0;
        float f5 = f3 + f;
        int[] a2 = new int[3];
        a2[0] = 0;
        a2[1] = 0;
        a2[2] = -16777216;
        float[] a3 = new float[3];
        a3[0] = 0.0f;
        a3[1] = f1;
        a3[2] = 1f;
        a0.setShader((android.graphics.Shader)new android.graphics.RadialGradient(f4, f5, f0, a2, a3, a1));
        this.fadePath = new android.graphics.Path();
        this.fadePath.moveTo((float)this.left, (float)this.bottom);
        a.inset(60f / 2f - f, 60f / 2f - f);
        a.top = a.top + f;
        a.bottom = a.bottom + f;
        this.fadePath.arcTo(a, 180f, 180f, false);
    }
    
    private void initFromAttributes(android.content.Context a, android.util.AttributeSet a0) {
        android.content.res.TypedArray a1 = a.getTheme().obtainStyledAttributes(a0, com.navdy.hud.app.R$styleable.MapMask, 0, 0);
        try {
            this.mHorizontalRadiusAttribute = com.navdy.hud.app.util.CustomDimension.getDimension((android.view.View)this, a1, 0, 0.0f);
            this.mVerticalRadiusAttribute = com.navdy.hud.app.util.CustomDimension.getDimension((android.view.View)this, a1, 1, 0.0f);
            this.mMaskColor = a1.getColor(2, -16777216);
        } catch(Throwable a2) {
            a1.recycle();
            throw a2;
        }
        a1.recycle();
    }
    
    public android.graphics.PointF getIndicatorPoint() {
        return new android.graphics.PointF((float)(this.left + this.mHorizontalRadius), (float)(this.bottom - this.mIndicator.getIndicatorHeight()));
    }
    
    public void layout(int i, int i0, int i1, int i2) {
        super.layout(i, i0, i1, i2);
        this.mIndicator.layout(this.left, 0, this.right, this.mIndicator.getIndicatorHeight());
    }
    
    protected void onDraw(android.graphics.Canvas a) {
        a.drawPath(this.maskPath, this.maskPaint);
        int i = this.bottom - this.mIndicator.getIndicatorHeight();
        a.translate((float)this.left, (float)i);
        this.mIndicator.draw(a);
        a.translate((float)(-this.left), (float)(-i));
        a.drawPath(this.fadePath, this.fadePaint);
    }
    
    protected void onSizeChanged(int i, int i0, int i1, int i2) {
        this.evaluateDimensions(i, i0);
        this.initDrawingTools();
    }
}
