package com.navdy.hud.app.view;

final public class GestureVideoCaptureView$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding gestureService;
    private dagger.internal.Binding mPresenter;
    
    public GestureVideoCaptureView$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.view.GestureVideoCaptureView", false, com.navdy.hud.app.view.GestureVideoCaptureView.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.mPresenter = a.requestBinding("com.navdy.hud.app.screen.GestureLearningScreen$Presenter", com.navdy.hud.app.view.GestureVideoCaptureView.class, (this).getClass().getClassLoader());
        this.gestureService = a.requestBinding("com.navdy.hud.app.gesture.GestureServiceConnector", com.navdy.hud.app.view.GestureVideoCaptureView.class, (this).getClass().getClassLoader());
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.view.GestureVideoCaptureView.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.mPresenter);
        a0.add(this.gestureService);
        a0.add(this.bus);
    }
    
    public void injectMembers(com.navdy.hud.app.view.GestureVideoCaptureView a) {
        a.mPresenter = (com.navdy.hud.app.screen.GestureLearningScreen$Presenter)this.mPresenter.get();
        a.gestureService = (com.navdy.hud.app.gesture.GestureServiceConnector)this.gestureService.get();
        a.bus = (com.squareup.otto.Bus)this.bus.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.view.GestureVideoCaptureView)a);
    }
}
