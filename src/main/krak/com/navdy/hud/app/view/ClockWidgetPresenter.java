package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class ClockWidgetPresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    final private static int TIME_UPDATE_INTERVAL = 30000;
    private StringBuilder amPmMarker;
    private com.navdy.hud.app.view.drawable.AnalogClockDrawable analogClockDrawable;
    private String analogClockWidgetName;
    private com.squareup.otto.Bus bus;
    private String dateText;
    private int dayOfTheMonth;
    private int dayOfTheWeek;
    private String dayText;
    private String digitalClock2WidgetName;
    private String digitalClockWidgetName;
    private com.navdy.hud.app.view.ClockWidgetPresenter$ClockType mClockType;
    private android.content.Context mContext;
    private android.os.Handler mHandler;
    private com.navdy.hud.app.view.drawable.SmallAnalogClockDrawable smallAnalogClockDrawable;
    private com.navdy.hud.app.common.TimeHelper timeHelper;
    private int timeHour;
    private int timeMinute;
    private int timeSeconds;
    private String timeText;
    private Runnable updateTimeRunnable;
    
    public ClockWidgetPresenter(android.content.Context a, com.navdy.hud.app.view.ClockWidgetPresenter$ClockType a0) {
        this.mHandler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.amPmMarker = new StringBuilder();
        this.mClockType = a0;
        this.mContext = a;
        this.timeHelper = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper();
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        this.analogClockWidgetName = a.getResources().getString(R.string.widget_clock_analog);
        this.digitalClockWidgetName = a.getResources().getString(R.string.widget_clock_digital);
        this.digitalClock2WidgetName = a.getResources().getString(R.string.widget_clock_time_and_date);
        this.updateTimeRunnable = (Runnable)new com.navdy.hud.app.view.ClockWidgetPresenter$1(this);
        switch(com.navdy.hud.app.view.ClockWidgetPresenter$2.$SwitchMap$com$navdy$hud$app$view$ClockWidgetPresenter$ClockType[a0.ordinal()]) {
            case 2: {
                this.smallAnalogClockDrawable = new com.navdy.hud.app.view.drawable.SmallAnalogClockDrawable(a);
                break;
            }
            case 1: {
                this.analogClockDrawable = new com.navdy.hud.app.view.drawable.AnalogClockDrawable(a);
                break;
            }
        }
    }
    
    static void access$000(com.navdy.hud.app.view.ClockWidgetPresenter a) {
        a.updateTime();
    }
    
    static Runnable access$100(com.navdy.hud.app.view.ClockWidgetPresenter a) {
        return a.updateTimeRunnable;
    }
    
    static int access$200(com.navdy.hud.app.view.ClockWidgetPresenter a) {
        return a.nextUpdateInterval();
    }
    
    static android.os.Handler access$300(com.navdy.hud.app.view.ClockWidgetPresenter a) {
        return a.mHandler;
    }
    
    private int nextUpdateInterval() {
        int i = (this.timeSeconds + 30) % 60;
        return ((i < 45) ? (i > 15) ? 30 : 30 - i : 30 + (60 - i)) * 1000;
    }
    
    private void updateTime() {
        java.util.Calendar a = java.util.Calendar.getInstance();
        java.util.Date a0 = new java.util.Date();
        a.setTime(a0);
        this.timeMinute = a.get(12);
        this.timeHour = a.get(11);
        this.timeSeconds = a.get(13);
        this.dayOfTheMonth = a.get(5);
        this.dayOfTheWeek = a.get(7);
        switch(com.navdy.hud.app.view.ClockWidgetPresenter$2.$SwitchMap$com$navdy$hud$app$view$ClockWidgetPresenter$ClockType[this.mClockType.ordinal()]) {
            case 3: {
                this.dateText = this.timeHelper.getDate();
                this.dayText = this.timeHelper.getDay();
                this.dateText = this.dateText.toUpperCase();
                this.dayText = this.dayText.toUpperCase();
            }
            case 2: {
                this.timeText = this.timeHelper.formatTime(a0, this.amPmMarker);
            }
            default: {
                this.reDraw();
                return;
            }
        }
    }
    
    public android.graphics.drawable.Drawable getDrawable() {
        com.navdy.hud.app.view.drawable.CustomDrawable a = null;
        switch(com.navdy.hud.app.view.ClockWidgetPresenter$2.$SwitchMap$com$navdy$hud$app$view$ClockWidgetPresenter$ClockType[this.mClockType.ordinal()]) {
            case 2: {
                a = this.smallAnalogClockDrawable;
                break;
            }
            case 1: {
                a = this.analogClockDrawable;
                break;
            }
            default: {
                a = null;
            }
        }
        return a;
    }
    
    public String getWidgetIdentifier() {
        String s = null;
        switch(com.navdy.hud.app.view.ClockWidgetPresenter$2.$SwitchMap$com$navdy$hud$app$view$ClockWidgetPresenter$ClockType[this.mClockType.ordinal()]) {
            case 3: {
                s = "DIGITAL_CLOCK_2_WIDGET";
                break;
            }
            case 2: {
                s = "DIGITAL_CLOCK_WIDGET";
                break;
            }
            case 1: {
                s = "ANALOG_CLOCK_WIDGET";
                break;
            }
            default: {
                s = null;
            }
        }
        return s;
    }
    
    public String getWidgetName() {
        String s = null;
        switch(com.navdy.hud.app.view.ClockWidgetPresenter$2.$SwitchMap$com$navdy$hud$app$view$ClockWidgetPresenter$ClockType[this.mClockType.ordinal()]) {
            case 3: {
                s = this.digitalClock2WidgetName;
                break;
            }
            case 2: {
                s = this.digitalClockWidgetName;
                break;
            }
            case 1: {
                s = this.analogClockWidgetName;
                break;
            }
            default: {
                s = null;
            }
        }
        return s;
    }
    
    protected boolean isRegisteringToBusRequired() {
        return true;
    }
    
    public void onUpdateClock(com.navdy.hud.app.common.TimeHelper$UpdateClock a) {
        this.updateTime();
    }
    
    public void setView(com.navdy.hud.app.view.DashboardWidgetView a, android.os.Bundle a0) {
        if (a == null) {
            this.mHandler.removeCallbacks(this.updateTimeRunnable);
            super.setView(a, a0);
        } else {
            switch(com.navdy.hud.app.view.ClockWidgetPresenter$2.$SwitchMap$com$navdy$hud$app$view$ClockWidgetPresenter$ClockType[this.mClockType.ordinal()]) {
                case 3: {
                    a.setContentView(R.layout.digital_clock2_layout);
                    break;
                }
                case 2: {
                    a.setContentView(R.layout.digital_clock1_layout);
                    break;
                }
                case 1: {
                    a.setContentView(R.layout.smart_dash_widget_circular_content_layout);
                    break;
                }
            }
            super.setView(a, a0);
            this.mHandler.removeCallbacks(this.updateTimeRunnable);
            this.updateTime();
            this.mHandler.postDelayed(this.updateTimeRunnable, (long)this.nextUpdateInterval());
        }
    }
    
    protected void updateGauge() {
        if (this.mWidgetView != null) {
            switch(com.navdy.hud.app.view.ClockWidgetPresenter$2.$SwitchMap$com$navdy$hud$app$view$ClockWidgetPresenter$ClockType[this.mClockType.ordinal()]) {
                case 3: {
                    android.widget.TextView a = (android.widget.TextView)this.mWidgetView.findViewById(R.id.txt_value);
                    android.widget.TextView a0 = (android.widget.TextView)this.mWidgetView.findViewById(R.id.txt_unit);
                    a.setText((CharSequence)this.timeText);
                    a0.setText((CharSequence)this.amPmMarker.toString());
                    ((android.widget.TextView)this.mWidgetView.findViewById(R.id.txt_date)).setText((CharSequence)this.dateText);
                    ((android.widget.TextView)this.mWidgetView.findViewById(R.id.txt_day)).setText((CharSequence)this.dayText);
                    break;
                }
                case 2: {
                    this.smallAnalogClockDrawable.setTime(this.timeHour, this.timeMinute, this.timeSeconds);
                    android.widget.TextView a1 = (android.widget.TextView)this.mWidgetView.findViewById(R.id.txt_value);
                    ((android.widget.TextView)this.mWidgetView.findViewById(R.id.txt_unit)).setText((CharSequence)this.amPmMarker.toString());
                    a1.setText((CharSequence)this.timeText);
                    break;
                }
                case 1: {
                    this.analogClockDrawable.setTime(this.dayOfTheMonth, this.timeHour, this.timeMinute, 0);
                    break;
                }
            }
        }
    }
}
