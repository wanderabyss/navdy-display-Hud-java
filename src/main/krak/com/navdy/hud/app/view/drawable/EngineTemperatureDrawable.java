package com.navdy.hud.app.view.drawable;
import com.navdy.hud.app.R;

final public class EngineTemperatureDrawable extends com.navdy.hud.app.view.drawable.GaugeDrawable {
    private int mBackgroundColor;
    private int mFuelGaugeWidth;
    private boolean mLeftOriented;
    
    public EngineTemperatureDrawable(android.content.Context a, int i) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        super(a, 0, i);
        this.mLeftOriented = true;
        this.mFuelGaugeWidth = a.getResources().getDimensionPixelSize(R.dimen.fuel_gauge_width);
        this.mBackgroundColor = this.mColorTable[3];
    }
    
    public void draw(android.graphics.Canvas a) {
        super.draw(a);
        android.graphics.Rect a0 = this.getBounds();
        android.graphics.RectF a1 = (this.mLeftOriented) ? new android.graphics.RectF((float)a0.left, (float)a0.top, (float)(a0.right + a0.width()), (float)a0.bottom) : new android.graphics.RectF((float)(a0.left - a0.width()), (float)a0.top, (float)a0.right, (float)a0.bottom);
        a1.inset((float)(this.mFuelGaugeWidth / 2 + 1), (float)(this.mFuelGaugeWidth / 2 + 1));
        this.mPaint.setStyle(android.graphics.Paint$Style.STROKE);
        this.mPaint.setStrokeWidth((float)this.mFuelGaugeWidth);
        this.mPaint.setColor(this.mBackgroundColor);
        int i = (this.mLeftOriented) ? 90 : 270;
        if (a != null) {
            a.drawArc(a1, (float)i, (float)180, false, this.mPaint);
        }
        this.mPaint.setColor(this.mDefaultColor);
        int i0 = (int)((this.mValue - this.mMinValue) / (this.mMaxValue - this.mMinValue) * (float)180);
        int i1 = (this.mLeftOriented) ? 90 : 450 - i0;
        boolean b = this.mLeftOriented;
        if (b) {
            if (a != null) {
                a.drawArc(a1, (float)i1, (float)i0 - 5f, false, this.mPaint);
            }
        } else if (!b && a != null) {
            a.drawArc(a1, (float)i1 + 5f, (float)i0, false, this.mPaint);
        }
        this.mPaint.setColor(-16777216);
        boolean b0 = this.mLeftOriented;
        if (b0) {
            if (a != null) {
                float f = (float)i1;
                a.drawArc(a1, (float)i0 + f, -5f, false, this.mPaint);
            }
        } else if (!b0 && a != null) {
            a.drawArc(a1, (float)i1, 5f, false, this.mPaint);
        }
    }
    
    final public void setLeftOriented(boolean b) {
        this.mLeftOriented = b;
    }
}
