package com.navdy.hud.app.view;

class GestureLearningView$7 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final com.navdy.hud.app.view.GestureLearningView this$0;
    final boolean val$rightSide;
    final android.view.ViewGroup val$view;
    
    GestureLearningView$7(com.navdy.hud.app.view.GestureLearningView a, android.view.ViewGroup a0, boolean b) {
        super();
        this.this$0 = a;
        this.val$view = a0;
        this.val$rightSide = b;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        android.view.ViewGroup$MarginLayoutParams a0 = (android.view.ViewGroup$MarginLayoutParams)this.val$view.getLayoutParams();
        int i = ((Integer)a.getAnimatedValue()).intValue();
        if (this.val$rightSide) {
            a0.leftMargin = i;
        } else {
            a0.rightMargin = i;
        }
        this.val$view.setLayoutParams((android.view.ViewGroup$LayoutParams)a0);
    }
}
