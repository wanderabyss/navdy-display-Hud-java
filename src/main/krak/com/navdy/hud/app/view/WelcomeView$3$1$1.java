package com.navdy.hud.app.view;

class WelcomeView$3$1$1 implements Runnable {
    final com.navdy.hud.app.view.WelcomeView$3$1 this$2;
    final boolean val$imageExists;
    
    WelcomeView$3$1$1(com.navdy.hud.app.view.WelcomeView$3$1 a, boolean b) {
        super();
        this.this$2 = a;
        this.val$imageExists = b;
    }
    
    public void run() {
        if (this.this$2.val$view.getTag() == this.this$2.val$deviceId) {
            if (this.val$imageExists) {
                com.navdy.hud.app.view.WelcomeView.access$1000(this.this$2.this$1.this$0, this.this$2.val$imagePath, (android.widget.ImageView)this.this$2.val$initialsImageView, (com.squareup.picasso.Callback)null);
            } else {
                this.this$2.val$initialsImageView.setInitials(this.this$2.val$initials, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.LARGE);
            }
        }
    }
}
