package com.navdy.hud.app.view;

final public class MainView$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding preferences;
    private dagger.internal.Binding presenter;
    private dagger.internal.Binding uiStateManager;
    
    public MainView$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.view.MainView", false, com.navdy.hud.app.view.MainView.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.presenter = a.requestBinding("com.navdy.hud.app.ui.activity.Main$Presenter", com.navdy.hud.app.view.MainView.class, (this).getClass().getClassLoader());
        this.uiStateManager = a.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.view.MainView.class, (this).getClass().getClassLoader());
        this.preferences = a.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.view.MainView.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.presenter);
        a0.add(this.uiStateManager);
        a0.add(this.preferences);
    }
    
    public void injectMembers(com.navdy.hud.app.view.MainView a) {
        a.presenter = (com.navdy.hud.app.ui.activity.Main$Presenter)this.presenter.get();
        a.uiStateManager = (com.navdy.hud.app.ui.framework.UIStateManager)this.uiStateManager.get();
        a.preferences = (android.content.SharedPreferences)this.preferences.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.view.MainView)a);
    }
}
