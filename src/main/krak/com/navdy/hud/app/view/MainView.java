package com.navdy.hud.app.view;

public class MainView extends android.widget.FrameLayout implements com.navdy.hud.app.manager.InputManager$IInputHandler {
    final private static String COMPACT_MODE_SCALE_PROPERTY = "persist.sys.compact.scale";
    final private static float DEFAULT_COMPACT_MODE_SCALE = 0.87f;
    final private static int SHOW_OPTION_LAG = 350;
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.navdy.hud.app.ui.framework.AnimationQueue animationQueue;
    private int basePaddingTop;
    private android.content.SharedPreferences$OnSharedPreferenceChangeListener brightnessControl;
    @InjectView(R.id.container)
    com.navdy.hud.app.view.ContainerView containerView;
    @InjectView(R.id.expandedNotifCoverView)
    android.view.View expandedNotificationCoverView;
    @InjectView(R.id.expandedNotifView)
    android.widget.FrameLayout expandedNotificationView;
    private com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat format;
    private android.os.Handler handler;
    private boolean isNotificationCollapsing;
    private boolean isNotificationExpanding;
    private boolean isScreenAnimating;
    @InjectView(R.id.mainLowerView)
    com.navdy.hud.app.ui.component.main.MainLowerView mainLowerView;
    private int mainPanelWidth;
    @InjectView(R.id.notifIndicator)
    com.navdy.hud.app.ui.component.carousel.CarouselIndicator notifIndicator;
    @InjectView(R.id.notifScrollIndicator)
    com.navdy.hud.app.ui.component.carousel.ProgressIndicator notifScrollIndicator;
    private com.navdy.hud.app.ui.framework.INotificationAnimationListener notificationAnimationListener;
    @InjectView(R.id.notificationColorView)
    com.navdy.hud.app.ui.component.image.ColorImageView notificationColorView;
    @InjectView(R.id.notificationExtensionView)
    android.widget.FrameLayout notificationExtensionView;
    @InjectView(R.id.notification)
    com.navdy.hud.app.view.NotificationView notificationView;
    @Inject
    android.content.SharedPreferences preferences;
    @Inject
    com.navdy.hud.app.ui.activity.Main$Presenter presenter;
    private int rightPanelStart;
    private com.navdy.hud.app.ui.framework.IScreenAnimationListener screenAnimationListener;
    @InjectView(R.id.screenContainer)
    android.widget.FrameLayout screenContainer;
    private boolean showingNotificationExtension;
    private int sidePanelWidth;
    @InjectView(R.id.splitter)
    android.widget.FrameLayout splitterView;
    @InjectView(R.id.systemTray)
    com.navdy.hud.app.ui.component.SystemTrayView systemTray;
    @InjectView(R.id.toastView)
    com.navdy.hud.app.view.ToastView toastView;
    @Inject
    com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    private int verticalOffset;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.MainView.class);
    }
    
    public MainView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public MainView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public MainView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.basePaddingTop = -1;
        this.animationQueue = new com.navdy.hud.app.ui.framework.AnimationQueue();
        this.handler = new android.os.Handler();
        this.notificationAnimationListener = (com.navdy.hud.app.ui.framework.INotificationAnimationListener)new com.navdy.hud.app.view.MainView$1(this);
        this.screenAnimationListener = (com.navdy.hud.app.ui.framework.IScreenAnimationListener)new com.navdy.hud.app.view.MainView$2(this);
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
        this.initLayout();
    }
    
    static boolean access$002(com.navdy.hud.app.view.MainView a, boolean b) {
        a.isNotificationExpanding = b;
        return b;
    }
    
    static void access$1000(com.navdy.hud.app.view.MainView a, boolean b) {
        a.postNotificationCollapseEvent(b);
    }
    
    static boolean access$102(com.navdy.hud.app.view.MainView a, boolean b) {
        a.isNotificationCollapsing = b;
        return b;
    }
    
    static boolean access$1102(com.navdy.hud.app.view.MainView a, boolean b) {
        a.showingNotificationExtension = b;
        return b;
    }
    
    static boolean access$202(com.navdy.hud.app.view.MainView a, boolean b) {
        a.isScreenAnimating = b;
        return b;
    }
    
    static int access$300(com.navdy.hud.app.view.MainView a) {
        return a.basePaddingTop;
    }
    
    static int access$302(com.navdy.hud.app.view.MainView a, int i) {
        a.basePaddingTop = i;
        return i;
    }
    
    static int access$400(com.navdy.hud.app.view.MainView a) {
        return a.verticalOffset;
    }
    
    static int access$500(com.navdy.hud.app.view.MainView a) {
        return a.mainPanelWidth;
    }
    
    static int access$502(com.navdy.hud.app.view.MainView a, int i) {
        a.mainPanelWidth = i;
        return i;
    }
    
    static int access$600(com.navdy.hud.app.view.MainView a) {
        return a.sidePanelWidth;
    }
    
    static int access$602(com.navdy.hud.app.view.MainView a, int i) {
        a.sidePanelWidth = i;
        return i;
    }
    
    static com.navdy.service.library.log.Logger access$700() {
        return sLogger;
    }
    
    static int access$800(com.navdy.hud.app.view.MainView a) {
        return a.rightPanelStart;
    }
    
    static int access$802(com.navdy.hud.app.view.MainView a, int i) {
        a.rightPanelStart = i;
        return i;
    }
    
    static android.os.Handler access$900(com.navdy.hud.app.view.MainView a) {
        return a.handler;
    }
    
    private void addDebugMarkers(android.content.Context a) {
        int i = this.getResources().getColor(17170443);
        android.util.DisplayMetrics a0 = new android.util.DisplayMetrics();
        ((android.view.WindowManager)com.navdy.hud.app.HudApplication.getAppContext().getSystemService("window")).getDefaultDisplay().getMetrics(a0);
        int i0 = (int)android.util.TypedValue.applyDimension(1, 1f, a0);
        android.widget.FrameLayout$LayoutParams a1 = new android.widget.FrameLayout$LayoutParams(-1, i0);
        a1.gravity = 48;
        android.view.View a2 = new android.view.View(a);
        a2.setBackgroundColor(i);
        this.splitterView.addView(a2, (android.view.ViewGroup$LayoutParams)a1);
        android.widget.FrameLayout$LayoutParams a3 = new android.widget.FrameLayout$LayoutParams(-1, i0);
        a3.gravity = 80;
        android.view.View a4 = new android.view.View(a);
        a4.setBackgroundColor(i);
        this.splitterView.addView(a4, (android.view.ViewGroup$LayoutParams)a3);
    }
    
    private float compactScale() {
        return com.navdy.hud.app.util.os.SystemProperties.getFloat("persist.sys.compact.scale", 0.87f);
    }
    
    private android.animation.Animator getRemoveNotificationExtensionAnimator() {
        android.widget.FrameLayout a = this.getNotificationExtensionView();
        android.animation.Animator a0 = android.animation.AnimatorInflater.loadAnimator(((android.view.ViewGroup)a).getContext(), 17498113);
        a0.setTarget(a);
        a0.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.view.MainView$7(this, (android.view.ViewGroup)a));
        return a0;
    }
    
    private android.animation.ObjectAnimator getXPositionAnimator(android.view.ViewGroup a, int i) {
        float[] a0 = new float[1];
        a0[0] = (float)i;
        return android.animation.ObjectAnimator.ofFloat(a, "x", a0);
    }
    
    private void initLayout() {
        this.getViewTreeObserver().addOnGlobalLayoutListener((android.view.ViewTreeObserver$OnGlobalLayoutListener)new com.navdy.hud.app.view.MainView$3(this));
    }
    
    private android.animation.Animator middleAnimate(com.navdy.hud.app.view.MainView$AnimationMode a) {
        com.navdy.hud.app.view.MainView$CustomAnimationMode a0 = null;
        if (a != com.navdy.hud.app.view.MainView$AnimationMode.MOVE_LEFT_SHRINK) {
            com.navdy.hud.app.view.MainView$AnimationMode a1 = com.navdy.hud.app.view.MainView$AnimationMode.RIGHT_EXPAND;
            a0 = null;
            if (a == a1) {
                a0 = com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND;
            }
        } else {
            a0 = com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT;
        }
        android.animation.Animator a2 = null;
        if (a0 != null) {
            a2 = this.containerView.getCustomContainerAnimator(a0);
        }
        android.animation.AnimatorSet a3 = new android.animation.AnimatorSet();
        switch(com.navdy.hud.app.view.MainView$8.$SwitchMap$com$navdy$hud$app$view$MainView$AnimationMode[a.ordinal()]) {
            case 2: {
                android.animation.ObjectAnimator a4 = this.getXPositionAnimator((android.view.ViewGroup)this.containerView, 0);
                if (a2 == null) {
                    a3.play((android.animation.Animator)a4);
                    break;
                } else {
                    android.animation.Animator[] a5 = new android.animation.Animator[2];
                    a5[0] = a4;
                    a5[1] = a2;
                    a3.playTogether(a5);
                    break;
                }
            }
            case 1: {
                android.animation.ObjectAnimator a6 = this.getXPositionAnimator((android.view.ViewGroup)this.containerView, -this.sidePanelWidth);
                if (a2 == null) {
                    android.animation.Animator[] a7 = new android.animation.Animator[1];
                    a7[0] = a6;
                    a3.playTogether(a7);
                    break;
                } else {
                    android.animation.Animator[] a8 = new android.animation.Animator[2];
                    a8[0] = a6;
                    a8[1] = a2;
                    a3.playTogether(a8);
                    break;
                }
            }
            default: {
                a3 = null;
            }
        }
        return a3;
    }
    
    private void postNotificationCollapseEvent(boolean b) {
        com.navdy.hud.app.framework.notifications.INotification a = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getCurrentNotification();
        String s = null;
        com.navdy.hud.app.framework.notifications.NotificationType a0 = null;
        if (a != null) {
            s = a.getId();
            a0 = a.getType();
        }
        this.uiStateManager.postNotificationAnimationEvent(b, s, a0, com.navdy.hud.app.ui.framework.UIStateManager$Mode.COLLAPSE);
    }
    
    private android.animation.Animator rightCollapse() {
        return this.getXPositionAnimator((android.view.ViewGroup)this.notificationView, this.mainPanelWidth);
    }
    
    private android.animation.ObjectAnimator rightExpand() {
        return this.getXPositionAnimator((android.view.ViewGroup)this.notificationView, this.rightPanelStart);
    }
    
    public void addNotificationExtensionView(android.view.View a) {
        sLogger.d("addNotificationExtensionView");
        android.widget.FrameLayout a0 = this.getNotificationExtensionView();
        ((android.view.ViewGroup)a0).addView(a);
        android.animation.Animator a1 = android.animation.AnimatorInflater.loadAnimator(((android.view.ViewGroup)a0).getContext(), 17498112);
        a1.setTarget(a0);
        a1.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.view.MainView$6(this, (android.view.ViewGroup)a0, a));
        a1.start();
    }
    
    public void dismissNotification() {
        android.animation.AnimatorSet a = new android.animation.AnimatorSet();
        sLogger.d("dismissNotification");
        if (this.notificationView.getX() < (float)this.mainPanelWidth) {
            android.animation.AnimatorSet$Builder a0 = a.play(this.rightCollapse()).with(this.middleAnimate(com.navdy.hud.app.view.MainView$AnimationMode.RIGHT_EXPAND));
            if (this.showingNotificationExtension) {
                sLogger.d("hiding notification extension along with notification");
                a0.with(this.getRemoveNotificationExtensionAnimator());
            }
        }
        a.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.view.MainView$5(this));
        this.animationQueue.startAnimation(a);
    }
    
    public void ejectMainLowerView() {
        this.mainLowerView.ejectView();
    }
    
    public int getBasePaddingTop() {
        return this.basePaddingTop;
    }
    
    public android.widget.FrameLayout getBoxView() {
        return this.splitterView;
    }
    
    public com.navdy.hud.app.view.ContainerView getContainerView() {
        return this.containerView;
    }
    
    public android.view.View getExpandedNotificationCoverView() {
        return this.expandedNotificationCoverView;
    }
    
    public android.widget.FrameLayout getExpandedNotificationView() {
        return this.expandedNotificationView;
    }
    
    public int getNotificationColorVisibility() {
        return this.notificationColorView.getVisibility();
    }
    
    public android.widget.FrameLayout getNotificationExtensionView() {
        return this.notificationExtensionView;
    }
    
    public com.navdy.hud.app.ui.component.carousel.CarouselIndicator getNotificationIndicator() {
        return this.notifIndicator;
    }
    
    public com.navdy.hud.app.ui.component.carousel.ProgressIndicator getNotificationScrollIndicator() {
        return this.notifScrollIndicator;
    }
    
    public com.navdy.hud.app.view.NotificationView getNotificationView() {
        return this.notificationView;
    }
    
    public com.navdy.hud.app.ui.component.SystemTrayView getSystemTray() {
        return this.systemTray;
    }
    
    public com.navdy.hud.app.view.ToastView getToastView() {
        return this.toastView;
    }
    
    public int getVerticalOffset() {
        return this.verticalOffset;
    }
    
    public void injectMainLowerView(android.view.View a) {
        this.mainLowerView.injectView(a);
    }
    
    public boolean isExpandedNotificationViewShowing() {
        return this.expandedNotificationView.getVisibility() == 0;
    }
    
    public boolean isMainLowerViewVisible() {
        return this.mainLowerView.getVisibility() == 0;
    }
    
    public boolean isMainUIShrunk() {
        boolean b = false;
        boolean b0 = this.isNotificationExpanding;
        label3: {
            label0: {
                label1: {
                    label2: {
                        if (b0) {
                            break label2;
                        }
                        if (!this.isNotificationViewShowing()) {
                            break label1;
                        }
                    }
                    if (!this.isNotificationCollapsing) {
                        break label0;
                    }
                }
                b = false;
                break label3;
            }
            b = true;
        }
        return b;
    }
    
    public boolean isNotificationCollapsing() {
        return this.isNotificationCollapsing;
    }
    
    public boolean isNotificationExpanding() {
        return this.isNotificationExpanding;
    }
    
    public boolean isNotificationViewShowing() {
        return this.notificationView.getX() < (float)this.mainPanelWidth;
    }
    
    public boolean isScreenAnimating() {
        return this.isScreenAnimating;
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return (com.navdy.hud.app.manager.InputManager$IInputHandler)this.presenter;
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
        if (this.preferences != null) {
            if (com.navdy.hud.app.settings.HUDSettings.USE_ADAPTIVE_AUTOBRIGHTNESS) {
                this.brightnessControl = (android.content.SharedPreferences$OnSharedPreferenceChangeListener)new com.navdy.hud.app.settings.AdaptiveBrightnessControl(this.getContext(), com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus(), this.preferences, "screen.brightness", "screen.auto_brightness", "screen.auto_brightness_adj", "screen.led_brightness");
            } else {
                this.brightnessControl = (android.content.SharedPreferences$OnSharedPreferenceChangeListener)new com.navdy.hud.app.settings.BrightnessControl(this.getContext(), com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus(), this.preferences, "screen.brightness", "screen.auto_brightness", "screen.auto_brightness_adj", "screen.led_brightness");
            }
        }
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView((android.view.View)this);
        }
        if (this.preferences != null) {
            this.preferences.unregisterOnSharedPreferenceChangeListener(this.brightnessControl);
            this.brightnessControl = null;
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        if (!this.isInEditMode()) {
            if (!com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
                this.addDebugMarkers(this.getContext());
            }
            this.uiStateManager.setToastView(this.toastView);
            this.uiStateManager.addScreenAnimationListener(this.screenAnimationListener);
            this.uiStateManager.addNotificationAnimationListener(this.notificationAnimationListener);
        }
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return false;
    }
    
    public void removeNotificationExtensionView() {
        sLogger.d(new StringBuilder().append("addNotificationExtensionView, showing extension ? : ").append(this.showingNotificationExtension).toString());
        android.widget.FrameLayout a = this.getNotificationExtensionView();
        if (a != null) {
            if (this.showingNotificationExtension) {
                this.getRemoveNotificationExtensionAnimator().start();
            } else {
                this.notificationExtensionView.setVisibility(8);
                ((android.view.ViewGroup)a).removeAllViews();
            }
        }
    }
    
    public void setDisplayFormat(com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat a) {
        if (a != this.format) {
            this.format = a;
            float f = (a != com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat.DISPLAY_FORMAT_COMPACT) ? 1f : this.compactScale();
            this.setScaleX(f);
            this.setScaleY(f);
        }
    }
    
    public void setNotificationColor(int i) {
        this.notificationColorView.setColor(i);
    }
    
    public void setNotificationColorVisibility(int i) {
        this.notificationColorView.setVisibility(i);
    }
    
    public void setVerticalOffset(int i) {
        if (i != this.verticalOffset) {
            this.verticalOffset = i;
            if (this.basePaddingTop != -1) {
                int i0 = this.basePaddingTop + this.verticalOffset;
                if (i0 >= 0) {
                    android.widget.FrameLayout a = this.getBoxView();
                    android.widget.FrameLayout$LayoutParams a0 = (android.widget.FrameLayout$LayoutParams)a.getLayoutParams();
                    a0.topMargin = i0;
                    a.setLayoutParams((android.view.ViewGroup$LayoutParams)a0);
                }
            }
        }
    }
    
    public void showNotification(String s, com.navdy.hud.app.framework.notifications.NotificationType a) {
        android.animation.AnimatorSet a0 = new android.animation.AnimatorSet();
        android.animation.AnimatorSet a1 = new android.animation.AnimatorSet();
        android.animation.Animator[] a2 = new android.animation.Animator[2];
        a2[0] = this.rightExpand();
        a2[1] = this.middleAnimate(com.navdy.hud.app.view.MainView$AnimationMode.MOVE_LEFT_SHRINK);
        a1.playTogether(a2);
        if (this.notificationView.getX() < (float)this.mainPanelWidth) {
            this.uiStateManager.postNotificationAnimationEvent(true, s, a, com.navdy.hud.app.ui.framework.UIStateManager$Mode.EXPAND);
            this.uiStateManager.postNotificationAnimationEvent(false, s, a, com.navdy.hud.app.ui.framework.UIStateManager$Mode.EXPAND);
        } else {
            a0.play((android.animation.Animator)a1);
            this.uiStateManager.getCurrentScreen();
            a0.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.view.MainView$4(this, s, a));
            this.animationQueue.startAnimation(a0);
        }
    }
}
