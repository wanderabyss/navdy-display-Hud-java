package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class AutoBrightnessView$$ViewInjector {
    public AutoBrightnessView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.AutoBrightnessView a0, Object a1) {
        a0.autoBrightnessTitle = (android.widget.TextView)a.findRequiredView(a1, R.id.mainTitle, "field 'autoBrightnessTitle'");
        a0.autoBrightnessIcon = (android.widget.ImageView)a.findRequiredView(a1, R.id.image, "field 'autoBrightnessIcon'");
        a0.title1 = (android.widget.TextView)a.findRequiredView(a1, R.id.title1, "field 'title1'");
        a0.brightnessTitle = (android.widget.TextView)a.findRequiredView(a1, R.id.title2, "field 'brightnessTitle'");
        a0.brightnessTitleDesc = (android.widget.TextView)a.findRequiredView(a1, R.id.title3, "field 'brightnessTitleDesc'");
        a0.infoContainer = (android.widget.LinearLayout)a.findRequiredView(a1, R.id.infoContainer, "field 'infoContainer'");
        a0.autoBrightnessChoices = (com.navdy.hud.app.ui.component.ChoiceLayout)a.findRequiredView(a1, R.id.choiceLayout, "field 'autoBrightnessChoices'");
    }
    
    public static void reset(com.navdy.hud.app.view.AutoBrightnessView a) {
        a.autoBrightnessTitle = null;
        a.autoBrightnessIcon = null;
        a.title1 = null;
        a.brightnessTitle = null;
        a.brightnessTitleDesc = null;
        a.infoContainer = null;
        a.autoBrightnessChoices = null;
    }
}
