package com.navdy.hud.app.screen;
import javax.inject.Inject;

public class GestureLearningScreen$Presenter extends com.navdy.hud.app.ui.framework.BasePresenter {
    @Inject
    com.navdy.hud.app.gesture.GestureServiceConnector gestureServiceConnector;
    @Inject
    com.squareup.otto.Bus mBus;
    @Inject
    android.content.SharedPreferences mPreferences;
    android.os.Bundle tipsBundle;
    @Inject
    com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    
    public GestureLearningScreen$Presenter() {
    }
    
    public void finish() {
        this.mBus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
    }
    
    public void hideCameraSensorBlocked() {
        com.navdy.hud.app.view.LearnGestureScreenLayout a = (com.navdy.hud.app.view.LearnGestureScreenLayout)this.getView();
        if (a != null) {
            a.hideSensorBlocked();
        }
    }
    
    public void hideCaptureView() {
        com.navdy.hud.app.view.LearnGestureScreenLayout a = (com.navdy.hud.app.view.LearnGestureScreenLayout)this.getView();
        if (a != null) {
            a.hideCaptureGestureVideosView();
        }
    }
    
    public void hideTips() {
        com.navdy.hud.app.view.LearnGestureScreenLayout a = (com.navdy.hud.app.view.LearnGestureScreenLayout)this.getView();
        if (a != null) {
            a.hideTips();
        }
    }
    
    public void onLoad(android.os.Bundle a) {
        com.navdy.hud.app.screen.GestureLearningScreen.access$000().v("onLoad");
        super.onLoad(a);
        this.uiStateManager.enableSystemTray(false);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(true);
    }
    
    protected void onUnload() {
        com.navdy.hud.app.screen.GestureLearningScreen.access$000().v("onUnload");
        this.uiStateManager.enableSystemTray(true);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
        super.onUnload();
    }
    
    public void showCameraSensorBlocked() {
        com.navdy.hud.app.view.LearnGestureScreenLayout a = (com.navdy.hud.app.view.LearnGestureScreenLayout)this.getView();
        if (a != null) {
            a.showSensorBlocked();
        }
    }
    
    public void showCaptureView() {
        com.navdy.hud.app.view.LearnGestureScreenLayout a = (com.navdy.hud.app.view.LearnGestureScreenLayout)this.getView();
        if (a != null) {
            a.showCaptureGestureVideosView();
        }
    }
    
    public void showTips() {
        com.navdy.hud.app.view.LearnGestureScreenLayout a = (com.navdy.hud.app.view.LearnGestureScreenLayout)this.getView();
        if (a != null) {
            a.showTips();
        }
    }
}
