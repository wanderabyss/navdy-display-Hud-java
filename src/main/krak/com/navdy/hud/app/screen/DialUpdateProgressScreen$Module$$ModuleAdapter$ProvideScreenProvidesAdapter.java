package com.navdy.hud.app.screen;

final public class DialUpdateProgressScreen$Module$$ModuleAdapter$ProvideScreenProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    final private com.navdy.hud.app.screen.DialUpdateProgressScreen$Module module;
    
    public DialUpdateProgressScreen$Module$$ModuleAdapter$ProvideScreenProvidesAdapter(com.navdy.hud.app.screen.DialUpdateProgressScreen$Module a) {
        super("com.navdy.hud.app.screen.DialUpdateProgressScreen", false, "com.navdy.hud.app.screen.DialUpdateProgressScreen.Module", "provideScreen");
        this.module = a;
        this.setLibrary(false);
    }
    
    public com.navdy.hud.app.screen.DialUpdateProgressScreen get() {
        return this.module.provideScreen();
    }
    
    public Object get() {
        return this.get();
    }
}
