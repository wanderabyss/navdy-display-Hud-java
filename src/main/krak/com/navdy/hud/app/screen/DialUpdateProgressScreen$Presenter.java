package com.navdy.hud.app.screen;
import com.navdy.hud.app.R;

public class DialUpdateProgressScreen$Presenter extends com.navdy.hud.app.ui.framework.BasePresenter {
    private int cause;
    @Inject
    com.squareup.otto.Bus mBus;
    @Inject
    android.content.SharedPreferences mPreferences;
    @Inject
    com.navdy.hud.app.screen.DialUpdateProgressScreen mScreen;
    
    public DialUpdateProgressScreen$Presenter() {
    }
    
    static Object access$100(com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter a) {
        return a.getView();
    }
    
    private void showDialUpdateErrorToast(com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error a) {
        android.content.res.Resources a0 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        android.os.Bundle a1 = new android.os.Bundle();
        a1.putInt("13", 10000);
        a1.putInt("8", R.drawable.icon_dial_update);
        a1.putString("1", a0.getString(R.string.dial_update_err));
        a1.putString("4", a.name());
        a1.putInt("5", R.style.Glances_1);
        a1.putBoolean("12", true);
        com.navdy.hud.app.framework.toast.ToastManager a2 = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        a2.dismissCurrentToast("dial-fw-update-err");
        a2.addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("dial-fw-update-err", a1, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, false));
    }
    
    public void cancelUpdate() {
        com.navdy.hud.app.device.dial.DialManager.getInstance().getDialFirmwareUpdater().cancelUpdate();
    }
    
    public void finish(com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error a) {
        com.navdy.hud.app.screen.DialUpdateProgressScreen.updateStarted = false;
        if (this.cause == 2) {
            com.navdy.hud.app.screen.DialUpdateProgressScreen.access$000().postDelayed((Runnable)new com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter$1(this), 30000L);
        }
        com.navdy.hud.app.service.ShutdownMonitor.getInstance().disableScreenDim(false);
        if (a == com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error.NONE) {
            com.navdy.hud.app.screen.DialUpdateProgressScreen.access$000().postDelayed((Runnable)new com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter$2(this), 6000L);
        } else {
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
            this.mBus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
            this.cancelUpdate();
            this.showDialUpdateErrorToast(a);
        }
    }
    
    public com.navdy.hud.app.device.dial.DialFirmwareUpdater$Versions getVersions() {
        return com.navdy.hud.app.device.dial.DialManager.getInstance().getDialFirmwareUpdater().getVersions();
    }
    
    public void onLoad(android.os.Bundle a) {
        super.onLoad(a);
        if (this.mScreen != null) {
            android.os.Bundle a0 = this.mScreen.arguments;
            if (a0 != null) {
                this.cause = a0.getInt("PROGRESS_CAUSE", 2);
            }
        }
    }
    
    public void startUpdate() {
        if (!com.navdy.hud.app.screen.DialUpdateProgressScreen.updateStarted) {
            com.navdy.hud.app.screen.DialUpdateProgressScreen.updateStarted = true;
            com.navdy.hud.app.service.ShutdownMonitor.getInstance().disableScreenDim(true);
            com.navdy.hud.app.device.dial.DialManager.getInstance().getDialFirmwareUpdater().startUpdate((com.navdy.hud.app.device.dial.DialFirmwareUpdater$UpdateProgressListener)new com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter$3(this));
        }
    }
}
