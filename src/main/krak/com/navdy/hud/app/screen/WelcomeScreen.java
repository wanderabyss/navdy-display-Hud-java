package com.navdy.hud.app.screen;

@Layout(R.layout.screen_welcome)
public class WelcomeScreen extends com.navdy.hud.app.screen.BaseScreen {
    public static String ACTION_RECONNECT;
    public static String ACTION_SWITCH_PHONE;
    final private static int APP_LAUNCH_TIMEOUT = 12000;
    public static String ARG_ACTION;
    final private static int CONNECTION_TIMEOUT = 5000;
    final private static com.navdy.service.library.log.Logger logger;
    
    static {
        logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.WelcomeScreen.class);
        ARG_ACTION = "action";
        ACTION_SWITCH_PHONE = "switch";
        ACTION_RECONNECT = "reconnect";
    }
    
    public WelcomeScreen() {
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return logger;
    }
    
    public int getAnimationIn(flow.Flow$Direction a) {
        return 17432576;
    }
    
    public int getAnimationOut(flow.Flow$Direction a) {
        return 17432577;
    }
    
    public Object getDaggerModule() {
        return new com.navdy.hud.app.screen.WelcomeScreen$Module(this);
    }
    
    public String getMortarScopeName() {
        return (this).getClass().getName();
    }
    
    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME;
    }
}
