package com.navdy.hud.app.screen;

abstract public interface IDashboardGaugesManager {
    abstract public int getOptionIconResourceForGauge(int arg);
    
    
    abstract public String getOptionLabelForGauge(int arg);
    
    
    abstract public java.util.List getSmartDashGauges();
    
    
    abstract public com.navdy.hud.app.view.DashboardWidgetPresenter getWidgetPresenterForGauge(int arg, boolean arg0);
    
    
    abstract public boolean isGaugeWorking(int arg);
    
    
    abstract public void showGauge(int arg, com.navdy.hud.app.view.DashboardWidgetView arg0);
    
    
    abstract public void updateGauge(int arg, Object arg0);
    
    
    abstract public void updateUserPreference(com.navdy.hud.app.screen.IDashboardGaugesManager$GaugePositioning arg, int arg0);
}
