package com.navdy.hud.app.event;


public enum InitEvents$Phase {
    EARLY(0),
    PRE_USER_INTERACTION(1),
    CONNECTION_SERVICE_STARTED(2),
    POST_START(3),
    LATE(4),
    SWITCHING_LOCALE(5),
    LOCALE_UP_TO_DATE(6);

    private int value;
    InitEvents$Phase(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class InitEvents$Phase extends Enum {
//    final private static com.navdy.hud.app.event.InitEvents$Phase[] $VALUES;
//    final public static com.navdy.hud.app.event.InitEvents$Phase CONNECTION_SERVICE_STARTED;
//    final public static com.navdy.hud.app.event.InitEvents$Phase EARLY;
//    final public static com.navdy.hud.app.event.InitEvents$Phase LATE;
//    final public static com.navdy.hud.app.event.InitEvents$Phase LOCALE_UP_TO_DATE;
//    final public static com.navdy.hud.app.event.InitEvents$Phase POST_START;
//    final public static com.navdy.hud.app.event.InitEvents$Phase PRE_USER_INTERACTION;
//    final public static com.navdy.hud.app.event.InitEvents$Phase SWITCHING_LOCALE;
//    
//    static {
//        EARLY = new com.navdy.hud.app.event.InitEvents$Phase("EARLY", 0);
//        PRE_USER_INTERACTION = new com.navdy.hud.app.event.InitEvents$Phase("PRE_USER_INTERACTION", 1);
//        CONNECTION_SERVICE_STARTED = new com.navdy.hud.app.event.InitEvents$Phase("CONNECTION_SERVICE_STARTED", 2);
//        POST_START = new com.navdy.hud.app.event.InitEvents$Phase("POST_START", 3);
//        LATE = new com.navdy.hud.app.event.InitEvents$Phase("LATE", 4);
//        SWITCHING_LOCALE = new com.navdy.hud.app.event.InitEvents$Phase("SWITCHING_LOCALE", 5);
//        LOCALE_UP_TO_DATE = new com.navdy.hud.app.event.InitEvents$Phase("LOCALE_UP_TO_DATE", 6);
//        com.navdy.hud.app.event.InitEvents$Phase[] a = new com.navdy.hud.app.event.InitEvents$Phase[7];
//        a[0] = EARLY;
//        a[1] = PRE_USER_INTERACTION;
//        a[2] = CONNECTION_SERVICE_STARTED;
//        a[3] = POST_START;
//        a[4] = LATE;
//        a[5] = SWITCHING_LOCALE;
//        a[6] = LOCALE_UP_TO_DATE;
//        $VALUES = a;
//    }
//    
//    private InitEvents$Phase(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.event.InitEvents$Phase valueOf(String s) {
//        return (com.navdy.hud.app.event.InitEvents$Phase)Enum.valueOf(com.navdy.hud.app.event.InitEvents$Phase.class, s);
//    }
//    
//    public static com.navdy.hud.app.event.InitEvents$Phase[] values() {
//        return $VALUES.clone();
//    }
//}
//