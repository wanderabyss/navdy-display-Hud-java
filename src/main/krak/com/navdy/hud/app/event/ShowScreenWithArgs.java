package com.navdy.hud.app.event;

public class ShowScreenWithArgs {
    public android.os.Bundle args;
    public Object args2;
    public boolean ignoreAnimation;
    public com.navdy.service.library.events.ui.Screen screen;
    
    public ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen a, android.os.Bundle a0, Object a1, boolean b) {
        this.screen = a;
        this.args = a0;
        this.args2 = a1;
        this.ignoreAnimation = b;
    }
    
    public ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen a, android.os.Bundle a0, boolean b) {
        this(a, a0, null, b);
    }
}
