package com.navdy.hud.app.receiver;

public class BootReceiver extends android.content.BroadcastReceiver {
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.receiver.BootReceiver.class);
    }
    
    public BootReceiver() {
    }
    
    public void onReceive(android.content.Context a, android.content.Intent a0) {
        boolean b = a.getSharedPreferences("App", 0).getBoolean("start_on_boot_preference", true);
        sLogger.d(new StringBuilder().append("Start on boot:").append(b).toString());
        if (b) {
            android.content.Intent a1 = new android.content.Intent(a, com.navdy.hud.app.ui.activity.MainActivity.class);
            a1.addFlags(268435456);
            a.startActivity(a1);
        }
    }
}
