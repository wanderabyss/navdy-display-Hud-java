package com.navdy.hud.mfi;


public enum CallStateUpdate$Status {
    Disconnected(0),
    Sending(1),
    Ringing(2),
    Connecting(3),
    Active(4),
    Held(5),
    Disconnecting(6);

    private int value;
    CallStateUpdate$Status(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class CallStateUpdate$Status extends Enum {
//    final private static com.navdy.hud.mfi.CallStateUpdate$Status[] $VALUES;
//    final public static com.navdy.hud.mfi.CallStateUpdate$Status Active;
//    final public static com.navdy.hud.mfi.CallStateUpdate$Status Connecting;
//    final public static com.navdy.hud.mfi.CallStateUpdate$Status Disconnected;
//    final public static com.navdy.hud.mfi.CallStateUpdate$Status Disconnecting;
//    final public static com.navdy.hud.mfi.CallStateUpdate$Status Held;
//    final public static com.navdy.hud.mfi.CallStateUpdate$Status Ringing;
//    final public static com.navdy.hud.mfi.CallStateUpdate$Status Sending;
//    
//    static {
//        Disconnected = new com.navdy.hud.mfi.CallStateUpdate$Status("Disconnected", 0);
//        Sending = new com.navdy.hud.mfi.CallStateUpdate$Status("Sending", 1);
//        Ringing = new com.navdy.hud.mfi.CallStateUpdate$Status("Ringing", 2);
//        Connecting = new com.navdy.hud.mfi.CallStateUpdate$Status("Connecting", 3);
//        Active = new com.navdy.hud.mfi.CallStateUpdate$Status("Active", 4);
//        Held = new com.navdy.hud.mfi.CallStateUpdate$Status("Held", 5);
//        Disconnecting = new com.navdy.hud.mfi.CallStateUpdate$Status("Disconnecting", 6);
//        com.navdy.hud.mfi.CallStateUpdate$Status[] a = new com.navdy.hud.mfi.CallStateUpdate$Status[7];
//        a[0] = Disconnected;
//        a[1] = Sending;
//        a[2] = Ringing;
//        a[3] = Connecting;
//        a[4] = Active;
//        a[5] = Held;
//        a[6] = Disconnecting;
//        $VALUES = a;
//    }
//    
//    private CallStateUpdate$Status(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.CallStateUpdate$Status valueOf(String s) {
//        return (com.navdy.hud.mfi.CallStateUpdate$Status)Enum.valueOf(com.navdy.hud.mfi.CallStateUpdate$Status.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.CallStateUpdate$Status[] values() {
//        return $VALUES.clone();
//    }
//}
//