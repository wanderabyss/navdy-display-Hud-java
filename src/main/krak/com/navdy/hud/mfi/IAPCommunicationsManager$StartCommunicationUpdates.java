package com.navdy.hud.mfi;


    public enum IAPCommunicationsManager$StartCommunicationUpdates {
        MuteStatus(0);

        private int value;
        IAPCommunicationsManager$StartCommunicationUpdates(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class IAPCommunicationsManager$StartCommunicationUpdates extends Enum {
//    final private static com.navdy.hud.mfi.IAPCommunicationsManager$StartCommunicationUpdates[] $VALUES;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$StartCommunicationUpdates MuteStatus;
//    int id;
//    
//    static {
//        MuteStatus = new com.navdy.hud.mfi.IAPCommunicationsManager$StartCommunicationUpdates("MuteStatus", 0, 9);
//        com.navdy.hud.mfi.IAPCommunicationsManager$StartCommunicationUpdates[] a = new com.navdy.hud.mfi.IAPCommunicationsManager$StartCommunicationUpdates[1];
//        a[0] = MuteStatus;
//        $VALUES = a;
//    }
//    
//    private IAPCommunicationsManager$StartCommunicationUpdates(String s, int i, int i0) {
//        super(s, i);
//        this.id = i0;
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$StartCommunicationUpdates valueOf(String s) {
//        return (com.navdy.hud.mfi.IAPCommunicationsManager$StartCommunicationUpdates)Enum.valueOf(com.navdy.hud.mfi.IAPCommunicationsManager$StartCommunicationUpdates.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$StartCommunicationUpdates[] values() {
//        return $VALUES.clone();
//    }
//}
//