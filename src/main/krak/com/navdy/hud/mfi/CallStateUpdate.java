package com.navdy.hud.mfi;

public class CallStateUpdate {
    public String addressBookID;
    public String callUUID;
    public int conferenceGroup;
    public com.navdy.hud.mfi.CallStateUpdate$Direction direction;
    public com.navdy.hud.mfi.CallStateUpdate$DisconnectReason disconnectReason;
    public String displayName;
    public boolean isConferenced;
    public String label;
    public String remoteID;
    public com.navdy.hud.mfi.IAPCommunicationsManager$Service service;
    public com.navdy.hud.mfi.CallStateUpdate$Status status;
    
    public CallStateUpdate() {
        this.status = com.navdy.hud.mfi.CallStateUpdate$Status.Disconnected;
        this.direction = com.navdy.hud.mfi.CallStateUpdate$Direction.Unknown;
        this.service = com.navdy.hud.mfi.IAPCommunicationsManager$Service.Unknown;
        this.disconnectReason = com.navdy.hud.mfi.CallStateUpdate$DisconnectReason.Ended;
    }
}
