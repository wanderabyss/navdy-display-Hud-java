package com.navdy.hud.mfi;


public enum iAPProcessor$MediaRemoteComponent {
    HIDKeyboard(0),
    HIDMediaRemote(1);

    private int value;
    iAPProcessor$MediaRemoteComponent(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class iAPProcessor$MediaRemoteComponent extends Enum {
//    final private static com.navdy.hud.mfi.iAPProcessor$MediaRemoteComponent[] $VALUES;
//    final public static com.navdy.hud.mfi.iAPProcessor$MediaRemoteComponent HIDKeyboard;
//    final public static com.navdy.hud.mfi.iAPProcessor$MediaRemoteComponent HIDMediaRemote;
//    
//    static {
//        HIDKeyboard = new com.navdy.hud.mfi.iAPProcessor$MediaRemoteComponent("HIDKeyboard", 0);
//        HIDMediaRemote = new com.navdy.hud.mfi.iAPProcessor$MediaRemoteComponent("HIDMediaRemote", 1);
//        com.navdy.hud.mfi.iAPProcessor$MediaRemoteComponent[] a = new com.navdy.hud.mfi.iAPProcessor$MediaRemoteComponent[2];
//        a[0] = HIDKeyboard;
//        a[1] = HIDMediaRemote;
//        $VALUES = a;
//    }
//    
//    private iAPProcessor$MediaRemoteComponent(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$MediaRemoteComponent valueOf(String s) {
//        return (com.navdy.hud.mfi.iAPProcessor$MediaRemoteComponent)Enum.valueOf(com.navdy.hud.mfi.iAPProcessor$MediaRemoteComponent.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$MediaRemoteComponent[] values() {
//        return $VALUES.clone();
//    }
//}
//