package com.navdy.hud.mfi;

public class AuthCoprocessor {
    final private static int ACCESSORY_CERTIFICATE_DATA_REGISTER = 49;
    final private static int ACCESSORY_CERTIFICATE_LENGTH_REGISTER = 48;
    final private static int ACCESSORY_SIGNATURE_GENERATION = 1;
    final private static int AUTH_ERROR_FLAG = 128;
    final private static int BUS = 0;
    final private static int CHALLENGE_DATA_REGISTER = 33;
    final private static int CHALLENGE_LENGTH_REGISTER = 32;
    final private static int DEVICE_ADDRESS = 17;
    final private static int DEVICE_CERTIFICATE_DATA_REGISTER = 81;
    final private static int DEVICE_CERTIFICATE_LENGTH_REGISTER = 80;
    final private static int DEVICE_CERTIFICATE_VALIDATION = 4;
    final private static int DEVICE_CHALLENGE_GENERATION = 2;
    final private static int DEVICE_SIGNATURE_VALIDATION = 3;
    final private static int ERROR_CODE_REGISTER = 5;
    final private static int PAGE_SIZE = 128;
    final private static int SIGNATURE_DATA_REGISTER = 18;
    final private static int SIGNATURE_LENGTH_REGISTER = 17;
    final private static int STATUS_CONTROL_REGISTER = 16;
    final private static String TAG = "AuthCoprocessor";
    private android.content.Context context;
    private boolean crashReportSent;
    private com.navdy.hud.mfi.I2C i2c;
    private com.navdy.hud.mfi.IAPListener iapListener;
    
    public AuthCoprocessor(com.navdy.hud.mfi.IAPListener a, android.content.Context a0) {
        this.crashReportSent = false;
        this.i2c = new com.navdy.hud.mfi.I2C(0);
        if (a != null) {
            this.iapListener = a;
        } else {
            this.iapListener = (com.navdy.hud.mfi.IAPListener)new com.navdy.hud.mfi.AuthCoprocessor$1(this);
        }
        this.context = a0;
    }
    
    private void checkStatus(String s) {
        int i = this.i2c.readByte(17, 16);
        int i0 = this.i2c.readByte(17, 5);
        if ((i & 128) == 0) {
            return;
        }
        byte[] a = new byte[1];
        int i1 = (byte)i;
        a[0] = (byte)i1;
        String s0 = com.navdy.hud.mfi.Utils.bytesToHex(a);
        byte[] a0 = new byte[1];
        int i2 = (byte)i0;
        a0[0] = (byte)i2;
        String s1 = com.navdy.hud.mfi.Utils.bytesToHex(a0);
        android.util.Log.e("AuthCoprocessor", new StringBuilder().append("auth CP status: ").append(s0).toString());
        android.util.Log.e("AuthCoprocessor", new StringBuilder().append("auth CP error code: ").append(s1).toString());
        this.sendCrashReport();
        try {
            this.iapListener.onCoprocessorStatusCheckFailed(s, s0, s1);
        } catch(Throwable a1) {
            android.util.Log.e("AuthCoprocessor", "logging to listener failed", a1);
        }
        throw new RuntimeException(new StringBuilder().append("failed to ").append(s).toString());
    }
    
    private void sendCrashReport() {
        boolean b = this.crashReportSent;
        label0: {
            Exception a = null;
            label1: if (b) {
                android.util.Log.e("AuthCoprocessor", "skipping duplicate crash report");
                break label0;
            } else if (this.context != null) {
                try {
                    Class a0 = Class.forName("com.navdy.hud.app.util.CrashReportService");
                    android.content.Intent a1 = new android.content.Intent(this.context, a0);
                    a1.setAction("DumpCrashReport");
                    a1.putExtra("EXTRA_CRASH_TYPE", "MFI_ERROR");
                    this.context.startService(a1);
                    this.crashReportSent = true;
                } catch(Exception a2) {
                    a = a2;
                    break label1;
                }
                break label0;
            } else {
                android.util.Log.e("AuthCoprocessor", "cannot sent crash report, null context");
                break label0;
            }
            android.util.Log.e("AuthCoprocessor", "error in lookup of CrashReportService class", (Throwable)a);
        }
    }
    
    private void writePages(int i, int i0, byte[] a, String s) {
        this.i2c.writeShort(17, i, a.length);
        this.checkStatus(new StringBuilder().append("write ").append(s).append(" length").toString());
        int i1 = a.length;
        byte[] a0 = new byte[128];
        int i2 = 0;
        while(i1 > 0) {
            int i3 = Math.min(i1, 128);
            System.arraycopy(a, i2, a0, 0, i3);
            this.i2c.write(17, i0, a0, 0, a0.length);
            this.checkStatus(new StringBuilder().append("write ").append(s).toString());
            int i4 = i0 + 1;
            i2 = i2 + i3;
            i1 = i1 - i3;
            i0 = i4;
        }
    }
    
    public void close() {
        this.i2c.close();
    }
    
    public byte[] createAccessorySignature(byte[] a) {
        byte[] a0 = null;
        try {
            if (a.length != 20) {
                throw new RuntimeException("wrong challenge size");
            }
            this.writePages(32, 33, a, "accessory signature");
            this.i2c.writeShort(17, 17, 128);
            this.i2c.writeByte(17, 16, 1);
            this.checkStatus("generate accessory signature");
            a0 = new byte[this.i2c.readShort(17, 17)];
            this.i2c.read(17, 18, a0);
            this.checkStatus("read accessory signature");
        } catch(Exception a1) {
            android.util.Log.e("AuthCoprocessor", "", (Throwable)a1);
            a0 = null;
        }
        return a0;
    }
    
    public void open() {
        this.i2c.open();
    }
    
    public byte[] readAccessoryCertificate() {
        byte[] a = new byte[this.i2c.readShort(17, 48)];
        this.i2c.read(17, 49, a);
        this.checkStatus("read accessory certificate");
        return a;
    }
    
    public byte[] validateDeviceCertificateAndGenerateDeviceChallenge(byte[] a) {
        byte[] a0 = null;
        try {
            this.writePages(80, 81, a, "device certificate");
            this.i2c.writeByte(17, 16, 4);
            this.checkStatus("validate device certificate");
            this.i2c.writeByte(17, 16, 2);
            this.checkStatus("generate device challenge");
            a0 = new byte[this.i2c.readShort(17, 32)];
            this.i2c.read(17, 33, a0);
            this.checkStatus("read device challenge");
        } catch(Exception a1) {
            android.util.Log.e("AuthCoprocessor", "", (Throwable)a1);
            a0 = null;
        }
        return a0;
    }
    
    public boolean validateDeviceSignature(byte[] a) {
        boolean b = false;
        try {
            this.writePages(17, 18, a, "device signature");
            this.i2c.writeByte(17, 16, 3);
            this.checkStatus("validate device signature");
            b = true;
        } catch(Exception a0) {
            android.util.Log.e("AuthCoprocessor", "", (Throwable)a0);
            b = false;
        }
        return b;
    }
}
