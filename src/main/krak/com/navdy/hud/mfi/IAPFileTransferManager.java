package com.navdy.hud.mfi;

public class IAPFileTransferManager implements com.navdy.hud.mfi.IIAPFileTransferManager {
    final public static int DEFAULT_FILE_TRANSFER_LIMIT = 1048576;
    final private static int FILE_TRANSFER_IDENTIFIER_INDEX = 0;
    final private static int MAX_SESSIONS = 3;
    final private static String TAG;
    private volatile int expectedFileTransferIdentifier;
    private long mFileTransferLimit;
    private com.navdy.hud.mfi.IAPFileTransferListener mFileTransferListener;
    private com.navdy.hud.mfi.iAPProcessor mIapProcessor;
    private java.util.concurrent.ExecutorService mSerialExecutor;
    private java.util.HashMap mSessionCache;
    
    static {
        TAG = com.navdy.hud.mfi.IAPFileTransferManager.class.getSimpleName();
    }
    
    public IAPFileTransferManager(com.navdy.hud.mfi.iAPProcessor a) {
        this.mSerialExecutor = java.util.concurrent.Executors.newSingleThreadExecutor();
        this.expectedFileTransferIdentifier = -1;
        this.mFileTransferLimit = 1048576L;
        this.mIapProcessor = a;
        this.mSessionCache = new java.util.HashMap();
    }
    
    static java.util.HashMap access$000(com.navdy.hud.mfi.IAPFileTransferManager a) {
        return a.mSessionCache;
    }
    
    static String access$100() {
        return TAG;
    }
    
    static com.navdy.hud.mfi.IAPFileTransferListener access$200(com.navdy.hud.mfi.IAPFileTransferManager a) {
        return a.mFileTransferListener;
    }
    
    public void cancel() {
        this.mSerialExecutor.submit((Runnable)new com.navdy.hud.mfi.IAPFileTransferManager$2(this));
    }
    
    public void clear() {
        this.mSessionCache.clear();
    }
    
    public long getFileTransferLimit() {
        return this.mFileTransferLimit;
    }
    
    public void onCanceled(int i) {
        android.util.Log.d(TAG, new StringBuilder().append("File transfer was cancelled ").append(i).toString());
        this.mSessionCache.remove(Integer.valueOf(i));
        if (this.mFileTransferListener != null) {
            this.mFileTransferListener.onFileTransferCancel(i);
        }
    }
    
    public void onError(int i, Throwable a) {
        android.util.Log.d(TAG, new StringBuilder().append("Error in file transfer ").append(a).toString());
        this.mSessionCache.remove(Integer.valueOf(i));
    }
    
    public void onFileTransferSetupRequest(int i, long j) {
        if (this.mFileTransferListener != null) {
            this.mFileTransferListener.onFileTransferSetup(i, j);
        }
    }
    
    public void onFileTransferSetupResponse(int i, boolean b) {
        com.navdy.hud.mfi.FileTransferSession a = (com.navdy.hud.mfi.FileTransferSession)this.mSessionCache.get(Integer.valueOf(i));
        if (a != null) {
            if (b) {
                a.proceed();
            } else {
                a.cancel();
            }
        }
    }
    
    public void onPaused(int i) {
        android.util.Log.d(TAG, "File transfer paused");
    }
    
    public void onSuccess(int i, byte[] a) {
        if (this.mFileTransferListener != null) {
            android.util.Log.d(TAG, new StringBuilder().append("File successfully transferred ").append(i).toString());
            this.mFileTransferListener.onFileReceived(i, a);
        }
        this.mSessionCache.remove(Integer.valueOf(i));
    }
    
    public void queue(byte[] a) {
        this.mSerialExecutor.submit((Runnable)new com.navdy.hud.mfi.IAPFileTransferManager$1(this, a));
    }
    
    public void sendMessage(int i, byte[] a) {
        this.mIapProcessor.sendToDevice(2, a);
    }
    
    public void setFileTransferLimit(int i) {
        this.mFileTransferLimit = (long)i;
    }
    
    public void setFileTransferListener(com.navdy.hud.mfi.IAPFileTransferListener a) {
        this.mFileTransferListener = a;
    }
}
