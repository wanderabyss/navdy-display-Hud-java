package com.navdy.hud.mfi;


    public enum iAPProcessor$iAPMessage {
        RequestAuthenticationRequest(0),
    AuthenticationCertificate(1),
    RequestAuthenticationChallengeResponse(2),
    AuthenticationResponse(3),
    AuthenticationFailed(4),
    AuthenticationSucceeded(5),
    StartIdentification(6),
    IdentificationInformation(7),
    IdentificationAccepted(8),
    IdentificationRejected(9),
    CancelIdentification(10),
    IdentificationInformationUpdate(11),
    StartExternalAccessoryProtocolSession(12),
    StopExternalAccessoryProtocolSession(13),
    StatusExternalAccessoryProtocolSession(14),
    RequestDeviceAuthenticationCertificate(15),
    DeviceAuthenticationCertificate(16),
    RequestDeviceAuthenticationChallengeResponse(17),
    DeviceAuthenticationResponse(18),
    DeviceAuthenticationFailed(19),
    DeviceAuthenticationSucceeded(20),
    StartHID(21),
    DeviceHIDReport(22),
    AccessoryHIDReport(23),
    StopHID(24),
    StartCallStateUpdates(25),
    CallStateUpdate(26),
    StopCallStateUpdates(27),
    InitiateCall(28),
    AcceptCall(29),
    EndCall(30),
    HoldStatusUpdate(31),
    MuteStatusUpdate(32),
    StartCommunicationUpdates(33),
    CommunicationUpdate(34),
    StopCommunicationUpdates(35),
    StartListUpdates(36),
    ListUpdate(37),
    StopListUpdate(38),
    StartNowPlayingUpdates(39),
    NowPlayingUpdate(40),
    StopNowPlayingUpdates(41),
    RequestAppLaunch(42);

        private int value;
        iAPProcessor$iAPMessage(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class iAPProcessor$iAPMessage extends Enum {
//    final private static com.navdy.hud.mfi.iAPProcessor$iAPMessage[] $VALUES;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage AcceptCall;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage AccessoryHIDReport;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage AuthenticationCertificate;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage AuthenticationFailed;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage AuthenticationResponse;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage AuthenticationSucceeded;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage CallStateUpdate;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage CancelIdentification;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage CommunicationUpdate;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage DeviceAuthenticationCertificate;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage DeviceAuthenticationFailed;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage DeviceAuthenticationResponse;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage DeviceAuthenticationSucceeded;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage DeviceHIDReport;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage EndCall;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage HoldStatusUpdate;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage IdentificationAccepted;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage IdentificationInformation;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage IdentificationInformationUpdate;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage IdentificationRejected;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage InitiateCall;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage ListUpdate;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage MuteStatusUpdate;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage NowPlayingUpdate;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage RequestAppLaunch;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage RequestAuthenticationChallengeResponse;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage RequestAuthenticationRequest;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage RequestDeviceAuthenticationCertificate;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage RequestDeviceAuthenticationChallengeResponse;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage StartCallStateUpdates;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage StartCommunicationUpdates;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage StartExternalAccessoryProtocolSession;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage StartHID;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage StartIdentification;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage StartListUpdates;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage StartNowPlayingUpdates;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage StatusExternalAccessoryProtocolSession;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage StopCallStateUpdates;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage StopCommunicationUpdates;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage StopExternalAccessoryProtocolSession;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage StopHID;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage StopListUpdate;
//    final public static com.navdy.hud.mfi.iAPProcessor$iAPMessage StopNowPlayingUpdates;
//    int id;
//    
//    static {
//        RequestAuthenticationRequest = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("RequestAuthenticationRequest", 0, 43520);
//        AuthenticationCertificate = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("AuthenticationCertificate", 1, 43521);
//        RequestAuthenticationChallengeResponse = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("RequestAuthenticationChallengeResponse", 2, 43522);
//        AuthenticationResponse = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("AuthenticationResponse", 3, 43523);
//        AuthenticationFailed = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("AuthenticationFailed", 4, 43524);
//        AuthenticationSucceeded = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("AuthenticationSucceeded", 5, 43525);
//        StartIdentification = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("StartIdentification", 6, 7424);
//        IdentificationInformation = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("IdentificationInformation", 7, 7425);
//        IdentificationAccepted = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("IdentificationAccepted", 8, 7426);
//        IdentificationRejected = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("IdentificationRejected", 9, 7427);
//        CancelIdentification = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("CancelIdentification", 10, 7428);
//        IdentificationInformationUpdate = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("IdentificationInformationUpdate", 11, 7429);
//        StartExternalAccessoryProtocolSession = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("StartExternalAccessoryProtocolSession", 12, 59904);
//        StopExternalAccessoryProtocolSession = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("StopExternalAccessoryProtocolSession", 13, 59905);
//        StatusExternalAccessoryProtocolSession = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("StatusExternalAccessoryProtocolSession", 14, 59906);
//        RequestDeviceAuthenticationCertificate = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("RequestDeviceAuthenticationCertificate", 15, 43536);
//        DeviceAuthenticationCertificate = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("DeviceAuthenticationCertificate", 16, 43537);
//        RequestDeviceAuthenticationChallengeResponse = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("RequestDeviceAuthenticationChallengeResponse", 17, 43538);
//        DeviceAuthenticationResponse = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("DeviceAuthenticationResponse", 18, 43539);
//        DeviceAuthenticationFailed = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("DeviceAuthenticationFailed", 19, 43540);
//        DeviceAuthenticationSucceeded = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("DeviceAuthenticationSucceeded", 20, 43541);
//        StartHID = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("StartHID", 21, 26624);
//        DeviceHIDReport = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("DeviceHIDReport", 22, 26625);
//        AccessoryHIDReport = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("AccessoryHIDReport", 23, 26626);
//        StopHID = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("StopHID", 24, 26627);
//        StartCallStateUpdates = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("StartCallStateUpdates", 25, 16724);
//        CallStateUpdate = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("CallStateUpdate", 26, 16725);
//        StopCallStateUpdates = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("StopCallStateUpdates", 27, 16726);
//        InitiateCall = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("InitiateCall", 28, 16730);
//        AcceptCall = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("AcceptCall", 29, 16731);
//        EndCall = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("EndCall", 30, 16732);
//        HoldStatusUpdate = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("HoldStatusUpdate", 31, 16735);
//        MuteStatusUpdate = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("MuteStatusUpdate", 32, 16736);
//        StartCommunicationUpdates = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("StartCommunicationUpdates", 33, 16727);
//        CommunicationUpdate = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("CommunicationUpdate", 34, 16728);
//        StopCommunicationUpdates = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("StopCommunicationUpdates", 35, 16729);
//        StartListUpdates = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("StartListUpdates", 36, 16752);
//        ListUpdate = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("ListUpdate", 37, 16753);
//        StopListUpdate = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("StopListUpdate", 38, 16754);
//        StartNowPlayingUpdates = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("StartNowPlayingUpdates", 39, 20480);
//        NowPlayingUpdate = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("NowPlayingUpdate", 40, 20481);
//        StopNowPlayingUpdates = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("StopNowPlayingUpdates", 41, 20482);
//        RequestAppLaunch = new com.navdy.hud.mfi.iAPProcessor$iAPMessage("RequestAppLaunch", 42, 59906);
//        com.navdy.hud.mfi.iAPProcessor$iAPMessage[] a = new com.navdy.hud.mfi.iAPProcessor$iAPMessage[43];
//        a[0] = RequestAuthenticationRequest;
//        a[1] = AuthenticationCertificate;
//        a[2] = RequestAuthenticationChallengeResponse;
//        a[3] = AuthenticationResponse;
//        a[4] = AuthenticationFailed;
//        a[5] = AuthenticationSucceeded;
//        a[6] = StartIdentification;
//        a[7] = IdentificationInformation;
//        a[8] = IdentificationAccepted;
//        a[9] = IdentificationRejected;
//        a[10] = CancelIdentification;
//        a[11] = IdentificationInformationUpdate;
//        a[12] = StartExternalAccessoryProtocolSession;
//        a[13] = StopExternalAccessoryProtocolSession;
//        a[14] = StatusExternalAccessoryProtocolSession;
//        a[15] = RequestDeviceAuthenticationCertificate;
//        a[16] = DeviceAuthenticationCertificate;
//        a[17] = RequestDeviceAuthenticationChallengeResponse;
//        a[18] = DeviceAuthenticationResponse;
//        a[19] = DeviceAuthenticationFailed;
//        a[20] = DeviceAuthenticationSucceeded;
//        a[21] = StartHID;
//        a[22] = DeviceHIDReport;
//        a[23] = AccessoryHIDReport;
//        a[24] = StopHID;
//        a[25] = StartCallStateUpdates;
//        a[26] = CallStateUpdate;
//        a[27] = StopCallStateUpdates;
//        a[28] = InitiateCall;
//        a[29] = AcceptCall;
//        a[30] = EndCall;
//        a[31] = HoldStatusUpdate;
//        a[32] = MuteStatusUpdate;
//        a[33] = StartCommunicationUpdates;
//        a[34] = CommunicationUpdate;
//        a[35] = StopCommunicationUpdates;
//        a[36] = StartListUpdates;
//        a[37] = ListUpdate;
//        a[38] = StopListUpdate;
//        a[39] = StartNowPlayingUpdates;
//        a[40] = NowPlayingUpdate;
//        a[41] = StopNowPlayingUpdates;
//        a[42] = RequestAppLaunch;
//        $VALUES = a;
//    }
//    
//    private iAPProcessor$iAPMessage(String s, int i, int i0) {
//        super(s, i);
//        this.id = i0;
//        com.navdy.hud.mfi.iAPProcessor.iAPMsgById.put(i0, this);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$iAPMessage valueOf(String s) {
//        return (com.navdy.hud.mfi.iAPProcessor$iAPMessage)Enum.valueOf(com.navdy.hud.mfi.iAPProcessor$iAPMessage.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$iAPMessage[] values() {
//        return $VALUES.clone();
//    }
//}
//