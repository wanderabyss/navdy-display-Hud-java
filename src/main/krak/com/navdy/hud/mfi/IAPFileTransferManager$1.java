package com.navdy.hud.mfi;

class IAPFileTransferManager$1 implements Runnable {
    final com.navdy.hud.mfi.IAPFileTransferManager this$0;
    final byte[] val$data;
    
    IAPFileTransferManager$1(com.navdy.hud.mfi.IAPFileTransferManager a, byte[] a0) {
        super();
        this.this$0 = a;
        this.val$data = a0;
    }
    
    public void run() {
        if (this.val$data != null) {
            int i = this.val$data[0];
            int i0 = i & 255;
            com.navdy.hud.mfi.FileTransferSession a = (com.navdy.hud.mfi.FileTransferSession)com.navdy.hud.mfi.IAPFileTransferManager.access$000(this.this$0).get(Integer.valueOf(i0));
            if (a == null) {
                android.util.Log.d(com.navdy.hud.mfi.IAPFileTransferManager.access$100(), new StringBuilder().append("Creating a new file transfer session ").append(i0).toString());
                a = new com.navdy.hud.mfi.FileTransferSession(i0, (com.navdy.hud.mfi.IIAPFileTransferManager)this.this$0);
                com.navdy.hud.mfi.IAPFileTransferManager.access$000(this.this$0).put(Integer.valueOf(i0), a);
            }
            a.bProcessFileTransferMessage(this.val$data);
        }
    }
}
