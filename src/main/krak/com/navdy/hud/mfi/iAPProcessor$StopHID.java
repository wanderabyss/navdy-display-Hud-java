package com.navdy.hud.mfi;


    public enum iAPProcessor$StopHID {
        HIDComponentIdentifier(0);

        private int value;
        iAPProcessor$StopHID(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class iAPProcessor$StopHID extends Enum {
//    final private static com.navdy.hud.mfi.iAPProcessor$StopHID[] $VALUES;
//    final public static com.navdy.hud.mfi.iAPProcessor$StopHID HIDComponentIdentifier;
//    
//    static {
//        HIDComponentIdentifier = new com.navdy.hud.mfi.iAPProcessor$StopHID("HIDComponentIdentifier", 0);
//        com.navdy.hud.mfi.iAPProcessor$StopHID[] a = new com.navdy.hud.mfi.iAPProcessor$StopHID[1];
//        a[0] = HIDComponentIdentifier;
//        $VALUES = a;
//    }
//    
//    private iAPProcessor$StopHID(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$StopHID valueOf(String s) {
//        return (com.navdy.hud.mfi.iAPProcessor$StopHID)Enum.valueOf(com.navdy.hud.mfi.iAPProcessor$StopHID.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$StopHID[] values() {
//        return $VALUES.clone();
//    }
//}
//