package com.navdy.service.library.device;

import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import com.google.gson.stream.JsonReader;
import com.google.gson.TypeAdapter;
import java.util.Locale;
import android.provider.Settings;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.text.TextUtils;
import android.bluetooth.BluetoothDevice;
import com.google.gson.annotations.JsonAdapter;

@JsonAdapter(Adapter.class)
public class NavdyDeviceId
{
    protected static final String ATTRIBUTE_NAME = "N";
    protected static final String ATTRIBUTE_SEPARATOR = ";";
    protected static final String RESERVED_CHARACTER_REGEX = ";|/";
    public static final NavdyDeviceId UNKNOWN_ID;
    protected static final String VALUE_SEPARATOR = "/";
    static NavdyDeviceId sThisDeviceId;
    protected String mDeviceId;
    protected Type mDeviceIdType;
    protected String mDeviceName;
    protected String mSerializedDeviceId;
    
    static {
        UNKNOWN_ID = new NavdyDeviceId("UNK/FF:FF:FF:FF:FF:FF;N/Unknown");
    }
    
    public NavdyDeviceId(final BluetoothDevice bluetoothDevice) {
        final Type bt = Type.BT;
        final String address = bluetoothDevice.getAddress();
        String name;
        if (TextUtils.isEmpty((CharSequence)bluetoothDevice.getName())) {
            name = "unknown";
        }
        else {
            name = bluetoothDevice.getName();
        }
        this(bt, address, name);
    }
    
    public NavdyDeviceId(final Type mDeviceIdType, final String s, final String s2) {
        this.mDeviceIdType = mDeviceIdType;
        this.mDeviceId = this.normalizeDeviceIdString(s);
        this.mDeviceName = this.normalizeDeviceName(s2);
        this.mSerializedDeviceId = this.createSerializedDeviceId();
    }
    
    public NavdyDeviceId(final String s) {
        if (s == null || s.equals("")) {
            throw new IllegalArgumentException("Cannot create empty NavdyDeviceId");
        }
        if (!this.parseDeviceIdString(s)) {
            throw new IllegalArgumentException("Invalid device ID string.");
        }
        this.mSerializedDeviceId = this.createSerializedDeviceId();
    }
    
    private String createSerializedDeviceId() {
        String s = this.mDeviceIdType.toString() + "/" + this.mDeviceId;
        if (!TextUtils.isEmpty((CharSequence)this.mDeviceName)) {
            s = s + ";N/" + this.mDeviceName;
        }
        return s;
    }
    
    public static NavdyDeviceId getThisDevice(final Context context) {
        if (NavdyDeviceId.sThisDeviceId == null || NavdyDeviceId.sThisDeviceId.equals(NavdyDeviceId.UNKNOWN_ID)) {
            final BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
            if (defaultAdapter == null) {
                if (context != null) {
                    final String string = Settings$Secure.getString(context.getContentResolver(), "android_id");
                    if (TextUtils.isEmpty((CharSequence)string)) {
                        NavdyDeviceId.sThisDeviceId = NavdyDeviceId.UNKNOWN_ID;
                    }
                    else {
                        NavdyDeviceId.sThisDeviceId = new NavdyDeviceId(Type.EMU, string, "emulator");
                    }
                }
            }
            else {
                final String address = defaultAdapter.getAddress();
                if (TextUtils.isEmpty((CharSequence)address)) {
                    NavdyDeviceId.sThisDeviceId = NavdyDeviceId.UNKNOWN_ID;
                }
                else {
                    NavdyDeviceId.sThisDeviceId = new NavdyDeviceId(Type.BT, address, defaultAdapter.getName());
                }
            }
        }
        return NavdyDeviceId.sThisDeviceId;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this != o) {
            if (o == null || this.getClass() != o.getClass()) {
                b = false;
            }
            else {
                final NavdyDeviceId navdyDeviceId = (NavdyDeviceId)o;
                if (!this.mDeviceId.equals(navdyDeviceId.mDeviceId) || this.mDeviceIdType != navdyDeviceId.mDeviceIdType) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    public String getBluetoothAddress() {
        String mDeviceId;
        if (this.mDeviceIdType == Type.BT || this.mDeviceIdType == Type.EA) {
            mDeviceId = this.mDeviceId;
        }
        else {
            mDeviceId = null;
        }
        return mDeviceId;
    }
    
    public String getDeviceName() {
        return this.mDeviceName;
    }
    
    public String getDisplayName() {
        String s;
        if (TextUtils.isEmpty((CharSequence)this.mDeviceName)) {
            s = this.toString();
        }
        else {
            s = this.mDeviceName;
        }
        return s;
    }
    
    @Override
    public int hashCode() {
        return this.mDeviceId.hashCode() * 31 + this.mDeviceIdType.hashCode();
    }
    
    protected String normalizeDeviceIdString(final String s) {
        return s.toUpperCase(Locale.US);
    }
    
    protected String normalizeDeviceName(String replaceAll) {
        if (replaceAll == null) {
            replaceAll = null;
        }
        else if ((replaceAll = replaceAll.replaceAll(";|/", "")).equals("")) {
            replaceAll = null;
        }
        return replaceAll;
    }
    
    protected boolean parseDeviceIdString(final String s) {
        final boolean b = false;
        final String[] split = s.split(";");
        boolean b2;
        if (split.length == 0) {
            b2 = b;
        }
        else {
            for (int length = split.length, i = 0; i < length; ++i) {
                final String[] split2 = split[i].split("/");
                b2 = b;
                if (split2.length < 2) {
                    return b2;
                }
                final String s2 = split2[0];
                final String s3 = split2[1];
                if (!s2.equals("N")) {
                    try {
                        this.mDeviceIdType = Type.valueOf(split2[0]);
                        this.mDeviceId = this.normalizeDeviceIdString(split2[1]);
                        continue;
                    }
                    catch (IllegalArgumentException ex) {
                        throw new IllegalArgumentException("Invalid device ID type");
                    }
                    break;
                }
                this.mDeviceName = this.normalizeDeviceName(s3);
            }
            b2 = true;
        }
        return b2;
    }
    
    @Override
    public String toString() {
        return this.mSerializedDeviceId;
    }
    
    public static class Adapter extends TypeAdapter
    {
        @Override
        public Object read(final JsonReader jsonReader) throws IOException {
            return new NavdyDeviceId(jsonReader.nextString());
        }
        
        @Override
        public void write(final JsonWriter jsonWriter, final Object o) throws IOException {
            jsonWriter.value(o.toString());
        }
    }
    
    public enum Type
    {
        BT, 
        EA, 
        EMU, 
        UNK;
    }
}
