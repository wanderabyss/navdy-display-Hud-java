package com.navdy.service.library.device.discovery;

import com.navdy.service.library.device.connection.ConnectionType;
import android.content.Context;

public class TCPRemoteDeviceScanner extends MDNSRemoteDeviceScanner
{
    public static final String TAG = "TCPRemoteDeviceScanner";
    
    public TCPRemoteDeviceScanner(final Context context) {
        super(context, ConnectionType.TCP_PROTOBUF.getServiceType());
    }
}
