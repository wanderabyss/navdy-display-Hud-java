package com.navdy.service.library.device.link;

import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionType;
import java.util.HashMap;

public class LinkManager
{
    private static LinkFactory sDefaultFactory;
    private static HashMap<ConnectionType, LinkFactory> sFactoryMap;
    
    static {
        LinkManager.sFactoryMap = new HashMap<ConnectionType, LinkFactory>();
        LinkManager.sDefaultFactory = (LinkFactory)new LinkFactory() {
            @Override
            public Link build(final ConnectionInfo connectionInfo) {
                Link link = null;
                switch (connectionInfo.getType()) {
                    default:
                        link = null;
                        break;
                    case BT_PROTOBUF:
                    case TCP_PROTOBUF:
                        link = new ProtobufLink(connectionInfo);
                        break;
                }
                return link;
            }
        };
        registerFactory(ConnectionType.BT_PROTOBUF, LinkManager.sDefaultFactory);
        registerFactory(ConnectionType.TCP_PROTOBUF, LinkManager.sDefaultFactory);
    }
    
    public static Link build(final ConnectionInfo connectionInfo) {
        final LinkFactory linkFactory = LinkManager.sFactoryMap.get(connectionInfo.getType());
        Link build;
        if (linkFactory != null) {
            build = linkFactory.build(connectionInfo);
        }
        else {
            build = null;
        }
        return build;
    }
    
    public static void registerFactory(final ConnectionType connectionType, final LinkFactory linkFactory) {
        if (linkFactory != null) {
            LinkManager.sFactoryMap.put(connectionType, linkFactory);
        }
    }
    
    public interface LinkFactory
    {
        Link build(final ConnectionInfo p0);
    }
}
