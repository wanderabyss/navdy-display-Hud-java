package com.navdy.service.library.file;

import com.navdy.service.library.events.file.FileType;

public interface IFileTransferAuthority
{
    String getDirectoryForFileType(final FileType p0);
    
    String getFileToSend(final FileType p0);
    
    boolean isFileTypeAllowed(final FileType p0);
    
    void onFileSent(final FileType p0);
}
