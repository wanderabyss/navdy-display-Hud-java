package com.navdy.service.library.util;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Arrays;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;
import android.text.TextUtils;
import android.util.Log;
import android.support.annotation.Nullable;
import java.lang.reflect.Field;
import java.io.FileDescriptor;
import android.net.LocalSocket;
import android.bluetooth.BluetoothSocket;
import java.io.Serializable;
import org.json.JSONObject;
import android.os.StatFs;
import android.os.Environment;
import java.util.zip.ZipEntry;
import java.io.FileOutputStream;
import java.util.zip.ZipInputStream;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.Closeable;
import android.os.ParcelFileDescriptor;
import java.io.IOException;
import android.content.Context;
import java.io.OutputStream;
import android.graphics.Bitmap;
import java.io.ByteArrayOutputStream;
import android.graphics.Bitmap;
import com.navdy.service.library.log.Logger;
import java.io.File;
import java.util.concurrent.atomic.AtomicLong;

public final class IOUtils
{
    private static final int BUFFER_SIZE = 16384;
    private static final int BUFFER_SIZE_FOR_DOWNLOADS = 1024;
    private static final String CHECKSUM_FILE_NAME = ".checksum";
    private static final boolean CHECKSUM_UPDATE_MODE = false;
    private static final int DIGEST_BUFFER_SIZE = 1048576;
    private static final int END_OF_STREAM = -1;
    private static final int INPUT_BUFFER_SIZE = 16384;
    private static final String TAG;
    private static final String TRASH_DIR_NAME = ".trash";
    public static final String UTF_8 = "UTF-8";
    private static final AtomicLong sCounter;
    private static File sExternalTrashDir;
    private static final Object sLock;
    private static Logger sLogger;
    private static volatile File sTrashDir;
    
    static {
        TAG = IOUtils.class.getName();
        IOUtils.sLogger = new Logger(IOUtils.class);
        sCounter = new AtomicLong(1L);
        sLock = new Object();
    }
    
    public static byte[] bitmap2ByteBuffer(final Bitmap bitmap) {
        byte[] byteArray;
        if (bitmap == null) {
            byteArray = null;
        }
        else {
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap$CompressFormat.PNG, 100, (OutputStream)byteArrayOutputStream);
            byteArray = byteArrayOutputStream.toByteArray();
        }
        return byteArray;
    }
    
    public static String bytesToHexString(final byte[] array) {
        return bytesToHexString(array, 0, array.length);
    }
    
    public static String bytesToHexString(final byte[] array, int i, final int n) {
        final StringBuilder sb = new StringBuilder();
        while (i < n) {
            final String hexString = Integer.toHexString(array[i] & 0xFF);
            if (hexString.length() == 1) {
                sb.append('0');
            }
            sb.append(hexString);
            ++i;
        }
        return sb.toString();
    }
    
    public static void checkIntegrity(final Context p0, final String p1, final int p2) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: new             Ljava/lang/StringBuilder;
        //     6: dup            
        //     7: invokespecial   java/lang/StringBuilder.<init>:()V
        //    10: ldc             "integrity check for "
        //    12: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    15: aload_1        
        //    16: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    19: ldc             " starting"
        //    21: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    24: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    27: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/String;)V
        //    30: invokestatic    android/os/SystemClock.elapsedRealtime:()J
        //    33: lstore_3       
        //    34: aconst_null    
        //    35: astore          5
        //    37: aconst_null    
        //    38: astore          6
        //    40: aconst_null    
        //    41: astore          7
        //    43: aconst_null    
        //    44: astore          8
        //    46: aload           5
        //    48: astore          9
        //    50: aload_0        
        //    51: invokevirtual   android/content/Context.getResources:()Landroid/content/res/Resources;
        //    54: iload_2        
        //    55: invokevirtual   android/content/res/Resources.openRawResource:(I)Ljava/io/InputStream;
        //    58: astore          10
        //    60: aload           5
        //    62: astore          9
        //    64: aload           10
        //    66: astore          8
        //    68: aload           10
        //    70: astore          7
        //    72: aload           10
        //    74: ldc             "UTF-8"
        //    76: invokestatic    com/navdy/service/library/util/IOUtils.convertInputStreamToString:(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
        //    79: astore          5
        //    81: aload           5
        //    83: astore          9
        //    85: aload           10
        //    87: astore          8
        //    89: aload           10
        //    91: astore          7
        //    93: new             Ljava/lang/StringBuilder;
        //    96: astore          11
        //    98: aload           5
        //   100: astore          9
        //   102: aload           10
        //   104: astore          8
        //   106: aload           10
        //   108: astore          7
        //   110: aload           11
        //   112: invokespecial   java/lang/StringBuilder.<init>:()V
        //   115: aload           5
        //   117: astore          9
        //   119: aload           10
        //   121: astore          8
        //   123: aload           10
        //   125: astore          7
        //   127: aload           11
        //   129: aload_1        
        //   130: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   133: getstatic       java/io/File.separator:Ljava/lang/String;
        //   136: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   139: ldc             ".checksum"
        //   141: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   144: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   147: invokestatic    com/navdy/service/library/util/IOUtils.convertFileToString:(Ljava/lang/String;)Ljava/lang/String;
        //   150: astore          11
        //   152: aload           11
        //   154: astore          7
        //   156: aload           10
        //   158: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   161: aload           5
        //   163: astore          9
        //   165: aload           9
        //   167: ifnull          305
        //   170: aload           7
        //   172: ifnull          305
        //   175: aload           9
        //   177: aload           7
        //   179: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   182: ifeq            305
        //   185: iconst_1       
        //   186: istore_2       
        //   187: iload_2        
        //   188: ifeq            310
        //   191: getstatic       com/navdy/service/library/util/IOUtils.sLogger:Lcom/navdy/service/library/log/Logger;
        //   194: new             Ljava/lang/StringBuilder;
        //   197: dup            
        //   198: invokespecial   java/lang/StringBuilder.<init>:()V
        //   201: ldc             "checksum for "
        //   203: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   206: aload_1        
        //   207: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   210: ldc             " is fine, no-op"
        //   212: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   215: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   218: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   221: invokestatic    android/os/SystemClock.elapsedRealtime:()J
        //   224: lstore          12
        //   226: getstatic       com/navdy/service/library/util/IOUtils.sLogger:Lcom/navdy/service/library/log/Logger;
        //   229: new             Ljava/lang/StringBuilder;
        //   232: dup            
        //   233: invokespecial   java/lang/StringBuilder.<init>:()V
        //   236: ldc             "integrity check for "
        //   238: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   241: aload_1        
        //   242: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   245: ldc             " took "
        //   247: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   250: lload           12
        //   252: lload_3        
        //   253: lsub           
        //   254: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   257: ldc             " ms"
        //   259: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   262: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   265: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/String;)V
        //   268: return         
        //   269: astore          5
        //   271: aload           8
        //   273: astore          7
        //   275: getstatic       com/navdy/service/library/util/IOUtils.sLogger:Lcom/navdy/service/library/log/Logger;
        //   278: ldc             "error while retrieving integrity checksum from filesystem, might not be present"
        //   280: aload           5
        //   282: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   285: aload           8
        //   287: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   290: aload           6
        //   292: astore          7
        //   294: goto            165
        //   297: astore_0       
        //   298: aload           7
        //   300: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   303: aload_0        
        //   304: athrow         
        //   305: iconst_0       
        //   306: istore_2       
        //   307: goto            187
        //   310: aload_1        
        //   311: iconst_1       
        //   312: iconst_1       
        //   313: anewarray       Ljava/lang/String;
        //   316: dup            
        //   317: iconst_0       
        //   318: ldc             ".checksum"
        //   320: aastore        
        //   321: invokestatic    com/navdy/service/library/util/IOUtils.hashForPath:(Ljava/lang/String;Z[Ljava/lang/String;)Ljava/lang/String;
        //   324: astore          5
        //   326: aload           5
        //   328: ifnull          402
        //   331: aload           9
        //   333: ifnull          402
        //   336: aload           5
        //   338: aload           9
        //   340: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   343: ifeq            402
        //   346: iconst_1       
        //   347: istore_2       
        //   348: iload_2        
        //   349: ifne            407
        //   352: getstatic       com/navdy/service/library/util/IOUtils.sLogger:Lcom/navdy/service/library/log/Logger;
        //   355: new             Ljava/lang/StringBuilder;
        //   358: dup            
        //   359: invokespecial   java/lang/StringBuilder.<init>:()V
        //   362: ldc             "files on "
        //   364: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   367: aload_1        
        //   368: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   371: ldc             " are out of date or corrupted, redoing"
        //   373: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   376: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   379: invokevirtual   com/navdy/service/library/log/Logger.w:(Ljava/lang/String;)V
        //   382: aload_0        
        //   383: new             Ljava/io/File;
        //   386: dup            
        //   387: aload_1        
        //   388: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   391: invokestatic    com/navdy/service/library/util/IOUtils.deleteDirectory:(Landroid/content/Context;Ljava/io/File;)V
        //   394: aload_1        
        //   395: invokestatic    com/navdy/service/library/util/IOUtils.createDirectory:(Ljava/lang/String;)Z
        //   398: pop            
        //   399: goto            221
        //   402: iconst_0       
        //   403: istore_2       
        //   404: goto            348
        //   407: getstatic       com/navdy/service/library/util/IOUtils.sLogger:Lcom/navdy/service/library/log/Logger;
        //   410: new             Ljava/lang/StringBuilder;
        //   413: dup            
        //   414: invokespecial   java/lang/StringBuilder.<init>:()V
        //   417: ldc             "checksum for "
        //   419: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   422: aload_1        
        //   423: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   426: ldc             " is fine, writing integrity checksum on filesystem"
        //   428: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   431: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   434: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   437: aconst_null    
        //   438: astore          7
        //   440: aconst_null    
        //   441: astore          8
        //   443: aload           7
        //   445: astore_0       
        //   446: new             Ljava/io/PrintWriter;
        //   449: astore          9
        //   451: aload           7
        //   453: astore_0       
        //   454: new             Ljava/lang/StringBuilder;
        //   457: astore          10
        //   459: aload           7
        //   461: astore_0       
        //   462: aload           10
        //   464: invokespecial   java/lang/StringBuilder.<init>:()V
        //   467: aload           7
        //   469: astore_0       
        //   470: aload           9
        //   472: aload           10
        //   474: aload_1        
        //   475: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   478: getstatic       java/io/File.separator:Ljava/lang/String;
        //   481: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   484: ldc             ".checksum"
        //   486: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   489: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   492: invokespecial   java/io/PrintWriter.<init>:(Ljava/lang/String;)V
        //   495: aload           9
        //   497: aload           5
        //   499: invokevirtual   java/io/PrintWriter.print:(Ljava/lang/String;)V
        //   502: aload           9
        //   504: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   507: goto            221
        //   510: astore          7
        //   512: aload           8
        //   514: astore          9
        //   516: aload           9
        //   518: astore_0       
        //   519: getstatic       com/navdy/service/library/util/IOUtils.sLogger:Lcom/navdy/service/library/log/Logger;
        //   522: ldc             "could not write integrity checksum on filesystem"
        //   524: aload           7
        //   526: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   529: aload           9
        //   531: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   534: goto            221
        //   537: astore_1       
        //   538: aload_0        
        //   539: astore          9
        //   541: aload           9
        //   543: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   546: aload_1        
        //   547: athrow         
        //   548: astore_0       
        //   549: aload_0        
        //   550: astore_1       
        //   551: goto            541
        //   554: astore          7
        //   556: goto            516
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  50     60     269    297    Ljava/lang/Throwable;
        //  50     60     297    305    Any
        //  72     81     269    297    Ljava/lang/Throwable;
        //  72     81     297    305    Any
        //  93     98     269    297    Ljava/lang/Throwable;
        //  93     98     297    305    Any
        //  110    115    269    297    Ljava/lang/Throwable;
        //  110    115    297    305    Any
        //  127    152    269    297    Ljava/lang/Throwable;
        //  127    152    297    305    Any
        //  275    285    297    305    Any
        //  446    451    510    516    Ljava/lang/Throwable;
        //  446    451    537    541    Any
        //  454    459    510    516    Ljava/lang/Throwable;
        //  454    459    537    541    Any
        //  462    467    510    516    Ljava/lang/Throwable;
        //  462    467    537    541    Any
        //  470    495    510    516    Ljava/lang/Throwable;
        //  470    495    537    541    Any
        //  495    502    554    559    Ljava/lang/Throwable;
        //  495    502    548    554    Any
        //  519    529    537    541    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0516:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static void cleanDirectory(final Context context, final File file) throws IOException {
        if (file.exists() && file.isDirectory()) {
            final File[] listFiles = file.listFiles();
            if (listFiles != null) {
                final IOException ex = null;
                final int length = listFiles.length;
                int i = 0;
            Label_0050_Outer:
                while (i < length) {
                    final File file2 = listFiles[i];
                    while (true) {
                        try {
                            forceDelete(context, file2);
                            ++i;
                            continue Label_0050_Outer;
                        }
                        catch (IOException ex) {
                            continue;
                        }
                        break;
                    }
                    break;
                }
                if (ex != null) {
                    throw ex;
                }
            }
        }
    }
    
    public static void closeFD(final int n) {
        if (n == -1) {
            return;
        }
        try {
            ParcelFileDescriptor.adoptFd(n).close();
        }
        catch (Throwable t) {
            IOUtils.sLogger.e(t);
        }
    }
    
    public static void closeObject(final Closeable closeable) {
        if (closeable == null) {
            return;
        }
        try {
            closeable.close();
        }
        catch (Throwable t) {
            IOUtils.sLogger.e(t);
        }
    }
    
    public static void closeStream(final Closeable closeable) {
        closeObject(closeable);
    }
    
    public static void compressFilesToZip(final Context p0, final File[] p1, final String p2) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: aload_2        
        //     5: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //     8: astore_3       
        //     9: aload_3        
        //    10: invokevirtual   java/io/File.exists:()Z
        //    13: ifeq            22
        //    16: aload_0        
        //    17: aload_2        
        //    18: invokestatic    com/navdy/service/library/util/IOUtils.deleteFile:(Landroid/content/Context;Ljava/lang/String;)Z
        //    21: pop            
        //    22: aload_3        
        //    23: invokevirtual   java/io/File.createNewFile:()Z
        //    26: istore          4
        //    28: iload           4
        //    30: ifne            47
        //    33: return         
        //    34: astore_0       
        //    35: getstatic       com/navdy/service/library/util/IOUtils.sLogger:Lcom/navdy/service/library/log/Logger;
        //    38: ldc_w           "IO Exception while creating new file"
        //    41: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //    44: goto            33
        //    47: aload_1        
        //    48: ifnull          33
        //    51: aconst_null    
        //    52: astore          5
        //    54: aconst_null    
        //    55: astore_3       
        //    56: aconst_null    
        //    57: astore          6
        //    59: aconst_null    
        //    60: astore          7
        //    62: aconst_null    
        //    63: astore          8
        //    65: aconst_null    
        //    66: astore          9
        //    68: aconst_null    
        //    69: astore          10
        //    71: aconst_null    
        //    72: astore          11
        //    74: aconst_null    
        //    75: astore          12
        //    77: aconst_null    
        //    78: astore          13
        //    80: aload           7
        //    82: astore          14
        //    84: aload           5
        //    86: astore          15
        //    88: aload           11
        //    90: astore          16
        //    92: sipush          16384
        //    95: newarray        B
        //    97: astore          17
        //    99: aload           7
        //   101: astore          14
        //   103: aload           5
        //   105: astore          15
        //   107: aload           11
        //   109: astore          16
        //   111: new             Ljava/io/FileOutputStream;
        //   114: astore_0       
        //   115: aload           7
        //   117: astore          14
        //   119: aload           5
        //   121: astore          15
        //   123: aload           11
        //   125: astore          16
        //   127: aload_0        
        //   128: aload_2        
        //   129: invokespecial   java/io/FileOutputStream.<init>:(Ljava/lang/String;)V
        //   132: new             Ljava/util/zip/ZipOutputStream;
        //   135: astore_3       
        //   136: aload_3        
        //   137: aload_0        
        //   138: invokespecial   java/util/zip/ZipOutputStream.<init>:(Ljava/io/OutputStream;)V
        //   141: aload           10
        //   143: astore          14
        //   145: aload           9
        //   147: astore_2       
        //   148: aload_1        
        //   149: arraylength    
        //   150: istore          18
        //   152: iconst_0       
        //   153: istore          19
        //   155: aconst_null    
        //   156: astore_2       
        //   157: iload           19
        //   159: iload           18
        //   161: if_icmpge       352
        //   164: aload_1        
        //   165: iload           19
        //   167: aaload         
        //   168: astore          16
        //   170: aload           16
        //   172: invokevirtual   java/io/File.exists:()Z
        //   175: ifeq            463
        //   178: aload           16
        //   180: invokevirtual   java/io/File.canRead:()Z
        //   183: ifne            192
        //   186: iinc            19, 1
        //   189: goto            157
        //   192: new             Ljava/io/FileInputStream;
        //   195: astore          15
        //   197: aload           15
        //   199: aload           16
        //   201: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //   204: aload           15
        //   206: astore          14
        //   208: aload           15
        //   210: astore_2       
        //   211: new             Ljava/util/zip/ZipEntry;
        //   214: astore          13
        //   216: aload           15
        //   218: astore          14
        //   220: aload           15
        //   222: astore_2       
        //   223: aload           13
        //   225: aload           16
        //   227: invokevirtual   java/io/File.getName:()Ljava/lang/String;
        //   230: invokespecial   java/util/zip/ZipEntry.<init>:(Ljava/lang/String;)V
        //   233: aload           15
        //   235: astore          14
        //   237: aload           15
        //   239: astore_2       
        //   240: aload_3        
        //   241: aload           13
        //   243: invokevirtual   java/util/zip/ZipOutputStream.putNextEntry:(Ljava/util/zip/ZipEntry;)V
        //   246: aload           15
        //   248: astore          14
        //   250: aload           15
        //   252: astore_2       
        //   253: aload           15
        //   255: aload           17
        //   257: invokevirtual   java/io/FileInputStream.read:([B)I
        //   260: istore          20
        //   262: iload           20
        //   264: ifle            324
        //   267: aload           15
        //   269: astore          14
        //   271: aload           15
        //   273: astore_2       
        //   274: aload_3        
        //   275: aload           17
        //   277: iconst_0       
        //   278: iload           20
        //   280: invokevirtual   java/util/zip/ZipOutputStream.write:([BII)V
        //   283: goto            246
        //   286: astore_2       
        //   287: aload           14
        //   289: astore_1       
        //   290: aload_1        
        //   291: astore          14
        //   293: aload_0        
        //   294: astore          15
        //   296: aload_3        
        //   297: astore          16
        //   299: getstatic       com/navdy/service/library/util/IOUtils.sLogger:Lcom/navdy/service/library/log/Logger;
        //   302: ldc_w           "Error while compressing files "
        //   305: aload_2        
        //   306: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   309: aload_3        
        //   310: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   313: aload_1        
        //   314: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   317: aload_0        
        //   318: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   321: goto            33
        //   324: aload           15
        //   326: astore          14
        //   328: aload           15
        //   330: astore_2       
        //   331: aload_3        
        //   332: invokevirtual   java/util/zip/ZipOutputStream.closeEntry:()V
        //   335: aload           15
        //   337: astore          14
        //   339: aload           15
        //   341: astore_2       
        //   342: aload           15
        //   344: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   347: aconst_null    
        //   348: astore_2       
        //   349: goto            186
        //   352: aload_3        
        //   353: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   356: aload_2        
        //   357: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   360: aload_0        
        //   361: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   364: goto            33
        //   367: astore_3       
        //   368: aload           16
        //   370: astore_2       
        //   371: aload           15
        //   373: astore_1       
        //   374: aload           14
        //   376: astore_0       
        //   377: aload_2        
        //   378: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   381: aload_0        
        //   382: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   385: aload_1        
        //   386: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   389: aload_3        
        //   390: athrow         
        //   391: astore_3       
        //   392: aload_0        
        //   393: astore_1       
        //   394: aload           8
        //   396: astore_0       
        //   397: aload           12
        //   399: astore_2       
        //   400: goto            377
        //   403: astore          14
        //   405: aload_0        
        //   406: astore_1       
        //   407: aload_2        
        //   408: astore_0       
        //   409: aload_3        
        //   410: astore_2       
        //   411: aload           14
        //   413: astore_3       
        //   414: goto            377
        //   417: astore          14
        //   419: aload_0        
        //   420: astore_1       
        //   421: aload_2        
        //   422: astore_0       
        //   423: aload_3        
        //   424: astore_2       
        //   425: aload           14
        //   427: astore_3       
        //   428: goto            377
        //   431: astore_2       
        //   432: aload           6
        //   434: astore_1       
        //   435: aload_3        
        //   436: astore_0       
        //   437: aload           13
        //   439: astore_3       
        //   440: goto            290
        //   443: astore_2       
        //   444: aload           6
        //   446: astore_1       
        //   447: aload           13
        //   449: astore_3       
        //   450: goto            290
        //   453: astore          14
        //   455: aload_2        
        //   456: astore_1       
        //   457: aload           14
        //   459: astore_2       
        //   460: goto            290
        //   463: goto            186
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  22     28     34     47     Ljava/io/IOException;
        //  92     99     431    443    Ljava/lang/Throwable;
        //  92     99     367    377    Any
        //  111    115    431    443    Ljava/lang/Throwable;
        //  111    115    367    377    Any
        //  127    132    431    443    Ljava/lang/Throwable;
        //  127    132    367    377    Any
        //  132    141    443    453    Ljava/lang/Throwable;
        //  132    141    391    403    Any
        //  148    152    286    290    Ljava/lang/Throwable;
        //  148    152    403    417    Any
        //  170    186    453    463    Ljava/lang/Throwable;
        //  170    186    417    431    Any
        //  192    204    453    463    Ljava/lang/Throwable;
        //  192    204    417    431    Any
        //  211    216    286    290    Ljava/lang/Throwable;
        //  211    216    403    417    Any
        //  223    233    286    290    Ljava/lang/Throwable;
        //  223    233    403    417    Any
        //  240    246    286    290    Ljava/lang/Throwable;
        //  240    246    403    417    Any
        //  253    262    286    290    Ljava/lang/Throwable;
        //  253    262    403    417    Any
        //  274    283    286    290    Ljava/lang/Throwable;
        //  274    283    403    417    Any
        //  299    309    367    377    Any
        //  331    335    286    290    Ljava/lang/Throwable;
        //  331    335    403    417    Any
        //  342    347    286    290    Ljava/lang/Throwable;
        //  342    347    403    417    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 254, Size: 254
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3435)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static String convertFileToString(String s) throws IOException {
        s = (String)new FileInputStream(s);
        try {
            return convertInputStreamToString((InputStream)s, "UTF-8");
        }
        finally {
            closeStream((Closeable)s);
        }
    }
    
    public static String convertInputStreamToString(final InputStream inputStream, final String s) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final byte[] array = new byte[16384];
        while (true) {
            final int read = inputStream.read(array);
            if (read == -1) {
                break;
            }
            byteArrayOutputStream.write(array, 0, read);
        }
        return byteArrayOutputStream.toString(s);
    }
    
    public static int copyFile(final String p0, final InputStream p1) throws IOException {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_2       
        //     2: iconst_0       
        //     3: istore_3       
        //     4: new             Ljava/io/FileOutputStream;
        //     7: astore          4
        //     9: aload           4
        //    11: aload_0        
        //    12: invokespecial   java/io/FileOutputStream.<init>:(Ljava/lang/String;)V
        //    15: sipush          16384
        //    18: newarray        B
        //    20: astore_0       
        //    21: aload_1        
        //    22: aload_0        
        //    23: invokevirtual   java/io/InputStream.read:([B)I
        //    26: istore          5
        //    28: iload           5
        //    30: ifle            50
        //    33: aload           4
        //    35: aload_0        
        //    36: iconst_0       
        //    37: iload           5
        //    39: invokevirtual   java/io/FileOutputStream.write:([BII)V
        //    42: iload_3        
        //    43: iload           5
        //    45: iadd           
        //    46: istore_3       
        //    47: goto            21
        //    50: aload_1        
        //    51: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //    54: aload           4
        //    56: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
        //    59: aload           4
        //    61: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //    64: iload_3        
        //    65: ireturn        
        //    66: astore_0       
        //    67: aload_1        
        //    68: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //    71: aload_2        
        //    72: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
        //    75: aload_2        
        //    76: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //    79: aload_0        
        //    80: athrow         
        //    81: astore_0       
        //    82: aload           4
        //    84: astore_2       
        //    85: goto            67
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  4      15     66     67     Any
        //  15     21     81     88     Any
        //  21     28     81     88     Any
        //  33     42     81     88     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0021:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static int copyFile(final String s, final byte[] array) throws IOException {
        return copyFile(s, array, true);
    }
    
    public static int copyFile(final String p0, final byte[] p1, final boolean p2) throws IOException {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_3       
        //     2: new             Ljava/io/FileOutputStream;
        //     5: astore          4
        //     7: aload           4
        //     9: aload_0        
        //    10: invokespecial   java/io/FileOutputStream.<init>:(Ljava/lang/String;)V
        //    13: aload           4
        //    15: aload_1        
        //    16: iconst_0       
        //    17: aload_1        
        //    18: arraylength    
        //    19: invokevirtual   java/io/FileOutputStream.write:([BII)V
        //    22: aload_1        
        //    23: arraylength    
        //    24: istore          5
        //    26: iload_2        
        //    27: ifeq            35
        //    30: aload           4
        //    32: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
        //    35: aload           4
        //    37: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //    40: iconst_0       
        //    41: iload           5
        //    43: iadd           
        //    44: ireturn        
        //    45: astore_0       
        //    46: aload_3        
        //    47: astore_1       
        //    48: iload_2        
        //    49: ifeq            56
        //    52: aload_1        
        //    53: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
        //    56: aload_1        
        //    57: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //    60: aload_0        
        //    61: athrow         
        //    62: astore_0       
        //    63: aload           4
        //    65: astore_1       
        //    66: goto            48
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  2      13     45     48     Any
        //  13     26     62     69     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0035:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static boolean copyFile(final String p0, final String p1) throws IOException {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_2       
        //     2: new             Ljava/io/File;
        //     5: astore_3       
        //     6: aload_3        
        //     7: aload_0        
        //     8: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    11: aload_3        
        //    12: invokevirtual   java/io/File.exists:()Z
        //    15: ifeq            46
        //    18: new             Ljava/io/FileInputStream;
        //    21: astore          4
        //    23: aload           4
        //    25: aload_3        
        //    26: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //    29: aload_1        
        //    30: aload           4
        //    32: invokestatic    com/navdy/service/library/util/IOUtils.copyFile:(Ljava/lang/String;Ljava/io/InputStream;)I
        //    35: pop            
        //    36: iconst_1       
        //    37: istore          5
        //    39: aconst_null    
        //    40: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //    43: iload           5
        //    45: ireturn        
        //    46: aconst_null    
        //    47: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //    50: iconst_0       
        //    51: istore          5
        //    53: goto            43
        //    56: astore_0       
        //    57: aload_2        
        //    58: astore_1       
        //    59: aload_1        
        //    60: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //    63: aload_0        
        //    64: athrow         
        //    65: astore_0       
        //    66: aload           4
        //    68: astore_1       
        //    69: goto            59
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  2      29     56     59     Any
        //  29     36     65     72     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0043:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static boolean createDirectory(final File file) {
        final boolean b = false;
        boolean mkdirs;
        if (file != null && !file.exists()) {
            mkdirs = file.mkdirs();
        }
        else {
            mkdirs = b;
            if (file != null) {
                mkdirs = b;
                if (file.isDirectory()) {
                    mkdirs = true;
                    IOUtils.sLogger.v("Directory already exists: " + file.getPath());
                }
            }
        }
        if (!mkdirs) {
            final Logger sLogger = IOUtils.sLogger;
            final StringBuilder append = new StringBuilder().append("Unable to create directory: ");
            String path;
            if (file != null) {
                path = file.getPath();
            }
            else {
                path = "null";
            }
            sLogger.e(append.append(path).toString());
        }
        else {
            IOUtils.sLogger.v("Successfully created directory: " + file.getPath());
        }
        return mkdirs;
    }
    
    public static boolean createDirectory(final String s) {
        return createDirectory(new File(s));
    }
    
    public static void deCompressZipToDirectory(final Context context, final File file, final File file2) throws IOException {
        final byte[] array = new byte[1024];
        final ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(file));
        for (ZipEntry zipEntry = zipInputStream.getNextEntry(); zipEntry != null; zipEntry = zipInputStream.getNextEntry()) {
            final File file3 = new File(file2, zipEntry.getName());
            new File(file3.getParent()).mkdirs();
            final FileOutputStream fileOutputStream = new FileOutputStream(file3);
            while (true) {
                final int read = zipInputStream.read(array);
                if (read <= 0) {
                    break;
                }
                fileOutputStream.write(array, 0, read);
            }
            fileOutputStream.close();
        }
        zipInputStream.closeEntry();
        zipInputStream.close();
    }
    
    public static void deleteDirectory(final Context context, File file) {
        if (file != null && file.exists()) {
            File file2;
            if (isAppsInternalFile(context, file.getAbsolutePath())) {
                file2 = new File(getNewTrashEntryPath(context));
            }
            else if (isAppsExternalFile(context, file.getAbsolutePath())) {
                file2 = new File(getNewExternalTrashEntryPath(context));
            }
            else {
                file2 = new File(getTrashEntryPathInSameFolder(file));
            }
            final boolean renameTo = file.renameTo(file2);
            if (!renameTo) {
                IOUtils.sLogger.e("Unable to rename dir " + file + " to: " + file2);
            }
            if (renameTo) {
                file = file2;
            }
            try {
                deleteDirectoryInternal(context, file);
            }
            catch (IOException ex) {
                IOUtils.sLogger.e(ex);
            }
        }
    }
    
    private static void deleteDirectoryInternal(final Context context, final File file) throws IOException {
        if (file.exists()) {
            cleanDirectory(context, file);
            if (!file.delete()) {
                throw new IOException("Unable to delete directory " + file);
            }
        }
    }
    
    public static boolean deleteFile(final Context context, final String s) {
        boolean delete = false;
        if (s != null) {
            final File file = new File(s);
            if (file.exists()) {
                File file2;
                if (isAppsInternalFile(context, s)) {
                    file2 = new File(getNewTrashEntryPath(context));
                }
                else if (isAppsExternalFile(context, s)) {
                    file2 = new File(getNewExternalTrashEntryPath(context));
                }
                else {
                    file2 = new File(getTrashEntryPathInSameFolder(file));
                }
                IOUtils.sLogger.d("Deleted file: " + s + ":" + file.renameTo(file2));
                delete = file2.delete();
            }
        }
        return delete;
    }
    
    public static byte[] downloadImage(final String p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_1       
        //     2: aconst_null    
        //     3: astore_2       
        //     4: aconst_null    
        //     5: astore_3       
        //     6: aconst_null    
        //     7: astore          4
        //     9: aconst_null    
        //    10: astore          5
        //    12: aconst_null    
        //    13: astore          6
        //    15: aconst_null    
        //    16: astore          7
        //    18: aload_3        
        //    19: astore          8
        //    21: aload           5
        //    23: astore          9
        //    25: aload_2        
        //    26: astore          10
        //    28: aload_1        
        //    29: astore          11
        //    31: new             Ljava/net/URL;
        //    34: astore          12
        //    36: aload_3        
        //    37: astore          8
        //    39: aload           5
        //    41: astore          9
        //    43: aload_2        
        //    44: astore          10
        //    46: aload_1        
        //    47: astore          11
        //    49: aload           12
        //    51: aload_0        
        //    52: invokespecial   java/net/URL.<init>:(Ljava/lang/String;)V
        //    55: aload_3        
        //    56: astore          8
        //    58: aload           5
        //    60: astore          9
        //    62: aload_2        
        //    63: astore          10
        //    65: aload_1        
        //    66: astore          11
        //    68: aload           12
        //    70: invokevirtual   java/net/URL.openConnection:()Ljava/net/URLConnection;
        //    73: checkcast       Ljava/net/HttpURLConnection;
        //    76: astore_0       
        //    77: aload_3        
        //    78: astore          8
        //    80: aload           5
        //    82: astore          9
        //    84: aload_0        
        //    85: astore          10
        //    87: aload_0        
        //    88: astore          11
        //    90: new             Ljava/io/BufferedInputStream;
        //    93: astore_2       
        //    94: aload_3        
        //    95: astore          8
        //    97: aload           5
        //    99: astore          9
        //   101: aload_0        
        //   102: astore          10
        //   104: aload_0        
        //   105: astore          11
        //   107: aload_2        
        //   108: aload_0        
        //   109: invokevirtual   java/net/HttpURLConnection.getInputStream:()Ljava/io/InputStream;
        //   112: invokespecial   java/io/BufferedInputStream.<init>:(Ljava/io/InputStream;)V
        //   115: new             Ljava/io/ByteArrayOutputStream;
        //   118: astore          10
        //   120: aload           10
        //   122: invokespecial   java/io/ByteArrayOutputStream.<init>:()V
        //   125: sipush          1024
        //   128: newarray        B
        //   130: astore          8
        //   132: aload_2        
        //   133: aload           8
        //   135: invokevirtual   java/io/BufferedInputStream.read:([B)I
        //   138: istore          13
        //   140: iload           13
        //   142: iflt            208
        //   145: aload           10
        //   147: aload           8
        //   149: iconst_0       
        //   150: iload           13
        //   152: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
        //   155: goto            132
        //   158: astore          6
        //   160: aload           10
        //   162: astore          7
        //   164: aload_2        
        //   165: astore          8
        //   167: aload           7
        //   169: astore          9
        //   171: aload_0        
        //   172: astore          10
        //   174: getstatic       com/navdy/service/library/util/IOUtils.sLogger:Lcom/navdy/service/library/log/Logger;
        //   177: ldc_w           "Exception while downloading binary resource"
        //   180: aload           6
        //   182: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   185: aconst_null    
        //   186: astore          8
        //   188: aload_0        
        //   189: ifnull          196
        //   192: aload_0        
        //   193: invokevirtual   java/net/HttpURLConnection.disconnect:()V
        //   196: aload_2        
        //   197: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   200: aload           7
        //   202: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   205: aload           8
        //   207: areturn        
        //   208: aload           10
        //   210: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
        //   213: astore          8
        //   215: aload_0        
        //   216: ifnull          223
        //   219: aload_0        
        //   220: invokevirtual   java/net/HttpURLConnection.disconnect:()V
        //   223: aload_2        
        //   224: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   227: aload           10
        //   229: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   232: goto            205
        //   235: astore_0       
        //   236: aload           10
        //   238: ifnull          246
        //   241: aload           10
        //   243: invokevirtual   java/net/HttpURLConnection.disconnect:()V
        //   246: aload           8
        //   248: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   251: aload           9
        //   253: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   256: aload_0        
        //   257: athrow         
        //   258: astore          11
        //   260: aload_2        
        //   261: astore          8
        //   263: aload           6
        //   265: astore          9
        //   267: aload_0        
        //   268: astore          10
        //   270: aload           11
        //   272: astore_0       
        //   273: goto            236
        //   276: astore          11
        //   278: aload_2        
        //   279: astore          8
        //   281: aload           10
        //   283: astore          9
        //   285: aload_0        
        //   286: astore          10
        //   288: aload           11
        //   290: astore_0       
        //   291: goto            236
        //   294: astore          6
        //   296: aload           4
        //   298: astore_2       
        //   299: aload           11
        //   301: astore_0       
        //   302: goto            164
        //   305: astore          6
        //   307: goto            164
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  31     36     294    305    Ljava/lang/Throwable;
        //  31     36     235    236    Any
        //  49     55     294    305    Ljava/lang/Throwable;
        //  49     55     235    236    Any
        //  68     77     294    305    Ljava/lang/Throwable;
        //  68     77     235    236    Any
        //  90     94     294    305    Ljava/lang/Throwable;
        //  90     94     235    236    Any
        //  107    115    294    305    Ljava/lang/Throwable;
        //  107    115    235    236    Any
        //  115    125    305    310    Ljava/lang/Throwable;
        //  115    125    258    276    Any
        //  125    132    158    164    Ljava/lang/Throwable;
        //  125    132    276    294    Any
        //  132    140    158    164    Ljava/lang/Throwable;
        //  132    140    276    294    Any
        //  145    155    158    164    Ljava/lang/Throwable;
        //  145    155    276    294    Any
        //  174    185    235    236    Any
        //  208    215    158    164    Ljava/lang/Throwable;
        //  208    215    276    294    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0132:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void fileSync(final FileOutputStream fileOutputStream) {
        if (fileOutputStream == null) {
            return;
        }
        try {
            fileOutputStream.getFD().sync();
        }
        catch (Throwable t) {
            IOUtils.sLogger.e(t);
        }
    }
    
    private static void forceDelete(final Context context, final File file) throws IOException {
        if (file.isDirectory()) {
            deleteDirectory(context, file);
        }
        else if (!file.delete()) {
            IOUtils.sLogger.e("Unable to delete kernel crash file: " + file);
        }
    }
    
    public static String getExternalStorageFolderPath() {
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return null;
        }
        final File externalStorageDirectory = Environment.getExternalStorageDirectory();
        if (externalStorageDirectory == null || !externalStorageDirectory.exists()) {
            return null;
        }
        return externalStorageDirectory.getPath();
        path = null;
        return path;
    }
    
    public static long getFreeSpace(final String s) {
        final File file = new File(s);
        long n;
        if (!file.exists() || !file.isDirectory()) {
            n = -1L;
        }
        else {
            final StatFs statFs = new StatFs(s);
            n = statFs.getFreeBlocks() * statFs.getBlockSize();
        }
        return n;
    }
    
    public static JSONObject getJSONFromURL(final String p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_1       
        //     2: aconst_null    
        //     3: astore_2       
        //     4: aconst_null    
        //     5: astore_3       
        //     6: aconst_null    
        //     7: astore          4
        //     9: aload_2        
        //    10: astore          5
        //    12: aload_3        
        //    13: astore          6
        //    15: aload_1        
        //    16: astore          7
        //    18: new             Ljava/net/URL;
        //    21: astore          8
        //    23: aload_2        
        //    24: astore          5
        //    26: aload_3        
        //    27: astore          6
        //    29: aload_1        
        //    30: astore          7
        //    32: aload           8
        //    34: aload_0        
        //    35: invokespecial   java/net/URL.<init>:(Ljava/lang/String;)V
        //    38: aload_2        
        //    39: astore          5
        //    41: aload_3        
        //    42: astore          6
        //    44: aload_1        
        //    45: astore          7
        //    47: aload           8
        //    49: invokevirtual   java/net/URL.openConnection:()Ljava/net/URLConnection;
        //    52: checkcast       Ljava/net/HttpURLConnection;
        //    55: astore_0       
        //    56: aload_0        
        //    57: astore          5
        //    59: aload_3        
        //    60: astore          6
        //    62: aload_0        
        //    63: astore          7
        //    65: new             Ljava/io/InputStreamReader;
        //    68: astore_2       
        //    69: aload_0        
        //    70: astore          5
        //    72: aload_3        
        //    73: astore          6
        //    75: aload_0        
        //    76: astore          7
        //    78: aload_2        
        //    79: aload_0        
        //    80: invokevirtual   java/net/HttpURLConnection.getInputStream:()Ljava/io/InputStream;
        //    83: ldc             "UTF-8"
        //    85: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;Ljava/lang/String;)V
        //    88: aload_0        
        //    89: invokevirtual   java/net/HttpURLConnection.getInputStream:()Ljava/io/InputStream;
        //    92: ldc             "UTF-8"
        //    94: invokestatic    com/navdy/service/library/util/IOUtils.convertInputStreamToString:(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
        //    97: astore          6
        //    99: new             Lorg/json/JSONObject;
        //   102: astore          7
        //   104: aload           7
        //   106: aload           6
        //   108: invokespecial   org/json/JSONObject.<init>:(Ljava/lang/String;)V
        //   111: aload_0        
        //   112: ifnull          119
        //   115: aload_0        
        //   116: invokevirtual   java/net/HttpURLConnection.disconnect:()V
        //   119: aload_2        
        //   120: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   123: aload           7
        //   125: astore_0       
        //   126: aload_0        
        //   127: areturn        
        //   128: astore_2       
        //   129: aload           5
        //   131: astore_0       
        //   132: aload           4
        //   134: astore          5
        //   136: aload           5
        //   138: astore          6
        //   140: aload_0        
        //   141: astore          7
        //   143: getstatic       com/navdy/service/library/util/IOUtils.sLogger:Lcom/navdy/service/library/log/Logger;
        //   146: ldc_w           "Exception while downloading/parsing json"
        //   149: aload_2        
        //   150: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   153: aconst_null    
        //   154: astore          7
        //   156: aload_0        
        //   157: ifnull          164
        //   160: aload_0        
        //   161: invokevirtual   java/net/HttpURLConnection.disconnect:()V
        //   164: aload           5
        //   166: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   169: aload           7
        //   171: astore_0       
        //   172: goto            126
        //   175: astore          5
        //   177: aload           7
        //   179: astore_0       
        //   180: aload_0        
        //   181: ifnull          188
        //   184: aload_0        
        //   185: invokevirtual   java/net/HttpURLConnection.disconnect:()V
        //   188: aload           6
        //   190: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   193: aload           5
        //   195: athrow         
        //   196: astore          7
        //   198: aload_2        
        //   199: astore          6
        //   201: aload           7
        //   203: astore          5
        //   205: goto            180
        //   208: astore          7
        //   210: aload_2        
        //   211: astore          5
        //   213: aload           7
        //   215: astore_2       
        //   216: goto            136
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  18     23     128    136    Ljava/lang/Throwable;
        //  18     23     175    180    Any
        //  32     38     128    136    Ljava/lang/Throwable;
        //  32     38     175    180    Any
        //  47     56     128    136    Ljava/lang/Throwable;
        //  47     56     175    180    Any
        //  65     69     128    136    Ljava/lang/Throwable;
        //  65     69     175    180    Any
        //  78     88     128    136    Ljava/lang/Throwable;
        //  78     88     175    180    Any
        //  88     111    208    219    Ljava/lang/Throwable;
        //  88     111    196    208    Any
        //  143    153    175    180    Any
        // 
        // The error that occurred was:
        // 
        // java.util.ConcurrentModificationException
        //     at java.util.ArrayList$Itr.checkForComodification(Unknown Source)
        //     at java.util.ArrayList$Itr.next(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertLocalVariables(AstBuilder.java:2863)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2445)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static String getNewExternalTrashEntryPath(final Context context) {
        Serializable sExternalTrashDir = null;
        Serializable s = null;
        if (context == null) {
            s = sExternalTrashDir;
        }
        else {
            final Object sLock = IOUtils.sLock;
            Label_0147: {
                synchronized (sLock) {
                    if (IOUtils.sExternalTrashDir == null) {
                        final File externalFilesDir = context.getExternalFilesDir((String)null);
                        if (externalFilesDir == null) {
                            break Label_0147;
                        }
                        sExternalTrashDir = new File(externalFilesDir.getAbsolutePath() + File.separator + ".trash");
                        createDirectory(IOUtils.sExternalTrashDir = (File)sExternalTrashDir);
                    }
                    new StringBuilder().append(IOUtils.sExternalTrashDir.getAbsolutePath()).append(File.separator).append(System.currentTimeMillis()).append("_").append(IOUtils.sCounter.getAndIncrement()).toString();
                    return (String)s;
                }
            }
            // monitorexit(o)
            s = sExternalTrashDir;
        }
        return (String)s;
    }
    
    private static String getNewTrashEntryPath(final Context context) {
        Label_0077: {
            if (IOUtils.sTrashDir != null) {
                break Label_0077;
            }
            final Object sLock = IOUtils.sLock;
            synchronized (sLock) {
                if (IOUtils.sTrashDir == null) {
                    createDirectory(IOUtils.sTrashDir = new File(context.getFilesDir().getAbsolutePath() + File.separator + ".trash"));
                }
                // monitorexit(sLock)
                return IOUtils.sTrashDir.getAbsolutePath() + File.separator + System.currentTimeMillis() + "_" + IOUtils.sCounter.getAndIncrement();
            }
        }
    }
    
    public static int getSocketFD(final BluetoothSocket bluetoothSocket) {
        if (bluetoothSocket == null) {
            return -1;
        }
        try {
            final Field declaredField = BluetoothSocket.class.getDeclaredField("mSocket");
            declaredField.setAccessible(true);
            final LocalSocket localSocket = (LocalSocket)declaredField.get(bluetoothSocket);
            if (localSocket != null) {
                final Field declaredField2 = LocalSocket.class.getDeclaredField("impl");
                declaredField2.setAccessible(true);
                final Object value = declaredField2.get(localSocket);
                if (value != null) {
                    final Field declaredField3 = value.getClass().getDeclaredField("fd");
                    declaredField3.setAccessible(true);
                    final FileDescriptor fileDescriptor = (FileDescriptor)declaredField3.get(value);
                    if (fileDescriptor != null) {
                        final Field declaredField4 = FileDescriptor.class.getDeclaredField("descriptor");
                        declaredField4.setAccessible(true);
                        return (int)declaredField4.get(fileDescriptor);
                    }
                }
            }
        }
        catch (Throwable t) {
            IOUtils.sLogger.e(t);
        }
        return -1;
    }
    
    public static File getTempFile(@Nullable final Logger logger, String s) {
        if (!Environment.getExternalStorageState().equals("mounted")) {
            if (logger != null) {
                logger.e("FileUtils:: External Storage not mounted!");
            }
            else {
                Log.e(IOUtils.TAG, "FileUtils:: External Storage not mounted!");
            }
            final Serializable s2 = null;
            return (File)s2;
        }
        Label_0079: {
            if (!TextUtils.isEmpty((CharSequence)s)) {
                break Label_0079;
            }
            s = new File(Environment.getExternalStorageDirectory(), getTempFilename());
            while (true) {
                Serializable s2 = s;
                try {
                    if (((File)s).createNewFile()) {
                        if (logger != null) {
                            logger.v("Created " + ((File)s).getAbsolutePath());
                            s2 = s;
                        }
                        else {
                            Log.v(IOUtils.TAG, "Created " + ((File)s).getAbsolutePath());
                            s2 = s;
                        }
                    }
                    return (File)s2;
                    s = new File(Environment.getExternalStorageDirectory(), (String)s);
                    continue;
                }
                catch (IOException ex) {
                    if (logger != null) {
                        logger.e("FileUtils:: Unable to create file. ", ex);
                        s2 = s;
                        return (File)s2;
                    }
                    Log.e(IOUtils.TAG, "FileUtils:: Unable to create file. ", (Throwable)ex);
                    s2 = s;
                    return (File)s2;
                }
                return (File)s2;
            }
        }
    }
    
    public static String getTempFilename() {
        return new SimpleDateFormat("yyyy-MM-dd'_'HH:mm:ss.SSS", Locale.US).format(new Date(System.currentTimeMillis())) + ".jpg";
    }
    
    public static String getTrashEntryPathInSameFolder(final File file) {
        String string;
        if (file != null) {
            string = file.getParent() + File.separator + System.currentTimeMillis() + "_" + IOUtils.sCounter.getAndIncrement();
        }
        else {
            string = null;
        }
        return string;
    }
    
    public static String hashForBitmap(final Bitmap bitmap) {
        String hashForBytes = null;
        if (bitmap == null) {
            return hashForBytes;
        }
        try {
            hashForBytes = hashForBytes(bitmap2ByteBuffer(bitmap));
            return hashForBytes;
        }
        catch (NoSuchAlgorithmException ex) {
            hashForBytes = hashForBytes;
            return hashForBytes;
        }
    }
    
    public static String hashForBytes(final byte[] array) throws NoSuchAlgorithmException {
        String bytesToHexString;
        if (array == null || array.length <= 0) {
            bytesToHexString = null;
        }
        else {
            bytesToHexString = null;
            final MessageDigest instance = MessageDigest.getInstance("MD5");
            if (instance != null) {
                instance.update(array);
                bytesToHexString = bytesToHexString(instance.digest());
            }
        }
        return bytesToHexString;
    }
    
    public static String hashForFile(final File file) {
        return hashForFile(file, file.length());
    }
    
    public static String hashForFile(final File p0, final long p1) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: invokevirtual   com/navdy/service/library/log/Logger.recordStartTime:()V
        //     6: aconst_null    
        //     7: astore_3       
        //     8: aconst_null    
        //     9: astore          4
        //    11: aload_0        
        //    12: ifnull          38
        //    15: aload_0        
        //    16: invokevirtual   java/io/File.exists:()Z
        //    19: ifeq            38
        //    22: aload_0        
        //    23: invokevirtual   java/io/File.canRead:()Z
        //    26: ifeq            38
        //    29: lload_1        
        //    30: aload_0        
        //    31: invokevirtual   java/io/File.length:()J
        //    34: lcmp           
        //    35: ifle            46
        //    38: new             Ljava/lang/IllegalArgumentException;
        //    41: dup            
        //    42: invokespecial   java/lang/IllegalArgumentException.<init>:()V
        //    45: athrow         
        //    46: aload_3        
        //    47: astore          5
        //    49: ldc_w           "MD5"
        //    52: invokestatic    java/security/MessageDigest.getInstance:(Ljava/lang/String;)Ljava/security/MessageDigest;
        //    55: astore          6
        //    57: lconst_0       
        //    58: lstore          7
        //    60: aload_3        
        //    61: astore          5
        //    63: ldc             1048576
        //    65: newarray        B
        //    67: astore          9
        //    69: aload_3        
        //    70: astore          5
        //    72: new             Ljava/io/FileInputStream;
        //    75: astore          10
        //    77: aload_3        
        //    78: astore          5
        //    80: aload           10
        //    82: aload_0        
        //    83: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //    86: ldc             1048576
        //    88: i2l            
        //    89: lstore          11
        //    91: aload           10
        //    93: aload           9
        //    95: iconst_0       
        //    96: lload_1        
        //    97: lload           7
        //    99: lsub           
        //   100: lload           11
        //   102: invokestatic    java/lang/Math.min:(JJ)J
        //   105: l2i            
        //   106: invokevirtual   java/io/FileInputStream.read:([BII)I
        //   109: istore          13
        //   111: aload           6
        //   113: aload           9
        //   115: iconst_0       
        //   116: iload           13
        //   118: invokevirtual   java/security/MessageDigest.update:([BII)V
        //   121: lload           7
        //   123: iload           13
        //   125: i2l            
        //   126: ladd           
        //   127: lstore          7
        //   129: lload           7
        //   131: lload_1        
        //   132: lcmp           
        //   133: ifge            142
        //   136: iload           13
        //   138: iconst_m1      
        //   139: if_icmpne       86
        //   142: aload           6
        //   144: invokevirtual   java/security/MessageDigest.digest:()[B
        //   147: invokestatic    com/navdy/service/library/util/IOUtils.bytesToHexString:([B)Ljava/lang/String;
        //   150: astore          5
        //   152: getstatic       com/navdy/service/library/util/IOUtils.sLogger:Lcom/navdy/service/library/log/Logger;
        //   155: astore          4
        //   157: new             Ljava/lang/StringBuilder;
        //   160: astore_3       
        //   161: aload_3        
        //   162: invokespecial   java/lang/StringBuilder.<init>:()V
        //   165: aload           4
        //   167: aload_3        
        //   168: ldc_w           "Time taken to calculate hash of the file of size :"
        //   171: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   174: aload_0        
        //   175: invokevirtual   java/io/File.length:()J
        //   178: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   181: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   184: invokevirtual   com/navdy/service/library/log/Logger.logTimeTaken:(Ljava/lang/String;)V
        //   187: aload           10
        //   189: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   192: aload           5
        //   194: astore_0       
        //   195: aload_0        
        //   196: areturn        
        //   197: astore          10
        //   199: aload           4
        //   201: astore_0       
        //   202: aload_0        
        //   203: astore          5
        //   205: getstatic       com/navdy/service/library/util/IOUtils.sLogger:Lcom/navdy/service/library/log/Logger;
        //   208: ldc_w           "Exception while calculating md5 checksum for the file "
        //   211: aload           10
        //   213: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   216: aload_0        
        //   217: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   220: aconst_null    
        //   221: astore_0       
        //   222: goto            195
        //   225: astore_0       
        //   226: aload           5
        //   228: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   231: aload_0        
        //   232: athrow         
        //   233: astore_0       
        //   234: aload           10
        //   236: astore          5
        //   238: goto            226
        //   241: astore          5
        //   243: aload           10
        //   245: astore_0       
        //   246: aload           5
        //   248: astore          10
        //   250: goto            202
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  49     57     197    202    Ljava/lang/Throwable;
        //  49     57     225    226    Any
        //  63     69     197    202    Ljava/lang/Throwable;
        //  63     69     225    226    Any
        //  72     77     197    202    Ljava/lang/Throwable;
        //  72     77     225    226    Any
        //  80     86     197    202    Ljava/lang/Throwable;
        //  80     86     225    226    Any
        //  91     121    241    253    Ljava/lang/Throwable;
        //  91     121    233    241    Any
        //  142    187    241    253    Ljava/lang/Throwable;
        //  142    187    233    241    Any
        //  205    216    225    226    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0142:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static String hashForKey(String s) {
        if (s == null) {
            return null;
        }
        try {
            s = hashForBytes(s.getBytes());
            return s;
        }
        catch (NoSuchAlgorithmException ex) {
            s = String.valueOf(s.hashCode());
            return s;
        }
        s = null;
        return s;
    }
    
    public static String hashForPath(String s, final boolean b, final String... array) {
        IOUtils.sLogger.recordStartTime();
        try {
            final MessageDigest instance = MessageDigest.getInstance("MD5");
            traverseFiles(s, b, Arrays.<String>asList(array), (OnFileTraversal)new OnFileTraversal() {
                final /* synthetic */ byte[] val$buffer = new byte[1048576];
                
                @Override
                public void onFileTraversal(final File p0) {
                    // 
                    This method could not be decompiled.
                    // 
                    // Original Bytecode:
                    // 
                    //     1: astore_2       
                    //     2: aconst_null    
                    //     3: astore_3       
                    //     4: aload_2        
                    //     5: astore          4
                    //     7: aload_1        
                    //     8: invokevirtual   java/io/File.length:()J
                    //    11: lstore          5
                    //    13: aload_2        
                    //    14: astore          4
                    //    16: new             Ljava/io/FileInputStream;
                    //    19: astore          7
                    //    21: aload_2        
                    //    22: astore          4
                    //    24: aload           7
                    //    26: aload_1        
                    //    27: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
                    //    30: lconst_0       
                    //    31: lstore          8
                    //    33: aload           7
                    //    35: aload_0        
                    //    36: getfield        com/navdy/service/library/util/IOUtils$1.val$buffer:[B
                    //    39: iconst_0       
                    //    40: lload           5
                    //    42: lload           8
                    //    44: lsub           
                    //    45: ldc2_w          1048576
                    //    48: invokestatic    java/lang/Math.min:(JJ)J
                    //    51: l2i            
                    //    52: invokevirtual   java/io/FileInputStream.read:([BII)I
                    //    55: istore          10
                    //    57: aload_0        
                    //    58: getfield        com/navdy/service/library/util/IOUtils$1.val$mDigest:Ljava/security/MessageDigest;
                    //    61: aload_0        
                    //    62: getfield        com/navdy/service/library/util/IOUtils$1.val$buffer:[B
                    //    65: iconst_0       
                    //    66: iload           10
                    //    68: invokevirtual   java/security/MessageDigest.update:([BII)V
                    //    71: lload           8
                    //    73: iload           10
                    //    75: i2l            
                    //    76: ladd           
                    //    77: lstore          8
                    //    79: lload           8
                    //    81: lload           5
                    //    83: lcmp           
                    //    84: ifge            93
                    //    87: iload           10
                    //    89: iconst_m1      
                    //    90: if_icmpne       33
                    //    93: aload           7
                    //    95: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
                    //    98: return         
                    //    99: astore          7
                    //   101: aload_3        
                    //   102: astore_1       
                    //   103: aload_1        
                    //   104: astore          4
                    //   106: invokestatic    com/navdy/service/library/util/IOUtils.access$000:()Lcom/navdy/service/library/log/Logger;
                    //   109: ldc             "Exception while calculating md5 checksum for a path "
                    //   111: aload           7
                    //   113: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
                    //   116: aload_1        
                    //   117: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
                    //   120: goto            98
                    //   123: astore_1       
                    //   124: aload           4
                    //   126: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
                    //   129: aload_1        
                    //   130: athrow         
                    //   131: astore_1       
                    //   132: aload           7
                    //   134: astore          4
                    //   136: goto            124
                    //   139: astore          4
                    //   141: aload           7
                    //   143: astore_1       
                    //   144: aload           4
                    //   146: astore          7
                    //   148: goto            103
                    //    Exceptions:
                    //  Try           Handler
                    //  Start  End    Start  End    Type                 
                    //  -----  -----  -----  -----  ---------------------
                    //  7      13     99     103    Ljava/lang/Throwable;
                    //  7      13     123    124    Any
                    //  16     21     99     103    Ljava/lang/Throwable;
                    //  16     21     123    124    Any
                    //  24     30     99     103    Ljava/lang/Throwable;
                    //  24     30     123    124    Any
                    //  33     71     139    151    Ljava/lang/Throwable;
                    //  33     71     131    139    Any
                    //  106    116    123    124    Any
                    // 
                    // The error that occurred was:
                    // 
                    // java.lang.IllegalStateException: Expression is linked from several locations: Label_0033:
                    //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
                    //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
                    //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
                    //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1163)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:1010)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:554)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:392)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:333)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:494)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:333)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:294)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
                    //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                    //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                    //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
                    //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
                    //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
                    //     at java.lang.Thread.run(Unknown Source)
                    // 
                    throw new IllegalStateException("An error occurred while decompiling this method.");
                }
            });
            final String bytesToHexString = bytesToHexString(instance.digest());
            IOUtils.sLogger.logTimeTaken("Time taken to calculate hash of all files on path:" + s);
            s = bytesToHexString;
            return s;
        }
        catch (Throwable t) {
            IOUtils.sLogger.e("Exception while calculating md5 checksum for a path ", t);
            s = null;
            return s;
        }
    }
    
    private static boolean isAppsExternalFile(final Context context, final String s) {
        return s.startsWith(context.getExternalFilesDir((String)null).getAbsolutePath());
    }
    
    private static boolean isAppsInternalFile(final Context context, final String s) {
        return s.startsWith(context.getFilesDir().getAbsolutePath());
    }
    
    public static boolean isExternalStorageReadable() {
        return "mounted".equals(Environment.getExternalStorageState());
    }
    
    public static byte[] readBinaryFile(final String s) throws IOException {
        final File file = new File(s);
        final byte[] array = new byte[(int)file.length()];
        final FileInputStream fileInputStream = new FileInputStream(file);
        try {
            fileInputStream.read(array);
            return array;
        }
        finally {
            fileInputStream.close();
        }
    }
    
    public static byte[] readInputStreamToByteArray(final InputStream inputStream) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final byte[] array = new byte[16384];
        while (true) {
            final int read = inputStream.read(array);
            if (read == -1) {
                break;
            }
            byteArrayOutputStream.write(array, 0, read);
        }
        return byteArrayOutputStream.toByteArray();
    }
    
    private static void traverseFiles(final String s, final boolean b, final List<String> list, final OnFileTraversal onFileTraversal) throws FileNotFoundException {
        final File file = new File(s);
        if (file.isDirectory()) {
            final File[] listFiles = file.listFiles();
            if (listFiles != null) {
                Arrays.sort(listFiles);
                for (final File file2 : listFiles) {
                    if (!file2.isDirectory() && !list.contains(file2.getName())) {
                        onFileTraversal.onFileTraversal(file2);
                    }
                    else if (b) {
                        traverseFiles(file2.getPath(), true, list, onFileTraversal);
                    }
                }
            }
        }
    }
    
    private interface OnFileTraversal
    {
        void onFileTraversal(final File p0);
    }
}
