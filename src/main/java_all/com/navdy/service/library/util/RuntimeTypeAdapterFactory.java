package com.navdy.service.library.util;

import java.util.Iterator;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.internal.Streams;
import com.google.gson.stream.JsonReader;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.Gson;
import java.util.LinkedHashMap;
import java.util.Map;
import com.google.gson.TypeAdapterFactory;

public final class RuntimeTypeAdapterFactory<T> implements TypeAdapterFactory
{
    private final Map<String, Class<?>> aliasToSubtype;
    private final Class<?> baseType;
    private final Map<String, Class<?>> labelToSubtype;
    private final Map<Class<?>, String> subtypeToLabel;
    private final String typeFieldName;
    
    private RuntimeTypeAdapterFactory(final Class<?> baseType, final String typeFieldName) {
        this.labelToSubtype = new LinkedHashMap<String, Class<?>>();
        this.aliasToSubtype = new LinkedHashMap<String, Class<?>>();
        this.subtypeToLabel = new LinkedHashMap<Class<?>, String>();
        if (typeFieldName == null || baseType == null) {
            throw new NullPointerException();
        }
        this.baseType = baseType;
        this.typeFieldName = typeFieldName;
    }
    
    public static <T> RuntimeTypeAdapterFactory<T> of(final Class<T> clazz) {
        return new RuntimeTypeAdapterFactory<T>(clazz, "type");
    }
    
    public static <T> RuntimeTypeAdapterFactory<T> of(final Class<T> clazz, final String s) {
        return new RuntimeTypeAdapterFactory<T>(clazz, s);
    }
    
    @Override
    public <R> TypeAdapter<R> create(final Gson gson, final TypeToken<R> typeToken) {
        TypeAdapter<R> typeAdapter;
        if (typeToken.getRawType() != this.baseType) {
            typeAdapter = null;
        }
        else {
            final LinkedHashMap<Object, TypeAdapter<Object>> linkedHashMap = new LinkedHashMap<Object, TypeAdapter<Object>>();
            final LinkedHashMap<Class<T>, TypeAdapter<Object>> linkedHashMap2 = new LinkedHashMap<Class<T>, TypeAdapter<Object>>();
            for (final Map.Entry<String, Class<?>> entry : this.labelToSubtype.entrySet()) {
                final TypeAdapter<Object> delegateAdapter = gson.<Object>getDelegateAdapter(this, (TypeToken<Object>)TypeToken.<T>get((Class<T>)entry.getValue()));
                linkedHashMap.put(entry.getKey(), delegateAdapter);
                linkedHashMap2.put(entry.getValue(), delegateAdapter);
            }
            for (final Map.Entry<String, Class<?>> entry2 : this.aliasToSubtype.entrySet()) {
                linkedHashMap.put(entry2.getKey(), gson.<Object>getDelegateAdapter(this, (TypeToken<Object>)TypeToken.<T>get((Class<T>)entry2.getValue())));
            }
            typeAdapter = new TypeAdapter<R>() {
                @Override
                public R read(final JsonReader jsonReader) throws IOException {
                    final JsonElement parse = Streams.parse(jsonReader);
                    final JsonElement remove = parse.getAsJsonObject().remove(RuntimeTypeAdapterFactory.this.typeFieldName);
                    if (remove == null) {
                        throw new JsonParseException("cannot deserialize " + RuntimeTypeAdapterFactory.this.baseType + " because it does not define a field named " + RuntimeTypeAdapterFactory.this.typeFieldName);
                    }
                    final String asString = remove.getAsString();
                    final TypeAdapter<R> typeAdapter = linkedHashMap.get(asString);
                    if (typeAdapter == null) {
                        throw new JsonParseException("cannot deserialize " + RuntimeTypeAdapterFactory.this.baseType + " subtype named " + asString + "; did you forget to register a subtype?");
                    }
                    return typeAdapter.fromJsonTree(parse);
                }
                
                @Override
                public void write(final JsonWriter jsonWriter, final R r) throws IOException {
                    final Class<?> class1 = r.getClass();
                    final String s = RuntimeTypeAdapterFactory.this.subtypeToLabel.get(class1);
                    final TypeAdapter<R> typeAdapter = linkedHashMap2.get(class1);
                    if (typeAdapter == null) {
                        throw new JsonParseException("cannot serialize " + class1.getName() + "; did you forget to register a subtype?");
                    }
                    final JsonObject asJsonObject = typeAdapter.toJsonTree(r).getAsJsonObject();
                    if (asJsonObject.has(RuntimeTypeAdapterFactory.this.typeFieldName)) {
                        throw new JsonParseException("cannot serialize " + class1.getName() + " because it already defines a field named " + RuntimeTypeAdapterFactory.this.typeFieldName);
                    }
                    final JsonObject jsonObject = new JsonObject();
                    jsonObject.add(RuntimeTypeAdapterFactory.this.typeFieldName, new JsonPrimitive(s));
                    for (final Map.Entry<String, JsonElement> entry : asJsonObject.entrySet()) {
                        jsonObject.add(entry.getKey(), entry.getValue());
                    }
                    Streams.write(jsonObject, jsonWriter);
                }
            };
        }
        return typeAdapter;
    }
    
    public RuntimeTypeAdapterFactory<T> registerSubtype(final Class<? extends T> clazz) {
        return this.registerSubtype(clazz, clazz.getSimpleName());
    }
    
    public RuntimeTypeAdapterFactory<T> registerSubtype(final Class<? extends T> clazz, final String s) {
        if (clazz == null || s == null) {
            throw new NullPointerException();
        }
        if (this.subtypeToLabel.containsKey(clazz) || this.labelToSubtype.containsKey(s)) {
            throw new IllegalArgumentException("types and labels must be unique");
        }
        if (this.aliasToSubtype.containsKey(s)) {
            throw new IllegalArgumentException("alias already exists for label");
        }
        this.labelToSubtype.put(s, clazz);
        this.subtypeToLabel.put(clazz, s);
        return this;
    }
    
    public RuntimeTypeAdapterFactory<T> registerSubtypeAlias(final Class<? extends T> clazz, final String s) {
        if (clazz == null || s == null) {
            throw new NullPointerException();
        }
        if (this.labelToSubtype.containsKey(s) || this.aliasToSubtype.containsKey(s)) {
            throw new IllegalArgumentException("labels must be unique");
        }
        if (!this.subtypeToLabel.containsKey(clazz)) {
            throw new IllegalArgumentException("subtype must already have a mapping to add an alias");
        }
        this.aliasToSubtype.put(s, clazz);
        return this;
    }
}
