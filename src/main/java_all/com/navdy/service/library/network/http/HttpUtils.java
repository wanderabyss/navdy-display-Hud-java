package com.navdy.service.library.network.http;

import okhttp3.RequestBody;
import okhttp3.MediaType;

public class HttpUtils
{
    public static final MediaType JSON;
    public static final String MIME_TYPE_IMAGE_PNG = "image/png";
    public static final String MIME_TYPE_TEXT = "text/plain";
    public static final String MIME_TYPE_ZIP = "application/zip";
    
    static {
        JSON = MediaType.parse("application/json; charset=utf-8");
    }
    
    public static RequestBody getJsonRequestBody(final String s) {
        return RequestBody.create(HttpUtils.JSON, s);
    }
}
