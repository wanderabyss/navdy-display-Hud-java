package com.navdy.service.library.network.http.services;

import java.util.Iterator;
import okhttp3.RequestBody;
import okhttp3.MediaType;
import java.io.File;
import okhttp3.MultipartBody;
import java.util.List;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.network.http.HttpUtils;
import org.json.JSONObject;
import org.json.JSONArray;
import okhttp3.Response;
import java.io.IOException;
import okhttp3.Call;
import okhttp3.Callback;
import android.text.TextUtils;
import okhttp3.Request;
import okhttp3.OkHttpClient;
import com.navdy.service.library.log.Logger;

public class JiraClient
{
    public static final String ASSIGNEE = "assignee";
    public static final String DESCRIPTION = "description";
    public static final String EMAIL_ADDRESS = "emailAddress";
    public static final String ENVIRONMENT = "environment";
    public static final String FIELDS = "fields";
    public static final String ISSUE_TYPE = "issuetype";
    public static final String ISSUE_TYPE_BUG = "Bug";
    public static final String JIRA_API_SEARCH_ASSIGNABLE_USER = "https://navdyhud.atlassian.net/rest/api/2/user/assignable/search";
    private static final String JIRA_ATTACHMENTS = "/attachments/";
    private static final String JIRA_ISSUE_API_URL = "https://navdyhud.atlassian.net/rest/api/2/issue/";
    private static final String JIRA_ISSUE_EDIT_URL = "https://navdyhud.atlassian.net/rest/api/2/issue/";
    public static final String KEY = "key";
    public static final String NAME = "name";
    public static final String PROJECT = "project";
    public static final String SUMMARY = "summary";
    private static final Logger sLogger;
    private String encodedCredentials;
    private OkHttpClient mClient;
    
    static {
        sLogger = new Logger(JiraClient.class);
    }
    
    public JiraClient(final OkHttpClient mClient) {
        this.mClient = mClient;
    }
    
    public void assignTicketForName(final String s, String string, final String s2, final ResultCallback resultCallback) {
        string = "https://navdyhud.atlassian.net/rest/api/2/user/assignable/search?issueKey=" + s + "&username=" + string;
        final Request.Builder value = new Request.Builder().url(string).get();
        if (!TextUtils.isEmpty((CharSequence)this.encodedCredentials)) {
            value.addHeader("Authorization", "Basic " + this.encodedCredentials);
        }
        this.mClient.newCall(value.build()).enqueue(new Callback() {
            @Override
            public void onFailure(final Call call, final IOException ex) {
                JiraClient.sLogger.e("onFailure ", ex);
                if (resultCallback != null) {
                    resultCallback.onError(ex);
                }
            }
            
            @Override
            public void onResponse(final Call call, final Response response) throws IOException {
                try {
                    final int code = response.code();
                    if (code == 200) {
                        final JSONArray jsonArray = new JSONArray(response.body().string());
                        if (jsonArray.length() > 0) {
                            String string = "";
                            int n = 0;
                            String string2;
                            while (true) {
                                string2 = string;
                                if (n >= jsonArray.length()) {
                                    break;
                                }
                                final JSONObject jsonObject = (JSONObject)jsonArray.get(n);
                                if (n == 0) {
                                    string = jsonObject.getString("name");
                                }
                                if (jsonObject.has("emailAddress")) {
                                    final String string3 = jsonObject.getString("emailAddress");
                                    JiraClient.sLogger.d("Email Id " + string3);
                                    if (TextUtils.equals((CharSequence)string3, (CharSequence)s2) && jsonObject.has("name")) {
                                        string2 = jsonObject.getString("name");
                                        JiraClient.sLogger.d("User name " + string2);
                                        break;
                                    }
                                }
                                ++n;
                            }
                            JiraClient.sLogger.d("Assigning the ticket to User " + string2);
                            JiraClient.this.assignTicketToUser(s, string2, resultCallback);
                        }
                    }
                    else if (resultCallback != null) {
                        resultCallback.onError(new IOException("Jira request failed with " + code));
                    }
                }
                catch (Throwable t) {
                    if (resultCallback != null) {
                        resultCallback.onError(t);
                    }
                }
            }
        });
    }
    
    public void assignTicketToUser(final String s, final String s2, final ResultCallback resultCallback) {
        try {
            final JSONObject jsonObject = new JSONObject();
            final JSONObject jsonObject2 = new JSONObject();
            final JSONObject jsonObject3 = new JSONObject();
            jsonObject3.put("name", s2);
            jsonObject2.put("assignee", jsonObject3);
            jsonObject.put("fields", jsonObject2);
            final Request.Builder put = new Request.Builder().url("https://navdyhud.atlassian.net/rest/api/2/issue//" + s).put(HttpUtils.getJsonRequestBody(jsonObject.toString()));
            if (!TextUtils.isEmpty((CharSequence)this.encodedCredentials)) {
                put.addHeader("Authorization", "Basic " + this.encodedCredentials);
            }
            this.mClient.newCall(put.build()).enqueue(new Callback() {
                @Override
                public void onFailure(final Call call, final IOException ex) {
                    JiraClient.sLogger.e("onFailure ", ex);
                    if (resultCallback != null) {
                        resultCallback.onError(ex);
                    }
                }
                
                @Override
                public void onResponse(final Call call, final Response response) throws IOException {
                    try {
                        final int code = response.code();
                        if (code == 200 || code == 204) {
                            if (resultCallback != null) {
                                resultCallback.onSuccess(s2);
                            }
                        }
                        else {
                            JiraClient.sLogger.d("Response data " + response.body().string());
                            if (resultCallback != null) {
                                resultCallback.onError(new IOException("Jira request failed with " + code));
                            }
                        }
                    }
                    catch (Throwable t) {
                        JiraClient.sLogger.e("Exception paring response ", t);
                        if (resultCallback != null) {
                            resultCallback.onError(t);
                        }
                    }
                }
            });
        }
        catch (Throwable t) {
            resultCallback.onError(t);
            IOUtils.closeStream(null);
        }
        finally {
            IOUtils.closeStream(null);
        }
    }
    
    public void attachFilesToTicket(final String s, final List<Attachment> list, final ResultCallback resultCallback) {
        final MultipartBody.Builder setType = new MultipartBody.Builder().setType(MultipartBody.FORM);
        for (final Attachment attachment : list) {
            final File file = new File(attachment.filePath);
            if (!file.exists()) {
                throw new IllegalArgumentException("Attachment " + file.getName() + "File does not exists");
            }
            if (!file.canRead()) {
                throw new IllegalArgumentException("Attachment " + file.getName() + "File cannot be read");
            }
            setType.addFormDataPart("file", file.getName(), RequestBody.create(MediaType.parse(attachment.mimeType), file));
        }
        final Request.Builder post = new Request.Builder().url("https://navdyhud.atlassian.net/rest/api/2/issue/" + s + "/attachments/").addHeader("X-Atlassian-Token", "nocheck").post(setType.build());
        if (!TextUtils.isEmpty((CharSequence)this.encodedCredentials)) {
            post.addHeader("Authorization", "Basic " + this.encodedCredentials);
        }
        this.mClient.newCall(post.build()).enqueue(new Callback() {
            @Override
            public void onFailure(final Call call, final IOException ex) {
                if (resultCallback != null) {
                    resultCallback.onError(ex);
                }
            }
            
            @Override
            public void onResponse(final Call call, final Response response) throws IOException {
                try {
                    final int code = response.code();
                    if ((code == 200 || code == 201) && resultCallback != null) {
                        resultCallback.onSuccess(s);
                    }
                }
                catch (Throwable t) {
                    if (resultCallback != null) {
                        resultCallback.onError(t);
                    }
                }
            }
        });
    }
    
    public void setEncodedCredentials(final String encodedCredentials) {
        this.encodedCredentials = encodedCredentials;
    }
    
    public void submitTicket(final String s, final String s2, final String s3, final String s4, final ResultCallback resultCallback) throws IOException {
        this.submitTicket(s, s2, s3, s4, null, resultCallback);
    }
    
    public void submitTicket(final String s, final String s2, final String s3, final String s4, final String s5, final ResultCallback resultCallback) throws IOException {
        try {
            final JSONObject jsonObject = new JSONObject();
            final JSONObject jsonObject2 = new JSONObject();
            final JSONObject jsonObject3 = new JSONObject();
            final JSONObject jsonObject4 = new JSONObject();
            jsonObject3.put("key", s);
            jsonObject2.put("project", jsonObject3);
            jsonObject4.put("name", s2);
            jsonObject2.put("issuetype", jsonObject4);
            jsonObject2.put("summary", s3);
            if (!TextUtils.isEmpty((CharSequence)s5)) {
                jsonObject2.put("environment", s5);
            }
            jsonObject2.put("description", s4);
            jsonObject.put("fields", jsonObject2);
            final Request.Builder post = new Request.Builder().url("https://navdyhud.atlassian.net/rest/api/2/issue/").post(HttpUtils.getJsonRequestBody(jsonObject.toString()));
            if (!TextUtils.isEmpty((CharSequence)this.encodedCredentials)) {
                post.addHeader("Authorization", "Basic " + this.encodedCredentials);
            }
            this.mClient.newCall(post.build()).enqueue(new Callback() {
                @Override
                public void onFailure(final Call call, final IOException ex) {
                    if (resultCallback != null) {
                        resultCallback.onError(ex);
                    }
                }
                
                @Override
                public void onResponse(final Call call, final Response response) throws IOException {
                    try {
                        final int code = response.code();
                        if (code == 200 || code == 201) {
                            final String string = new JSONObject(response.body().string()).getString("key");
                            if (resultCallback != null) {
                                resultCallback.onSuccess(string);
                            }
                        }
                        else if (resultCallback != null) {
                            resultCallback.onError(new IOException("Jira request failed with " + code));
                        }
                    }
                    catch (Throwable t) {
                        if (resultCallback != null) {
                            resultCallback.onError(t);
                        }
                    }
                }
            });
        }
        catch (Throwable t) {
            resultCallback.onError(t);
            IOUtils.closeStream(null);
        }
        finally {
            IOUtils.closeStream(null);
        }
    }
    
    public static class Attachment
    {
        public String filePath;
        public String mimeType;
        
        public Attachment() {
            this.filePath = "";
            this.mimeType = "";
        }
    }
    
    public interface ResultCallback
    {
        void onError(final Throwable p0);
        
        void onSuccess(final Object p0);
    }
}
