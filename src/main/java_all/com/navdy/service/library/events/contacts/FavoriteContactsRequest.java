package com.navdy.service.library.events.contacts;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class FavoriteContactsRequest extends Message
{
    public static final Integer DEFAULT_MAXCONTACTS;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.INT32)
    public final Integer maxContacts;
    
    static {
        DEFAULT_MAXCONTACTS = 0;
    }
    
    private FavoriteContactsRequest(final Builder builder) {
        this(builder.maxContacts);
        this.setBuilder((Message.Builder)builder);
    }
    
    public FavoriteContactsRequest(final Integer maxContacts) {
        this.maxContacts = maxContacts;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof FavoriteContactsRequest && this.equals(this.maxContacts, ((FavoriteContactsRequest)o).maxContacts));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.maxContacts != null) {
                hashCode = this.maxContacts.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<FavoriteContactsRequest>
    {
        public Integer maxContacts;
        
        public Builder() {
        }
        
        public Builder(final FavoriteContactsRequest favoriteContactsRequest) {
            super(favoriteContactsRequest);
            if (favoriteContactsRequest != null) {
                this.maxContacts = favoriteContactsRequest.maxContacts;
            }
        }
        
        public FavoriteContactsRequest build() {
            return new FavoriteContactsRequest(this, null);
        }
        
        public Builder maxContacts(final Integer maxContacts) {
            this.maxContacts = maxContacts;
            return this;
        }
    }
}
