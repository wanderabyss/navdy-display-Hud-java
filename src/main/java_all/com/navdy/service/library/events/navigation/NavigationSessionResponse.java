package com.navdy.service.library.events.navigation;

import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;

public final class NavigationSessionResponse extends Message
{
    public static final NavigationSessionState DEFAULT_PENDINGSESSIONSTATE;
    public static final String DEFAULT_ROUTEID = "";
    public static final RequestStatus DEFAULT_STATUS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final NavigationSessionState pendingSessionState;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String routeId;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String statusDetail;
    
    static {
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
        DEFAULT_PENDINGSESSIONSTATE = NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
    }
    
    public NavigationSessionResponse(final RequestStatus status, final String statusDetail, final NavigationSessionState pendingSessionState, final String routeId) {
        this.status = status;
        this.statusDetail = statusDetail;
        this.pendingSessionState = pendingSessionState;
        this.routeId = routeId;
    }
    
    private NavigationSessionResponse(final Builder builder) {
        this(builder.status, builder.statusDetail, builder.pendingSessionState, builder.routeId);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NavigationSessionResponse)) {
                b = false;
            }
            else {
                final NavigationSessionResponse navigationSessionResponse = (NavigationSessionResponse)o;
                if (!this.equals(this.status, navigationSessionResponse.status) || !this.equals(this.statusDetail, navigationSessionResponse.statusDetail) || !this.equals(this.pendingSessionState, navigationSessionResponse.pendingSessionState) || !this.equals(this.routeId, navigationSessionResponse.routeId)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.status != null) {
                hashCode3 = this.status.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.statusDetail != null) {
                hashCode4 = this.statusDetail.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.pendingSessionState != null) {
                hashCode5 = this.pendingSessionState.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            if (this.routeId != null) {
                hashCode = this.routeId.hashCode();
            }
            hashCode2 = ((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<NavigationSessionResponse>
    {
        public NavigationSessionState pendingSessionState;
        public String routeId;
        public RequestStatus status;
        public String statusDetail;
        
        public Builder() {
        }
        
        public Builder(final NavigationSessionResponse navigationSessionResponse) {
            super(navigationSessionResponse);
            if (navigationSessionResponse != null) {
                this.status = navigationSessionResponse.status;
                this.statusDetail = navigationSessionResponse.statusDetail;
                this.pendingSessionState = navigationSessionResponse.pendingSessionState;
                this.routeId = navigationSessionResponse.routeId;
            }
        }
        
        public NavigationSessionResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new NavigationSessionResponse(this, null);
        }
        
        public Builder pendingSessionState(final NavigationSessionState pendingSessionState) {
            this.pendingSessionState = pendingSessionState;
            return this;
        }
        
        public Builder routeId(final String routeId) {
            this.routeId = routeId;
            return this;
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
        
        public Builder statusDetail(final String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }
    }
}
