package com.navdy.service.library.events.connection;

import com.squareup.wire.Message;

public final class NetworkLinkReady extends Message
{
    private static final long serialVersionUID = 0L;
    
    public NetworkLinkReady() {
    }
    
    private NetworkLinkReady(final Builder builder) {
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof NetworkLinkReady;
    }
    
    @Override
    public int hashCode() {
        return 0;
    }
    
    public static final class Builder extends Message.Builder<NetworkLinkReady>
    {
        public Builder() {
        }
        
        public Builder(final NetworkLinkReady networkLinkReady) {
            super(networkLinkReady);
        }
        
        public NetworkLinkReady build() {
            return new NetworkLinkReady(this, null);
        }
    }
}
