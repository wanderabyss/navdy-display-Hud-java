package com.navdy.service.library.events.callcontrol;

import com.squareup.wire.Message;

public final class PhoneStatusRequest extends Message
{
    private static final long serialVersionUID = 0L;
    
    public PhoneStatusRequest() {
    }
    
    private PhoneStatusRequest(final Builder builder) {
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof PhoneStatusRequest;
    }
    
    @Override
    public int hashCode() {
        return 0;
    }
    
    public static final class Builder extends Message.Builder<PhoneStatusRequest>
    {
        public Builder() {
        }
        
        public Builder(final PhoneStatusRequest phoneStatusRequest) {
            super(phoneStatusRequest);
        }
        
        public PhoneStatusRequest build() {
            return new PhoneStatusRequest(this, null);
        }
    }
}
