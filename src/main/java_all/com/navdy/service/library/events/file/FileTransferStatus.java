package com.navdy.service.library.events.file;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class FileTransferStatus extends Message
{
    public static final Integer DEFAULT_CHUNKINDEX;
    public static final FileTransferError DEFAULT_ERROR;
    public static final Boolean DEFAULT_SUCCESS;
    public static final Long DEFAULT_TOTALBYTESTRANSFERRED;
    public static final Boolean DEFAULT_TRANSFERCOMPLETE;
    public static final Integer DEFAULT_TRANSFERID;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 5, type = Datatype.INT32)
    public final Integer chunkIndex;
    @ProtoField(tag = 4, type = Datatype.ENUM)
    public final FileTransferError error;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.BOOL)
    public final Boolean success;
    @ProtoField(tag = 3, type = Datatype.INT64)
    public final Long totalBytesTransferred;
    @ProtoField(label = Label.REQUIRED, tag = 6, type = Datatype.BOOL)
    public final Boolean transferComplete;
    @ProtoField(tag = 1, type = Datatype.INT32)
    public final Integer transferId;
    
    static {
        DEFAULT_TRANSFERID = 0;
        DEFAULT_SUCCESS = false;
        DEFAULT_TOTALBYTESTRANSFERRED = 0L;
        DEFAULT_ERROR = FileTransferError.FILE_TRANSFER_NO_ERROR;
        DEFAULT_CHUNKINDEX = 0;
        DEFAULT_TRANSFERCOMPLETE = false;
    }
    
    private FileTransferStatus(final Builder builder) {
        this(builder.transferId, builder.success, builder.totalBytesTransferred, builder.error, builder.chunkIndex, builder.transferComplete);
        this.setBuilder((Message.Builder)builder);
    }
    
    public FileTransferStatus(final Integer transferId, final Boolean success, final Long totalBytesTransferred, final FileTransferError error, final Integer chunkIndex, final Boolean transferComplete) {
        this.transferId = transferId;
        this.success = success;
        this.totalBytesTransferred = totalBytesTransferred;
        this.error = error;
        this.chunkIndex = chunkIndex;
        this.transferComplete = transferComplete;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof FileTransferStatus)) {
                b = false;
            }
            else {
                final FileTransferStatus fileTransferStatus = (FileTransferStatus)o;
                if (!this.equals(this.transferId, fileTransferStatus.transferId) || !this.equals(this.success, fileTransferStatus.success) || !this.equals(this.totalBytesTransferred, fileTransferStatus.totalBytesTransferred) || !this.equals(this.error, fileTransferStatus.error) || !this.equals(this.chunkIndex, fileTransferStatus.chunkIndex) || !this.equals(this.transferComplete, fileTransferStatus.transferComplete)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.transferId != null) {
                hashCode3 = this.transferId.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.success != null) {
                hashCode4 = this.success.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.totalBytesTransferred != null) {
                hashCode5 = this.totalBytesTransferred.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.error != null) {
                hashCode6 = this.error.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.chunkIndex != null) {
                hashCode7 = this.chunkIndex.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            if (this.transferComplete != null) {
                hashCode = this.transferComplete.hashCode();
            }
            hashCode2 = ((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<FileTransferStatus>
    {
        public Integer chunkIndex;
        public FileTransferError error;
        public Boolean success;
        public Long totalBytesTransferred;
        public Boolean transferComplete;
        public Integer transferId;
        
        public Builder() {
        }
        
        public Builder(final FileTransferStatus fileTransferStatus) {
            super(fileTransferStatus);
            if (fileTransferStatus != null) {
                this.transferId = fileTransferStatus.transferId;
                this.success = fileTransferStatus.success;
                this.totalBytesTransferred = fileTransferStatus.totalBytesTransferred;
                this.error = fileTransferStatus.error;
                this.chunkIndex = fileTransferStatus.chunkIndex;
                this.transferComplete = fileTransferStatus.transferComplete;
            }
        }
        
        public FileTransferStatus build() {
            ((Message.Builder)this).checkRequiredFields();
            return new FileTransferStatus(this, null);
        }
        
        public Builder chunkIndex(final Integer chunkIndex) {
            this.chunkIndex = chunkIndex;
            return this;
        }
        
        public Builder error(final FileTransferError error) {
            this.error = error;
            return this;
        }
        
        public Builder success(final Boolean success) {
            this.success = success;
            return this;
        }
        
        public Builder totalBytesTransferred(final Long totalBytesTransferred) {
            this.totalBytesTransferred = totalBytesTransferred;
            return this;
        }
        
        public Builder transferComplete(final Boolean transferComplete) {
            this.transferComplete = transferComplete;
            return this;
        }
        
        public Builder transferId(final Integer transferId) {
            this.transferId = transferId;
            return this;
        }
    }
}
