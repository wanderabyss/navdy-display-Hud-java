package com.navdy.service.library.events.notification;

import com.squareup.wire.ProtoEnum;

public enum NotificationCategory implements ProtoEnum
{
    CATEGORY_BUSINESS_AND_FINANCE(9), 
    CATEGORY_EMAIL(6), 
    CATEGORY_ENTERTAINMENT(11), 
    CATEGORY_HEALTH_AND_FITNESS(8), 
    CATEGORY_INCOMING_CALL(1), 
    CATEGORY_LOCATION(10), 
    CATEGORY_MISSED_CALL(2), 
    CATEGORY_NEWS(7), 
    CATEGORY_OTHER(0), 
    CATEGORY_SCHEDULE(5), 
    CATEGORY_SOCIAL(4), 
    CATEGORY_VOICE_MAIL(3);
    
    private final int value;
    
    private NotificationCategory(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
