package com.navdy.service.library.events.places;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class PlaceTypeSearchRequest extends Message
{
    public static final PlaceType DEFAULT_PLACE_TYPE;
    public static final String DEFAULT_REQUEST_ID = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final PlaceType place_type;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String request_id;
    
    static {
        DEFAULT_PLACE_TYPE = PlaceType.PLACE_TYPE_UNKNOWN;
    }
    
    private PlaceTypeSearchRequest(final Builder builder) {
        this(builder.request_id, builder.place_type);
        this.setBuilder((Message.Builder)builder);
    }
    
    public PlaceTypeSearchRequest(final String request_id, final PlaceType place_type) {
        this.request_id = request_id;
        this.place_type = place_type;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof PlaceTypeSearchRequest)) {
                b = false;
            }
            else {
                final PlaceTypeSearchRequest placeTypeSearchRequest = (PlaceTypeSearchRequest)o;
                if (!this.equals(this.request_id, placeTypeSearchRequest.request_id) || !this.equals(this.place_type, placeTypeSearchRequest.place_type)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.request_id != null) {
                hashCode3 = this.request_id.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.place_type != null) {
                hashCode = this.place_type.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<PlaceTypeSearchRequest>
    {
        public PlaceType place_type;
        public String request_id;
        
        public Builder() {
        }
        
        public Builder(final PlaceTypeSearchRequest placeTypeSearchRequest) {
            super(placeTypeSearchRequest);
            if (placeTypeSearchRequest != null) {
                this.request_id = placeTypeSearchRequest.request_id;
                this.place_type = placeTypeSearchRequest.place_type;
            }
        }
        
        public PlaceTypeSearchRequest build() {
            return new PlaceTypeSearchRequest(this, null);
        }
        
        public Builder place_type(final PlaceType place_type) {
            this.place_type = place_type;
            return this;
        }
        
        public Builder request_id(final String request_id) {
            this.request_id = request_id;
            return this;
        }
    }
}
