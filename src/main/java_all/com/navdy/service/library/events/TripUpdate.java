package com.navdy.service.library.events;

import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.location.LatLong;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class TripUpdate extends Message
{
    public static final String DEFAULT_ARRIVED_AT_DESTINATION_ID = "";
    public static final Float DEFAULT_BEARING;
    public static final String DEFAULT_CHOSEN_DESTINATION_ID = "";
    public static final Integer DEFAULT_DISTANCE_TO_DESTINATION;
    public static final Integer DEFAULT_DISTANCE_TRAVELED;
    public static final Double DEFAULT_ELEVATION;
    public static final Float DEFAULT_ELEVATION_ACCURACY;
    public static final Integer DEFAULT_ESTIMATED_TIME_REMAINING;
    public static final Double DEFAULT_EXCESSIVE_SPEEDING_RATIO;
    public static final Float DEFAULT_GPS_SPEED;
    public static final Integer DEFAULT_HARD_ACCELERATION_COUNT;
    public static final Integer DEFAULT_HARD_BREAKING_COUNT;
    public static final Integer DEFAULT_HIGH_G_COUNT;
    public static final Float DEFAULT_HORIZONTAL_ACCURACY;
    public static final Integer DEFAULT_METERS_TRAVELED_SINCE_BOOT;
    public static final Integer DEFAULT_OBD_SPEED;
    public static final String DEFAULT_ROAD_ELEMENT = "";
    public static final Integer DEFAULT_SEQUENCE_NUMBER;
    public static final Double DEFAULT_SPEEDING_RATIO;
    public static final Long DEFAULT_TIMESTAMP;
    public static final Long DEFAULT_TRIP_NUMBER;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 12, type = Datatype.STRING)
    public final String arrived_at_destination_id;
    @ProtoField(tag = 7, type = Datatype.FLOAT)
    public final Float bearing;
    @ProtoField(tag = 11, type = Datatype.STRING)
    public final String chosen_destination_id;
    @ProtoField(label = Label.REQUIRED, tag = 5)
    public final LatLong current_position;
    @ProtoField(tag = 14, type = Datatype.INT32)
    public final Integer distance_to_destination;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.INT32)
    public final Integer distance_traveled;
    @ProtoField(tag = 6, type = Datatype.DOUBLE)
    public final Double elevation;
    @ProtoField(tag = 22, type = Datatype.FLOAT)
    public final Float elevation_accuracy;
    @ProtoField(tag = 13, type = Datatype.INT32)
    public final Integer estimated_time_remaining;
    @ProtoField(tag = 19, type = Datatype.DOUBLE)
    public final Double excessive_speeding_ratio;
    @ProtoField(tag = 8, type = Datatype.FLOAT)
    public final Float gps_speed;
    @ProtoField(tag = 17, type = Datatype.INT32)
    public final Integer hard_acceleration_count;
    @ProtoField(tag = 16, type = Datatype.INT32)
    public final Integer hard_breaking_count;
    @ProtoField(tag = 15, type = Datatype.INT32)
    public final Integer high_g_count;
    @ProtoField(tag = 21, type = Datatype.FLOAT)
    public final Float horizontal_accuracy;
    @ProtoField(tag = 23)
    public final Coordinate last_raw_coordinate;
    @ProtoField(tag = 20, type = Datatype.INT32)
    public final Integer meters_traveled_since_boot;
    @ProtoField(tag = 9, type = Datatype.INT32)
    public final Integer obd_speed;
    @ProtoField(tag = 10, type = Datatype.STRING)
    public final String road_element;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.INT32)
    public final Integer sequence_number;
    @ProtoField(tag = 18, type = Datatype.DOUBLE)
    public final Double speeding_ratio;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.INT64)
    public final Long timestamp;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT64)
    public final Long trip_number;
    
    static {
        DEFAULT_TRIP_NUMBER = 0L;
        DEFAULT_SEQUENCE_NUMBER = 0;
        DEFAULT_TIMESTAMP = 0L;
        DEFAULT_DISTANCE_TRAVELED = 0;
        DEFAULT_ELEVATION = 0.0;
        DEFAULT_BEARING = 0.0f;
        DEFAULT_GPS_SPEED = 0.0f;
        DEFAULT_OBD_SPEED = 0;
        DEFAULT_ESTIMATED_TIME_REMAINING = 0;
        DEFAULT_DISTANCE_TO_DESTINATION = 0;
        DEFAULT_HIGH_G_COUNT = 0;
        DEFAULT_HARD_BREAKING_COUNT = 0;
        DEFAULT_HARD_ACCELERATION_COUNT = 0;
        DEFAULT_SPEEDING_RATIO = 0.0;
        DEFAULT_EXCESSIVE_SPEEDING_RATIO = 0.0;
        DEFAULT_METERS_TRAVELED_SINCE_BOOT = 0;
        DEFAULT_HORIZONTAL_ACCURACY = 0.0f;
        DEFAULT_ELEVATION_ACCURACY = 0.0f;
    }
    
    private TripUpdate(final Builder builder) {
        this(builder.trip_number, builder.sequence_number, builder.timestamp, builder.distance_traveled, builder.current_position, builder.elevation, builder.bearing, builder.gps_speed, builder.obd_speed, builder.road_element, builder.chosen_destination_id, builder.arrived_at_destination_id, builder.estimated_time_remaining, builder.distance_to_destination, builder.high_g_count, builder.hard_breaking_count, builder.hard_acceleration_count, builder.speeding_ratio, builder.excessive_speeding_ratio, builder.meters_traveled_since_boot, builder.horizontal_accuracy, builder.elevation_accuracy, builder.last_raw_coordinate);
        this.setBuilder((Message.Builder)builder);
    }
    
    public TripUpdate(final Long trip_number, final Integer sequence_number, final Long timestamp, final Integer distance_traveled, final LatLong current_position, final Double elevation, final Float bearing, final Float gps_speed, final Integer obd_speed, final String road_element, final String chosen_destination_id, final String arrived_at_destination_id, final Integer estimated_time_remaining, final Integer distance_to_destination, final Integer high_g_count, final Integer hard_breaking_count, final Integer hard_acceleration_count, final Double speeding_ratio, final Double excessive_speeding_ratio, final Integer meters_traveled_since_boot, final Float horizontal_accuracy, final Float elevation_accuracy, final Coordinate last_raw_coordinate) {
        this.trip_number = trip_number;
        this.sequence_number = sequence_number;
        this.timestamp = timestamp;
        this.distance_traveled = distance_traveled;
        this.current_position = current_position;
        this.elevation = elevation;
        this.bearing = bearing;
        this.gps_speed = gps_speed;
        this.obd_speed = obd_speed;
        this.road_element = road_element;
        this.chosen_destination_id = chosen_destination_id;
        this.arrived_at_destination_id = arrived_at_destination_id;
        this.estimated_time_remaining = estimated_time_remaining;
        this.distance_to_destination = distance_to_destination;
        this.high_g_count = high_g_count;
        this.hard_breaking_count = hard_breaking_count;
        this.hard_acceleration_count = hard_acceleration_count;
        this.speeding_ratio = speeding_ratio;
        this.excessive_speeding_ratio = excessive_speeding_ratio;
        this.meters_traveled_since_boot = meters_traveled_since_boot;
        this.horizontal_accuracy = horizontal_accuracy;
        this.elevation_accuracy = elevation_accuracy;
        this.last_raw_coordinate = last_raw_coordinate;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof TripUpdate)) {
                b = false;
            }
            else {
                final TripUpdate tripUpdate = (TripUpdate)o;
                if (!this.equals(this.trip_number, tripUpdate.trip_number) || !this.equals(this.sequence_number, tripUpdate.sequence_number) || !this.equals(this.timestamp, tripUpdate.timestamp) || !this.equals(this.distance_traveled, tripUpdate.distance_traveled) || !this.equals(this.current_position, tripUpdate.current_position) || !this.equals(this.elevation, tripUpdate.elevation) || !this.equals(this.bearing, tripUpdate.bearing) || !this.equals(this.gps_speed, tripUpdate.gps_speed) || !this.equals(this.obd_speed, tripUpdate.obd_speed) || !this.equals(this.road_element, tripUpdate.road_element) || !this.equals(this.chosen_destination_id, tripUpdate.chosen_destination_id) || !this.equals(this.arrived_at_destination_id, tripUpdate.arrived_at_destination_id) || !this.equals(this.estimated_time_remaining, tripUpdate.estimated_time_remaining) || !this.equals(this.distance_to_destination, tripUpdate.distance_to_destination) || !this.equals(this.high_g_count, tripUpdate.high_g_count) || !this.equals(this.hard_breaking_count, tripUpdate.hard_breaking_count) || !this.equals(this.hard_acceleration_count, tripUpdate.hard_acceleration_count) || !this.equals(this.speeding_ratio, tripUpdate.speeding_ratio) || !this.equals(this.excessive_speeding_ratio, tripUpdate.excessive_speeding_ratio) || !this.equals(this.meters_traveled_since_boot, tripUpdate.meters_traveled_since_boot) || !this.equals(this.horizontal_accuracy, tripUpdate.horizontal_accuracy) || !this.equals(this.elevation_accuracy, tripUpdate.elevation_accuracy) || !this.equals(this.last_raw_coordinate, tripUpdate.last_raw_coordinate)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.trip_number != null) {
                hashCode3 = this.trip_number.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.sequence_number != null) {
                hashCode4 = this.sequence_number.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.timestamp != null) {
                hashCode5 = this.timestamp.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.distance_traveled != null) {
                hashCode6 = this.distance_traveled.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.current_position != null) {
                hashCode7 = this.current_position.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.elevation != null) {
                hashCode8 = this.elevation.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            int hashCode9;
            if (this.bearing != null) {
                hashCode9 = this.bearing.hashCode();
            }
            else {
                hashCode9 = 0;
            }
            int hashCode10;
            if (this.gps_speed != null) {
                hashCode10 = this.gps_speed.hashCode();
            }
            else {
                hashCode10 = 0;
            }
            int hashCode11;
            if (this.obd_speed != null) {
                hashCode11 = this.obd_speed.hashCode();
            }
            else {
                hashCode11 = 0;
            }
            int hashCode12;
            if (this.road_element != null) {
                hashCode12 = this.road_element.hashCode();
            }
            else {
                hashCode12 = 0;
            }
            int hashCode13;
            if (this.chosen_destination_id != null) {
                hashCode13 = this.chosen_destination_id.hashCode();
            }
            else {
                hashCode13 = 0;
            }
            int hashCode14;
            if (this.arrived_at_destination_id != null) {
                hashCode14 = this.arrived_at_destination_id.hashCode();
            }
            else {
                hashCode14 = 0;
            }
            int hashCode15;
            if (this.estimated_time_remaining != null) {
                hashCode15 = this.estimated_time_remaining.hashCode();
            }
            else {
                hashCode15 = 0;
            }
            int hashCode16;
            if (this.distance_to_destination != null) {
                hashCode16 = this.distance_to_destination.hashCode();
            }
            else {
                hashCode16 = 0;
            }
            int hashCode17;
            if (this.high_g_count != null) {
                hashCode17 = this.high_g_count.hashCode();
            }
            else {
                hashCode17 = 0;
            }
            int hashCode18;
            if (this.hard_breaking_count != null) {
                hashCode18 = this.hard_breaking_count.hashCode();
            }
            else {
                hashCode18 = 0;
            }
            int hashCode19;
            if (this.hard_acceleration_count != null) {
                hashCode19 = this.hard_acceleration_count.hashCode();
            }
            else {
                hashCode19 = 0;
            }
            int hashCode20;
            if (this.speeding_ratio != null) {
                hashCode20 = this.speeding_ratio.hashCode();
            }
            else {
                hashCode20 = 0;
            }
            int hashCode21;
            if (this.excessive_speeding_ratio != null) {
                hashCode21 = this.excessive_speeding_ratio.hashCode();
            }
            else {
                hashCode21 = 0;
            }
            int hashCode22;
            if (this.meters_traveled_since_boot != null) {
                hashCode22 = this.meters_traveled_since_boot.hashCode();
            }
            else {
                hashCode22 = 0;
            }
            int hashCode23;
            if (this.horizontal_accuracy != null) {
                hashCode23 = this.horizontal_accuracy.hashCode();
            }
            else {
                hashCode23 = 0;
            }
            int hashCode24;
            if (this.elevation_accuracy != null) {
                hashCode24 = this.elevation_accuracy.hashCode();
            }
            else {
                hashCode24 = 0;
            }
            if (this.last_raw_coordinate != null) {
                hashCode = this.last_raw_coordinate.hashCode();
            }
            hashCode2 = (((((((((((((((((((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode9) * 37 + hashCode10) * 37 + hashCode11) * 37 + hashCode12) * 37 + hashCode13) * 37 + hashCode14) * 37 + hashCode15) * 37 + hashCode16) * 37 + hashCode17) * 37 + hashCode18) * 37 + hashCode19) * 37 + hashCode20) * 37 + hashCode21) * 37 + hashCode22) * 37 + hashCode23) * 37 + hashCode24) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<TripUpdate>
    {
        public String arrived_at_destination_id;
        public Float bearing;
        public String chosen_destination_id;
        public LatLong current_position;
        public Integer distance_to_destination;
        public Integer distance_traveled;
        public Double elevation;
        public Float elevation_accuracy;
        public Integer estimated_time_remaining;
        public Double excessive_speeding_ratio;
        public Float gps_speed;
        public Integer hard_acceleration_count;
        public Integer hard_breaking_count;
        public Integer high_g_count;
        public Float horizontal_accuracy;
        public Coordinate last_raw_coordinate;
        public Integer meters_traveled_since_boot;
        public Integer obd_speed;
        public String road_element;
        public Integer sequence_number;
        public Double speeding_ratio;
        public Long timestamp;
        public Long trip_number;
        
        public Builder() {
        }
        
        public Builder(final TripUpdate tripUpdate) {
            super(tripUpdate);
            if (tripUpdate != null) {
                this.trip_number = tripUpdate.trip_number;
                this.sequence_number = tripUpdate.sequence_number;
                this.timestamp = tripUpdate.timestamp;
                this.distance_traveled = tripUpdate.distance_traveled;
                this.current_position = tripUpdate.current_position;
                this.elevation = tripUpdate.elevation;
                this.bearing = tripUpdate.bearing;
                this.gps_speed = tripUpdate.gps_speed;
                this.obd_speed = tripUpdate.obd_speed;
                this.road_element = tripUpdate.road_element;
                this.chosen_destination_id = tripUpdate.chosen_destination_id;
                this.arrived_at_destination_id = tripUpdate.arrived_at_destination_id;
                this.estimated_time_remaining = tripUpdate.estimated_time_remaining;
                this.distance_to_destination = tripUpdate.distance_to_destination;
                this.high_g_count = tripUpdate.high_g_count;
                this.hard_breaking_count = tripUpdate.hard_breaking_count;
                this.hard_acceleration_count = tripUpdate.hard_acceleration_count;
                this.speeding_ratio = tripUpdate.speeding_ratio;
                this.excessive_speeding_ratio = tripUpdate.excessive_speeding_ratio;
                this.meters_traveled_since_boot = tripUpdate.meters_traveled_since_boot;
                this.horizontal_accuracy = tripUpdate.horizontal_accuracy;
                this.elevation_accuracy = tripUpdate.elevation_accuracy;
                this.last_raw_coordinate = tripUpdate.last_raw_coordinate;
            }
        }
        
        public Builder arrived_at_destination_id(final String arrived_at_destination_id) {
            this.arrived_at_destination_id = arrived_at_destination_id;
            return this;
        }
        
        public Builder bearing(final Float bearing) {
            this.bearing = bearing;
            return this;
        }
        
        public TripUpdate build() {
            ((Message.Builder)this).checkRequiredFields();
            return new TripUpdate(this, null);
        }
        
        public Builder chosen_destination_id(final String chosen_destination_id) {
            this.chosen_destination_id = chosen_destination_id;
            return this;
        }
        
        public Builder current_position(final LatLong current_position) {
            this.current_position = current_position;
            return this;
        }
        
        public Builder distance_to_destination(final Integer distance_to_destination) {
            this.distance_to_destination = distance_to_destination;
            return this;
        }
        
        public Builder distance_traveled(final Integer distance_traveled) {
            this.distance_traveled = distance_traveled;
            return this;
        }
        
        public Builder elevation(final Double elevation) {
            this.elevation = elevation;
            return this;
        }
        
        public Builder elevation_accuracy(final Float elevation_accuracy) {
            this.elevation_accuracy = elevation_accuracy;
            return this;
        }
        
        public Builder estimated_time_remaining(final Integer estimated_time_remaining) {
            this.estimated_time_remaining = estimated_time_remaining;
            return this;
        }
        
        public Builder excessive_speeding_ratio(final Double excessive_speeding_ratio) {
            this.excessive_speeding_ratio = excessive_speeding_ratio;
            return this;
        }
        
        public Builder gps_speed(final Float gps_speed) {
            this.gps_speed = gps_speed;
            return this;
        }
        
        public Builder hard_acceleration_count(final Integer hard_acceleration_count) {
            this.hard_acceleration_count = hard_acceleration_count;
            return this;
        }
        
        public Builder hard_breaking_count(final Integer hard_breaking_count) {
            this.hard_breaking_count = hard_breaking_count;
            return this;
        }
        
        public Builder high_g_count(final Integer high_g_count) {
            this.high_g_count = high_g_count;
            return this;
        }
        
        public Builder horizontal_accuracy(final Float horizontal_accuracy) {
            this.horizontal_accuracy = horizontal_accuracy;
            return this;
        }
        
        public Builder last_raw_coordinate(final Coordinate last_raw_coordinate) {
            this.last_raw_coordinate = last_raw_coordinate;
            return this;
        }
        
        public Builder meters_traveled_since_boot(final Integer meters_traveled_since_boot) {
            this.meters_traveled_since_boot = meters_traveled_since_boot;
            return this;
        }
        
        public Builder obd_speed(final Integer obd_speed) {
            this.obd_speed = obd_speed;
            return this;
        }
        
        public Builder road_element(final String road_element) {
            this.road_element = road_element;
            return this;
        }
        
        public Builder sequence_number(final Integer sequence_number) {
            this.sequence_number = sequence_number;
            return this;
        }
        
        public Builder speeding_ratio(final Double speeding_ratio) {
            this.speeding_ratio = speeding_ratio;
            return this;
        }
        
        public Builder timestamp(final Long timestamp) {
            this.timestamp = timestamp;
            return this;
        }
        
        public Builder trip_number(final Long trip_number) {
            this.trip_number = trip_number;
            return this;
        }
    }
}
