package com.navdy.service.library.events.photo;

import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import okio.ByteString;
import com.squareup.wire.Message;

public final class PhotoResponse extends Message
{
    public static final String DEFAULT_IDENTIFIER = "";
    public static final ByteString DEFAULT_PHOTO;
    public static final String DEFAULT_PHOTOCHECKSUM = "";
    public static final PhotoType DEFAULT_PHOTOTYPE;
    public static final RequestStatus DEFAULT_STATUS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.STRING)
    public final String identifier;
    @ProtoField(tag = 1, type = Datatype.BYTES)
    public final ByteString photo;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String photoChecksum;
    @ProtoField(tag = 6, type = Datatype.ENUM)
    public final PhotoType photoType;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String statusDetail;
    
    static {
        DEFAULT_PHOTO = ByteString.EMPTY;
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
        DEFAULT_PHOTOTYPE = PhotoType.PHOTO_CONTACT;
    }
    
    private PhotoResponse(final Builder builder) {
        this(builder.photo, builder.status, builder.statusDetail, builder.identifier, builder.photoChecksum, builder.photoType);
        this.setBuilder((Message.Builder)builder);
    }
    
    public PhotoResponse(final ByteString photo, final RequestStatus status, final String statusDetail, final String identifier, final String photoChecksum, final PhotoType photoType) {
        this.photo = photo;
        this.status = status;
        this.statusDetail = statusDetail;
        this.identifier = identifier;
        this.photoChecksum = photoChecksum;
        this.photoType = photoType;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof PhotoResponse)) {
                b = false;
            }
            else {
                final PhotoResponse photoResponse = (PhotoResponse)o;
                if (!this.equals(this.photo, photoResponse.photo) || !this.equals(this.status, photoResponse.status) || !this.equals(this.statusDetail, photoResponse.statusDetail) || !this.equals(this.identifier, photoResponse.identifier) || !this.equals(this.photoChecksum, photoResponse.photoChecksum) || !this.equals(this.photoType, photoResponse.photoType)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.photo != null) {
                hashCode3 = this.photo.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.status != null) {
                hashCode4 = this.status.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.statusDetail != null) {
                hashCode5 = this.statusDetail.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.identifier != null) {
                hashCode6 = this.identifier.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.photoChecksum != null) {
                hashCode7 = this.photoChecksum.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            if (this.photoType != null) {
                hashCode = this.photoType.hashCode();
            }
            hashCode2 = ((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<PhotoResponse>
    {
        public String identifier;
        public ByteString photo;
        public String photoChecksum;
        public PhotoType photoType;
        public RequestStatus status;
        public String statusDetail;
        
        public Builder() {
        }
        
        public Builder(final PhotoResponse photoResponse) {
            super(photoResponse);
            if (photoResponse != null) {
                this.photo = photoResponse.photo;
                this.status = photoResponse.status;
                this.statusDetail = photoResponse.statusDetail;
                this.identifier = photoResponse.identifier;
                this.photoChecksum = photoResponse.photoChecksum;
                this.photoType = photoResponse.photoType;
            }
        }
        
        public PhotoResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new PhotoResponse(this, null);
        }
        
        public Builder identifier(final String identifier) {
            this.identifier = identifier;
            return this;
        }
        
        public Builder photo(final ByteString photo) {
            this.photo = photo;
            return this;
        }
        
        public Builder photoChecksum(final String photoChecksum) {
            this.photoChecksum = photoChecksum;
            return this;
        }
        
        public Builder photoType(final PhotoType photoType) {
            this.photoType = photoType;
            return this;
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
        
        public Builder statusDetail(final String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }
    }
}
