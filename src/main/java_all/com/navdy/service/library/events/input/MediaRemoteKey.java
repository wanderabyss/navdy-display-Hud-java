package com.navdy.service.library.events.input;

import com.squareup.wire.ProtoEnum;

public enum MediaRemoteKey implements ProtoEnum
{
    MEDIA_REMOTE_KEY_NEXT(3), 
    MEDIA_REMOTE_KEY_PLAY(2), 
    MEDIA_REMOTE_KEY_PREV(4), 
    MEDIA_REMOTE_KEY_SIRI(1);
    
    private final int value;
    
    private MediaRemoteKey(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
