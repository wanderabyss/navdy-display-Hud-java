package com.navdy.service.library.events;

import java.io.IOException;
import java.io.OutputStream;
import com.navdy.service.library.log.Logger;

public class NavdyEventWriter
{
    private static final Logger sLogger;
    protected OutputStream mOutputStream;
    
    static {
        sLogger = new Logger(NavdyEventWriter.class);
    }
    
    public NavdyEventWriter(final OutputStream mOutputStream) {
        if (mOutputStream == null) {
            throw new IllegalArgumentException();
        }
        this.mOutputStream = mOutputStream;
    }
    
    public void write(final byte[] array) throws IOException {
        this.mOutputStream.write(new Frame(Integer.valueOf(array.length)).toByteArray());
        this.mOutputStream.write(array);
        this.mOutputStream.flush();
        if (NavdyEventWriter.sLogger.isLoggable(2)) {
            NavdyEventWriter.sLogger.v("[Outgoing-Event] size:" + array.length);
        }
    }
}
