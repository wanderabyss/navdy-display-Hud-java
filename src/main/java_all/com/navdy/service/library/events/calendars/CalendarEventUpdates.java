package com.navdy.service.library.events.calendars;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import java.util.List;
import com.squareup.wire.Message;

public final class CalendarEventUpdates extends Message
{
    public static final List<CalendarEvent> DEFAULT_CALENDAR_EVENTS;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REPEATED, messageType = CalendarEvent.class, tag = 1)
    public final List<CalendarEvent> calendar_events;
    
    static {
        DEFAULT_CALENDAR_EVENTS = Collections.<CalendarEvent>emptyList();
    }
    
    private CalendarEventUpdates(final Builder builder) {
        this(builder.calendar_events);
        this.setBuilder((Message.Builder)builder);
    }
    
    public CalendarEventUpdates(final List<CalendarEvent> list) {
        this.calendar_events = Message.<CalendarEvent>immutableCopyOf(list);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof CalendarEventUpdates && this.equals(this.calendar_events, ((CalendarEventUpdates)o).calendar_events));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.calendar_events != null) {
                hashCode = this.calendar_events.hashCode();
            }
            else {
                hashCode = 1;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<CalendarEventUpdates>
    {
        public List<CalendarEvent> calendar_events;
        
        public Builder() {
        }
        
        public Builder(final CalendarEventUpdates calendarEventUpdates) {
            super(calendarEventUpdates);
            if (calendarEventUpdates != null) {
                this.calendar_events = (List<CalendarEvent>)Message.<Object>copyOf((List<Object>)calendarEventUpdates.calendar_events);
            }
        }
        
        public CalendarEventUpdates build() {
            return new CalendarEventUpdates(this, null);
        }
        
        public Builder calendar_events(final List<CalendarEvent> list) {
            this.calendar_events = Message.Builder.<CalendarEvent>checkForNulls(list);
            return this;
        }
    }
}
