package com.navdy.ancs;

import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import android.os.RemoteException;
import android.os.IInterface;

public interface IAncsServiceListener extends IInterface
{
    void onConnectionStateChange(final int p0) throws RemoteException;
    
    void onNotification(final AppleNotification p0) throws RemoteException;
    
    public abstract static class Stub extends Binder implements IAncsServiceListener
    {
        private static final String DESCRIPTOR = "com.navdy.ancs.IAncsServiceListener";
        static final int TRANSACTION_onConnectionStateChange = 1;
        static final int TRANSACTION_onNotification = 2;
        
        public Stub() {
            this.attachInterface((IInterface)this, "com.navdy.ancs.IAncsServiceListener");
        }
        
        public static IAncsServiceListener asInterface(final IBinder binder) {
            IAncsServiceListener ancsServiceListener;
            if (binder == null) {
                ancsServiceListener = null;
            }
            else {
                final IInterface queryLocalInterface = binder.queryLocalInterface("com.navdy.ancs.IAncsServiceListener");
                if (queryLocalInterface != null && queryLocalInterface instanceof IAncsServiceListener) {
                    ancsServiceListener = (IAncsServiceListener)queryLocalInterface;
                }
                else {
                    ancsServiceListener = new Proxy(binder);
                }
            }
            return ancsServiceListener;
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            boolean onTransact = true;
            switch (n) {
                default:
                    onTransact = super.onTransact(n, parcel, parcel2, n2);
                    break;
                case 1598968902:
                    parcel2.writeString("com.navdy.ancs.IAncsServiceListener");
                    break;
                case 1:
                    parcel.enforceInterface("com.navdy.ancs.IAncsServiceListener");
                    this.onConnectionStateChange(parcel.readInt());
                    parcel2.writeNoException();
                    break;
                case 2: {
                    parcel.enforceInterface("com.navdy.ancs.IAncsServiceListener");
                    AppleNotification appleNotification;
                    if (parcel.readInt() != 0) {
                        appleNotification = (AppleNotification)AppleNotification.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        appleNotification = null;
                    }
                    this.onNotification(appleNotification);
                    parcel2.writeNoException();
                    break;
                }
            }
            return onTransact;
        }
        
        private static class Proxy implements IAncsServiceListener
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            public String getInterfaceDescriptor() {
                return "com.navdy.ancs.IAncsServiceListener";
            }
            
            @Override
            public void onConnectionStateChange(final int n) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.ancs.IAncsServiceListener");
                    obtain.writeInt(n);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void onNotification(final AppleNotification appleNotification) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.ancs.IAncsServiceListener");
                    if (appleNotification != null) {
                        obtain.writeInt(1);
                        appleNotification.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
