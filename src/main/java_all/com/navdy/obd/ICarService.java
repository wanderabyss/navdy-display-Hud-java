package com.navdy.obd;

import android.os.Parcelable.Creator;
import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import android.os.RemoteException;
import java.util.List;
import android.os.IInterface;

public interface ICarService extends IInterface
{
    void addListener(final List<Pid> p0, final IPidListener p1) throws RemoteException;
    
    boolean applyConfiguration(final String p0) throws RemoteException;
    
    double getBatteryVoltage() throws RemoteException;
    
    int getConnectionState() throws RemoteException;
    
    String getCurrentConfigurationName() throws RemoteException;
    
    List<ECU> getEcus() throws RemoteException;
    
    int getMode() throws RemoteException;
    
    String getObdChipFirmwareVersion() throws RemoteException;
    
    String getProtocol() throws RemoteException;
    
    List<Pid> getReadings(final List<Pid> p0) throws RemoteException;
    
    List<Pid> getSupportedPids() throws RemoteException;
    
    List<String> getTroubleCodes() throws RemoteException;
    
    String getVIN() throws RemoteException;
    
    boolean isCheckEngineLightOn() throws RemoteException;
    
    boolean isObdPidsScanningEnabled() throws RemoteException;
    
    void removeListener(final IPidListener p0) throws RemoteException;
    
    void rescan() throws RemoteException;
    
    void setCANBusMonitoringListener(final ICanBusMonitoringListener p0) throws RemoteException;
    
    void setMode(final int p0, final boolean p1) throws RemoteException;
    
    void setObdPidsScanningEnabled(final boolean p0) throws RemoteException;
    
    void setVoltageSettings(final VoltageSettings p0) throws RemoteException;
    
    void sleep(final boolean p0) throws RemoteException;
    
    void startCanBusMonitoring() throws RemoteException;
    
    void stopCanBusMonitoring() throws RemoteException;
    
    void updateFirmware(final String p0, final String p1) throws RemoteException;
    
    void updateScan(final ScanSchedule p0, final IPidListener p1) throws RemoteException;
    
    void wakeup() throws RemoteException;
    
    public abstract static class Stub extends Binder implements ICarService
    {
        private static final String DESCRIPTOR = "com.navdy.obd.ICarService";
        static final int TRANSACTION_addListener = 6;
        static final int TRANSACTION_applyConfiguration = 10;
        static final int TRANSACTION_getBatteryVoltage = 13;
        static final int TRANSACTION_getConnectionState = 1;
        static final int TRANSACTION_getCurrentConfigurationName = 9;
        static final int TRANSACTION_getEcus = 3;
        static final int TRANSACTION_getMode = 21;
        static final int TRANSACTION_getObdChipFirmwareVersion = 19;
        static final int TRANSACTION_getProtocol = 4;
        static final int TRANSACTION_getReadings = 5;
        static final int TRANSACTION_getSupportedPids = 2;
        static final int TRANSACTION_getTroubleCodes = 27;
        static final int TRANSACTION_getVIN = 8;
        static final int TRANSACTION_isCheckEngineLightOn = 26;
        static final int TRANSACTION_isObdPidsScanningEnabled = 15;
        static final int TRANSACTION_removeListener = 7;
        static final int TRANSACTION_rescan = 12;
        static final int TRANSACTION_setCANBusMonitoringListener = 23;
        static final int TRANSACTION_setMode = 22;
        static final int TRANSACTION_setObdPidsScanningEnabled = 14;
        static final int TRANSACTION_setVoltageSettings = 18;
        static final int TRANSACTION_sleep = 16;
        static final int TRANSACTION_startCanBusMonitoring = 24;
        static final int TRANSACTION_stopCanBusMonitoring = 25;
        static final int TRANSACTION_updateFirmware = 20;
        static final int TRANSACTION_updateScan = 11;
        static final int TRANSACTION_wakeup = 17;
        
        public Stub() {
            this.attachInterface((IInterface)this, "com.navdy.obd.ICarService");
        }
        
        public static ICarService asInterface(final IBinder binder) {
            ICarService carService;
            if (binder == null) {
                carService = null;
            }
            else {
                final IInterface queryLocalInterface = binder.queryLocalInterface("com.navdy.obd.ICarService");
                if (queryLocalInterface != null && queryLocalInterface instanceof ICarService) {
                    carService = (ICarService)queryLocalInterface;
                }
                else {
                    carService = new Proxy(binder);
                }
            }
            return carService;
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            final int n3 = 0;
            final int n4 = 0;
            final int n5 = 0;
            final boolean b = true;
            boolean onTransact = false;
            switch (n) {
                default:
                    onTransact = super.onTransact(n, parcel, parcel2, n2);
                    break;
                case 1598968902:
                    parcel2.writeString("com.navdy.obd.ICarService");
                    onTransact = b;
                    break;
                case 1:
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    n = this.getConnectionState();
                    parcel2.writeNoException();
                    parcel2.writeInt(n);
                    onTransact = b;
                    break;
                case 2: {
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    final List<Pid> supportedPids = this.getSupportedPids();
                    parcel2.writeNoException();
                    parcel2.writeTypedList((List)supportedPids);
                    onTransact = b;
                    break;
                }
                case 3: {
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    final List<ECU> ecus = this.getEcus();
                    parcel2.writeNoException();
                    parcel2.writeTypedList((List)ecus);
                    onTransact = b;
                    break;
                }
                case 4: {
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    final String protocol = this.getProtocol();
                    parcel2.writeNoException();
                    parcel2.writeString(protocol);
                    onTransact = b;
                    break;
                }
                case 5: {
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    final List<Pid> readings = this.getReadings(parcel.createTypedArrayList((Parcelable.Creator)Pid.CREATOR));
                    parcel2.writeNoException();
                    parcel2.writeTypedList((List)readings);
                    onTransact = b;
                    break;
                }
                case 6:
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    this.addListener(parcel.createTypedArrayList((Parcelable.Creator)Pid.CREATOR), IPidListener.Stub.asInterface(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    onTransact = b;
                    break;
                case 7:
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    this.removeListener(IPidListener.Stub.asInterface(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    onTransact = b;
                    break;
                case 8: {
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    final String vin = this.getVIN();
                    parcel2.writeNoException();
                    parcel2.writeString(vin);
                    onTransact = b;
                    break;
                }
                case 9: {
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    final String currentConfigurationName = this.getCurrentConfigurationName();
                    parcel2.writeNoException();
                    parcel2.writeString(currentConfigurationName);
                    onTransact = b;
                    break;
                }
                case 10: {
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    final boolean applyConfiguration = this.applyConfiguration(parcel.readString());
                    parcel2.writeNoException();
                    n = n5;
                    if (applyConfiguration) {
                        n = 1;
                    }
                    parcel2.writeInt(n);
                    onTransact = b;
                    break;
                }
                case 11: {
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    ScanSchedule scanSchedule;
                    if (parcel.readInt() != 0) {
                        scanSchedule = (ScanSchedule)ScanSchedule.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        scanSchedule = null;
                    }
                    this.updateScan(scanSchedule, IPidListener.Stub.asInterface(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    onTransact = b;
                    break;
                }
                case 12:
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    this.rescan();
                    parcel2.writeNoException();
                    onTransact = b;
                    break;
                case 13: {
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    final double batteryVoltage = this.getBatteryVoltage();
                    parcel2.writeNoException();
                    parcel2.writeDouble(batteryVoltage);
                    onTransact = b;
                    break;
                }
                case 14:
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    this.setObdPidsScanningEnabled(parcel.readInt() != 0);
                    parcel2.writeNoException();
                    onTransact = b;
                    break;
                case 15: {
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    final boolean obdPidsScanningEnabled = this.isObdPidsScanningEnabled();
                    parcel2.writeNoException();
                    n = n3;
                    if (obdPidsScanningEnabled) {
                        n = 1;
                    }
                    parcel2.writeInt(n);
                    onTransact = b;
                    break;
                }
                case 16:
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    this.sleep(parcel.readInt() != 0);
                    parcel2.writeNoException();
                    onTransact = b;
                    break;
                case 17:
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    this.wakeup();
                    parcel2.writeNoException();
                    onTransact = b;
                    break;
                case 18: {
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    VoltageSettings voltageSettings;
                    if (parcel.readInt() != 0) {
                        voltageSettings = (VoltageSettings)VoltageSettings.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        voltageSettings = null;
                    }
                    this.setVoltageSettings(voltageSettings);
                    parcel2.writeNoException();
                    onTransact = b;
                    break;
                }
                case 19: {
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    final String obdChipFirmwareVersion = this.getObdChipFirmwareVersion();
                    parcel2.writeNoException();
                    parcel2.writeString(obdChipFirmwareVersion);
                    onTransact = b;
                    break;
                }
                case 20:
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    this.updateFirmware(parcel.readString(), parcel.readString());
                    parcel2.writeNoException();
                    onTransact = b;
                    break;
                case 21:
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    n = this.getMode();
                    parcel2.writeNoException();
                    parcel2.writeInt(n);
                    onTransact = b;
                    break;
                case 22:
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    n = parcel.readInt();
                    this.setMode(n, parcel.readInt() != 0);
                    parcel2.writeNoException();
                    onTransact = b;
                    break;
                case 23:
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    this.setCANBusMonitoringListener(ICanBusMonitoringListener.Stub.asInterface(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    onTransact = b;
                    break;
                case 24:
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    this.startCanBusMonitoring();
                    parcel2.writeNoException();
                    onTransact = b;
                    break;
                case 25:
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    this.stopCanBusMonitoring();
                    parcel2.writeNoException();
                    onTransact = b;
                    break;
                case 26: {
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    final boolean checkEngineLightOn = this.isCheckEngineLightOn();
                    parcel2.writeNoException();
                    n = n4;
                    if (checkEngineLightOn) {
                        n = 1;
                    }
                    parcel2.writeInt(n);
                    onTransact = b;
                    break;
                }
                case 27: {
                    parcel.enforceInterface("com.navdy.obd.ICarService");
                    final List<String> troubleCodes = this.getTroubleCodes();
                    parcel2.writeNoException();
                    parcel2.writeStringList((List)troubleCodes);
                    onTransact = b;
                    break;
                }
            }
            return onTransact;
        }
        
        private static class Proxy implements ICarService
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            @Override
            public void addListener(final List<Pid> list, final IPidListener pidListener) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    obtain.writeTypedList((List)list);
                    IBinder binder;
                    if (pidListener != null) {
                        binder = pidListener.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean applyConfiguration(final String s) throws RemoteException {
                boolean b = false;
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    obtain.writeString(s);
                    this.mRemote.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            @Override
            public double getBatteryVoltage() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    this.mRemote.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readDouble();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public int getConnectionState() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public String getCurrentConfigurationName() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    this.mRemote.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public List<ECU> getEcus() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                    return (List<ECU>)obtain2.createTypedArrayList((Parcelable.Creator)ECU.CREATOR);
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            public String getInterfaceDescriptor() {
                return "com.navdy.obd.ICarService";
            }
            
            @Override
            public int getMode() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    this.mRemote.transact(21, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public String getObdChipFirmwareVersion() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    this.mRemote.transact(19, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public String getProtocol() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public List<Pid> getReadings(final List<Pid> list) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    obtain.writeTypedList((List)list);
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    return (List<Pid>)obtain2.createTypedArrayList((Parcelable.Creator)Pid.CREATOR);
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public List<Pid> getSupportedPids() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    return (List<Pid>)obtain2.createTypedArrayList((Parcelable.Creator)Pid.CREATOR);
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public List<String> getTroubleCodes() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    this.mRemote.transact(27, obtain, obtain2, 0);
                    obtain2.readException();
                    return (List<String>)obtain2.createStringArrayList();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public String getVIN() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    this.mRemote.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean isCheckEngineLightOn() throws RemoteException {
                boolean b = false;
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    this.mRemote.transact(26, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean isObdPidsScanningEnabled() throws RemoteException {
                boolean b = false;
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    this.mRemote.transact(15, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void removeListener(final IPidListener pidListener) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    IBinder binder;
                    if (pidListener != null) {
                        binder = pidListener.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void rescan() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    this.mRemote.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void setCANBusMonitoringListener(final ICanBusMonitoringListener canBusMonitoringListener) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    IBinder binder;
                    if (canBusMonitoringListener != null) {
                        binder = canBusMonitoringListener.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(23, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void setMode(int n, final boolean b) throws RemoteException {
                final int n2 = 0;
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    obtain.writeInt(n);
                    n = n2;
                    if (b) {
                        n = 1;
                    }
                    obtain.writeInt(n);
                    this.mRemote.transact(22, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void setObdPidsScanningEnabled(final boolean b) throws RemoteException {
                int n = 0;
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    if (b) {
                        n = 1;
                    }
                    obtain.writeInt(n);
                    this.mRemote.transact(14, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void setVoltageSettings(final VoltageSettings voltageSettings) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    if (voltageSettings != null) {
                        obtain.writeInt(1);
                        voltageSettings.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(18, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void sleep(final boolean b) throws RemoteException {
                int n = 0;
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    if (b) {
                        n = 1;
                    }
                    obtain.writeInt(n);
                    this.mRemote.transact(16, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void startCanBusMonitoring() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    this.mRemote.transact(24, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void stopCanBusMonitoring() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    this.mRemote.transact(25, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void updateFirmware(final String s, final String s2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    obtain.writeString(s);
                    obtain.writeString(s2);
                    this.mRemote.transact(20, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void updateScan(final ScanSchedule scanSchedule, final IPidListener pidListener) throws RemoteException {
                while (true) {
                    final Parcel obtain = Parcel.obtain();
                    final Parcel obtain2 = Parcel.obtain();
                    while (true) {
                        try {
                            obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                            if (scanSchedule != null) {
                                obtain.writeInt(1);
                                scanSchedule.writeToParcel(obtain, 0);
                            }
                            else {
                                obtain.writeInt(0);
                            }
                            if (pidListener != null) {
                                final IBinder binder = pidListener.asBinder();
                                obtain.writeStrongBinder(binder);
                                this.mRemote.transact(11, obtain, obtain2, 0);
                                obtain2.readException();
                                return;
                            }
                        }
                        finally {
                            obtain2.recycle();
                            obtain.recycle();
                        }
                        final IBinder binder = null;
                        continue;
                    }
                }
            }
            
            @Override
            public void wakeup() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICarService");
                    this.mRemote.transact(17, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
