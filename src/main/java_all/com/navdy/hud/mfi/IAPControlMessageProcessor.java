package com.navdy.hud.mfi;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public abstract class IAPControlMessageProcessor
{
    protected ExecutorService mSerialExecutor;
    protected iAPProcessor miAPProcessor;
    
    public IAPControlMessageProcessor(final iAPProcessor.iAPMessage[] array, final iAPProcessor miAPProcessor) {
        this.mSerialExecutor = Executors.newSingleThreadExecutor();
        this.miAPProcessor = miAPProcessor;
        if (array != null) {
            for (int length = array.length, i = 0; i < length; ++i) {
                this.miAPProcessor.registerControlMessageProcessor(array[i], this);
            }
        }
    }
    
    public abstract void bProcessControlMessage(final iAPProcessor.iAPMessage p0, final int p1, final byte[] p2);
    
    public void processControlMessage(final iAPProcessor.iAPMessage iapMessage, final int n, final byte[] array) {
        this.mSerialExecutor.submit(new Runnable() {
            @Override
            public void run() {
                IAPControlMessageProcessor.this.bProcessControlMessage(iapMessage, n, array);
            }
        });
    }
}
