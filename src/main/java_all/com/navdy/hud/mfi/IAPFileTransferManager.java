package com.navdy.hud.mfi;

import java.util.Iterator;
import android.util.Log;
import java.util.concurrent.Executors;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;

public class IAPFileTransferManager implements IIAPFileTransferManager
{
    public static final int DEFAULT_FILE_TRANSFER_LIMIT = 1048576;
    private static final int FILE_TRANSFER_IDENTIFIER_INDEX = 0;
    private static final int MAX_SESSIONS = 3;
    private static final String TAG;
    private volatile int expectedFileTransferIdentifier;
    private long mFileTransferLimit;
    private IAPFileTransferListener mFileTransferListener;
    private iAPProcessor mIapProcessor;
    private ExecutorService mSerialExecutor;
    private HashMap<Integer, FileTransferSession> mSessionCache;
    
    static {
        TAG = IAPFileTransferManager.class.getSimpleName();
    }
    
    public IAPFileTransferManager(final iAPProcessor mIapProcessor) {
        this.mSerialExecutor = Executors.newSingleThreadExecutor();
        this.expectedFileTransferIdentifier = -1;
        this.mFileTransferLimit = 1048576L;
        this.mIapProcessor = mIapProcessor;
        this.mSessionCache = new HashMap<Integer, FileTransferSession>();
    }
    
    @Override
    public void cancel() {
        this.mSerialExecutor.submit(new Runnable() {
            @Override
            public void run() {
                for (final FileTransferSession fileTransferSession : IAPFileTransferManager.this.mSessionCache.values()) {
                    if (fileTransferSession != null) {
                        if (fileTransferSession.isActive()) {
                            Log.d(IAPFileTransferManager.TAG, "Canceling, File transfer session , ID : " + fileTransferSession.mFileTransferIdentifier);
                            fileTransferSession.cancel();
                            if (IAPFileTransferManager.this.mFileTransferListener == null) {
                                continue;
                            }
                            IAPFileTransferManager.this.mFileTransferListener.onFileTransferCancel(fileTransferSession.mFileTransferIdentifier);
                        }
                        else {
                            Log.d(IAPFileTransferManager.TAG, "File transfer session not canceled , ID : " + fileTransferSession.mFileTransferIdentifier + ", Not active");
                        }
                    }
                }
            }
        });
    }
    
    @Override
    public void clear() {
        this.mSessionCache.clear();
    }
    
    @Override
    public long getFileTransferLimit() {
        return this.mFileTransferLimit;
    }
    
    @Override
    public void onCanceled(final int n) {
        Log.d(IAPFileTransferManager.TAG, "File transfer was cancelled " + n);
        this.mSessionCache.remove(n);
        if (this.mFileTransferListener != null) {
            this.mFileTransferListener.onFileTransferCancel(n);
        }
    }
    
    @Override
    public void onError(final int n, final Throwable t) {
        Log.d(IAPFileTransferManager.TAG, "Error in file transfer " + t);
        this.mSessionCache.remove(n);
    }
    
    @Override
    public void onFileTransferSetupRequest(final int n, final long n2) {
        if (this.mFileTransferListener != null) {
            this.mFileTransferListener.onFileTransferSetup(n, n2);
        }
    }
    
    @Override
    public void onFileTransferSetupResponse(final int n, final boolean b) {
        final FileTransferSession fileTransferSession = this.mSessionCache.get(n);
        if (fileTransferSession != null) {
            if (b) {
                fileTransferSession.proceed();
            }
            else {
                fileTransferSession.cancel();
            }
        }
    }
    
    @Override
    public void onPaused(final int n) {
        Log.d(IAPFileTransferManager.TAG, "File transfer paused");
    }
    
    @Override
    public void onSuccess(final int n, final byte[] array) {
        if (this.mFileTransferListener != null) {
            Log.d(IAPFileTransferManager.TAG, "File successfully transferred " + n);
            this.mFileTransferListener.onFileReceived(n, array);
        }
        this.mSessionCache.remove(n);
    }
    
    @Override
    public void queue(final byte[] array) {
        this.mSerialExecutor.submit(new Runnable() {
            @Override
            public void run() {
                if (array != null) {
                    final int n = array[0] & 0xFF;
                    FileTransferSession fileTransferSession;
                    if ((fileTransferSession = IAPFileTransferManager.this.mSessionCache.get(n)) == null) {
                        Log.d(IAPFileTransferManager.TAG, "Creating a new file transfer session " + n);
                        fileTransferSession = new FileTransferSession(n, IAPFileTransferManager.this);
                        IAPFileTransferManager.this.mSessionCache.put(n, fileTransferSession);
                    }
                    fileTransferSession.bProcessFileTransferMessage(array);
                }
            }
        });
    }
    
    @Override
    public void sendMessage(final int n, final byte[] array) {
        this.mIapProcessor.sendToDevice(2, array);
    }
    
    @Override
    public void setFileTransferLimit(final int n) {
        this.mFileTransferLimit = n;
    }
    
    public void setFileTransferListener(final IAPFileTransferListener mFileTransferListener) {
        this.mFileTransferListener = mFileTransferListener;
    }
}
