package com.navdy.hud.mfi;

public interface IAPFileTransferListener
{
    void onFileReceived(final int p0, final byte[] p1);
    
    void onFileTransferCancel(final int p0);
    
    void onFileTransferSetup(final int p0, final long p1);
}
