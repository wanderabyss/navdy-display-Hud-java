package com.navdy.hud.mfi;

public interface EASessionPacketReceiver
{
    void queue(final EASessionPacket p0);
}
