package com.navdy.hud.app.audio;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.SoundPool;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.util.GenericUtil;
import android.media.SoundPool;
import java.util.HashMap;
import com.navdy.service.library.log.Logger;

public class SoundUtils
{
    private static boolean initialized;
    private static final Logger sLogger;
    private static HashMap<Sound, Integer> soundIdMap;
    private static SoundPool soundPool;
    
    static {
        sLogger = new Logger(SoundUtils.class);
        SoundUtils.soundIdMap = new HashMap<Sound, Integer>();
    }
    
    public static void init() {
        if (!SoundUtils.initialized) {
            GenericUtil.checkNotOnMainThread();
            SoundUtils.initialized = true;
            final Context appContext = HudApplication.getAppContext();
            SoundUtils.soundPool = new SoundPool$Builder().setAudioAttributes(new AudioAttributes$Builder().setUsage(13).setContentType(4).build()).build();
            SoundUtils.soundIdMap.put(Sound.MENU_MOVE, SoundUtils.soundPool.load(appContext, R.raw.sound_menu_move, 1));
            SoundUtils.soundIdMap.put(Sound.MENU_SELECT, SoundUtils.soundPool.load(appContext, R.raw.sound_menu_select, 1));
            SoundUtils.soundIdMap.put(Sound.STARTUP, SoundUtils.soundPool.load(appContext, R.raw.sound_startup, 1));
            SoundUtils.soundIdMap.put(Sound.SHUTDOWN, SoundUtils.soundPool.load(appContext, R.raw.sound_shutdown, 1));
            SoundUtils.soundIdMap.put(Sound.ALERT_POSITIVE, SoundUtils.soundPool.load(appContext, R.raw.sound_alert_positive, 1));
            SoundUtils.soundIdMap.put(Sound.ALERT_NEGATIVE, SoundUtils.soundPool.load(appContext, R.raw.sound_alert_negative, 1));
            SoundUtils.sLogger.v("sound pool created");
        }
    }
    
    public static void playSound(final Sound sound) {
        while (true) {
            Integer n = null;
            Label_0065: {
                try {
                    if (SoundUtils.initialized) {
                        n = SoundUtils.soundIdMap.get(sound);
                        if (n != null) {
                            break Label_0065;
                        }
                        SoundUtils.sLogger.v("soundId not found:" + sound);
                    }
                    return;
                }
                catch (Throwable t) {
                    SoundUtils.sLogger.e(t);
                    return;
                }
            }
            SoundUtils.soundPool.play((int)n, 1.0f, 1.0f, 0, 0, 1.0f);
        }
    }
    
    public enum Sound
    {
        ALERT_NEGATIVE, 
        ALERT_POSITIVE, 
        MENU_MOVE, 
        MENU_SELECT, 
        SHUTDOWN, 
        STARTUP;
    }
}
