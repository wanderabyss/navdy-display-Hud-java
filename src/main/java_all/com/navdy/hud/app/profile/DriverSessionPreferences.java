package com.navdy.hud.app.profile;

import com.navdy.service.library.events.preferences.LocalPreferences;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.maps.NavSessionPreferences;
import com.navdy.service.library.events.settings.DateTimeConfiguration;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class DriverSessionPreferences
{
    private static final Logger sLogger;
    private Bus bus;
    private DateTimeConfiguration clockConfiguration;
    private DateTimeConfiguration defaultClockConfiguration;
    private NavSessionPreferences navSessionPreferences;
    private TimeHelper timeHelper;
    
    static {
        sLogger = new Logger(DriverSessionPreferences.class);
    }
    
    public DriverSessionPreferences(final Bus bus, final DriverProfile driverProfile, final TimeHelper timeHelper) {
        this.bus = bus;
        this.timeHelper = timeHelper;
        this.defaultClockConfiguration = new DateTimeConfiguration(0L, null, DateTimeConfiguration.Clock.CLOCK_12_HOUR);
        timeHelper.setFormat(DateTimeConfiguration.Clock.CLOCK_12_HOUR);
        this.navSessionPreferences = new NavSessionPreferences(driverProfile.getNavigationPreferences());
        this.setClockConfiguration(this.defaultClockConfiguration);
    }
    
    public NavSessionPreferences getNavigationSessionPreference() {
        return this.navSessionPreferences;
    }
    
    public void setClockConfiguration(final DateTimeConfiguration.Clock clock) {
        if (clock != null) {
            switch (clock) {
                case CLOCK_12_HOUR:
                    this.timeHelper.setFormat(DateTimeConfiguration.Clock.CLOCK_12_HOUR);
                    break;
                case CLOCK_24_HOUR:
                    this.timeHelper.setFormat(DateTimeConfiguration.Clock.CLOCK_24_HOUR);
                    break;
            }
            DriverSessionPreferences.sLogger.i("setClockConfiguration:" + clock);
            this.bus.post(TimeHelper.CLOCK_CHANGED);
        }
    }
    
    public void setClockConfiguration(final DateTimeConfiguration clockConfiguration) {
        this.clockConfiguration = clockConfiguration;
        if (clockConfiguration.format != null) {
            this.setClockConfiguration(clockConfiguration.format);
        }
    }
    
    public void setDefault(final DriverProfile driverProfile, final boolean b) {
        final LocalPreferences localPreferences = driverProfile.getLocalPreferences();
        if (localPreferences != null && localPreferences.clockFormat != null) {
            this.setClockConfiguration(localPreferences.clockFormat);
        }
        else if (b) {
            this.setClockConfiguration(this.defaultClockConfiguration);
        }
        this.navSessionPreferences.setDefault(driverProfile.getNavigationPreferences());
    }
}
