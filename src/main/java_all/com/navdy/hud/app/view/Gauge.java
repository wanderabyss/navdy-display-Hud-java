package com.navdy.hud.app.view;

import android.view.View;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.graphics.Paint;
import android.graphics.Paint;
import android.graphics.RectF;
import android.content.res.TypedArray;
import com.navdy.hud.app.R;
import android.graphics.Paint;
import android.graphics.Canvas;
import android.util.TypedValue;
import android.util.AttributeSet;
import android.content.Context;
import com.navdy.hud.app.util.CustomDimension;
import android.graphics.Paint;
import com.navdy.service.library.log.Logger;
import android.view.View;

public class Gauge extends View implements SerialValueAnimatorAdapter
{
    private static final int ANIMATION_DURATION = 100;
    private static final int DEFAULT_SHADOW_THICKNESS_DP = 0;
    private static final float DEFAULT_SUB_TEXT_SIZE = 40.0f;
    private static final float DEFAULT_TEXT_SIZE = 90.0f;
    private static final int DEFAULT_THICKNESS_DP = 40;
    private static final int DEFAULT_TIC_LENGTH_DP = 4;
    private static final int SHADOW_START_ANGLE = 2;
    private static final int TIC_STYLE_CIRCLE = 2;
    private static final int TIC_STYLE_LINE = 1;
    private static final int TIC_STYLE_NONE = 0;
    private static final Logger sLogger;
    private boolean AntialiasSetting;
    private boolean mAnimateValues;
    protected int mBackgroundColor;
    private Paint mBackgroundPaint;
    protected int mBackgroundThickness;
    protected CustomDimension mBackgroundThicknessAttribute;
    protected String mCenterSubtext;
    protected String mCenterText;
    protected int mEndColor;
    protected int mMaxValue;
    protected int mMinValue;
    private SerialValueAnimator mSerialValueAnimator;
    protected int mShadowColor;
    protected int mShadowThickness;
    protected CustomDimension mShadowThicknessAttribute;
    protected int mStartAngle;
    protected int mStartColor;
    protected float mSubTextSize;
    protected CustomDimension mSubTextSizeAttribute;
    protected int mSweepAngle;
    protected int mTextColor;
    protected float mTextSize;
    protected CustomDimension mTextSizeAttribute;
    protected int mThickness;
    protected CustomDimension mThicknessAttribute;
    protected int mTicColor;
    private int mTicInterval;
    private int mTicLength;
    protected int mTicPadding;
    private Paint mTicPaint;
    protected int mTicStyle;
    protected int mValue;
    protected int mWarningColor;
    protected int mWarningValue;
    
    static {
        sLogger = new Logger(Gauge.class);
    }
    
    public Gauge(final Context context) {
        this(context, null);
    }
    
    public Gauge(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public Gauge(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mTicColor = -1;
        this.AntialiasSetting = true;
        this.mAnimateValues = true;
        this.initFromAttributes(context, set);
        this.initDrawingTools();
        this.mSerialValueAnimator = new SerialValueAnimator((SerialValueAnimator.SerialValueAnimatorAdapter)this, 100);
    }
    
    private int chooseDimension(final int n, final int n2) {
        int preferredSize = n2;
        if (n != Integer.MIN_VALUE) {
            if (n == 1073741824) {
                preferredSize = n2;
            }
            else {
                preferredSize = this.getPreferredSize();
            }
        }
        return preferredSize;
    }
    
    private int convertDpToPx(final int n) {
        return (int)TypedValue.applyDimension(1, (float)n, this.getResources().getDisplayMetrics());
    }
    
    private void drawText(final Canvas canvas) {
        if (this.mCenterText != null || this.mCenterSubtext != null) {
            final Paint paint = new Paint();
            paint.setColor(this.mTextColor);
            final float mTextSize = this.mTextSize;
            final float mSubTextSize = this.mSubTextSize;
            paint.setTextSize(mTextSize);
            paint.setTextAlign(Paint$Align.CENTER);
            final float n = this.getWidth() / 2.0f;
            final float n2 = this.getHeight() / 2.0f;
            if (this.mCenterText != null) {
                canvas.drawText(this.mCenterText, n, n2, paint);
            }
            if (this.mCenterSubtext != null) {
                paint.setTextSize(mSubTextSize);
                canvas.drawText(this.mCenterSubtext, n, n2 + mSubTextSize + 10.0f, paint);
            }
        }
    }
    
    private void drawTics(final Canvas canvas) {
        if (this.mTicStyle != 0) {
            final float n = Math.round(this.getWidth() / 2.0f);
            final float n2 = Math.round(this.getHeight() / 2.0f);
            final float radius = this.getRadius();
            canvas.save(1);
            canvas.rotate((float)this.mStartAngle, n, n2);
            float n3 = this.mStartAngle;
            final float deltaToAngle = this.deltaToAngle(this.mTicInterval);
            final float n4 = this.mStartAngle + this.mSweepAngle;
            final float n5 = deltaToAngle / 2.0f;
            final float valueToAngle = this.valueToAngle(this.mValue);
            while (n3 <= n4) {
                int alpha = 128;
                if (valueToAngle > n3) {
                    alpha = 255;
                }
                else if (valueToAngle > n3 - n5) {
                    alpha = 128 + Math.round(127 * (valueToAngle - (n3 - n5)) / n5);
                }
                this.mTicPaint.setAlpha(alpha);
                if (this.mTicStyle == 1) {
                    canvas.drawLine(n + radius - this.mThickness / 2.0f - this.mTicLength - this.mTicPadding, n2, n + radius - this.mThickness / 2.0f - this.mTicPadding, n2, this.mTicPaint);
                }
                else if (this.mTicStyle == 2) {
                    canvas.drawCircle(n + radius - this.mThickness / 2.0f - this.mTicLength / 2.0f - this.mTicPadding, n2, this.mTicLength / 2.0f, this.mTicPaint);
                }
                canvas.rotate(deltaToAngle, n, n2);
                n3 += deltaToAngle;
            }
            canvas.restore();
        }
    }
    
    private void evaluateDimensions() {
        final int min = Math.min(this.getWidth(), this.getHeight());
        this.mThickness = (int)this.mThicknessAttribute.getSize(this, min, 0.0f);
        this.mShadowThickness = (int)this.mShadowThicknessAttribute.getSize(this, min, 0.0f);
        if (this.mThickness <= this.mShadowThickness) {
            Gauge.sLogger.e("Shadow is set to be bigger than gauge's thickness - removing shadow");
            this.mShadowThickness = 0;
        }
        this.mBackgroundThickness = (int)this.mBackgroundThicknessAttribute.getSize(this, min, 0.0f);
        this.mTextSize = this.mTextSizeAttribute.getSize(this, min, 0.0f);
        this.mSubTextSize = this.mSubTextSizeAttribute.getSize(this, min, 0.0f);
    }
    
    private int getPreferredSize() {
        return 300;
    }
    
    private float getRadius(final int n) {
        return (Math.min(this.getHeight(), this.getWidth()) - n) / 2.0f;
    }
    
    private void initFromAttributes(Context obtainStyledAttributes, final AttributeSet set) {
        obtainStyledAttributes = (Context)obtainStyledAttributes.getTheme().obtainStyledAttributes(set, R.styleable.Gauge, 0, 0);
        final int convertDpToPx = this.convertDpToPx(40);
        final int convertDpToPx2 = this.convertDpToPx(0);
        final int convertDpToPx3 = this.convertDpToPx(4);
        try {
            this.mMinValue = ((TypedArray)obtainStyledAttributes).getInteger(5, 0);
            this.mValue = ((TypedArray)obtainStyledAttributes).getInteger(7, 0);
            this.mMaxValue = ((TypedArray)obtainStyledAttributes).getInteger(6, 120);
            this.mStartAngle = ((TypedArray)obtainStyledAttributes).getInteger(0, 150);
            this.mSweepAngle = ((TypedArray)obtainStyledAttributes).getInteger(1, 240);
            this.mTicPadding = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(20, 0);
            this.mThicknessAttribute = CustomDimension.getDimension(this, (TypedArray)obtainStyledAttributes, 2, convertDpToPx);
            if (CustomDimension.hasDimension(this, (TypedArray)obtainStyledAttributes, 3)) {
                this.mBackgroundThicknessAttribute = CustomDimension.getDimension(this, (TypedArray)obtainStyledAttributes, 3, convertDpToPx);
            }
            else {
                this.mBackgroundThicknessAttribute = CustomDimension.getDimension(this, (TypedArray)obtainStyledAttributes, 2, convertDpToPx);
            }
            this.mShadowThicknessAttribute = CustomDimension.getDimension(this, (TypedArray)obtainStyledAttributes, 4, convertDpToPx2);
            this.mTextSizeAttribute = CustomDimension.getDimension(this, (TypedArray)obtainStyledAttributes, 14, 90.0f);
            this.mSubTextSizeAttribute = CustomDimension.getDimension(this, (TypedArray)obtainStyledAttributes, 15, 40.0f);
            this.mTicStyle = ((TypedArray)obtainStyledAttributes).getInteger(19, 0);
            this.mTicLength = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(21, convertDpToPx3);
            this.mTicInterval = ((TypedArray)obtainStyledAttributes).getInteger(22, 10);
            this.mBackgroundColor = ((TypedArray)obtainStyledAttributes).getColor(8, -7829368);
            this.mWarningColor = ((TypedArray)obtainStyledAttributes).getColor(10, -65536);
            this.mTextColor = ((TypedArray)obtainStyledAttributes).getColor(11, -1);
            this.mWarningValue = ((TypedArray)obtainStyledAttributes).getInteger(9, 75);
            this.mStartColor = ((TypedArray)obtainStyledAttributes).getColor(16, -16777216);
            this.mEndColor = ((TypedArray)obtainStyledAttributes).getColor(17, -1);
            this.mShadowColor = ((TypedArray)obtainStyledAttributes).getColor(18, -7829368);
            this.mCenterText = ((TypedArray)obtainStyledAttributes).getString(12);
            this.mCenterSubtext = ((TypedArray)obtainStyledAttributes).getString(13);
            ((TypedArray)obtainStyledAttributes).recycle();
            if (this.mMaxValue < this.mMinValue) {
                this.mMaxValue = this.mMinValue + 1;
            }
            if (this.mSweepAngle <= 0) {
                this.mSweepAngle = 1;
            }
            if (this.mTicInterval <= 0) {
                this.mTicInterval = 1;
            }
        }
        finally {
            ((TypedArray)obtainStyledAttributes).recycle();
        }
    }
    
    private float valueToAngle(final int n) {
        return this.mStartAngle + this.mSweepAngle * (n - this.mMinValue) / (this.mMaxValue - this.mMinValue);
    }
    
    public void animationComplete(final float n) {
    }
    
    public void clearAnimationQueue() {
        this.mSerialValueAnimator.release();
    }
    
    protected float deltaToAngle(final int n) {
        return this.mSweepAngle * n / (this.mMaxValue - this.mMinValue);
    }
    
    protected void drawBackground(final Canvas canvas) {
        final float n = this.getWidth();
        final float n2 = this.getHeight();
        final float radius = this.getRadius();
        final Paint mBackgroundPaint = this.mBackgroundPaint;
        final RectF rectF = new RectF();
        rectF.set(n / 2.0f - radius, n2 / 2.0f - radius, n / 2.0f + radius, n2 / 2.0f + radius);
        mBackgroundPaint.setColor(this.mBackgroundColor);
        float n3 = this.mSweepAngle;
        if (this.mWarningValue != 0) {
            final float deltaToAngle = this.deltaToAngle(this.mMaxValue - this.mWarningValue);
            n3 -= deltaToAngle;
            mBackgroundPaint.setColor(this.mWarningColor);
            canvas.drawArc(rectF, this.valueToAngle(this.mWarningValue), deltaToAngle, false, mBackgroundPaint);
        }
        mBackgroundPaint.setColor(this.mBackgroundColor);
        canvas.drawArc(rectF, (float)this.mStartAngle, n3, false, mBackgroundPaint);
    }
    
    protected void drawGauge(final Canvas canvas) {
        this.drawIndicator(canvas, this.mMinValue, this.mValue);
    }
    
    public void drawIndicator(final Canvas canvas) {
        final float n = this.getWidth();
        final float n2 = this.getHeight();
        final float radius = this.getRadius();
        final Paint paint = new Paint();
        paint.setColor(this.mStartColor);
        paint.setStrokeWidth((float)this.mThickness);
        paint.setAntiAlias(true);
        paint.setStrokeCap(Paint$Cap.BUTT);
        paint.setStyle(Paint$Style.STROKE);
        final float deltaToAngle = this.deltaToAngle(this.mValue - this.mMinValue);
        if (this.mStartColor != this.mEndColor) {
            paint.setShader((Shader)new SweepGradient(n / 2.0f, n2 / 2.0f, new int[] { this.mStartColor, this.mStartColor, this.mEndColor, this.mEndColor }, new float[] { 0.0f, this.mStartAngle / 360.0f, (this.mStartAngle + deltaToAngle) / 360.0f, 1.0f }));
        }
        final float n3 = n / 2.0f - radius;
        final float n4 = n2 / 2.0f - radius;
        final float n5 = n / 2.0f + radius;
        final float n6 = n2 / 2.0f + radius;
        float n7 = 0.0f;
        float n8 = 0.0f;
        if (this.mShadowThickness > 0) {
            final Paint paint2 = new Paint(paint);
            paint2.setColor(this.mShadowColor);
            canvas.drawArc(new RectF(n3, n4, n5, n6), (float)this.mStartAngle, deltaToAngle, false, paint2);
            final int n9 = this.mThickness - this.mShadowThickness;
            paint.setStrokeWidth((float)n9);
            n7 = radius - this.getRadius(n9);
            n8 = 2.0f;
        }
        if (deltaToAngle > n8) {
            final RectF rectF = new RectF();
            rectF.set(n3 + n7, n4 + n7, n5 - n7, n6 - n7);
            canvas.drawArc(rectF, this.mStartAngle + n8, deltaToAngle - n8, false, paint);
        }
    }
    
    public void drawIndicator(final Canvas canvas, int mStartColor, int mEndColor) {
        final float n = this.getWidth();
        final float n2 = this.getHeight();
        final float radius = this.getRadius();
        final Paint paint = new Paint();
        paint.setColor(this.mStartColor);
        paint.setStrokeWidth((float)this.mThickness);
        paint.setAntiAlias(true);
        paint.setStrokeCap(Paint$Cap.BUTT);
        paint.setStyle(Paint$Style.STROKE);
        final float deltaToAngle = this.deltaToAngle(mEndColor - mStartColor);
        final float n3 = this.mStartAngle + this.deltaToAngle(mStartColor - this.mMinValue);
        if (this.mStartColor != this.mEndColor) {
            final int mStartColor2 = this.mStartColor;
            mStartColor = this.mStartColor;
            mEndColor = this.mEndColor;
            paint.setShader((Shader)new SweepGradient(n / 2.0f, n2 / 2.0f, new int[] { mStartColor2, mStartColor, mEndColor, this.mEndColor }, new float[] { 0.0f, n3 / 360.0f, (n3 + deltaToAngle) / 360.0f, 1.0f }));
        }
        final float n4 = n / 2.0f - radius;
        final float n5 = n2 / 2.0f - radius;
        final float n6 = n / 2.0f + radius;
        final float n7 = n2 / 2.0f + radius;
        float n8 = 0.0f;
        float n9 = 0.0f;
        if (this.mShadowThickness > 0) {
            final Paint paint2 = new Paint(paint);
            paint2.setColor(this.mShadowColor);
            canvas.drawArc(new RectF(n4, n5, n6, n7), n3, deltaToAngle, false, paint2);
            mStartColor = this.mThickness - this.mShadowThickness;
            paint.setStrokeWidth((float)mStartColor);
            n8 = radius - this.getRadius(mStartColor);
            n9 = 2.0f;
        }
        if (deltaToAngle > n9) {
            final RectF rectF = new RectF();
            rectF.set(n4 + n8, n5 + n8, n6 - n8, n7 - n8);
            canvas.drawArc(rectF, n3 + n9, deltaToAngle - n9, false, paint);
        }
    }
    
    public float getRadius() {
        return this.getRadius(this.mThickness);
    }
    
    public float getValue() {
        return this.mValue;
    }
    
    protected void initDrawingTools() {
        this.evaluateDimensions();
        (this.mBackgroundPaint = new Paint()).setStrokeWidth((float)this.mBackgroundThickness);
        this.mBackgroundPaint.setAntiAlias(true);
        this.mBackgroundPaint.setStrokeCap(Paint$Cap.BUTT);
        this.mBackgroundPaint.setStyle(Paint$Style.STROKE);
        (this.mTicPaint = new Paint()).setColor(this.mTicColor);
        final Paint mTicPaint = this.mTicPaint;
        Paint$Style style;
        if (this.mTicStyle == 1) {
            style = Paint$Style.STROKE;
        }
        else {
            style = Paint$Style.FILL;
        }
        mTicPaint.setStyle(style);
        this.mTicPaint.setStrokeWidth(0.0f);
        this.mTicPaint.setAntiAlias(this.AntialiasSetting);
    }
    
    protected void onDraw(final Canvas canvas) {
        this.drawBackground(canvas);
        this.drawTics(canvas);
        this.drawText(canvas);
        this.drawGauge(canvas);
    }
    
    protected void onMeasure(int n, int size) {
        final int mode = View$MeasureSpec.getMode(n);
        final int size2 = View$MeasureSpec.getSize(n);
        n = View$MeasureSpec.getMode(size);
        size = View$MeasureSpec.getSize(size);
        n = Math.min(this.chooseDimension(mode, size2), this.chooseDimension(n, size));
        this.setMeasuredDimension(n, n);
    }
    
    protected void onSizeChanged(final int n, final int n2, final int n3, final int n4) {
        this.initDrawingTools();
    }
    
    public void setAnimated(final boolean mAnimateValues) {
        this.mAnimateValues = mAnimateValues;
    }
    
    public void setBackgroundColor(final int mBackgroundColor) {
        this.mBackgroundColor = mBackgroundColor;
        this.invalidate();
    }
    
    public void setCenterSubtext(final String mCenterSubtext) {
        this.mCenterSubtext = mCenterSubtext;
        this.invalidate();
    }
    
    public void setCenterText(final String mCenterText) {
        this.mCenterText = mCenterText;
        this.invalidate();
    }
    
    public void setMaxValue(final int mMaxValue) {
        this.mMaxValue = mMaxValue;
    }
    
    public void setValue(final float n) {
        this.mValue = (int)n;
        this.invalidate();
    }
    
    public void setValue(int mMaxValue) {
        int mMinValue = mMaxValue;
        if (mMaxValue < this.mMinValue) {
            mMinValue = this.mMinValue;
        }
        if ((mMaxValue = mMinValue) > this.mMaxValue) {
            mMaxValue = this.mMaxValue;
        }
        if (this.mAnimateValues) {
            this.mSerialValueAnimator.setValue(mMaxValue);
        }
        else {
            this.mValue = mMaxValue;
            this.invalidate();
        }
    }
}
