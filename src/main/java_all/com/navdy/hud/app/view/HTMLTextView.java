package com.navdy.hud.app.view;

import android.text.Html;
import android.widget.TextView;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.TextView;

public class HTMLTextView extends TextView
{
    public HTMLTextView(final Context context) {
        super(context);
    }
    
    public HTMLTextView(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public HTMLTextView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    public void setText(final CharSequence charSequence, final TextView$BufferType textView$BufferType) {
        super.setText((CharSequence)Html.fromHtml(charSequence.toString()), textView$BufferType);
    }
}
