package com.navdy.hud.app.view;

import com.navdy.hud.app.service.S3FileUploadService;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.service.GestureVideosSyncService;
import com.navdy.service.library.util.IOUtils;
import java.io.File;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.service.library.events.input.GestureEvent;
import android.view.View;
import butterknife.ButterKnife;
import com.navdy.hud.app.profile.DriverProfile;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.navdy.hud.app.framework.DriverProfileHelper;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import mortar.Mortar;
import android.os.Looper;
import java.util.Collections;
import java.util.HashSet;
import android.util.AttributeSet;
import android.content.Context;
import java.util.concurrent.ExecutorService;
import java.util.Set;
import android.widget.TextView;
import android.widget.ImageView;
import android.os.Handler;
import com.navdy.hud.app.screen.GestureLearningScreen;
import butterknife.InjectView;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import java.util.List;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import android.widget.RelativeLayout;

public class GestureVideoCaptureView extends RelativeLayout implements IListener, IInputHandler
{
    public static final int TAG_EXIT = 0;
    public static final int TAG_NEXT = 1;
    private static final long UPLOAD_TIMEOUT = 300000L;
    public static final Logger sLogger;
    @Inject
    Bus bus;
    private List<Choice> emptyChoices;
    private List<Choice> exitChoices;
    @Inject
    GestureServiceConnector gestureService;
    @InjectView(R.id.choiceLayout)
    ChoiceLayout mChoiceLayout;
    @Inject
    GestureLearningScreen.Presenter mPresenter;
    private Handler mainHandler;
    @InjectView(R.id.image)
    ImageView mainImageView;
    @InjectView(R.id.title1)
    TextView mainText;
    private List<Choice> nextAndExitChoices;
    private State prevState;
    private RecordingSavingContext recordingToSave;
    private Set<String> recordingsToUpload;
    private boolean registered;
    @InjectView(R.id.title3)
    TextView secondaryText;
    private ExecutorService serialExecutor;
    private String sessionName;
    @InjectView(R.id.sideImage)
    ImageView sideImageView;
    private State state;
    private Runnable uploadTimeoutRunnable;
    
    static {
        sLogger = new Logger(GestureVideoCaptureView.class);
    }
    
    public GestureVideoCaptureView(final Context context) {
        this(context, null);
    }
    
    public GestureVideoCaptureView(final Context context, final AttributeSet set) {
        this(context, set, -1);
    }
    
    public GestureVideoCaptureView(final Context context, final AttributeSet set, final int n) {
        this(context, set, n, -1);
    }
    
    public GestureVideoCaptureView(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.registered = false;
        this.recordingsToUpload = new HashSet<String>();
        this.emptyChoices = (List<Choice>)Collections.EMPTY_LIST;
        this.mainHandler = new Handler(Looper.getMainLooper());
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
        this.serialExecutor = Executors.newSingleThreadExecutor();
        (this.nextAndExitChoices = new ArrayList<Choice>()).add(new ChoiceLayout.Choice(context.getString(R.string.next_recording_step), 1));
        this.nextAndExitChoices.add(new ChoiceLayout.Choice(context.getString(R.string.exit_recording), 0));
        (this.exitChoices = new ArrayList<Choice>()).add(new ChoiceLayout.Choice(context.getString(R.string.exit_recording), 0));
    }
    
    private List<Choice> choicesForState(final State state) {
        List<Choice> list = null;
        switch (state) {
            default:
                list = this.emptyChoices;
                break;
            case SWIPE_LEFT:
            case SWIPE_RIGHT:
            case DOUBLE_TAP_1:
            case DOUBLE_TAP_2:
                list = this.nextAndExitChoices;
                break;
            case UPLOADING:
            case ALL_DONE:
            case UPLOADING_FAILURE:
            case NOT_PAIRED:
                list = this.exitChoices;
                break;
        }
        return list;
    }
    
    private void createSession() {
        this.gestureService.setCalibrationEnabled(false);
        final DriverProfile currentProfile = DriverProfileHelper.getInstance().getCurrentProfile();
        if (currentProfile.isDefaultProfile()) {
            this.setState(State.NOT_PAIRED);
        }
        else {
            this.sessionName = currentProfile.getDriverEmail() + "-" + new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date(System.currentTimeMillis()));
            this.setState(State.SWIPE_LEFT);
        }
    }
    
    private State nextState() {
        State state2;
        final State state = state2 = null;
        switch (this.state) {
            default:
                state2 = state;
                return state2;
            case SWIPE_LEFT:
            case SWIPE_RIGHT:
            case DOUBLE_TAP_1:
            case DOUBLE_TAP_2:
                state2 = State.SAVING;
            case UPLOADING:
            case ALL_DONE:
            case UPLOADING_FAILURE:
            case NOT_PAIRED:
                return state2;
            case SAVING:
                switch (this.prevState) {
                    default:
                        state2 = state;
                        return state2;
                    case SWIPE_LEFT:
                        state2 = State.SWIPE_RIGHT;
                        return state2;
                    case SWIPE_RIGHT:
                        state2 = State.DOUBLE_TAP_1;
                        return state2;
                    case DOUBLE_TAP_1:
                        state2 = State.DOUBLE_TAP_2;
                        return state2;
                    case DOUBLE_TAP_2:
                        state2 = State.UPLOADING;
                        return state2;
                }
                break;
        }
    }
    
    private void resetSession() {
        this.gestureService.setCalibrationEnabled(true);
        this.recordingToSave = null;
        this.recordingsToUpload.clear();
        if (this.uploadTimeoutRunnable != null) {
            this.mainHandler.removeCallbacks(this.uploadTimeoutRunnable);
            this.uploadTimeoutRunnable = null;
        }
    }
    
    private void setState(final State state) {
        if (this.state != null) {
            switch (this.state) {
                case SWIPE_LEFT:
                case SWIPE_RIGHT:
                case DOUBLE_TAP_1:
                case DOUBLE_TAP_2:
                    this.stopRecording();
                    break;
            }
        }
        this.prevState = this.state;
        this.state = state;
        if (this.state == State.SAVING) {
            this.secondaryText.setText(this.state.detailText);
        }
        else {
            this.mainText.setText(this.state.mainText);
            if (this.state.detailText != 0) {
                this.secondaryText.setText(this.state.detailText);
            }
            else {
                this.secondaryText.setText((CharSequence)"");
            }
        }
        this.mChoiceLayout.setChoices(Mode.LABEL, this.choicesForState(this.state), 0, (ChoiceLayout.IListener)this);
        switch (this.state) {
            case SWIPE_LEFT:
            case SWIPE_RIGHT:
            case DOUBLE_TAP_1:
            case DOUBLE_TAP_2:
                this.startRecording();
                break;
            case UPLOADING:
                if (this.recordingsToUpload.isEmpty()) {
                    this.mainHandler.post((Runnable)new Runnable() {
                        @Override
                        public void run() {
                            GestureVideoCaptureView.this.setState(State.ALL_DONE);
                        }
                    });
                    break;
                }
                this.uploadTimeoutRunnable = new UploadTimeoutRunnable(this.sessionName);
                this.mainHandler.postDelayed(this.uploadTimeoutRunnable, 300000L);
                break;
        }
    }
    
    private void startRecording() {
        GestureVideoCaptureView.sLogger.d("Start recording");
        this.recordingToSave = new RecordingSavingContext(this.sessionName, this.state);
        this.gestureService.startRecordingVideo();
    }
    
    private void stopRecording() {
        GestureVideoCaptureView.sLogger.d("Stop recording for " + this.state.filename);
        this.gestureService.stopRecordingVideo();
    }
    
    public void executeItem(final int n, final int n2) {
        switch (n2) {
            case 0:
                this.mPresenter.hideCaptureView();
                break;
            case 1:
                this.setState(this.nextState());
                break;
        }
    }
    
    public void itemSelected(final int n, final int n2) {
    }
    
    public IInputHandler nextHandler() {
        return null;
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        this.sideImageView.setVisibility(View.VISIBLE);
        this.mainImageView.setVisibility(View.VISIBLE);
        this.mainImageView.setImageResource(R.drawable.icon_settings_learning_to_gesture_gray);
        this.findViewById(R.id.title2).setVisibility(GONE);
        this.findViewById(R.id.title4).setVisibility(GONE);
        this.secondaryText.setSingleLine(false);
        this.secondaryText.setTextSize(1, 17.0f);
        this.mChoiceLayout.setHighlightPersistent(true);
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        switch (customKeyEvent) {
            case LEFT:
                this.mChoiceLayout.moveSelectionLeft();
                break;
            case RIGHT:
                this.mChoiceLayout.moveSelectionRight();
                break;
            case SELECT:
                this.mChoiceLayout.executeSelectedItem(true);
                break;
        }
        return false;
    }
    
    @Subscribe
    public void onRecordingSaved(final GestureServiceConnector.RecordingSaved recordingSaved) {
        GestureVideoCaptureView.sLogger.d("Recording saved");
        final RecordingSavingContext recordingToSave = this.recordingToSave;
        if (this.recordingToSave == null) {
            GestureVideoCaptureView.sLogger.d("Ignore recording " + recordingSaved.path + " due to missing context.");
        }
        else if (!this.recordingToSave.sessionName.equals(this.sessionName)) {
            GestureVideoCaptureView.sLogger.d("Recording session " + this.recordingToSave.sessionName + " doesn't match current session " + this.sessionName);
            GestureVideoCaptureView.sLogger.d("Ignore recording " + recordingSaved.path);
        }
        else {
            this.recordingToSave = null;
            this.setState(this.nextState());
            final String string = PathManager.getInstance().getGestureVideosSyncFolder() + File.separator + recordingToSave.sessionName;
            GestureVideoCaptureView.sLogger.d("Session Path : " + string);
            GestureVideoCaptureView.sLogger.d("Video archive name to be saved : " + recordingToSave.state.filename);
            final String string2 = string + File.separator + recordingToSave.state.filename + ".zip";
            this.recordingsToUpload.add(string2);
            this.serialExecutor.submit(new Runnable() {
                final /* synthetic */ String val$userTag = GestureVideoCaptureView.this.sessionName;
                
                @Override
                public void run() {
                    final File file = new File(string);
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                    IOUtils.compressFilesToZip(GestureVideoCaptureView.this.getContext(), new File(recordingSaved.path).listFiles(), string2);
                    GestureVideoCaptureView.sLogger.d("Compressed file : " + string2);
                    GestureVideosSyncService.addGestureVideoToUploadQueue(new File(string2), this.val$userTag);
                    GestureVideosSyncService.syncNow();
                }
            });
        }
    }
    
    @Subscribe
    public void onUploadFinished(final S3FileUploadService.UploadFinished uploadFinished) {
        if (uploadFinished.userTag != null && uploadFinished.userTag.equals(this.sessionName)) {
            if (!uploadFinished.succeeded) {
                GestureVideoCaptureView.sLogger.d("Failed to upload: " + uploadFinished.filePath);
                this.setState(State.UPLOADING_FAILURE);
            }
            else {
                this.recordingsToUpload.remove(uploadFinished.filePath);
                GestureVideoCaptureView.sLogger.d("Remaining files to be uploaded: " + this.recordingsToUpload.size());
                if (this.recordingsToUpload.isEmpty() && this.state == State.UPLOADING) {
                    if (this.uploadTimeoutRunnable != null) {
                        this.mainHandler.removeCallbacks(this.uploadTimeoutRunnable);
                        this.uploadTimeoutRunnable = null;
                    }
                    this.setState(State.ALL_DONE);
                }
            }
        }
    }
    
    public void setVisibility(final int visibility) {
        super.setVisibility(visibility);
        if (visibility == 0) {
            this.gestureService.setRecordMode(true);
            this.createSession();
            if (!this.registered) {
                this.bus.register(this);
                this.registered = true;
            }
        }
        else {
            this.gestureService.stopRecordingVideo();
            this.resetSession();
            this.gestureService.setRecordMode(false);
            if (this.registered) {
                this.bus.unregister(this);
                this.registered = false;
            }
        }
    }
    
    private static class ChoiceInfo
    {
        public final int tag;
        public final int text;
        
        public ChoiceInfo(final int text, final int tag) {
            this.text = text;
            this.tag = tag;
        }
    }
    
    private class RecordingSavingContext
    {
        public final String sessionName;
        public final State state;
        
        public RecordingSavingContext(final String sessionName, final State state) {
            this.sessionName = sessionName;
            this.state = state;
        }
    }
    
    enum State
    {
        ALL_DONE(R.string.uploading_success, 0, (String)null), 
        DOUBLE_TAP_1(R.string.double_tap_1, R.string.double_tap_detail_1, "tap"), 
        DOUBLE_TAP_2(R.string.double_tap_2, R.string.double_tap_detail_2, "tap2"), 
        NOT_PAIRED(R.string.please_pair_phone, R.string.please_pair_phone_detail, (String)null), 
        SAVING(0, R.string.saving_recording, (String)null), 
        SWIPE_LEFT(R.string.swipe_left, 0, "swipe-left"), 
        SWIPE_RIGHT(R.string.swipe_right, 0, "swipe-right"), 
        UPLOADING(R.string.uploading, R.string.uploading_detail, (String)null), 
        UPLOADING_FAILURE(R.string.upload_failure, R.string.upload_failure_detail, (String)null);
        
        int detailText;
        String filename;
        int mainText;
        
        private State(final int mainText, final int detailText, final String filename) {
            this.mainText = mainText;
            this.detailText = detailText;
            this.filename = filename;
        }
    }
    
    private class UploadTimeoutRunnable implements Runnable
    {
        private final String sessionName;
        
        UploadTimeoutRunnable(final String sessionName) {
            this.sessionName = sessionName;
        }
        
        @Override
        public void run() {
            GestureVideoCaptureView.sLogger.d("Upload has timed out.");
            if (this.sessionName.equals(GestureVideoCaptureView.this.sessionName)) {
                GestureVideoCaptureView.this.setState(State.UPLOADING_FAILURE);
            }
        }
    }
}
