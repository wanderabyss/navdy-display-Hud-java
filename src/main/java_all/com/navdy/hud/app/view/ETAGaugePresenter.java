package com.navdy.hud.app.view;

import com.navdy.service.library.events.navigation.DistanceUnit;
import com.navdy.hud.app.maps.util.DistanceConverter;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.here.HereMapsManager;
import android.view.ViewGroup.LayoutParams;
import android.support.constraint.ConstraintSet;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.View;
import butterknife.ButterKnife;
import android.os.Bundle;
import com.navdy.hud.app.maps.NavigationMode;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.ui.framework.UIStateManager;
import android.graphics.drawable.Drawable;
import android.text.style.ForegroundColorSpan;
import android.text.style.AbsoluteSizeSpan;
import android.text.SpannableStringBuilder;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.content.Context;
import java.util.concurrent.TimeUnit;
import android.support.constraint.ConstraintLayout;
import android.widget.ImageView;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.framework.trips.TripManager;
import android.os.Handler;
import android.view.ViewGroup;
import com.navdy.hud.app.view.drawable.ETAProgressDrawable;
import butterknife.InjectView;
import android.widget.TextView;

public class ETAGaugePresenter extends DashboardWidgetPresenter
{
    private static final long TRIP_INFO_UPDATE_INTERVAL;
    @InjectView(R.id.txt_eta_am_pm)
    TextView etaAmPmText;
    private ETAProgressDrawable etaProgressDrawable;
    @InjectView(R.id.txt_eta)
    TextView etaText;
    private String etaTripGaugeName;
    @InjectView(R.id.eta_view)
    ViewGroup etaView;
    private int farMargin;
    private String feetLabel;
    private int fontSize18;
    private int fontSize30;
    private int gravity;
    private int greyColor;
    private boolean isNavigationActive;
    private String kiloMetersLabel;
    private String[] labelShort;
    private String[] labels;
    private Handler mHandler;
    private Runnable mTripInfoUpdateRunnable;
    private TripManager mTripManager;
    private MapEvents.ManeuverDisplay maneuverDisplay;
    private String metersLabel;
    private String milesLabel;
    private int nearMargin;
    @InjectView(R.id.txt_remaining_distance)
    TextView remainingDistanceText;
    @InjectView(R.id.txt_remaining_distance_unit)
    TextView remainingDistanceUnitText;
    @InjectView(R.id.tripDistance)
    TextView tripDistanceView;
    private int tripGaugeIconMargin;
    private int tripGaugeLongMargin;
    private int tripGaugeShortMargin;
    @InjectView(R.id.tripIcon)
    ImageView tripIconView;
    @InjectView(R.id.tripOpenMap)
    ConstraintLayout tripOpenMapView;
    @InjectView(R.id.tripTime)
    TextView tripTimeView;
    @InjectView(R.id.txt_tta1)
    TextView ttaText1;
    @InjectView(R.id.txt_tta2)
    TextView ttaText2;
    @InjectView(R.id.txt_tta3)
    TextView ttaText3;
    @InjectView(R.id.txt_tta4)
    TextView ttaText4;
    
    static {
        TRIP_INFO_UPDATE_INTERVAL = TimeUnit.MINUTES.toMillis(1L);
    }
    
    public ETAGaugePresenter(final Context context) {
        final Resources resources = context.getResources();
        this.mTripManager = RemoteDeviceManager.getInstance().getTripManager();
        this.labels = resources.getStringArray(R.array.time_labels);
        this.labelShort = resources.getStringArray(R.array.time_labels_short);
        this.metersLabel = context.getResources().getString(R.string.unit_meters);
        this.kiloMetersLabel = context.getResources().getString(R.string.unit_kilometers);
        this.feetLabel = context.getResources().getString(R.string.unit_feet);
        this.milesLabel = context.getResources().getString(R.string.unit_miles);
        this.farMargin = resources.getDimensionPixelSize(R.dimen.far_margin);
        this.nearMargin = resources.getDimensionPixelSize(R.dimen.near_margin);
        this.etaTripGaugeName = context.getResources().getString(R.string.widget_trip_eta);
        this.tripGaugeShortMargin = resources.getDimensionPixelSize(R.dimen.trip_gauge_short_margin);
        this.tripGaugeLongMargin = resources.getDimensionPixelSize(R.dimen.trip_gauge_long_margin);
        this.tripGaugeIconMargin = resources.getDimensionPixelSize(R.dimen.trip_gauge_icon_margin);
        this.greyColor = ContextCompat.getColor(context, R.color.grey_ababab);
        this.fontSize18 = resources.getDimensionPixelSize(R.dimen.active_trip_18);
        this.fontSize30 = resources.getDimensionPixelSize(R.dimen.size_30);
        this.mHandler = new Handler();
        this.mTripInfoUpdateRunnable = new Runnable() {
            @Override
            public void run() {
                ETAGaugePresenter.this.mHandler.postDelayed(ETAGaugePresenter.this.mTripInfoUpdateRunnable, ETAGaugePresenter.TRIP_INFO_UPDATE_INTERVAL);
                ETAGaugePresenter.this.reDraw();
            }
        };
        (this.etaProgressDrawable = new ETAProgressDrawable(context)).setMinValue(0.0f);
        this.etaProgressDrawable.setMaxGaugeValue(100.0f);
    }
    
    private void addPadding(final SpannableStringBuilder spannableStringBuilder) {
        spannableStringBuilder.append((CharSequence)" ");
        spannableStringBuilder.append((CharSequence)" ");
        spannableStringBuilder.append((CharSequence)" ");
    }
    
    private SpannableStringBuilder getDistance(final String s, final String s2) {
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        if (this.gravity == 0) {
            this.addPadding(spannableStringBuilder);
        }
        spannableStringBuilder.append((CharSequence)s);
        final int length = spannableStringBuilder.length();
        spannableStringBuilder.setSpan(new AbsoluteSizeSpan(this.fontSize18), 0, length, 34);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(-1), 0, length, 34);
        spannableStringBuilder.append((CharSequence)" ");
        final int length2 = spannableStringBuilder.length();
        spannableStringBuilder.append((CharSequence)s2);
        final int length3 = spannableStringBuilder.length();
        spannableStringBuilder.setSpan(new AbsoluteSizeSpan(this.fontSize18), length2, length3, 34);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(this.greyColor), length2, length3, 34);
        if (this.gravity == 2) {
            this.addPadding(spannableStringBuilder);
        }
        return spannableStringBuilder;
    }
    
    private SpannableStringBuilder getTime(final String... array) {
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        if (this.gravity == 0) {
            this.addPadding(spannableStringBuilder);
        }
        int i = 0;
        while (i < array.length) {
            if (i != 0) {
                spannableStringBuilder.append((CharSequence)" ");
            }
            final String s = array[i];
            final String s2 = array[i + 1];
            i += 2;
            final int length = spannableStringBuilder.length();
            spannableStringBuilder.append((CharSequence)s);
            final int length2 = spannableStringBuilder.length();
            spannableStringBuilder.setSpan(new AbsoluteSizeSpan(this.fontSize30), length, length2, 34);
            spannableStringBuilder.setSpan(new ForegroundColorSpan(-1), length, length2, 34);
            spannableStringBuilder.append((CharSequence)" ");
            final int length3 = spannableStringBuilder.length();
            spannableStringBuilder.append((CharSequence)s2);
            final int length4 = spannableStringBuilder.length();
            spannableStringBuilder.setSpan(new AbsoluteSizeSpan(this.fontSize18), length3, length4, 34);
            spannableStringBuilder.setSpan(new ForegroundColorSpan(this.greyColor), length3, length4, 34);
        }
        if (this.gravity == 2) {
            this.addPadding(spannableStringBuilder);
        }
        return spannableStringBuilder;
    }
    
    private boolean parseSeconds(final long n, final int[] array, final byte[] array2) {
        boolean b = true;
        array[3] = (int)TimeUnit.SECONDS.toDays(n);
        array[2] = (int)(TimeUnit.SECONDS.toHours(n) - array[3] * 24);
        array[1] = (int)(TimeUnit.SECONDS.toMinutes(n) - TimeUnit.SECONDS.toHours(n) * 60L);
        array[0] = (int)(n - TimeUnit.SECONDS.toMinutes(n) * 60L);
        array2[0] = 0;
        array2[1] = 0;
        for (int i = 3; i > 0; i = (byte)(i - 1)) {
            if (array[i] > 0) {
                array2[1] = (byte)i;
                if (array2[0] == 0) {
                    array2[0] = (byte)i;
                }
            }
            else if (array2[0] != 0) {
                break;
            }
        }
        assert array2[0] == array2[1] + 1;
        if (array2[0] <= array2[1]) {
            b = false;
        }
        return b;
    }
    
    private void setTTAText(long max) {
        max = Math.max(60L, max / 1000L);
        final int[] array = new int[4];
        final byte[] array2 = new byte[2];
        final boolean seconds = this.parseSeconds(max, array, array2);
        final byte b = array2[0];
        final byte b2 = array2[1];
        if (seconds) {
            this.ttaText1.setText((CharSequence)Integer.toString(array[b]));
            this.ttaText2.setText((CharSequence)Character.toString(this.labels[b].charAt(0)));
            this.ttaText3.setVisibility(View.VISIBLE);
            this.ttaText4.setVisibility(View.VISIBLE);
            this.ttaText3.setText((CharSequence)Integer.toString(array[b2]));
            this.ttaText4.setText((CharSequence)Character.toString(this.labels[b2].charAt(0)));
        }
        else {
            this.ttaText1.setText((CharSequence)Integer.toString(array[b]));
            this.ttaText2.setText((CharSequence)this.labels[b]);
            this.ttaText3.setVisibility(GONE);
            this.ttaText4.setVisibility(GONE);
        }
    }
    
    private void setTripTimeText(long max) {
        max /= 1000L;
        if (max == 0L) {
            this.tripDistanceView.setVisibility(GONE);
            this.tripTimeView.setText((CharSequence)this.getTime(String.valueOf(0), this.labelShort[0]));
        }
        else {
            max = Math.max(60L, max);
            final int[] array = new int[4];
            final byte[] array2 = new byte[2];
            final boolean seconds = this.parseSeconds(max, array, array2);
            final byte b = array2[0];
            final byte b2 = array2[1];
            SpannableStringBuilder text;
            if (seconds) {
                text = this.getTime(Integer.toString(array[b]), String.valueOf(this.labelShort[b].charAt(0)), Integer.toString(array[b2]), String.valueOf(this.labelShort[b2].charAt(0)));
            }
            else {
                text = this.getTime(Integer.toString(array[b]), String.valueOf(this.labels[b]));
            }
            this.tripTimeView.setText((CharSequence)text);
        }
    }
    
    @Override
    public Drawable getDrawable() {
        return this.etaProgressDrawable;
    }
    
    @Override
    public String getWidgetIdentifier() {
        return "ETA_GAUGE_ID";
    }
    
    @Override
    public String getWidgetName() {
        return this.etaTripGaugeName;
    }
    
    @Override
    protected boolean isRegisteringToBusRequired() {
        return true;
    }
    
    @Subscribe
    public void onManeuverDisplay(final MapEvents.ManeuverDisplay maneuverDisplay) {
        if (!maneuverDisplay.isEmpty()) {
            final UIStateManager uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
            this.maneuverDisplay = maneuverDisplay;
            if (this.maneuverDisplay != null) {
                this.isNavigationActive = this.maneuverDisplay.isNavigating();
            }
            else {
                this.isNavigationActive = uiStateManager.isNavigationActive();
            }
            this.reDraw();
        }
    }
    
    @Subscribe
    public void onNavigationModeChange(final NavigationMode navigationMode) {
        this.isNavigationActive = RemoteDeviceManager.getInstance().getUiStateManager().isNavigationActive();
        this.maneuverDisplay = null;
        this.reDraw();
    }
    
    @Override
    public void setView(final DashboardWidgetView dashboardWidgetView, final Bundle bundle) {
        if (dashboardWidgetView != null) {
            int int1;
            if (bundle != null) {
                int1 = bundle.getInt("EXTRA_GRAVITY", 0);
            }
            else {
                int1 = 0;
            }
            this.gravity = int1;
            dashboardWidgetView.setContentView(R.layout.eta_gauge_layout);
            ButterKnife.inject(this, (View)dashboardWidgetView);
            final ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams)this.etaView.getLayoutParams();
            final ConstraintSet set = new ConstraintSet();
            set.clone(this.tripOpenMapView);
            final int id = this.tripOpenMapView.getId();
            final int id2 = this.tripIconView.getId();
            switch (this.gravity) {
                case 0:
                    layoutParams.leftMargin = this.nearMargin;
                    layoutParams.rightMargin = this.farMargin;
                    set.connect(id2, 6, id, 6, 0);
                    set.connect(this.tripTimeView.getId(), 1, id2, 2, this.tripGaugeIconMargin);
                    break;
                case 2:
                    layoutParams.leftMargin = this.farMargin;
                    layoutParams.rightMargin = this.nearMargin;
                    set.connect(id2, 7, id, 7, 0);
                    set.connect(this.tripTimeView.getId(), 2, id2, 1, this.tripGaugeIconMargin);
                    break;
            }
            set.applyTo(this.tripOpenMapView);
            this.etaView.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
            this.isNavigationActive = RemoteDeviceManager.getInstance().getUiStateManager().isNavigationActive();
            HereMapsManager.getInstance().postManeuverDisplay();
            super.setView(dashboardWidgetView, bundle);
            this.reDraw();
        }
        else {
            super.setView(dashboardWidgetView, bundle);
            this.reDraw();
        }
    }
    
    @Override
    protected void updateGauge() {
        if (!this.isNavigationActive || this.maneuverDisplay == null || this.maneuverDisplay.etaDate == null || this.maneuverDisplay.totalDistanceUnit == null || this.maneuverDisplay.totalDistanceRemainingUnit == null) {
            this.tripOpenMapView.setVisibility(View.VISIBLE);
            long tripTimeText;
            if (this.mTripManager.getCurrentTripStartTime() > 0L) {
                tripTimeText = System.currentTimeMillis() - this.mTripManager.getCurrentTripStartTime();
            }
            else {
                tripTimeText = 0L;
            }
            final long n = this.mTripManager.getCurrentDistanceTraveled();
            final SpeedManager instance = SpeedManager.getInstance();
            final DistanceConverter.Distance distance = new DistanceConverter.Distance();
            DistanceConverter.convertToDistance(instance.getSpeedUnit(), n, distance);
            String s = "";
            switch (distance.unit) {
                case DISTANCE_METERS:
                    s = this.metersLabel;
                    break;
                case DISTANCE_KMS:
                    s = this.kiloMetersLabel;
                    break;
                case DISTANCE_MILES:
                    s = this.milesLabel;
                    break;
                case DISTANCE_FEET:
                    s = this.feetLabel;
                    break;
            }
            this.tripDistanceView.setText((CharSequence)this.getDistance(Float.toString(distance.value), s));
            this.tripDistanceView.setVisibility(View.VISIBLE);
            this.setTripTimeText(tripTimeText);
            this.mHandler.removeCallbacks(this.mTripInfoUpdateRunnable);
            this.mHandler.postDelayed(this.mTripInfoUpdateRunnable, ETAGaugePresenter.TRIP_INFO_UPDATE_INTERVAL);
            this.etaView.setVisibility(GONE);
        }
        else {
            this.mHandler.removeCallbacks(this.mTripInfoUpdateRunnable);
            this.tripOpenMapView.setVisibility(GONE);
            this.etaView.setVisibility(View.VISIBLE);
            final long currentTimeMillis = System.currentTimeMillis();
            final long time = this.maneuverDisplay.etaDate.getTime();
            long ttaText;
            if (time > currentTimeMillis) {
                ttaText = time - currentTimeMillis;
            }
            else {
                ttaText = 0L;
            }
            this.setTTAText(ttaText);
            this.etaText.setText((CharSequence)this.maneuverDisplay.eta);
            this.etaAmPmText.setText((CharSequence)this.maneuverDisplay.etaAmPm);
            final float convertToMeters = DistanceConverter.convertToMeters(this.maneuverDisplay.totalDistance, this.maneuverDisplay.totalDistanceUnit);
            final float n2 = convertToMeters - DistanceConverter.convertToMeters(this.maneuverDisplay.totalDistanceRemaining, this.maneuverDisplay.totalDistanceRemainingUnit);
            int n3;
            if (convertToMeters > 0.0f && convertToMeters > n2) {
                n3 = (int)(n2 / convertToMeters * 100.0f);
            }
            else {
                n3 = 0;
            }
            this.etaProgressDrawable.setGaugeValue(n3);
            this.remainingDistanceText.setText((CharSequence)Float.toString(this.maneuverDisplay.totalDistanceRemaining));
            switch (this.maneuverDisplay.totalDistanceRemainingUnit) {
                case DISTANCE_METERS:
                    this.remainingDistanceUnitText.setText((CharSequence)this.metersLabel);
                    break;
                case DISTANCE_KMS:
                    this.remainingDistanceUnitText.setText((CharSequence)this.kiloMetersLabel);
                    break;
                case DISTANCE_MILES:
                    this.remainingDistanceUnitText.setText((CharSequence)this.milesLabel);
                    break;
                case DISTANCE_FEET:
                    this.remainingDistanceUnitText.setText((CharSequence)this.feetLabel);
                    break;
            }
        }
    }
}
