package com.navdy.hud.app.view;

import android.widget.TextView;
import android.os.Bundle;
import com.squareup.otto.Subscribe;
import android.graphics.drawable.Drawable;
import java.util.Date;
import java.util.Calendar;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.os.Looper;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.view.drawable.SmallAnalogClockDrawable;
import android.os.Handler;
import android.content.Context;
import com.squareup.otto.Bus;
import com.navdy.hud.app.view.drawable.AnalogClockDrawable;

public class ClockWidgetPresenter extends DashboardWidgetPresenter
{
    private static final int TIME_UPDATE_INTERVAL = 30000;
    private StringBuilder amPmMarker;
    private AnalogClockDrawable analogClockDrawable;
    private String analogClockWidgetName;
    private Bus bus;
    private String dateText;
    private int dayOfTheMonth;
    private int dayOfTheWeek;
    private String dayText;
    private String digitalClock2WidgetName;
    private String digitalClockWidgetName;
    private ClockType mClockType;
    private Context mContext;
    private Handler mHandler;
    private SmallAnalogClockDrawable smallAnalogClockDrawable;
    private TimeHelper timeHelper;
    private int timeHour;
    private int timeMinute;
    private int timeSeconds;
    private String timeText;
    private Runnable updateTimeRunnable;
    
    public ClockWidgetPresenter(final Context mContext, final ClockType mClockType) {
        this.mHandler = new Handler(Looper.getMainLooper());
        this.amPmMarker = new StringBuilder();
        this.mClockType = mClockType;
        this.mContext = mContext;
        this.timeHelper = RemoteDeviceManager.getInstance().getTimeHelper();
        this.bus = RemoteDeviceManager.getInstance().getBus();
        this.analogClockWidgetName = mContext.getResources().getString(R.string.widget_clock_analog);
        this.digitalClockWidgetName = mContext.getResources().getString(R.string.widget_clock_digital);
        this.digitalClock2WidgetName = mContext.getResources().getString(R.string.widget_clock_time_and_date);
        this.updateTimeRunnable = new Runnable() {
            @Override
            public void run() {
                ClockWidgetPresenter.this.updateTime();
                ClockWidgetPresenter.this.mHandler.postDelayed(ClockWidgetPresenter.this.updateTimeRunnable, (long)ClockWidgetPresenter.this.nextUpdateInterval());
            }
        };
        switch (mClockType) {
            case ANALOG:
                this.analogClockDrawable = new AnalogClockDrawable(mContext);
                break;
            case DIGITAL1:
                this.smallAnalogClockDrawable = new SmallAnalogClockDrawable(mContext);
                break;
        }
    }
    
    private int nextUpdateInterval() {
        int n = 30;
        final int n2 = (this.timeSeconds + 30) % 60;
        if (n2 >= 45) {
            n = 30 + (60 - n2);
        }
        else if (n2 <= 15) {
            n = 30 - n2;
        }
        return n * 1000;
    }
    
    private void updateTime() {
        final Calendar instance = Calendar.getInstance();
        final Date time = new Date();
        instance.setTime(time);
        this.timeMinute = instance.get(12);
        this.timeHour = instance.get(11);
        this.timeSeconds = instance.get(13);
        this.dayOfTheMonth = instance.get(5);
        this.dayOfTheWeek = instance.get(7);
        switch (this.mClockType) {
            case DIGITAL2:
                this.dateText = this.timeHelper.getDate();
                this.dayText = this.timeHelper.getDay();
                this.dateText = this.dateText.toUpperCase();
                this.dayText = this.dayText.toUpperCase();
            case DIGITAL1:
                this.timeText = this.timeHelper.formatTime(time, this.amPmMarker);
                break;
        }
        this.reDraw();
    }
    
    @Override
    public Drawable getDrawable() {
        Drawable drawable = null;
        switch (this.mClockType) {
            default:
                drawable = null;
                break;
            case ANALOG:
                drawable = this.analogClockDrawable;
                break;
            case DIGITAL1:
                drawable = this.smallAnalogClockDrawable;
                break;
        }
        return drawable;
    }
    
    @Override
    public String getWidgetIdentifier() {
        String s = null;
        switch (this.mClockType) {
            default:
                s = null;
                break;
            case ANALOG:
                s = "ANALOG_CLOCK_WIDGET";
                break;
            case DIGITAL1:
                s = "DIGITAL_CLOCK_WIDGET";
                break;
            case DIGITAL2:
                s = "DIGITAL_CLOCK_2_WIDGET";
                break;
        }
        return s;
    }
    
    @Override
    public String getWidgetName() {
        String s = null;
        switch (this.mClockType) {
            default:
                s = null;
                break;
            case ANALOG:
                s = this.analogClockWidgetName;
                break;
            case DIGITAL1:
                s = this.digitalClockWidgetName;
                break;
            case DIGITAL2:
                s = this.digitalClock2WidgetName;
                break;
        }
        return s;
    }
    
    @Override
    protected boolean isRegisteringToBusRequired() {
        return true;
    }
    
    @Subscribe
    public void onUpdateClock(final TimeHelper.UpdateClock updateClock) {
        this.updateTime();
    }
    
    @Override
    public void setView(final DashboardWidgetView dashboardWidgetView, final Bundle bundle) {
        if (dashboardWidgetView != null) {
            switch (this.mClockType) {
                case ANALOG:
                    dashboardWidgetView.setContentView(R.layout.smart_dash_widget_circular_content_layout);
                    break;
                case DIGITAL1:
                    dashboardWidgetView.setContentView(R.layout.digital_clock1_layout);
                    break;
                case DIGITAL2:
                    dashboardWidgetView.setContentView(R.layout.digital_clock2_layout);
                    break;
            }
            super.setView(dashboardWidgetView, bundle);
            this.mHandler.removeCallbacks(this.updateTimeRunnable);
            this.updateTime();
            this.mHandler.postDelayed(this.updateTimeRunnable, (long)this.nextUpdateInterval());
        }
        else {
            this.mHandler.removeCallbacks(this.updateTimeRunnable);
            super.setView(dashboardWidgetView, bundle);
        }
    }
    
    @Override
    protected void updateGauge() {
        if (this.mWidgetView != null) {
            switch (this.mClockType) {
                case ANALOG:
                    this.analogClockDrawable.setTime(this.dayOfTheMonth, this.timeHour, this.timeMinute, 0);
                    break;
                case DIGITAL2: {
                    final TextView textView = (TextView)this.mWidgetView.findViewById(R.id.txt_value);
                    final TextView textView2 = (TextView)this.mWidgetView.findViewById(R.id.txt_unit);
                    textView.setText((CharSequence)this.timeText);
                    textView2.setText((CharSequence)this.amPmMarker.toString());
                    ((TextView)this.mWidgetView.findViewById(R.id.txt_date)).setText((CharSequence)this.dateText);
                    ((TextView)this.mWidgetView.findViewById(R.id.txt_day)).setText((CharSequence)this.dayText);
                    break;
                }
                case DIGITAL1: {
                    this.smallAnalogClockDrawable.setTime(this.timeHour, this.timeMinute, this.timeSeconds);
                    final TextView textView3 = (TextView)this.mWidgetView.findViewById(R.id.txt_value);
                    ((TextView)this.mWidgetView.findViewById(R.id.txt_unit)).setText((CharSequence)this.amPmMarker.toString());
                    textView3.setText((CharSequence)this.timeText);
                    break;
                }
            }
        }
    }
    
    public enum ClockType
    {
        ANALOG, 
        DIGITAL1, 
        DIGITAL2;
    }
}
