package com.navdy.hud.app.view;

import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.text.TextUtils;
import com.navdy.hud.app.R;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Typeface;
import java.util.HashMap;
import android.widget.TextView;

public class FontTextView extends TextView
{
    private static HashMap<String, Typeface> mCachedTypeFaces;
    
    static {
        FontTextView.mCachedTypeFaces = new HashMap<String, Typeface>();
    }
    
    public FontTextView(final Context context) {
        this(context, null);
    }
    
    public FontTextView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public FontTextView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.FontTextView, n, 0);
        if (obtainStyledAttributes != null) {
            final String string = obtainStyledAttributes.getString(0);
            if (!TextUtils.isEmpty((CharSequence)string)) {
                this.setTypeface(createFromAsset(context.getAssets(), string));
            }
            obtainStyledAttributes.recycle();
        }
    }
    
    public static Typeface createFromAsset(final AssetManager assetManager, final String s) {
        Typeface fromAsset;
        if (FontTextView.mCachedTypeFaces.containsKey(s)) {
            fromAsset = FontTextView.mCachedTypeFaces.get(s);
        }
        else {
            fromAsset = Typeface.createFromAsset(assetManager, s);
            if (fromAsset != null) {
                FontTextView.mCachedTypeFaces.put(s, fromAsset);
            }
        }
        return fromAsset;
    }
}
