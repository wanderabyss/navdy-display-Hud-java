package com.navdy.hud.app.view;

import com.navdy.service.library.events.input.Gesture;
import com.navdy.service.library.events.input.GestureEvent;
import android.graphics.Rect;
import android.view.View;
import android.view.MotionEvent;
import android.view.View;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.animation.ObjectAnimator;
import android.animation.AnimatorSet;
import android.animation.Animator;
import android.util.AttributeSet;
import android.content.Context;
import com.navdy.service.library.log.Logger;
import android.view.animation.Animation;
import android.view.animation.Animation;
import com.navdy.hud.app.gesture.GestureDetector;
import com.navdy.hud.app.manager.InputManager;
import android.widget.RelativeLayout;

public abstract class ChoicesView extends RelativeLayout implements IInputHandler, GestureListener
{
    protected boolean animateItemOnExecution;
    protected boolean animateItemOnSelection;
    protected GestureDetector detector;
    protected Animation executeAnimation;
    protected int lastTouchX;
    protected int lastTouchY;
    private Animation$AnimationListener listener;
    protected Logger logger;
    protected int selectedItem;
    protected boolean wrapAround;
    
    public ChoicesView(final Context context) {
        this(context, null);
    }
    
    public ChoicesView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public ChoicesView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.logger = new Logger(this.getClass());
        this.selectedItem = -1;
        this.wrapAround = false;
        this.animateItemOnExecution = true;
        this.animateItemOnSelection = true;
        this.listener = (Animation$AnimationListener)new Animation$AnimationListener() {
            public void onAnimationEnd(final Animation animation) {
                ChoicesView.this.onExecuteItem(ChoicesView.this.selectedItem);
            }
            
            public void onAnimationRepeat(final Animation animation) {
            }
            
            public void onAnimationStart(final Animation animation) {
            }
        };
        this.detector = new GestureDetector((GestureDetector.GestureListener)this);
    }
    
    private Animator buildBounceAnim(final float n, final float n2) {
        final AnimatorSet set = new AnimatorSet();
        set.play((Animator)ObjectAnimator.ofFloat(this, "value", new float[] { n })).before((Animator)ObjectAnimator.ofFloat(this, "value", new float[] { n2 }));
        return (Animator)set;
    }
    
    private void moveSelectionLeft() {
        final int itemCount = this.getItemCount();
        if (this.selectedItem > 0 && itemCount > 0) {
            this.setSelectedItem((this.selectedItem - 1 + itemCount) % itemCount);
        }
    }
    
    private void moveSelectionRight() {
        if (this.selectedItem != -1) {
            final int itemCount = this.getItemCount();
            if (this.selectedItem + 1 < itemCount) {
                this.setSelectedItem((this.selectedItem + 1) % itemCount);
            }
        }
    }
    
    protected abstract void animateDown();
    
    protected abstract void animateUp();
    
    public void executeSelectedItem(final boolean b) {
        if (this.selectedItem != -1 && this.getItemCount() > 0) {
            this.logger.d("USER_STUDY executing option " + (this.selectedItem + 1));
            if (b) {
                if (this.executeAnimation == null) {
                    (this.executeAnimation = AnimationUtils.loadAnimation(this.getContext(), R.anim.press_button)).setAnimationListener(this.listener);
                }
                this.getItemAt(this.selectedItem).startAnimation(this.executeAnimation);
            }
            else {
                this.onExecuteItem(this.selectedItem);
            }
        }
    }
    
    public abstract View getItemAt(final int p0);
    
    public abstract int getItemCount();
    
    public int getSelectedItem() {
        return this.selectedItem;
    }
    
    public abstract float getValue();
    
    public int indexForValue(final float n) {
        final int itemCount = this.getItemCount();
        int n2;
        if (itemCount <= 1) {
            n2 = 0;
        }
        else {
            n2 = (int)((itemCount - 1) * n + 0.5f);
        }
        return n2;
    }
    
    public IInputHandler nextHandler() {
        return InputManager.nextContainingHandler((View)this);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.setOnTouchListener((View$OnTouchListener)new View$OnTouchListener() {
            public boolean onTouch(final View view, final MotionEvent motionEvent) {
                if (motionEvent.getAction() == 1) {
                    ChoicesView.this.lastTouchX = (int)motionEvent.getX();
                    ChoicesView.this.lastTouchY = (int)motionEvent.getY();
                }
                return false;
            }
        });
        this.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(View item) {
                final Rect rect = new Rect();
                final int[] array = new int[2];
                final int[] array2 = new int[2];
                ChoicesView.this.getLocationInWindow(array);
                int i = 0;
                while (i < ChoicesView.this.getItemCount()) {
                    item = ChoicesView.this.getItemAt(i);
                    ((View)item.getParent()).getLocationInWindow(array2);
                    final int lastTouchX = ChoicesView.this.lastTouchX;
                    final int n = array2[0];
                    final int n2 = array[0];
                    final int lastTouchY = ChoicesView.this.lastTouchY;
                    final int n3 = array2[1];
                    final int n4 = array[1];
                    item.getHitRect(rect);
                    if (rect.contains(lastTouchX - (n - n2), lastTouchY - (n3 - n4))) {
                        if (i == ChoicesView.this.getSelectedItem()) {
                            ChoicesView.this.executeSelectedItem(ChoicesView.this.animateItemOnExecution);
                            break;
                        }
                        ChoicesView.this.setSelectedItem(i);
                        break;
                    }
                    else {
                        ++i;
                    }
                }
            }
        });
    }
    
    public void onClick() {
        this.executeSelectedItem(this.animateItemOnExecution);
    }
    
    protected void onDeselectItem(final View view, final int n) {
    }
    
    public abstract void onExecuteItem(final int p0);
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        boolean b = true;
        this.detector.onGesture(gestureEvent);
        switch (gestureEvent.gesture) {
            case GESTURE_FINGER_UP:
            case GESTURE_HAND_UP:
                this.animateUp();
                break;
            case GESTURE_FINGER_DOWN:
            case GESTURE_HAND_DOWN:
                this.animateDown();
                this.setSelectedItem(-1);
                break;
            case GESTURE_SWIPE_LEFT:
                if (!this.wrapAround && this.selectedItem == 0) {
                    this.buildBounceAnim(0.0f, this.valueForIndex(0)).start();
                    return b;
                }
                this.moveSelectionLeft();
                return b;
            case GESTURE_SWIPE_RIGHT:
                if (!this.wrapAround && this.selectedItem + 1 == this.getItemCount()) {
                    this.buildBounceAnim(1.0f, this.valueForIndex(this.selectedItem)).start();
                    return b;
                }
                this.moveSelectionRight();
                return b;
        }
        b = false;
        return b;
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        boolean b;
        if (this.selectedItem == -1) {
            this.animateUp();
            b = true;
        }
        else {
            b = false;
            switch (customKeyEvent) {
                case LEFT:
                    this.moveSelectionLeft();
                    b = true;
                    break;
                case RIGHT:
                    this.moveSelectionRight();
                    b = true;
                    break;
                case SELECT:
                    this.executeSelectedItem(true);
                    b = true;
                    break;
            }
        }
        return b;
    }
    
    protected void onSelectItem(final View view, final int n) {
    }
    
    public void onTrackHand(final float value) {
        this.setValue(value);
    }
    
    public void setSelectedItem(final int selectedItem) {
        if (selectedItem != this.selectedItem) {
            String value;
            if (selectedItem < 0) {
                value = "none";
            }
            else {
                value = String.valueOf(selectedItem + 1);
            }
            this.logger.d("selecting option - " + value);
            View item = null;
            View item2 = null;
            if (this.selectedItem != -1) {
                item = this.getItemAt(this.selectedItem);
            }
            if (selectedItem != -1) {
                item2 = this.getItemAt(selectedItem);
            }
            if (item != null) {
                this.onDeselectItem(item, selectedItem);
            }
            final int selectedItem2 = this.selectedItem;
            this.selectedItem = selectedItem;
            if (item2 != null) {
                this.onSelectItem(item2, selectedItem2);
            }
            if (selectedItem != -1) {
                this.setValue(this.valueForIndex(selectedItem));
            }
        }
    }
    
    public abstract void setValue(final float p0);
    
    public float valueForIndex(final int n) {
        final int itemCount = this.getItemCount();
        float n2;
        if (itemCount <= 1) {
            n2 = 0.0f;
        }
        else {
            n2 = n / (itemCount - 1);
        }
        return n2;
    }
}
