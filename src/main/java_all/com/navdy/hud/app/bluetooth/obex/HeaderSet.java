package com.navdy.hud.app.bluetooth.obex;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.Calendar;

public final class HeaderSet
{
    public static final int APPLICATION_PARAMETER = 76;
    public static final int AUTH_CHALLENGE = 77;
    public static final int AUTH_RESPONSE = 78;
    public static final int BODY = 72;
    public static final int CONNECTION_ID = 203;
    public static final int COUNT = 192;
    public static final int DESCRIPTION = 5;
    public static final int END_OF_BODY = 73;
    public static final int HTTP = 71;
    public static final int LENGTH = 195;
    public static final int NAME = 1;
    public static final int OBJECT_CLASS = 79;
    public static final int SINGLE_RESPONSE_MODE = 151;
    public static final int SINGLE_RESPONSE_MODE_PARAMETER = 152;
    public static final int TARGET = 70;
    public static final int TIME_4_BYTE = 196;
    public static final int TIME_ISO_8601 = 68;
    public static final int TYPE = 66;
    public static final int WHO = 74;
    private byte[] mAppParam;
    public byte[] mAuthChall;
    public byte[] mAuthResp;
    private Calendar mByteTime;
    private Byte[] mByteUserDefined;
    public byte[] mConnectionID;
    private Long mCount;
    private String mDescription;
    private boolean mEmptyName;
    private byte[] mHttpHeader;
    private Long[] mIntegerUserDefined;
    private Calendar mIsoTime;
    private Long mLength;
    private String mName;
    private byte[] mObjectClass;
    private SecureRandom mRandom;
    private byte[][] mSequenceUserDefined;
    private Byte mSingleResponseMode;
    private Byte mSrmParam;
    private byte[] mTarget;
    private String mType;
    private String[] mUnicodeUserDefined;
    private byte[] mWho;
    byte[] nonce;
    public int responseCode;
    
    public HeaderSet() {
        this.mRandom = null;
        this.mUnicodeUserDefined = new String[16];
        this.mSequenceUserDefined = new byte[16][];
        this.mByteUserDefined = new Byte[16];
        this.mIntegerUserDefined = new Long[16];
        this.responseCode = -1;
    }
    
    public void createAuthenticationChallenge(final String s, final boolean b, final boolean b2) throws IOException {
        this.nonce = new byte[16];
        if (this.mRandom == null) {
            this.mRandom = new SecureRandom();
        }
        for (int i = 0; i < 16; ++i) {
            this.nonce[i] = (byte)this.mRandom.nextInt();
        }
        this.mAuthChall = ObexHelper.computeAuthenticationChallenge(this.nonce, s, b2, b);
    }
    
    public boolean getEmptyNameHeader() {
        return this.mEmptyName;
    }
    
    public Object getHeader(final int n) throws IOException {
        Object o = null;
        switch (n) {
            default:
                if (n >= 48 && n <= 63) {
                    o = this.mUnicodeUserDefined[n - 48];
                    break;
                }
                if (n >= 112 && n <= 127) {
                    o = this.mSequenceUserDefined[n - 112];
                    break;
                }
                if (n >= 176 && n <= 191) {
                    o = this.mByteUserDefined[n - 176];
                    break;
                }
                if (n >= 240 && n <= 255) {
                    o = this.mIntegerUserDefined[n - 240];
                    break;
                }
                throw new IllegalArgumentException("Invalid Header Identifier");
            case 192:
                o = this.mCount;
                break;
            case 1:
                o = this.mName;
                break;
            case 66:
                o = this.mType;
                break;
            case 195:
                o = this.mLength;
                break;
            case 68:
                o = this.mIsoTime;
                break;
            case 196:
                o = this.mByteTime;
                break;
            case 5:
                o = this.mDescription;
                break;
            case 70:
                o = this.mTarget;
                break;
            case 71:
                o = this.mHttpHeader;
                break;
            case 74:
                o = this.mWho;
                break;
            case 203:
                o = this.mConnectionID;
                break;
            case 79:
                o = this.mObjectClass;
                break;
            case 76:
                o = this.mAppParam;
                break;
            case 151:
                o = this.mSingleResponseMode;
                break;
            case 152:
                o = this.mSrmParam;
                break;
        }
        return o;
    }
    
    public int[] getHeaderList() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (this.mCount != null) {
            byteArrayOutputStream.write(192);
        }
        if (this.mName != null) {
            byteArrayOutputStream.write(1);
        }
        if (this.mType != null) {
            byteArrayOutputStream.write(66);
        }
        if (this.mLength != null) {
            byteArrayOutputStream.write(195);
        }
        if (this.mIsoTime != null) {
            byteArrayOutputStream.write(68);
        }
        if (this.mByteTime != null) {
            byteArrayOutputStream.write(196);
        }
        if (this.mDescription != null) {
            byteArrayOutputStream.write(5);
        }
        if (this.mTarget != null) {
            byteArrayOutputStream.write(70);
        }
        if (this.mHttpHeader != null) {
            byteArrayOutputStream.write(71);
        }
        if (this.mWho != null) {
            byteArrayOutputStream.write(74);
        }
        if (this.mAppParam != null) {
            byteArrayOutputStream.write(76);
        }
        if (this.mObjectClass != null) {
            byteArrayOutputStream.write(79);
        }
        if (this.mSingleResponseMode != null) {
            byteArrayOutputStream.write(151);
        }
        if (this.mSrmParam != null) {
            byteArrayOutputStream.write(152);
        }
        for (int i = 48; i < 64; ++i) {
            if (this.mUnicodeUserDefined[i - 48] != null) {
                byteArrayOutputStream.write(i);
            }
        }
        for (int j = 112; j < 128; ++j) {
            if (this.mSequenceUserDefined[j - 112] != null) {
                byteArrayOutputStream.write(j);
            }
        }
        for (int k = 176; k < 192; ++k) {
            if (this.mByteUserDefined[k - 176] != null) {
                byteArrayOutputStream.write(k);
            }
        }
        for (int l = 240; l < 256; ++l) {
            if (this.mIntegerUserDefined[l - 240] != null) {
                byteArrayOutputStream.write(l);
            }
        }
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();
        int[] array;
        if (byteArray == null || byteArray.length == 0) {
            array = null;
        }
        else {
            final int[] array2 = new int[byteArray.length];
            int n = 0;
            while (true) {
                array = array2;
                if (n >= byteArray.length) {
                    break;
                }
                array2[n] = (byteArray[n] & 0xFF);
                ++n;
            }
        }
        return array;
    }
    
    public int getResponseCode() throws IOException {
        if (this.responseCode == -1) {
            throw new IOException("May not be called on a server");
        }
        return this.responseCode;
    }
    
    public void setEmptyNameHeader() {
        this.mName = null;
        this.mEmptyName = true;
    }
    
    public void setHeader(final int n, final Object o) {
        switch (n) {
            default:
                if (n >= 48 && n <= 63) {
                    if (o != null && !(o instanceof String)) {
                        throw new IllegalArgumentException("Unicode String User Defined must be a String");
                    }
                    this.mUnicodeUserDefined[n - 48] = (String)o;
                    break;
                }
                else if (n >= 112 && n <= 127) {
                    if (o == null) {
                        this.mSequenceUserDefined[n - 112] = null;
                        break;
                    }
                    if (!(o instanceof byte[])) {
                        throw new IllegalArgumentException("Byte Sequence User Defined must be a byte array");
                    }
                    System.arraycopy(o, 0, this.mSequenceUserDefined[n - 112] = new byte[((byte[])o).length], 0, this.mSequenceUserDefined[n - 112].length);
                    break;
                }
                else if (n >= 176 && n <= 191) {
                    if (o != null && !(o instanceof Byte)) {
                        throw new IllegalArgumentException("ByteUser Defined must be a Byte");
                    }
                    this.mByteUserDefined[n - 176] = (Byte)o;
                    break;
                }
                else {
                    if (n < 240 || n > 255) {
                        throw new IllegalArgumentException("Invalid Header Identifier");
                    }
                    if (!(o instanceof Long)) {
                        if (o == null) {
                            this.mIntegerUserDefined[n - 240] = null;
                            break;
                        }
                        throw new IllegalArgumentException("Integer User Defined must be a Long");
                    }
                    else {
                        final long longValue = (long)o;
                        if (longValue < 0L || longValue > 4294967295L) {
                            throw new IllegalArgumentException("Integer User Defined must be between 0 and 0xFFFFFFFF");
                        }
                        this.mIntegerUserDefined[n - 240] = (Long)o;
                        break;
                    }
                }
                break;
            case 192:
                if (!(o instanceof Long)) {
                    if (o == null) {
                        this.mCount = null;
                        break;
                    }
                    throw new IllegalArgumentException("Count must be a Long");
                }
                else {
                    final long longValue2 = (long)o;
                    if (longValue2 < 0L || longValue2 > 4294967295L) {
                        throw new IllegalArgumentException("Count must be between 0 and 0xFFFFFFFF");
                    }
                    this.mCount = (Long)o;
                    break;
                }
                break;
            case 1:
                if (o != null && !(o instanceof String)) {
                    throw new IllegalArgumentException("Name must be a String");
                }
                this.mEmptyName = false;
                this.mName = (String)o;
                break;
            case 66:
                if (o != null && !(o instanceof String)) {
                    throw new IllegalArgumentException("Type must be a String");
                }
                this.mType = (String)o;
                break;
            case 195:
                if (!(o instanceof Long)) {
                    if (o == null) {
                        this.mLength = null;
                        break;
                    }
                    throw new IllegalArgumentException("Length must be a Long");
                }
                else {
                    final long longValue3 = (long)o;
                    if (longValue3 < 0L || longValue3 > 4294967295L) {
                        throw new IllegalArgumentException("Length must be between 0 and 0xFFFFFFFF");
                    }
                    this.mLength = (Long)o;
                    break;
                }
                break;
            case 68:
                if (o != null && !(o instanceof Calendar)) {
                    throw new IllegalArgumentException("Time ISO 8601 must be a Calendar");
                }
                this.mIsoTime = (Calendar)o;
                break;
            case 196:
                if (o != null && !(o instanceof Calendar)) {
                    throw new IllegalArgumentException("Time 4 Byte must be a Calendar");
                }
                this.mByteTime = (Calendar)o;
                break;
            case 5:
                if (o != null && !(o instanceof String)) {
                    throw new IllegalArgumentException("Description must be a String");
                }
                this.mDescription = (String)o;
                break;
            case 70:
                if (o == null) {
                    this.mTarget = null;
                    break;
                }
                if (!(o instanceof byte[])) {
                    throw new IllegalArgumentException("Target must be a byte array");
                }
                System.arraycopy(o, 0, this.mTarget = new byte[((byte[])o).length], 0, this.mTarget.length);
                break;
            case 71:
                if (o == null) {
                    this.mHttpHeader = null;
                    break;
                }
                if (!(o instanceof byte[])) {
                    throw new IllegalArgumentException("HTTP must be a byte array");
                }
                System.arraycopy(o, 0, this.mHttpHeader = new byte[((byte[])o).length], 0, this.mHttpHeader.length);
                break;
            case 74:
                if (o == null) {
                    this.mWho = null;
                    break;
                }
                if (!(o instanceof byte[])) {
                    throw new IllegalArgumentException("WHO must be a byte array");
                }
                System.arraycopy(o, 0, this.mWho = new byte[((byte[])o).length], 0, this.mWho.length);
                break;
            case 79:
                if (o == null) {
                    this.mObjectClass = null;
                    break;
                }
                if (!(o instanceof byte[])) {
                    throw new IllegalArgumentException("Object Class must be a byte array");
                }
                System.arraycopy(o, 0, this.mObjectClass = new byte[((byte[])o).length], 0, this.mObjectClass.length);
                break;
            case 76:
                if (o == null) {
                    this.mAppParam = null;
                    break;
                }
                if (!(o instanceof byte[])) {
                    throw new IllegalArgumentException("Application Parameter must be a byte array");
                }
                System.arraycopy(o, 0, this.mAppParam = new byte[((byte[])o).length], 0, this.mAppParam.length);
                break;
            case 151:
                if (o == null) {
                    this.mSingleResponseMode = null;
                    break;
                }
                if (!(o instanceof Byte)) {
                    throw new IllegalArgumentException("Single Response Mode must be a Byte");
                }
                this.mSingleResponseMode = (Byte)o;
                break;
            case 152:
                if (o == null) {
                    this.mSrmParam = null;
                    break;
                }
                if (!(o instanceof Byte)) {
                    throw new IllegalArgumentException("Single Response Mode Parameter must be a Byte");
                }
                this.mSrmParam = (Byte)o;
                break;
        }
    }
}
