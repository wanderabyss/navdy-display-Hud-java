package com.navdy.hud.app.bluetooth.obex;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;

public final class ServerOperation implements Operation, BaseStream
{
    private static final String TAG = "ServerOperation";
    private static final boolean V = false;
    public boolean finalBitSet;
    public boolean isAborted;
    private boolean mClosed;
    private String mExceptionString;
    private boolean mGetOperation;
    private boolean mHasBody;
    private InputStream mInput;
    private ServerRequestHandler mListener;
    private int mMaxPacketLength;
    private ServerSession mParent;
    private PrivateInputStream mPrivateInput;
    private PrivateOutputStream mPrivateOutput;
    private boolean mPrivateOutputOpen;
    private boolean mRequestFinished;
    private int mResponseSize;
    private boolean mSendBodyHeader;
    private boolean mSrmActive;
    private boolean mSrmEnabled;
    private boolean mSrmLocalWait;
    private boolean mSrmResponseSent;
    private boolean mSrmWaitingForRemote;
    private ObexTransport mTransport;
    public HeaderSet replyHeader;
    public HeaderSet requestHeader;
    
    public ServerOperation(final ServerSession mParent, final InputStream mInput, final int n, final int mMaxPacketLength, final ServerRequestHandler mListener) throws IOException {
        this.mSendBodyHeader = true;
        this.mSrmEnabled = false;
        this.mSrmActive = false;
        this.mSrmResponseSent = false;
        this.mSrmWaitingForRemote = true;
        this.mSrmLocalWait = false;
        this.isAborted = false;
        this.mParent = mParent;
        this.mInput = mInput;
        this.mMaxPacketLength = mMaxPacketLength;
        this.mClosed = false;
        this.requestHeader = new HeaderSet();
        this.replyHeader = new HeaderSet();
        this.mPrivateInput = new PrivateInputStream(this);
        this.mResponseSize = 3;
        this.mListener = mListener;
        this.mRequestFinished = false;
        this.mPrivateOutputOpen = false;
        this.mHasBody = false;
        this.mTransport = mParent.getTransport();
        if (n == 2 || n == 130) {
            this.mGetOperation = false;
            if ((n & 0x80) == 0x0) {
                this.finalBitSet = false;
            }
            else {
                this.finalBitSet = true;
                this.mRequestFinished = true;
            }
        }
        else {
            if (n != 3 && n != 131) {
                throw new IOException("ServerOperation can not handle such request");
            }
            this.mGetOperation = true;
            this.finalBitSet = false;
            if (n == 131) {
                this.mRequestFinished = true;
            }
        }
        final ObexPacket read = ObexPacket.read(n, this.mInput);
        if (read.mLength > ObexHelper.getMaxRxPacketSize(this.mTransport)) {
            this.mParent.sendResponse(206, null);
            throw new IOException("Packet received was too large. Length: " + read.mLength + " maxLength: " + ObexHelper.getMaxRxPacketSize(this.mTransport));
        }
        if (read.mLength > 3) {
            if (!this.handleObexPacket(read)) {
                return;
            }
            if (!this.mHasBody) {
                while (!this.mGetOperation && !this.finalBitSet) {
                    this.sendReply(144);
                    if (this.mPrivateInput.available() > 0) {
                        break;
                    }
                }
            }
        }
        while (!this.mGetOperation && !this.finalBitSet && this.mPrivateInput.available() == 0) {
            this.sendReply(144);
            if (this.mPrivateInput.available() > 0) {
                break;
            }
        }
        while (this.mGetOperation && !this.mRequestFinished) {
            this.sendReply(144);
        }
    }
    
    private void checkForSrmWait(final int n) {
        if (!this.mSrmEnabled) {
            return;
        }
        if (n != 3 && n != 131) {
            if (n != 2) {
                return;
            }
        }
        try {
            this.mSrmWaitingForRemote = false;
            final Byte b = (Byte)this.requestHeader.getHeader(152);
            if (b != null && b == 1) {
                this.mSrmWaitingForRemote = true;
                this.requestHeader.setHeader(152, null);
            }
        }
        catch (IOException ex) {}
    }
    
    private void checkSrmRemoteAbort() throws IOException {
        if (this.mInput.available() > 0) {
            final ObexPacket read = ObexPacket.read(this.mInput);
            if (read.mHeaderId == 255) {
                this.handleRemoteAbort();
            }
            else {
                Log.w("ServerOperation", "Received unexpected request from client - discarding...\n   headerId: " + read.mHeaderId + " length: " + read.mLength);
            }
        }
    }
    
    private boolean handleObexPacket(final ObexPacket obexPacket) throws IOException {
        boolean b = false;
        final byte[] updateRequestHeaders = this.updateRequestHeaders(obexPacket);
        if (updateRequestHeaders != null) {
            this.mHasBody = true;
        }
        if (this.mListener.getConnectionId() != -1L && this.requestHeader.mConnectionID != null) {
            this.mListener.setConnectionId(ObexHelper.convertToLong(this.requestHeader.mConnectionID));
        }
        else {
            this.mListener.setConnectionId(1L);
        }
        Label_0136: {
            if (this.requestHeader.mAuthResp == null) {
                break Label_0136;
            }
            if (this.mParent.handleAuthResp(this.requestHeader.mAuthResp)) {
                this.requestHeader.mAuthResp = null;
                break Label_0136;
            }
            this.mExceptionString = "Authentication Failed";
            this.mParent.sendResponse(193, null);
            this.mClosed = true;
            this.requestHeader.mAuthResp = null;
            return b;
        }
        if (this.requestHeader.mAuthChall != null) {
            this.mParent.handleAuthChall(this.requestHeader);
            this.replyHeader.mAuthResp = new byte[this.requestHeader.mAuthResp.length];
            System.arraycopy(this.requestHeader.mAuthResp, 0, this.replyHeader.mAuthResp, 0, this.replyHeader.mAuthResp.length);
            this.requestHeader.mAuthResp = null;
            this.requestHeader.mAuthChall = null;
        }
        if (updateRequestHeaders != null) {
            this.mPrivateInput.writeBytes(updateRequestHeaders, 1);
        }
        b = true;
        return b;
    }
    
    private void handleRemoteAbort() throws IOException {
        this.mParent.sendResponse(160, null);
        this.mClosed = true;
        this.isAborted = true;
        this.mExceptionString = "Abort Received";
        throw new IOException("Abort Received");
    }
    
    private byte[] updateRequestHeaders(final ObexPacket obexPacket) throws IOException {
        byte[] updateHeaderSet = null;
        if (obexPacket.mPayload != null) {
            updateHeaderSet = ObexHelper.updateHeaderSet(this.requestHeader, obexPacket.mPayload);
        }
        final Byte b = (Byte)this.requestHeader.getHeader(151);
        if (this.mTransport.isSrmSupported() && b != null && b == 1) {
            this.mSrmEnabled = true;
        }
        this.checkForSrmWait(obexPacket.mHeaderId);
        if (!this.mSrmWaitingForRemote && this.mSrmEnabled) {
            this.mSrmActive = true;
        }
        return updateHeaderSet;
    }
    
    @Override
    public void abort() throws IOException {
        throw new IOException("Called from a server");
    }
    
    @Override
    public void close() throws IOException {
        this.ensureOpen();
        this.mClosed = true;
    }
    
    @Override
    public boolean continueOperation(final boolean b, boolean b2) throws IOException {
        while (true) {
            b2 = true;
            Label_0086: {
                synchronized (this) {
                    if (this.mGetOperation) {
                        break Label_0086;
                    }
                    if (!this.finalBitSet) {
                        boolean b3;
                        if (b) {
                            this.sendReply(144);
                            b3 = b2;
                        }
                        else {
                            if (this.mResponseSize <= 3 && this.mPrivateOutput.size() <= 0) {
                                return false;
                            }
                            this.sendReply(144);
                            b3 = b2;
                        }
                        return b3;
                    }
                    return false;
                }
                return false;
                b3 = false;
                return b3;
            }
            this.sendReply(144);
            return b2;
        }
    }
    
    @Override
    public void ensureNotDone() throws IOException {
    }
    
    @Override
    public void ensureOpen() throws IOException {
        if (this.mExceptionString != null) {
            throw new IOException(this.mExceptionString);
        }
        if (this.mClosed) {
            throw new IOException("Operation has already ended");
        }
    }
    
    @Override
    public String getEncoding() {
        return null;
    }
    
    @Override
    public int getHeaderLength() {
        final long connectionId = this.mListener.getConnectionId();
        if (connectionId == -1L) {
            this.replyHeader.mConnectionID = null;
        }
        else {
            this.replyHeader.mConnectionID = ObexHelper.convertToByteArray(connectionId);
        }
        return ObexHelper.createHeader(this.replyHeader, false).length;
    }
    
    @Override
    public long getLength() {
        long longValue = -1L;
        try {
            final Long n = (Long)this.requestHeader.getHeader(195);
            if (n != null) {
                longValue = n;
            }
            return longValue;
        }
        catch (IOException ex) {
            return longValue;
        }
        return longValue;
    }
    
    @Override
    public int getMaxPacketSize() {
        return this.mMaxPacketLength - 6 - this.getHeaderLength();
    }
    
    @Override
    public HeaderSet getReceivedHeader() throws IOException {
        this.ensureOpen();
        return this.requestHeader;
    }
    
    @Override
    public int getResponseCode() throws IOException {
        throw new IOException("Called from a server");
    }
    
    @Override
    public String getType() {
        try {
            return (String)this.requestHeader.getHeader(66);
        }
        catch (IOException ex) {
            return null;
        }
    }
    
    public boolean isValidBody() {
        return this.mHasBody;
    }
    
    @Override
    public void noBodyHeader() {
        this.mSendBodyHeader = false;
    }
    
    @Override
    public DataInputStream openDataInputStream() throws IOException {
        return new DataInputStream(this.openInputStream());
    }
    
    @Override
    public DataOutputStream openDataOutputStream() throws IOException {
        return new DataOutputStream(this.openOutputStream());
    }
    
    @Override
    public InputStream openInputStream() throws IOException {
        this.ensureOpen();
        return this.mPrivateInput;
    }
    
    @Override
    public OutputStream openOutputStream() throws IOException {
        this.ensureOpen();
        if (this.mPrivateOutputOpen) {
            throw new IOException("no more input streams available, stream already opened");
        }
        if (!this.mRequestFinished) {
            throw new IOException("no  output streams available ,request not finished");
        }
        if (this.mPrivateOutput == null) {
            this.mPrivateOutput = new PrivateOutputStream(this, this.getMaxPacketSize());
        }
        this.mPrivateOutputOpen = true;
        return this.mPrivateOutput;
    }
    
    @Override
    public void sendHeaders(final HeaderSet set) throws IOException {
        this.ensureOpen();
        if (set == null) {
            throw new IOException("Headers may not be null");
        }
        final int[] headerList = set.getHeaderList();
        if (headerList != null) {
            for (int i = 0; i < headerList.length; ++i) {
                this.replyHeader.setHeader(headerList[i], set.getHeader(headerList[i]));
            }
        }
    }
    
    public boolean sendReply(int mHeaderId) throws IOException {
        boolean b3 = false;
    Label_0175_Outer:
        while (true) {
            Object o = null;
            boolean b = false;
            boolean b2 = false;
            int n = 0;
            int n2 = 0;
            byte[] header = null;
            int n3 = 0;
            Label_0340: {
            Label_0321:
                while (true) {
                    int n4 = 0;
                Label_0286:
                    while (true) {
                        final long connectionId;
                        synchronized (this) {
                            o = new ByteArrayOutputStream();
                            b = false;
                            b2 = false;
                            n = 0;
                            connectionId = this.mListener.getConnectionId();
                            if (connectionId == -1L) {
                                this.replyHeader.mConnectionID = null;
                                n2 = n;
                                if (this.mSrmEnabled) {
                                    n2 = n;
                                    if (!this.mSrmResponseSent) {
                                        this.replyHeader.setHeader(151, (byte)1);
                                        n2 = 1;
                                    }
                                }
                                if (this.mSrmEnabled && !this.mGetOperation && this.mSrmLocalWait) {
                                    this.replyHeader.setHeader(151, (byte)1);
                                }
                                header = ObexHelper.createHeader(this.replyHeader, true);
                                n = -1;
                                n3 = -1;
                                if (this.mPrivateOutput != null) {
                                    n = (n3 = this.mPrivateOutput.size());
                                }
                                if (header.length + 3 <= this.mMaxPacketLength) {
                                    break Label_0340;
                                }
                                n3 = 0;
                                n4 = 0;
                                if (n3 == header.length) {
                                    break Label_0321;
                                }
                                n3 = ObexHelper.findHeaderEnd(header, n4, this.mMaxPacketLength - 3);
                                if (n3 == -1) {
                                    this.mClosed = true;
                                    if (this.mPrivateInput != null) {
                                        this.mPrivateInput.close();
                                    }
                                    if (this.mPrivateOutput != null) {
                                        this.mPrivateOutput.close();
                                    }
                                    this.mParent.sendResponse(208, null);
                                    throw new IOException("OBEX Packet exceeds max packet size");
                                }
                                break Label_0286;
                            }
                        }
                        this.replyHeader.mConnectionID = ObexHelper.convertToByteArray(connectionId);
                        continue Label_0175_Outer;
                    }
                    o = new byte[n3 - n4];
                    System.arraycopy(header, n4, o, 0, ((ByteArrayOutputStream)o).length);
                    this.mParent.sendResponse(mHeaderId, (byte[])o);
                    n4 = n3;
                    continue;
                }
                b3 = (n > 0);
                break;
            }
            ((OutputStream)o).write(header);
            if (this.mGetOperation && mHeaderId == 160) {
                this.finalBitSet = true;
            }
            boolean b4 = b2;
            int n5 = b ? 1 : 0;
            if (this.mSrmActive) {
                if (!this.mGetOperation && mHeaderId == 144 && this.mSrmResponseSent) {
                    n5 = 1;
                    b4 = b2;
                }
                else if (this.mGetOperation && !this.mRequestFinished && this.mSrmResponseSent) {
                    n5 = 1;
                    b4 = b2;
                }
                else {
                    b4 = b2;
                    n5 = (b ? 1 : 0);
                    if (this.mGetOperation) {
                        b4 = b2;
                        n5 = (b ? 1 : 0);
                        if (this.mRequestFinished) {
                            b4 = true;
                            n5 = (b ? 1 : 0);
                        }
                    }
                }
            }
            if (n2 != 0) {
                this.mSrmResponseSent = true;
            }
            if ((this.finalBitSet || header.length < this.mMaxPacketLength - 20) && n > 0) {
                int n6;
                if ((n6 = n) > this.mMaxPacketLength - header.length - 6) {
                    n6 = this.mMaxPacketLength - header.length - 6;
                }
                final byte[] bytes = this.mPrivateOutput.readBytes(n6);
                if (this.finalBitSet || this.mPrivateOutput.isClosed()) {
                    if (this.mSendBodyHeader) {
                        ((ByteArrayOutputStream)o).write(73);
                        final int n7 = n6 + 3;
                        ((ByteArrayOutputStream)o).write(n7 >> 8);
                        ((ByteArrayOutputStream)o).write(n7);
                        ((OutputStream)o).write(bytes);
                    }
                }
                else if (this.mSendBodyHeader) {
                    ((ByteArrayOutputStream)o).write(72);
                    final int n8 = n6 + 3;
                    ((ByteArrayOutputStream)o).write(n8 >> 8);
                    ((ByteArrayOutputStream)o).write(n8);
                    ((OutputStream)o).write(bytes);
                }
            }
            if (this.finalBitSet && mHeaderId == 160 && n3 <= 0 && this.mSendBodyHeader) {
                ((ByteArrayOutputStream)o).write(73);
                ((ByteArrayOutputStream)o).write(0);
                ((ByteArrayOutputStream)o).write(3);
            }
            if (n5 == 0) {
                this.mResponseSize = 3;
                this.mParent.sendResponse(mHeaderId, ((ByteArrayOutputStream)o).toByteArray());
            }
            if (mHeaderId == 144) {
                if (this.mGetOperation && b4) {
                    this.checkSrmRemoteAbort();
                }
                else {
                    final ObexPacket read = ObexPacket.read(this.mInput);
                    mHeaderId = read.mHeaderId;
                    if (mHeaderId != 2 && mHeaderId != 130 && mHeaderId != 3 && mHeaderId != 131) {
                        if (mHeaderId != 255) {
                            this.mParent.sendResponse(192, null);
                            this.mClosed = true;
                            this.mExceptionString = "Bad Request Received";
                            throw new IOException("Bad Request Received");
                        }
                        this.handleRemoteAbort();
                    }
                    else {
                        if (mHeaderId == 130) {
                            this.finalBitSet = true;
                        }
                        else if (mHeaderId == 131) {
                            this.mRequestFinished = true;
                        }
                        if (read.mLength > ObexHelper.getMaxRxPacketSize(this.mTransport)) {
                            this.mParent.sendResponse(206, null);
                            throw new IOException("Packet received was too large");
                        }
                        if ((read.mLength > 3 || (this.mSrmEnabled && read.mLength == 3)) && !this.handleObexPacket(read)) {
                            b3 = false;
                            break;
                        }
                    }
                }
                b3 = true;
                break;
            }
            b3 = false;
            break;
        }
        // monitorexit(this)
        return b3;
    }
    
    @Override
    public void streamClosed(final boolean b) throws IOException {
    }
}
