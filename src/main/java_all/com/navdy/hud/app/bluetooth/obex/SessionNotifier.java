package com.navdy.hud.app.bluetooth.obex;

import java.io.IOException;

public interface SessionNotifier
{
    ObexSession acceptAndOpen(final ServerRequestHandler p0) throws IOException;
    
    ObexSession acceptAndOpen(final ServerRequestHandler p0, final Authenticator p1) throws IOException;
}
