package com.navdy.hud.app.bluetooth.obex;

import java.util.Date;
import java.util.TimeZone;
import java.util.Calendar;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import java.io.IOException;

public final class ObexHelper
{
    public static final int BASE_PACKET_LENGTH = 3;
    public static final int LOWER_LIMIT_MAX_PACKET_SIZE = 255;
    public static final int MAX_CLIENT_PACKET_SIZE = 64512;
    public static final int MAX_PACKET_SIZE_INT = 65534;
    public static final int OBEX_AUTH_REALM_CHARSET_ASCII = 0;
    public static final int OBEX_AUTH_REALM_CHARSET_ISO_8859_1 = 1;
    public static final int OBEX_AUTH_REALM_CHARSET_ISO_8859_2 = 2;
    public static final int OBEX_AUTH_REALM_CHARSET_ISO_8859_3 = 3;
    public static final int OBEX_AUTH_REALM_CHARSET_ISO_8859_4 = 4;
    public static final int OBEX_AUTH_REALM_CHARSET_ISO_8859_5 = 5;
    public static final int OBEX_AUTH_REALM_CHARSET_ISO_8859_6 = 6;
    public static final int OBEX_AUTH_REALM_CHARSET_ISO_8859_7 = 7;
    public static final int OBEX_AUTH_REALM_CHARSET_ISO_8859_8 = 8;
    public static final int OBEX_AUTH_REALM_CHARSET_ISO_8859_9 = 9;
    public static final int OBEX_AUTH_REALM_CHARSET_UNICODE = 255;
    public static final int OBEX_OPCODE_ABORT = 255;
    public static final int OBEX_OPCODE_CONNECT = 128;
    public static final int OBEX_OPCODE_DISCONNECT = 129;
    public static final int OBEX_OPCODE_FINAL_BIT_MASK = 128;
    public static final int OBEX_OPCODE_GET = 3;
    public static final int OBEX_OPCODE_GET_FINAL = 131;
    public static final int OBEX_OPCODE_PUT = 2;
    public static final int OBEX_OPCODE_PUT_FINAL = 130;
    public static final int OBEX_OPCODE_RESERVED = 4;
    public static final int OBEX_OPCODE_RESERVED_FINAL = 132;
    public static final int OBEX_OPCODE_SETPATH = 133;
    public static final byte OBEX_SRMP_WAIT = 1;
    public static final byte OBEX_SRM_DISABLE = 0;
    public static final byte OBEX_SRM_ENABLE = 1;
    public static final byte OBEX_SRM_SUPPORT = 2;
    private static final String TAG = "ObexHelper";
    public static final boolean VDBG = false;
    
    public static byte[] computeAuthenticationChallenge(final byte[] array, final String s, final boolean b, final boolean b2) throws IOException {
        if (array.length != 16) {
            throw new IllegalArgumentException("Nonce must be 16 bytes long");
        }
        byte[] array2;
        if (s == null) {
            array2 = new byte[21];
        }
        else {
            if (s.length() >= 255) {
                throw new IllegalArgumentException("Realm must be less then 255 bytes");
            }
            final byte[] array3 = new byte[s.length() + 24];
            array3[21] = 2;
            array3[22] = (byte)(s.length() + 1);
            array3[23] = 1;
            System.arraycopy(s.getBytes("ISO8859_1"), 0, array3, 24, s.length());
            array2 = array3;
        }
        array2[0] = 0;
        array2[1] = 16;
        System.arraycopy(array, 0, array2, 2, 16);
        array2[18] = 1;
        array2[19] = 1;
        array2[20] = 0;
        if (!b) {
            array2[20] |= 0x2;
        }
        if (b2) {
            array2[20] |= 0x1;
        }
        return array2;
    }
    
    public static byte[] computeMd5Hash(byte[] digest) {
        try {
            digest = MessageDigest.getInstance("MD5").digest(digest);
            return digest;
        }
        catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    public static byte[] convertToByteArray(final long n) {
        return new byte[] { (byte)(n >> 24 & 0xFFL), (byte)(n >> 16 & 0xFFL), (byte)(n >> 8 & 0xFFL), (byte)(0xFFL & n) };
    }
    
    public static long convertToLong(final byte[] array) {
        long n = 0L;
        long n2 = 0L;
        for (int i = array.length - 1; i >= 0; --i) {
            long n4;
            final long n3 = n4 = array[i];
            if (n3 < 0L) {
                n4 = n3 + 256L;
            }
            n |= n4 << (int)n2;
            n2 += 8L;
        }
        return n;
    }
    
    public static String convertToUnicode(final byte[] array, final boolean b) {
        String s;
        if (array == null || array.length == 0) {
            s = null;
        }
        else {
            final int length = array.length;
            if (length % 2 != 0) {
                throw new IllegalArgumentException("Byte array not of a valid form");
            }
            int n2;
            final int n = n2 = length >> 1;
            if (b) {
                n2 = n - 1;
            }
            final char[] array2 = new char[n2];
            for (int i = 0; i < n2; ++i) {
                final byte b2 = array[i * 2];
                final byte b3 = array[i * 2 + 1];
                int n3;
                if ((n3 = b2) < 0) {
                    n3 = b2 + 256;
                }
                int n4;
                if ((n4 = b3) < 0) {
                    n4 = b3 + 256;
                }
                if (n3 == 0 && n4 == 0) {
                    s = new String(array2, 0, i);
                    return s;
                }
                array2[i] = (char)(n3 << 8 | n4);
            }
            s = new String(array2);
        }
        return s;
    }
    
    public static byte[] convertToUnicodeByteArray(final String s) {
        byte[] array;
        if (s == null) {
            array = null;
        }
        else {
            final char[] charArray = s.toCharArray();
            array = new byte[charArray.length * 2 + 2];
            for (int i = 0; i < charArray.length; ++i) {
                array[i * 2] = (byte)(charArray[i] >> 8);
                array[i * 2 + 1] = (byte)charArray[i];
            }
            array[array.length - 2] = 0;
            array[array.length - 1] = 0;
        }
        return array;
    }
    
    public static byte[] createHeader(final HeaderSet p0, final boolean p1) {
        // 
         This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: newarray        B
        //     3: astore_2       
        //     4: new             Ljava/io/ByteArrayOutputStream;
        //     7: dup            
        //     8: invokespecial   java/io/ByteArrayOutputStream.<init>:()V
        //    11: astore_3       
        //    12: aload_0        
        //    13: getfield        com/navdy/hud/app/bluetooth/obex/HeaderSet.mConnectionID:[B
        //    16: ifnull          42
        //    19: aload_0        
        //    20: bipush          70
        //    22: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getHeader:(I)Ljava/lang/Object;
        //    25: ifnonnull       42
        //    28: aload_3        
        //    29: bipush          -53
        //    31: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //    34: aload_3        
        //    35: aload_0        
        //    36: getfield        com/navdy/hud/app/bluetooth/obex/HeaderSet.mConnectionID:[B
        //    39: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //    42: aload_0        
        //    43: sipush          192
        //    46: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getHeader:(I)Ljava/lang/Object;
        //    49: checkcast       Ljava/lang/Long;
        //    52: astore          4
        //    54: aload           4
        //    56: ifnull          89
        //    59: aload_3        
        //    60: bipush          -64
        //    62: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //    65: aload_3        
        //    66: aload           4
        //    68: invokevirtual   java/lang/Long.longValue:()J
        //    71: invokestatic    com/navdy/hud/app/bluetooth/obex/ObexHelper.convertToByteArray:(J)[B
        //    74: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //    77: iload_1        
        //    78: ifeq            89
        //    81: aload_0        
        //    82: sipush          192
        //    85: aconst_null    
        //    86: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.setHeader:(ILjava/lang/Object;)V
        //    89: aload_0        
        //    90: iconst_1       
        //    91: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getHeader:(I)Ljava/lang/Object;
        //    94: checkcast       Ljava/lang/String;
        //    97: astore          4
        //    99: aload           4
        //   101: ifnull          370
        //   104: aload_3        
        //   105: iconst_1       
        //   106: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //   109: aload           4
        //   111: invokestatic    com/navdy/hud/app/bluetooth/obex/ObexHelper.convertToUnicodeByteArray:(Ljava/lang/String;)[B
        //   114: astore          4
        //   116: aload           4
        //   118: arraylength    
        //   119: iconst_3       
        //   120: iadd           
        //   121: istore          5
        //   123: aload_2        
        //   124: iconst_0       
        //   125: iload           5
        //   127: bipush          8
        //   129: ishr           
        //   130: sipush          255
        //   133: iand           
        //   134: i2b            
        //   135: i2b            
        //   136: bastore        
        //   137: aload_2        
        //   138: iconst_1       
        //   139: iload           5
        //   141: sipush          255
        //   144: iand           
        //   145: i2b            
        //   146: i2b            
        //   147: bastore        
        //   148: aload_3        
        //   149: aload_2        
        //   150: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //   153: aload_3        
        //   154: aload           4
        //   156: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //   159: iload_1        
        //   160: ifeq            169
        //   163: aload_0        
        //   164: iconst_1       
        //   165: aconst_null    
        //   166: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.setHeader:(ILjava/lang/Object;)V
        //   169: aload_0        
        //   170: bipush          66
        //   172: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getHeader:(I)Ljava/lang/Object;
        //   175: checkcast       Ljava/lang/String;
        //   178: astore          4
        //   180: aload           4
        //   182: ifnull          259
        //   185: aload_3        
        //   186: bipush          66
        //   188: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //   191: aload           4
        //   193: ldc             "ISO8859_1"
        //   195: invokevirtual   java/lang/String.getBytes:(Ljava/lang/String;)[B
        //   198: astore          4
        //   200: aload           4
        //   202: arraylength    
        //   203: iconst_4       
        //   204: iadd           
        //   205: istore          5
        //   207: aload_2        
        //   208: iconst_0       
        //   209: iload           5
        //   211: bipush          8
        //   213: ishr           
        //   214: sipush          255
        //   217: iand           
        //   218: i2b            
        //   219: i2b            
        //   220: bastore        
        //   221: aload_2        
        //   222: iconst_1       
        //   223: iload           5
        //   225: sipush          255
        //   228: iand           
        //   229: i2b            
        //   230: i2b            
        //   231: bastore        
        //   232: aload_3        
        //   233: aload_2        
        //   234: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //   237: aload_3        
        //   238: aload           4
        //   240: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //   243: aload_3        
        //   244: iconst_0       
        //   245: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //   248: iload_1        
        //   249: ifeq            259
        //   252: aload_0        
        //   253: bipush          66
        //   255: aconst_null    
        //   256: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.setHeader:(ILjava/lang/Object;)V
        //   259: aload_0        
        //   260: sipush          195
        //   263: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getHeader:(I)Ljava/lang/Object;
        //   266: checkcast       Ljava/lang/Long;
        //   269: astore          4
        //   271: aload           4
        //   273: ifnull          306
        //   276: aload_3        
        //   277: bipush          -61
        //   279: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //   282: aload_3        
        //   283: aload           4
        //   285: invokevirtual   java/lang/Long.longValue:()J
        //   288: invokestatic    com/navdy/hud/app/bluetooth/obex/ObexHelper.convertToByteArray:(J)[B
        //   291: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //   294: iload_1        
        //   295: ifeq            306
        //   298: aload_0        
        //   299: sipush          195
        //   302: aconst_null    
        //   303: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.setHeader:(ILjava/lang/Object;)V
        //   306: aload_0        
        //   307: bipush          68
        //   309: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getHeader:(I)Ljava/lang/Object;
        //   312: checkcast       Ljava/util/Calendar;
        //   315: astore          6
        //   317: aload           6
        //   319: ifnull          697
        //   322: new             Ljava/lang/StringBuffer;
        //   325: astore          4
        //   327: aload           4
        //   329: invokespecial   java/lang/StringBuffer.<init>:()V
        //   332: aload           6
        //   334: iconst_1       
        //   335: invokevirtual   java/util/Calendar.get:(I)I
        //   338: istore          7
        //   340: iload           7
        //   342: istore          5
        //   344: iload           5
        //   346: sipush          1000
        //   349: if_icmpge       427
        //   352: aload           4
        //   354: ldc             "0"
        //   356: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   359: pop            
        //   360: iload           5
        //   362: bipush          10
        //   364: imul           
        //   365: istore          5
        //   367: goto            344
        //   370: aload_0        
        //   371: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getEmptyNameHeader:()Z
        //   374: ifeq            169
        //   377: aload_3        
        //   378: iconst_1       
        //   379: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //   382: aload_2        
        //   383: iconst_0       
        //   384: iconst_0       
        //   385: i2b            
        //   386: bastore        
        //   387: aload_2        
        //   388: iconst_1       
        //   389: iconst_3       
        //   390: i2b            
        //   391: bastore        
        //   392: aload_3        
        //   393: aload_2        
        //   394: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //   397: goto            169
        //   400: astore_0       
        //   401: aload_3        
        //   402: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
        //   405: astore_0       
        //   406: aload_3        
        //   407: invokevirtual   java/io/ByteArrayOutputStream.close:()V
        //   410: aload_0        
        //   411: areturn        
        //   412: astore_0       
        //   413: aload_0        
        //   414: athrow         
        //   415: astore_0       
        //   416: aload_3        
        //   417: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
        //   420: pop            
        //   421: aload_3        
        //   422: invokevirtual   java/io/ByteArrayOutputStream.close:()V
        //   425: aload_0        
        //   426: athrow         
        //   427: aload           4
        //   429: iload           7
        //   431: invokevirtual   java/lang/StringBuffer.append:(I)Ljava/lang/StringBuffer;
        //   434: pop            
        //   435: aload           6
        //   437: iconst_2       
        //   438: invokevirtual   java/util/Calendar.get:(I)I
        //   441: istore          5
        //   443: iload           5
        //   445: bipush          10
        //   447: if_icmpge       458
        //   450: aload           4
        //   452: ldc             "0"
        //   454: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   457: pop            
        //   458: aload           4
        //   460: iload           5
        //   462: invokevirtual   java/lang/StringBuffer.append:(I)Ljava/lang/StringBuffer;
        //   465: pop            
        //   466: aload           6
        //   468: iconst_5       
        //   469: invokevirtual   java/util/Calendar.get:(I)I
        //   472: istore          5
        //   474: iload           5
        //   476: bipush          10
        //   478: if_icmpge       489
        //   481: aload           4
        //   483: ldc             "0"
        //   485: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   488: pop            
        //   489: aload           4
        //   491: iload           5
        //   493: invokevirtual   java/lang/StringBuffer.append:(I)Ljava/lang/StringBuffer;
        //   496: pop            
        //   497: aload           4
        //   499: ldc             "T"
        //   501: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   504: pop            
        //   505: aload           6
        //   507: bipush          11
        //   509: invokevirtual   java/util/Calendar.get:(I)I
        //   512: istore          5
        //   514: iload           5
        //   516: bipush          10
        //   518: if_icmpge       529
        //   521: aload           4
        //   523: ldc             "0"
        //   525: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   528: pop            
        //   529: aload           4
        //   531: iload           5
        //   533: invokevirtual   java/lang/StringBuffer.append:(I)Ljava/lang/StringBuffer;
        //   536: pop            
        //   537: aload           6
        //   539: bipush          12
        //   541: invokevirtual   java/util/Calendar.get:(I)I
        //   544: istore          5
        //   546: iload           5
        //   548: bipush          10
        //   550: if_icmpge       561
        //   553: aload           4
        //   555: ldc             "0"
        //   557: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   560: pop            
        //   561: aload           4
        //   563: iload           5
        //   565: invokevirtual   java/lang/StringBuffer.append:(I)Ljava/lang/StringBuffer;
        //   568: pop            
        //   569: aload           6
        //   571: bipush          13
        //   573: invokevirtual   java/util/Calendar.get:(I)I
        //   576: istore          5
        //   578: iload           5
        //   580: bipush          10
        //   582: if_icmpge       593
        //   585: aload           4
        //   587: ldc             "0"
        //   589: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   592: pop            
        //   593: aload           4
        //   595: iload           5
        //   597: invokevirtual   java/lang/StringBuffer.append:(I)Ljava/lang/StringBuffer;
        //   600: pop            
        //   601: aload           6
        //   603: invokevirtual   java/util/Calendar.getTimeZone:()Ljava/util/TimeZone;
        //   606: invokevirtual   java/util/TimeZone.getID:()Ljava/lang/String;
        //   609: ldc             "UTC"
        //   611: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   614: ifeq            625
        //   617: aload           4
        //   619: ldc             "Z"
        //   621: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   624: pop            
        //   625: aload           4
        //   627: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   630: ldc             "ISO8859_1"
        //   632: invokevirtual   java/lang/String.getBytes:(Ljava/lang/String;)[B
        //   635: astore          4
        //   637: aload           4
        //   639: arraylength    
        //   640: iconst_3       
        //   641: iadd           
        //   642: istore          5
        //   644: aload_2        
        //   645: iconst_0       
        //   646: iload           5
        //   648: bipush          8
        //   650: ishr           
        //   651: sipush          255
        //   654: iand           
        //   655: i2b            
        //   656: i2b            
        //   657: bastore        
        //   658: aload_2        
        //   659: iconst_1       
        //   660: iload           5
        //   662: sipush          255
        //   665: iand           
        //   666: i2b            
        //   667: i2b            
        //   668: bastore        
        //   669: aload_3        
        //   670: bipush          68
        //   672: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //   675: aload_3        
        //   676: aload_2        
        //   677: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //   680: aload_3        
        //   681: aload           4
        //   683: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //   686: iload_1        
        //   687: ifeq            697
        //   690: aload_0        
        //   691: bipush          68
        //   693: aconst_null    
        //   694: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.setHeader:(ILjava/lang/Object;)V
        //   697: aload_0        
        //   698: sipush          196
        //   701: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getHeader:(I)Ljava/lang/Object;
        //   704: checkcast       Ljava/util/Calendar;
        //   707: astore          4
        //   709: aload           4
        //   711: ifnull          752
        //   714: aload_3        
        //   715: sipush          196
        //   718: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //   721: aload_3        
        //   722: aload           4
        //   724: invokevirtual   java/util/Calendar.getTime:()Ljava/util/Date;
        //   727: invokevirtual   java/util/Date.getTime:()J
        //   730: ldc2_w          1000
        //   733: ldiv           
        //   734: invokestatic    com/navdy/hud/app/bluetooth/obex/ObexHelper.convertToByteArray:(J)[B
        //   737: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //   740: iload_1        
        //   741: ifeq            752
        //   744: aload_0        
        //   745: sipush          196
        //   748: aconst_null    
        //   749: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.setHeader:(ILjava/lang/Object;)V
        //   752: aload_0        
        //   753: iconst_5       
        //   754: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getHeader:(I)Ljava/lang/Object;
        //   757: checkcast       Ljava/lang/String;
        //   760: astore          4
        //   762: aload           4
        //   764: ifnull          832
        //   767: aload_3        
        //   768: iconst_5       
        //   769: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //   772: aload           4
        //   774: invokestatic    com/navdy/hud/app/bluetooth/obex/ObexHelper.convertToUnicodeByteArray:(Ljava/lang/String;)[B
        //   777: astore          4
        //   779: aload           4
        //   781: arraylength    
        //   782: iconst_3       
        //   783: iadd           
        //   784: istore          5
        //   786: aload_2        
        //   787: iconst_0       
        //   788: iload           5
        //   790: bipush          8
        //   792: ishr           
        //   793: sipush          255
        //   796: iand           
        //   797: i2b            
        //   798: i2b            
        //   799: bastore        
        //   800: aload_2        
        //   801: iconst_1       
        //   802: iload           5
        //   804: sipush          255
        //   807: iand           
        //   808: i2b            
        //   809: i2b            
        //   810: bastore        
        //   811: aload_3        
        //   812: aload_2        
        //   813: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //   816: aload_3        
        //   817: aload           4
        //   819: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //   822: iload_1        
        //   823: ifeq            832
        //   826: aload_0        
        //   827: iconst_5       
        //   828: aconst_null    
        //   829: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.setHeader:(ILjava/lang/Object;)V
        //   832: aload_0        
        //   833: bipush          70
        //   835: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getHeader:(I)Ljava/lang/Object;
        //   838: checkcast       [B
        //   841: astore          4
        //   843: aload           4
        //   845: ifnull          908
        //   848: aload_3        
        //   849: bipush          70
        //   851: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //   854: aload           4
        //   856: arraylength    
        //   857: iconst_3       
        //   858: iadd           
        //   859: istore          5
        //   861: aload_2        
        //   862: iconst_0       
        //   863: iload           5
        //   865: bipush          8
        //   867: ishr           
        //   868: sipush          255
        //   871: iand           
        //   872: i2b            
        //   873: i2b            
        //   874: bastore        
        //   875: aload_2        
        //   876: iconst_1       
        //   877: iload           5
        //   879: sipush          255
        //   882: iand           
        //   883: i2b            
        //   884: i2b            
        //   885: bastore        
        //   886: aload_3        
        //   887: aload_2        
        //   888: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //   891: aload_3        
        //   892: aload           4
        //   894: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //   897: iload_1        
        //   898: ifeq            908
        //   901: aload_0        
        //   902: bipush          70
        //   904: aconst_null    
        //   905: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.setHeader:(ILjava/lang/Object;)V
        //   908: aload_0        
        //   909: bipush          71
        //   911: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getHeader:(I)Ljava/lang/Object;
        //   914: checkcast       [B
        //   917: astore          4
        //   919: aload           4
        //   921: ifnull          984
        //   924: aload_3        
        //   925: bipush          71
        //   927: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //   930: aload           4
        //   932: arraylength    
        //   933: iconst_3       
        //   934: iadd           
        //   935: istore          5
        //   937: aload_2        
        //   938: iconst_0       
        //   939: iload           5
        //   941: bipush          8
        //   943: ishr           
        //   944: sipush          255
        //   947: iand           
        //   948: i2b            
        //   949: i2b            
        //   950: bastore        
        //   951: aload_2        
        //   952: iconst_1       
        //   953: iload           5
        //   955: sipush          255
        //   958: iand           
        //   959: i2b            
        //   960: i2b            
        //   961: bastore        
        //   962: aload_3        
        //   963: aload_2        
        //   964: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //   967: aload_3        
        //   968: aload           4
        //   970: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //   973: iload_1        
        //   974: ifeq            984
        //   977: aload_0        
        //   978: bipush          71
        //   980: aconst_null    
        //   981: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.setHeader:(ILjava/lang/Object;)V
        //   984: aload_0        
        //   985: bipush          74
        //   987: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getHeader:(I)Ljava/lang/Object;
        //   990: checkcast       [B
        //   993: astore          4
        //   995: aload           4
        //   997: ifnull          1060
        //  1000: aload_3        
        //  1001: bipush          74
        //  1003: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //  1006: aload           4
        //  1008: arraylength    
        //  1009: iconst_3       
        //  1010: iadd           
        //  1011: istore          5
        //  1013: aload_2        
        //  1014: iconst_0       
        //  1015: iload           5
        //  1017: bipush          8
        //  1019: ishr           
        //  1020: sipush          255
        //  1023: iand           
        //  1024: i2b            
        //  1025: i2b            
        //  1026: bastore        
        //  1027: aload_2        
        //  1028: iconst_1       
        //  1029: iload           5
        //  1031: sipush          255
        //  1034: iand           
        //  1035: i2b            
        //  1036: i2b            
        //  1037: bastore        
        //  1038: aload_3        
        //  1039: aload_2        
        //  1040: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //  1043: aload_3        
        //  1044: aload           4
        //  1046: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //  1049: iload_1        
        //  1050: ifeq            1060
        //  1053: aload_0        
        //  1054: bipush          74
        //  1056: aconst_null    
        //  1057: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.setHeader:(ILjava/lang/Object;)V
        //  1060: aload_0        
        //  1061: bipush          76
        //  1063: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getHeader:(I)Ljava/lang/Object;
        //  1066: checkcast       [B
        //  1069: astore          4
        //  1071: aload           4
        //  1073: ifnull          1136
        //  1076: aload_3        
        //  1077: bipush          76
        //  1079: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //  1082: aload           4
        //  1084: arraylength    
        //  1085: iconst_3       
        //  1086: iadd           
        //  1087: istore          5
        //  1089: aload_2        
        //  1090: iconst_0       
        //  1091: iload           5
        //  1093: bipush          8
        //  1095: ishr           
        //  1096: sipush          255
        //  1099: iand           
        //  1100: i2b            
        //  1101: i2b            
        //  1102: bastore        
        //  1103: aload_2        
        //  1104: iconst_1       
        //  1105: iload           5
        //  1107: sipush          255
        //  1110: iand           
        //  1111: i2b            
        //  1112: i2b            
        //  1113: bastore        
        //  1114: aload_3        
        //  1115: aload_2        
        //  1116: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //  1119: aload_3        
        //  1120: aload           4
        //  1122: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //  1125: iload_1        
        //  1126: ifeq            1136
        //  1129: aload_0        
        //  1130: bipush          76
        //  1132: aconst_null    
        //  1133: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.setHeader:(ILjava/lang/Object;)V
        //  1136: aload_0        
        //  1137: bipush          79
        //  1139: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getHeader:(I)Ljava/lang/Object;
        //  1142: checkcast       [B
        //  1145: astore          4
        //  1147: aload           4
        //  1149: ifnull          1212
        //  1152: aload_3        
        //  1153: bipush          79
        //  1155: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //  1158: aload           4
        //  1160: arraylength    
        //  1161: iconst_3       
        //  1162: iadd           
        //  1163: istore          5
        //  1165: aload_2        
        //  1166: iconst_0       
        //  1167: iload           5
        //  1169: bipush          8
        //  1171: ishr           
        //  1172: sipush          255
        //  1175: iand           
        //  1176: i2b            
        //  1177: i2b            
        //  1178: bastore        
        //  1179: aload_2        
        //  1180: iconst_1       
        //  1181: iload           5
        //  1183: sipush          255
        //  1186: iand           
        //  1187: i2b            
        //  1188: i2b            
        //  1189: bastore        
        //  1190: aload_3        
        //  1191: aload_2        
        //  1192: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //  1195: aload_3        
        //  1196: aload           4
        //  1198: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //  1201: iload_1        
        //  1202: ifeq            1212
        //  1205: aload_0        
        //  1206: bipush          79
        //  1208: aconst_null    
        //  1209: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.setHeader:(ILjava/lang/Object;)V
        //  1212: iconst_0       
        //  1213: istore          5
        //  1215: iload           5
        //  1217: bipush          16
        //  1219: if_icmpge       1527
        //  1222: aload_0        
        //  1223: iload           5
        //  1225: bipush          48
        //  1227: iadd           
        //  1228: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getHeader:(I)Ljava/lang/Object;
        //  1231: checkcast       Ljava/lang/String;
        //  1234: astore          4
        //  1236: aload           4
        //  1238: ifnull          1315
        //  1241: aload_3        
        //  1242: iload           5
        //  1244: i2b            
        //  1245: bipush          48
        //  1247: iadd           
        //  1248: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //  1251: aload           4
        //  1253: invokestatic    com/navdy/hud/app/bluetooth/obex/ObexHelper.convertToUnicodeByteArray:(Ljava/lang/String;)[B
        //  1256: astore          4
        //  1258: aload           4
        //  1260: arraylength    
        //  1261: iconst_3       
        //  1262: iadd           
        //  1263: istore          7
        //  1265: aload_2        
        //  1266: iconst_0       
        //  1267: iload           7
        //  1269: bipush          8
        //  1271: ishr           
        //  1272: sipush          255
        //  1275: iand           
        //  1276: i2b            
        //  1277: i2b            
        //  1278: bastore        
        //  1279: aload_2        
        //  1280: iconst_1       
        //  1281: iload           7
        //  1283: sipush          255
        //  1286: iand           
        //  1287: i2b            
        //  1288: i2b            
        //  1289: bastore        
        //  1290: aload_3        
        //  1291: aload_2        
        //  1292: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //  1295: aload_3        
        //  1296: aload           4
        //  1298: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //  1301: iload_1        
        //  1302: ifeq            1315
        //  1305: aload_0        
        //  1306: iload           5
        //  1308: bipush          48
        //  1310: iadd           
        //  1311: aconst_null    
        //  1312: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.setHeader:(ILjava/lang/Object;)V
        //  1315: aload_0        
        //  1316: iload           5
        //  1318: bipush          112
        //  1320: iadd           
        //  1321: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getHeader:(I)Ljava/lang/Object;
        //  1324: checkcast       [B
        //  1327: astore          4
        //  1329: aload           4
        //  1331: ifnull          1401
        //  1334: aload_3        
        //  1335: iload           5
        //  1337: i2b            
        //  1338: bipush          112
        //  1340: iadd           
        //  1341: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //  1344: aload           4
        //  1346: arraylength    
        //  1347: iconst_3       
        //  1348: iadd           
        //  1349: istore          7
        //  1351: aload_2        
        //  1352: iconst_0       
        //  1353: iload           7
        //  1355: bipush          8
        //  1357: ishr           
        //  1358: sipush          255
        //  1361: iand           
        //  1362: i2b            
        //  1363: i2b            
        //  1364: bastore        
        //  1365: aload_2        
        //  1366: iconst_1       
        //  1367: iload           7
        //  1369: sipush          255
        //  1372: iand           
        //  1373: i2b            
        //  1374: i2b            
        //  1375: bastore        
        //  1376: aload_3        
        //  1377: aload_2        
        //  1378: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //  1381: aload_3        
        //  1382: aload           4
        //  1384: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //  1387: iload_1        
        //  1388: ifeq            1401
        //  1391: aload_0        
        //  1392: iload           5
        //  1394: bipush          112
        //  1396: iadd           
        //  1397: aconst_null    
        //  1398: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.setHeader:(ILjava/lang/Object;)V
        //  1401: aload_0        
        //  1402: iload           5
        //  1404: sipush          176
        //  1407: iadd           
        //  1408: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getHeader:(I)Ljava/lang/Object;
        //  1411: checkcast       Ljava/lang/Byte;
        //  1414: astore          4
        //  1416: aload           4
        //  1418: ifnull          1456
        //  1421: aload_3        
        //  1422: iload           5
        //  1424: i2b            
        //  1425: sipush          176
        //  1428: iadd           
        //  1429: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //  1432: aload_3        
        //  1433: aload           4
        //  1435: invokevirtual   java/lang/Byte.byteValue:()B
        //  1438: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //  1441: iload_1        
        //  1442: ifeq            1456
        //  1445: aload_0        
        //  1446: iload           5
        //  1448: sipush          176
        //  1451: iadd           
        //  1452: aconst_null    
        //  1453: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.setHeader:(ILjava/lang/Object;)V
        //  1456: aload_0        
        //  1457: iload           5
        //  1459: sipush          240
        //  1462: iadd           
        //  1463: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getHeader:(I)Ljava/lang/Object;
        //  1466: checkcast       Ljava/lang/Long;
        //  1469: astore          4
        //  1471: aload           4
        //  1473: ifnull          1514
        //  1476: aload_3        
        //  1477: iload           5
        //  1479: i2b            
        //  1480: sipush          240
        //  1483: iadd           
        //  1484: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //  1487: aload_3        
        //  1488: aload           4
        //  1490: invokevirtual   java/lang/Long.longValue:()J
        //  1493: invokestatic    com/navdy/hud/app/bluetooth/obex/ObexHelper.convertToByteArray:(J)[B
        //  1496: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //  1499: iload_1        
        //  1500: ifeq            1514
        //  1503: aload_0        
        //  1504: iload           5
        //  1506: sipush          240
        //  1509: iadd           
        //  1510: aconst_null    
        //  1511: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.setHeader:(ILjava/lang/Object;)V
        //  1514: iinc            5, 1
        //  1517: goto            1215
        //  1520: astore_0       
        //  1521: aload_0        
        //  1522: athrow         
        //  1523: astore_0       
        //  1524: goto            401
        //  1527: aload_0        
        //  1528: getfield        com/navdy/hud/app/bluetooth/obex/HeaderSet.mAuthChall:[B
        //  1531: ifnull          1596
        //  1534: aload_3        
        //  1535: bipush          77
        //  1537: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //  1540: aload_0        
        //  1541: getfield        com/navdy/hud/app/bluetooth/obex/HeaderSet.mAuthChall:[B
        //  1544: arraylength    
        //  1545: iconst_3       
        //  1546: iadd           
        //  1547: istore          5
        //  1549: aload_2        
        //  1550: iconst_0       
        //  1551: iload           5
        //  1553: bipush          8
        //  1555: ishr           
        //  1556: sipush          255
        //  1559: iand           
        //  1560: i2b            
        //  1561: i2b            
        //  1562: bastore        
        //  1563: aload_2        
        //  1564: iconst_1       
        //  1565: iload           5
        //  1567: sipush          255
        //  1570: iand           
        //  1571: i2b            
        //  1572: i2b            
        //  1573: bastore        
        //  1574: aload_3        
        //  1575: aload_2        
        //  1576: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //  1579: aload_3        
        //  1580: aload_0        
        //  1581: getfield        com/navdy/hud/app/bluetooth/obex/HeaderSet.mAuthChall:[B
        //  1584: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //  1587: iload_1        
        //  1588: ifeq            1596
        //  1591: aload_0        
        //  1592: aconst_null    
        //  1593: putfield        com/navdy/hud/app/bluetooth/obex/HeaderSet.mAuthChall:[B
        //  1596: aload_0        
        //  1597: getfield        com/navdy/hud/app/bluetooth/obex/HeaderSet.mAuthResp:[B
        //  1600: ifnull          1665
        //  1603: aload_3        
        //  1604: bipush          78
        //  1606: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //  1609: aload_0        
        //  1610: getfield        com/navdy/hud/app/bluetooth/obex/HeaderSet.mAuthResp:[B
        //  1613: arraylength    
        //  1614: iconst_3       
        //  1615: iadd           
        //  1616: istore          5
        //  1618: aload_2        
        //  1619: iconst_0       
        //  1620: iload           5
        //  1622: bipush          8
        //  1624: ishr           
        //  1625: sipush          255
        //  1628: iand           
        //  1629: i2b            
        //  1630: i2b            
        //  1631: bastore        
        //  1632: aload_2        
        //  1633: iconst_1       
        //  1634: iload           5
        //  1636: sipush          255
        //  1639: iand           
        //  1640: i2b            
        //  1641: i2b            
        //  1642: bastore        
        //  1643: aload_3        
        //  1644: aload_2        
        //  1645: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //  1648: aload_3        
        //  1649: aload_0        
        //  1650: getfield        com/navdy/hud/app/bluetooth/obex/HeaderSet.mAuthResp:[B
        //  1653: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //  1656: iload_1        
        //  1657: ifeq            1665
        //  1660: aload_0        
        //  1661: aconst_null    
        //  1662: putfield        com/navdy/hud/app/bluetooth/obex/HeaderSet.mAuthResp:[B
        //  1665: aload_0        
        //  1666: sipush          151
        //  1669: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getHeader:(I)Ljava/lang/Object;
        //  1672: checkcast       Ljava/lang/Byte;
        //  1675: astore_2       
        //  1676: aload_2        
        //  1677: ifnull          1706
        //  1680: aload_3        
        //  1681: bipush          -105
        //  1683: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //  1686: aload_3        
        //  1687: aload_2        
        //  1688: invokevirtual   java/lang/Byte.byteValue:()B
        //  1691: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //  1694: iload_1        
        //  1695: ifeq            1706
        //  1698: aload_0        
        //  1699: sipush          151
        //  1702: aconst_null    
        //  1703: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.setHeader:(ILjava/lang/Object;)V
        //  1706: aload_0        
        //  1707: sipush          152
        //  1710: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.getHeader:(I)Ljava/lang/Object;
        //  1713: checkcast       Ljava/lang/Byte;
        //  1716: astore_2       
        //  1717: aload_2        
        //  1718: ifnull          1747
        //  1721: aload_3        
        //  1722: bipush          -104
        //  1724: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //  1727: aload_3        
        //  1728: aload_2        
        //  1729: invokevirtual   java/lang/Byte.byteValue:()B
        //  1732: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //  1735: iload_1        
        //  1736: ifeq            1747
        //  1739: aload_0        
        //  1740: sipush          152
        //  1743: aconst_null    
        //  1744: invokevirtual   com/navdy/hud/app/bluetooth/obex/HeaderSet.setHeader:(ILjava/lang/Object;)V
        //  1747: aload_3        
        //  1748: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
        //  1751: astore_0       
        //  1752: aload_3        
        //  1753: invokevirtual   java/io/ByteArrayOutputStream.close:()V
        //  1756: goto            410
        //  1759: astore_3       
        //  1760: goto            410
        //  1763: astore_3       
        //  1764: goto            410
        //  1767: astore_3       
        //  1768: goto            425
        //  1771: astore_0       
        //  1772: goto            416
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                  
        //  -----  -----  -----  -----  --------------------------------------
        //  12     42     400    401    Ljava/io/IOException;
        //  12     42     415    416    Any
        //  42     54     400    401    Ljava/io/IOException;
        //  42     54     415    416    Any
        //  59     77     400    401    Ljava/io/IOException;
        //  59     77     415    416    Any
        //  81     89     400    401    Ljava/io/IOException;
        //  81     89     415    416    Any
        //  89     99     400    401    Ljava/io/IOException;
        //  89     99     415    416    Any
        //  104    123    400    401    Ljava/io/IOException;
        //  104    123    415    416    Any
        //  148    159    400    401    Ljava/io/IOException;
        //  148    159    415    416    Any
        //  163    169    400    401    Ljava/io/IOException;
        //  163    169    415    416    Any
        //  169    180    400    401    Ljava/io/IOException;
        //  169    180    415    416    Any
        //  185    191    400    401    Ljava/io/IOException;
        //  185    191    415    416    Any
        //  191    200    412    415    Ljava/io/UnsupportedEncodingException;
        //  191    200    400    401    Ljava/io/IOException;
        //  191    200    415    416    Any
        //  200    207    400    401    Ljava/io/IOException;
        //  200    207    415    416    Any
        //  232    248    400    401    Ljava/io/IOException;
        //  232    248    415    416    Any
        //  252    259    400    401    Ljava/io/IOException;
        //  252    259    415    416    Any
        //  259    271    400    401    Ljava/io/IOException;
        //  259    271    415    416    Any
        //  276    294    400    401    Ljava/io/IOException;
        //  276    294    415    416    Any
        //  298    306    400    401    Ljava/io/IOException;
        //  298    306    415    416    Any
        //  306    317    400    401    Ljava/io/IOException;
        //  306    317    415    416    Any
        //  322    332    400    401    Ljava/io/IOException;
        //  322    332    415    416    Any
        //  332    340    1523   1527   Ljava/io/IOException;
        //  332    340    1771   1775   Any
        //  352    360    1523   1527   Ljava/io/IOException;
        //  352    360    1771   1775   Any
        //  370    382    400    401    Ljava/io/IOException;
        //  370    382    415    416    Any
        //  392    397    400    401    Ljava/io/IOException;
        //  392    397    415    416    Any
        //  406    410    1763   1767   Ljava/lang/Exception;
        //  413    415    400    401    Ljava/io/IOException;
        //  413    415    415    416    Any
        //  421    425    1767   1771   Ljava/lang/Exception;
        //  427    443    1523   1527   Ljava/io/IOException;
        //  427    443    1771   1775   Any
        //  450    458    1523   1527   Ljava/io/IOException;
        //  450    458    1771   1775   Any
        //  458    474    1523   1527   Ljava/io/IOException;
        //  458    474    1771   1775   Any
        //  481    489    1523   1527   Ljava/io/IOException;
        //  481    489    1771   1775   Any
        //  489    514    1523   1527   Ljava/io/IOException;
        //  489    514    1771   1775   Any
        //  521    529    1523   1527   Ljava/io/IOException;
        //  521    529    1771   1775   Any
        //  529    546    1523   1527   Ljava/io/IOException;
        //  529    546    1771   1775   Any
        //  553    561    1523   1527   Ljava/io/IOException;
        //  553    561    1771   1775   Any
        //  561    578    1523   1527   Ljava/io/IOException;
        //  561    578    1771   1775   Any
        //  585    593    1523   1527   Ljava/io/IOException;
        //  585    593    1771   1775   Any
        //  593    625    1523   1527   Ljava/io/IOException;
        //  593    625    1771   1775   Any
        //  625    637    1520   1523   Ljava/io/UnsupportedEncodingException;
        //  625    637    1523   1527   Ljava/io/IOException;
        //  625    637    1771   1775   Any
        //  637    644    1523   1527   Ljava/io/IOException;
        //  637    644    1771   1775   Any
        //  669    686    1523   1527   Ljava/io/IOException;
        //  669    686    1771   1775   Any
        //  690    697    1523   1527   Ljava/io/IOException;
        //  690    697    1771   1775   Any
        //  697    709    400    401    Ljava/io/IOException;
        //  697    709    415    416    Any
        //  714    740    400    401    Ljava/io/IOException;
        //  714    740    415    416    Any
        //  744    752    400    401    Ljava/io/IOException;
        //  744    752    415    416    Any
        //  752    762    400    401    Ljava/io/IOException;
        //  752    762    415    416    Any
        //  767    786    400    401    Ljava/io/IOException;
        //  767    786    415    416    Any
        //  811    822    400    401    Ljava/io/IOException;
        //  811    822    415    416    Any
        //  826    832    400    401    Ljava/io/IOException;
        //  826    832    415    416    Any
        //  832    843    400    401    Ljava/io/IOException;
        //  832    843    415    416    Any
        //  848    861    400    401    Ljava/io/IOException;
        //  848    861    415    416    Any
        //  886    897    400    401    Ljava/io/IOException;
        //  886    897    415    416    Any
        //  901    908    400    401    Ljava/io/IOException;
        //  901    908    415    416    Any
        //  908    919    400    401    Ljava/io/IOException;
        //  908    919    415    416    Any
        //  924    937    400    401    Ljava/io/IOException;
        //  924    937    415    416    Any
        //  962    973    400    401    Ljava/io/IOException;
        //  962    973    415    416    Any
        //  977    984    400    401    Ljava/io/IOException;
        //  977    984    415    416    Any
        //  984    995    400    401    Ljava/io/IOException;
        //  984    995    415    416    Any
        //  1000   1013   400    401    Ljava/io/IOException;
        //  1000   1013   415    416    Any
        //  1038   1049   400    401    Ljava/io/IOException;
        //  1038   1049   415    416    Any
        //  1053   1060   400    401    Ljava/io/IOException;
        //  1053   1060   415    416    Any
        //  1060   1071   400    401    Ljava/io/IOException;
        //  1060   1071   415    416    Any
        //  1076   1089   400    401    Ljava/io/IOException;
        //  1076   1089   415    416    Any
        //  1114   1125   400    401    Ljava/io/IOException;
        //  1114   1125   415    416    Any
        //  1129   1136   400    401    Ljava/io/IOException;
        //  1129   1136   415    416    Any
        //  1136   1147   400    401    Ljava/io/IOException;
        //  1136   1147   415    416    Any
        //  1152   1165   400    401    Ljava/io/IOException;
        //  1152   1165   415    416    Any
        //  1190   1201   400    401    Ljava/io/IOException;
        //  1190   1201   415    416    Any
        //  1205   1212   400    401    Ljava/io/IOException;
        //  1205   1212   415    416    Any
        //  1222   1236   400    401    Ljava/io/IOException;
        //  1222   1236   415    416    Any
        //  1241   1265   400    401    Ljava/io/IOException;
        //  1241   1265   415    416    Any
        //  1290   1301   400    401    Ljava/io/IOException;
        //  1290   1301   415    416    Any
        //  1305   1315   400    401    Ljava/io/IOException;
        //  1305   1315   415    416    Any
        //  1315   1329   400    401    Ljava/io/IOException;
        //  1315   1329   415    416    Any
        //  1334   1351   400    401    Ljava/io/IOException;
        //  1334   1351   415    416    Any
        //  1376   1387   400    401    Ljava/io/IOException;
        //  1376   1387   415    416    Any
        //  1391   1401   400    401    Ljava/io/IOException;
        //  1391   1401   415    416    Any
        //  1401   1416   400    401    Ljava/io/IOException;
        //  1401   1416   415    416    Any
        //  1421   1441   400    401    Ljava/io/IOException;
        //  1421   1441   415    416    Any
        //  1445   1456   400    401    Ljava/io/IOException;
        //  1445   1456   415    416    Any
        //  1456   1471   400    401    Ljava/io/IOException;
        //  1456   1471   415    416    Any
        //  1476   1499   400    401    Ljava/io/IOException;
        //  1476   1499   415    416    Any
        //  1503   1514   400    401    Ljava/io/IOException;
        //  1503   1514   415    416    Any
        //  1521   1523   1523   1527   Ljava/io/IOException;
        //  1521   1523   1771   1775   Any
        //  1527   1549   400    401    Ljava/io/IOException;
        //  1527   1549   415    416    Any
        //  1574   1587   400    401    Ljava/io/IOException;
        //  1574   1587   415    416    Any
        //  1591   1596   400    401    Ljava/io/IOException;
        //  1591   1596   415    416    Any
        //  1596   1618   400    401    Ljava/io/IOException;
        //  1596   1618   415    416    Any
        //  1643   1656   400    401    Ljava/io/IOException;
        //  1643   1656   415    416    Any
        //  1660   1665   400    401    Ljava/io/IOException;
        //  1660   1665   415    416    Any
        //  1665   1676   400    401    Ljava/io/IOException;
        //  1665   1676   415    416    Any
        //  1680   1694   400    401    Ljava/io/IOException;
        //  1680   1694   415    416    Any
        //  1698   1706   400    401    Ljava/io/IOException;
        //  1698   1706   415    416    Any
        //  1706   1717   400    401    Ljava/io/IOException;
        //  1706   1717   415    416    Any
        //  1721   1735   400    401    Ljava/io/IOException;
        //  1721   1735   415    416    Any
        //  1739   1747   400    401    Ljava/io/IOException;
        //  1739   1747   415    416    Any
        //  1752   1756   1759   1763   Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 976, Size: 976
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static int findHeaderEnd(final byte[] array, int length, final int n) {
        int n2 = 0;
        int n3 = -1;
        int n4 = length;
        while (n2 < n && n4 < array.length) {
            int n5;
            if (array[n4] < 0) {
                n5 = array[n4] + 256;
            }
            else {
                n5 = array[n4];
            }
            n3 = n2;
            switch (n5 & 0xC0) {
                default:
                    continue;
                case 0:
                case 64: {
                    final int n6 = n4 + 1;
                    int n7;
                    if (array[n6] < 0) {
                        n7 = array[n6] + 256;
                    }
                    else {
                        n7 = array[n6];
                    }
                    final int n8 = n6 + 1;
                    int n9;
                    if (array[n8] < 0) {
                        n9 = array[n8] + 256;
                    }
                    else {
                        n9 = array[n8];
                    }
                    final int n10 = (n7 << 8) + n9 - 3;
                    n4 = n8 + 1 + n10;
                    n2 += n10 + 3;
                    continue;
                }
                case 128:
                    n4 = n4 + 1 + 1;
                    n2 += 2;
                    continue;
                case 192:
                    n4 += 5;
                    n2 += 5;
                    continue;
            }
        }
        if (n3 == 0) {
            if (n2 < n) {
                length = array.length;
            }
            else {
                length = -1;
            }
        }
        else {
            length += n3;
        }
        return length;
    }
    
    public static int findTag(final byte b, final byte[] array) {
        int n;
        if (array == null) {
            n = -1;
        }
        else {
            int n2;
            for (n2 = 0; n2 < array.length && array[n2] != b; n2 += (array[n2 + 1] & 0xFF) + 2) {}
            if ((n = n2) >= array.length) {
                n = -1;
            }
        }
        return n;
    }
    
    public static int getMaxRxPacketSize(final ObexTransport obexTransport) {
        return validateMaxPacketSize(obexTransport.getMaxReceivePacketSize());
    }
    
    public static int getMaxTxPacketSize(final ObexTransport obexTransport) {
        return validateMaxPacketSize(obexTransport.getMaxTransmitPacketSize());
    }
    
    public static byte[] getTagValue(final byte b, byte[] array) {
        int tag = findTag(b, array);
        if (tag == -1) {
            array = null;
        }
        else {
            ++tag;
            final int n = array[tag] & 0xFF;
            final byte[] array2 = new byte[n];
            System.arraycopy(array, tag + 1, array2, 0, n);
            array = array2;
        }
        return array;
    }
    
    public static byte[] updateHeaderSet(HeaderSet set, final byte[] ex) throws IOException {
        int i = 0;
        byte[] array = null;
    Label_0249_Outer:
        while (true) {
            Label_0618: {
                int n = 0;
            Label_0597:
                while (true) {
                    byte[] array2 = null;
                    Label_0413: {
                        int n4 = 0;
                        int n5 = 0;
                        Label_0386: {
                            Label_0362: {
                                Label_0338: {
                                    Label_0307: {
                                        try {
                                            while (i < ex.length) {
                                                n = (ex[i] & 0xFF);
                                                switch (n & 0xC0) {
                                                    default:
                                                        continue Label_0249_Outer;
                                                    case 0:
                                                    case 64: {
                                                        final boolean b = true;
                                                        final int n2 = i + 1;
                                                        final int n3 = ex[n2];
                                                        n4 = n2 + 1;
                                                        n5 = ((n3 & 0xFF) << 8) + (ex[n4] & 0xFF) - 3;
                                                        ++n4;
                                                        array2 = new byte[n5];
                                                        System.arraycopy(ex, n4, array2, 0, n5);
                                                        boolean b2 = false;
                                                        Label_0159: {
                                                            if (n5 != 0) {
                                                                b2 = b;
                                                                if (n5 <= 0) {
                                                                    break Label_0159;
                                                                }
                                                                b2 = b;
                                                                if (array2[n5 - 1] == 0) {
                                                                    break Label_0159;
                                                                }
                                                            }
                                                            b2 = false;
                                                        }
                                                        switch (n) {
                                                            default:
                                                                if ((n & 0xC0) == 0x0) {
                                                                    set.setHeader(n, convertToUnicode(array2, true));
                                                                    break;
                                                                }
                                                                break Label_0413;
                                                            case 66:
                                                                if (b2) {
                                                                    break Label_0307;
                                                                }
                                                                try {
                                                                    set.setHeader(n, new String(array2, 0, array2.length, "ISO8859_1"));
                                                                }
                                                                catch (UnsupportedEncodingException ex2) {
                                                                    throw ex2;
                                                                }
                                                                break;
                                                            case 77:
                                                                break Label_0338;
                                                            case 78:
                                                                break Label_0362;
                                                            case 72:
                                                            case 73:
                                                                break Label_0386;
                                                            case 68:
                                                                break Label_0413;
                                                        }
                                                        i = n4 + n5;
                                                        continue Label_0249_Outer;
                                                    }
                                                    case 128:
                                                        break Label_0597;
                                                    case 192:
                                                        break Label_0618;
                                                        break Label_0618;
                                                }
                                            }
                                            return array;
                                        }
                                        catch (IOException ex3) {
                                            throw new IOException("Header was not formatted properly", ex3);
                                        }
                                    }
                                    set.setHeader(n, new String(array2, 0, array2.length - 1, "ISO8859_1"));
                                    continue;
                                }
                                System.arraycopy(ex, n4, set.mAuthChall = new byte[n5], 0, n5);
                                continue;
                            }
                            System.arraycopy(ex, n4, set.mAuthResp = new byte[n5], 0, n5);
                            continue;
                        }
                        array = new byte[n5 + 1];
                        array[0] = (byte)n;
                        System.arraycopy(ex, n4, array, 1, n5);
                        continue;
                        try {
                            final String s = new String(array2, "ISO8859_1");
                            final Calendar instance = Calendar.getInstance();
                            if (s.length() == 16 && s.charAt(15) == 'Z') {
                                instance.setTimeZone(TimeZone.getTimeZone("UTC"));
                            }
                            instance.set(1, Integer.parseInt(s.substring(0, 4)));
                            instance.set(2, Integer.parseInt(s.substring(4, 6)));
                            instance.set(5, Integer.parseInt(s.substring(6, 8)));
                            instance.set(11, Integer.parseInt(s.substring(9, 11)));
                            instance.set(12, Integer.parseInt(s.substring(11, 13)));
                            instance.set(13, Integer.parseInt(s.substring(13, 15)));
                            set.setHeader(68, instance);
                            continue;
                        }
                        catch (UnsupportedEncodingException ex4) {
                            throw ex4;
                        }
                    }
                    set.setHeader(n, array2);
                    continue;
                }
                ++i;
            Block_17_Outer:
                while (true) {
                    try {
                        set.setHeader(n, Byte.valueOf(ex[i]));
                        ++i;
                        continue Label_0249_Outer;
                        ++i;
                        final byte[] array3 = new byte[4];
                        System.arraycopy(ex, i, array3, 0, 4);
                        // iftrue(Label_0708:, n == 196)
                        while (true) {
                            Block_16: {
                                break Block_16;
                            Label_0670_Outer:
                                while (true) {
                                    final Calendar instance2 = Calendar.getInstance();
                                    instance2.setTime(new Date(convertToLong(array3) * 1000L));
                                    set.setHeader(196, instance2);
                                    while (true) {
                                        break Label_0670;
                                        try {
                                            System.arraycopy(array3, 0, set.mConnectionID = new byte[4], 0, 4);
                                            i += 4;
                                            continue Label_0249_Outer;
                                            Label_0676: {
                                                set.setHeader(n, convertToLong(array3));
                                            }
                                            continue Block_17_Outer;
                                        }
                                        catch (Exception ex) {
                                            set = (HeaderSet)new IOException("Header was not formatted properly", ex);
                                            throw set;
                                        }
                                        break;
                                    }
                                    continue Label_0670_Outer;
                                }
                            }
                            continue;
                        }
                    }
                    // iftrue(Label_0676:, n != 203)
                    catch (Exception ex5) {
                        continue;
                    }
                    break;
                }
            }
        }
    }
    
    private static int validateMaxPacketSize(final int n) {
        int n2;
        if (n != -1) {
            if ((n2 = n) < 255) {
                throw new IllegalArgumentException(n + " is less that the lower limit: " + 255);
            }
        }
        else {
            n2 = 65534;
        }
        return n2;
    }
}
