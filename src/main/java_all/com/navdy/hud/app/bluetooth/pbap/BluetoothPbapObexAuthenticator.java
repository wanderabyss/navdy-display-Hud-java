package com.navdy.hud.app.bluetooth.pbap;

import android.util.Log;
import com.navdy.hud.app.bluetooth.obex.PasswordAuthentication;
import android.os.Handler;
import com.navdy.hud.app.bluetooth.obex.Authenticator;

class BluetoothPbapObexAuthenticator implements Authenticator
{
    private static final String TAG = "BTPbapObexAuthenticator";
    private final Handler mCallback;
    private boolean mReplied;
    private String mSessionKey;
    
    public BluetoothPbapObexAuthenticator(final Handler mCallback) {
        this.mCallback = mCallback;
    }
    
    @Override
    public PasswordAuthentication onAuthenticationChallenge(final String s, final boolean b, final boolean b2) {
        this.mReplied = false;
        Log.d("BTPbapObexAuthenticator", "onAuthenticationChallenge: sending request");
        this.mCallback.obtainMessage(105).sendToTarget();
        synchronized (this) {
            while (!this.mReplied) {
                try {
                    Log.v("BTPbapObexAuthenticator", "onAuthenticationChallenge: waiting for response");
                    this.wait();
                }
                catch (InterruptedException ex) {
                    Log.e("BTPbapObexAuthenticator", "Interrupted while waiting for challenge response");
                }
            }
        }
        // monitorexit(this)
        final PasswordAuthentication passwordAuthentication;
        if (this.mSessionKey != null && this.mSessionKey.length() != 0) {
            Log.v("BTPbapObexAuthenticator", "onAuthenticationChallenge: mSessionKey=" + this.mSessionKey);
            passwordAuthentication = new PasswordAuthentication(null, this.mSessionKey.getBytes());
        }
        else {
            Log.v("BTPbapObexAuthenticator", "onAuthenticationChallenge: mSessionKey is empty, timeout/cancel occured");
        }
        return passwordAuthentication;
    }
    
    @Override
    public byte[] onAuthenticationResponse(final byte[] array) {
        return null;
    }
    
    public void setReply(final String mSessionKey) {
        synchronized (this) {
            Log.d("BTPbapObexAuthenticator", "setReply key=" + mSessionKey);
            this.mSessionKey = mSessionKey;
            this.mReplied = true;
            this.notify();
        }
    }
}
