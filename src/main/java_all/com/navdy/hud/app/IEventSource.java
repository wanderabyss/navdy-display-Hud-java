package com.navdy.hud.app;

import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import android.os.RemoteException;
import android.os.IInterface;

public interface IEventSource extends IInterface
{
    void addEventListener(final IEventListener p0) throws RemoteException;
    
    void postEvent(final byte[] p0) throws RemoteException;
    
    void postRemoteEvent(final String p0, final byte[] p1) throws RemoteException;
    
    void removeEventListener(final IEventListener p0) throws RemoteException;
    
    public abstract static class Stub extends Binder implements IEventSource
    {
        private static final String DESCRIPTOR = "com.navdy.hud.app.IEventSource";
        static final int TRANSACTION_addEventListener = 1;
        static final int TRANSACTION_postEvent = 3;
        static final int TRANSACTION_postRemoteEvent = 4;
        static final int TRANSACTION_removeEventListener = 2;
        
        public Stub() {
            this.attachInterface((IInterface)this, "com.navdy.hud.app.IEventSource");
        }
        
        public static IEventSource asInterface(final IBinder binder) {
            IEventSource eventSource;
            if (binder == null) {
                eventSource = null;
            }
            else {
                final IInterface queryLocalInterface = binder.queryLocalInterface("com.navdy.hud.app.IEventSource");
                if (queryLocalInterface != null && queryLocalInterface instanceof IEventSource) {
                    eventSource = (IEventSource)queryLocalInterface;
                }
                else {
                    eventSource = new Proxy(binder);
                }
            }
            return eventSource;
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            boolean onTransact = true;
            switch (n) {
                default:
                    onTransact = super.onTransact(n, parcel, parcel2, n2);
                    break;
                case 1598968902:
                    parcel2.writeString("com.navdy.hud.app.IEventSource");
                    break;
                case 1:
                    parcel.enforceInterface("com.navdy.hud.app.IEventSource");
                    this.addEventListener(IEventListener.Stub.asInterface(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    break;
                case 2:
                    parcel.enforceInterface("com.navdy.hud.app.IEventSource");
                    this.removeEventListener(IEventListener.Stub.asInterface(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    break;
                case 3:
                    parcel.enforceInterface("com.navdy.hud.app.IEventSource");
                    this.postEvent(parcel.createByteArray());
                    break;
                case 4:
                    parcel.enforceInterface("com.navdy.hud.app.IEventSource");
                    this.postRemoteEvent(parcel.readString(), parcel.createByteArray());
                    break;
            }
            return onTransact;
        }
        
        private static class Proxy implements IEventSource
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            @Override
            public void addEventListener(final IEventListener eventListener) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.hud.app.IEventSource");
                    IBinder binder;
                    if (eventListener != null) {
                        binder = eventListener.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            public String getInterfaceDescriptor() {
                return "com.navdy.hud.app.IEventSource";
            }
            
            @Override
            public void postEvent(final byte[] array) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.hud.app.IEventSource");
                    obtain.writeByteArray(array);
                    this.mRemote.transact(3, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void postRemoteEvent(final String s, final byte[] array) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.hud.app.IEventSource");
                    obtain.writeString(s);
                    obtain.writeByteArray(array);
                    this.mRemote.transact(4, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void removeEventListener(final IEventListener eventListener) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.hud.app.IEventSource");
                    IBinder binder;
                    if (eventListener != null) {
                        binder = eventListener.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
