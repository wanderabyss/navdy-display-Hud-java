package com.navdy.hud.app.util;

public interface Pauseable
{
    void onPause();
    
    void onResume();
}
