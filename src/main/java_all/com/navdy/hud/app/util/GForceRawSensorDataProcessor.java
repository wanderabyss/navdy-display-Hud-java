package com.navdy.hud.app.util;

import com.navdy.hud.app.device.gps.RawSensorData;
import java.util.Arrays;
import android.renderscript.Matrix3f;
import com.navdy.service.library.log.Logger;

public class GForceRawSensorDataProcessor
{
    private static Logger logger;
    private boolean calibrated;
    private Matrix3f rotationMatrix;
    private float[] v;
    public float xAccel;
    public float yAccel;
    public float zAccel;
    
    static {
        GForceRawSensorDataProcessor.logger = new Logger(GForceRawSensorDataProcessor.class);
    }
    
    public GForceRawSensorDataProcessor() {
        this.v = new float[3];
    }
    
    private Matrix3f calculateRotationMatrix(final float[] array) {
        final Matrix3f matrix3f = new Matrix3f();
        final float n = array[0];
        final float n2 = array[1];
        final float n3 = array[2];
        final double atan2 = Math.atan2(n2, n3);
        final double atan3 = Math.atan2(-n, Math.sqrt(n2 * n2 + n3 * n3));
        final double n4 = -atan2;
        final double n5 = -atan3;
        final float n6 = (float)Math.cos(n4);
        final float n7 = (float)Math.sin(n4);
        final float n8 = -n7;
        final float n9 = (float)Math.cos(n5);
        final float n10 = (float)Math.sin(n5);
        matrix3f.loadMultiply(new Matrix3f(new float[] { 1.0f, 0.0f, 0.0f, 0.0f, n6, n7, 0.0f, n8, n6 }), new Matrix3f(new float[] { n9, 0.0f, -n10, 0.0f, 1.0f, 0.0f, n10, 0.0f, n9 }));
        GForceRawSensorDataProcessor.logger.d("acceleration vector: " + n + "," + n2 + "," + n3 + " derived rotation matrix: " + Arrays.toString(matrix3f.getArray()));
        return matrix3f;
    }
    
    private float magnitude(final float[] array) {
        return (float)Math.sqrt(array[0] * array[0] + array[1] * array[1] + array[2] * array[2]);
    }
    
    private void multiply(final Matrix3f matrix3f, final float[] array) {
        final float[] array2 = matrix3f.getArray();
        final float n = array[0];
        final float n2 = array[1];
        final float n3 = array[2];
        final float n4 = array2[0];
        final float n5 = array2[1];
        final float n6 = array2[2];
        final float n7 = array2[3];
        final float n8 = array2[4];
        final float n9 = array2[5];
        final float n10 = array2[6];
        final float n11 = array2[7];
        final float n12 = array2[8];
        array[0] = n4 * n + n5 * n2 + n6 * n3;
        array[1] = n7 * n + n8 * n2 + n9 * n3;
        array[2] = n10 * n + n11 * n2 + n12 * n3;
    }
    
    public boolean isCalibrated() {
        return this.calibrated;
    }
    
    public void onRawData(final RawSensorData rawSensorData) {
        this.v[0] = rawSensorData.x;
        this.v[1] = rawSensorData.y;
        this.v[2] = rawSensorData.z;
        if (!this.calibrated) {
            final float magnitude = this.magnitude(this.v);
            if (magnitude > 0.9 && magnitude < 1.1) {
                this.rotationMatrix = this.calculateRotationMatrix(this.v);
                this.calibrated = true;
            }
        }
        if (this.calibrated) {
            this.multiply(this.rotationMatrix, this.v);
        }
        this.xAccel = this.v[0];
        this.yAccel = this.v[1];
        this.zAccel = this.v[2];
    }
    
    public void setCalibrated(final boolean calibrated) {
        this.calibrated = calibrated;
    }
}
