package com.navdy.hud.app.util;

import mortar.MortarScope;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.service.library.events.ui.Screen;
import android.view.animation.Animation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.navdy.hud.app.screen.BaseScreen;
import flow.Flow;
import android.view.View;
import flow.Layouts;
import mortar.Mortar;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import android.content.Context;
import android.view.ViewGroup;
import mortar.Blueprint;

public class ScreenConductor<S extends Blueprint> implements CanShowScreen<S>
{
    private final ViewGroup container;
    private final Context context;
    HomeScreenView homeScreenView;
    private final Logger logger;
    UIStateManager uiStateManager;
    
    public ScreenConductor(final Context context, final ViewGroup container, final UIStateManager uiStateManager) {
        this.logger = new Logger(this.getClass());
        this.context = context;
        this.container = container;
        this.uiStateManager = uiStateManager;
    }
    
    public void createScreens() {
        if (this.homeScreenView == null) {
            final HomeScreen homeScreen = new HomeScreen();
            (this.homeScreenView = (HomeScreenView)Layouts.createView(Mortar.getScope(this.context).requireChild(homeScreen).createContext(this.context), homeScreen)).setVisibility(INVISIBLE);
            this.container.addView((View)this.homeScreenView, 0);
            this.uiStateManager.setHomescreenView(this.homeScreenView);
            this.logger.v("createScreens:homescreen created and added");
        }
    }
    
    public View getChildView() {
        int i = this.container.getChildCount();
        View child;
        if (i == 0) {
            child = null;
        }
        else {
            --i;
            while (i >= 0) {
                if ((child = this.container.getChildAt(i)).getVisibility() == 0) {
                    return child;
                }
                --i;
            }
            child = null;
        }
        return child;
    }
    
    protected void setAnimation(final Flow.Direction direction, final View view, final View view2, final BaseScreen baseScreen, final BaseScreen baseScreen2, final int n, final int n2) {
        if (view == null) {
            if (baseScreen2 != null) {
                baseScreen2.onAnimationInStart();
                this.uiStateManager.postScreenAnimationEvent(true, baseScreen2, null);
                baseScreen2.onAnimationInEnd();
                this.uiStateManager.postScreenAnimationEvent(false, baseScreen2, null);
            }
        }
        else if (n == -1 && n2 == -1) {
            this.uiStateManager.postScreenAnimationEvent(true, baseScreen2, baseScreen);
        }
        else {
            if (n2 != -1) {
                final Animation loadAnimation = AnimationUtils.loadAnimation(this.context, n2);
                if (baseScreen != null) {
                    loadAnimation.setAnimationListener((Animation$AnimationListener)new Animation$AnimationListener() {
                        public void onAnimationEnd(final Animation animation) {
                            baseScreen.onAnimationOutEnd();
                            if (n == -1) {
                                ScreenConductor.this.uiStateManager.postScreenAnimationEvent(false, baseScreen2, baseScreen);
                            }
                        }
                        
                        public void onAnimationRepeat(final Animation animation) {
                        }
                        
                        public void onAnimationStart(final Animation animation) {
                            baseScreen.onAnimationOutStart();
                            if (n == -1) {
                                ScreenConductor.this.uiStateManager.postScreenAnimationEvent(true, baseScreen2, baseScreen);
                            }
                        }
                    });
                }
                view.setAnimation(loadAnimation);
            }
            if (n != -1) {
                final Animation loadAnimation2 = AnimationUtils.loadAnimation(this.context, n);
                if (baseScreen2 != null) {
                    loadAnimation2.setAnimationListener((Animation$AnimationListener)new Animation$AnimationListener() {
                        public void onAnimationEnd(final Animation animation) {
                            baseScreen2.onAnimationInEnd();
                            ScreenConductor.this.uiStateManager.postScreenAnimationEvent(false, baseScreen2, baseScreen);
                        }
                        
                        public void onAnimationRepeat(final Animation animation) {
                        }
                        
                        public void onAnimationStart(final Animation animation) {
                            baseScreen2.onAnimationInStart();
                            ScreenConductor.this.uiStateManager.postScreenAnimationEvent(true, baseScreen2, baseScreen);
                        }
                    });
                }
                view2.setAnimation(loadAnimation2);
            }
        }
    }
    
    @Override
    public void showScreen(final S n, final Flow.Direction direction, final int n2, final int n3) {
        final MortarScope requireChild = Mortar.getScope(this.context).requireChild(n);
        final View childView = this.getChildView();
        final View view = null;
        if (childView != null && Mortar.getScope(childView.getContext()).getName().equals(n.getMortarScopeName()) && childView.getVisibility() == 0) {
            final BaseScreen baseScreen = (BaseScreen)n;
            this.uiStateManager.postScreenAnimationEvent(true, baseScreen, baseScreen);
            this.uiStateManager.postScreenAnimationEvent(false, baseScreen, baseScreen);
        }
        else {
            final boolean b = n instanceof HomeScreen;
            final boolean b2 = false;
            Object homeScreenView = view;
            if (b) {
                homeScreenView = view;
                if (this.homeScreenView != null) {
                    this.logger.v("reusing homescreen view");
                    homeScreenView = this.homeScreenView;
                }
            }
            Object view2 = homeScreenView;
            boolean b3 = b2;
            if (homeScreenView == null) {
                final View view3 = (View)(view2 = Layouts.createView(requireChild.createContext(this.context), n));
                b3 = b2;
                if (b) {
                    this.homeScreenView = (HomeScreenView)view3;
                    b3 = true;
                    view2 = view3;
                }
            }
            final BaseScreen currentScreen = this.uiStateManager.getCurrentScreen();
            final BaseScreen baseScreen2 = (BaseScreen)n;
            this.setAnimation(direction, childView, (View)view2, currentScreen, baseScreen2, n2, n3);
            if (childView != null && childView != this.homeScreenView) {
                this.container.removeView(childView);
            }
            if (b) {
                if (this.homeScreenView != null) {
                    this.homeScreenView.setVisibility(INVISIBLE);
                }
                if (b3) {
                    this.logger.v("home screen added");
                    this.container.addView((View)view2);
                }
                this.homeScreenView.onResume();
                this.homeScreenView.setVisibility(View.VISIBLE);
            }
            else {
                if (this.homeScreenView != null) {
                    final UIStateManager uiStateManager = this.uiStateManager;
                    if (UIStateManager.isPauseHomescreen(((BaseScreen)n).getScreen())) {
                        this.homeScreenView.onPause();
                        this.homeScreenView.setVisibility(INVISIBLE);
                    }
                }
                this.container.addView((View)view2);
            }
            GenericUtil.clearInputMethodManagerFocusLeak();
            this.uiStateManager.setMainActiveScreen((BaseScreen)n);
            if (((BaseScreen)n).getScreen() != Screen.SCREEN_BACK) {
                AnalyticsSupport.recordScreen((BaseScreen)n);
            }
            if (n2 == -1 && n3 == -1) {
                this.uiStateManager.postScreenAnimationEvent(false, baseScreen2, currentScreen);
            }
        }
    }
}
