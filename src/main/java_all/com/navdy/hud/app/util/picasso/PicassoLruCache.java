package com.navdy.hud.app.util.picasso;

import java.lang.reflect.Field;
import android.graphics.Bitmap;
import java.util.HashMap;
import com.squareup.picasso.LruCache;

public class PicassoLruCache extends LruCache implements PicassoItemCacheListener<String>
{
    private HashMap<String, String> keyMap;
    
    public PicassoLruCache(final int n) {
        super(n);
        this.keyMap = new HashMap<String, String>();
    }
    
    private String getKey(final String s) {
        String substring;
        if (s == null) {
            substring = null;
        }
        else {
            final int index = s.indexOf("\n");
            substring = s;
            if (index != -1) {
                substring = s.substring(0, index);
            }
        }
        return substring;
    }
    
    public Bitmap getBitmap(final String s) {
        final Bitmap bitmap = null;
        // monitorenter(this)
        final Bitmap bitmap2;
        if (s == null) {
            bitmap2 = bitmap;
        }
        else {
            try {
                final String s2 = this.keyMap.get(s);
                if (s2 != null) {
                    super.get(s2);
                }
            }
            finally {
            }
            // monitorexit(this)
        }
        // monitorexit(this)
        return bitmap2;
    }
    
    public void init() throws Exception {
        final PicassoCacheMap picassoCacheMap = new PicassoCacheMap(0, 0.75f);
        picassoCacheMap.setListener(this);
        final Field declaredField = LruCache.class.getDeclaredField("map");
        declaredField.setAccessible(true);
        declaredField.set(this, picassoCacheMap);
    }
    
    @Override
    public void itemAdded(final String s) {
        // monitorenter(this)
        if (s != null) {
            try {
                this.keyMap.put(this.getKey(s), s);
            }
            finally {
            }
            // monitorexit(this)
        }
    }
    // monitorexit(this)
    
    @Override
    public void itemRemoved(final String s) {
        // monitorenter(this)
        if (s != null) {
            try {
                this.keyMap.remove(this.getKey(s));
            }
            finally {
            }
            // monitorexit(this)
        }
    }
    // monitorexit(this)
    
    public void setBitmap(final String s, final Bitmap bitmap) {
        // monitorenter(this)
        if (s != null && bitmap != null) {
            try {
                this.keyMap.put(this.getKey(s), s);
                super.set(s, bitmap);
            }
            finally {
            }
            // monitorexit(this)
        }
    }
    // monitorexit(this)
}
