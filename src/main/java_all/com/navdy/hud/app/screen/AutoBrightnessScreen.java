package com.navdy.hud.app.screen;

import com.navdy.hud.app.HudApplication;
import android.os.Bundle;
import com.navdy.hud.app.manager.InputManager;
import android.content.SharedPreferences;
import com.navdy.hud.app.util.os.SystemProperties;
import java.util.ArrayList;
import java.util.List;
import com.navdy.service.library.events.ui.ShowScreen;
import android.content.SharedPreferences;
import android.content.res.Resources;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import javax.inject.Singleton;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.view.AutoBrightnessView;
import com.navdy.hud.app.ui.activity.Main;
import dagger.Module;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import flow.Layout;

@Layout(R.layout.screen_auto_brightness)
public class AutoBrightnessScreen extends BaseScreen
{
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(AutoBrightnessScreen.class);
    }
    
    @Override
    public Object getDaggerModule() {
        return new Module();
    }
    
    @Override
    public String getMortarScopeName() {
        return this.getClass().getName();
    }
    
    @Override
    public Screen getScreen() {
        return Screen.SCREEN_AUTO_BRIGHTNESS;
    }
    
    @dagger.Module(addsTo = Main.Module.class, injects = { AutoBrightnessView.class })
    public class Module
    {
    }
    
    @Singleton
    public static class Presenter extends BasePresenter<AutoBrightnessView>
    {
        private static final int POS_ACTION = 0;
        private ChoiceLayout.IListener autoBrightnessOffListener;
        private ChoiceLayout.IListener autoBrightnessOnListener;
        @Inject
        Bus bus;
        private Resources resources;
        @Inject
        SharedPreferences sharedPreferences;
        private AutoBrightnessView view;
        
        public Presenter() {
            this.autoBrightnessOnListener = new ChoiceLayout.IListener() {
                @Override
                public void executeItem(final int n, final int n2) {
                    if (n == 0) {
                        Presenter.this.setAutoBrightness(false);
                    }
                    Presenter.this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_BACK).build());
                }
                
                @Override
                public void itemSelected(final int n, final int n2) {
                }
            };
            this.autoBrightnessOffListener = new ChoiceLayout.IListener() {
                @Override
                public void executeItem(final int n, final int n2) {
                    if (n == 0) {
                        Presenter.this.setAutoBrightness(true);
                    }
                    Presenter.this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_BACK).build());
                }
                
                @Override
                public void itemSelected(final int n, final int n2) {
                }
            };
        }
        
        private List<ChoiceLayout.Choice> getChoicesOff() {
            final ArrayList<ChoiceLayout.Choice> list = new ArrayList<ChoiceLayout.Choice>();
            final String string = this.resources.getString(R.string.auto_brightness_choice_enable);
            final String string2 = this.resources.getString(R.string.auto_brightness_choice_cancel);
            list.add(new ChoiceLayout.Choice(string, 0));
            list.add(new ChoiceLayout.Choice(string2, 0));
            return list;
        }
        
        private List<ChoiceLayout.Choice> getChoicesOn() {
            final ArrayList<ChoiceLayout.Choice> list = new ArrayList<ChoiceLayout.Choice>();
            final String string = this.resources.getString(R.string.auto_brightness_choice_disable);
            final String string2 = this.resources.getString(R.string.auto_brightness_choice_cancel);
            list.add(new ChoiceLayout.Choice(string, 0));
            list.add(new ChoiceLayout.Choice(string2, 0));
            return list;
        }
        
        private void init(final AutoBrightnessView view) {
            if ("true".equals(this.sharedPreferences.getString("screen.auto_brightness", ""))) {
                final List<ChoiceLayout.Choice> choicesOn = this.getChoicesOn();
                view.setTitle(this.resources.getString(R.string.auto_brightness_on_title));
                view.setStatusLabel(this.resources.getString(R.string.auto_brightness_on_status_label));
                view.setIcon(this.resources.getDrawable(R.drawable.icon_mm_brightness_copy));
                view.setChoices(choicesOn, this.autoBrightnessOnListener);
            }
            else {
                final List<ChoiceLayout.Choice> choicesOff = this.getChoicesOff();
                view.setTitle(this.resources.getString(R.string.auto_brightness_off_title));
                view.setStatusLabel(this.resources.getString(R.string.auto_brightness_off_status_label));
                view.setIcon(this.resources.getDrawable(R.drawable.icon_mm_brightness_disabled));
                view.setChoices(choicesOff, this.autoBrightnessOffListener);
            }
            this.view = view;
        }
        
        private void setAutoBrightness(final boolean b) {
            final SharedPreferences$Editor edit = this.sharedPreferences.edit();
            String s;
            if (b) {
                s = "true";
            }
            else {
                s = "false";
            }
            edit.putString("screen.auto_brightness", s);
            edit.apply();
            String s2;
            if (b) {
                s2 = "enabled";
            }
            else {
                s2 = "disabled";
            }
            SystemProperties.set("persist.sys.autobrightness", s2);
        }
        
        public boolean handleKey(final InputManager.CustomKeyEvent customKeyEvent) {
            switch (customKeyEvent) {
                case LEFT:
                    this.view.moveSelectionLeft();
                    break;
                case RIGHT:
                    this.view.moveSelectionRight();
                    break;
                case SELECT:
                    this.view.executeSelectedItem();
                    break;
            }
            return true;
        }
        
        @Override
        public void onLoad(final Bundle bundle) {
            this.resources = HudApplication.getAppContext().getResources();
            this.bus.register(this);
            this.updateView();
            super.onLoad(bundle);
        }
        
        protected void updateView() {
            final AutoBrightnessView autoBrightnessView = this.getView();
            if (autoBrightnessView != null) {
                this.init(autoBrightnessView);
            }
        }
    }
}
