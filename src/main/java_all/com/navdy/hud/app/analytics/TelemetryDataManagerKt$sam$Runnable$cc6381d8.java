package com.navdy.hud.app.analytics;

import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function0;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 1 }, k = 3, mv = { 1, 1, 6 })
final class TelemetryDataManagerKt$sam$Runnable$cc6381d8 implements Runnable
{
    private final /* synthetic */ Function0 function;
    
    TelemetryDataManagerKt$sam$Runnable$cc6381d8(final Function0 function) {
        this.function = function;
    }
}
