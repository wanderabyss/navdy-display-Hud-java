package com.navdy.hud.app.storage.db.table;

import com.navdy.hud.app.storage.db.DatabaseUtil;
import android.database.sqlite.SQLiteDatabase;
import com.navdy.service.library.log.Logger;

public class MusicArtworkCacheTable
{
    public static final String ALBUM = "album";
    public static final String AUTHOR = "author";
    public static final String FILE_NAME = "file_name";
    public static final String NAME = "name";
    public static final String TABLE_NAME = "music_artwork_cache";
    public static final String TYPE = "type";
    public static final String TYPE_AUDIOBOOK = "audiobook";
    public static final String TYPE_MUSIC = "music";
    public static final String TYPE_PLAYLIST = "playlist";
    public static final String TYPE_PODCAST = "podcast";
    private static final String[] VALID_TYPES;
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(MusicArtworkCacheTable.class);
        VALID_TYPES = new String[] { "music", "playlist", "podcast", "audiobook" };
    }
    
    public static void createTable(final SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " + "music_artwork_cache" + " (" + "type" + " TEXT NOT NULL CHECK(" + "type" + " IN " + validTypes() + "), " + "author" + " TEXT, " + "album" + " TEXT, " + "name" + " TEXT, " + "file_name" + " TEXT NOT NULL, UNIQUE (" + "type" + ", " + "author" + ", " + "album" + ", " + "name" + ") ON CONFLICT REPLACE);");
        DatabaseUtil.createIndex(sqLiteDatabase, "music_artwork_cache", "type", MusicArtworkCacheTable.sLogger);
        DatabaseUtil.createIndex(sqLiteDatabase, "music_artwork_cache", "album", MusicArtworkCacheTable.sLogger);
        DatabaseUtil.createIndex(sqLiteDatabase, "music_artwork_cache", "author", MusicArtworkCacheTable.sLogger);
        DatabaseUtil.createIndex(sqLiteDatabase, "music_artwork_cache", "name", MusicArtworkCacheTable.sLogger);
    }
    
    private static String validTypes() {
        final StringBuilder sb = new StringBuilder();
        sb.append("('");
        sb.append(MusicArtworkCacheTable.VALID_TYPES[0]);
        for (int i = 1; i < MusicArtworkCacheTable.VALID_TYPES.length; ++i) {
            sb.append("', '");
            sb.append(MusicArtworkCacheTable.VALID_TYPES[i]);
        }
        sb.append("')");
        return sb.toString();
    }
}
