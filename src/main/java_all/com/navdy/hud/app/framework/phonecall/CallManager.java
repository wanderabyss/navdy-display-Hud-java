package com.navdy.hud.app.framework.phonecall;

import com.navdy.service.library.events.callcontrol.TelephonyRequest;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.callcontrol.PhoneStatusResponse;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.service.library.events.callcontrol.CallEvent;
import android.text.TextUtils;
import com.navdy.service.library.events.callcontrol.PhoneEvent;
import com.navdy.service.library.events.callcontrol.PhoneBatteryStatus;
import com.squareup.otto.Subscribe;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.event.DeviceInfoAvailable;
import android.os.SystemClock;
import com.navdy.hud.app.util.PhoneUtil;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.os.Looper;
import android.content.Context;
import com.navdy.service.library.events.callcontrol.PhoneStatus;
import com.navdy.service.library.events.callcontrol.CallAction;
import android.os.Handler;
import java.util.Stack;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.events.callcontrol.PhoneStatusRequest;

public class CallManager
{
    private static final CallAccepted CALL_ACCEPTED;
    private static final CallEnded CALL_ENDED;
    private static final int NO_RESPONSE_THRESHOLD = 10000;
    private static final PhoneStatusRequest PHONE_STATUS_REQUEST;
    private static final Logger sLogger;
    boolean answered;
    private Bus bus;
    boolean callMuted;
    private CallNotification callNotification;
    Stack<Info> callStack;
    long callStartMs;
    String callUUID;
    String contact;
    boolean dialSet;
    long duration;
    private Handler handler;
    boolean incomingCall;
    CallAction lastCallAction;
    private Runnable noResponseCheckRunnable;
    String normalizedNumber;
    String number;
    PhoneStatus phoneStatus;
    boolean putOnStack;
    CallNotificationState state;
    boolean userCancelledCall;
    boolean userRejectedCall;
    
    static {
        sLogger = new Logger(CallManager.class);
        CALL_ENDED = new CallEnded();
        CALL_ACCEPTED = new CallAccepted();
        PHONE_STATUS_REQUEST = new PhoneStatusRequest();
    }
    
    public CallManager(final Bus bus, final Context context) {
        this.state = CallNotificationState.IDLE;
        this.noResponseCheckRunnable = new Runnable() {
            @Override
            public void run() {
                CallManager.sLogger.v("no response for call action:" + CallManager.this.lastCallAction);
                if (CallManager.this.lastCallAction != null && CallManager.this.isCallActionResponseRequired(CallManager.this.lastCallAction)) {
                    switch (CallManager.this.lastCallAction) {
                        case CALL_ACCEPT:
                            CallManager.this.state = CallNotificationState.FAILED;
                            break;
                        case CALL_END:
                            CallManager.this.state = CallNotificationState.IDLE;
                            break;
                        case CALL_DIAL:
                            CallManager.this.state = CallNotificationState.FAILED;
                            break;
                    }
                    CallManager.this.userRejectedCall = false;
                    CallManager.this.userCancelledCall = false;
                    CallManager.this.dialSet = false;
                    if (CallManager.this.isNotificationAllowed()) {
                        CallManager.this.sendNotification();
                    }
                }
            }
        };
        this.handler = new Handler(Looper.getMainLooper());
        this.phoneStatus = PhoneStatus.PHONE_IDLE;
        this.number = null;
        this.normalizedNumber = null;
        this.contact = null;
        this.incomingCall = false;
        this.answered = false;
        this.putOnStack = false;
        this.callStack = new Stack<Info>();
        (this.bus = bus).register(this);
        this.callNotification = new CallNotification(this, bus);
    }
    
    private boolean isCallActionResponseRequired(final CallAction callAction) {
        boolean b = false;
        if (callAction != null) {
            switch (callAction) {
                case CALL_ACCEPT:
                case CALL_END:
                case CALL_DIAL:
                    b = true;
                    break;
            }
        }
        return b;
    }
    
    private boolean isIos() {
        final RemoteDeviceManager instance = RemoteDeviceManager.getInstance();
        DeviceInfo.Platform remoteDevicePlatform;
        if (instance.isRemoteDeviceConnected()) {
            remoteDevicePlatform = instance.getRemoteDevicePlatform();
        }
        else {
            remoteDevicePlatform = null;
        }
        return DeviceInfo.Platform.PLATFORM_iOS.equals(remoteDevicePlatform);
    }
    
    private boolean isNotificationAllowed() {
        return (!this.incomingCall || (this.incomingCall && GlanceHelper.isPhoneNotificationEnabled())) && NotificationManager.getInstance().isPhoneNotificationsEnabled();
    }
    
    private long toSeconds(final long n) {
        return (500L + n) / 1000L;
    }
    
    public void clearCallStack() {
        this.callStack.clear();
    }
    
    public void dial(final String number, final String s, final String contact) {
        if (number == null) {
            CallManager.sLogger.e("null number passed contact[" + contact + "]");
        }
        else {
            this.state = CallNotificationState.DIALING;
            this.phoneStatus = PhoneStatus.PHONE_DIALING;
            this.dialSet = true;
            this.incomingCall = false;
            this.number = number;
            this.normalizedNumber = PhoneUtil.normalizeNumber(number);
            this.contact = contact;
            this.sendNotification();
            this.sendCallAction(CallAction.CALL_DIAL, number);
        }
    }
    
    public void disconnect() {
        CallManager.sLogger.v("got disconnect event, setting to IDLE");
        this.phoneStatus = PhoneStatus.PHONE_IDLE;
        this.state = CallNotificationState.IDLE;
        this.incomingCall = false;
        this.number = null;
        this.normalizedNumber = null;
        this.contact = null;
    }
    
    public int getCurrentCallDuration() {
        int n;
        if (this.callStartMs <= 0L) {
            n = -1;
        }
        else {
            n = (int)((SystemClock.elapsedRealtime() - this.callStartMs) / 1000L);
        }
        return n;
    }
    
    public PhoneStatus getPhoneStatus() {
        return this.phoneStatus;
    }
    
    public boolean isCallInProgress() {
        return this.phoneStatus == PhoneStatus.PHONE_OFFHOOK || this.phoneStatus == PhoneStatus.PHONE_DIALING || this.phoneStatus == PhoneStatus.PHONE_RINGING;
    }
    
    @Subscribe
    public void onDeviceInfoAvailable(final DeviceInfoAvailable deviceInfoAvailable) {
        if (deviceInfoAvailable.deviceInfo != null && deviceInfoAvailable.deviceInfo.platform == DeviceInfo.Platform.PLATFORM_Android) {
            this.bus.post(new RemoteEvent(new PhoneStatusRequest()));
        }
    }
    
    @Subscribe
    public void onPhoneBatteryEvent(final PhoneBatteryStatus phoneBatteryStatus) {
        CallManager.sLogger.v("[Battery-phone] status[" + phoneBatteryStatus.status + "] level[" + phoneBatteryStatus.level + "] charging[" + phoneBatteryStatus.charging + "]");
        PhoneBatteryNotification.showBatteryToast(phoneBatteryStatus);
    }
    
    @Subscribe
    public void onPhoneEvent(final PhoneEvent phoneEvent) {
        this.handler.removeCallbacks(this.noResponseCheckRunnable);
        this.lastCallAction = null;
        if (phoneEvent.status == this.phoneStatus) {
            if (this.dialSet && phoneEvent.status == PhoneStatus.PHONE_DIALING) {
                this.dialSet = false;
            }
            else if (TextUtils.equals((CharSequence)PhoneUtil.normalizeNumber(phoneEvent.number), (CharSequence)this.normalizedNumber)) {
                return;
            }
        }
        CallManager.sLogger.v("phoneEvent:" + phoneEvent);
        final DeviceInfo remoteDeviceInfo = RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
        final PhoneStatus phoneStatus = this.phoneStatus;
        this.phoneStatus = phoneEvent.status;
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        switch (phoneEvent.status) {
            case PHONE_IDLE:
                CallManager.sLogger.v("IDLE: putOnStack=" + this.putOnStack + " lastStatus=" + phoneStatus);
                this.callUUID = null;
                this.callMuted = false;
                if (phoneStatus == PhoneStatus.PHONE_RINGING || phoneStatus == PhoneStatus.PHONE_DIALING) {
                    this.duration = 0L;
                    boolean b = false;
                    if (this.userRejectedCall) {
                        this.state = CallNotificationState.REJECTED;
                        b = true;
                    }
                    else if (this.userCancelledCall) {
                        this.state = CallNotificationState.CANCELLED;
                    }
                    else if (this.answered) {
                        this.state = CallNotificationState.ENDED;
                    }
                    else {
                        this.state = CallNotificationState.MISSED;
                    }
                    CallNotification.clearToast();
                    this.userRejectedCall = false;
                    this.userCancelledCall = false;
                    if (this.putOnStack) {
                        this.removeNotification();
                    }
                    else if (!b) {
                        final INotification currentNotification = NotificationManager.getInstance().getCurrentNotification();
                        if (currentNotification == null || TextUtils.equals((CharSequence)currentNotification.getId(), (CharSequence)"navdy#phone#call#notif")) {
                            this.removeNotification();
                        }
                        else if (this.isNotificationAllowed()) {
                            this.sendNotification();
                        }
                    }
                }
                else if (phoneStatus == PhoneStatus.PHONE_OFFHOOK) {
                    if (remoteDeviceInfo != null && remoteDeviceInfo.platform == DeviceInfo.Platform.PLATFORM_iOS && !TextUtils.isEmpty((CharSequence)phoneEvent.number) && !TextUtils.equals((CharSequence)PhoneUtil.normalizeNumber(phoneEvent.number), (CharSequence)this.normalizedNumber)) {
                        CallManager.sLogger.d("Received end for a different call than the current one");
                        return;
                    }
                    this.duration = this.toSeconds(elapsedRealtime - this.callStartMs);
                    this.state = CallNotificationState.ENDED;
                    if (this.userRejectedCall || this.userCancelledCall || this.putOnStack) {
                        this.removeNotification();
                    }
                    else if (this.isNotificationAllowed()) {
                        this.sendNotification();
                    }
                    this.userRejectedCall = false;
                    this.userCancelledCall = false;
                }
                this.putOnStack = false;
                this.bus.post(CallManager.CALL_ENDED);
                if (this.isNotificationAllowed() && this.number != null) {
                    this.bus.post(new CallEvent(this.incomingCall, this.answered, this.number, this.contact, this.toSeconds(this.callStartMs), this.duration));
                    break;
                }
                break;
            case PHONE_RINGING:
                if (phoneStatus == PhoneStatus.PHONE_OFFHOOK) {
                    CallManager.sLogger.v("we are in a call");
                    if (remoteDeviceInfo == null || remoteDeviceInfo.platform != DeviceInfo.Platform.PLATFORM_iOS) {
                        CallManager.sLogger.w("stack not implemented on android");
                        return;
                    }
                    this.callStack.push(new Info(this.number, this.normalizedNumber, this.contact, this.incomingCall, this.answered, this.callStartMs, this.callUUID, this.duration, this.userRejectedCall, this.userCancelledCall, true, this.callMuted));
                    CallManager.sLogger.v("stack pushed [" + this.contact + " , " + this.number + "," + this.normalizedNumber + "]");
                }
                this.callStartMs = elapsedRealtime;
                this.incomingCall = true;
                this.answered = false;
                this.number = phoneEvent.number;
                this.normalizedNumber = PhoneUtil.normalizeNumber(phoneEvent.number);
                this.contact = phoneEvent.contact_name;
                this.callMuted = false;
                if (!TextUtils.isEmpty((CharSequence)phoneEvent.callUUID)) {
                    this.callUUID = phoneEvent.callUUID;
                }
                this.state = CallNotificationState.RINGING;
                if (this.isNotificationAllowed()) {
                    this.callNotification.showIncomingCallToast();
                    break;
                }
                CallManager.sLogger.v("incoming phone call disabled");
                break;
            case PHONE_DIALING:
                this.callStartMs = elapsedRealtime;
                this.incomingCall = false;
                this.answered = false;
                this.number = phoneEvent.number;
                this.normalizedNumber = PhoneUtil.normalizeNumber(phoneEvent.number);
                this.contact = phoneEvent.contact_name;
                if (!TextUtils.isEmpty((CharSequence)phoneEvent.callUUID)) {
                    this.callUUID = phoneEvent.callUUID;
                }
                this.state = CallNotificationState.DIALING;
                if (this.isNotificationAllowed()) {
                    this.sendNotification();
                    break;
                }
                break;
            case PHONE_OFFHOOK:
                CallNotification.clearToast();
                this.callStartMs = elapsedRealtime;
                this.answered = true;
                if (!TextUtils.isEmpty((CharSequence)phoneEvent.callUUID)) {
                    this.callUUID = phoneEvent.callUUID;
                }
                if (TextUtils.isEmpty((CharSequence)this.normalizedNumber)) {
                    this.normalizedNumber = PhoneUtil.normalizeNumber(phoneEvent.number);
                }
                if (TextUtils.isEmpty((CharSequence)this.contact)) {
                    this.contact = phoneEvent.contact_name;
                }
                if (TextUtils.isEmpty((CharSequence)this.number)) {
                    this.number = phoneEvent.number;
                }
                this.state = CallNotificationState.IN_PROGRESS;
                if (this.isNotificationAllowed()) {
                    this.sendNotification();
                    this.bus.post(CallManager.CALL_ACCEPTED);
                    break;
                }
                break;
            case PHONE_DISCONNECTING:
                if (this.state != CallNotificationState.DIALING) {
                    break;
                }
                this.state = CallNotificationState.FAILED;
                if (this.isNotificationAllowed()) {
                    this.sendNotification();
                    break;
                }
                break;
        }
        CallManager.sLogger.v("state:" + this.state);
    }
    
    @Subscribe
    public void onPhoneStatusResponse(final PhoneStatusResponse phoneStatusResponse) {
        CallManager.sLogger.v("onPhoneStatusResponse [" + phoneStatusResponse.status + "] [" + phoneStatusResponse.callStatus + "]");
        if (phoneStatusResponse.status == RequestStatus.REQUEST_SUCCESS && phoneStatusResponse.callStatus != null && (this.state == CallNotificationState.IDLE || this.state == CallNotificationState.IN_PROGRESS)) {
            CallManager.sLogger.v("onPhoneStatusResponse fwding event");
            this.onPhoneEvent(phoneStatusResponse.callStatus);
        }
    }
    
    void removeNotification() {
        NotificationManager.getInstance().removeNotification(this.callNotification.getId());
    }
    
    void restoreInfo(final Info info) {
        this.bus.post(new CallEvent(this.incomingCall, this.answered, this.number, this.contact, this.toSeconds(this.callStartMs), this.duration));
        this.number = info.number;
        this.normalizedNumber = info.normalizedNumber;
        this.contact = info.contact;
        this.incomingCall = info.incomingCall;
        this.answered = info.answered;
        this.callStartMs = info.callStartMs;
        this.callUUID = info.callUUID;
        this.duration = info.duration;
        this.userRejectedCall = info.userRejectedCall;
        this.userCancelledCall = info.userCancelledCall;
        this.putOnStack = info.putOnStack;
        this.callMuted = info.callMuted;
        CallManager.sLogger.v("stack restored [" + this.contact + " , " + this.number + " , " + this.normalizedNumber + "]");
        if (!this.isIos()) {
            this.handler.postDelayed((Runnable)new Runnable() {
                @Override
                public void run() {
                    CallManager.this.bus.post(new RemoteEvent(CallManager.PHONE_STATUS_REQUEST));
                }
            }, 1000L);
        }
        else if (this.answered) {
            this.phoneStatus = PhoneStatus.PHONE_OFFHOOK;
        }
    }
    
    void sendCallAction(final CallAction lastCallAction, final String s) {
        while (true) {
        Label_0272:
            while (true) {
                Label_0264: {
                    try {
                        if (this.state == CallNotificationState.RINGING && lastCallAction == CallAction.CALL_REJECT) {
                            this.userRejectedCall = true;
                        }
                        else if ((this.state == CallNotificationState.DIALING || this.state == CallNotificationState.IN_PROGRESS) && lastCallAction == CallAction.CALL_END) {
                            this.userCancelledCall = true;
                        }
                        switch (lastCallAction) {
                            default: {
                                if (this.isCallActionResponseRequired(lastCallAction)) {
                                    this.handler.removeCallbacks(this.noResponseCheckRunnable);
                                    this.handler.postDelayed(this.noResponseCheckRunnable, 10000L);
                                }
                                this.lastCallAction = lastCallAction;
                                this.bus.post(new RemoteEvent(new TelephonyRequest(lastCallAction, s, this.callUUID)));
                                final int size = this.callStack.size();
                                CallManager.sLogger.v("call stack size=" + size + " action=" + lastCallAction.name());
                                if (size > 0) {
                                    if (lastCallAction != CallAction.CALL_REJECT) {
                                        break Label_0272;
                                    }
                                    this.restoreInfo(this.callStack.pop());
                                }
                                return;
                            }
                            case CALL_MUTE:
                                break;
                            case CALL_UNMUTE:
                                break Label_0264;
                        }
                    }
                    catch (Throwable t) {
                        CallManager.sLogger.e(t);
                        return;
                    }
                    this.callMuted = true;
                    continue;
                }
                this.callMuted = false;
                continue;
            }
            if (lastCallAction == CallAction.CALL_END) {
                this.restoreInfo(this.callStack.pop());
            }
        }
    }
    
    void sendNotification() {
        NotificationManager.getInstance().addNotification(this.callNotification);
    }
    
    public static class CallAccepted
    {
    }
    
    public static class CallEnded
    {
    }
    
    enum CallNotificationState
    {
        CANCELLED, 
        DIALING, 
        ENDED, 
        FAILED, 
        IDLE, 
        IN_PROGRESS, 
        MISSED, 
        REJECTED, 
        RINGING;
    }
    
    private static class Info
    {
        boolean answered;
        boolean callMuted;
        long callStartMs;
        String callUUID;
        String contact;
        long duration;
        boolean incomingCall;
        String normalizedNumber;
        String number;
        boolean putOnStack;
        boolean userCancelledCall;
        boolean userRejectedCall;
        
        Info(final String number, final String normalizedNumber, final String contact, final boolean incomingCall, final boolean answered, final long callStartMs, final String callUUID, final long duration, final boolean userRejectedCall, final boolean userCancelledCall, final boolean putOnStack, final boolean callMuted) {
            this.number = number;
            this.normalizedNumber = normalizedNumber;
            this.contact = contact;
            this.incomingCall = incomingCall;
            this.answered = answered;
            this.callStartMs = callStartMs;
            this.callUUID = callUUID;
            this.duration = duration;
            this.userRejectedCall = userRejectedCall;
            this.userCancelledCall = userCancelledCall;
            this.putOnStack = putOnStack;
            this.callMuted = callMuted;
        }
    }
}
