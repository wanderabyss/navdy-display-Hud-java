package com.navdy.hud.app.framework.recentcall;

import com.navdy.hud.app.framework.DriverProfileHelper;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.navdy.hud.app.util.PhoneUtil;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import android.text.TextUtils;
import com.navdy.hud.app.framework.contacts.NumberType;
import java.util.Date;
import com.navdy.service.library.log.Logger;

public class RecentCall implements Comparable<RecentCall>
{
    private static final Logger sLogger;
    public Date callTime;
    public CallType callType;
    public Category category;
    public int defaultImageIndex;
    public String firstName;
    public String formattedNumber;
    public String initials;
    public String name;
    public String number;
    public NumberType numberType;
    public String numberTypeStr;
    public long numericNumber;
    
    static {
        sLogger = new Logger(RecentCall.class);
    }
    
    public RecentCall(final RecentCall recentCall) {
        this.name = recentCall.name;
        this.number = recentCall.number;
        this.category = recentCall.category;
        this.numberType = recentCall.numberType;
        this.callTime = recentCall.callTime;
        this.callType = recentCall.callType;
        this.defaultImageIndex = recentCall.defaultImageIndex;
    }
    
    public RecentCall(final String name, final Category category, final String number, final NumberType numberType, final Date callTime, final CallType callType, final int defaultImageIndex, final long n) {
        this.name = name;
        this.number = number;
        this.category = category;
        this.numberType = numberType;
        this.callTime = callTime;
        this.callType = callType;
        this.defaultImageIndex = defaultImageIndex;
        this.validateArguments();
    }
    
    @Override
    public int compareTo(final RecentCall recentCall) {
        int compareTo;
        if (recentCall == null) {
            compareTo = 0;
        }
        else {
            compareTo = this.callTime.compareTo(recentCall.callTime);
        }
        return compareTo;
    }
    
    @Override
    public String toString() {
        return "RecentCall{name='" + this.name + '\'' + ", category=" + this.category + ", number='" + this.number + '\'' + ", numberType=" + this.numberType + ", callTime=" + this.callTime + ", callType=" + this.callType + ", defaultImageIndex=" + this.defaultImageIndex + '}';
    }
    
    public void validateArguments() {
        if (!TextUtils.isEmpty((CharSequence)this.name)) {
            this.firstName = ContactUtil.getFirstName(this.name);
            this.initials = ContactUtil.getInitials(this.name);
        }
        this.numberTypeStr = ContactUtil.getPhoneType(this.numberType);
        this.formattedNumber = PhoneUtil.formatPhoneNumber(this.number);
        if (!TextUtils.isEmpty((CharSequence)this.number)) {
            if (this.numericNumber > 0L) {
                this.numericNumber = this.numericNumber;
            }
            else {
                try {
                    this.numericNumber = PhoneNumberUtil.getInstance().parse(this.number, DriverProfileHelper.getInstance().getCurrentLocale().getCountry()).getNationalNumber();
                }
                catch (Throwable t) {
                    if (RecentCall.sLogger.isLoggable(2)) {
                        RecentCall.sLogger.e(t);
                    }
                }
            }
        }
    }
    
    public enum CallType
    {
        INCOMING(1), 
        MISSED(3), 
        OUTGOING(2), 
        UNNKNOWN(0);
        
        int value;
        
        private CallType(final int value) {
            this.value = value;
        }
        
        public static CallType buildFromValue(final int n) {
            CallType callType = null;
            switch (n) {
                default:
                    callType = CallType.UNNKNOWN;
                    break;
                case 1:
                    callType = CallType.INCOMING;
                    break;
                case 2:
                    callType = CallType.OUTGOING;
                    break;
                case 3:
                    callType = CallType.MISSED;
                    break;
            }
            return callType;
        }
        
        public int getValue() {
            return this.value;
        }
    }
    
    public enum Category
    {
        MESSAGE(2), 
        PHONE_CALL(1), 
        UNNKNOWN(0);
        
        int value;
        
        private Category(final int value) {
            this.value = value;
        }
        
        public static Category buildFromValue(final int n) {
            Category category = null;
            switch (n) {
                default:
                    category = Category.UNNKNOWN;
                    break;
                case 1:
                    category = Category.PHONE_CALL;
                    break;
                case 2:
                    category = Category.MESSAGE;
                    break;
            }
            return category;
        }
        
        public int getValue() {
            return this.value;
        }
    }
}
