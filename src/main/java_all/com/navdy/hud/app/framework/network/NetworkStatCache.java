package com.navdy.hud.app.framework.network;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import com.navdy.service.library.log.Logger;
import java.util.HashMap;

public class NetworkStatCache
{
    private DnsCache dnsCache;
    private HashMap<Integer, NetworkStatCacheInfo> networkStatMap;
    private HashMap<String, NetworkStatCacheInfo> networkStatMapBoot;
    
    NetworkStatCache(final DnsCache dnsCache) {
        this.networkStatMap = new HashMap<Integer, NetworkStatCacheInfo>();
        this.networkStatMapBoot = new HashMap<String, NetworkStatCacheInfo>();
        this.dnsCache = dnsCache;
    }
    
    private void dumpBootStat(final Logger logger) {
        if (this.networkStatMapBoot.size() != 0) {
            final Iterator<NetworkStatCacheInfo> iterator = this.networkStatMapBoot.values().iterator();
            logger.v("dump-boot total endpoints:" + this.networkStatMapBoot.size());
            while (iterator.hasNext()) {
                final NetworkStatCacheInfo networkStatCacheInfo = iterator.next();
                String s;
                if ((s = this.dnsCache.getHostnamefromIP(networkStatCacheInfo.destIP)) == null) {
                    s = networkStatCacheInfo.destIP;
                }
                logger.v("dump-boot  dest[" + s + "] tx[" + networkStatCacheInfo.txBytes + "] rx[" + networkStatCacheInfo.rxBytes + "]");
            }
        }
    }
    
    private void dumpSessionStat(final Logger logger, final boolean b) {
        if (this.networkStatMap.size() != 0) {
            final Iterator<Integer> iterator = this.networkStatMap.keySet().iterator();
            logger.v("dump-session total connections:" + this.networkStatMap.size());
            while (iterator.hasNext()) {
                final int intValue = iterator.next();
                final NetworkStatCacheInfo networkStatCacheInfo = this.networkStatMap.get(intValue);
                String s;
                if ((s = this.dnsCache.getHostnamefromIP(networkStatCacheInfo.destIP)) == null) {
                    s = networkStatCacheInfo.destIP;
                }
                logger.v("dump-session fd[" + intValue + "] dest[" + s + "] tx[" + networkStatCacheInfo.txBytes + "] rx[" + networkStatCacheInfo.rxBytes + "]");
            }
            if (b) {
                this.clear();
            }
        }
    }
    
    public void addStat(final String s, final int n, final int n2, final int n3) {
        while (true) {
            synchronized (this) {
                final NetworkStatCacheInfo networkStatCacheInfo = this.networkStatMap.get(n3);
                if (networkStatCacheInfo == null) {
                    this.networkStatMap.put(n3, new NetworkStatCacheInfo(n, n2, s));
                }
                else {
                    networkStatCacheInfo.txBytes += n;
                    networkStatCacheInfo.rxBytes += n2;
                }
                if (s == null) {
                    return;
                }
            }
            final String s2;
            final NetworkStatCacheInfo networkStatCacheInfo2 = this.networkStatMapBoot.get(s2);
            if (networkStatCacheInfo2 == null) {
                this.networkStatMapBoot.put(s2, new NetworkStatCacheInfo(n, n2, s2));
                return;
            }
            networkStatCacheInfo2.txBytes += n;
            networkStatCacheInfo2.rxBytes += n2;
        }
    }
    
    public void clear() {
        synchronized (this) {
            this.networkStatMap.clear();
        }
    }
    
    public void dump(final Logger logger, final boolean b) {
        synchronized (this) {
            this.dumpSessionStat(logger, b);
            this.dumpBootStat(logger);
        }
    }
    
    public List<NetworkStatCacheInfo> getBootStat() {
        synchronized (this) {
            return this.getStat(this.networkStatMapBoot.values().iterator());
        }
    }
    
    public List<NetworkStatCacheInfo> getSessionStat() {
        synchronized (this) {
            return this.getStat(this.networkStatMap.values().iterator());
        }
    }
    
    public List<NetworkStatCacheInfo> getStat(final Iterator<NetworkStatCacheInfo> iterator) {
        final ArrayList<NetworkStatCacheInfo> list;
        synchronized (this) {
            list = new ArrayList<NetworkStatCacheInfo>();
            while (iterator.hasNext()) {
                final NetworkStatCacheInfo networkStatCacheInfo = iterator.next();
                String s;
                if ((s = this.dnsCache.getHostnamefromIP(networkStatCacheInfo.destIP)) == null) {
                    s = networkStatCacheInfo.destIP;
                }
                list.add(new NetworkStatCacheInfo(networkStatCacheInfo.txBytes, networkStatCacheInfo.rxBytes, s));
            }
        }
        // monitorexit(this)
        return list;
    }
    
    public static class NetworkStatCacheInfo
    {
        public String destIP;
        public int rxBytes;
        public int txBytes;
        
        NetworkStatCacheInfo() {
        }
        
        NetworkStatCacheInfo(final int txBytes, final int rxBytes, final String destIP) {
            this.txBytes = txBytes;
            this.rxBytes = rxBytes;
            this.destIP = destIP;
        }
    }
}
