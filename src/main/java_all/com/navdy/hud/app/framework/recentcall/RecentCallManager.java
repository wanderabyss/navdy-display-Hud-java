package com.navdy.hud.app.framework.recentcall;

import com.navdy.service.library.events.contacts.ContactResponse;
import com.squareup.otto.Subscribe;
import java.util.Date;
import com.navdy.hud.app.framework.contacts.NumberType;
import com.navdy.service.library.events.callcontrol.CallEvent;
import android.text.TextUtils;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.task.TaskManager;
import java.util.ArrayList;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.events.contacts.ContactRequest;
import com.navdy.hud.app.storage.db.helper.RecentCallPersistenceHelper;
import com.navdy.hud.app.framework.DriverProfileHelper;
import java.util.Iterator;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.service.library.events.contacts.Contact;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class RecentCallManager
{
    private static final RecentCallChanged RECENT_CALL_CHANGED;
    private static final RecentCallManager sInstance;
    private static final Logger sLogger;
    private Bus bus;
    private HashMap<String, RecentCall> contactRequestMap;
    private Map<Long, RecentCall> numberMap;
    private volatile List<RecentCall> recentCalls;
    private HashMap<String, List<Contact>> tempContactLookupMap;
    
    static {
        sLogger = new Logger(RecentCallManager.class);
        RECENT_CALL_CHANGED = new RecentCallChanged();
        sInstance = new RecentCallManager();
    }
    
    private RecentCallManager() {
        this.numberMap = new HashMap<Long, RecentCall>();
        this.tempContactLookupMap = new HashMap<String, List<Contact>>();
        this.contactRequestMap = new HashMap<String, RecentCall>();
        (this.bus = RemoteDeviceManager.getInstance().getBus()).register(this);
    }
    
    private void buildMap() {
        this.numberMap.clear();
        if (this.recentCalls != null && this.recentCalls.size() != 0) {
            for (final RecentCall recentCall : this.recentCalls) {
                this.numberMap.put(recentCall.numericNumber, recentCall);
            }
        }
    }
    
    public static RecentCallManager getInstance() {
        return RecentCallManager.sInstance;
    }
    
    private void load() {
        try {
            final String profileName = DriverProfileHelper.getInstance().getCurrentProfile().getProfileName();
            this.recentCalls = RecentCallPersistenceHelper.getRecentsCalls(profileName);
            RecentCallManager.sLogger.v("load recentCall id[" + profileName + "] calls[" + this.recentCalls.size() + "]");
        }
        catch (Throwable t) {
            this.recentCalls = null;
            RecentCallManager.sLogger.e(t);
            this.buildMap();
            this.bus.post(RecentCallManager.RECENT_CALL_CHANGED);
        }
        finally {
            this.buildMap();
            this.bus.post(RecentCallManager.RECENT_CALL_CHANGED);
        }
    }
    
    private void requestContactInfo(final RecentCall recentCall) {
        RecentCallManager.sLogger.v("number not available for [" + recentCall.number + "]");
        final HashMap<String, RecentCall> contactRequestMap = this.contactRequestMap;
        synchronized (contactRequestMap) {
            if (this.contactRequestMap.containsKey(recentCall.number)) {
                RecentCallManager.sLogger.v("contact [" + recentCall.number + "] request already pending");
            }
            else {
                this.contactRequestMap.put(recentCall.number, recentCall);
                RecentCallManager.sLogger.v("contact [" + recentCall.number + "] request sent");
                this.bus.post(new RemoteEvent(new ContactRequest(recentCall.number)));
            }
            // monitorexit(contactRequestMap)
        }
    }
    
    private void storeContactInfo(final RecentCall recentCall) {
        final ArrayList<RecentCall> list = new ArrayList<RecentCall>();
        list.add(recentCall);
        this.storeContactInfo(list);
    }
    
    private void storeContactInfo(final List<RecentCall> list) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                RecentCallPersistenceHelper.storeRecentCalls(DriverProfileHelper.getInstance().getCurrentProfile().getProfileName(), list, false);
                final Iterator<RecentCall> iterator = list.iterator();
                while (iterator.hasNext()) {
                    RecentCallManager.sLogger.v("added contact [" + iterator.next().number + "]");
                }
            }
        }, 1);
    }
    
    public void buildRecentCalls() {
        while (true) {
            final HashMap<String, RecentCall> contactRequestMap = this.contactRequestMap;
            synchronized (contactRequestMap) {
                this.contactRequestMap.clear();
                // monitorexit(contactRequestMap)
                if (GenericUtil.isMainThread()) {
                    TaskManager.getInstance().execute(new Runnable() {
                        @Override
                        public void run() {
                            RecentCallManager.this.load();
                        }
                    }, 1);
                    return;
                }
            }
            this.load();
        }
    }
    
    public void clearContactLookupMap() {
        final HashMap<String, List<Contact>> tempContactLookupMap = this.tempContactLookupMap;
        synchronized (tempContactLookupMap) {
            this.tempContactLookupMap.clear();
        }
    }
    
    public void clearRecentCalls() {
        this.setRecentCalls(null);
    }
    
    public List<Contact> getContactsFromId(final String s) {
        final HashMap<String, List<Contact>> tempContactLookupMap = this.tempContactLookupMap;
        synchronized (tempContactLookupMap) {
            return this.tempContactLookupMap.get(s);
        }
    }
    
    public int getDefaultContactImage(final long n) {
        final RecentCall recentCall = this.numberMap.get(n);
        int defaultImageIndex;
        if (recentCall == null) {
            defaultImageIndex = -1;
        }
        else {
            defaultImageIndex = recentCall.defaultImageIndex;
        }
        return defaultImageIndex;
    }
    
    public List<RecentCall> getRecentCalls() {
        return this.recentCalls;
    }
    
    public int getRecentCallsSize() {
        int size;
        if (this.recentCalls != null) {
            size = this.recentCalls.size();
        }
        else {
            size = 0;
        }
        return size;
    }
    
    public boolean handleNewCall(final RecentCall recentCall) {
        RecentCallManager.sLogger.v("handle new call:" + recentCall.number);
        if (ContactUtil.isValidNumber(recentCall.number)) {
            this.storeContactInfo(recentCall);
            return true;
        }
        while (true) {
            String number = null;
            final HashMap<String, List<Contact>> tempContactLookupMap = this.tempContactLookupMap;
            synchronized (tempContactLookupMap) {
                final List<Contact> list = this.tempContactLookupMap.get(recentCall.number);
                // monitorexit(tempContactLookupMap)
                Label_0107: {
                    if (list == null) {
                        break Label_0107;
                    }
                    if (list.size() <= 1) {
                        number = list.get(0).number;
                        break Label_0107;
                    }
                    return false;
                }
                // monitorexit(tempContactLookupMap)
                if (!TextUtils.isEmpty((CharSequence)number)) {
                    recentCall.number = number;
                    this.storeContactInfo(recentCall);
                    return true;
                }
            }
            final RecentCall recentCall2;
            this.requestContactInfo(recentCall2);
            return false;
        }
    }
    
    @Subscribe
    public void onCallEvent(final CallEvent callEvent) {
        if (TextUtils.isEmpty((CharSequence)callEvent.number)) {
            RecentCallManager.sLogger.e("call event does not have a number, " + callEvent.contact_name);
        }
        else {
            RecentCall.CallType callType;
            if (callEvent.incoming) {
                if (callEvent.answered) {
                    callType = RecentCall.CallType.INCOMING;
                }
                else {
                    callType = RecentCall.CallType.MISSED;
                }
            }
            else {
                callType = RecentCall.CallType.OUTGOING;
            }
            this.handleNewCall(new RecentCall(callEvent.contact_name, RecentCall.Category.PHONE_CALL, callEvent.number, NumberType.OTHER, new Date(), callType, -1, 0L));
        }
    }
    
    @Subscribe
    public void onContactResponse(final ContactResponse contactResponse) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                // 
                This method could not be decompiled.
                // 
                // Original Bytecode:
                // 
                //     1: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;
                //     4: getfield        com/navdy/service/library/events/contacts/ContactResponse.identifier:Ljava/lang/String;
                //     7: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
                //    10: ifeq            22
                //    13: invokestatic    com/navdy/hud/app/framework/recentcall/RecentCallManager.access$100:()Lcom/navdy/service/library/log/Logger;
                //    16: ldc             "invalid contact repsonse"
                //    18: invokevirtual   com/navdy/service/library/log/Logger.w:(Ljava/lang/String;)V
                //    21: return         
                //    22: aconst_null    
                //    23: astore_1       
                //    24: aconst_null    
                //    25: astore_2       
                //    26: aload_0        
                //    27: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.this$0:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;
                //    30: invokestatic    com/navdy/hud/app/framework/recentcall/RecentCallManager.access$200:(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;)Ljava/util/HashMap;
                //    33: astore_3       
                //    34: aload_3        
                //    35: dup            
                //    36: astore          4
                //    38: monitorenter   
                //    39: aload_0        
                //    40: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.this$0:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;
                //    43: invokestatic    com/navdy/hud/app/framework/recentcall/RecentCallManager.access$200:(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;)Ljava/util/HashMap;
                //    46: aload_0        
                //    47: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;
                //    50: getfield        com/navdy/service/library/events/contacts/ContactResponse.identifier:Ljava/lang/String;
                //    53: invokevirtual   java/util/HashMap.remove:(Ljava/lang/Object;)Ljava/lang/Object;
                //    56: checkcast       Lcom/navdy/hud/app/framework/recentcall/RecentCall;
                //    59: astore          5
                //    61: aload           5
                //    63: ifnonnull       129
                //    66: invokestatic    com/navdy/hud/app/framework/recentcall/RecentCallManager.access$100:()Lcom/navdy/service/library/log/Logger;
                //    69: astore_2       
                //    70: new             Ljava/lang/StringBuilder;
                //    73: astore_1       
                //    74: aload_1        
                //    75: invokespecial   java/lang/StringBuilder.<init>:()V
                //    78: aload_2        
                //    79: aload_1        
                //    80: ldc             "identifer not found["
                //    82: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //    85: aload_0        
                //    86: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;
                //    89: getfield        com/navdy/service/library/events/contacts/ContactResponse.identifier:Ljava/lang/String;
                //    92: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //    95: ldc             "]"
                //    97: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   100: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //   103: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/String;)V
                //   106: aload           4
                //   108: monitorexit    
                //   109: goto            21
                //   112: astore_1       
                //   113: aload           4
                //   115: monitorexit    
                //   116: aload_1        
                //   117: athrow         
                //   118: astore_1       
                //   119: invokestatic    com/navdy/hud/app/framework/recentcall/RecentCallManager.access$100:()Lcom/navdy/service/library/log/Logger;
                //   122: aload_1        
                //   123: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
                //   126: goto            21
                //   129: aload_0        
                //   130: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;
                //   133: getfield        com/navdy/service/library/events/contacts/ContactResponse.status:Lcom/navdy/service/library/events/RequestStatus;
                //   136: getstatic       com/navdy/service/library/events/RequestStatus.REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;
                //   139: if_acmpne       726
                //   142: aload_0        
                //   143: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;
                //   146: getfield        com/navdy/service/library/events/contacts/ContactResponse.contacts:Ljava/util/List;
                //   149: ifnull          167
                //   152: aload_0        
                //   153: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;
                //   156: getfield        com/navdy/service/library/events/contacts/ContactResponse.contacts:Ljava/util/List;
                //   159: invokeinterface java/util/List.size:()I
                //   164: ifne            228
                //   167: invokestatic    com/navdy/hud/app/framework/recentcall/RecentCallManager.access$100:()Lcom/navdy/service/library/log/Logger;
                //   170: astore_1       
                //   171: new             Ljava/lang/StringBuilder;
                //   174: astore_2       
                //   175: aload_2        
                //   176: invokespecial   java/lang/StringBuilder.<init>:()V
                //   179: aload_1        
                //   180: aload_2        
                //   181: ldc             "no contact returned for ["
                //   183: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   186: aload_0        
                //   187: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;
                //   190: getfield        com/navdy/service/library/events/contacts/ContactResponse.identifier:Ljava/lang/String;
                //   193: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   196: ldc             "] status["
                //   198: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   201: aload_0        
                //   202: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;
                //   205: getfield        com/navdy/service/library/events/contacts/ContactResponse.status:Lcom/navdy/service/library/events/RequestStatus;
                //   208: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
                //   211: ldc             "]"
                //   213: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   216: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //   219: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/String;)V
                //   222: aload           4
                //   224: monitorexit    
                //   225: goto            21
                //   228: invokestatic    com/navdy/hud/app/framework/recentcall/RecentCallManager.access$100:()Lcom/navdy/service/library/log/Logger;
                //   231: astore_2       
                //   232: new             Ljava/lang/StringBuilder;
                //   235: astore_1       
                //   236: aload_1        
                //   237: invokespecial   java/lang/StringBuilder.<init>:()V
                //   240: aload_2        
                //   241: aload_1        
                //   242: ldc             "contacts returned ["
                //   244: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   247: aload_0        
                //   248: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;
                //   251: getfield        com/navdy/service/library/events/contacts/ContactResponse.contacts:Ljava/util/List;
                //   254: invokeinterface java/util/List.size:()I
                //   259: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
                //   262: ldc             "]"
                //   264: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   267: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //   270: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
                //   273: aload_0        
                //   274: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;
                //   277: getfield        com/navdy/service/library/events/contacts/ContactResponse.contacts:Ljava/util/List;
                //   280: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
                //   285: astore          6
                //   287: aconst_null    
                //   288: astore_2       
                //   289: aconst_null    
                //   290: astore_1       
                //   291: aload           6
                //   293: invokeinterface java/util/Iterator.hasNext:()Z
                //   298: ifeq            542
                //   301: aload           6
                //   303: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
                //   308: checkcast       Lcom/navdy/service/library/events/contacts/Contact;
                //   311: astore          7
                //   313: aload           7
                //   315: getfield        com/navdy/service/library/events/contacts/Contact.number:Ljava/lang/String;
                //   318: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
                //   321: ifne            335
                //   324: aload           7
                //   326: getfield        com/navdy/service/library/events/contacts/Contact.number:Ljava/lang/String;
                //   329: invokestatic    com/navdy/hud/app/framework/contacts/ContactUtil.isValidNumber:(Ljava/lang/String;)Z
                //   332: ifne            414
                //   335: invokestatic    com/navdy/hud/app/framework/recentcall/RecentCallManager.access$100:()Lcom/navdy/service/library/log/Logger;
                //   338: astore          8
                //   340: new             Ljava/lang/StringBuilder;
                //   343: astore          7
                //   345: aload           7
                //   347: invokespecial   java/lang/StringBuilder.<init>:()V
                //   350: aload           8
                //   352: aload           7
                //   354: ldc             "no number for  ["
                //   356: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   359: aload_0        
                //   360: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;
                //   363: getfield        com/navdy/service/library/events/contacts/ContactResponse.identifier:Ljava/lang/String;
                //   366: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   369: ldc             "] status["
                //   371: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   374: aload_0        
                //   375: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;
                //   378: getfield        com/navdy/service/library/events/contacts/ContactResponse.status:Lcom/navdy/service/library/events/RequestStatus;
                //   381: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
                //   384: ldc             "]"
                //   386: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   389: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //   392: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/String;)V
                //   395: aload_1        
                //   396: astore          8
                //   398: aload_2        
                //   399: astore_1       
                //   400: aload           8
                //   402: astore_2       
                //   403: aload_1        
                //   404: astore          8
                //   406: aload_2        
                //   407: astore_1       
                //   408: aload           8
                //   410: astore_2       
                //   411: goto            291
                //   414: new             Lcom/navdy/hud/app/framework/recentcall/RecentCall;
                //   417: astore          9
                //   419: aload           9
                //   421: aload           5
                //   423: invokespecial   com/navdy/hud/app/framework/recentcall/RecentCall.<init>:(Lcom/navdy/hud/app/framework/recentcall/RecentCall;)V
                //   426: aload           7
                //   428: getfield        com/navdy/service/library/events/contacts/Contact.name:Ljava/lang/String;
                //   431: aload           7
                //   433: getfield        com/navdy/service/library/events/contacts/Contact.number:Ljava/lang/String;
                //   436: aload           7
                //   438: getfield        com/navdy/service/library/events/contacts/Contact.number:Ljava/lang/String;
                //   441: invokestatic    com/navdy/hud/app/framework/contacts/ContactUtil.getPhoneNumber:(Ljava/lang/String;)J
                //   444: invokestatic    com/navdy/hud/app/framework/contacts/ContactUtil.isDisplayNameValid:(Ljava/lang/String;Ljava/lang/String;J)Z
                //   447: ifeq            529
                //   450: aload           9
                //   452: aload           7
                //   454: getfield        com/navdy/service/library/events/contacts/Contact.name:Ljava/lang/String;
                //   457: putfield        com/navdy/hud/app/framework/recentcall/RecentCall.name:Ljava/lang/String;
                //   460: aload           9
                //   462: aload           7
                //   464: getfield        com/navdy/service/library/events/contacts/Contact.number:Ljava/lang/String;
                //   467: putfield        com/navdy/hud/app/framework/recentcall/RecentCall.number:Ljava/lang/String;
                //   470: aload           9
                //   472: aload           7
                //   474: getfield        com/navdy/service/library/events/contacts/Contact.numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;
                //   477: invokestatic    com/navdy/hud/app/framework/contacts/ContactUtil.getNumberType:(Lcom/navdy/service/library/events/contacts/PhoneNumberType;)Lcom/navdy/hud/app/framework/contacts/NumberType;
                //   480: putfield        com/navdy/hud/app/framework/recentcall/RecentCall.numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;
                //   483: aload           9
                //   485: invokevirtual   com/navdy/hud/app/framework/recentcall/RecentCall.validateArguments:()V
                //   488: aload_1        
                //   489: ifnonnull       820
                //   492: new             Ljava/util/ArrayList;
                //   495: dup            
                //   496: invokespecial   java/util/ArrayList.<init>:()V
                //   499: astore_2       
                //   500: new             Ljava/util/ArrayList;
                //   503: dup            
                //   504: invokespecial   java/util/ArrayList.<init>:()V
                //   507: astore_1       
                //   508: aload_2        
                //   509: aload           9
                //   511: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
                //   516: pop            
                //   517: aload_1        
                //   518: aload           7
                //   520: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
                //   525: pop            
                //   526: goto            403
                //   529: aload           9
                //   531: aconst_null    
                //   532: putfield        com/navdy/hud/app/framework/recentcall/RecentCall.name:Ljava/lang/String;
                //   535: goto            460
                //   538: astore_1       
                //   539: goto            113
                //   542: aload           4
                //   544: monitorexit    
                //   545: aload_1        
                //   546: ifnull          21
                //   549: invokestatic    com/navdy/hud/app/framework/recentcall/RecentCallManager.access$100:()Lcom/navdy/service/library/log/Logger;
                //   552: astore_3       
                //   553: new             Ljava/lang/StringBuilder;
                //   556: astore          8
                //   558: aload           8
                //   560: invokespecial   java/lang/StringBuilder.<init>:()V
                //   563: aload_3        
                //   564: aload           8
                //   566: ldc             "contact found ["
                //   568: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   571: aload_0        
                //   572: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;
                //   575: getfield        com/navdy/service/library/events/contacts/ContactResponse.identifier:Ljava/lang/String;
                //   578: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   581: ldc             "] downloading photo"
                //   583: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   586: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //   589: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
                //   592: aload_0        
                //   593: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.this$0:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;
                //   596: invokestatic    com/navdy/hud/app/framework/recentcall/RecentCallManager.access$300:(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;)Ljava/util/HashMap;
                //   599: astore          8
                //   601: aload           8
                //   603: dup            
                //   604: astore          10
                //   606: monitorenter   
                //   607: aload_0        
                //   608: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.this$0:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;
                //   611: invokestatic    com/navdy/hud/app/framework/recentcall/RecentCallManager.access$300:(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;)Ljava/util/HashMap;
                //   614: aload_0        
                //   615: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;
                //   618: getfield        com/navdy/service/library/events/contacts/ContactResponse.identifier:Ljava/lang/String;
                //   621: aload_2        
                //   622: invokevirtual   java/util/HashMap.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
                //   625: pop            
                //   626: aload           10
                //   628: monitorexit    
                //   629: aload_0        
                //   630: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.this$0:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;
                //   633: invokestatic    com/navdy/hud/app/framework/recentcall/RecentCallManager.access$400:(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;)Lcom/squareup/otto/Bus;
                //   636: astore_3       
                //   637: new             Lcom/navdy/hud/app/framework/recentcall/RecentCallManager$ContactFound;
                //   640: astore          8
                //   642: aload           8
                //   644: aload_0        
                //   645: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;
                //   648: getfield        com/navdy/service/library/events/contacts/ContactResponse.identifier:Ljava/lang/String;
                //   651: aload_2        
                //   652: invokespecial   com/navdy/hud/app/framework/recentcall/RecentCallManager$ContactFound.<init>:(Ljava/lang/String;Ljava/util/List;)V
                //   655: aload_3        
                //   656: aload           8
                //   658: invokevirtual   com/squareup/otto/Bus.post:(Ljava/lang/Object;)V
                //   661: invokestatic    com/navdy/hud/app/framework/contacts/PhoneImageDownloader.getInstance:()Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
                //   664: astore_2       
                //   665: aload_1        
                //   666: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
                //   671: astore          8
                //   673: aload           8
                //   675: invokeinterface java/util/Iterator.hasNext:()Z
                //   680: ifeq            795
                //   683: aload           8
                //   685: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
                //   690: checkcast       Lcom/navdy/hud/app/framework/recentcall/RecentCall;
                //   693: astore_3       
                //   694: aload_2        
                //   695: aload_3        
                //   696: getfield        com/navdy/hud/app/framework/recentcall/RecentCall.number:Ljava/lang/String;
                //   699: getstatic       com/navdy/service/library/events/photo/PhotoType.PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;
                //   702: invokevirtual   com/navdy/hud/app/framework/contacts/PhoneImageDownloader.clearPhotoCheckEntry:(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)V
                //   705: aload_2        
                //   706: aload_3        
                //   707: getfield        com/navdy/hud/app/framework/recentcall/RecentCall.number:Ljava/lang/String;
                //   710: getstatic       com/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority.HIGH:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;
                //   713: getstatic       com/navdy/service/library/events/photo/PhotoType.PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;
                //   716: aload_3        
                //   717: getfield        com/navdy/hud/app/framework/recentcall/RecentCall.name:Ljava/lang/String;
                //   720: invokevirtual   com/navdy/hud/app/framework/contacts/PhoneImageDownloader.submitDownload:(Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;Lcom/navdy/service/library/events/photo/PhotoType;Ljava/lang/String;)V
                //   723: goto            673
                //   726: invokestatic    com/navdy/hud/app/framework/recentcall/RecentCallManager.access$100:()Lcom/navdy/service/library/log/Logger;
                //   729: astore          8
                //   731: new             Ljava/lang/StringBuilder;
                //   734: astore          5
                //   736: aload           5
                //   738: invokespecial   java/lang/StringBuilder.<init>:()V
                //   741: aload           8
                //   743: aload           5
                //   745: ldc             "request failed for ["
                //   747: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   750: aload_0        
                //   751: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;
                //   754: getfield        com/navdy/service/library/events/contacts/ContactResponse.identifier:Ljava/lang/String;
                //   757: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   760: ldc             "] status["
                //   762: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   765: aload_0        
                //   766: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.val$event:Lcom/navdy/service/library/events/contacts/ContactResponse;
                //   769: getfield        com/navdy/service/library/events/contacts/ContactResponse.status:Lcom/navdy/service/library/events/RequestStatus;
                //   772: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
                //   775: ldc             "]"
                //   777: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   780: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //   783: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/String;)V
                //   786: goto            542
                //   789: astore_1       
                //   790: aload           10
                //   792: monitorexit    
                //   793: aload_1        
                //   794: athrow         
                //   795: aload_1        
                //   796: invokeinterface java/util/List.size:()I
                //   801: iconst_1       
                //   802: if_icmpne       21
                //   805: aload_0        
                //   806: getfield        com/navdy/hud/app/framework/recentcall/RecentCallManager$2.this$0:Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;
                //   809: aload_1        
                //   810: invokestatic    com/navdy/hud/app/framework/recentcall/RecentCallManager.access$500:(Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;Ljava/util/List;)V
                //   813: goto            21
                //   816: astore_1       
                //   817: goto            113
                //   820: aload_2        
                //   821: astore          8
                //   823: aload_1        
                //   824: astore_2       
                //   825: aload           8
                //   827: astore_1       
                //   828: goto            508
                //    Exceptions:
                //  Try           Handler
                //  Start  End    Start  End    Type                 
                //  -----  -----  -----  -----  ---------------------
                //  0      21     118    129    Ljava/lang/Throwable;
                //  26     39     118    129    Ljava/lang/Throwable;
                //  39     61     112    113    Any
                //  66     109    112    113    Any
                //  113    116    112    113    Any
                //  116    118    118    129    Ljava/lang/Throwable;
                //  129    167    112    113    Any
                //  167    225    112    113    Any
                //  228    287    112    113    Any
                //  291    335    538    542    Any
                //  335    395    538    542    Any
                //  414    460    538    542    Any
                //  460    488    538    542    Any
                //  492    500    538    542    Any
                //  500    508    816    820    Any
                //  508    526    112    113    Any
                //  529    535    538    542    Any
                //  542    545    112    113    Any
                //  549    607    118    129    Ljava/lang/Throwable;
                //  607    629    789    795    Any
                //  629    673    118    129    Ljava/lang/Throwable;
                //  673    723    118    129    Ljava/lang/Throwable;
                //  726    786    112    113    Any
                //  790    793    789    795    Any
                //  793    795    118    129    Ljava/lang/Throwable;
                //  795    813    118    129    Ljava/lang/Throwable;
                // 
                // The error that occurred was:
                // 
                // java.lang.IllegalStateException: Expression is linked from several locations: Label_0508:
                //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
                //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
                //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
                //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1163)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:1010)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:554)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:392)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:333)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:294)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
                //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
                //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
                //     at java.lang.Thread.run(Unknown Source)
                // 
                throw new IllegalStateException("An error occurred while decompiling this method.");
            }
        }, 1);
    }
    
    public void setRecentCalls(final List<RecentCall> recentCalls) {
        this.recentCalls = recentCalls;
        this.buildMap();
        this.bus.post(RecentCallManager.RECENT_CALL_CHANGED);
    }
    
    public static class ContactFound
    {
        public List<Contact> contact;
        public String identifier;
        
        public ContactFound(final String identifier, final List<Contact> contact) {
            this.identifier = identifier;
            this.contact = contact;
        }
    }
    
    public static class RecentCallChanged
    {
    }
}
