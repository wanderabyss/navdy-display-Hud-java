package com.navdy.hud.app.framework.notifications;

import com.navdy.hud.app.ui.framework.UIStateManager;
import android.animation.AnimatorSet;
import android.view.View;
import android.content.Context;
import com.navdy.hud.app.gesture.GestureDetector;
import com.navdy.hud.app.manager.InputManager;

public interface INotification extends IInputHandler, GestureListener
{
    boolean canAddToStackIfCurrentExists();
    
    boolean expandNotification();
    
    int getColor();
    
    View getExpandedView(final Context p0, final Object p1);
    
    int getExpandedViewIndicatorColor();
    
    String getId();
    
    int getTimeout();
    
    NotificationType getType();
    
    View getView(final Context p0);
    
    AnimatorSet getViewSwitchAnimation(final boolean p0);
    
    boolean isAlive();
    
    boolean isPurgeable();
    
    void onExpandedNotificationEvent(final UIStateManager.Mode p0);
    
    void onExpandedNotificationSwitched();
    
    void onNotificationEvent(final UIStateManager.Mode p0);
    
    void onStart(final INotificationController p0);
    
    void onStop();
    
    void onUpdate();
    
    boolean supportScroll();
}
