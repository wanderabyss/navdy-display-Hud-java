package com.navdy.hud.app.framework.music;

import com.navdy.service.library.events.input.GestureEvent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.ViewGroup.MarginLayoutParams;
import butterknife.ButterKnife;
import mortar.Mortar;
import android.view.View;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import android.util.AttributeSet;
import android.content.Context;
import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;
import javax.inject.Inject;
import android.widget.TextView;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.manager.InputManager;
import android.widget.RelativeLayout;

public class MusicDetailsView extends RelativeLayout implements IInputHandler
{
    private static final int musicRightContainerLeftMargin;
    private static final int musicRightContainerSize;
    private static final Logger sLogger;
    ImageView albumArt;
    ViewGroup albumArtContainer;
    private VerticalMenuComponent.Callback callback;
    TextView counter;
    @Inject
    public MusicDetailsScreen.Presenter presenter;
    VerticalMenuComponent vmenuComponent;
    
    static {
        sLogger = new Logger(MusicDetailsView.class);
        final Resources resources = HudApplication.getAppContext().getResources();
        musicRightContainerSize = resources.getDimensionPixelSize(R.dimen.music_details_rightC_w);
        musicRightContainerLeftMargin = resources.getDimensionPixelSize(R.dimen.music_details_rightC_left_margin);
    }
    
    public MusicDetailsView(final Context context) {
        this(context, null);
    }
    
    public MusicDetailsView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public MusicDetailsView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.callback = new VerticalMenuComponent.Callback() {
            @Override
            public void close() {
                MusicDetailsView.this.presenter.close();
            }
            
            @Override
            public boolean isClosed() {
                return MusicDetailsView.this.presenter.isClosed();
            }
            
            @Override
            public boolean isItemClickable(final VerticalList.ItemSelectionState itemSelectionState) {
                return MusicDetailsView.this.presenter.isItemClickable(itemSelectionState);
            }
            
            @Override
            public void onBindToView(final VerticalList.Model model, final View view, final int n, final VerticalList.ModelState modelState) {
            }
            
            @Override
            public void onFastScrollEnd() {
            }
            
            @Override
            public void onFastScrollStart() {
            }
            
            @Override
            public void onItemSelected(final VerticalList.ItemSelectionState itemSelectionState) {
            }
            
            @Override
            public void onLoad() {
                MusicDetailsView.sLogger.v("onLoad");
                MusicDetailsView.this.presenter.resetSelectedItem();
            }
            
            @Override
            public void onScrollIdle() {
            }
            
            @Override
            public void select(final VerticalList.ItemSelectionState itemSelectionState) {
                MusicDetailsView.this.presenter.selectItem(itemSelectionState);
            }
            
            @Override
            public void showToolTip() {
            }
        };
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
    }
    
    public IInputHandler nextHandler() {
        return InputManager.nextContainingHandler((View)this);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.vmenuComponent.clear();
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        this.vmenuComponent = new VerticalMenuComponent((ViewGroup)this, this.callback, true);
        this.vmenuComponent.leftContainer.setAlpha(0.0f);
        this.vmenuComponent.rightContainer.setAlpha(0.0f);
        final ViewGroup.MarginLayoutParams ViewGroup.MarginLayoutParams = (ViewGroup.MarginLayoutParams)this.vmenuComponent.rightContainer.getLayoutParams();
        ViewGroup.MarginLayoutParams.width = MusicDetailsView.musicRightContainerSize;
        ViewGroup.MarginLayoutParams.leftMargin = MusicDetailsView.musicRightContainerLeftMargin;
        final LayoutInflater from = LayoutInflater.from(this.getContext());
        this.vmenuComponent.leftContainer.removeAllViews();
        this.albumArtContainer = (ViewGroup)from.inflate(R.layout.music_details_album_art, (ViewGroup)this, false);
        this.albumArt = (ImageView)this.albumArtContainer.findViewById(R.id.albumArt);
        this.counter = (TextView)this.albumArtContainer.findViewById(R.id.counter);
        this.albumArt.setImageDrawable((Drawable)new ColorDrawable(-12303292));
        this.vmenuComponent.leftContainer.addView((View)this.albumArtContainer);
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        return this.vmenuComponent.handleGesture(gestureEvent);
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        return this.vmenuComponent.handleKey(customKeyEvent);
    }
    
    void performSelectionAnimation(final Runnable runnable, final int n) {
        this.vmenuComponent.performSelectionAnimation(runnable, n);
    }
}
