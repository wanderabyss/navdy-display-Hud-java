package com.navdy.hud.app.framework.notifications;

import com.navdy.service.library.log.Logger;

public class NotificationHelper
{
    static Logger sLogger;
    
    static {
        NotificationHelper.sLogger = NotificationManager.sLogger;
    }
    
    public static boolean isNotificationRemovable(final String s) {
        boolean b = false;
        switch (s) {
            default:
                b = true;
                return b;
            case "navdy#phone#call#notif":
            case "navdy#traffic#reroute#notif":
                return b;
        }
    }
}
