package com.navdy.hud.app.framework.glance;

import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.Context;
import java.util.HashSet;
import com.navdy.service.library.log.Logger;
import java.util.HashMap;
import android.view.View;
import java.util.ArrayList;

public class GlanceViewCache
{
    private static final boolean VERBOSE = false;
    private static ArrayList<View> bigCalendarViewCache;
    private static ArrayList<View> bigGlanceMessageSingleViewCache;
    private static ArrayList<View> bigGlanceMessageViewCache;
    private static ArrayList<View> bigMultiTextViewCache;
    private static ArrayList<View> bigTextViewCache;
    private static HashMap<ViewType, ArrayList<View>> cachedViewMap;
    private static final Logger sLogger;
    private static ArrayList<View> smallGlanceMessageViewCache;
    private static ArrayList<View> smallImageViewCache;
    private static ArrayList<View> smallSignViewCache;
    private static HashSet<View> viewSet;
    
    static {
        sLogger = new Logger(GlanceViewCache.class);
        GlanceViewCache.smallGlanceMessageViewCache = new ArrayList<View>(ViewType.SMALL_GLANCE_MESSAGE.cacheSize);
        GlanceViewCache.bigGlanceMessageViewCache = new ArrayList<View>(ViewType.BIG_GLANCE_MESSAGE.cacheSize);
        GlanceViewCache.bigGlanceMessageSingleViewCache = new ArrayList<View>(ViewType.BIG_GLANCE_MESSAGE_SINGLE.cacheSize);
        GlanceViewCache.smallImageViewCache = new ArrayList<View>(ViewType.SMALL_IMAGE.cacheSize);
        GlanceViewCache.bigTextViewCache = new ArrayList<View>(ViewType.BIG_TEXT.cacheSize);
        GlanceViewCache.bigMultiTextViewCache = new ArrayList<View>(ViewType.BIG_MULTI_TEXT.cacheSize);
        GlanceViewCache.smallSignViewCache = new ArrayList<View>(ViewType.SMALL_SIGN.cacheSize);
        GlanceViewCache.bigCalendarViewCache = new ArrayList<View>(ViewType.BIG_CALENDAR.cacheSize);
        GlanceViewCache.cachedViewMap = new HashMap<ViewType, ArrayList<View>>();
        GlanceViewCache.viewSet = new HashSet<View>();
        GlanceViewCache.cachedViewMap.put(ViewType.SMALL_GLANCE_MESSAGE, GlanceViewCache.smallGlanceMessageViewCache);
        GlanceViewCache.cachedViewMap.put(ViewType.BIG_GLANCE_MESSAGE, GlanceViewCache.bigGlanceMessageViewCache);
        GlanceViewCache.cachedViewMap.put(ViewType.BIG_GLANCE_MESSAGE_SINGLE, GlanceViewCache.bigGlanceMessageSingleViewCache);
        GlanceViewCache.cachedViewMap.put(ViewType.SMALL_IMAGE, GlanceViewCache.smallImageViewCache);
        GlanceViewCache.cachedViewMap.put(ViewType.BIG_TEXT, GlanceViewCache.bigTextViewCache);
        GlanceViewCache.cachedViewMap.put(ViewType.BIG_MULTI_TEXT, GlanceViewCache.bigMultiTextViewCache);
        GlanceViewCache.cachedViewMap.put(ViewType.SMALL_SIGN, GlanceViewCache.smallSignViewCache);
        GlanceViewCache.cachedViewMap.put(ViewType.BIG_CALENDAR, GlanceViewCache.bigCalendarViewCache);
    }
    
    public static void clearCache() {
        GlanceViewCache.smallGlanceMessageViewCache.clear();
        GlanceViewCache.bigGlanceMessageViewCache.clear();
        GlanceViewCache.bigGlanceMessageSingleViewCache.clear();
        GlanceViewCache.smallImageViewCache.clear();
        GlanceViewCache.bigTextViewCache.clear();
        GlanceViewCache.bigMultiTextViewCache.clear();
        GlanceViewCache.smallSignViewCache.clear();
        GlanceViewCache.bigCalendarViewCache.clear();
        GlanceViewCache.viewSet.clear();
    }
    
    public static View getView(final ViewType viewType, final Context context) {
        View view = null;
        final ArrayList<View> list = GlanceViewCache.cachedViewMap.get(viewType);
        if (list.size() > 0) {
            final View view2 = list.remove(0);
            GlanceViewCache.viewSet.remove(view2);
            view = view2;
            if (view2.getParent() != null) {
                GlanceViewCache.sLogger.e(":-/ view already has parent:" + viewType);
                view = view2;
            }
        }
        View inflate;
        if (view == null) {
            inflate = LayoutInflater.from(context).inflate(viewType.layoutId, (ViewGroup)null);
            if (GlanceViewCache.sLogger.isLoggable(2)) {
                GlanceViewCache.sLogger.v(" creating view for " + viewType);
                inflate = inflate;
            }
        }
        else {
            inflate = view;
            if (GlanceViewCache.sLogger.isLoggable(2)) {
                GlanceViewCache.sLogger.v(" reusing cache for " + viewType);
                inflate = view;
            }
        }
        return inflate;
    }
    
    public static void putView(final ViewType viewType, final View view) {
        if (view != null) {
            if (GlanceViewCache.viewSet.contains(view)) {
                GlanceViewCache.sLogger.e(":-/ view already in cache:" + viewType);
            }
            else {
                if (view.getParent() != null) {
                    GlanceViewCache.sLogger.e(":-/ view already has parent:" + viewType);
                }
                view.setTranslationX(0.0f);
                view.setTranslationY(0.0f);
                view.setTag(null);
                final ArrayList<View> list = GlanceViewCache.cachedViewMap.get(viewType);
                if (list.size() < viewType.cacheSize) {
                    list.add(view);
                    GlanceViewCache.viewSet.add(view);
                }
                if (GlanceViewCache.sLogger.isLoggable(2)) {
                    GlanceViewCache.sLogger.v(" putView cache for " + viewType);
                }
            }
        }
    }
    
    public static boolean supportScroll(final ViewType viewType) {
        boolean b = false;
        switch (viewType) {
            default:
                b = false;
                break;
            case BIG_GLANCE_MESSAGE:
                b = true;
                break;
        }
        return b;
    }
    
    public enum ViewType
    {
        BIG_CALENDAR(R.layout.glance_large_calendar, 2), 
        BIG_GLANCE_MESSAGE(R.layout.glance_large_message, 2), 
        BIG_GLANCE_MESSAGE_SINGLE(R.layout.glance_large_message_single, 2), 
        BIG_MULTI_TEXT(R.layout.glance_large_multi_text, 2), 
        BIG_TEXT(R.layout.glance_large_text, 2), 
        SMALL_GLANCE_MESSAGE(R.layout.glance_small_message, 2), 
        SMALL_IMAGE(R.layout.glance_small_image, 2), 
        SMALL_SIGN(R.layout.glance_small_sign, 2);
        
        final int cacheSize;
        final int layoutId;
        
        private ViewType(final int layoutId, final int cacheSize) {
            this.layoutId = layoutId;
            this.cacheSize = cacheSize;
        }
    }
}
