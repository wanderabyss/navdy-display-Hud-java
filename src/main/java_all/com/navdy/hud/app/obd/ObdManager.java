package com.navdy.hud.app.obd;

import com.navdy.hud.app.event.Wakeup;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.events.obd.ObdStatusResponse;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.obd.ObdStatusRequest;
import com.navdy.hud.app.event.DrivingStateChange;
import com.navdy.hud.app.event.DriverProfileUpdated;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.event.DriverProfileChanged;
import android.os.SystemClock;
import android.text.TextUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.squareup.wire.ProtoEnum;
import com.navdy.service.library.events.preferences.DriverProfilePreferences;
import android.os.Looper;
import mortar.Mortar;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.profile.DriverProfile;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.hud.app.maps.here.HereMapsManager;
import java.util.Iterator;
import com.navdy.obd.ICarService;
import com.navdy.obd.Pid;
import android.os.RemoteException;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.service.library.task.TaskManager;
import com.navdy.hud.app.util.os.SystemProperties;
import java.util.concurrent.TimeUnit;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.analytics.TelemetryDataManager;
import com.navdy.obd.PidSet;
import com.navdy.hud.app.manager.SpeedManager;
import android.content.SharedPreferences;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.obd.IPidListener;
import java.util.HashMap;
import android.os.Handler;
import com.navdy.obd.ScanSchedule;
import com.navdy.obd.ECU;
import java.util.List;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.debug.DriveRecorder;
import com.navdy.obd.ICanBusMonitoringListener;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public final class ObdManager
{
    public static final float BATTERY_CHARGING_VOLTAGE = 13.1f;
    public static final double BATTERY_NO_OBD = -1.0;
    private static final String BLACKLIST_MODIFICATION_TIME = "blacklist_modification_time";
    public static final double CRITICAL_BATTERY_VOLTAGE;
    private static final String CRITICAL_VOLTAGE_OVERRIDE_PROPERTY = "persist.sys.voltage.crit";
    private static final float DEFAULT_CRITICAL_BATTERY_VOLTAGE = 12.0f;
    private static final long DEFAULT_LOW_VOLTAGE_SHUTOFF_DURATION;
    private static final String DEFAULT_SET = "default_set";
    private static final int DISTANCE_UPDATE_INTERVAL = 60000;
    private static final int ENGINE_TEMP_UPDATE_INTERVAL = 2000;
    public static final String FIRMWARE_VERSION = "4.2.1";
    public static final String FIRST_ODOMETER_READING_KM = "first_odometer_reading";
    private static final int FLUSH_INTERVAL_MS = 30000;
    private static final int FUEL_UPDATE_INTERVAL = 15000;
    private static final int GENERIC_UPDATE_INTERVAL = 1000;
    public static final String LAST_ODOMETER_READING_KM = "last_odometer_reading";
    private static final int LOW_BATTERY_COUNT_THRESHOLD;
    public static final double LOW_BATTERY_VOLTAGE;
    public static final int LOW_FUEL_LEVEL = 10;
    private static final String LOW_VOLTAGE_DURATION_OVERRIDE_PROPERTY = "persist.sys.voltage.duration";
    private static final String LOW_VOLTAGE_OVERRIDE_PROPERTY = "persist.sys.voltage.low";
    private static final long LOW_VOLTAGE_SHUTOFF_DURATION;
    public static final long MINIMUM_TIME_BETWEEN_MILEAGE_REPORTING;
    private static final int MPG_UPDATE_INTERVAL = 1000;
    public static final int NOT_AVAILABLE = -1;
    private static final ObdConnectionStatusEvent OBD_CONNECTED;
    private static final ObdConnectionStatusEvent OBD_NOT_CONNECTED;
    private static final int RPM_UPDATE_INTERVAL = 250;
    public static final int SAFE_TO_SLEEP_BATTERY_OBSERVED_COUNT_THRESHOLD = 5;
    private static final String SCAN_SETTING = "scan_setting";
    private static final int SLOW_RPM_UPDATE_INTERVAL = 2500;
    private static final int SPEED_UPDATE_INTERVAL = 100;
    private static final int STATE_CONNECTED = 2;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_IDLE = 4;
    private static final int STATE_SLEEPING = 5;
    public static final String TELEMETRY_TAG = "Telemetry";
    public static final String TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS = "total_distance_travelled_with_navdy";
    public static final int VALID_FUEL_DATA_WAIT_TIME = 45000;
    public static final String VEHICLE_VIN = "vehicle_vin";
    public static final int VOLTAGE_REPORT_INTERVAL = 2000;
    private static final Logger sLogger;
    private static final Logger sTelemetryLogger;
    private static final ObdManager singleton;
    private double batteryVoltage;
    @Inject
    Bus bus;
    private ICanBusMonitoringListener canBusMonitoringListener;
    private CarServiceConnector carServiceConnector;
    private Runnable checkVoltage;
    private ConnectionType connectionType;
    private DriveRecorder driveRecorder;
    @Inject
    DriverProfileManager driverProfileManager;
    private List<ECU> ecus;
    private boolean firstScan;
    PidCheck fuelPidCheck;
    private ScanSchedule fullScan;
    private Handler handler;
    private boolean isCheckEngineLightOn;
    private int lowBatteryCount;
    private double maxBatteryVoltage;
    private long mileageEventLastReportTime;
    private double minBatteryVoltage;
    private ScanSchedule minimalScan;
    private ObdCanBusRecordingPolicy obdCanBusRecordingPolicy;
    private String obdChipFirmwareVersion;
    private volatile boolean obdConnected;
    private ObdDeviceConfigurationManager obdDeviceConfigurationManager;
    private HashMap<Integer, Double> obdPidsCache;
    private boolean odoMeterReadingValidated;
    private Runnable periodicCheckRunnable;
    private IPidListener pidListener;
    @Inject
    PowerManager powerManager;
    private String protocol;
    private IPidListener recordingPidListener;
    private int safeToSleepBatteryLevelObservedCount;
    private ScanSchedule schedule;
    @Inject
    SharedPreferences sharedPreferences;
    private SpeedManager speedManager;
    private PidSet supportedPidSet;
    @Inject
    public TelemetryDataManager telemetryDataManager;
    private long totalDistanceInCurrentSessionPersisted;
    @Inject
    TripManager tripManager;
    private List<String> troubleCodes;
    private String vin;
    
    static {
        DEFAULT_LOW_VOLTAGE_SHUTOFF_DURATION = TimeUnit.MINUTES.toMillis(15L);
        LOW_BATTERY_VOLTAGE = SystemProperties.getFloat("persist.sys.voltage.low", 12.2f);
        CRITICAL_BATTERY_VOLTAGE = SystemProperties.getFloat("persist.sys.voltage.crit", 12.0f);
        LOW_VOLTAGE_SHUTOFF_DURATION = SystemProperties.getLong("persist.sys.voltage.duration", ObdManager.DEFAULT_LOW_VOLTAGE_SHUTOFF_DURATION);
        MINIMUM_TIME_BETWEEN_MILEAGE_REPORTING = TimeUnit.MINUTES.toMillis(10L);
        sLogger = new Logger(ObdManager.class);
        sTelemetryLogger = new Logger("Telemetry");
        OBD_CONNECTED = new ObdConnectionStatusEvent(true);
        OBD_NOT_CONNECTED = new ObdConnectionStatusEvent(false);
        LOW_BATTERY_COUNT_THRESHOLD = (int)Math.ceil(ObdManager.LOW_VOLTAGE_SHUTOFF_DURATION / 2000.0);
        singleton = new ObdManager();
    }
    
    private ObdManager() {
        this.odoMeterReadingValidated = false;
        this.totalDistanceInCurrentSessionPersisted = 0L;
        this.mileageEventLastReportTime = 0L;
        this.connectionType = ConnectionType.POWER_ONLY;
        this.supportedPidSet = new PidSet();
        this.speedManager = SpeedManager.getInstance();
        this.obdPidsCache = new HashMap<Integer, Double>();
        this.batteryVoltage = -1.0;
        this.minBatteryVoltage = -1.0;
        this.maxBatteryVoltage = -1.0;
        this.safeToSleepBatteryLevelObservedCount = 0;
        this.isCheckEngineLightOn = false;
        this.periodicCheckRunnable = new Runnable() {
            @Override
            public void run() {
                TaskManager.getInstance().execute(ObdManager.this.checkVoltage, 1);
                ObdManager.this.handler.postDelayed((Runnable)this, 2000L);
            }
        };
        this.checkVoltage = new Runnable() {
            @Override
            public void run() {
                final double access$400 = ObdManager.this.batteryVoltage;
                ObdManager.this.batteryVoltage = ObdManager.this.getBatteryVoltage();
                if (ObdManager.this.batteryVoltage != -1.0 && access$400 == -1.0) {
                    ObdManager.this.minBatteryVoltage = ObdManager.this.batteryVoltage;
                    AnalyticsSupport.recordStartingBatteryVoltage(ObdManager.this.batteryVoltage);
                }
                if (ObdManager.this.batteryVoltage == 0.0 || ObdManager.this.batteryVoltage == -1.0) {
                    if (ObdManager.this.lowBatteryCount != 0) {
                        ObdManager.sLogger.d("Battery level data is not available");
                    }
                    ObdManager.this.lowBatteryCount = 0;
                    ObdManager.this.safeToSleepBatteryLevelObservedCount = 0;
                }
                else {
                    if (ObdManager.this.batteryVoltage < ObdManager.this.minBatteryVoltage) {
                        ObdManager.this.minBatteryVoltage = ObdManager.this.batteryVoltage;
                    }
                    if (ObdManager.this.batteryVoltage > ObdManager.this.maxBatteryVoltage) {
                        ObdManager.this.maxBatteryVoltage = ObdManager.this.batteryVoltage;
                    }
                    double n;
                    if (ObdManager.this.powerManager.inQuietMode()) {
                        n = ObdManager.LOW_BATTERY_VOLTAGE;
                    }
                    else {
                        n = ObdManager.CRITICAL_BATTERY_VOLTAGE;
                    }
                    if (ObdManager.this.batteryVoltage >= 13.100000381469727 && ObdManager.this.powerManager.inQuietMode()) {
                        ObdManager.sLogger.d("Battery seems to be charging, waking up");
                        ObdManager.this.powerManager.wakeUp(AnalyticsSupport.WakeupReason.VOLTAGE_SPIKE);
                        ObdManager.this.safeToSleepBatteryLevelObservedCount = 0;
                    }
                    else if (ObdManager.this.batteryVoltage < n) {
                        ObdManager.this.safeToSleepBatteryLevelObservedCount = 0;
                        ObdManager.sLogger.d("Battery voltage (LOW) : " + ObdManager.this.batteryVoltage);
                        ObdManager.this.lowBatteryCount++;
                        if (ObdManager.this.lowBatteryCount > ObdManager.LOW_BATTERY_COUNT_THRESHOLD) {
                            while (true) {
                                ObdManager.sLogger.d("Battery Voltage is too low, shutting down");
                                while (true) {
                                    Label_0448: {
                                        try {
                                            if (!ObdManager.this.powerManager.inQuietMode()) {
                                                break Label_0448;
                                            }
                                            final Shutdown.Reason reason = Shutdown.Reason.LOW_VOLTAGE;
                                            ObdManager.this.sleep(true);
                                            ObdManager.this.bus.post(new Shutdown(reason));
                                        }
                                        catch (Exception ex) {
                                            ObdManager.sLogger.e("error invoking ShutdownMonitor.androidShutdown(): " + ex);
                                        }
                                        break;
                                    }
                                    final Shutdown.Reason reason = Shutdown.Reason.CRITICAL_VOLTAGE;
                                    continue;
                                }
                            }
                        }
                    }
                    else {
                        ObdManager.this.lowBatteryCount = 0;
                        if (ObdManager.this.powerManager.inQuietMode()) {
                            ObdManager.sLogger.d("In Quiet mode , Battery voltage : " + ObdManager.this.batteryVoltage);
                            ObdManager.this.safeToSleepBatteryLevelObservedCount++;
                            if (ObdManager.this.safeToSleepBatteryLevelObservedCount >= 5) {
                                ObdManager.sLogger.d("Safe to put obd chip to sleep, invoking sleep");
                                ObdManager.this.safeToSleepBatteryLevelObservedCount = 0;
                                ObdManager.this.handler.removeCallbacks(ObdManager.this.periodicCheckRunnable);
                                ObdManager.this.sleep(false);
                            }
                        }
                    }
                }
            }
        };
        this.pidListener = new IPidListener.Stub() {
            public void onConnectionStateChange(final int n) throws RemoteException {
                ObdManager.this.updateConnectionState(n);
            }
            
            public void pidsChanged(final List<Pid> list) throws RemoteException {
            }
            
            public void pidsRead(final List<Pid> list, List<Pid> bus) throws RemoteException {
                while (true) {
                Label_0517:
                    while (true) {
                        Label_0506: {
                            while (true) {
                                Label_0499: {
                                    synchronized (this) {
                                        if (!ObdManager.this.firstScan || bus == null) {
                                            break Label_0438;
                                        }
                                        ObdManager.this.firstScan = false;
                                        try {
                                            final ICarService carApi = ObdManager.this.carServiceConnector.getCarApi();
                                            ObdManager.this.vin = carApi.getVIN();
                                            ObdManager.this.onVinRead(ObdManager.this.vin);
                                            ObdManager.this.ecus = carApi.getEcus();
                                            ObdManager.this.protocol = carApi.getProtocol();
                                            Object supportedPids = ObdManager.this.carServiceConnector.getCarApi().getSupportedPids();
                                            ObdManager.sLogger.d("First scan, got the supported PIDS");
                                            if (supportedPids != null) {
                                                final Object access$700 = ObdManager.sLogger;
                                                ((Logger)access$700).d("Supported PIDS size :" + ((List)supportedPids).size());
                                            }
                                            else {
                                                ObdManager.sLogger.d("First scan, got the supported PIDS , list null");
                                            }
                                            if (supportedPids != null) {
                                                final Object access$700 = new PidSet((List<Pid>)supportedPids);
                                                if (((PidSet)access$700).equals(ObdManager.this.supportedPidSet)) {
                                                    break Label_0506;
                                                }
                                                final Object o = ObdManager.sTelemetryLogger;
                                                final StringBuilder append = new StringBuilder().append("SUPPORTED VIN: ");
                                                if (ObdManager.this.vin == null) {
                                                    break Label_0499;
                                                }
                                                final String access$701 = ObdManager.this.vin;
                                                ((Logger)o).v(append.append(access$701).append(", ").append("FUEL: ").append(((PidSet)access$700).contains(47)).append(", ").append("SPEED: ").append(((PidSet)access$700).contains(13)).append(", ").append("RPM: ").append(((PidSet)access$700).contains(12)).append(", ").append("DIST: ").append(((PidSet)access$700).contains(49)).append(", ").append("MAF: ").append(((PidSet)access$700).contains(16)).append(", ").append("IFC(LPKKM): ").append(((List)supportedPids).contains(256)).toString());
                                                ObdManager.this.supportedPidSet = (PidSet)access$700;
                                                ObdManager.this.fuelPidCheck.reset();
                                                final Bus bus2 = ObdManager.this.bus;
                                                supportedPids = new ObdSupportedPidsChangedEvent();
                                                bus2.post(supportedPids);
                                                ObdManager.sLogger.v("pid-changed event sent");
                                            }
                                            if (bus == null) {
                                                bus = ObdManager.this.bus;
                                                ((Bus)bus).post(new ObdPidReadEvent(list));
                                                return;
                                            }
                                            break Label_0517;
                                        }
                                        catch (Throwable t) {
                                            ObdManager.sLogger.e(t);
                                        }
                                    }
                                }
                                final String access$701 = "";
                                continue;
                            }
                        }
                        ObdManager.sLogger.v("pid-changed event not sent");
                        continue;
                    }
                    final Iterator<Pid> iterator = ((List<Pid>)bus).iterator();
                    while (iterator.hasNext()) {
                        final Object access$700 = iterator.next();
                        switch (((Pid)access$700).getId()) {
                            default:
                                ObdManager.this.obdPidsCache.put(((Pid)access$700).getId(), ((Pid)access$700).getValue());
                                break;
                            case 13:
                                ObdManager.this.speedManager.setObdSpeed((int)((Pid)access$700).getValue(), ((Pid)access$700).getTimeStamp());
                                break;
                            case 47:
                                ObdManager.this.fuelPidCheck.checkPid((int)((Pid)access$700).getValue());
                                if (!ObdManager.this.fuelPidCheck.hasIncorrectData()) {
                                    ObdManager.this.obdPidsCache.put(((Pid)access$700).getId(), ((Pid)access$700).getValue());
                                    break;
                                }
                                ObdManager.this.obdPidsCache.remove(47);
                                break;
                            case 49:
                                ObdManager.this.obdPidsCache.put(((Pid)access$700).getId(), ((Pid)access$700).getValue());
                                ObdManager.this.updateCumulativeVehicleDistanceTravelled((long)((Pid)access$700).getValue());
                                break;
                        }
                        final Object o = ObdManager.this.bus;
                        final Object supportedPids = new ObdPidChangeEvent((Pid)access$700);
                        ((Bus)o).post(supportedPids);
                    }
                    if (ObdManager.this.recordingPidListener != null && bus != null) {
                        ObdManager.this.recordingPidListener.pidsRead(list, (List<Pid>)bus);
                    }
                    final Bus bus3 = ObdManager.this.bus;
                    bus = new ObdPidReadEvent(list);
                    bus3.post(bus);
                }
            }
        };
        this.canBusMonitoringListener = new ICanBusMonitoringListener.Stub() {
            public int getGpsSpeed() throws RemoteException {
                return Math.round(ObdManager.this.speedManager.getGpsSpeedInMetersPerSecond());
            }
            
            public double getLatitude() throws RemoteException {
                if (!HereMapsManager.getInstance().isInitialized()) {
                    return -1.0;
                }
                final GeoCoordinate lastGeoCoordinate = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                if (lastGeoCoordinate == null) {
                    return -1.0;
                }
                return lastGeoCoordinate.getLatitude();
                latitude = -1.0;
                return latitude;
            }
            
            public double getLongitude() throws RemoteException {
                if (!HereMapsManager.getInstance().isInitialized()) {
                    return -1.0;
                }
                final GeoCoordinate lastGeoCoordinate = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                if (lastGeoCoordinate == null) {
                    return -1.0;
                }
                return lastGeoCoordinate.getLongitude();
                longitude = -1.0;
                return longitude;
            }
            
            public String getMake() throws RemoteException {
                if (ObdManager.this.driverProfileManager == null) {
                    return "";
                }
                final DriverProfile currentProfile = ObdManager.this.driverProfileManager.getCurrentProfile();
                if (currentProfile == null) {
                    return "";
                }
                return currentProfile.getCarMake();
                carMake = "";
                return carMake;
            }
            
            public String getModel() throws RemoteException {
                if (ObdManager.this.driverProfileManager == null) {
                    return "";
                }
                final DriverProfile currentProfile = ObdManager.this.driverProfileManager.getCurrentProfile();
                if (currentProfile == null) {
                    return "";
                }
                return currentProfile.getCarModel();
                carModel = "";
                return carModel;
            }
            
            public String getYear() throws RemoteException {
                if (ObdManager.this.driverProfileManager == null) {
                    return "";
                }
                final DriverProfile currentProfile = ObdManager.this.driverProfileManager.getCurrentProfile();
                if (currentProfile == null) {
                    return "";
                }
                return currentProfile.getCarYear();
                carYear = "";
                return carYear;
            }
            
            public boolean isMonitoringLimitReached() throws RemoteException {
                return ObdManager.this.obdCanBusRecordingPolicy.isCanBusMonitoringLimitReached();
            }
            
            public void onCanBusMonitorSuccess(final int n) throws RemoteException {
                ObdManager.sLogger.d("onCanBusMonitorSuccess , Average amount of data : " + n);
                ObdManager.this.obdCanBusRecordingPolicy.onCanBusMonitorSuccess(n);
            }
            
            public void onCanBusMonitoringError(final String s) throws RemoteException {
                ObdManager.sLogger.d("onCanBusMonitoringError, Error : " + s);
                ObdManager.this.obdCanBusRecordingPolicy.onCanBusMonitoringFailed();
            }
            
            public void onNewDataAvailable(final String s) throws RemoteException {
                ObdManager.sLogger.d("onNewDataAvailable : " + s);
                ObdManager.this.obdCanBusRecordingPolicy.onNewDataAvailable(s);
            }
        };
        Mortar.inject(HudApplication.getAppContext(), this);
        this.obdCanBusRecordingPolicy = new ObdCanBusRecordingPolicy(HudApplication.getAppContext(), this.sharedPreferences, this, this.tripManager);
        this.carServiceConnector = new CarServiceConnector(HudApplication.getAppContext());
        this.obdDeviceConfigurationManager = new ObdDeviceConfigurationManager(this.carServiceConnector);
        (this.minimalScan = new ScanSchedule()).addPid(13, 100);
        this.minimalScan.addPid(47, 15000);
        this.minimalScan.addPid(49, 60000);
        (this.fullScan = new ScanSchedule(this.minimalScan)).addPid(12, 250);
        this.fullScan.addPid(16, 1000);
        this.fullScan.addPid(256, 1000);
        this.fullScan.addPid(5, 2000);
        this.fullScan.addPid(258, 1000);
        this.fullScan.addPid(259, 1000);
        this.fullScan.addPid(260, 1000);
        this.minimalScan.addPid(12, 2500);
        this.bus.register(this);
        this.bus.register(this.obdDeviceConfigurationManager);
        this.bus.register(this.obdCanBusRecordingPolicy);
        ObdManager.sTelemetryLogger.v("SESSION started");
        this.handler = new Handler(Looper.getMainLooper());
        this.fuelPidCheck = (PidCheck)new PidCheck(47, this.handler) {
            @Override
            public long getWaitForValidDataTimeout() {
                return 45000L;
            }
            
            @Override
            public void invalidatePid(final int n) {
                ObdManager.this.obdPidsCache.remove(47);
                if (ObdManager.this.supportedPidSet != null) {
                    ObdManager.this.supportedPidSet.remove(47);
                    ObdManager.this.bus.post(new ObdSupportedPidsChangedEvent());
                }
                AnalyticsSupport.recordIncorrectFuelData();
            }
            
            @Override
            public boolean isPidValueValid(final double n) {
                return n > 0.0;
            }
        };
        this.handler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        Logger.flush();
                    }
                }, 6);
                ObdManager.this.handler.postDelayed((Runnable)this, 30000L);
            }
        });
    }
    
    public static ObdManager getInstance() {
        return ObdManager.singleton;
    }
    
    private DriverProfilePreferences.ObdScanSetting getScanSetting(final SharedPreferences sharedPreferences) {
        final DriverProfilePreferences.ObdScanSetting scan_DEFAULT_ON = DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_ON;
        try {
            final ProtoEnum value = DriverProfilePreferences.ObdScanSetting.valueOf(sharedPreferences.getString("scan_setting", scan_DEFAULT_ON.name()));
            return (DriverProfilePreferences.ObdScanSetting)value;
        }
        catch (Exception ex) {
            ObdManager.sLogger.e("Failed to parse saved scan setting ", ex);
            final ProtoEnum value = scan_DEFAULT_ON;
            return (DriverProfilePreferences.ObdScanSetting)value;
        }
    }
    
    private void onVinRead(final String s) {
        while (true) {
            synchronized (this) {
                final SharedPreferences sharedPreferences = RemoteDeviceManager.getInstance().getSharedPreferences();
                final String string = sharedPreferences.getString("vehicle_vin", (String)null);
                if (!TextUtils.equals((CharSequence)s, (CharSequence)string)) {
                    ObdManager.sLogger.d("Vin changed , Old :" + string + ", New Vin :" + s);
                    if (TextUtils.isEmpty((CharSequence)s)) {
                        sharedPreferences.edit().remove("vehicle_vin").apply();
                    }
                    else {
                        sharedPreferences.edit().putString("vehicle_vin", s).apply();
                    }
                    this.resetCumulativeMileageData();
                    return;
                }
            }
            final String s2;
            ObdManager.sLogger.d("Vin has not changed , Vin " + s2);
        }
    }
    
    private void reportMileageEvent() {
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        if (this.mileageEventLastReportTime == 0L || elapsedRealtime - this.mileageEventLastReportTime > ObdManager.MINIMUM_TIME_BETWEEN_MILEAGE_REPORTING) {
            final SharedPreferences sharedPreferences = RemoteDeviceManager.getInstance().getSharedPreferences();
            final long long1 = sharedPreferences.getLong("first_odometer_reading", 0L);
            final long long2 = sharedPreferences.getLong("last_odometer_reading", 0L);
            final double n = long2 - long1;
            ObdManager.sLogger.d("Initial reading (KM) : " + long1 + ", Current reading (KM) : " + long2 + " , Total (KM) : " + n);
            final double n2 = sharedPreferences.getLong("total_distance_travelled_with_navdy", 0L);
            ObdManager.sLogger.d("Distance travelled with Navdy(Meters) : " + n2);
            final double n3 = n2 / 1000.0;
            if (n > 0.0 && n3 > 0.0) {
                final double n4 = n / n;
                ObdManager.sLogger.d("Reporting the Navdy Mileage Vehicle (KMs) : " + n + " , Navdy (KMs) : " + n + " , Navdy Usage rate " + n4);
                AnalyticsSupport.recordNavdyMileageEvent(n, n3, n4);
                this.mileageEventLastReportTime = elapsedRealtime;
            }
        }
    }
    
    private void resetCumulativeMileageData() {
        final SharedPreferences sharedPreferences = RemoteDeviceManager.getInstance().getSharedPreferences();
        ObdManager.sLogger.d("Resetting the cumulative mileage statistics");
        sharedPreferences.edit().putLong("first_odometer_reading", 0L).putLong("last_odometer_reading", 0L).putLong("total_distance_travelled_with_navdy", 0L).apply();
        this.totalDistanceInCurrentSessionPersisted = 0L;
        this.odoMeterReadingValidated = false;
    }
    
    private void setConnected(final boolean obdConnected) {
        if (this.obdConnected == obdConnected) {
            return;
        }
        if (this.driveRecorder != null) {
            this.driveRecorder.setRealObdConnected(obdConnected);
        }
        this.fuelPidCheck.reset();
        this.obdDeviceConfigurationManager.setConnectionState(obdConnected);
        this.obdConnected = obdConnected;
    Label_0156:
        while (true) {
            Label_0197: {
                if (!obdConnected) {
                    break Label_0197;
                }
                while (true) {
                    while (true) {
                        Label_0250: {
                            try {
                                final ICarService carApi = this.carServiceConnector.getCarApi();
                                this.onVinRead(this.vin = carApi.getVIN());
                                this.ecus = carApi.getEcus();
                                this.protocol = carApi.getProtocol();
                                this.isCheckEngineLightOn = carApi.isCheckEngineLightOn();
                                this.troubleCodes = carApi.getTroubleCodes();
                                this.obdChipFirmwareVersion = carApi.getObdChipFirmwareVersion();
                                ObdManager.sLogger.d("Connected, Obd chip firmware version " + this.obdChipFirmwareVersion);
                                final Bus bus = this.bus;
                                if (obdConnected) {
                                    final ObdConnectionStatusEvent obdConnectionStatusEvent = ObdManager.OBD_CONNECTED;
                                    bus.post(obdConnectionStatusEvent);
                                    this.obdCanBusRecordingPolicy.onObdConnectionStateChanged(obdConnected);
                                    return;
                                }
                                break Label_0250;
                            }
                            catch (Throwable t) {
                                ObdManager.sLogger.d("Failed to read vin/ecu/protocol after connecting", t);
                                continue Label_0156;
                            }
                            break;
                        }
                        final ObdConnectionStatusEvent obdConnectionStatusEvent = ObdManager.OBD_NOT_CONNECTED;
                        continue;
                    }
                }
            }
            this.firstScan = true;
            this.vin = null;
            this.ecus = null;
            this.protocol = null;
            this.speedManager.setObdSpeed(-1, 0L);
            if (this.supportedPidSet != null) {
                this.supportedPidSet.clear();
            }
            this.obdPidsCache.clear();
            continue Label_0156;
        }
    }
    
    private void setScanSchedule(final ScanSchedule schedule) {
        if (schedule != this.schedule) {
            this.schedule = schedule;
            this.updateListener();
        }
    }
    
    private void updateConnectionState(final int n) {
        if (n == 2) {
            this.setConnected(true);
        }
        else {
            this.setConnected(false);
        }
        if (this.connectionType == ConnectionType.POWER_ONLY && n > 1) {
            this.connectionType = ConnectionType.OBD;
        }
    }
    
    private void updateCumulativeVehicleDistanceTravelled(long totalDistanceInCurrentSessionPersisted) {
        // monitorenter(this)
        if (totalDistanceInCurrentSessionPersisted <= 0L) {
            return;
        }
        try {
            ObdManager.sLogger.d("New odometer reading (KMs) : " + totalDistanceInCurrentSessionPersisted);
            final SharedPreferences sharedPreferences = RemoteDeviceManager.getInstance().getSharedPreferences();
            if (!this.odoMeterReadingValidated) {
                ObdManager.sLogger.d("Validating the Cumulative mileage data");
                final double n = sharedPreferences.getLong("last_odometer_reading", 0L);
                if (n == 0.0) {
                    ObdManager.sLogger.d("No saved odo meter reading, starting fresh");
                    this.resetCumulativeMileageData();
                    sharedPreferences.edit().putLong("first_odometer_reading", totalDistanceInCurrentSessionPersisted).apply();
                }
                else if (totalDistanceInCurrentSessionPersisted < n) {
                    ObdManager.sLogger.d("Odometer reading mismatch with saved reading, resetting, Saved :" + n + ", New : " + totalDistanceInCurrentSessionPersisted);
                    this.resetCumulativeMileageData();
                    sharedPreferences.edit().putLong("first_odometer_reading", totalDistanceInCurrentSessionPersisted).apply();
                }
                this.odoMeterReadingValidated = true;
            }
            sharedPreferences.edit().putLong("last_odometer_reading", totalDistanceInCurrentSessionPersisted).apply();
            final TripManager tripManager = RemoteDeviceManager.getInstance().getTripManager();
            final long long1 = sharedPreferences.getLong("total_distance_travelled_with_navdy", 0L);
            totalDistanceInCurrentSessionPersisted = tripManager.getTotalDistanceTravelled();
            ObdManager.sLogger.d("Saving total distance travelled in current session, before :" + long1 + ", Current Trip mileage : " + tripManager.getTotalDistanceTravelled());
            final long n2 = totalDistanceInCurrentSessionPersisted - this.totalDistanceInCurrentSessionPersisted;
            if (n2 > 0L) {
                sharedPreferences.edit().putLong("total_distance_travelled_with_navdy", long1 + n2).apply();
                this.totalDistanceInCurrentSessionPersisted = totalDistanceInCurrentSessionPersisted;
                this.reportMileageEvent();
            }
        }
        finally {
        }
        // monitorexit(this)
    }
    
    private void updateObdScanSetting() {
        if (this.driverProfileManager != null) {
            final DriverProfile currentProfile = this.driverProfileManager.getCurrentProfile();
            if (currentProfile != null) {
                if (!currentProfile.isAutoOnEnabled()) {
                    this.obdDeviceConfigurationManager.setAutoOnEnabled(false);
                }
                else if (!TextUtils.isEmpty((CharSequence)currentProfile.getCarMake()) || !TextUtils.isEmpty((CharSequence)currentProfile.getCarModel()) || !TextUtils.isEmpty((CharSequence)currentProfile.getCarYear())) {
                    this.obdCanBusRecordingPolicy.onCarDetailsAvailable(currentProfile.getCarMake(), currentProfile.getCarModel(), currentProfile.getCarYear());
                    this.obdDeviceConfigurationManager.setCarDetails(currentProfile.getCarMake(), currentProfile.getCarModel(), currentProfile.getCarYear());
                }
                final DriverProfilePreferences.ObdScanSetting obdScanSetting = currentProfile.getObdScanSetting();
                ObdManager.sLogger.d("ObdScanSetting received : " + obdScanSetting);
                if (obdScanSetting != null) {
                    boolean b = false;
                    final SharedPreferences sharedPreferences = RemoteDeviceManager.getInstance().getSharedPreferences();
                    ProtoEnum protoEnum;
                    if (sharedPreferences.getBoolean("default_set", false)) {
                        b = true;
                        if (this.isObdScanningEnabled()) {
                            protoEnum = DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_ON;
                        }
                        else {
                            protoEnum = DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_OFF;
                        }
                        sharedPreferences.edit().remove("default_set").apply();
                    }
                    else {
                        protoEnum = this.getScanSetting(sharedPreferences);
                    }
                    final long obdBlacklistModificationTime = currentProfile.getObdBlacklistModificationTime();
                    final long long1 = sharedPreferences.getLong("blacklist_modification_time", 0L);
                    final boolean b2 = obdBlacklistModificationTime > long1;
                    ObdManager.sLogger.d("saved setting:" + protoEnum + " blacklist updated: (" + obdBlacklistModificationTime + "/" + long1 + ") (phone/local)");
                    ObdManager.sLogger.d("Is Obd enabled :" + this.isObdScanningEnabled());
                    final boolean b3 = protoEnum == DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_OFF || protoEnum == DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_ON;
                    int n = 0;
                    switch (obdScanSetting) {
                        default:
                            n = (b ? 1 : 0);
                            break;
                        case SCAN_DEFAULT_ON:
                            n = (b ? 1 : 0);
                            if (!b2) {
                                break;
                            }
                            n = (b ? 1 : 0);
                            if (b3) {
                                ObdManager.sLogger.d("Setting default to on");
                                this.enableObdScanning(true);
                                n = 1;
                                break;
                            }
                            break;
                        case SCAN_ON:
                            this.enableObdScanning(true);
                            n = 1;
                            break;
                        case SCAN_OFF:
                            this.enableObdScanning(false);
                            n = 1;
                            break;
                        case SCAN_DEFAULT_OFF:
                            n = (b ? 1 : 0);
                            if (!b2) {
                                break;
                            }
                            n = (b ? 1 : 0);
                            if (b3) {
                                this.enableObdScanning(false);
                                n = 1;
                                break;
                            }
                            break;
                    }
                    if (n != 0) {
                        ObdManager.sLogger.i("Writing scan setting:" + obdScanSetting + " modTime:" + obdBlacklistModificationTime);
                        sharedPreferences.edit().putString("scan_setting", obdScanSetting.name()).putLong("blacklist_modification_time", obdBlacklistModificationTime).apply();
                    }
                }
            }
        }
    }
    
    public void enableInstantaneousMode(final boolean b) {
        this.obdCanBusRecordingPolicy.onInstantaneousModeChanged(b);
        if (b) {
            this.setScanSchedule(this.fullScan);
        }
        else if (DriveRecorder.isAutoRecordingEnabled()) {
            this.setScanSchedule(this.fullScan);
        }
        else {
            this.setScanSchedule(this.minimalScan);
        }
    }
    
    public void enableObdScanning(final boolean obdPidsScanningEnabled) {
        final ICarService carApi = this.carServiceConnector.getCarApi();
        if (carApi == null) {
            return;
        }
        try {
            ObdManager.sLogger.i("setting obd scanning enabled:" + obdPidsScanningEnabled);
            carApi.setObdPidsScanningEnabled(obdPidsScanningEnabled);
        }
        catch (RemoteException ex) {
            final Logger sLogger = ObdManager.sLogger;
            final StringBuilder append = new StringBuilder().append("Error while ");
            String s;
            if (obdPidsScanningEnabled) {
                s = "enabling ";
            }
            else {
                s = "disabling ";
            }
            sLogger.e(append.append(s).append("Obd PIDs scanning ").toString(), (Throwable)ex);
        }
    }
    
    public double getBatteryVoltage() {
        final ICarService carApi = this.carServiceConnector.getCarApi();
        Label_0022: {
            if (carApi == null) {
                break Label_0022;
            }
            try {
                this.batteryVoltage = carApi.getBatteryVoltage();
                return this.batteryVoltage;
            }
            catch (Throwable t) {
                ObdManager.sLogger.e(t);
                return this.batteryVoltage;
            }
        }
    }
    
    public ICarService getCarService() {
        ICarService carApi;
        if (this.carServiceConnector != null) {
            carApi = this.carServiceConnector.getCarApi();
        }
        else {
            carApi = null;
        }
        return carApi;
    }
    
    public ConnectionType getConnectionType() {
        return this.connectionType;
    }
    
    public int getDistanceTravelled() {
        return (int)this.getPidValue(49);
    }
    
    public long getDistanceTravelledWithNavdy() {
        return RemoteDeviceManager.getInstance().getSharedPreferences().getLong("total_distance_travelled_with_navdy", -1L);
    }
    
    public DriveRecorder getDriveRecorder() {
        return this.driveRecorder;
    }
    
    public List<ECU> getEcus() {
        return this.ecus;
    }
    
    public int getEngineRpm() {
        return (int)this.getPidValue(12);
    }
    
    public int getFuelLevel() {
        int n;
        if (this.fuelPidCheck.hasIncorrectData || this.fuelPidCheck.isWaitingForValidData()) {
            n = -1;
        }
        else {
            n = (int)this.getPidValue(47);
        }
        return n;
    }
    
    public double getInstantFuelConsumption() {
        return (int)this.getPidValue(256);
    }
    
    public double getMaxBatteryVoltage() {
        return this.maxBatteryVoltage;
    }
    
    public double getMinBatteryVoltage() {
        return this.minBatteryVoltage;
    }
    
    public ObdCanBusRecordingPolicy getObdCanBusRecordingPolicy() {
        return this.obdCanBusRecordingPolicy;
    }
    
    public String getObdChipFirmwareVersion() {
        return this.obdChipFirmwareVersion;
    }
    
    public ObdDeviceConfigurationManager getObdDeviceConfigurationManager() {
        return this.obdDeviceConfigurationManager;
    }
    
    public double getPidValue(final int n) {
        double doubleValue;
        if (this.obdPidsCache.containsKey(n)) {
            doubleValue = this.obdPidsCache.get(n);
        }
        else {
            doubleValue = -1.0;
        }
        return doubleValue;
    }
    
    public String getProtocol() {
        return this.protocol;
    }
    
    public PidSet getSupportedPids() {
        return this.supportedPidSet;
    }
    
    public List<String> getTroubleCodes() {
        return this.troubleCodes;
    }
    
    public String getVin() {
        return this.vin;
    }
    
    public void injectObdData(final List<Pid> list, final List<Pid> list2) {
        try {
            this.pidListener.pidsRead(list, list2);
        }
        catch (RemoteException ex) {
            ObdManager.sLogger.e("Remote exception when injecting fake obd data");
        }
    }
    
    public boolean isCheckEngineLightOn() {
        return this.isCheckEngineLightOn;
    }
    
    public boolean isConnected() {
        return this.obdConnected;
    }
    
    public boolean isObdScanningEnabled() {
        final ICarService carApi = this.carServiceConnector.getCarApi();
        if (carApi == null) {
            return false;
        }
        try {
            return carApi.isObdPidsScanningEnabled();
        }
        catch (RemoteException ex) {
            ObdManager.sLogger.e("RemoteException ", (Throwable)ex);
        }
        return false;
    }
    
    public boolean isSleeping() {
        final boolean b = false;
        final ICarService carApi = this.carServiceConnector.getCarApi();
        boolean b2 = b;
        if (carApi == null) {
            return b2;
        }
        try {
            final int connectionState = carApi.getConnectionState();
            b2 = b;
            if (connectionState == 5) {
                b2 = true;
            }
            return b2;
        }
        catch (RemoteException ex) {
            b2 = b;
            return b2;
        }
    }
    
    public boolean isSpeedPidAvailable() {
        return this.supportedPidSet != null && this.supportedPidSet.contains(13);
    }
    
    public void monitorBatteryVoltage() {
        ObdManager.sLogger.i("monitorBatteryVoltage()");
        this.handler.removeCallbacks(this.periodicCheckRunnable);
        this.handler.postDelayed(this.periodicCheckRunnable, 4000L);
    }
    
    @Subscribe
    public void onDriverProfileChanged(final DriverProfileChanged driverProfileChanged) {
        this.updateObdScanSetting();
    }
    
    @Subscribe
    public void onDriverProfileUpdated(final DriverProfileUpdated driverProfileUpdated) {
        this.updateObdScanSetting();
    }
    
    @Subscribe
    public void onDrivingStateChange(final DrivingStateChange drivingStateChange) {
        if (!drivingStateChange.driving || this.isConnected()) {
            return;
        }
        final ICarService carApi = this.carServiceConnector.getCarApi();
        if (carApi == null) {
            return;
        }
        try {
            ObdManager.sLogger.i("Driving started - triggering obd scan");
            carApi.rescan();
        }
        catch (RemoteException ex) {
            ObdManager.sLogger.e("Failed to trigger OBD rescan");
        }
    }
    
    @Subscribe
    public void onObdStatusRequest(final ObdStatusRequest obdStatusRequest) {
        final int obdSpeed = this.speedManager.getObdSpeed();
        final Bus bus = this.bus;
        final RequestStatus request_SUCCESS = RequestStatus.REQUEST_SUCCESS;
        final boolean connected = this.isConnected();
        final String vin = this.vin;
        Integer value;
        if (obdSpeed != -1) {
            value = obdSpeed;
        }
        else {
            value = null;
        }
        bus.post(new RemoteEvent(new ObdStatusResponse(request_SUCCESS, connected, vin, value)));
    }
    
    @Subscribe
    public void onShutdown(final Shutdown shutdown) {
        if (shutdown.state == Shutdown.State.SHUTTING_DOWN) {
            this.carServiceConnector.shutdown();
        }
    }
    
    @Subscribe
    public void onWakeUp(final Wakeup wakeup) {
        ObdManager.sLogger.d("onWakeUp");
        this.updateListener();
    }
    
    void serviceConnected() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                ObdManager.this.updateListener();
            }
        }, 6);
    }
    
    void serviceDisconnected() {
        this.setConnected(false);
    }
    
    public void setDriveRecorder(final DriveRecorder driveRecorder) {
        this.driveRecorder = driveRecorder;
    }
    
    public void setMode(final int n, final boolean b) {
        switch (n) {
            case 0:
            case 1: {
                final ICarService carApi = this.carServiceConnector.getCarApi();
                if (carApi != null) {
                    try {
                        carApi.setMode(n, b);
                    }
                    catch (RemoteException ex) {
                        ObdManager.sLogger.e("RemoteException :" + ex);
                    }
                    break;
                }
                break;
            }
        }
    }
    
    public void setRecordingPidListener(final IPidListener recordingPidListener) {
        this.recordingPidListener = recordingPidListener;
    }
    
    public void setSupportedPidSet(final PidSet set) {
        if (this.firstScan) {
            this.firstScan = true;
            this.supportedPidSet = set;
        }
        else if (this.supportedPidSet != null) {
            this.supportedPidSet.merge(set);
        }
        else {
            this.supportedPidSet = set;
        }
        this.bus.post(new ObdSupportedPidsChangedEvent());
    }
    
    public void sleep(final boolean b) {
        final ICarService carApi = this.carServiceConnector.getCarApi();
        if (carApi == null) {
            return;
        }
        try {
            carApi.sleep(b);
        }
        catch (RemoteException ex) {
            ObdManager.sLogger.e("Error during call to sleep Remote Exception " + ex.getCause());
        }
    }
    
    public void updateFirmware(final Firmware p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: astore_2       
        //     4: new             Ljava/lang/StringBuilder;
        //     7: dup            
        //     8: invokespecial   java/lang/StringBuilder.<init>:()V
        //    11: ldc_w           "Updating the firmware of the OBD chip , Version : "
        //    14: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    17: astore_3       
        //    18: aload_1        
        //    19: getstatic       com/navdy/hud/app/obd/ObdManager$Firmware.OLD_320:Lcom/navdy/hud/app/obd/ObdManager$Firmware;
        //    22: if_acmpne       145
        //    25: ldc_w           "3.2.0"
        //    28: astore          4
        //    30: aload_2        
        //    31: aload_3        
        //    32: aload           4
        //    34: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    37: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    40: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //    43: aload_0        
        //    44: getfield        com/navdy/hud/app/obd/ObdManager.carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;
        //    47: invokevirtual   com/navdy/hud/app/obd/CarServiceConnector.getCarApi:()Lcom/navdy/obd/ICarService;
        //    50: astore          4
        //    52: invokestatic    com/navdy/hud/app/HudApplication.getAppContext:()Landroid/content/Context;
        //    55: invokevirtual   android/content/Context.getResources:()Landroid/content/res/Resources;
        //    58: aload_1        
        //    59: getfield        com/navdy/hud/app/obd/ObdManager$Firmware.resource:I
        //    62: invokevirtual   android/content/res/Resources.openRawResource:(I)Ljava/io/InputStream;
        //    65: astore_3       
        //    66: new             Ljava/lang/StringBuilder;
        //    69: dup            
        //    70: invokespecial   java/lang/StringBuilder.<init>:()V
        //    73: invokestatic    android/os/Environment.getExternalStorageDirectory:()Ljava/io/File;
        //    76: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //    79: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    82: getstatic       java/io/File.separator:Ljava/lang/String;
        //    85: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    88: ldc_w           "update.bin"
        //    91: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    94: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    97: astore_2       
        //    98: new             Ljava/io/File;
        //   101: dup            
        //   102: aload_2        
        //   103: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   106: astore_1       
        //   107: aload_1        
        //   108: invokevirtual   java/io/File.exists:()Z
        //   111: ifeq            125
        //   114: invokestatic    com/navdy/hud/app/HudApplication.getAppContext:()Landroid/content/Context;
        //   117: aload_1        
        //   118: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   121: invokestatic    com/navdy/service/library/util/IOUtils.deleteFile:(Landroid/content/Context;Ljava/lang/String;)Z
        //   124: pop            
        //   125: aload_1        
        //   126: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   129: aload_3        
        //   130: invokestatic    com/navdy/service/library/util/IOUtils.copyFile:(Ljava/lang/String;Ljava/io/InputStream;)I
        //   133: pop            
        //   134: aload           4
        //   136: ldc             "4.2.1"
        //   138: aload_2        
        //   139: invokeinterface com/navdy/obd/ICarService.updateFirmware:(Ljava/lang/String;Ljava/lang/String;)V
        //   144: return         
        //   145: ldc             "4.2.1"
        //   147: astore          4
        //   149: goto            30
        //   152: astore_1       
        //   153: getstatic       com/navdy/hud/app/obd/ObdManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   156: ldc_w           "Error writing to the file "
        //   159: aload_1        
        //   160: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   163: goto            144
        //   166: astore_1       
        //   167: getstatic       com/navdy/hud/app/obd/ObdManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   170: ldc_w           "Remote Exception while updating firmware "
        //   173: aload_1        
        //   174: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   177: goto            144
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                        
        //  -----  -----  -----  -----  ----------------------------
        //  125    134    152    166    Ljava/io/IOException;
        //  134    144    166    180    Landroid/os/RemoteException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0144:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void updateListener() {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/navdy/hud/app/obd/ObdManager.carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;
        //     4: invokevirtual   com/navdy/hud/app/obd/CarServiceConnector.getCarApi:()Lcom/navdy/obd/ICarService;
        //     7: astore_1       
        //     8: aload_1        
        //     9: ifnull          160
        //    12: aload_1        
        //    13: aload_0        
        //    14: getfield        com/navdy/hud/app/obd/ObdManager.canBusMonitoringListener:Lcom/navdy/obd/ICanBusMonitoringListener;
        //    17: invokeinterface com/navdy/obd/ICarService.setCANBusMonitoringListener:(Lcom/navdy/obd/ICanBusMonitoringListener;)V
        //    22: aload_0        
        //    23: iconst_1       
        //    24: putfield        com/navdy/hud/app/obd/ObdManager.firstScan:Z
        //    27: aload_1        
        //    28: invokeinterface com/navdy/obd/ICarService.getConnectionState:()I
        //    33: istore_2       
        //    34: iload_2        
        //    35: tableswitch {
        //                4: 204
        //                5: 64
        //                6: 204
        //                7: 175
        //          default: 64
        //        }
        //    64: aload_0        
        //    65: aload_1        
        //    66: invokeinterface com/navdy/obd/ICarService.getConnectionState:()I
        //    71: invokespecial   com/navdy/hud/app/obd/ObdManager.updateConnectionState:(I)V
        //    74: aload_0        
        //    75: invokevirtual   com/navdy/hud/app/obd/ObdManager.monitorBatteryVoltage:()V
        //    78: getstatic       com/navdy/hud/app/obd/ObdManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //    81: astore_3       
        //    82: new             Ljava/lang/StringBuilder;
        //    85: astore          4
        //    87: aload           4
        //    89: invokespecial   java/lang/StringBuilder.<init>:()V
        //    92: aload_3        
        //    93: aload           4
        //    95: ldc_w           "adding listener for "
        //    98: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   101: aload_0        
        //   102: getfield        com/navdy/hud/app/obd/ObdManager.schedule:Lcom/navdy/obd/ScanSchedule;
        //   105: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   108: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   111: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/String;)V
        //   114: aload_0        
        //   115: getfield        com/navdy/hud/app/obd/ObdManager.schedule:Lcom/navdy/obd/ScanSchedule;
        //   118: ifnull          135
        //   121: aload_1        
        //   122: aload_0        
        //   123: getfield        com/navdy/hud/app/obd/ObdManager.schedule:Lcom/navdy/obd/ScanSchedule;
        //   126: aload_0        
        //   127: getfield        com/navdy/hud/app/obd/ObdManager.pidListener:Lcom/navdy/obd/IPidListener;
        //   130: invokeinterface com/navdy/obd/ICarService.updateScan:(Lcom/navdy/obd/ScanSchedule;Lcom/navdy/obd/IPidListener;)V
        //   135: aload_0        
        //   136: getfield        com/navdy/hud/app/obd/ObdManager.obdCanBusRecordingPolicy:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;
        //   139: invokevirtual   com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.isCanBusMonitoringNeeded:()Z
        //   142: ifeq            262
        //   145: getstatic       com/navdy/hud/app/obd/ObdManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   148: ldc_w           "Start CAN bus monitoring"
        //   151: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   154: aload_1        
        //   155: invokeinterface com/navdy/obd/ICarService.startCanBusMonitoring:()V
        //   160: return         
        //   161: astore_3       
        //   162: getstatic       com/navdy/hud/app/obd/ObdManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   165: ldc_w           "RemoteException "
        //   168: aload_3        
        //   169: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   172: goto            22
        //   175: getstatic       com/navdy/hud/app/obd/ObdManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   178: ldc_w           "ObdService is in Sleeping state, waking it up"
        //   181: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   184: aload_1        
        //   185: invokeinterface com/navdy/obd/ICarService.wakeup:()V
        //   190: goto            64
        //   193: astore_1       
        //   194: getstatic       com/navdy/hud/app/obd/ObdManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   197: aload_1        
        //   198: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   201: goto            160
        //   204: aload_0        
        //   205: aload_1        
        //   206: invokeinterface com/navdy/obd/ICarService.getObdChipFirmwareVersion:()Ljava/lang/String;
        //   211: putfield        com/navdy/hud/app/obd/ObdManager.obdChipFirmwareVersion:Ljava/lang/String;
        //   214: getstatic       com/navdy/hud/app/obd/ObdManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   217: astore          4
        //   219: new             Ljava/lang/StringBuilder;
        //   222: astore_3       
        //   223: aload_3        
        //   224: invokespecial   java/lang/StringBuilder.<init>:()V
        //   227: aload           4
        //   229: aload_3        
        //   230: ldc_w           "Obd chip firmware version "
        //   233: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   236: aload_0        
        //   237: getfield        com/navdy/hud/app/obd/ObdManager.obdChipFirmwareVersion:Ljava/lang/String;
        //   240: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   243: ldc_w           ", Connection State :"
        //   246: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   249: iload_2        
        //   250: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   253: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   256: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   259: goto            64
        //   262: getstatic       com/navdy/hud/app/obd/ObdManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   265: ldc_w           "stop CAN bus monitoring"
        //   268: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   271: aload_1        
        //   272: invokeinterface com/navdy/obd/ICarService.stopCanBusMonitoring:()V
        //   277: goto            160
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                        
        //  -----  -----  -----  -----  ----------------------------
        //  12     22     161    175    Landroid/os/RemoteException;
        //  22     34     193    204    Ljava/lang/Throwable;
        //  64     135    193    204    Ljava/lang/Throwable;
        //  135    160    193    204    Ljava/lang/Throwable;
        //  175    190    193    204    Ljava/lang/Throwable;
        //  204    259    193    204    Ljava/lang/Throwable;
        //  262    277    193    204    Ljava/lang/Throwable;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0022:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void wakeup() {
        final ICarService carApi = this.carServiceConnector.getCarApi();
        if (carApi == null) {
            return;
        }
        try {
            carApi.wakeup();
        }
        catch (RemoteException ex) {
            ObdManager.sLogger.e("Error during call to sleep Remote Exception " + ex.getCause());
        }
    }
    
    public enum ConnectionType
    {
        OBD, 
        POWER_ONLY;
    }
    
    public enum Firmware
    {
        NEW_421(R.raw.update4_2_1), 
        OLD_320(R.raw.update3_2_0);
        
        public int resource;
        
        private Firmware(final int resource) {
            this.resource = resource;
        }
    }
    
    public static class ObdConnectionStatusEvent
    {
        public boolean connected;
        
        ObdConnectionStatusEvent(final boolean connected) {
            this.connected = connected;
        }
    }
    
    public static class ObdPidChangeEvent
    {
        public Pid pid;
        
        ObdPidChangeEvent(final Pid pid) {
            this.pid = pid;
        }
    }
    
    public static class ObdPidReadEvent
    {
        public PidSet readPids;
        
        public ObdPidReadEvent(final List<Pid> list) {
            this.readPids = new PidSet(list);
        }
    }
    
    public static class ObdSupportedPidsChangedEvent
    {
    }
    
    class PidCheck
    {
        private Handler handler;
        volatile boolean hasIncorrectData;
        private Runnable invalidPidRunnable;
        int pid;
        volatile boolean waitingForValidData;
        
        public PidCheck(final int pid, final Handler handler) {
            this.hasIncorrectData = false;
            this.waitingForValidData = false;
            this.pid = pid;
            this.handler = handler;
            this.invalidPidRunnable = new Runnable() {
                @Override
                public void run() {
                    if (!PidCheck.this.hasIncorrectData) {
                        PidCheck.this.waitingForValidData = false;
                        PidCheck.this.hasIncorrectData = true;
                        PidCheck.this.invalidatePid(pid);
                    }
                }
            };
        }
        
        public void checkPid(final double n) {
            if (!this.isPidValueValid(n)) {
                if (!this.hasIncorrectData) {
                    if (!this.waitingForValidData) {
                        this.handler.removeCallbacks(this.invalidPidRunnable);
                        this.handler.postDelayed(this.invalidPidRunnable, this.getWaitForValidDataTimeout());
                        this.waitingForValidData = true;
                    }
                    else {
                        ObdManager.sLogger.d("Already waiting for valid PID data, PID : " + this.pid + " ,  Value : " + n);
                    }
                }
            }
            else {
                this.reset();
            }
        }
        
        public long getWaitForValidDataTimeout() {
            return 0L;
        }
        
        public boolean hasIncorrectData() {
            return this.hasIncorrectData;
        }
        
        public void invalidatePid(final int n) {
        }
        
        public boolean isPidValueValid(final double n) {
            return true;
        }
        
        public boolean isWaitingForValidData() {
            return this.waitingForValidData;
        }
        
        public void reset() {
            this.hasIncorrectData = false;
            this.handler.removeCallbacks(this.invalidPidRunnable);
            this.waitingForValidData = false;
        }
    }
}
