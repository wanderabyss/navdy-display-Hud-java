package com.navdy.hud.app.obd;

import android.text.TextUtils;
import com.google.gson.JsonSyntaxException;
import com.google.gson.Gson;
import com.navdy.service.library.log.Logger;

public class CarDetails
{
    public static final Logger sLogger;
    String aaia;
    String engine;
    String engineType;
    String make;
    String model;
    String vin;
    String year;
    
    static {
        sLogger = new Logger(CarDetails.class);
    }
    
    public static CarDetails fromJson(final String s) {
        try {
            return new Gson().<CarDetails>fromJson(s.toString(), CarDetails.class);
        }
        catch (JsonSyntaxException ex) {
            CarDetails.sLogger.e("Exception while parsing the car details response from CarMD ", ex);
            return null;
        }
    }
    
    public static boolean matches(final CarDetails carDetails, final String s) {
        return ObdDeviceConfigurationManager.isValidVin(s) && carDetails != null && TextUtils.equals((CharSequence)carDetails.vin, (CharSequence)s);
    }
}
