package com.navdy.hud.app.event;

import com.navdy.service.library.events.preferences.DriverProfilePreferences;

public class DisplayScaleChange
{
    public final DriverProfilePreferences.DisplayFormat format;
    
    public DisplayScaleChange(final DriverProfilePreferences.DisplayFormat format) {
        this.format = format;
    }
}
