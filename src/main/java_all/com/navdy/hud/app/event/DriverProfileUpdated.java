package com.navdy.hud.app.event;

public class DriverProfileUpdated
{
    public State state;
    
    public DriverProfileUpdated(final State state) {
        this.state = state;
    }
    
    public enum State
    {
        UPDATED, 
        UP_TO_DATE;
    }
}
