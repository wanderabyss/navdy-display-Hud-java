package com.navdy.hud.app.device.dial;

import android.view.KeyEvent;
import android.app.Instrumentation;
import com.squareup.otto.Subscribe;
import com.navdy.service.library.task.TaskManager;
import android.view.ViewConfiguration;
import android.content.Context;
import java.util.HashMap;
import com.navdy.service.library.events.input.DialSimulationEvent;
import java.util.Map;

public class DialSimulatorMessagesHandler
{
    private static final int LONG_PRESS_KEY_UP_FLAGS_SIMULATION = 640;
    private static final Map<DialSimulationEvent.DialInputAction, Integer> SIMULATION_TO_KEY_EVENT;
    private int longPressTimeThreshold;
    
    static {
        SIMULATION_TO_KEY_EVENT = new HashMap<DialSimulationEvent.DialInputAction, Integer>() {
            {
                this.put(DialSimulationEvent.DialInputAction.DIAL_LEFT_TURN, 21);
                this.put(DialSimulationEvent.DialInputAction.DIAL_RIGHT_TURN, 22);
                this.put(DialSimulationEvent.DialInputAction.DIAL_CLICK, 66);
            }
        };
    }
    
    public DialSimulatorMessagesHandler(final Context context) {
        ViewConfiguration.get(context);
        this.longPressTimeThreshold = ViewConfiguration.getLongPressTimeout();
    }
    
    @Subscribe
    public void onDialSimulationEvent(final DialSimulationEvent dialSimulationEvent) {
        if (dialSimulationEvent != null) {
            TaskManager.getInstance().execute(new SimulatedKeyEventRunnable(dialSimulationEvent.dialAction), 1);
        }
    }
    
    private class SimulatedKeyEventRunnable implements Runnable
    {
        private DialSimulationEvent.DialInputAction eventAction;
        
        public SimulatedKeyEventRunnable(final DialSimulationEvent.DialInputAction eventAction) {
            this.eventAction = eventAction;
        }
        
        @Override
        public void run() {
            final Instrumentation instrumentation = new Instrumentation();
            if (this.eventAction == DialSimulationEvent.DialInputAction.DIAL_LONG_CLICK) {
                final KeyEvent keyEvent = new KeyEvent(0, 66);
                final int flags = keyEvent.getFlags();
                final KeyEvent changeTimeRepeat = KeyEvent.changeTimeRepeat(keyEvent, keyEvent.getEventTime(), 1, flags | 0x80);
                instrumentation.sendKeySync(changeTimeRepeat);
                instrumentation.sendKeySync(KeyEvent.changeAction(KeyEvent.changeTimeRepeat(changeTimeRepeat, changeTimeRepeat.getEventTime() + DialSimulatorMessagesHandler.this.longPressTimeThreshold, 0, flags | 0x280), 1));
            }
            else {
                instrumentation.sendKeyDownUpSync((int)DialSimulatorMessagesHandler.SIMULATION_TO_KEY_EVENT.get(this.eventAction));
            }
        }
    }
}
