package com.navdy.hud.app.device.light;

import android.util.SparseArray;

public class LightManager
{
    public static final int FRONT_LED = 0;
    private static final String LED_DEVICE = "/sys/class/leds/as3668";
    private static final LightManager sInstance;
    private SparseArray<ILight> mLights;
    
    static {
        sInstance = new LightManager();
    }
    
    private LightManager() {
        this.mLights = (SparseArray<ILight>)new SparseArray();
        final LED led = new LED("/sys/class/leds/as3668");
        if (led.isAvailable()) {
            this.mLights.append(0, led);
        }
    }
    
    public static LightManager getInstance() {
        return LightManager.sInstance;
    }
    
    public ILight getLight(final int n) {
        return (ILight)this.mLights.get(n);
    }
}
