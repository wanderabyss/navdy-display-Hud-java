package com.navdy.hud.app.maps.notification;

import com.squareup.otto.Bus;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.service.library.events.audio.SpeechRequest;
import com.navdy.hud.app.maps.here.HereRouteManager;
import com.navdy.service.library.events.navigation.NavigationSessionRouteChange;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.service.library.task.TaskManager;
import com.navdy.hud.app.maps.here.HereRouteCache;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.ui.component.destination.IDestinationPicker;

public class RouteCalcPickerHandler implements IDestinationPicker
{
    private static final Logger sLogger;
    private MapEvents.RouteCalculationEvent event;
    
    static {
        sLogger = new Logger(RouteCalcPickerHandler.class);
    }
    
    public RouteCalcPickerHandler(final MapEvents.RouteCalculationEvent event) {
        this.event = event;
    }
    
    @Override
    public void onDestinationPickerClosed() {
    }
    
    @Override
    public boolean onItemClicked(int size, final int n, final DestinationPickerScreen.DestinationPickerState destinationPickerState) {
        RemoteDeviceManager.getInstance().getUiStateManager().getNavigationView().cleanupMapOverviewRoutes();
        if (n == 0) {
            RouteCalcPickerHandler.sLogger.v("map-route- onItemClicked current route");
        }
        else {
            RouteCalcPickerHandler.sLogger.v("map-route- onItemClicked new route");
            size = this.event.response.results.size();
            if (n >= size) {
                RouteCalcPickerHandler.sLogger.w("map-route- invalid pos:" + n + " len=" + size);
            }
            else {
                final NavigationRouteResult navigationRouteResult = this.event.response.results.get(n);
                final HereRouteCache.RouteInfo route = HereRouteCache.getInstance().getRoute(navigationRouteResult.routeId);
                if (route == null) {
                    RouteCalcPickerHandler.sLogger.w("map-route- route not found in cache:" + navigationRouteResult.routeId);
                }
                else {
                    destinationPickerState.doNotAddOriginalRoute = true;
                    TaskManager.getInstance().execute(new Runnable() {
                        @Override
                        public void run() {
                            final HereNavigationManager instance = HereNavigationManager.getInstance();
                            instance.setReroute(route.route, NavigationSessionRouteChange.RerouteReason.NAV_SESSION_ROUTE_PICKER, navigationRouteResult.routeId, navigationRouteResult.via, true, instance.isTrafficConsidered(navigationRouteResult.routeId));
                            if (instance.getNavigationSessionPreference().spokenTurnByTurn) {
                                final String buildChangeRouteTTS = HereRouteManager.buildChangeRouteTTS(navigationRouteResult.routeId);
                                RouteCalcPickerHandler.sLogger.v("tts[" + buildChangeRouteTTS + "]");
                                final Bus bus = RemoteDeviceManager.getInstance().getBus();
                                bus.post(RouteCalculationNotification.CANCEL_TBT_TTS);
                                bus.post(RouteCalculationNotification.CANCEL_CALC_TTS);
                                TTSUtils.sendSpeechRequest(buildChangeRouteTTS, SpeechRequest.Category.SPEECH_TURN_BY_TURN, RouteCalculationNotification.ROUTE_CALC_TTS_ID);
                            }
                            RouteCalcPickerHandler.sLogger.w("new route set: " + route.routeRequest);
                        }
                    }, 20);
                }
            }
        }
        return true;
    }
    
    @Override
    public boolean onItemSelected(final int n, final int n2, final DestinationPickerScreen.DestinationPickerState destinationPickerState) {
        return false;
    }
}
