package com.navdy.hud.app.maps.here;

import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.service.library.events.audio.SpeechRequest;
import com.navdy.hud.app.maps.notification.RouteCalculationNotification;
import com.navdy.service.library.task.TaskManager;
import com.here.android.mpa.routing.Maneuver;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.here.android.mpa.guidance.NavigationManager;

class HereNewManeuverListener extends NewInstructionEventListener
{
    private final Bus bus;
    private boolean hasNewRoute;
    private final HereNavigationManager hereNavigationManager;
    private final Logger logger;
    private final HereNavController navController;
    private Maneuver navManeuver;
    private final String tag;
    private final boolean verbose;
    
    HereNewManeuverListener(final Logger logger, final String tag, final boolean verbose, final HereNavController navController, final HereNavigationManager hereNavigationManager, final Bus bus) {
        this.logger = logger;
        this.tag = tag;
        this.verbose = verbose;
        this.navController = navController;
        this.hereNavigationManager = hereNavigationManager;
        this.bus = bus;
    }
    
    void clearNavManeuver() {
        this.navManeuver = null;
    }
    
    Maneuver getNavManeuver() {
        return this.navManeuver;
    }
    
    @Override
    public void onNewInstructionEvent() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Maneuver nextManeuver = null;
                    Label_0495: {
                        try {
                            nextManeuver = HereNewManeuverListener.this.navController.getNextManeuver();
                            HereNewManeuverListener.this.navManeuver = nextManeuver;
                            if (nextManeuver == null) {
                                HereNewManeuverListener.this.logger.w(HereNewManeuverListener.this.tag + "There is no next maneuver");
                            }
                            else {
                                Maneuver afterNextManeuver = HereNewManeuverListener.this.navController.getAfterNextManeuver();
                                if (HereNewManeuverListener.this.verbose) {
                                    HereNewManeuverListener.this.logger.v(HereNewManeuverListener.this.tag + " " + HereNewManeuverListener.this.navController.getState().name() + " current road[" + HereMapUtil.getCurrentRoadName() + "] maneuver road name[" + nextManeuver.getRoadName() + "] maneuver next roadname[" + nextManeuver.getNextRoadName() + "] turn[" + nextManeuver.getTurn().name() + "] action[" + nextManeuver.getAction().name() + "] icon[" + nextManeuver.getIcon().name() + "]");
                                }
                                if (nextManeuver.getAction() == Maneuver.Action.END || nextManeuver.getIcon() == Maneuver.Icon.END) {
                                    HereNewManeuverListener.this.logger.v(HereNewManeuverListener.this.tag + " " + HereNewManeuverListener.this.navController.getState() + " LAST MANEUVER RECEIVED");
                                    afterNextManeuver = null;
                                }
                                final boolean access$500 = HereNewManeuverListener.this.hasNewRoute;
                                HereNewManeuverListener.this.hasNewRoute = false;
                                HereNewManeuverListener.this.hereNavigationManager.updateNavigationInfo(nextManeuver, afterNextManeuver, null, access$500);
                                if (HereNewManeuverListener.this.hereNavigationManager.isShownFirstManeuver()) {
                                    break Label_0495;
                                }
                                HereNewManeuverListener.this.hereNavigationManager.setShownFirstManeuver(true);
                                if (HereNewManeuverListener.this.hereNavigationManager.getNavigationSessionPreference().spokenTurnByTurn) {
                                    final String buildStartRouteTTS = HereRouteManager.buildStartRouteTTS();
                                    HereNewManeuverListener.this.logger.v("tts[" + buildStartRouteTTS + "]");
                                    HereNewManeuverListener.this.bus.post(RouteCalculationNotification.CANCEL_CALC_TTS);
                                    TTSUtils.sendSpeechRequest(buildStartRouteTTS, SpeechRequest.Category.SPEECH_TURN_BY_TURN, RouteCalculationNotification.ROUTE_CALC_TTS_ID);
                                }
                                HereNewManeuverListener.this.bus.post(new MapEvents.ManeuverEvent(MapEvents.ManeuverEvent.Type.FIRST, nextManeuver));
                            }
                            return;
                        }
                        catch (Throwable t) {
                            HereNewManeuverListener.this.logger.e(t);
                            return;
                        }
                    }
                    HereNewManeuverListener.this.bus.post(new MapEvents.ManeuverEvent(MapEvents.ManeuverEvent.Type.INTERMEDIATE, nextManeuver));
                }
            }
        }, 20);
    }
    
    void setNewRoute() {
        this.logger.v("setNewRoute");
        this.hasNewRoute = true;
        if (this.logger.isLoggable(2)) {
            this.logger.v("setNewRoute: send empty maneuver");
        }
        this.bus.post(HereManeuverDisplayBuilder.EMPTY_MANEUVER_DISPLAY);
    }
}
