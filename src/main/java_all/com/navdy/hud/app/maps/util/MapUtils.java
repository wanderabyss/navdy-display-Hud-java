package com.navdy.hud.app.maps.util;

import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.service.library.events.destination.Destination;
import android.location.Location;
import com.navdy.service.library.events.location.LatLong;

public final class MapUtils
{
    public static final float INVALID_DISTANCE = -1.0f;
    private static final double MAX_ALTITUDE = 10000.0;
    private static final double MAX_LATITUDE = 90.0;
    private static final double MAX_LONGITUDE = 180.0;
    private static final double MIN_ALTITUDE = -10000.0;
    private static final double MIN_LATITUDE = -90.0;
    private static final double MIN_LONGITUDE = -180.0;
    public static final int SECONDS_PER_HOUR = 3600;
    public static final int SECONDS_PER_MINUTE = 60;
    
    public static float distanceBetween(final LatLong latLong, final LatLong latLong2) {
        float n;
        if (latLong == null || latLong2 == null) {
            n = -1.0f;
        }
        else {
            final float[] array = { 0.0f };
            Location.distanceBetween((double)latLong.latitude, (double)latLong.longitude, (double)latLong2.latitude, (double)latLong2.longitude, array);
            n = array[0];
        }
        return n;
    }
    
    public static String formatTime(final int n) {
        String s;
        if (n >= 3600) {
            s = String.format("%d:%02d", n / 3600, n / 60);
        }
        else {
            s = String.format("%d", n / 60);
        }
        return s;
    }
    
    public static int getIconForDestination(final Destination.FavoriteType favoriteType) {
        return R.drawable.icon_pin_route_circle;
    }
    
    public static int getMinuteCeiling(int n) {
        if (n % 60 != 0) {
            n = n + 60 - n % 60;
        }
        return n;
    }
    
    public static GeoCoordinate sanitizeCoords(final GeoCoordinate geoCoordinate) {
        final double latitude = geoCoordinate.getLatitude();
        final double longitude = geoCoordinate.getLongitude();
        final double altitude = geoCoordinate.getAltitude();
        GeoCoordinate geoCoordinate2;
        if (!sanitizeCoords(latitude, longitude)) {
            geoCoordinate2 = null;
        }
        else {
            if (altitude >= -10000.0) {
                geoCoordinate2 = geoCoordinate;
                if (altitude <= 10000.0) {
                    return geoCoordinate2;
                }
            }
            geoCoordinate.setAltitude(0.0);
            geoCoordinate2 = geoCoordinate;
        }
        return geoCoordinate2;
    }
    
    public static boolean sanitizeCoords(final double n, final double n2) {
        return n <= 90.0 && n >= -90.0 && n2 <= 180.0 && n2 >= -180.0;
    }
}
