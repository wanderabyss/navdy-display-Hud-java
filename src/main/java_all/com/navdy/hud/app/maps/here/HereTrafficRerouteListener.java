package com.navdy.hud.app.maps.here;

import com.navdy.hud.app.maps.MapEvents;
import com.here.android.mpa.guidance.NavigationManager.TrafficRerouteListener;
import com.here.android.mpa.guidance.TrafficNotification;
import android.os.Bundle;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import java.util.Iterator;
import java.util.Date;
import com.here.android.mpa.routing.RouteTta;
import java.util.ArrayList;
import java.util.HashSet;
import com.navdy.hud.app.util.CrashReporter;
import android.os.SystemClock;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.service.library.events.navigation.NavigationSessionRouteChange;
import com.navdy.service.library.task.TaskManager;
import com.navdy.hud.app.maps.notification.TrafficNotificationManager;
import com.here.android.mpa.routing.Maneuver;
import java.util.List;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.service.library.log.Logger;
import com.here.android.mpa.routing.Route;
import com.squareup.otto.Bus;
import com.here.android.mpa.guidance.NavigationManager;

class HereTrafficRerouteListener extends TrafficRerouteListener
{
    static final int MIN_INTERVAL_REROUTE_FIRST = 90;
    static final long NOTIF_FROM_HERE_THRESHOLD;
    static final long REROUTE_POINT_THRESHOLD;
    static final int ROUTE_CHECK_INITIAL_INTERVAL;
    static final int ROUTE_CHECK_INTERVAL;
    static final int ROUTE_CHECK_INTERVAL_LIMIT_BANDWIDTH;
    static final int WAIT_INTERVAL_AFTER_FIRST_MANEUVER_SHOWN;
    private final Bus bus;
    private Route currentProposedRoute;
    private long fasterBy;
    private final HereNavigationManager hereNavigationManager;
    private long lastTrafficNotifFromHere;
    private final Logger logger;
    private final HereNavController navController;
    private NavdyTrafficRerouteManager navdyTrafficRerouteManager;
    private boolean notifFromHere;
    private GeoCoordinate placeCalculated;
    private Runnable startRunnable;
    private Runnable stopRunnable;
    private final String tag;
    private long timeCalculated;
    private boolean trafficRerouteListenerRunning;
    private final boolean verbose;
    private String viaString;
    
    static {
        WAIT_INTERVAL_AFTER_FIRST_MANEUVER_SHOWN = (int)TimeUnit.SECONDS.toMillis(60L);
        ROUTE_CHECK_INITIAL_INTERVAL = (int)TimeUnit.MINUTES.toMillis(1L);
        ROUTE_CHECK_INTERVAL = (int)TimeUnit.MINUTES.toMillis(5L);
        ROUTE_CHECK_INTERVAL_LIMIT_BANDWIDTH = (int)TimeUnit.MINUTES.toMillis(30L);
        REROUTE_POINT_THRESHOLD = TimeUnit.MINUTES.toSeconds(20L);
        NOTIF_FROM_HERE_THRESHOLD = TimeUnit.MINUTES.toMillis(1L);
    }
    
    HereTrafficRerouteListener(final String tag, final boolean verbose, final HereNavController navController, final HereNavigationManager hereNavigationManager, final Bus bus) {
        this.logger = new Logger("HereTrafficReroute");
        this.startRunnable = new Runnable() {
            @Override
            public void run() {
                HereTrafficRerouteListener.this.navController.addTrafficRerouteListener(new WeakReference<TrafficRerouteListener>(HereTrafficRerouteListener.this));
                HereTrafficRerouteListener.this.logger.v("addTrafficRerouteListener");
            }
        };
        this.stopRunnable = new Runnable() {
            @Override
            public void run() {
                HereTrafficRerouteListener.this.navController.removeTrafficRerouteListener(HereTrafficRerouteListener.this);
                HereTrafficRerouteListener.this.logger.v("removeTrafficRerouteListener");
            }
        };
        this.logger.v("HereTrafficRerouteListener:ctor");
        this.tag = tag;
        this.verbose = verbose;
        this.navController = navController;
        this.hereNavigationManager = hereNavigationManager;
        this.bus = bus;
    }
    
    private void clearProposedRoute() {
        this.logger.v("clearProposedRoute");
        if (this.currentProposedRoute != null) {
            this.logger.v("clearProposedRoute:cleared");
            TrafficNotificationManager.getInstance().removeFasterRouteNotifiation();
            this.currentProposedRoute = null;
            this.timeCalculated = 0L;
            this.placeCalculated = null;
            this.notifFromHere = false;
            this.viaString = null;
            this.fasterBy = 0L;
        }
    }
    
    private boolean isFasterRouteDivergePointCloseBy(final List<Maneuver> list, final List<Maneuver> list2, final List<Maneuver> list3) {
        final Maneuver nextManeuver = this.navController.getNextManeuver();
        final Maneuver maneuver = list2.get(0);
        Maneuver maneuver2;
        if (list3.size() > 1) {
            maneuver2 = list3.get(1);
        }
        else {
            maneuver2 = list3.get(0);
        }
        Maneuver maneuver3;
        if (list2.size() > 1) {
            maneuver3 = list2.get(1);
        }
        else {
            maneuver3 = maneuver;
        }
        this.logger.v(this.tag + " diverge point road faster [" + HereMapUtil.getRoadName(maneuver2) + "," + HereMapUtil.getNextRoadName(maneuver2) + "]  slower [" + HereMapUtil.getRoadName(maneuver3) + "," + HereMapUtil.getNextRoadName(maneuver3) + "]");
        if (HereMapUtil.areManeuverEqual(nextManeuver, maneuver)) {
            this.logger.i(this.tag + "first change and current are same");
            return true;
        }
        final int n = -1;
        int n2 = 0;
        Label_0191: {
            if (nextManeuver == null) {
                this.logger.i(this.tag + "no current maneuver");
                n2 = n;
            }
            else {
                final int size = list.size();
                int n3 = 0;
                while (true) {
                    n2 = n;
                    if (n3 >= size) {
                        break Label_0191;
                    }
                    if (HereMapUtil.areManeuverEqual(list.get(n3), nextManeuver)) {
                        break;
                    }
                    ++n3;
                }
                n2 = n3;
            }
        }
        if (n2 == -1) {
            this.logger.i(this.tag + "offset not found");
            return true;
        }
        this.logger.v(this.tag + "reroute offset=" + n2);
        long n4 = 0L;
        long n5 = 0L;
        final HereMapUtil.LongContainer longContainer = new HereMapUtil.LongContainer();
        final int size2 = list.size();
        final boolean b = false;
        boolean b2;
        while (true) {
            b2 = b;
            if (n2 >= size2) {
                break;
            }
            final Maneuver maneuver4 = list.get(n2);
            if (HereMapUtil.areManeuverEqual(maneuver4, maneuver)) {
                this.logger.v(this.tag + "reroute diverge:" + n2 + "," + size2);
                b2 = true;
                break;
            }
            n4 += HereMapUtil.getManeuverDriveTime(maneuver4, longContainer);
            n5 += longContainer.val;
            ++n2;
        }
        boolean b3;
        if (!b2) {
            this.logger.i(this.tag + "reroute threshold slow maneuver not found");
            b3 = true;
        }
        else {
            if (n4 < HereTrafficRerouteListener.REROUTE_POINT_THRESHOLD) {
                this.logger.v(this.tag + "reroute point threshold [" + n4 + "] distance[" + n5 + "]");
                return true;
            }
            this.logger.i(this.tag + "reroute threshold [" + n4 + "] is > " + HereTrafficRerouteListener.REROUTE_POINT_THRESHOLD + " dist=" + n5);
            this.dismissReroute();
            b3 = false;
        }
        return b3;
        b3 = true;
        return b3;
    }
    
    public void confirmReroute() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                if (HereTrafficRerouteListener.this.currentProposedRoute != null) {
                    HereTrafficRerouteListener.this.hereNavigationManager.clearNavManeuver();
                    final Error setRoute = HereTrafficRerouteListener.this.navController.setRoute(HereTrafficRerouteListener.this.currentProposedRoute);
                    if (setRoute == Error.NONE) {
                        HereTrafficRerouteListener.this.hereNavigationManager.updateMapRoute(HereTrafficRerouteListener.this.currentProposedRoute, NavigationSessionRouteChange.RerouteReason.NAV_SESSION_TRAFFIC_REROUTE, null, null, false, true);
                        HereTrafficRerouteListener.this.logger.e("confirmReroute: set");
                    }
                    else {
                        HereTrafficRerouteListener.this.logger.e("confirmReroute: traffic route could not be set:" + setRoute);
                    }
                    HereTrafficRerouteListener.this.clearProposedRoute();
                }
                else {
                    HereTrafficRerouteListener.this.logger.e("confirmReroute: no route available");
                }
            }
        }, 20);
    }
    
    public void dismissReroute() {
        this.clearProposedRoute();
    }
    
    void fasterRouteNotFound(final boolean b, final long n, final String s, final String s2) {
        String s3;
        if (b) {
            s3 = "[Here Traffic]";
        }
        else {
            s3 = "[Navdy Calc]";
        }
        this.logger.v(this.tag + s3 + " new route threshold not met:" + this.getRerouteMinDuration());
        if (n > 0L && TTSUtils.isDebugTTSEnabled()) {
            TTSUtils.debugShowFasterRouteToast(s3 + " Faster route below threshold", "via[" + s + "] faster by[" + n + "] seconds current via[" + s2 + "]");
        }
    }
    
    public Route getCurrentProposedRoute() {
        return this.currentProposedRoute;
    }
    
    public long getCurrentProposedRouteTime() {
        return this.timeCalculated;
    }
    
    public long getFasterBy() {
        return this.fasterBy;
    }
    
    public int getRerouteMinDuration() {
        int n;
        if (this.hereNavigationManager.getCurrentTrafficRerouteCount() < 1) {
            this.logger.v("getRerouteMinDuration:90");
            n = 90;
        }
        else {
            final int n2 = (int)(SystemClock.elapsedRealtime() - this.hereNavigationManager.getLastTrafficRerouteTime()) / 1000;
            n = (int)(90.0 + 1.5 * Math.max(360 - n2, 0));
            this.logger.v("getRerouteMinDuration:" + n + " secslast:" + n2);
        }
        return n;
    }
    
    public String getViaString() {
        return this.viaString;
    }
    
    public void handleFasterRoute(final Route route, final boolean b) {
        if (route == null) {
            this.logger.e("null faster route:" + b);
        }
        else {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        String s = null;
                        Label_0169: {
                            try {
                                if (b) {
                                    s = "[Here Traffic]";
                                }
                                else {
                                    s = "[Navdy Calc]";
                                }
                                HereTrafficRerouteListener.this.logger.v(HereTrafficRerouteListener.this.tag + s + " onTrafficRerouted");
                                HereTrafficRerouteListener.this.dismissReroute();
                                if (!HereTrafficRerouteListener.this.hereNavigationManager.isNavigationModeOn()) {
                                    HereTrafficRerouteListener.this.logger.v(HereTrafficRerouteListener.this.tag + " onTrafficRerouted, not currently navigating");
                                }
                                else {
                                    if (!HereTrafficRerouteListener.this.hereNavigationManager.hasArrived()) {
                                        break Label_0169;
                                    }
                                    HereTrafficRerouteListener.this.logger.v("onTrafficRerouted, arrival mode on");
                                }
                                return;
                            }
                            catch (Throwable t) {
                                HereTrafficRerouteListener.this.logger.e(t);
                                CrashReporter.getInstance().reportNonFatalException(t);
                                return;
                            }
                        }
                        if (HereTrafficRerouteListener.this.hereNavigationManager.isOnGasRoute()) {
                            HereTrafficRerouteListener.this.logger.v("onTrafficRerouted, on fas route");
                            return;
                        }
                        final Route currentRoute = HereTrafficRerouteListener.this.hereNavigationManager.getCurrentRoute();
                        if (currentRoute == null) {
                            HereTrafficRerouteListener.this.logger.v(HereTrafficRerouteListener.this.tag + " onTrafficRerouted, not current route");
                            return;
                        }
                        final List<Maneuver> maneuvers = currentRoute.getManeuvers();
                        if (b) {
                            final String allRoadNames = HereMapUtil.getAllRoadNames(route.getManeuvers(), 0);
                            final String allRoadNames2 = HereMapUtil.getAllRoadNames(maneuvers, 0);
                            HereTrafficRerouteListener.this.logger.v("[HereNotif-RoadNames-current]");
                            HereTrafficRerouteListener.this.logger.v(allRoadNames);
                            HereTrafficRerouteListener.this.logger.v("[HereNotif-RoadNames-faster]");
                            HereTrafficRerouteListener.this.logger.v(allRoadNames2);
                        }
                        final RouteTta tta = route.getTta(Route.TrafficPenaltyMode.OPTIMAL, 268435455);
                        final RouteTta tta2 = route.getTta(Route.TrafficPenaltyMode.DISABLED, 268435455);
                        if (tta == null || tta2 == null) {
                            HereTrafficRerouteListener.this.logger.v(HereTrafficRerouteListener.this.tag + "error retrieving navigation and TTA");
                            return;
                        }
                        if (!b) {
                            final long n = tta.getDuration();
                            if (n == tta2.getDuration()) {
                                HereTrafficRerouteListener.this.logger.v(HereTrafficRerouteListener.this.tag + "faster route durations is same=" + n);
                                return;
                            }
                        }
                        long n2 = 0L;
                        final RouteTta tta3 = currentRoute.getTta(Route.TrafficPenaltyMode.OPTIMAL, 268435455);
                        if (tta3 != null) {
                            n2 = tta3.getDuration();
                        }
                        final Date eta = HereTrafficRerouteListener.this.navController.getEta(true, Route.TrafficPenaltyMode.OPTIMAL);
                        if (!HereMapUtil.isValidEtaDate(eta)) {
                            HereTrafficRerouteListener.this.logger.v(HereTrafficRerouteListener.this.tag + "error retrieving eta for current");
                            return;
                        }
                        final long time = eta.getTime();
                        final long currentTimeMillis = System.currentTimeMillis();
                        final long n3 = (time - currentTimeMillis) / 1000L;
                        final long n4 = tta.getDuration();
                        final long n5 = n3 - n4;
                        HereTrafficRerouteListener.this.logger.i("etaDate[" + eta + "] etaUtc[" + time + "] currentTta[" + n3 + "] now[" + currentTimeMillis + "] newTta[" + n4 + "]" + "] diff[" + n5 + "]" + "] oldTtaDuration[" + n2 + "]");
                        final HashSet<String> set = new HashSet<String>();
                        final ArrayList<Maneuver> list = new ArrayList<Maneuver>(1);
                        final List<Maneuver> differentManeuver = HereMapUtil.getDifferentManeuver(route, currentRoute, 2, list);
                        if (differentManeuver.size() == 0) {
                            HereTrafficRerouteListener.this.logger.i("could not find a difference in maneuvers");
                            return;
                        }
                        String s2 = null;
                        final StringBuilder sb = new StringBuilder();
                        final Iterator<Maneuver> iterator = differentManeuver.iterator();
                        while (iterator.hasNext()) {
                            final String nextRoadName = HereMapUtil.getNextRoadName(iterator.next());
                            if (s2 == null) {
                                s2 = nextRoadName;
                                sb.append(s2);
                                set.add(s2);
                            }
                            else {
                                if (set.contains(nextRoadName)) {
                                    continue;
                                }
                                sb.append(",");
                                sb.append(" ");
                                sb.append(nextRoadName);
                                set.add(nextRoadName);
                            }
                        }
                        final String viaString = HereRouteViaGenerator.getViaString(route);
                        if (!set.contains(viaString)) {
                            sb.append(",");
                            sb.append(" ");
                            sb.append(viaString);
                        }
                        String via = null;
                        final HereRouteCache.RouteInfo route = HereRouteCache.getInstance().getRoute(HereTrafficRerouteListener.this.hereNavigationManager.getCurrentRouteId());
                        if (route != null) {
                            via = route.routeResult.via;
                        }
                        final String string = sb.toString();
                        final long n6 = route.getLength() - HereTrafficRerouteListener.this.navController.getDestinationDistance();
                        final int rerouteMinDuration = HereTrafficRerouteListener.this.getRerouteMinDuration();
                        HereTrafficRerouteListener.this.logger.v(HereTrafficRerouteListener.this.tag + s + " new route via[" + s2 + "] additionalVia[" + string + "] is faster by[" + n5 + "] seconds than current route via[" + via + "] distanceDiff [" + n6 + "] minDur[" + rerouteMinDuration + "]");
                        if (n5 >= rerouteMinDuration) {
                            HereTrafficRerouteListener.this.logger.v("faster route is above threshold:" + n5);
                            if (!HereTrafficRerouteListener.this.isFasterRouteDivergePointCloseBy(maneuvers, list, differentManeuver)) {
                                HereTrafficRerouteListener.this.logger.v("faster route not near by, threshold not met");
                                return;
                            }
                            HereTrafficRerouteListener.this.setFasterRoute(route, b, s2, n5, string, time, n6, 0L);
                        }
                        else {
                            HereTrafficRerouteListener.this.logger.v("faster route is below threshold:" + n5);
                            if (!HereMapsManager.getInstance().isRecalcCurrentRouteForTrafficEnabled()) {
                                HereTrafficRerouteListener.this.fasterRouteNotFound(b, n5, s2, via);
                                return;
                            }
                            if (!HereTrafficRerouteListener.this.isFasterRouteDivergePointCloseBy(maneuvers, list, differentManeuver)) {
                                HereTrafficRerouteListener.this.logger.v("faster route not near by, threshold not met");
                                return;
                            }
                            HereTrafficRerouteListener.this.navdyTrafficRerouteManager.reCalculateCurrentRoute(currentRoute, route, b, s2, n5, string, time, n6, via, tta);
                        }
                    }
                }
            }, 2);
        }
    }
    
    public boolean isMoreRouteNotificationComplete() {
        boolean b = false;
        if (!this.hereNavigationManager.isShownFirstManeuver()) {
            this.logger.v("isMoreRouteNotificationComplete:not shown first maneuver");
        }
        else if (NotificationManager.getInstance().isCurrentNotificationId("navdy#route#calc#notif")) {
            this.logger.v("isMoreRouteNotificationComplete:route calc notif on");
        }
        else {
            final BaseScreen currentScreen = RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
            if (currentScreen != null && currentScreen.getScreen() == Screen.SCREEN_DESTINATION_PICKER) {
                final Bundle arguments = currentScreen.getArguments();
                if (arguments != null && arguments.getBoolean("ROUTE_PICKER")) {
                    this.logger.v("isMoreRouteNotificationComplete:user looking at route picker");
                    return b;
                }
            }
            if (HereMapsManager.getInstance().getRouteCalcEventHandler().hasMoreRoutes()) {
                this.logger.v("isMoreRouteNotificationComplete:user has not seen more routes");
            }
            else {
                final long n = SystemClock.elapsedRealtime() - this.hereNavigationManager.getFirstManeuverShowntime();
                if (n < HereTrafficRerouteListener.WAIT_INTERVAL_AFTER_FIRST_MANEUVER_SHOWN) {
                    this.logger.v("isMoreRouteNotificationComplete:first maneuver just shown:" + n);
                }
                else {
                    b = true;
                }
            }
        }
        return b;
    }
    
    @Override
    public void onTrafficRerouteBegin(final TrafficNotification trafficNotification) {
        this.logger.v(this.tag + " reroute-begin:");
        if (this.verbose) {
            HereMapUtil.printTrafficNotification(trafficNotification, this.tag);
        }
    }
    
    @Override
    public void onTrafficRerouteFailed(final TrafficNotification trafficNotification) {
        this.logger.e(this.tag + " reroute-failed");
        HereMapUtil.printTrafficNotification(trafficNotification, this.tag);
    }
    
    @Override
    public void onTrafficRerouteState(final TrafficEnabledRoutingState trafficEnabledRoutingState) {
    }
    
    @Override
    public void onTrafficRerouted(final Route route) {
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        final long n = elapsedRealtime - this.lastTrafficNotifFromHere;
        if (n < HereTrafficRerouteListener.NOTIF_FROM_HERE_THRESHOLD) {
            this.logger.i(this.tag + "here traffic notif came in " + n);
        }
        else if (!this.isMoreRouteNotificationComplete()) {
            this.logger.v("onTrafficRerouted: more route notif in progress");
        }
        else {
            this.lastTrafficNotifFromHere = elapsedRealtime;
            this.handleFasterRoute(route, true);
        }
    }
    
    void setFasterRoute(final Route currentProposedRoute, final boolean notifFromHere, final String viaString, final long fasterBy, final String s, final long n, final long n2, final long n3) {
        this.hereNavigationManager.incrCurrentTrafficRerouteCount();
        this.hereNavigationManager.setLastTrafficRerouteTime(SystemClock.elapsedRealtime());
        this.currentProposedRoute = currentProposedRoute;
        this.timeCalculated = SystemClock.elapsedRealtime();
        this.notifFromHere = notifFromHere;
        this.viaString = viaString;
        this.fasterBy = fasterBy;
        this.placeCalculated = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
        this.bus.post(new MapEvents.TrafficRerouteEvent(s, s, fasterBy, n, n2, n3));
        this.logger.v("faster reroute event sent");
    }
    
    public void setNavdyTrafficRerouteManager(final NavdyTrafficRerouteManager navdyTrafficRerouteManager) {
        this.navdyTrafficRerouteManager = navdyTrafficRerouteManager;
    }
    
    public boolean shouldCalculateFasterRoute() {
        boolean b = false;
        final long n = SystemClock.elapsedRealtime() - this.timeCalculated;
        this.logger.v("elapsed=" + n);
        if (n >= this.hereNavigationManager.getRerouteInterval() - 15000) {
            if (!this.isMoreRouteNotificationComplete()) {
                this.logger.v("shouldCalculateFasterRoute: more route notif in progress");
            }
            else {
                b = true;
            }
        }
        return b;
    }
    
    public void start() {
        if (!this.trafficRerouteListenerRunning) {
            this.trafficRerouteListenerRunning = true;
            TaskManager.getInstance().execute(this.startRunnable, 15);
            this.logger.v("added traffic reroute listener");
        }
    }
    
    public void stop() {
        if (this.trafficRerouteListenerRunning) {
            this.trafficRerouteListenerRunning = false;
            TaskManager.getInstance().execute(this.stopRunnable, 15);
            this.logger.v("removed traffic reroute listener");
        }
    }
}
