package com.navdy.hud.app.manager;

import com.squareup.wire.ProtoEnum;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.LegacyCapability;
import android.content.Context;
import mortar.Mortar;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.framework.voice.VoiceSearchHandler;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.analytics.TelemetryDataManager;
import android.content.SharedPreferences;
import com.navdy.service.library.network.http.IHttpManager;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.util.FeatureUtil;
import com.navdy.hud.app.debug.DriveRecorder;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.framework.phonecall.CallManager;
import com.navdy.hud.app.framework.calendar.CalendarManager;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public final class RemoteDeviceManager
{
    private static final Logger sLogger;
    private static final RemoteDeviceManager singleton;
    @Inject
    Bus bus;
    @Inject
    CalendarManager calendarManager;
    @Inject
    CallManager callManager;
    @Inject
    ConnectionHandler connectionHandler;
    @Inject
    DriveRecorder driveRecorder;
    @Inject
    FeatureUtil featureUtil;
    @Inject
    GestureServiceConnector gestureServiceConnector;
    @Inject
    IHttpManager httpManager;
    @Inject
    InputManager inputManager;
    @Inject
    MusicManager musicManager;
    @Inject
    SharedPreferences preferences;
    @Inject
    TelemetryDataManager telemetryDataManager;
    @Inject
    TimeHelper timeHelper;
    @Inject
    TripManager tripManager;
    @Inject
    UIStateManager uiStateManager;
    @Inject
    VoiceSearchHandler voiceSearchHandler;
    
    static {
        sLogger = new Logger(RemoteDeviceManager.class);
        singleton = new RemoteDeviceManager();
    }
    
    private RemoteDeviceManager() {
        final Context appContext = HudApplication.getAppContext();
        if (appContext != null) {
            Mortar.inject(appContext, this);
        }
    }
    
    public static RemoteDeviceManager getInstance() {
        return RemoteDeviceManager.singleton;
    }
    
    public boolean doesRemoteDeviceHasCapability(final LegacyCapability legacyCapability) {
        final boolean b = false;
        final RemoteDevice remoteDevice = this.connectionHandler.getRemoteDevice();
        boolean b2 = b;
        if (remoteDevice != null) {
            final DeviceInfo deviceInfo = remoteDevice.getDeviceInfo();
            b2 = b;
            if (deviceInfo != null) {
                b2 = b;
                if (deviceInfo.legacyCapabilities != null) {
                    b2 = b;
                    if (deviceInfo.legacyCapabilities.contains(legacyCapability)) {
                        b2 = true;
                    }
                }
            }
        }
        return b2;
    }
    
    public Bus getBus() {
        return this.bus;
    }
    
    public CalendarManager getCalendarManager() {
        return this.calendarManager;
    }
    
    public CallManager getCallManager() {
        return this.callManager;
    }
    
    public ConnectionHandler getConnectionHandler() {
        return this.connectionHandler;
    }
    
    public NavdyDeviceId getDeviceId() {
        final RemoteDevice remoteDevice = this.connectionHandler.getRemoteDevice();
        NavdyDeviceId deviceId;
        if (remoteDevice != null) {
            deviceId = remoteDevice.getDeviceId();
        }
        else {
            deviceId = null;
        }
        return deviceId;
    }
    
    public DriveRecorder getDriveRecorder() {
        return this.driveRecorder;
    }
    
    public FeatureUtil getFeatureUtil() {
        return this.featureUtil;
    }
    
    public GestureServiceConnector getGestureServiceConnector() {
        return this.gestureServiceConnector;
    }
    
    public IHttpManager getHttpManager() {
        return this.httpManager;
    }
    
    public InputManager getInputManager() {
        return this.inputManager;
    }
    
    public MusicManager getMusicManager() {
        return this.musicManager;
    }
    
    public DeviceInfo getRemoteDeviceInfo() {
        final RemoteDevice remoteDevice = this.connectionHandler.getRemoteDevice();
        DeviceInfo deviceInfo;
        if (remoteDevice != null) {
            deviceInfo = remoteDevice.getDeviceInfo();
        }
        else {
            deviceInfo = null;
        }
        return deviceInfo;
    }
    
    public DeviceInfo.Platform getRemoteDevicePlatform() {
        final RemoteDevice remoteDevice = this.connectionHandler.getRemoteDevice();
        ProtoEnum protoEnum;
        if (remoteDevice != null) {
            final DeviceInfo deviceInfo = remoteDevice.getDeviceInfo();
            if (deviceInfo != null) {
                protoEnum = deviceInfo.platform;
            }
            else {
                protoEnum = DeviceInfo.Platform.PLATFORM_iOS;
            }
        }
        else {
            protoEnum = null;
        }
        return (DeviceInfo.Platform)protoEnum;
    }
    
    public SharedPreferences getSharedPreferences() {
        return this.preferences;
    }
    
    public TelemetryDataManager getTelemetryDataManager() {
        return this.telemetryDataManager;
    }
    
    public TimeHelper getTimeHelper() {
        return this.timeHelper;
    }
    
    public TripManager getTripManager() {
        return this.tripManager;
    }
    
    public UIStateManager getUiStateManager() {
        return this.uiStateManager;
    }
    
    public VoiceSearchHandler getVoiceSearchHandler() {
        return this.voiceSearchHandler;
    }
    
    public boolean isAppConnected() {
        boolean b = false;
        if (this.isRemoteDeviceConnected()) {
            b = b;
            if (!this.connectionHandler.isAppClosed()) {
                b = true;
            }
        }
        return b;
    }
    
    public boolean isNetworkLinkReady() {
        return this.connectionHandler.isNetworkLinkReady();
    }
    
    public boolean isRemoteDeviceConnected() {
        return this.getRemoteDeviceInfo() != null;
    }
    
    public boolean isRemoteDeviceIos() {
        return DeviceInfo.Platform.PLATFORM_iOS.equals(this.getRemoteDevicePlatform());
    }
}
