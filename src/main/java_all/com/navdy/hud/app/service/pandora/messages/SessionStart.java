package com.navdy.hud.app.service.pandora.messages;

import java.io.ByteArrayOutputStream;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import java.io.IOException;

public class SessionStart extends BaseOutgoingConstantMessage
{
    private static final int ACCESSORY_ID_FIELD_LENGTH = 8;
    public static final SessionStart INSTANCE;
    
    static {
        INSTANCE = new SessionStart();
    }
    
    public ByteArrayOutputStream putThis(final ByteArrayOutputStream byteArrayOutputStream) throws IOException, StringOverflowException, UnexpectedEndOfStringException {
        BaseOutgoingMessage.putByte(byteArrayOutputStream, (byte)0);
        BaseOutgoingMessage.putShort(byteArrayOutputStream, (short)3);
        BaseOutgoingMessage.putFixedLengthASCIIString(byteArrayOutputStream, 8, "CFDA92FC");
        BaseOutgoingMessage.putShort(byteArrayOutputStream, (short)100);
        BaseOutgoingMessage.putByte(byteArrayOutputStream, (byte)1);
        BaseOutgoingMessage.putByte(byteArrayOutputStream, (byte)2);
        BaseOutgoingMessage.putShort(byteArrayOutputStream, (short)0);
        return byteArrayOutputStream;
    }
    
    @Override
    public String toString() {
        return "Session Start";
    }
}
