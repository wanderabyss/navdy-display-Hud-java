package com.navdy.hud.app.service.pandora;

import java.util.Arrays;
import java.nio.ByteBuffer;

public class AckFrameMessage extends FrameMessage
{
    private static final ByteBuffer ACK_FRAME_BASE;
    public static final AckFrameMessage ACK_WITH_SEQUENCE_0;
    private static final byte[] ACK_WITH_SEQUENCE_0_FRAME;
    public static final AckFrameMessage ACK_WITH_SEQUENCE_1;
    private static final byte[] ACK_WITH_SEQUENCE_1_FRAME;
    
    static {
        ACK_WITH_SEQUENCE_0 = new AckFrameMessage(true);
        ACK_WITH_SEQUENCE_1 = new AckFrameMessage(false);
        ACK_FRAME_BASE = ByteBuffer.allocate(6).put((byte)1).put((byte)0).putInt(0);
        ACK_WITH_SEQUENCE_0_FRAME = FrameMessage.buildFrameFromCRCPart(AckFrameMessage.ACK_FRAME_BASE);
        ACK_WITH_SEQUENCE_1_FRAME = FrameMessage.buildFrameFromCRCPart(AckFrameMessage.ACK_FRAME_BASE.put(1, (byte)1));
    }
    
    private AckFrameMessage(final boolean b) {
        super(b, AckFrameMessage.ACK_PAYLOAD);
    }
    
    protected static AckFrameMessage parseAckFrame(final byte[] array) throws IllegalArgumentException {
        AckFrameMessage ackFrameMessage;
        if (Arrays.equals(array, AckFrameMessage.ACK_WITH_SEQUENCE_0_FRAME)) {
            ackFrameMessage = AckFrameMessage.ACK_WITH_SEQUENCE_0;
        }
        else {
            if (!Arrays.equals(array, AckFrameMessage.ACK_WITH_SEQUENCE_1_FRAME)) {
                throw new IllegalArgumentException("Not a valid ACK message frame");
            }
            ackFrameMessage = AckFrameMessage.ACK_WITH_SEQUENCE_1;
        }
        return ackFrameMessage;
    }
    
    @Override
    public byte[] buildFrame() throws IllegalArgumentException {
        byte[] array;
        if (this.isSequence0) {
            array = AckFrameMessage.ACK_WITH_SEQUENCE_0_FRAME;
        }
        else {
            array = AckFrameMessage.ACK_WITH_SEQUENCE_1_FRAME;
        }
        return array;
    }
}
