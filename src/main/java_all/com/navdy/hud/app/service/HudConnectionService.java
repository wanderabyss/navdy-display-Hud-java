package com.navdy.hud.app.service;

import com.navdy.service.library.events.Ext_NavdyEvent;
import com.navdy.service.library.events.connection.ConnectionRequest;
import com.squareup.wire.Wire;
import com.navdy.service.library.events.audio.AudioStatus;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.file.FileTransferData;
import com.navdy.service.library.events.file.FileTransferStatus;
import com.navdy.service.library.events.file.FileTransferRequest;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.util.NetworkActivityTracker;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.util.CrashReportService;
import com.navdy.service.library.device.connection.Connection;
import com.navdy.service.library.device.discovery.TCPRemoteDeviceBroadcaster;
import com.navdy.service.library.device.discovery.BTDeviceBroadcaster;
import com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster;
import com.squareup.wire.ProtoEnum;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.device.connection.AcceptorListener;
import com.navdy.service.library.device.connection.ConnectionType;
import com.navdy.service.library.network.BTSocketAcceptor;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.service.library.device.connection.ConnectionListener;
import android.content.IntentFilter;
import com.navdy.service.library.events.connection.ConnectionStatus;
import com.navdy.service.library.network.SocketAcceptor;
import com.navdy.service.library.network.BTSocketFactory;
import com.navdy.hud.device.connection.EASessionSocketAdapter;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.network.SocketFactory;
import com.navdy.service.library.network.TCPSocketAcceptor;
import com.navdy.service.library.device.connection.tunnel.Tunnel;
import java.io.IOException;
import com.navdy.service.library.device.connection.ProxyService;
import com.navdy.service.library.device.NavdyDeviceId;
import android.text.TextUtils;
import com.navdy.service.library.events.debug.StartDriveRecordingEvent;
import com.navdy.service.library.events.NavdyEventUtil;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.debug.StartDrivePlaybackEvent;
import com.squareup.wire.Message;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.device.RemoteDevice;
import android.os.SystemClock;
import com.navdy.hud.app.util.BluetoothUtil;
import android.content.Intent;
import android.content.Context;
import android.bluetooth.BluetoothDevice;
import com.navdy.hud.mfi.IAPListener;
import com.navdy.hud.device.connection.iAP2Link;
import com.navdy.service.library.device.link.Link;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.hud.app.debug.RouteRecorder;
import com.navdy.service.library.device.link.LinkManager;
import com.navdy.hud.app.device.gps.GpsManager;
import android.content.BroadcastReceiver;
import com.navdy.service.library.device.connection.ConnectionService;

public class HudConnectionService extends ConnectionService
{
    public static final String ACTION_DEVICE_FORCE_RECONNECT = "com.navdy.hud.app.force_reconnect";
    private static final String APP_TERMINATION_RECONNECT_DELAY_MS = "persist.sys.app_reconnect_ms";
    private static final String AUTO_DRIVERECORD_PROP = "persist.sys.driverecord.auto";
    public static final String EXTRA_HUD_FORCE_RECONNECT_REASON = "force_reconnect_reason";
    private static final int PROXY_ENTRY_LOCAL_PORT = 3000;
    public static final String REASON_CONNECTION_DISCONNECTED = "CONNECTION_DISCONNECTED";
    private BroadcastReceiver bluetoothReceiver;
    private BroadcastReceiver debugReceiver;
    private ReconnectRunnable deviceReconnectRunnable;
    private FileTransferHandler fileTransferHandler;
    private final GpsManager gpsManager;
    private LinkManager.LinkFactory iAPLinkFactory;
    private boolean isSimulatingGpsCoordinates;
    private boolean needAutoSearch;
    private final RouteRecorder routeRecorder;
    private DeviceSearch search;
    
    public HudConnectionService() {
        this.iAPLinkFactory = new LinkManager.LinkFactory() {
            @Override
            public Link build(final ConnectionInfo connectionInfo) {
                return new iAP2Link(new ConnectionServiceAnalyticsSupport.IAPListenerReceiver(), HudConnectionService.this.getApplicationContext());
            }
        };
        this.routeRecorder = RouteRecorder.getInstance();
        this.gpsManager = GpsManager.getInstance();
        this.needAutoSearch = true;
        this.bluetoothReceiver = new BroadcastReceiver() {
            private BluetoothDevice cancelledDevice;
            private long cancelledTime;
            
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                final BluetoothDevice cancelledDevice = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                if (action.equals("android.bluetooth.device.action.BOND_STATE_CHANGED")) {
                    final int intExtra = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", Integer.MIN_VALUE);
                    HudConnectionService.this.logger.d("Bond state change - device:" + cancelledDevice + " state:" + intExtra);
                    if (intExtra == 11 && ((HudConnectionService.this.search != null && HudConnectionService.this.search.forgetDevice(cancelledDevice)) || (HudConnectionService.this.mRemoteDevice != null && HudConnectionService.this.mRemoteDevice.getDeviceId().getBluetoothAddress().equals(cancelledDevice.getAddress())))) {
                        HudConnectionService.this.logger.i("Unexpected bonding with known device - removing bond");
                        HudConnectionService.this.forgetPairedDevice(cancelledDevice);
                        BluetoothUtil.cancelBondProcess(cancelledDevice);
                        BluetoothUtil.removeBond(cancelledDevice);
                        this.cancelledDevice = cancelledDevice;
                        this.cancelledTime = SystemClock.elapsedRealtime();
                        HudConnectionService.this.serviceHandler.removeCallbacks(HudConnectionService.this.reconnectRunnable);
                        HudConnectionService.this.setActiveDevice(null);
                    }
                }
                else if (action.equals("android.bluetooth.device.action.PAIRING_REQUEST")) {
                    if (cancelledDevice.equals(this.cancelledDevice) && SystemClock.elapsedRealtime() - this.cancelledTime < 250L) {
                        HudConnectionService.this.logger.d("Aborting pairing request for cancelled bonding");
                        this.abortBroadcast();
                    }
                    this.cancelledDevice = null;
                }
                else if (action.equals("android.bluetooth.device.action.ACL_DISCONNECTED") && HudConnectionService.this.search != null) {
                    HudConnectionService.this.search.handleDisconnect(cancelledDevice);
                }
            }
        };
        this.deviceReconnectRunnable = new ReconnectRunnable();
    }
    
    private boolean parseStartDrivePlaybackEvent(final byte[] array) {
        boolean booleanValue = false;
        try {
            final StartDrivePlaybackEvent startDrivePlaybackEvent = (StartDrivePlaybackEvent)NavdyEventUtil.messageFromEvent(this.mWire.<NavdyEvent>parseFrom(array, NavdyEvent.class));
            final RouteRecorder routeRecorder = this.routeRecorder;
            final String label = startDrivePlaybackEvent.label;
            if (startDrivePlaybackEvent.playSecondaryLocation != null) {
                booleanValue = startDrivePlaybackEvent.playSecondaryLocation;
            }
            routeRecorder.startPlayback(label, booleanValue, false);
            this.logger.v("onStartDrivePlayback, name=" + startDrivePlaybackEvent.label);
            return true;
        }
        catch (Throwable t) {
            this.logger.e(t);
            return true;
        }
    }
    
    private boolean parseStartDriveRecordingEvent(final byte[] array) {
        while (true) {
            try {
                final StartDriveRecordingEvent startDriveRecordingEvent = (StartDriveRecordingEvent)NavdyEventUtil.messageFromEvent(this.mWire.<NavdyEvent>parseFrom(array, NavdyEvent.class));
                if (startDriveRecordingEvent != null) {
                    final String startRecording = this.routeRecorder.startRecording(startDriveRecordingEvent.label, false);
                    if (!TextUtils.isEmpty((CharSequence)startRecording)) {
                        this.forwardEventLocally(new StartDriveRecordingEvent(startRecording));
                        this.logger.v("onStartDriveRecording, name=" + startDriveRecordingEvent.label);
                    }
                    else {
                        this.logger.e("startDriveRecording event fileName is empty");
                    }
                    return true;
                }
            }
            catch (Throwable t) {
                this.logger.e(t);
                return true;
            }
            this.logger.e("startDriveRecording event is null");
            return true;
        }
    }
    
    private void selectDevice(final NavdyDeviceId navdyDeviceId) {
        if (this.search != null) {
            this.logger.i("Connecting to " + navdyDeviceId);
            final RemoteDevice select = this.search.select(navdyDeviceId);
            if (select != null) {
                this.logger.i("Found corresponding remote device:" + select);
                this.setActiveDevice(select);
            }
        }
        else {
            final ConnectionInfo device = this.deviceRegistry.findDevice(navdyDeviceId);
            if (device != null) {
                this.logger.i("Found corresponding connection info:" + device);
                this.connect(device);
            }
        }
    }
    
    public void broadcastReconnectingIntent(final String s) {
        final Intent intent = new Intent("com.navdy.hud.app.force_reconnect");
        intent.putExtra("force_reconnect_reason", s);
        this.sendBroadcast(intent);
    }
    
    @Override
    protected Tunnel createProxyService() throws IOException {
        return new Tunnel(new TCPSocketAcceptor(3000, true), new SocketFactory() {
            @Override
            public SocketAdapter build() throws IOException {
                SocketAdapter build;
                if (iAP2Link.proxyEASession != null) {
                    build = new EASessionSocketAdapter(iAP2Link.proxyEASession);
                }
                else {
                    if (HudConnectionService.this.mRemoteDevice == null) {
                        throw new IOException("can't create proxy tunnel because HUD is not connected to remote device");
                    }
                    build = new BTSocketFactory(HudConnectionService.this.mRemoteDevice.getDeviceId().getBluetoothAddress(), ConnectionService.NAVDY_PROXY_TUNNEL_UUID).build();
                }
                return build;
            }
        });
    }
    
    @Override
    protected void enterState(final State state) {
        switch (state) {
            case IDLE:
                if (this.search != null) {
                    this.setState(State.SEARCHING);
                    break;
                }
                break;
            case SEARCHING:
                if (this.isPromiscuous()) {
                    this.startBroadcasters();
                }
                this.startListeners();
                if (this.search == null) {
                    (this.search = new DeviceSearch((Context)this, (DeviceSearch.EventSink)new DeviceSearch.EventSink() {
                        @Override
                        public void onEvent(final Message message) {
                            if (message instanceof ConnectionStatus && ((ConnectionStatus)message).status == ConnectionStatus.Status.CONNECTION_SEARCH_FINISHED && HudConnectionService.this.state == State.SEARCHING) {
                                if (HudConnectionService.this.search != null) {
                                    HudConnectionService.this.search.close();
                                    HudConnectionService.this.search = null;
                                }
                                HudConnectionService.this.setState(State.IDLE);
                            }
                            ConnectionService.this.forwardEventLocally(message);
                        }
                    }, this.inProcess)).next();
                }
                if (this.mRemoteDevice != null && (this.mRemoteDevice.isConnected() || this.mRemoteDevice.isConnecting())) {
                    this.setState(State.DISCONNECTING);
                    break;
                }
                break;
            case CONNECTED:
                this.needAutoSearch = false;
                if (this.search != null) {
                    this.search.close();
                    this.search = null;
                }
                if (this.mRemoteDevice != null && this.deviceRegistry.findDevice(this.mRemoteDevice.getDeviceId()) == null) {
                    ConnectionServiceAnalyticsSupport.recordNewDevice();
                    break;
                }
                break;
            case CONNECTING:
            case RECONNECTING:
                this.needAutoSearch = false;
                break;
        }
        super.enterState(state);
    }
    
    @Override
    protected void exitState(final State state) {
        super.exitState(state);
        switch (state) {
            case START: {
                final IntentFilter intentFilter = new IntentFilter("android.bluetooth.device.action.PAIRING_REQUEST");
                intentFilter.setPriority(1);
                intentFilter.addAction("android.bluetooth.device.action.ACL_DISCONNECTED");
                intentFilter.addAction("android.bluetooth.device.action.BOND_STATE_CHANGED");
                this.registerReceiver(this.bluetoothReceiver, intentFilter);
                break;
            }
        }
    }
    
    @Override
    protected void forgetPairedDevice(final BluetoothDevice bluetoothDevice) {
        if (this.search != null) {
            this.search.forgetDevice(bluetoothDevice);
        }
        super.forgetPairedDevice(bluetoothDevice);
    }
    
    @Override
    protected ConnectionListener[] getConnectionListeners(final Context context) {
        ConnectionListener[] array;
        if (DeviceUtil.isNavdyDevice()) {
            array = new ConnectionListener[] { new AcceptorListener(context, new BTSocketAcceptor("Navdy", HudConnectionService.NAVDY_PROTO_SERVICE_UUID), ConnectionType.BT_PROTOBUF), new AcceptorListener(context, new BTSocketAcceptor("Navdy iAP", HudConnectionService.ACCESSORY_IAP2), ConnectionType.BT_IAP2_LINK) };
        }
        else {
            array = new ConnectionListener[] { new AcceptorListener(context, new TCPSocketAcceptor(21301), ConnectionType.TCP_PROTOBUF), new AcceptorListener(context, new BTSocketAcceptor("Navdy", HudConnectionService.NAVDY_PROTO_SERVICE_UUID), ConnectionType.BT_PROTOBUF) };
        }
        return array;
    }
    
    public DeviceInfo.Platform getDevicePlatform() {
        ProtoEnum platform;
        final ProtoEnum protoEnum = platform = null;
        try {
            if (this.mRemoteDevice != null) {
                if (!this.mRemoteDevice.isConnected()) {
                    platform = protoEnum;
                }
                else {
                    platform = this.mRemoteDevice.getDeviceInfo().platform;
                }
            }
            return (DeviceInfo.Platform)platform;
        }
        catch (Throwable t) {
            platform = protoEnum;
            return (DeviceInfo.Platform)platform;
        }
        return (DeviceInfo.Platform)platform;
    }
    
    @Override
    protected RemoteDeviceBroadcaster[] getRemoteDeviceBroadcasters() {
        RemoteDeviceBroadcaster[] array;
        if (DeviceUtil.isNavdyDevice()) {
            array = new RemoteDeviceBroadcaster[] { new BTDeviceBroadcaster() };
        }
        else {
            array = new RemoteDeviceBroadcaster[] { new BTDeviceBroadcaster(), new TCPRemoteDeviceBroadcaster(this.getApplicationContext()) };
        }
        return array;
    }
    
    @Override
    protected void handleDeviceDisconnect(final RemoteDevice remoteDevice, final DisconnectCause disconnectCause) {
        super.handleDeviceDisconnect(remoteDevice, disconnectCause);
        this.logger.d("HUDConnectionService: handleDeviceDisconnect " + remoteDevice + ", Cause :" + disconnectCause);
        if (this.mRemoteDevice == remoteDevice && disconnectCause == DisconnectCause.ABORTED && this.serverMode) {
            this.logger.d("HUDConnectionService: Reporting the non fatal crash, Bluetooth disconnected");
            CrashReportService.dumpCrashReportAsync(CrashReportService.CrashType.BLUETOOTH_DISCONNECTED);
        }
    }
    
    @Override
    protected void heartBeat() {
        switch (this.state) {
            case SEARCHING:
                this.search.next();
                break;
        }
        super.heartBeat();
    }
    
    @Override
    public boolean isPromiscuous() {
        return PowerManager.isAwake();
    }
    
    @Override
    public boolean needAutoSearch() {
        return this.needAutoSearch && PowerManager.isAwake();
    }
    
    @Override
    public void onCreate() {
        super.onCreate();
        LinkManager.registerFactory(ConnectionType.BT_IAP2_LINK, this.iAPLinkFactory);
        LinkManager.registerFactory(ConnectionType.EA_PROTOBUF, this.iAPLinkFactory);
        this.gpsManager.setConnectionService(this);
        this.routeRecorder.setConnectionService(this);
        this.serverMode = true;
        this.inProcess = false;
        NetworkActivityTracker.getInstance().start();
        this.fileTransferHandler = new FileTransferHandler(this.getApplicationContext());
        if (!DeviceUtil.isUserBuild()) {
            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("LINK_BANDWIDTH_LEVEL_CHANGED");
            intentFilter.addCategory("NAVDY_LINK");
            this.registerReceiver(this.debugReceiver = new BroadcastReceiver() {
                public void onReceive(final Context context, final Intent intent) {
                    ConnectionService.this.setBandwidthLevel(intent.getIntExtra("EXTRA_BANDWIDTH_MODE", 1));
                }
            }, intentFilter);
        }
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.routeRecorder.stopRecording(false);
        this.gpsManager.shutdown();
        this.unregisterReceiver(this.bluetoothReceiver);
        if (this.debugReceiver != null) {
            this.unregisterReceiver(this.debugReceiver);
        }
    }
    
    @Override
    public void onDeviceConnected(final RemoteDevice remoteDevice) {
        super.onDeviceConnected(remoteDevice);
        this.fileTransferHandler.onDeviceConnected(remoteDevice);
        this.startProxyService();
    }
    
    @Override
    public void onDeviceDisconnected(final RemoteDevice remoteDevice, final DisconnectCause disconnectCause) {
        super.onDeviceDisconnected(remoteDevice, disconnectCause);
        this.fileTransferHandler.onDeviceDisconnected();
    }
    
    @Override
    protected boolean processEvent(final byte[] array, final NavdyEvent.MessageType messageType) {
        final boolean b = true;
        switch (messageType) {
            case Coordinate: {
                boolean startDriveRecordingEvent = b;
                try {
                    if (!this.isSimulatingGpsCoordinates) {
                        this.gpsManager.feedLocation((Coordinate)NavdyEventUtil.messageFromEvent(this.mWire.<NavdyEvent>parseFrom(array, NavdyEvent.class)));
                        startDriveRecordingEvent = b;
                    }
                }
                catch (Throwable t) {
                    this.logger.e(t);
                    startDriveRecordingEvent = b;
                }
                return startDriveRecordingEvent;
            }
            case StartDriveRecordingEvent: {
                return this.parseStartDriveRecordingEvent(array);
            }
            case StopDriveRecordingEvent: {
                this.routeRecorder.stopRecording(false);
                this.logger.v("onStopDriveRecording");
                return b;
            }
            case DriveRecordingsRequest: {
                this.routeRecorder.requestRecordings();
                this.logger.v("onDriveRecordingsRequest");
                return b;
            }
            case StartDrivePlaybackEvent: {
                this.parseStartDrivePlaybackEvent(array);
                return false;
            }
            case StopDrivePlaybackEvent: {
                this.routeRecorder.stopPlayback();
                this.logger.v("onStopDrivePlayback");
                return false;
            }
            case FileTransferRequest: {
                boolean startDriveRecordingEvent;
                try {
                    this.fileTransferHandler.onFileTransferRequest((FileTransferRequest)NavdyEventUtil.messageFromEvent(this.mWire.<NavdyEvent>parseFrom(array, NavdyEvent.class)));
                    startDriveRecordingEvent = b;
                }
                catch (Throwable t2) {
                    this.logger.e(t2);
                    startDriveRecordingEvent = b;
                }
                return startDriveRecordingEvent;
            }
            case FileTransferStatus: {
                boolean startDriveRecordingEvent;
                try {
                    this.fileTransferHandler.onFileTransferStatus((FileTransferStatus)NavdyEventUtil.messageFromEvent(this.mWire.<NavdyEvent>parseFrom(array, NavdyEvent.class)));
                    startDriveRecordingEvent = b;
                }
                catch (Throwable t3) {
                    this.logger.e(t3);
                    startDriveRecordingEvent = b;
                }
                return startDriveRecordingEvent;
            }
            case FileTransferData: {
                boolean startDriveRecordingEvent;
                try {
                    this.fileTransferHandler.onFileTransferData((FileTransferData)NavdyEventUtil.messageFromEvent(this.mWire.<NavdyEvent>parseFrom(array, NavdyEvent.class)));
                    startDriveRecordingEvent = b;
                }
                catch (Throwable t4) {
                    this.logger.e(t4);
                    startDriveRecordingEvent = b;
                }
                return startDriveRecordingEvent;
            }
            case ConnectionStateChange:
                while (true) {
                    while (true) {
                        Label_0483: {
                            try {
                                switch (((ConnectionStateChange)NavdyEventUtil.messageFromEvent(this.mWire.<NavdyEvent>parseFrom(array, NavdyEvent.class))).state) {
                                    case CONNECTION_DISCONNECTED: {
                                        final int int1 = SystemProperties.getInt("persist.sys.app_reconnect_ms", 1000);
                                        this.deviceReconnectRunnable.setReason("CONNECTION_DISCONNECTED");
                                        this.serviceHandler.postDelayed((Runnable)this.deviceReconnectRunnable, (long)int1);
                                        break;
                                    }
                                    case CONNECTION_CONNECTED:
                                    case CONNECTION_LINK_LOST:
                                        break Label_0483;
                                }
                                return false;
                            }
                            catch (Throwable t5) {
                                this.logger.e(t5);
                                return false;
                            }
                            continue;
                        }
                        this.serviceHandler.removeCallbacks((Runnable)this.deviceReconnectRunnable);
                        continue;
                    }
                }
                break;
            case AudioStatus:
                Label_0639: {
                    Label_0621: {
                        try {
                            final AudioStatus audioStatus = (AudioStatus)NavdyEventUtil.messageFromEvent(this.mWire.<NavdyEvent>parseFrom(array, NavdyEvent.class));
                            if (audioStatus != null && audioStatus.profileType == AudioStatus.ProfileType.AUDIO_PROFILE_HFP) {
                                this.logger.d("AudioStatus for HFP received");
                                if (!Wire.<Boolean>get(audioStatus.isConnected, false)) {
                                    break Label_0639;
                                }
                                this.logger.d("AudioStatus: HFP connected");
                                if (!Wire.<Boolean>get(audioStatus.hasSCOConnection, false)) {
                                    break Label_0621;
                                }
                                this.logger.d("AudioStatus: has SCO connection");
                                this.setBandwidthLevel(0);
                            }
                        }
                        catch (Throwable t6) {
                            this.logger.e(t6);
                        }
                        break;
                    }
                    this.logger.d("AudioStatus: does not have SCO connection");
                    this.setBandwidthLevel(1);
                    break;
                }
                this.logger.d("AudioStatus: HFP disconnected");
                this.setBandwidthLevel(1);
                break;
        }
        return false;
    }
    
    @Override
    protected boolean processLocalEvent(final byte[] array, final NavdyEvent.MessageType messageType) {
        final boolean b = true;
        if (messageType != NavdyEvent.MessageType.ConnectionRequest) {
            if (messageType == NavdyEvent.MessageType.StartDrivePlaybackEvent) {
                this.parseStartDrivePlaybackEvent(array);
                this.forwardEventLocally(array);
                return b;
            }
            if (messageType == NavdyEvent.MessageType.StopDrivePlaybackEvent) {
                this.routeRecorder.stopPlayback();
                this.forwardEventLocally(array);
                this.logger.v("onStopDrivePlayback");
                return b;
            }
            if (messageType == NavdyEvent.MessageType.StartDriveRecordingEvent) {
                return this.parseStartDriveRecordingEvent(array);
            }
            if (messageType == NavdyEvent.MessageType.StopDriveRecordingEvent) {
                this.routeRecorder.stopRecording(false);
                this.forwardEventLocally(array);
            }
            return false;
        }
        while (true) {
            Label_0152: {
                Label_0139: {
                    Label_0127: {
                        try {
                            final ConnectionRequest connectionRequest = this.mWire.<NavdyEvent>parseFrom(array, NavdyEvent.class).<ConnectionRequest>getExtension(Ext_NavdyEvent.connectionRequest);
                            boolean startDriveRecordingEvent = b;
                            if (connectionRequest != null) {
                                switch (connectionRequest.action) {
                                    default:
                                        startDriveRecordingEvent = b;
                                        break;
                                    case CONNECTION_SELECT:
                                        if (connectionRequest.remoteDeviceId != null) {
                                            this.selectDevice(new NavdyDeviceId(connectionRequest.remoteDeviceId));
                                            startDriveRecordingEvent = b;
                                            break;
                                        }
                                        break Label_0127;
                                    case CONNECTION_START_SEARCH:
                                        break Label_0139;
                                    case CONNECTION_STOP_SEARCH:
                                        break Label_0152;
                                }
                            }
                            return startDriveRecordingEvent;
                        }
                        catch (Throwable t) {
                            this.logger.e(t);
                        }
                        break;
                    }
                    this.setActiveDevice(null);
                    return b;
                }
                this.setState(State.SEARCHING);
                return b;
            }
            this.serviceHandler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    HudConnectionService.this.logger.i("Ending search");
                    if (HudConnectionService.this.search != null) {
                        HudConnectionService.this.search.close();
                        HudConnectionService.this.search = null;
                    }
                    HudConnectionService.this.setState(State.IDLE);
                }
            });
            return b;
        }
        return false;
    }
    
    @Override
    public void reconnect(final String s) {
        synchronized (this) {
            this.broadcastReconnectingIntent(s);
            super.reconnect(s);
        }
    }
    
    @Override
    public boolean reconnectAfterDeadConnection() {
        return true;
    }
    
    @Override
    protected void sendEventsOnLocalConnect() {
        this.logger.v("sendEventsOnLocalConnect");
        final RouteRecorder instance = RouteRecorder.getInstance();
        if (instance.isRecording()) {
            final String label = instance.getLabel();
            if (label != null) {
                this.forwardEventLocally(new StartDriveRecordingEvent(label));
                this.logger.v("sendEventsOnLocalConnect: send recording event:" + label);
            }
            else {
                this.logger.v("sendEventsOnLocalConnect: invalid label");
            }
        }
    }
    
    public void setSimulatingGpsCoordinates(final boolean isSimulatingGpsCoordinates) {
        this.isSimulatingGpsCoordinates = isSimulatingGpsCoordinates;
    }
    
    public class ReconnectRunnable implements Runnable
    {
        private String reason;
        
        @Override
        public void run() {
            HudConnectionService.this.reconnect(this.reason);
        }
        
        public void setReason(final String reason) {
            this.reason = reason;
        }
    }
}
