package com.navdy.hud.app.service;

import android.os.TransactionTooLargeException;
import android.os.DeadObjectException;
import android.os.SystemClock;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.events.WireUtil;
import com.squareup.otto.Subscribe;
import com.squareup.wire.Message;
import com.navdy.service.library.events.NavdyEventUtil;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.Ext_NavdyEvent;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import android.os.RemoteException;
import com.navdy.hud.app.event.InitEvents;
import android.content.Intent;
import android.os.IBinder;
import android.content.ComponentName;
import android.content.ServiceConnection;
import com.squareup.wire.Wire;
import com.navdy.hud.app.IEventSource;
import android.content.Context;
import com.navdy.hud.app.IEventListener;
import com.navdy.service.library.events.debug.StartDriveRecordingEvent;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class ConnectionServiceProxy
{
    private static final boolean VERBOSE = true;
    private static final Logger sLogger;
    protected Bus bus;
    private StartDriveRecordingEvent driveRecordingEvent;
    private IEventListener.Stub eventListener;
    protected Context mContext;
    protected IEventSource mEventSource;
    protected Wire mWire;
    private ServiceConnection serviceConnection;
    
    static {
        sLogger = new Logger(ConnectionServiceProxy.class);
    }
    
    public ConnectionServiceProxy(final Context mContext, final Bus bus) {
        this.serviceConnection = (ServiceConnection)new ServiceConnection() {
            public void onServiceConnected(final ComponentName componentName, final IBinder binder) {
                ConnectionServiceProxy.sLogger.i("Connected to IEventSource service");
                ConnectionServiceProxy.this.mEventSource = IEventSource.Stub.asInterface(binder);
                while (true) {
                    try {
                        ConnectionServiceProxy.this.mEventSource.addEventListener(ConnectionServiceProxy.this.eventListener);
                        ConnectionServiceProxy.sLogger.d("Starting connection service state machine");
                        ConnectionServiceProxy.this.mContext.startService(new Intent(ConnectionServiceProxy.this.mContext, (Class)HudConnectionService.class));
                        ConnectionServiceProxy.this.bus.post(new InitEvents.ConnectionServiceStarted());
                    }
                    catch (RemoteException ex) {
                        ConnectionServiceProxy.sLogger.e("Failed to connect event listener", (Throwable)ex);
                        continue;
                    }
                    break;
                }
            }
            
            public void onServiceDisconnected(final ComponentName componentName) {
                ConnectionServiceProxy.sLogger.i("Disconnected from IEventSource service");
                ConnectionServiceProxy.this.bus.post(new ConnectionStateChange("", ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED));
                ConnectionServiceProxy.this.mEventSource = null;
            }
        };
        this.eventListener = new IEventListener.Stub() {
            public void onEvent(final byte[] array) throws RemoteException {
                ConnectionServiceProxy.this.postEvent(array);
            }
        };
        this.mContext = mContext;
        (this.bus = bus).register(this);
        this.mWire = new Wire((Class<?>[])new Class[] { Ext_NavdyEvent.class });
        ConnectionServiceProxy.sLogger.i("Creating connection service proxy:" + this);
    }
    
    public void connect() {
        if (this.mEventSource == null) {
            ConnectionServiceProxy.sLogger.d("Starting connection service");
            final Intent intent = new Intent(this.mContext, (Class)HudConnectionService.class);
            this.mContext.startService(intent);
            ConnectionServiceProxy.sLogger.d("Binding to connection service");
            intent.setAction(IEventSource.class.getName());
            this.mContext.bindService(intent, this.serviceConnection, 0);
        }
    }
    
    public boolean connected() {
        return this.mEventSource != null;
    }
    
    public void disconnect() {
        if (this.mEventSource == null) {
            return;
        }
        while (true) {
            try {
                this.mEventSource.removeEventListener(this.eventListener);
                this.mContext.unbindService(this.serviceConnection);
                this.mContext.stopService(new Intent(this.mContext, (Class)HudConnectionService.class));
                this.mEventSource = null;
            }
            catch (RemoteException ex) {
                ConnectionServiceProxy.sLogger.e("Failed to remove event listener", (Throwable)ex);
                continue;
            }
            break;
        }
    }
    
    public Bus getBus() {
        return this.bus;
    }
    
    public StartDriveRecordingEvent getDriverRecordingEvent() {
        return this.driveRecordingEvent;
    }
    
    @Subscribe
    public void onEvent(final NavdyEvent navdyEvent) {
        final Message messageFromEvent = NavdyEventUtil.messageFromEvent(navdyEvent);
        if (messageFromEvent != null) {
            if (ConnectionServiceProxy.sLogger.isLoggable(2)) {
                ConnectionServiceProxy.sLogger.d("Received message: " + messageFromEvent);
            }
            this.bus.post(messageFromEvent);
            switch (navdyEvent.type) {
                case StartDriveRecordingEvent:
                    this.driveRecordingEvent = (StartDriveRecordingEvent)messageFromEvent;
                    break;
                case StopDriveRecordingEvent:
                    this.driveRecordingEvent = null;
                    break;
            }
        }
    }
    
    public void postEvent(final byte[] t) {
        if (this.bus == null) {
            return;
        }
        try {
            this.bus.post(this.mWire.<NavdyEvent>parseFrom((byte[])t, NavdyEvent.class));
        }
        catch (Throwable t2) {
            int eventTypeIndex = -1;
            try {
                eventTypeIndex = WireUtil.getEventTypeIndex((byte[])t);
                ConnectionServiceProxy.sLogger.e("Ignoring invalid navdy event[" + eventTypeIndex + "]", t2);
            }
            catch (Throwable t) {}
        }
    }
    
    public void postRemoteEvent(final NavdyDeviceId navdyDeviceId, final NavdyEvent navdyEvent) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                final int n = -1;
                Label_0290: {
                    if (ConnectionServiceProxy.this.mEventSource == null) {
                        break Label_0290;
                    }
                    int length = n;
                    try {
                        final long elapsedRealtime = SystemClock.elapsedRealtime();
                        length = n;
                        final byte[] byteArray = navdyEvent.toByteArray();
                        length = n;
                        final long elapsedRealtime2 = SystemClock.elapsedRealtime();
                        length = n;
                        final int n2 = length = byteArray.length;
                        if (ConnectionServiceProxy.sLogger.isLoggable(2)) {
                            length = n2;
                            final Logger access$000 = ConnectionServiceProxy.sLogger;
                            length = n2;
                            length = n2;
                            final StringBuilder sb = new StringBuilder();
                            length = n2;
                            access$000.v(sb.append("NAVDY-PACKET [H2P-Outgoing-Event] ").append(navdyEvent).toString());
                        }
                        final long n3 = elapsedRealtime2 - elapsedRealtime;
                        if (n3 >= 100L) {
                            length = n2;
                            final Logger access$2 = ConnectionServiceProxy.sLogger;
                            length = n2;
                            length = n2;
                            final StringBuilder sb2 = new StringBuilder();
                            length = n2;
                            access$2.v(sb2.append("NAVDY-PACKET [H2P-Outgoing-Event]").append(navdyEvent.type.name()).append(" took: ").append(n3).append(" len:").append(n2).toString());
                        }
                        length = n2;
                        ConnectionServiceProxy.this.mEventSource.postRemoteEvent(navdyDeviceId.toString(), byteArray);
                        return;
                    }
                    catch (DeadObjectException ex) {
                        ConnectionServiceProxy.sLogger.e("Exception posting remote event, server died", (Throwable)ex);
                        return;
                    }
                    catch (TransactionTooLargeException ex2) {
                        ConnectionServiceProxy.sLogger.e("Exception posting remote event, large payload type[" + navdyEvent.type + "] size[" + length + "]", (Throwable)ex2);
                        return;
                    }
                    catch (Throwable t) {
                        ConnectionServiceProxy.sLogger.e("Exception posting remote event", t);
                        return;
                    }
                }
                ConnectionServiceProxy.sLogger.e("Failed to send event - service connection broken" + navdyEvent);
            }
        }, 11);
    }
}
