package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import java.io.IOException;

public class GetTrackInfoExtended extends BaseOutgoingEmptyMessage
{
    public static final GetTrackInfoExtended INSTANCE;
    
    static {
        INSTANCE = new GetTrackInfoExtended();
    }
    
    @Override
    protected byte getMessageType() {
        return 22;
    }
    
    @Override
    public String toString() {
        return "Getting extended track's info";
    }
}
