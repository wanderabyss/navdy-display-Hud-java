package com.navdy.hud.app.service;

import com.navdy.hud.mfi.IAPListener;
import android.os.Process;
import com.navdy.hud.app.HudApplication;
import android.content.Intent;

public class ConnectionServiceAnalyticsSupport
{
    public static void recordGpsAccuracy(final String s, final String s2, final String s3) {
        sendEvent("GPS_Accuracy", "Min_Accuracy", s, "Max_Accuracy", s2, "Average_Accuracy", s3);
    }
    
    public static void recordGpsAcquireLocation(final String s, final String s2) {
        sendEvent("GPS_Acquired_Location", "time", s, "accuracy", s2);
    }
    
    public static void recordGpsAttemptAcquireLocation() {
        sendEvent("GPS_Attempt_Acquire_Location", new String[0]);
    }
    
    public static void recordGpsLostLocation(final String s) {
        sendEvent("GPS_Lost_Signal", "time", s);
    }
    
    public static void recordNewDevice() {
        sendEvent("Analytics_New_Device", new String[0]);
    }
    
    private static void sendEvent(final String s, final boolean b, final String... array) {
        final Intent intent = new Intent("com.navdy.hud.app.analytics.AnalyticsEvent");
        intent.putExtra("tag", s);
        intent.putExtra("arguments", array);
        if (b) {
            intent.putExtra("include_device_info", true);
        }
        HudApplication.getAppContext().sendBroadcastAsUser(intent, Process.myUserHandle());
    }
    
    private static void sendEvent(final String s, final String... array) {
        sendEvent(s, false, array);
    }
    
    public static class IAPListenerReceiver implements IAPListener
    {
        @Override
        public void onCoprocessorStatusCheckFailed(final String s, final String s2, final String s3) {
            sendEvent("Mobile_iAP_Failure", true, new String[] { "Description", s, "Status", s2, "Error_Code", s3 });
        }
        
        @Override
        public void onDeviceAuthenticationSuccess(final int n) {
            if (n > 0) {
                sendEvent("Mobile_iAP_Retry_Success", true, new String[] { "Retries", Integer.toString(n) });
            }
        }
    }
}
