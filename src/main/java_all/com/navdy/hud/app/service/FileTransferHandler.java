package com.navdy.hud.app.service;

import com.navdy.service.library.events.file.FileTransferResponse;
import com.navdy.service.library.events.file.FileType;
import com.navdy.service.library.events.file.FileTransferRequest;
import com.navdy.service.library.events.file.FileTransferError;
import com.navdy.service.library.events.file.FileTransferStatus;
import com.navdy.hud.app.util.os.SystemProperties;
import android.os.SystemClock;
import com.squareup.wire.Message;
import android.os.Process;
import com.navdy.hud.app.HudApplication;
import android.content.Intent;
import java.io.File;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.events.file.FileTransferData;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.file.FileTransferSessionManager;
import com.navdy.service.library.file.IFileTransferAuthority;
import android.content.Context;
import com.navdy.service.library.log.Logger;

public class FileTransferHandler
{
    public static final String ACTION_OTA_DOWNLOAD = "com.navdy.hud.app.service.OTA_DOWNLOAD";
    public static final String OTA_DOWLOADING_PROPERTY = "navdy.ota.downloading";
    static final Logger sLogger;
    private boolean closed;
    private Context context;
    IFileTransferAuthority fileTransferAuthority;
    FileTransferSessionManager fileTransferManager;
    private final Logger logger;
    RemoteDevice remoteDevice;
    
    static {
        sLogger = new Logger(FileTransferHandler.class);
    }
    
    public FileTransferHandler(final Context context) {
        this.logger = new Logger(this.getClass());
        this.fileTransferAuthority = PathManager.getInstance();
        this.fileTransferManager = new FileTransferSessionManager(context, this.fileTransferAuthority);
        this.context = context;
    }
    
    private void onFileTransferDone(final String s) {
        synchronized (this) {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    FileTransferHandler.this.logger.v("onFileTransferDone");
                    final File file = new File(s);
                    if (!file.exists() || !file.isFile() || !file.canRead()) {
                        FileTransferHandler.this.logger.e("Cannot read from downloaded OTA file " + file.getAbsolutePath());
                    }
                    else {
                        final Intent intent = new Intent("com.navdy.hud.app.service.OTA_DOWNLOAD");
                        intent.putExtra("path", file.getAbsolutePath());
                        HudApplication.getAppContext().sendBroadcastAsUser(intent, Process.myUserHandle());
                    }
                }
            }, 5);
        }
    }
    
    private FileTransferData sendNextChunk(final int n) throws Throwable {
        final FileTransferData nextChunk = this.fileTransferManager.getNextChunk(n);
        if (nextChunk != null) {
            this.remoteDevice.postEvent(nextChunk);
        }
        return nextChunk;
    }
    
    private void setDownloadingProperty(final boolean b) {
        int n = 0;
        if (b) {
            n = (int)(SystemClock.elapsedRealtime() / 1000L);
        }
        SystemProperties.set("navdy.ota.downloading", String.format("%d", n));
    }
    
    public void close() {
        if (!this.closed) {
            this.closed = true;
            if (this.fileTransferManager != null) {
                this.fileTransferManager.stop();
            }
        }
    }
    
    public void onDeviceConnected(final RemoteDevice remoteDevice) {
        this.remoteDevice = remoteDevice;
    }
    
    public void onDeviceDisconnected() {
        this.remoteDevice = null;
    }
    
    public void onFileTransferData(final FileTransferData fileTransferData) {
        if (!this.closed) {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    final boolean b = false;
                    FileTransferHandler.this.logger.v("onFileTransferData");
                    if (FileTransferHandler.this.remoteDevice.getLinkBandwidthLevel() <= 0) {
                        FileTransferHandler.this.remoteDevice.postEvent(new FileTransferStatus.Builder().success(false).transferComplete(false).error(FileTransferError.FILE_TRANSFER_HOST_BUSY).build());
                    }
                    else {
                        final String absolutePathForTransferId = FileTransferHandler.this.fileTransferManager.absolutePathForTransferId(fileTransferData.transferId);
                        final FileTransferStatus handleFileTransferData = FileTransferHandler.this.fileTransferManager.handleFileTransferData(fileTransferData);
                        FileTransferHandler.this.remoteDevice.postEvent(handleFileTransferData);
                        final FileTransferHandler this$0 = FileTransferHandler.this;
                        boolean b2 = b;
                        if (handleFileTransferData.success) {
                            b2 = b;
                            if (!handleFileTransferData.transferComplete) {
                                b2 = true;
                            }
                        }
                        this$0.setDownloadingProperty(b2);
                        if (absolutePathForTransferId != null && !absolutePathForTransferId.equals("") && handleFileTransferData.transferComplete) {
                            FileTransferHandler.this.onFileTransferDone(absolutePathForTransferId);
                        }
                    }
                }
            }, 5);
        }
    }
    
    public void onFileTransferRequest(final FileTransferRequest fileTransferRequest) {
        if (!this.closed) {
            if (fileTransferRequest != null && fileTransferRequest.fileType != null && fileTransferRequest.fileType == FileType.FILE_TYPE_LOGS) {
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (FileTransferHandler.this.remoteDevice.getLinkBandwidthLevel() <= 0) {
                            FileTransferHandler.sLogger.d("Link bandwidth is low");
                            if (fileTransferRequest != null) {
                                FileTransferHandler.sLogger.d("FileTransferRequest (pull) : rejecting file transfer request to avoid link traffic");
                                FileTransferHandler.this.remoteDevice.postEvent(new FileTransferResponse.Builder().success(false).destinationFileName(fileTransferRequest.destinationFileName).fileType(fileTransferRequest.fileType).error(FileTransferError.FILE_TRANSFER_HOST_BUSY).build());
                            }
                        }
                        else {
                            final FileTransferResponse handleFileTransferRequest = FileTransferHandler.this.fileTransferManager.handleFileTransferRequest(fileTransferRequest);
                            FileTransferHandler.this.remoteDevice.postEvent(handleFileTransferRequest);
                            if (handleFileTransferRequest.success) {
                                int intValue;
                                while (true) {
                                    intValue = handleFileTransferRequest.transferId;
                                    while (true) {
                                        Label_0230: {
                                            try {
                                                if (handleFileTransferRequest.supportsAcks) {
                                                    break;
                                                }
                                                FileTransferData access$000;
                                                do {
                                                    access$000 = FileTransferHandler.this.sendNextChunk(intValue);
                                                    if (access$000 == null) {
                                                        break Label_0230;
                                                    }
                                                    Thread.sleep(1000L);
                                                } while (access$000 != null && !access$000.lastChunk);
                                                if (access$000 != null && access$000.lastChunk) {
                                                    FileTransferHandler.this.fileTransferAuthority.onFileSent(fileTransferRequest.fileType);
                                                }
                                            }
                                            catch (Throwable t) {
                                                FileTransferHandler.sLogger.d("Exception while reading the data and sending it across", t);
                                            }
                                            return;
                                        }
                                        FileTransferHandler.sLogger.e("Error sending the chunk because data was null");
                                        continue;
                                    }
                                }
                                if (FileTransferHandler.this.sendNextChunk(intValue) != null) {
                                    FileTransferHandler.sLogger.d("Send the first chunk");
                                }
                                else {
                                    FileTransferHandler.sLogger.e("Error sending the first chunk because data was null");
                                }
                                FileTransferHandler.sLogger.d("Flow control enabled, waiting for the FileTransferStatus");
                            }
                        }
                    }
                }, 5);
            }
            else {
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        FileTransferHandler.this.logger.v("onFileTransferRequest");
                        if (FileTransferHandler.this.remoteDevice.getLinkBandwidthLevel() <= 0) {
                            FileTransferHandler.sLogger.d("Link bandwidth is low");
                            if (fileTransferRequest != null) {
                                FileTransferHandler.sLogger.d("FileTransferRequest (PUSH) : rejecting file transfer request to avoid link traffic");
                                FileTransferHandler.this.remoteDevice.postEvent(new FileTransferResponse.Builder().success(false).destinationFileName(fileTransferRequest.destinationFileName).fileType(fileTransferRequest.fileType).error(FileTransferError.FILE_TRANSFER_HOST_BUSY).build());
                            }
                        }
                        else {
                            FileTransferHandler.this.remoteDevice.postEvent(FileTransferHandler.this.fileTransferManager.handleFileTransferRequest(fileTransferRequest));
                        }
                    }
                }, 5);
            }
        }
    }
    
    public void onFileTransferStatus(final FileTransferStatus fileTransferStatus) {
        if (!this.closed) {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    if (fileTransferStatus != null) {
                        if (fileTransferStatus.success && fileTransferStatus.transferComplete) {
                            FileTransferHandler.sLogger.d("File Transfer complete " + fileTransferStatus.transferId);
                            final FileType fileType = FileTransferHandler.this.fileTransferManager.getFileType(fileTransferStatus.transferId);
                            FileTransferHandler.this.fileTransferManager.endFileTransferSession(fileTransferStatus.transferId, true);
                            if (fileType != null) {
                                FileTransferHandler.this.fileTransferAuthority.onFileSent(fileType);
                            }
                            else {
                                FileTransferHandler.sLogger.e("Cannot find FileType associated with transfer ID :" + fileTransferStatus.transferId);
                            }
                        }
                        else {
                            Label_0216: {
                                try {
                                    if (FileTransferHandler.this.remoteDevice.getLinkBandwidthLevel() > 0) {
                                        break Label_0216;
                                    }
                                    FileTransferHandler.sLogger.d("Link bandwidth is low, Not sending the next chunk. Ending the file transfer");
                                    FileTransferHandler.this.fileTransferManager.endFileTransferSession(fileTransferStatus.transferId, true);
                                }
                                catch (Throwable t) {
                                    FileTransferHandler.sLogger.d("Exception while reading the data and sending it across", t);
                                }
                                return;
                            }
                            if (FileTransferHandler.this.fileTransferManager.handleFileTransferStatus(fileTransferStatus)) {
                                if (FileTransferHandler.this.sendNextChunk(fileTransferStatus.transferId) != null) {
                                    FileTransferHandler.sLogger.d("Sent chunk");
                                }
                                else {
                                    FileTransferHandler.sLogger.e("Failed to get the data for the transfer session");
                                }
                            }
                            else {
                                FileTransferHandler.this.fileTransferManager.endFileTransferSession(fileTransferStatus.transferId, true);
                            }
                        }
                    }
                }
            }, 5);
        }
    }
}
