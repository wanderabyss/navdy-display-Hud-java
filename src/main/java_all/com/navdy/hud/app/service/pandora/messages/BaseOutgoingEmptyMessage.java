package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import java.io.IOException;
import java.io.ByteArrayOutputStream;

abstract class BaseOutgoingEmptyMessage extends BaseOutgoingConstantMessage
{
    protected abstract byte getMessageType();
    
    @Override
    protected ByteArrayOutputStream putThis(final ByteArrayOutputStream byteArrayOutputStream) throws IOException, StringOverflowException, UnexpectedEndOfStringException {
        BaseOutgoingMessage.putByte(byteArrayOutputStream, this.getMessageType());
        return byteArrayOutputStream;
    }
}
