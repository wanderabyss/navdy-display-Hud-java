package com.navdy.hud.app.gesture;

import com.navdy.service.library.events.input.GestureEvent;
import android.os.Looper;
import android.os.Handler;
import com.navdy.hud.app.manager.InputManager;

public class MultipleClickGestureDetector implements IInputHandler
{
    public static final int DOUBLE_CLICK = 2;
    private static final int MULTIPLE_CLICK_THRESHOLD = 400;
    private static final CustomKeyEvent MULTIPLIABLE_EVENT;
    public static final int TRIPLE_CLICK = 3;
    private final Handler handler;
    private IMultipleClickKeyGesture listener;
    private int maxAcceptableClickCount;
    private int multipleClickCount;
    private final Runnable multipleClickTrigger;
    
    static {
        MULTIPLIABLE_EVENT = CustomKeyEvent.SELECT;
    }
    
    public MultipleClickGestureDetector(final int maxAcceptableClickCount, final IMultipleClickKeyGesture listener) {
        this.handler = new Handler(Looper.getMainLooper());
        this.multipleClickTrigger = new Runnable() {
            @Override
            public void run() {
                MultipleClickGestureDetector.this.processEvent(null);
            }
        };
        if (listener == null || maxAcceptableClickCount <= 1) {
            throw new IllegalArgumentException();
        }
        this.maxAcceptableClickCount = maxAcceptableClickCount;
        this.listener = listener;
    }
    
    private boolean processEvent(final CustomKeyEvent customKeyEvent) {
        while (true) {
            boolean b = false;
            synchronized (this) {
                this.handler.removeCallbacks(this.multipleClickTrigger);
                if (this.listener == null) {
                    return b;
                }
                b = MultipleClickGestureDetector.MULTIPLIABLE_EVENT.equals(customKeyEvent);
                if (b) {
                    ++this.multipleClickCount;
                }
                while (this.multipleClickCount >= this.maxAcceptableClickCount) {
                    this.listener.onMultipleClick(this.maxAcceptableClickCount);
                    this.multipleClickCount -= this.maxAcceptableClickCount;
                }
            }
            if (b) {
                if (this.multipleClickCount > 0) {
                    this.handler.postDelayed(this.multipleClickTrigger, 400L);
                }
            }
            else {
                if (this.multipleClickCount > 1) {
                    this.listener.onMultipleClick(this.multipleClickCount);
                }
                else if (this.multipleClickCount == 1) {
                    ((InputManager.IInputHandler)this.listener).onKey(MultipleClickGestureDetector.MULTIPLIABLE_EVENT);
                }
                this.multipleClickCount = 0;
                final CustomKeyEvent customKeyEvent2;
                if (customKeyEvent2 != null) {
                    b = ((InputManager.IInputHandler)this.listener).onKey(customKeyEvent2);
                    switch (customKeyEvent2) {
                        case POWER_BUTTON_LONG_PRESS:
                        case LONG_PRESS:
                            return b;
                    }
                }
            }
            b = true;
            return b;
        }
    }
    
    @Override
    public IInputHandler nextHandler() {
        return null;
    }
    
    @Override
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    @Override
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        return this.processEvent(customKeyEvent);
    }
    
    public interface IMultipleClickKeyGesture extends IInputHandler
    {
        void onMultipleClick(final int p0);
    }
}
