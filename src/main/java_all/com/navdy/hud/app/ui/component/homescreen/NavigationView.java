package com.navdy.hud.app.ui.component.homescreen;

import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.config.SettingsManager;
import com.navdy.service.library.events.input.GestureEvent;
import butterknife.ButterKnife;
import com.squareup.otto.Subscribe;
import android.animation.ValueAnimator;
import android.animation.ObjectAnimator;
import android.animation.Animator;
import android.view.View;
import android.animation.AnimatorSet;
import java.util.Iterator;
import java.util.Collection;
import android.os.Looper;
import com.here.android.mpa.mapping.PositionIndicator;
import com.navdy.hud.app.device.gps.GpsUtils;
import com.here.android.mpa.common.ViewRect;
import com.here.android.mpa.common.GeoBoundingBox;
import com.navdy.hud.app.framework.DriverProfileHelper;
import android.os.SystemClock;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.hud.app.maps.here.HereRouteManager;
import com.navdy.hud.app.framework.destinations.DestinationsManager;
import com.navdy.hud.app.maps.here.HereMapUtil;
import android.view.ViewGroup;
import com.navdy.hud.app.view.MainView;
import com.navdy.hud.app.util.GenericUtil;
import com.here.android.mpa.common.GeoCoordinate;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.events.navigation.NavigationSessionStatusEvent;
import com.navdy.service.library.events.navigation.NavigationSessionState;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.framework.fuel.FuelRoutingManager;
import com.navdy.hud.app.util.FeatureUtil;
import android.graphics.PointF;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.service.library.task.TaskManager;
import com.here.android.mpa.common.GeoPosition;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.Route;
import com.navdy.hud.app.maps.here.HereMapCameraManager;
import com.here.android.mpa.mapping.MapState;
import android.util.AttributeSet;
import android.content.Context;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.ui.component.FluctuatorAnimatorView;
import com.here.android.mpa.common.Image;
import java.util.ArrayList;
import com.here.android.mpa.mapping.MapView;
import com.here.android.mpa.mapping.MapObject;
import java.util.List;
import butterknife.InjectView;
import android.widget.ImageView;
import com.navdy.hud.app.maps.here.HereMapController;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereLocationFixManager;
import android.os.Handler;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapMarker;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.squareup.otto.Bus;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.gesture.GestureDetector;
import com.navdy.hud.app.manager.InputManager;
import android.widget.FrameLayout;

public class NavigationView extends FrameLayout implements InputManager.IInputHandler, GestureDetector.GestureListener, IHomeScreenLifecycle
{
    private static final int MAP_MASK_OFFSET = 1;
    private static final MapEvents.DialMapZoom ZOOM_IN;
    private static final MapEvents.DialMapZoom ZOOM_OUT;
    private Bus bus;
    private Runnable cleanupMapOverviewRunnable;
    private DriverProfileManager driverProfileManager;
    private MapMarker endMarker;
    private Map.OnTransformListener fluctuatorPosListener;
    private Handler handler;
    private HereLocationFixManager hereLocationFixManager;
    private HereMapsManager hereMapsManager;
    private HomeScreenView homeScreenView;
    private boolean initComplete;
    private boolean isRenderingEnabled;
    private Logger logger;
    private HereMapController mapController;
    @InjectView(R.id.mapIconIndicator)
    ImageView mapIconIndicator;
    private List<MapObject> mapMarkerList;
    @InjectView(R.id.map_mask)
    ImageView mapMask;
    @InjectView(R.id.mapView)
    MapView mapView;
    private boolean networkCheckRequired;
    @InjectView(R.id.noLocationContainer)
    NoLocationView noLocationView;
    private ArrayList<MapObject> overviewMapRouteObjects;
    private boolean paused;
    private Runnable resetMapOverviewRunnable;
    private Image selectedDestinationImage;
    private boolean setInitialCenter;
    private Runnable showMapIconIndicatorRunnable;
    @InjectView(R.id.startDestinationFluctuator)
    FluctuatorAnimatorView startDestinationFluctuatorView;
    private MapMarker startMarker;
    private UIStateManager uiStateManager;
    private Image unselectedDestinationImage;
    
    static {
        ZOOM_IN = new MapEvents.DialMapZoom(MapEvents.DialMapZoom.Type.IN);
        ZOOM_OUT = new MapEvents.DialMapZoom(MapEvents.DialMapZoom.Type.OUT);
    }
    
    public NavigationView(final Context context) {
        this(context, null);
    }
    
    public NavigationView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public NavigationView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.logger = new Logger(NavigationView.class);
        this.handler = new Handler();
        this.overviewMapRouteObjects = new ArrayList<MapObject>();
        this.mapMarkerList = new ArrayList<MapObject>();
        this.fluctuatorPosListener = new Map.OnTransformListener() {
            @Override
            public void onMapTransformEnd(final MapState mapState) {
                NavigationView.this.mapController.removeTransformListener(this);
                NavigationView.this.positionFluctuator();
            }
            
            @Override
            public void onMapTransformStart() {
            }
        };
        this.showMapIconIndicatorRunnable = new Runnable() {
            @Override
            public void run() {
                NavigationView.this.mapIconIndicator.setVisibility(View.VISIBLE);
            }
        };
        this.resetMapOverviewRunnable = new Runnable() {
            @Override
            public void run() {
                NavigationView.this.logger.v("resetMapOverview: " + NavigationView.this.mapController.getState());
                NavigationView.this.cleanupMapOverview();
                if (HereMapsManager.getInstance().isInitialized()) {
                    final HereMapCameraManager instance = HereMapCameraManager.getInstance();
                    if (instance.isOverviewZoomLevel()) {
                        NavigationView.this.showOverviewMap(null, null, instance.getLastGeoCoordinate(), false);
                    }
                }
            }
        };
        this.cleanupMapOverviewRunnable = new Runnable() {
            @Override
            public void run() {
                NavigationView.this.logger.v("cleanupMapOverview");
                if (NavigationView.this.startMarker != null) {
                    NavigationView.this.mapController.removeMapObject(NavigationView.this.startMarker);
                }
                if (NavigationView.this.endMarker != null) {
                    NavigationView.this.mapController.removeMapObject(NavigationView.this.endMarker);
                }
                NavigationView.this.cleanupMapOverviewRoutes();
                NavigationView.this.cleanupFluctuator();
            }
        };
        if (!this.isInEditMode()) {
            this.uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
            this.hereMapsManager = HereMapsManager.getInstance();
            this.hereLocationFixManager = this.hereMapsManager.getLocationFixManager();
        }
    }
    
    private Map.OnTransformListener animateBackfromOverviewMap(final Runnable runnable) {
        return new Map.OnTransformListener() {
            @Override
            public void onMapTransformEnd(final MapState mapState) {
                NavigationView.this.logger.v("animateBackfromOverview: transform end");
                NavigationView.this.mapController.removeTransformListener(this);
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        NavigationView.this.handler.post(NavigationView.this.showMapIconIndicatorRunnable);
                        if (NavigationView.this.startMarker != null) {
                            NavigationView.this.mapController.removeMapObject(NavigationView.this.startMarker);
                        }
                        HereNavigationManager.getInstance().getCurrentMapRoute();
                        NavigationView.this.logger.v("animateBackfromOverview traffic=" + NavigationView.this.driverProfileManager.isTrafficEnabled());
                        NavigationView.this.mapController.setState(HereMapController.State.AR_MODE);
                        if (runnable != null) {
                            runnable.run();
                        }
                    }
                }, 17);
            }
            
            @Override
            public void onMapTransformStart() {
            }
        };
    }
    
    private void handleEngineInit() {
        while (true) {
            Label_0059: {
                synchronized (this) {
                    if (!this.hereMapsManager.isRenderingAllowed()) {
                        this.logger.v("onMapEngineInitializationCompleted: Maps Rendering not allowed, Quiet mode On!");
                    }
                    else {
                        if (!this.hereMapsManager.isInitialized()) {
                            break Label_0059;
                        }
                        this.logger.v("engine initialized");
                        this.onMapEngineInitializationCompleted();
                    }
                    return;
                }
            }
            this.logger.e("onCreate() : Cannot initialize map engine: " + this.hereMapsManager.getError());
            this.noLocationView.hideLocationUI();
        }
    }
    
    private void initViews() {
        if (!this.isInEditMode()) {
            if (this.hereLocationFixManager != null) {
                this.hereLocationFixManager.hasLocationFix();
            }
            if (!false || !this.initComplete) {
                this.logger.v("first layout: location fix[" + false + "] initComplete[" + this.initComplete + "]");
                this.noLocationView.showLocationUI();
            }
            else {
                this.logger.v("first layout: location fix available");
                this.setMapCenter();
            }
        }
    }
    
    private void onMapEngineInitializationCompleted() {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: dup            
        //     2: astore_1       
        //     3: monitorenter   
        //     4: aload_0        
        //     5: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.initComplete:Z
        //     8: istore_2       
        //     9: iload_2        
        //    10: ifeq            16
        //    13: aload_1        
        //    14: monitorexit    
        //    15: return         
        //    16: aload_0        
        //    17: aload_0        
        //    18: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;
        //    21: invokevirtual   com/navdy/hud/app/maps/here/HereMapsManager.getMapController:()Lcom/navdy/hud/app/maps/here/HereMapController;
        //    24: putfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
        //    27: aload_0        
        //    28: iconst_1       
        //    29: putfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.initComplete:Z
        //    32: aload_0        
        //    33: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.logger:Lcom/navdy/service/library/log/Logger;
        //    36: ldc_w           "onMapEngineInitializationCompleted: Maps Rendering allowed"
        //    39: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //    42: aload_0        
        //    43: invokevirtual   com/navdy/hud/app/ui/component/homescreen/NavigationView.enableRendering:()V
        //    46: aload_0        
        //    47: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.mapView:Lcom/here/android/mpa/mapping/MapView;
        //    50: getstatic       com/here/android/mpa/common/CopyrightLogoPosition.TOP_RIGHT:Lcom/here/android/mpa/common/CopyrightLogoPosition;
        //    53: invokevirtual   com/here/android/mpa/mapping/MapView.setCopyrightLogoPosition:(Lcom/here/android/mpa/common/CopyrightLogoPosition;)V
        //    56: aload_0        
        //    57: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.mapView:Lcom/here/android/mpa/mapping/MapView;
        //    60: invokevirtual   com/here/android/mpa/mapping/MapView.onResume:()V
        //    63: aload_0        
        //    64: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;
        //    67: aload_0        
        //    68: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.mapView:Lcom/here/android/mpa/mapping/MapView;
        //    71: aload_0        
        //    72: invokevirtual   com/navdy/hud/app/maps/here/HereMapsManager.setMapView:(Lcom/here/android/mpa/mapping/MapView;Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)V
        //    75: new             Lcom/here/android/mpa/common/Image;
        //    78: astore_3       
        //    79: aload_3        
        //    80: invokespecial   com/here/android/mpa/common/Image.<init>:()V
        //    83: aload_3        
        //    84: ldc_w           R.drawable.icon_driver_position
        //    87: invokevirtual   com/here/android/mpa/common/Image.setImageResource:(I)V
        //    90: aload_0        
        //    91: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;
        //    94: invokevirtual   com/navdy/hud/app/maps/here/HereMapsManager.getLocationFixManager:()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
        //    97: invokevirtual   com/navdy/hud/app/maps/here/HereLocationFixManager.getLastGeoCoordinate:()Lcom/here/android/mpa/common/GeoCoordinate;
        //   100: astore          4
        //   102: aload           4
        //   104: astore          5
        //   106: aload           4
        //   108: ifnonnull       127
        //   111: new             Lcom/here/android/mpa/common/GeoCoordinate;
        //   114: astore          5
        //   116: aload           5
        //   118: ldc2_w          37.802086
        //   121: ldc2_w          -122.419015
        //   124: invokespecial   com/here/android/mpa/common/GeoCoordinate.<init>:(DD)V
        //   127: new             Lcom/here/android/mpa/mapping/MapMarker;
        //   130: astore          4
        //   132: aload           4
        //   134: aload           5
        //   136: aload_3        
        //   137: invokespecial   com/here/android/mpa/mapping/MapMarker.<init>:(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/Image;)V
        //   140: aload_0        
        //   141: aload           4
        //   143: putfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.startMarker:Lcom/here/android/mpa/mapping/MapMarker;
        //   146: aload_0        
        //   147: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.logger:Lcom/navdy/service/library/log/Logger;
        //   150: ldc_w           "view rendered"
        //   153: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   156: aload_0        
        //   157: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.noLocationView:Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;
        //   160: invokevirtual   com/navdy/hud/app/ui/component/homescreen/NoLocationView.isAcquiringLocation:()Z
        //   163: ifeq            187
        //   166: aload_0        
        //   167: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.initComplete:Z
        //   170: ifeq            187
        //   173: aload_0        
        //   174: invokespecial   com/navdy/hud/app/ui/component/homescreen/NavigationView.setMapCenter:()Z
        //   177: ifeq            187
        //   180: aload_0        
        //   181: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.noLocationView:Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;
        //   184: invokevirtual   com/navdy/hud/app/ui/component/homescreen/NoLocationView.hideLocationUI:()V
        //   187: aload_0        
        //   188: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.bus:Lcom/squareup/otto/Bus;
        //   191: astore          5
        //   193: new             Lcom/navdy/hud/app/maps/MapEvents$MapUIReady;
        //   196: astore          4
        //   198: aload           4
        //   200: invokespecial   com/navdy/hud/app/maps/MapEvents$MapUIReady.<init>:()V
        //   203: aload           5
        //   205: aload           4
        //   207: invokevirtual   com/squareup/otto/Bus.post:(Ljava/lang/Object;)V
        //   210: goto            13
        //   213: astore          5
        //   215: aload_1        
        //   216: monitorexit    
        //   217: aload           5
        //   219: athrow         
        //   220: astore          5
        //   222: aload_0        
        //   223: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.logger:Lcom/navdy/service/library/log/Logger;
        //   226: aload           5
        //   228: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   231: goto            146
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  4      9      213    220    Any
        //  16     75     213    220    Any
        //  75     102    220    234    Ljava/lang/Throwable;
        //  75     102    213    220    Any
        //  111    127    220    234    Ljava/lang/Throwable;
        //  111    127    213    220    Any
        //  127    146    220    234    Ljava/lang/Throwable;
        //  127    146    213    220    Any
        //  146    187    213    220    Any
        //  187    210    213    220    Any
        //  222    231    213    220    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0127:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void positionFluctuator() {
        try {
            if (this.startMarker != null) {
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        final Map.PixelResult projectToPixel = NavigationView.this.mapController.projectToPixel(NavigationView.this.startMarker.getCoordinate());
                        if (projectToPixel != null) {
                            final PointF result = projectToPixel.getResult();
                            if (result != null) {
                                NavigationView.this.handler.post((Runnable)new Runnable() {
                                    final /* synthetic */ int val$x = (int)result.x - HomeScreenResourceValues.startFluctuatorDimension / 2;
                                    final /* synthetic */ int val$y = (int)result.y - HomeScreenResourceValues.startFluctuatorDimension / 2;
                                    
                                    @Override
                                    public void run() {
                                        final HereMapController.State state = NavigationView.this.mapController.getState();
                                        if (state == HereMapController.State.ROUTE_PICKER) {
                                            NavigationView.this.startDestinationFluctuatorView.setX((float)(this.val$x - HomeScreenResourceValues.transformCenterMoveX));
                                            NavigationView.this.startDestinationFluctuatorView.setY((float)this.val$y);
                                            NavigationView.this.startDestinationFluctuatorView.setVisibility(View.VISIBLE);
                                            NavigationView.this.startDestinationFluctuatorView.start();
                                        }
                                        else {
                                            NavigationView.this.logger.v("fluctuator state from route search mode:" + state);
                                        }
                                    }
                                });
                            }
                        }
                    }
                }, 3);
            }
        }
        catch (Throwable t) {
            this.logger.e(t);
        }
    }
    
    private boolean setMapCenter() {
        boolean b = true;
        if (!this.setInitialCenter) {
            if (!this.initComplete) {
                this.logger.w("setMapCenter:init not complete yet, deferring");
                b = false;
            }
            else {
                if (this.hereLocationFixManager == null) {
                    this.hereLocationFixManager = this.hereMapsManager.getLocationFixManager();
                }
                final GeoCoordinate lastGeoCoordinate = this.hereLocationFixManager.getLastGeoCoordinate();
                if (lastGeoCoordinate != null) {
                    this.setInitialCenter = true;
                    this.mapController.setCenter(lastGeoCoordinate, Map.Animation.NONE, -1.0, -1.0f, -1.0f);
                    this.logger.w("setMapCenter:setcenterInit done " + lastGeoCoordinate);
                    if (RemoteDeviceManager.getInstance().getFeatureUtil().isFeatureEnabled(FeatureUtil.Feature.FUEL_ROUTING)) {
                        FuelRoutingManager.getInstance().markAvailable();
                    }
                    this.updateMapIndicator();
                    if (NetworkStateManager.getInstance().isNetworkStateInitialized()) {
                        this.logger.v("checkPrevRoute:n/w is initialized");
                        this.startPreviousRoute(lastGeoCoordinate);
                        this.logger.v("send map engine ready event-1");
                        this.bus.post(new RemoteEvent(new NavigationSessionStatusEvent(NavigationSessionState.NAV_SESSION_ENGINE_READY, null, null, null, null)));
                        this.bus.post(new MapEvents.MapEngineReady());
                    }
                    else {
                        this.networkCheckRequired = true;
                        this.logger.v("checkPrevRoute:n/w is initialized, defer and wait");
                    }
                }
                else {
                    b = false;
                }
            }
        }
        return b;
    }
    
    private void setMapToArModeInternal(final boolean b, final boolean b2, final boolean b3, final GeoPosition geoPosition, double lastZoom, float lastTilt, final boolean b4) {
        GenericUtil.checkNotOnMainThread();
        this.logger.v("setMapToArModeInternal animate:" + b + " changeState:" + b2);
        final HereMapCameraManager instance = HereMapCameraManager.getInstance();
        GeoPosition lastGeoPosition = geoPosition;
        if (geoPosition == null) {
            lastGeoPosition = instance.getLastGeoPosition();
        }
        if (b4) {
            this.cleanupMapOverview();
        }
        GeoCoordinate geoCoordinate;
        float n;
        if (lastGeoPosition != null) {
            geoCoordinate = lastGeoPosition.getCoordinate();
            n = (float)lastGeoPosition.getHeading();
            if (lastTilt == -1.0f) {
                lastTilt = instance.getLastTilt();
            }
            if (b3) {
                lastZoom = instance.getLastZoom();
            }
            else if (lastZoom == -1.0) {
                lastZoom = 12.0;
            }
        }
        else {
            geoCoordinate = this.mapController.getCenter();
            n = -1.0f;
            if (lastTilt == -1.0f) {
                lastTilt = 60.0f;
            }
            if (lastZoom == -1.0) {
                lastZoom = 16.5;
            }
        }
        this.setTransformCenter(HereMapController.State.AR_MODE);
        final HereMapController mapController = this.mapController;
        final HereMapController.State state = this.mapController.getState();
        Map.Animation animation;
        if (b) {
            animation = Map.Animation.BOW;
        }
        else {
            animation = Map.Animation.NONE;
        }
        mapController.setCenterForState(state, geoCoordinate, animation, lastZoom, n, lastTilt);
        if (b2) {
            this.mapController.setState(HereMapController.State.AR_MODE);
        }
        else {
            this.mapController.setState(HereMapController.State.TRANSITION);
        }
    }
    
    private void setTransformCenter() {
        if (this.mapController == null) {
            this.logger.v("setTransformCenter:no map");
        }
        else {
            this.setTransformCenter(this.mapController.getState());
        }
    }
    
    private void setTransformCenter(final HereMapController.State state) {
        if (this.mapController == null) {
            this.logger.v("setTransformCenter:no map");
        }
        else {
            switch (state) {
                case OVERVIEW:
                    if (this.homeScreenView.isNavigationActive()) {
                        this.logger.v("setTransformCenter:overview:nav");
                        this.mapController.setTransformCenter(HomeScreenConstants.transformCenterSmallMiddle);
                        break;
                    }
                    this.logger.v("setTransformCenter:overview:open");
                    this.mapController.setTransformCenter(HomeScreenConstants.transformCenterLargeMiddle);
                    break;
                case ROUTE_PICKER:
                    this.logger.v("setTransformCenter:routeSearch");
                    this.mapController.setTransformCenter(HomeScreenConstants.transformCenterPicker);
                    break;
                case AR_MODE:
                    if (this.homeScreenView.isNavigationActive()) {
                        this.logger.v("setTransformCenter:ar:nav");
                        this.mapController.setTransformCenter(HomeScreenConstants.transformCenterSmallBottom);
                        break;
                    }
                    this.logger.v("setTransformCenter:ar:open");
                    this.mapController.setTransformCenter(HomeScreenConstants.transformCenterLargeBottom);
                    break;
            }
        }
    }
    
    private void setViewMap(final MainView.CustomAnimationMode customAnimationMode) {
        switch (customAnimationMode) {
            case EXPAND:
                this.mapView.setX((float)HomeScreenResourceValues.mapViewX);
                break;
            case SHRINK_LEFT:
                this.mapView.setX((float)HomeScreenResourceValues.mapViewShrinkLeftX);
                break;
        }
    }
    
    private void setViewMapIconIndicator(final MainView.CustomAnimationMode customAnimationMode) {
        switch (customAnimationMode) {
            case EXPAND:
                this.mapIconIndicator.setX((float)HomeScreenResourceValues.iconIndicatorX);
                break;
            case SHRINK_LEFT:
                this.mapIconIndicator.setX((float)HomeScreenResourceValues.iconIndicatorShrinkLeft_X);
                break;
        }
    }
    
    private void setViewMapMask(final MainView.CustomAnimationMode customAnimationMode) {
        final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup.MarginLayoutParams)this.mapMask.getLayoutParams();
        switch (customAnimationMode) {
            case EXPAND:
                this.mapMask.setX((float)HomeScreenResourceValues.mapMaskX);
                viewGroup$MarginLayoutParams.width = HomeScreenResourceValues.fullWidth;
                break;
            case SHRINK_LEFT:
                this.mapMask.setX((float)HomeScreenResourceValues.mapMaskShrinkLeftX);
                viewGroup$MarginLayoutParams.width = HomeScreenResourceValues.mapMaskShrinkWidth;
                break;
        }
    }
    
    private void startPreviousRoute(final GeoCoordinate geoCoordinate) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                Label_0654: {
                    while (true) {
                        try {
                            NavigationView.this.logger.v("check if prev route was active");
                            final HereMapUtil.SavedRouteData savedRouteData = HereMapUtil.getSavedRouteData(NavigationView.this.logger);
                            final NavigationRouteRequest navigationRouteRequest = savedRouteData.navigationRouteRequest;
                            if (navigationRouteRequest == null) {
                                NavigationView.this.logger.v("no saved route info");
                                DestinationsManager.getInstance().launchSuggestedDestination();
                            }
                            else {
                                NavigationView.this.logger.v("saved route found");
                                HereRouteManager.printRouteRequest(navigationRouteRequest);
                                if (NavigationView.this.homeScreenView.isNavigationActive()) {
                                    NavigationView.this.logger.v("navigation is already active or route being calculated,ignore the saved route");
                                    DestinationsManager.getInstance().clearSuggestedDestination();
                                    HereMapUtil.removeRouteInfo(NavigationView.this.logger, false);
                                }
                                else {
                                    final GeoCoordinate geoCoordinate = new GeoCoordinate(navigationRouteRequest.destination.latitude, navigationRouteRequest.destination.longitude, 0.0);
                                    final double distanceTo = geoCoordinate.distanceTo(geoCoordinate);
                                    Object o = NavigationView.this.logger;
                                    ((Logger)o).v("distance remaining to destination is [" + distanceTo + "] current[" + geoCoordinate + "] target [" + geoCoordinate + "]");
                                    boolean contains;
                                    final boolean b = contains = navigationRouteRequest.routeAttributes.contains(NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_GAS);
                                    double distanceTo2 = distanceTo;
                                    o = navigationRouteRequest;
                                    Object savedRouteData2 = savedRouteData;
                                    if (b) {
                                        contains = b;
                                        distanceTo2 = distanceTo;
                                        o = navigationRouteRequest;
                                        savedRouteData2 = savedRouteData;
                                        if (distanceTo >= 0.0) {
                                            contains = b;
                                            distanceTo2 = distanceTo;
                                            o = navigationRouteRequest;
                                            savedRouteData2 = savedRouteData;
                                            if (distanceTo <= 500.0) {
                                                NavigationView.this.logger.v("gas route and distance to destination is <= 500.0, don't navigate");
                                                NavigationView.this.logger.v("remove gas route and look for non gas route");
                                                HereMapUtil.removeRouteInfo(NavigationView.this.logger, false);
                                                savedRouteData2 = HereMapUtil.getSavedRouteData(NavigationView.this.logger);
                                                o = ((HereMapUtil.SavedRouteData)savedRouteData2).navigationRouteRequest;
                                                if (o == null) {
                                                    NavigationView.this.logger.v("no non gas route");
                                                    DestinationsManager.getInstance().launchSuggestedDestination();
                                                    HereMapUtil.removeRouteInfo(NavigationView.this.logger, false);
                                                    return;
                                                }
                                                contains = false;
                                                distanceTo2 = geoCoordinate.distanceTo(new GeoCoordinate(((NavigationRouteRequest)o).destination.latitude, ((NavigationRouteRequest)o).destination.longitude, 0.0));
                                            }
                                        }
                                    }
                                    if (!contains && distanceTo2 >= 0.0 && distanceTo2 <= 500.0) {
                                        NavigationView.this.logger.v("distance to destination is <= 500, don't navigate");
                                        DestinationsManager.getInstance().launchSuggestedDestination();
                                        HereMapUtil.removeRouteInfo(NavigationView.this.logger, false);
                                    }
                                    else {
                                        DestinationsManager.getInstance().clearSuggestedDestination();
                                        o = new NavigationRouteRequest.Builder((NavigationRouteRequest)o).originDisplay(true).cancelCurrent(false).build();
                                        NavigationView.this.logger.v("sleep before starting navigation");
                                        GenericUtil.sleep(2000);
                                        NavigationView.this.logger.v("slept");
                                        if (!((HereMapUtil.SavedRouteData)savedRouteData2).isGasRoute || !RemoteDeviceManager.getInstance().getFeatureUtil().isFeatureEnabled(FeatureUtil.Feature.FUEL_ROUTING)) {
                                            break Label_0654;
                                        }
                                        NavigationView.this.logger.v("start saveroute navigation (gas route)");
                                        NavigationView.this.bus.post(o);
                                        NavigationView.this.bus.post(new RemoteEvent((Message)o));
                                    }
                                }
                            }
                        }
                        catch (Throwable t) {
                            NavigationView.this.logger.e(t);
                            HereMapUtil.removeRouteInfo(NavigationView.this.logger, false);
                            return;
                            NavigationView.this.logger.v("start saveroute navigation");
                            continue;
                        }
                        finally {
                            HereMapUtil.removeRouteInfo(NavigationView.this.logger, false);
                        }
                        break;
                    }
                }
            }
        }, 3);
    }
    
    private void switchToOverviewMode(final GeoCoordinate geoCoordinate, final Route route, final MapRoute mapRoute, final Runnable runnable, final boolean b, final boolean b2, final double n) {
        this.logger.v("switchToOverviewMode:" + b2);
        this.mapController.setState(HereMapController.State.OVERVIEW);
        this.cleanupFluctuator();
        this.mapIconIndicator.setVisibility(INVISIBLE);
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                final boolean trafficEnabled = DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled();
                if (route == null) {
                    NavigationView.this.logger.v("switchToOverviewMode:no route traffic=" + trafficEnabled);
                    NavigationView.this.setTransformCenter(HereMapController.State.OVERVIEW);
                    NavigationView.this.mapController.addTransformListener(new Map.OnTransformListener() {
                        @Override
                        public void onMapTransformEnd(final MapState mapState) {
                            NavigationView.this.logger.v("switchToOverviewMode:no route  tranform end");
                            NavigationView.this.mapController.removeTransformListener(this);
                            NavigationView.this.handler.post(runnable);
                        }
                        
                        @Override
                        public void onMapTransformStart() {
                        }
                    });
                    final HereMapController access$000 = NavigationView.this.mapController;
                    final HereMapController.State overview = HereMapController.State.OVERVIEW;
                    final GeoCoordinate val$geoCoordinate = geoCoordinate;
                    Map.Animation animation;
                    if (b2) {
                        animation = Map.Animation.BOW;
                    }
                    else {
                        animation = Map.Animation.NONE;
                    }
                    access$000.setCenterForState(overview, val$geoCoordinate, animation, n, 0.0f, 0.0f);
                    if (NavigationView.this.startMarker != null) {
                        NavigationView.this.startMarker.setCoordinate(geoCoordinate);
                        NavigationView.this.mapController.addMapObject(NavigationView.this.startMarker);
                    }
                }
                else {
                    NavigationView.this.logger.v("switchToOverviewMode:route traffic=" + trafficEnabled);
                    if (NavigationView.this.startMarker != null) {
                        NavigationView.this.startMarker.setCoordinate(geoCoordinate);
                        NavigationView.this.mapController.addMapObject(NavigationView.this.startMarker);
                    }
                    NavigationView.this.logger.v("switchToOverviewMode:map:executeSynchronized");
                    NavigationView.this.mapController.setTilt(0.0f);
                    if (runnable != null) {
                        NavigationView.this.mapController.addTransformListener(new Map.OnTransformListener() {
                            @Override
                            public void onMapTransformEnd(final MapState mapState) {
                                NavigationView.this.logger.v("switchToOverviewMode:map tranform end");
                                NavigationView.this.mapController.removeTransformListener(this);
                                NavigationView.this.handler.post(runnable);
                            }
                            
                            @Override
                            public void onMapTransformStart() {
                            }
                        });
                    }
                    NavigationView.this.setTransformCenter(HereMapController.State.OVERVIEW);
                    final HereMapController access$2 = NavigationView.this.mapController;
                    final GeoBoundingBox boundingBox = route.getBoundingBox();
                    final ViewRect routeOverviewRect = HomeScreenConstants.routeOverviewRect;
                    Map.Animation animation2;
                    if (b2) {
                        animation2 = Map.Animation.BOW;
                    }
                    else {
                        animation2 = Map.Animation.NONE;
                    }
                    access$2.zoomTo(boundingBox, routeOverviewRect, animation2, 0.0f);
                    NavigationView.this.logger.v("switchToOverviewMode took [" + (SystemClock.elapsedRealtime() - elapsedRealtime) + "]");
                }
            }
        }, 17);
    }
    
    private void updateMapIndicator() {
        final boolean debugRawGpsPosEnabled = GpsUtils.isDebugRawGpsPosEnabled();
        final ImageView mapIconIndicator = this.mapIconIndicator;
        float alpha;
        if (debugRawGpsPosEnabled) {
            alpha = 0.4f;
        }
        else {
            alpha = 1.0f;
        }
        mapIconIndicator.setAlpha(alpha);
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                final PositionIndicator positionIndicator = NavigationView.this.mapController.getPositionIndicator();
                try {
                    if (debugRawGpsPosEnabled) {
                        final Image marker = new Image();
                        marker.setImageResource(R.drawable.here_pos_indicator);
                        positionIndicator.setMarker(marker);
                    }
                    positionIndicator.setVisible(debugRawGpsPosEnabled);
                }
                catch (Throwable t) {
                    NavigationView.this.logger.e(t);
                }
            }
        }, 2);
        if (debugRawGpsPosEnabled) {
            this.hereLocationFixManager.addMarkers(this.mapController);
        }
        else {
            this.hereLocationFixManager.removeMarkers(this.mapController);
        }
    }
    
    public void addMapOverviewRoutes(final MapRoute mapRoute) {
        this.logger.v("addMapOverviewRoutes:" + mapRoute);
        this.mapController.addMapObject(mapRoute);
        this.overviewMapRouteObjects.add(mapRoute);
    }
    
    public void adjustMaplayoutHeight(final int height) {
        int n;
        if (height == HomeScreenResourceValues.activeMapHeight) {
            n = HomeScreenResourceValues.iconIndicatorActiveY;
        }
        else {
            n = HomeScreenResourceValues.iconIndicatorOpenY;
        }
        this.mapIconIndicator.setY(n - HomeScreenResourceValues.transformCenterIconHeight * 0.6f);
        ((ViewGroup.MarginLayoutParams)this.mapMask.getLayoutParams()).height = height;
        this.layoutMap();
        this.invalidate();
        this.requestLayout();
    }
    
    public void animateBackfromOverview(final Runnable runnable) {
        this.logger.v("animateBackfromOverview");
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                NavigationView.this.setMapToArMode(true, false, false, null, -1.0, -1.0, false);
                NavigationView.this.logger.v("animateBackfromOverview: add transform");
                NavigationView.this.mapController.addTransformListener(NavigationView.this.animateBackfromOverviewMap(runnable));
            }
        }, 17);
    }
    
    public void animateToOverview(final GeoCoordinate geoCoordinate, final Route route, final MapRoute mapRoute, final Runnable runnable, final boolean b) {
        this.switchToOverviewMode(geoCoordinate, route, mapRoute, runnable, b, true, -1.0);
    }
    
    public void changeMarkerSelection(final int n, final int n2) {
        final HereMapController.State state = this.mapController.getState();
        if (state != HereMapController.State.ROUTE_PICKER) {
            this.logger.i("changeMarkerSelection: incorrect state:" + state);
        }
        else {
            final int size = this.mapMarkerList.size();
            if (size > 0) {
                this.logger.i("changeMarkerSelection {" + n + "," + n2 + "}");
                if (n2 >= 0 && n2 < size) {
                    final MapMarker mapMarker = this.mapMarkerList.get(n2);
                    if (mapMarker != null) {
                        final MapMarker mapMarker2 = new MapMarker(mapMarker.getCoordinate(), this.unselectedDestinationImage);
                        this.mapMarkerList.set(n2, mapMarker2);
                        this.mapController.removeMapObject(mapMarker);
                        this.mapController.addMapObject(mapMarker2);
                    }
                }
                if (n >= 0 && n < size) {
                    final MapMarker mapMarker3 = this.mapMarkerList.get(n);
                    if (mapMarker3 != null) {
                        final MapMarker mapMarker4 = new MapMarker(mapMarker3.getCoordinate(), this.selectedDestinationImage);
                        this.mapMarkerList.set(n, mapMarker4);
                        this.mapController.removeMapObject(mapMarker3);
                        this.mapController.addMapObject(mapMarker4);
                    }
                }
            }
        }
    }
    
    public void cleanupFluctuator() {
        this.logger.v("cleanupFluctuator");
        this.mapController.removeTransformListener(this.fluctuatorPosListener);
        this.startDestinationFluctuatorView.stop();
        this.startDestinationFluctuatorView.setVisibility(GONE);
    }
    
    public void cleanupMapOverview() {
        if (Looper.getMainLooper() == Looper.myLooper()) {
            this.cleanupMapOverviewRunnable.run();
        }
        else {
            this.handler.post(this.cleanupMapOverviewRunnable);
        }
    }
    
    public void cleanupMapOverviewRoutes() {
        this.logger.v("cleanupMapOverviewRoutes");
        if (this.overviewMapRouteObjects.size() > 0) {
            final ArrayList<Object> list = new ArrayList<Object>(this.overviewMapRouteObjects);
            final Iterator<MapObject> iterator = list.iterator();
            while (iterator.hasNext()) {
                this.logger.v("cleanupMapOverviewRoutes:" + iterator.next());
            }
            this.mapController.removeMapObjects((List<MapObject>)list);
            this.overviewMapRouteObjects.clear();
        }
    }
    
    public void clearState() {
    }
    
    public void enableRendering() {
        while (true) {
            Label_0049: {
                synchronized (this) {
                    if (this.isRenderingEnabled) {
                        this.logger.v("enableRendering:Maps Rendering already enabled");
                    }
                    else {
                        if (this.initComplete) {
                            break Label_0049;
                        }
                        this.logger.v("enableRendering:Maps Rendering init-engine not complete yet");
                    }
                    return;
                }
            }
            this.hereMapsManager.initNavigation();
            this.mapController.setState(HereMapController.State.AR_MODE);
            this.hereMapsManager.installPositionListener();
            this.setTransformCenter(HereMapController.State.AR_MODE);
            this.mapView.setMap(this.mapController.getMap());
            this.logger.v("enableRendering:Maps Rendering enabled");
            this.isRenderingEnabled = true;
        }
    }
    
    public void getCustomAnimator(final MainView.CustomAnimationMode customAnimationMode, final AnimatorSet.Builder animatorSet$Builder) {
        this.logger.v("getCustomAnimator: " + customAnimationMode);
        switch (customAnimationMode) {
            case SHRINK_LEFT:
            case SHRINK_MODE: {
                int n;
                int n2;
                int mapMaskShrinkLeftX;
                int n3;
                if (customAnimationMode == MainView.CustomAnimationMode.SHRINK_LEFT) {
                    n = HomeScreenResourceValues.iconIndicatorShrinkLeft_X;
                    n2 = HomeScreenResourceValues.mapViewShrinkLeftX;
                    mapMaskShrinkLeftX = HomeScreenResourceValues.mapMaskShrinkLeftX;
                    n3 = HomeScreenResourceValues.mapMaskShrinkWidth;
                }
                else {
                    n = HomeScreenResourceValues.iconIndicatorShrinkModeX;
                    n2 = HomeScreenResourceValues.mapViewShrinkModeX;
                    mapMaskShrinkLeftX = 0;
                    n3 = HomeScreenResourceValues.mapMaskShrinkWidth;
                }
                animatorSet$Builder.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this.mapIconIndicator, n));
                animatorSet$Builder.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this.mapView, n2));
                final ObjectAnimator xPositionAnimator = HomeScreenUtils.getXPositionAnimator((View)this.mapMask, mapMaskShrinkLeftX);
                final ValueAnimator widthAnimator = HomeScreenUtils.getWidthAnimator((View)this.mapMask, n3);
                animatorSet$Builder.with((Animator)xPositionAnimator);
                animatorSet$Builder.with((Animator)widthAnimator);
                if (customAnimationMode != MainView.CustomAnimationMode.SHRINK_LEFT) {
                    break;
                }
                if (this.isAcquiringLocation()) {
                    this.noLocationView.getCustomAnimator(MainView.CustomAnimationMode.SHRINK_LEFT, animatorSet$Builder);
                    break;
                }
                this.noLocationView.setView(MainView.CustomAnimationMode.SHRINK_LEFT);
                break;
            }
            case EXPAND: {
                animatorSet$Builder.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this.mapIconIndicator, HomeScreenResourceValues.iconIndicatorX));
                animatorSet$Builder.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this.mapView, HomeScreenResourceValues.mapViewX));
                final ObjectAnimator xPositionAnimator2 = HomeScreenUtils.getXPositionAnimator((View)this.mapMask, HomeScreenResourceValues.mapMaskX);
                final ValueAnimator widthAnimator2 = HomeScreenUtils.getWidthAnimator((View)this.mapMask, this.mapMask.getMeasuredWidth() + 1, HomeScreenResourceValues.fullWidth + 1);
                animatorSet$Builder.with((Animator)xPositionAnimator2);
                animatorSet$Builder.with((Animator)widthAnimator2);
                if (this.isAcquiringLocation()) {
                    this.noLocationView.getCustomAnimator(MainView.CustomAnimationMode.EXPAND, animatorSet$Builder);
                    break;
                }
                this.noLocationView.setView(MainView.CustomAnimationMode.EXPAND);
                break;
            }
        }
    }
    
    public Map.OnTransformListener getFluctuatorTransformListener() {
        return this.fluctuatorPosListener;
    }
    
    public HereMapController getMapController() {
        return this.mapController;
    }
    
    public ImageView getMapIconIndicatorView() {
        return this.mapIconIndicator;
    }
    
    public int getMapViewX() {
        return (int)this.mapView.getX();
    }
    
    public void getTopAnimator(final AnimatorSet.Builder animatorSet$Builder, final boolean b) {
    }
    
    public void init(final HomeScreenView homeScreenView) {
        this.homeScreenView = homeScreenView;
        this.bus.register(this);
    }
    
    public boolean isAcquiringLocation() {
        return this.noLocationView.isAcquiringLocation();
    }
    
    public boolean isOverviewMapMode() {
        boolean b = false;
        if (this.mapController != null) {
            switch (this.mapController.getState()) {
                case OVERVIEW:
                case ROUTE_PICKER:
                    b = true;
                    break;
            }
        }
        return b;
    }
    
    public void layoutMap() {
        this.setTransformCenter();
    }
    
    public InputManager.IInputHandler nextHandler() {
        return null;
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.hereMapsManager.isInitializing()) {
            this.logger.v("engine initializing, wait");
        }
        else {
            this.handleEngineInit();
        }
    }
    
    public void onClick() {
    }
    
    @Subscribe
    public void onEngineInitialized(final MapEvents.MapEngineInitialize mapEngineInitialize) {
        this.handleEngineInit();
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        this.mapMask.setX(0.0f);
        this.bus = RemoteDeviceManager.getInstance().getBus();
        this.driverProfileManager = DriverProfileHelper.getInstance().getDriverProfileManager();
        this.initViews();
    }
    
    @Subscribe
    public void onGeoPositionChange(final GeoPosition geoPosition) {
        if (this.startMarker != null) {
            switch (this.mapController.getState()) {
                case OVERVIEW:
                    this.startMarker.setCoordinate(geoPosition.getCoordinate());
                    break;
                case ROUTE_PICKER:
                    this.startMarker.setCoordinate(geoPosition.getCoordinate());
                    break;
            }
        }
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    @Subscribe
    public void onHudNetworkInit(final NetworkStateManager.HudNetworkInitialized hudNetworkInitialized) {
        this.logger.v("checkPrevRoute:hud n/w is initialized: " + this.networkCheckRequired);
        if (this.networkCheckRequired) {
            this.networkCheckRequired = false;
            if (this.hereLocationFixManager == null) {
                this.hereLocationFixManager = this.hereMapsManager.getLocationFixManager();
            }
            this.startPreviousRoute(this.hereLocationFixManager.getLastGeoCoordinate());
            this.logger.v("send map engine ready event-2");
            this.bus.post(new RemoteEvent(new NavigationSessionStatusEvent(NavigationSessionState.NAV_SESSION_ENGINE_READY, null, null, null, null)));
        }
    }
    
    public boolean onKey(final InputManager.CustomKeyEvent customKeyEvent) {
        boolean b = true;
        switch (customKeyEvent) {
            default:
                b = false;
                break;
            case RIGHT:
                this.bus.post(NavigationView.ZOOM_IN);
                break;
            case LEFT:
                this.bus.post(NavigationView.ZOOM_OUT);
                break;
        }
        return b;
    }
    
    @Subscribe
    public void onLocationFixChangeEvent(final MapEvents.LocationFix locationFix) {
        this.logger.v("location fix event locationAvailable[" + locationFix.locationAvailable + "] phone[" + locationFix.usingPhoneLocation + "] gps [" + locationFix.usingLocalGpsLocation + "]");
        if (this.isAcquiringLocation() && this.initComplete && this.setMapCenter()) {
            this.noLocationView.hideLocationUI();
        }
    }
    
    public void onPause() {
        if (!this.paused) {
            this.logger.v("::onPause");
            this.paused = true;
            this.hereMapsManager.stopMapRendering();
        }
    }
    
    public void onResume() {
        if (this.paused) {
            this.logger.v("::onResume");
            this.paused = false;
            this.hereMapsManager.startMapRendering();
        }
    }
    
    @Subscribe
    public void onSettingsChange(final SettingsManager.SettingsChanged settingsChanged) {
        if (settingsChanged.setting == GpsUtils.SHOW_RAW_GPS) {
            this.updateMapIndicator();
        }
    }
    
    public void onTrackHand(final float n) {
    }
    
    @Subscribe
    public void onWakeup(final Wakeup wakeup) {
        this.handleEngineInit();
    }
    
    public void resetMapOverview() {
        if (Looper.getMainLooper() == Looper.myLooper()) {
            this.resetMapOverviewRunnable.run();
        }
        else {
            this.handler.post(this.resetMapOverviewRunnable);
        }
    }
    
    public void resetTopViewsAnimator() {
    }
    
    public void setMapMaskVisibility(final int visibility) {
        this.mapMask.setVisibility(visibility);
    }
    
    public void setMapToArMode(final boolean b, final boolean b2, final boolean b3, final GeoPosition geoPosition, final double n, final double n2, final boolean b4) {
        if (GenericUtil.isMainThread()) {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    NavigationView.this.setMapToArModeInternal(b, b2, b3, geoPosition, n, (float)n2, b4);
                }
            }, 17);
        }
        else {
            this.setMapToArModeInternal(b, b2, b3, geoPosition, n, (float)n2, b4);
        }
    }
    
    public void setMapViewX(final int n) {
        this.mapView.setX((float)n);
    }
    
    public void setView(final MainView.CustomAnimationMode customAnimationMode) {
        this.logger.v("setview: " + customAnimationMode);
        this.setViewMapIconIndicator(customAnimationMode);
        this.setViewMap(customAnimationMode);
        this.setViewMapMask(customAnimationMode);
        this.noLocationView.setView(customAnimationMode);
    }
    
    public void showOverviewMap(final Route route, final MapRoute mapRoute, final GeoCoordinate geoCoordinate, final boolean b) {
        this.logger.v("showOverviewMap");
        this.switchToOverviewMode(geoCoordinate, route, mapRoute, null, b, false, 12.0);
        this.mapIconIndicator.setVisibility(INVISIBLE);
    }
    
    public void showRouteMap(final GeoPosition geoPosition, final double n, final float n2) {
        this.logger.v("showRouteMap");
        this.setMapToArMode(false, true, false, geoPosition, n, n2, true);
        this.mapIconIndicator.setVisibility(View.VISIBLE);
    }
    
    public void showStartMarker() {
        this.logger.v("showStartMarker");
        if (this.startMarker != null) {
            this.mapController.addMapObject(this.startMarker);
        }
    }
    
    public void startFluctuator() {
        this.mapController.addTransformListener(this.getFluctuatorTransformListener());
    }
    
    public void switchBackfromRouteSearchMode(final boolean b) {
        final HereMapController.State state = this.mapController.getState();
        if (state != HereMapController.State.ROUTE_PICKER) {
            this.logger.v("switchBackfromRouteSearchMode: invalid state:" + state);
        }
        else {
            this.logger.v("map-route- switchBackfromRouteSearchMode:" + b);
            if (HereMapsManager.getInstance().isInitialized()) {
                HereNavigationManager.getInstance().getSafetySpotListener().setSafetySpotsEnabledOnMap(true);
            }
            final int size = this.mapMarkerList.size();
            if (size > 0) {
                this.logger.v("switchBackfromRouteSearchMode remove map markers:" + size);
                this.mapController.removeMapObjects(this.mapMarkerList);
                this.mapMarkerList = new ArrayList<MapObject>();
            }
            this.setMapMaskVisibility(0);
            this.setMapViewX(0);
            this.homeScreenView.navigationViewsContainer.setVisibility(View.VISIBLE);
            if (this.homeScreenView.isNavigationActive()) {
                if (this.homeScreenView.getDisplayMode() == HomeScreen.DisplayMode.MAP) {
                    this.homeScreenView.activeEtaContainer.setVisibility(View.VISIBLE);
                }
            }
            else {
                this.homeScreenView.openMapRoadInfoContainer.setVisibility(View.VISIBLE);
                if (this.homeScreenView.getDisplayMode() == HomeScreen.DisplayMode.MAP) {
                    this.homeScreenView.timeContainer.setVisibility(View.VISIBLE);
                }
            }
            this.mapController.setState(HereMapController.State.TRANSITION);
            if (!DriverProfileHelper.getInstance().getDriverProfileManager().isManualZoom()) {
                this.setMapToArMode(false, true, true, null, -1.0, -1.0, false);
                this.mapIconIndicator.setVisibility(View.VISIBLE);
            }
            if (this.startMarker != null) {
                this.mapController.removeMapObject(this.startMarker);
            }
            if (this.endMarker != null) {
                this.mapController.removeMapObject(this.endMarker);
            }
            this.logger.v("map-route- switchBackfromRouteSearchMode cleanup routes");
            this.cleanupMapOverviewRoutes();
            final HereNavigationManager instance = HereNavigationManager.getInstance();
            final Route currentRoute = instance.getCurrentRoute();
            if (currentRoute != null && instance.isNavigationModeOn() && !instance.hasArrived()) {
                if (b) {
                    final MapRoute mapRoute = new MapRoute(currentRoute);
                    mapRoute.setTrafficEnabled(true);
                    instance.addCurrentRoute(mapRoute);
                    this.logger.v("map-route- switchBackfromRouteSearchMode: nav route added");
                }
                else {
                    this.logger.v("map-route- switchBackfromRouteSearchMode: nav route not added back");
                }
            }
            final MapMarker destinationMarker = HereNavigationManager.getInstance().getDestinationMarker();
            if (destinationMarker != null) {
                this.logger.v("switchBackfromRouteSearchMode: destination marker added");
                this.mapController.addMapObject(destinationMarker);
            }
            this.cleanupFluctuator();
            HereMapCameraManager.getInstance().setZoom();
        }
    }
    
    public void switchScreen() {
        final BaseScreen currentScreen = this.uiStateManager.getCurrentScreen();
        if (currentScreen != null) {
            final HomeScreenView homescreenView = this.uiStateManager.getHomescreenView();
            if (homescreenView != null) {
                Main.saveHomeScreenPreference(homescreenView.globalPreferences, HomeScreen.DisplayMode.MAP.ordinal());
                homescreenView.setDisplayMode(HomeScreen.DisplayMode.MAP);
            }
            if (currentScreen.getScreen() != Screen.SCREEN_HOME) {
                this.logger.v("current screen is not map:" + currentScreen.getScreen());
                final Main rootScreen = this.uiStateManager.getRootScreen();
                if (rootScreen.isNotificationViewShowing() || rootScreen.isNotificationExpanding()) {
                    this.logger.v("switching, but collpasing notif first");
                    this.homeScreenView.setShowCollapsedNotification(true);
                }
                else {
                    this.logger.v("switched to map");
                }
                this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_HYBRID_MAP).build());
            }
            else {
                this.logger.v("current screen is map no switch reqd");
            }
        }
        else {
            this.logger.w("current screen is null");
        }
    }
    
    public void switchToRouteSearchMode(final GeoCoordinate p0, final GeoCoordinate p1, final GeoBoundingBox p2, final Route p3, final boolean p4, final List<GeoCoordinate> p5, final int p6) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.logger:Lcom/navdy/service/library/log/Logger;
        //     4: new             Ljava/lang/StringBuilder;
        //     7: dup            
        //     8: invokespecial   java/lang/StringBuilder.<init>:()V
        //    11: ldc_w           "switchToRouteSearchMode: start["
        //    14: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    17: aload_1        
        //    18: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //    21: ldc_w           "] end ["
        //    24: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    27: aload_2        
        //    28: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //    31: ldc_w           "] x="
        //    34: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    37: aload_0        
        //    38: invokevirtual   com/navdy/hud/app/ui/component/homescreen/NavigationView.getMapViewX:()I
        //    41: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    44: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    47: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //    50: invokestatic    com/navdy/hud/app/maps/here/HereMapsManager.getInstance:()Lcom/navdy/hud/app/maps/here/HereMapsManager;
        //    53: invokevirtual   com/navdy/hud/app/maps/here/HereMapsManager.isInitialized:()Z
        //    56: ifeq            69
        //    59: invokestatic    com/navdy/hud/app/maps/here/HereNavigationManager.getInstance:()Lcom/navdy/hud/app/maps/here/HereNavigationManager;
        //    62: invokevirtual   com/navdy/hud/app/maps/here/HereNavigationManager.getSafetySpotListener:()Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;
        //    65: iconst_0       
        //    66: invokevirtual   com/navdy/hud/app/maps/here/HereSafetySpotListener.setSafetySpotsEnabledOnMap:(Z)V
        //    69: aload_0        
        //    70: iconst_4       
        //    71: invokevirtual   com/navdy/hud/app/ui/component/homescreen/NavigationView.setMapMaskVisibility:(I)V
        //    74: aload_0        
        //    75: getstatic       com/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues.transformCenterMoveX:I
        //    78: ineg           
        //    79: invokevirtual   com/navdy/hud/app/ui/component/homescreen/NavigationView.setMapViewX:(I)V
        //    82: aload_0        
        //    83: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
        //    86: getfield        com/navdy/hud/app/ui/component/homescreen/HomeScreenView.navigationViewsContainer:Landroid/widget/RelativeLayout;
        //    89: iconst_4       
        //    90: invokevirtual   android/widget/RelativeLayout.setVisibility:(I)V
        //    93: aload_0        
        //    94: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
        //    97: getfield        com/navdy/hud/app/ui/component/homescreen/HomeScreenView.openMapRoadInfoContainer:Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;
        //   100: iconst_4       
        //   101: invokevirtual   com/navdy/hud/app/ui/component/homescreen/OpenRoadView.setVisibility:(I)V
        //   104: aload_0        
        //   105: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
        //   108: getfield        com/navdy/hud/app/ui/component/homescreen/HomeScreenView.timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;
        //   111: invokevirtual   com/navdy/hud/app/ui/component/homescreen/TimeView.getVisibility:()I
        //   114: ifne            581
        //   117: aload_0        
        //   118: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
        //   121: getfield        com/navdy/hud/app/ui/component/homescreen/HomeScreenView.timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;
        //   124: iconst_4       
        //   125: invokevirtual   com/navdy/hud/app/ui/component/homescreen/TimeView.setVisibility:(I)V
        //   128: aload_0        
        //   129: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
        //   132: getstatic       com/navdy/hud/app/maps/here/HereMapController$State.ROUTE_PICKER:Lcom/navdy/hud/app/maps/here/HereMapController$State;
        //   135: invokevirtual   com/navdy/hud/app/maps/here/HereMapController.setState:(Lcom/navdy/hud/app/maps/here/HereMapController$State;)V
        //   138: aload_0        
        //   139: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.mapIconIndicator:Landroid/widget/ImageView;
        //   142: iconst_4       
        //   143: invokevirtual   android/widget/ImageView.setVisibility:(I)V
        //   146: aload_0        
        //   147: invokevirtual   com/navdy/hud/app/ui/component/homescreen/NavigationView.cleanupMapOverview:()V
        //   150: aload_0        
        //   151: getstatic       com/navdy/hud/app/maps/here/HereMapController$State.ROUTE_PICKER:Lcom/navdy/hud/app/maps/here/HereMapController$State;
        //   154: invokespecial   com/navdy/hud/app/ui/component/homescreen/NavigationView.setTransformCenter:(Lcom/navdy/hud/app/maps/here/HereMapController$State;)V
        //   157: aload_1        
        //   158: astore          8
        //   160: aload           8
        //   162: astore          9
        //   164: aload           8
        //   166: ifnonnull       178
        //   169: aload_0        
        //   170: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
        //   173: invokevirtual   com/navdy/hud/app/maps/here/HereLocationFixManager.getLastGeoCoordinate:()Lcom/here/android/mpa/common/GeoCoordinate;
        //   176: astore          9
        //   178: aload_0        
        //   179: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
        //   182: getstatic       com/navdy/hud/app/maps/here/HereMapController$State.ROUTE_PICKER:Lcom/navdy/hud/app/maps/here/HereMapController$State;
        //   185: aload           9
        //   187: getstatic       com/here/android/mpa/mapping/Map$Animation.NONE:Lcom/here/android/mpa/mapping/Map$Animation;
        //   190: ldc2_w          15.5
        //   193: fconst_0       
        //   194: fconst_0       
        //   195: invokevirtual   com/navdy/hud/app/maps/here/HereMapController.setCenterForState:(Lcom/navdy/hud/app/maps/here/HereMapController$State;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V
        //   198: invokestatic    com/navdy/hud/app/maps/here/HereNavigationManager.getInstance:()Lcom/navdy/hud/app/maps/here/HereNavigationManager;
        //   201: astore          8
        //   203: aload           8
        //   205: invokevirtual   com/navdy/hud/app/maps/here/HereNavigationManager.removeCurrentRoute:()Z
        //   208: ifeq            609
        //   211: aload_0        
        //   212: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.logger:Lcom/navdy/service/library/log/Logger;
        //   215: ldc_w           "map-route- switchToRouteSearchMode: nav route removed"
        //   218: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   221: aload           8
        //   223: invokevirtual   com/navdy/hud/app/maps/here/HereNavigationManager.getDestinationMarker:()Lcom/here/android/mpa/mapping/MapMarker;
        //   226: astore          8
        //   228: aload           8
        //   230: ifnull          252
        //   233: aload_0        
        //   234: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
        //   237: aload           8
        //   239: invokevirtual   com/navdy/hud/app/maps/here/HereMapController.removeMapObject:(Lcom/here/android/mpa/mapping/MapObject;)V
        //   242: aload_0        
        //   243: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.logger:Lcom/navdy/service/library/log/Logger;
        //   246: ldc_w           "switchToRouteSearchMode: dest removed"
        //   249: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   252: aload_0        
        //   253: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.startMarker:Lcom/here/android/mpa/mapping/MapMarker;
        //   256: ifnonnull       303
        //   259: new             Lcom/here/android/mpa/common/Image;
        //   262: astore          8
        //   264: aload           8
        //   266: invokespecial   com/here/android/mpa/common/Image.<init>:()V
        //   269: aload           8
        //   271: ldc_w           R.drawable.icon_driver_position
        //   274: invokevirtual   com/here/android/mpa/common/Image.setImageResource:(I)V
        //   277: new             Lcom/here/android/mpa/mapping/MapMarker;
        //   280: astore          9
        //   282: aload           9
        //   284: invokespecial   com/here/android/mpa/mapping/MapMarker.<init>:()V
        //   287: aload_0        
        //   288: aload           9
        //   290: putfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.startMarker:Lcom/here/android/mpa/mapping/MapMarker;
        //   293: aload_0        
        //   294: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.startMarker:Lcom/here/android/mpa/mapping/MapMarker;
        //   297: aload           8
        //   299: invokevirtual   com/here/android/mpa/mapping/MapMarker.setIcon:(Lcom/here/android/mpa/common/Image;)Lcom/here/android/mpa/mapping/MapMarker;
        //   302: pop            
        //   303: aload_1        
        //   304: ifnull          327
        //   307: aload_0        
        //   308: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.startMarker:Lcom/here/android/mpa/mapping/MapMarker;
        //   311: aload_1        
        //   312: invokevirtual   com/here/android/mpa/mapping/MapMarker.setCoordinate:(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/mapping/MapMarker;
        //   315: pop            
        //   316: aload_0        
        //   317: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
        //   320: aload_0        
        //   321: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.startMarker:Lcom/here/android/mpa/mapping/MapMarker;
        //   324: invokevirtual   com/navdy/hud/app/maps/here/HereMapController.addMapObject:(Lcom/here/android/mpa/mapping/MapObject;)V
        //   327: aload_0        
        //   328: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.endMarker:Lcom/here/android/mpa/mapping/MapMarker;
        //   331: ifnonnull       375
        //   334: new             Lcom/here/android/mpa/common/Image;
        //   337: astore          8
        //   339: aload           8
        //   341: invokespecial   com/here/android/mpa/common/Image.<init>:()V
        //   344: aload           8
        //   346: ldc_w           R.drawable.icon_pin_dot_destination
        //   349: invokevirtual   com/here/android/mpa/common/Image.setImageResource:(I)V
        //   352: new             Lcom/here/android/mpa/mapping/MapMarker;
        //   355: astore_1       
        //   356: aload_1        
        //   357: invokespecial   com/here/android/mpa/mapping/MapMarker.<init>:()V
        //   360: aload_0        
        //   361: aload_1        
        //   362: putfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.endMarker:Lcom/here/android/mpa/mapping/MapMarker;
        //   365: aload_0        
        //   366: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.endMarker:Lcom/here/android/mpa/mapping/MapMarker;
        //   369: aload           8
        //   371: invokevirtual   com/here/android/mpa/mapping/MapMarker.setIcon:(Lcom/here/android/mpa/common/Image;)Lcom/here/android/mpa/mapping/MapMarker;
        //   374: pop            
        //   375: aload_2        
        //   376: ifnull          399
        //   379: aload_0        
        //   380: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.endMarker:Lcom/here/android/mpa/mapping/MapMarker;
        //   383: aload_2        
        //   384: invokevirtual   com/here/android/mpa/mapping/MapMarker.setCoordinate:(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/mapping/MapMarker;
        //   387: pop            
        //   388: aload_0        
        //   389: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
        //   392: aload_0        
        //   393: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.endMarker:Lcom/here/android/mpa/mapping/MapMarker;
        //   396: invokevirtual   com/navdy/hud/app/maps/here/HereMapController.addMapObject:(Lcom/here/android/mpa/mapping/MapObject;)V
        //   399: aload_0        
        //   400: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.mapMarkerList:Ljava/util/List;
        //   403: invokeinterface java/util/List.size:()I
        //   408: ifle            433
        //   411: aload_0        
        //   412: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
        //   415: aload_0        
        //   416: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.mapMarkerList:Ljava/util/List;
        //   419: invokevirtual   com/navdy/hud/app/maps/here/HereMapController.removeMapObjects:(Ljava/util/List;)V
        //   422: aload_0        
        //   423: new             Ljava/util/ArrayList;
        //   426: dup            
        //   427: invokespecial   java/util/ArrayList.<init>:()V
        //   430: putfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.mapMarkerList:Ljava/util/List;
        //   433: aload           6
        //   435: ifnull          712
        //   438: aload           6
        //   440: invokeinterface java/util/List.size:()I
        //   445: istore          10
        //   447: new             Lcom/here/android/mpa/common/Image;
        //   450: astore_1       
        //   451: aload_1        
        //   452: invokespecial   com/here/android/mpa/common/Image.<init>:()V
        //   455: aload_0        
        //   456: aload_1        
        //   457: putfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.selectedDestinationImage:Lcom/here/android/mpa/common/Image;
        //   460: aload_0        
        //   461: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.selectedDestinationImage:Lcom/here/android/mpa/common/Image;
        //   464: astore_1       
        //   465: iload           7
        //   467: istore          11
        //   469: iload           7
        //   471: iconst_m1      
        //   472: if_icmpne       480
        //   475: ldc_w           R.drawable.icon_pin_dot_destination_blue
        //   478: istore          11
        //   480: aload_1        
        //   481: iload           11
        //   483: invokevirtual   com/here/android/mpa/common/Image.setImageResource:(I)V
        //   486: aload_0        
        //   487: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.unselectedDestinationImage:Lcom/here/android/mpa/common/Image;
        //   490: ifnonnull       516
        //   493: new             Lcom/here/android/mpa/common/Image;
        //   496: astore_1       
        //   497: aload_1        
        //   498: invokespecial   com/here/android/mpa/common/Image.<init>:()V
        //   501: aload_0        
        //   502: aload_1        
        //   503: putfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.unselectedDestinationImage:Lcom/here/android/mpa/common/Image;
        //   506: aload_0        
        //   507: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.unselectedDestinationImage:Lcom/here/android/mpa/common/Image;
        //   510: ldc_w           R.drawable.icon_destination_pin_unselected
        //   513: invokevirtual   com/here/android/mpa/common/Image.setImageResource:(I)V
        //   516: iconst_0       
        //   517: istore          7
        //   519: iload           7
        //   521: iload           10
        //   523: if_icmpge       684
        //   526: aload           6
        //   528: iload           7
        //   530: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   535: checkcast       Lcom/here/android/mpa/common/GeoCoordinate;
        //   538: astore_1       
        //   539: aload_1        
        //   540: ifnull          670
        //   543: new             Lcom/here/android/mpa/mapping/MapMarker;
        //   546: dup            
        //   547: aload_1        
        //   548: aload_0        
        //   549: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.unselectedDestinationImage:Lcom/here/android/mpa/common/Image;
        //   552: invokespecial   com/here/android/mpa/mapping/MapMarker.<init>:(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/Image;)V
        //   555: astore_1       
        //   556: aload_0        
        //   557: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
        //   560: aload_1        
        //   561: invokevirtual   com/navdy/hud/app/maps/here/HereMapController.addMapObject:(Lcom/here/android/mpa/mapping/MapObject;)V
        //   564: aload_0        
        //   565: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.mapMarkerList:Ljava/util/List;
        //   568: aload_1        
        //   569: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   574: pop            
        //   575: iinc            7, 1
        //   578: goto            519
        //   581: aload_0        
        //   582: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
        //   585: getfield        com/navdy/hud/app/ui/component/homescreen/HomeScreenView.activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;
        //   588: iconst_4       
        //   589: invokevirtual   com/navdy/hud/app/ui/component/homescreen/EtaView.setVisibility:(I)V
        //   592: goto            128
        //   595: astore          8
        //   597: aload_0        
        //   598: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.logger:Lcom/navdy/service/library/log/Logger;
        //   601: aload           8
        //   603: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   606: goto            198
        //   609: aload_0        
        //   610: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.logger:Lcom/navdy/service/library/log/Logger;
        //   613: ldc_w           "map-route- switchToRouteSearchMode: nav route not removed, does not exist"
        //   616: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   619: goto            221
        //   622: astore_1       
        //   623: aload_0        
        //   624: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.logger:Lcom/navdy/service/library/log/Logger;
        //   627: aload_1        
        //   628: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   631: goto            327
        //   634: astore_1       
        //   635: aload_0        
        //   636: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.logger:Lcom/navdy/service/library/log/Logger;
        //   639: aload_1        
        //   640: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   643: goto            399
        //   646: astore_1       
        //   647: aload_0        
        //   648: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.logger:Lcom/navdy/service/library/log/Logger;
        //   651: aload_1        
        //   652: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   655: goto            486
        //   658: astore_1       
        //   659: aload_0        
        //   660: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.logger:Lcom/navdy/service/library/log/Logger;
        //   663: aload_1        
        //   664: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   667: goto            516
        //   670: aload_0        
        //   671: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.mapMarkerList:Ljava/util/List;
        //   674: aconst_null    
        //   675: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   680: pop            
        //   681: goto            575
        //   684: aload_0        
        //   685: getfield        com/navdy/hud/app/ui/component/homescreen/NavigationView.logger:Lcom/navdy/service/library/log/Logger;
        //   688: new             Ljava/lang/StringBuilder;
        //   691: dup            
        //   692: invokespecial   java/lang/StringBuilder.<init>:()V
        //   695: ldc_w           "addded dest marker:"
        //   698: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   701: iload           10
        //   703: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   706: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   709: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   712: aload_0        
        //   713: aload_3        
        //   714: aload           4
        //   716: iload           5
        //   718: iconst_1       
        //   719: invokevirtual   com/navdy/hud/app/ui/component/homescreen/NavigationView.zoomToBoundBox:(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/routing/Route;ZZ)V
        //   722: return         
        //    Signature:
        //  (Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/routing/Route;ZLjava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;I)V
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  150    157    595    609    Ljava/lang/Throwable;
        //  169    178    595    609    Ljava/lang/Throwable;
        //  178    198    595    609    Ljava/lang/Throwable;
        //  252    303    622    634    Ljava/lang/Throwable;
        //  307    327    622    634    Ljava/lang/Throwable;
        //  327    375    634    646    Ljava/lang/Throwable;
        //  379    399    634    646    Ljava/lang/Throwable;
        //  447    465    646    658    Ljava/lang/Throwable;
        //  480    486    646    658    Ljava/lang/Throwable;
        //  493    516    658    670    Ljava/lang/Throwable;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 324, Size: 324
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void updateLayoutForMode(final NavigationMode navigationMode) {
        int n;
        if (this.homeScreenView.isNavigationActive()) {
            n = HomeScreenResourceValues.activeMapHeight;
        }
        else {
            n = HomeScreenResourceValues.openMapHeight;
        }
        this.adjustMaplayoutHeight(n);
    }
    
    public void zoomToBoundBox(final GeoBoundingBox geoBoundingBox, final Route route, final boolean b, final boolean b2) {
        final HereMapController.State state = this.mapController.getState();
        if (state != HereMapController.State.ROUTE_PICKER) {
            this.logger.i("zoomToBoundBox: incorrect state:" + state);
        }
        else {
            Label_0141: {
                try {
                    this.cleanupFluctuator();
                    if (b) {
                        this.startFluctuator();
                    }
                    if (geoBoundingBox == null) {
                        break Label_0141;
                    }
                    if (b2) {
                        this.cleanupMapOverviewRoutes();
                    }
                    if (route != null) {
                        final MapRoute mapRoute = new MapRoute(route);
                        mapRoute.setTrafficEnabled(true);
                        this.addMapOverviewRoutes(mapRoute);
                    }
                    this.mapController.zoomTo(geoBoundingBox, HomeScreenConstants.routePickerViewRect, Map.Animation.BOW, -1.0f);
                    this.logger.v("zoomed to bbox");
                }
                catch (Throwable t) {
                    this.logger.e(t);
                }
                return;
            }
            this.logger.v("zoomed to bbox: null");
        }
    }
}
