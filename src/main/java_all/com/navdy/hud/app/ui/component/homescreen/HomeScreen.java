package com.navdy.hud.app.ui.component.homescreen;

import android.os.Bundle;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import javax.inject.Singleton;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.ui.activity.Main;
import dagger.Module;
import com.navdy.service.library.events.ui.Screen;
import flow.Flow;
import flow.Layout;
import com.navdy.hud.app.screen.BaseScreen;

@Layout(R.layout.screen_home)
public class HomeScreen extends BaseScreen
{
    private static Presenter presenter;
    
    @Override
    public int getAnimationIn(final Flow.Direction direction) {
        return 17432576;
    }
    
    @Override
    public int getAnimationOut(final Flow.Direction direction) {
        return 17432577;
    }
    
    @Override
    public Object getDaggerModule() {
        return new Module();
    }
    
    public DisplayMode getDisplayMode() {
        Enum<DisplayMode> displayMode;
        if (HomeScreen.presenter != null) {
            displayMode = HomeScreen.presenter.getDisplayMode();
        }
        else {
            displayMode = null;
        }
        return (DisplayMode)displayMode;
    }
    
    public HomeScreenView getHomeScreenView() {
        HomeScreenView homeScreenView;
        if (HomeScreen.presenter != null) {
            homeScreenView = HomeScreen.presenter.getHomeScreenView();
        }
        else {
            homeScreenView = null;
        }
        return homeScreenView;
    }
    
    @Override
    public String getMortarScopeName() {
        return this.getClass().getName();
    }
    
    @Override
    public Screen getScreen() {
        return Screen.SCREEN_HOME;
    }
    
    public void setDisplayMode(final DisplayMode displayMode) {
        if (HomeScreen.presenter != null) {
            HomeScreen.presenter.setDisplayMode(displayMode);
        }
    }
    
    public enum DisplayMode
    {
        MAP, 
        SMART_DASH;
    }
    
    @dagger.Module(addsTo = Main.Module.class, injects = { HomeScreenView.class })
    public class Module
    {
    }
    
    @Singleton
    public static class Presenter extends BasePresenter<HomeScreenView>
    {
        private static final Logger logger;
        @Inject
        Bus bus;
        
        static {
            logger = new Logger(Presenter.class);
        }
        
        DisplayMode getDisplayMode() {
            final HomeScreenView homeScreenView = this.getHomeScreenView();
            DisplayMode displayMode;
            if (homeScreenView != null) {
                displayMode = homeScreenView.getDisplayMode();
            }
            else {
                displayMode = null;
            }
            return displayMode;
        }
        
        HomeScreenView getHomeScreenView() {
            return this.getView();
        }
        
        @Override
        public void onLoad(final Bundle bundle) {
            HomeScreen.presenter = this;
        }
        
        @Override
        protected void onUnload() {
            HomeScreen.presenter = null;
        }
        
        void setDisplayMode(final DisplayMode displayMode) {
            final HomeScreenView homeScreenView = this.getHomeScreenView();
            if (homeScreenView != null) {
                homeScreenView.setDisplayMode(displayMode);
            }
        }
    }
}
