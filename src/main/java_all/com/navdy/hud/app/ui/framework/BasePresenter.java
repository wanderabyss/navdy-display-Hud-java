package com.navdy.hud.app.ui.framework;

import android.os.Bundle;
import mortar.ViewPresenter;
import android.view.View;

public class BasePresenter<V extends View> extends ViewPresenter<V>
{
    protected boolean active;
    
    @Override
    public void dropView(final V v) {
        final View view = this.getView();
        if (view != null) {
            if (v == view) {
                this.active = false;
                this.onUnload();
            }
            super.dropView((V)view);
        }
    }
    
    public boolean isActive() {
        return this.active;
    }
    
    public void onLoad(final Bundle bundle) {
        this.active = true;
        super.onLoad(bundle);
    }
    
    protected void onUnload() {
    }
}
