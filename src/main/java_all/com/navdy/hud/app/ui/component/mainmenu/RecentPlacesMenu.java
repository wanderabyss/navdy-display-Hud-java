package com.navdy.hud.app.ui.component.mainmenu;

import android.graphics.Shader;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;
import android.view.View;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import java.util.Iterator;
import com.navdy.hud.app.framework.destinations.Destination;
import com.navdy.hud.app.framework.destinations.DestinationsManager;
import java.util.ArrayList;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import java.util.List;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import android.content.res.Resources;
import com.navdy.hud.app.ui.component.vlist.VerticalList;

public class RecentPlacesMenu implements IMenu
{
    private static final VerticalList.Model back;
    private static final String places;
    private static final int placesColor;
    private static final Resources resources;
    private static final Logger sLogger;
    private int backSelection;
    private Bus bus;
    private List<VerticalList.Model> cachedList;
    private IMenu parent;
    private MainMenuScreen2.Presenter presenter;
    private List<VerticalList.Model> returnToCacheList;
    private VerticalMenuComponent vscrollComponent;
    
    static {
        sLogger = new Logger(RecentPlacesMenu.class);
        resources = HudApplication.getAppContext().getResources();
        placesColor = RecentPlacesMenu.resources.getColor(R.color.mm_recent_places);
        places = RecentPlacesMenu.resources.getString(R.string.carousel_menu_recent_place);
        final String string = RecentPlacesMenu.resources.getString(R.string.back);
        final int color = RecentPlacesMenu.resources.getColor(R.color.mm_back);
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, color, MainMenu.bkColorUnselected, color, string, null);
    }
    
    public RecentPlacesMenu(final Bus bus, final VerticalMenuComponent vscrollComponent, final MainMenuScreen2.Presenter presenter, final IMenu parent) {
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
    }
    
    @Override
    public IMenu getChildMenu(final IMenu menu, final String s, final String s2) {
        return null;
    }
    
    @Override
    public int getInitialSelection() {
        int n = 1;
        if (this.cachedList == null || this.cachedList.size() <= 1) {
            n = 0;
        }
        return n;
    }
    
    @Override
    public List<VerticalList.Model> getItems() {
        List<VerticalList.Model> cachedList;
        if (this.cachedList != null) {
            cachedList = this.cachedList;
        }
        else {
            cachedList = new ArrayList<VerticalList.Model>();
            this.returnToCacheList = new ArrayList<VerticalList.Model>();
            cachedList.add(RecentPlacesMenu.back);
            final List<Destination> recentDestinations = DestinationsManager.getInstance().getRecentDestinations();
            if (recentDestinations != null && recentDestinations.size() > 0) {
                RecentPlacesMenu.sLogger.v("recent destinations:" + recentDestinations.size());
                int n = 0;
                for (final Destination state : recentDestinations) {
                    final VerticalList.Model buildModel = PlacesMenu.buildModel(state, n);
                    buildModel.state = state;
                    cachedList.add(buildModel);
                    this.returnToCacheList.add(buildModel);
                    ++n;
                }
            }
            else {
                RecentPlacesMenu.sLogger.v("no recent destinations");
            }
            this.cachedList = cachedList;
        }
        return cachedList;
    }
    
    @Override
    public VerticalList.Model getModelfromPos(final int n) {
        VerticalList.Model model;
        if (this.cachedList != null && this.cachedList.size() > n) {
            model = this.cachedList.get(n);
        }
        else {
            model = null;
        }
        return model;
    }
    
    @Override
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    @Override
    public Menu getType() {
        return Menu.RECENT_PLACES;
    }
    
    @Override
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    @Override
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    @Override
    public boolean isItemClickable(final int n, final int n2) {
        return true;
    }
    
    @Override
    public void onBindToView(final VerticalList.Model model, final View view, final int n, final VerticalList.ModelState modelState) {
    }
    
    @Override
    public void onFastScrollEnd() {
    }
    
    @Override
    public void onFastScrollStart() {
    }
    
    @Override
    public void onItemSelected(final VerticalList.ItemSelectionState itemSelectionState) {
    }
    
    @Override
    public void onScrollIdle() {
    }
    
    @Override
    public void onUnload(final MenuLevel menuLevel) {
        switch (menuLevel) {
            case CLOSE:
                if (this.returnToCacheList != null) {
                    RecentPlacesMenu.sLogger.v("rpm:unload add to cache");
                    VerticalModelCache.addToCache(this.returnToCacheList);
                    this.returnToCacheList = null;
                    break;
                }
                break;
        }
    }
    
    @Override
    public boolean selectItem(final VerticalList.ItemSelectionState itemSelectionState) {
        RecentPlacesMenu.sLogger.v("select id:" + itemSelectionState.id + " pos:" + itemSelectionState.pos);
        switch (itemSelectionState.id) {
            default:
                ((PlacesMenu)this.parent).launchDestination(this.cachedList.get(itemSelectionState.pos));
                break;
            case R.id.menu_back:
                RecentPlacesMenu.sLogger.v("back");
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                break;
        }
        return true;
    }
    
    @Override
    public void setBackSelectionId(final int n) {
    }
    
    @Override
    public void setBackSelectionPos(final int backSelection) {
        this.backSelection = backSelection;
    }
    
    @Override
    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_place_recent_2, RecentPlacesMenu.placesColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText((CharSequence)RecentPlacesMenu.places);
    }
    
    @Override
    public void showToolTip() {
    }
}
