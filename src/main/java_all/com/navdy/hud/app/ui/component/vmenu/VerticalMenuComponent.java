package com.navdy.hud.app.ui.component.vmenu;

import com.navdy.service.library.events.input.Gesture;
import java.util.List;
import android.graphics.Shader;
import android.view.ViewPropertyAnimator;
import android.animation.AnimatorSet;
import android.os.SystemClock;
import com.navdy.service.library.events.input.GestureEvent;
import android.animation.PropertyValuesHolder;
import android.animation.ObjectAnimator;
import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.animation.AnimatorSet;
import android.graphics.drawable.BitmapDrawable;
import android.content.Context;
import android.view.ViewGroup;
import android.view.ViewGroup;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder;
import butterknife.ButterKnife;
import com.navdy.hud.app.ui.component.UISettings;
import android.animation.Animator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;
import java.util.concurrent.TimeUnit;
import com.navdy.hud.app.view.ToolTipView;
import android.graphics.Bitmap;
import java.util.HashMap;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import android.widget.FrameLayout;
import com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView;
import com.navdy.hud.app.manager.InputManager;
import java.util.HashSet;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator;
import android.os.Handler;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.ui.component.HaloView;
import android.view.View;
import butterknife.InjectView;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;
import android.view.animation.Interpolator;
import com.navdy.service.library.log.Logger;

public class VerticalMenuComponent
{
    public static final int ANIMATION_IN_OUT_DURATION = 150;
    public static final int CLICK_ANIMATION_DURATION = 50;
    public static final int CLOSE_ANIMATION_DURATION = 150;
    private static final int FAST_SCROLL_APPEARANCE_ANIM_TIME = 50;
    private static final int FAST_SCROLL_CLOSE_MENU_WAIT_PERIOD = 150;
    private static final int FAST_SCROLL_EXIT_DELAY = 800;
    private static final int FAST_SCROLL_TRIGGER_CLICKS = 10;
    private static final int FAST_SCROLL_TRIGGER_TIME;
    private static final int INDICATOR_FADE_IDLE_DURATION = 2000;
    private static final int INDICATOR_FADE_IN_DURATION = 500;
    private static final int INDICATOR_FADE_OUT_DURATION = 500;
    private static final float MAIN_TO_SELECTED_ICON_SCALE = 1.0f;
    public static final int MIN_FAST_SCROLL_ITEM = 40;
    public static final int MIN_INDEX_ENTRY_COUNT = 2;
    private static final int NO_ANIMATION = -1;
    private static final float SELECTED_TO_MAIN_ICON_SCALE = 1.2f;
    public static final int animateTranslateY;
    private static final int closeContainerHeight;
    private static final int closeContainerScrollY;
    private static final int closeHaloColor;
    private static final int indicatorY;
    private static final int[] location;
    private static final Logger sLogger;
    private Interpolator accelerateInterpolator;
    private ImageView animImageView;
    private TextView animSubTitle;
    private TextView animSubTitle2;
    private TextView animTitle;
    private Callback callback;
    private int clickCount;
    private long clickTime;
    @InjectView(R.id.closeContainer)
    public ViewGroup closeContainer;
    @InjectView(R.id.closeContainerScrim)
    public View closeContainerScrim;
    @InjectView(R.id.closeHalo)
    public HaloView closeHalo;
    @InjectView(R.id.closeIconContainer)
    public ViewGroup closeIconContainer;
    private Runnable closeIconEndAction;
    private boolean closeMenuAnimationActive;
    private DefaultAnimationListener closeMenuHideListener;
    private DefaultAnimationListener closeMenuShowListener;
    private VerticalList.ContainerCallback containerCallback;
    private Interpolator decelerateInterpolator;
    private Runnable fadeOutRunnable;
    private final int fastScrollCloseWaitPeriod;
    @InjectView(R.id.fastScrollView)
    public ViewGroup fastScrollContainer;
    private int fastScrollCurrentItem;
    private DefaultAnimationListener fastScrollIn;
    private VerticalFastScrollIndex fastScrollIndex;
    private int fastScrollOffset;
    private DefaultAnimationListener fastScrollOut;
    @InjectView(R.id.fastScrollText)
    public TextView fastScrollText;
    private Runnable fastScrollTimeout;
    private Handler handler;
    @InjectView(R.id.indicator)
    public CarouselIndicator indicator;
    private long lastUpEvent;
    @InjectView(R.id.leftContainer)
    public ViewGroup leftContainer;
    private Interpolator linearInterpolator;
    private boolean listLoaded;
    private HashSet<InputManager.CustomKeyEvent> overrideDefaultKeyEvents;
    private ViewGroup parentContainer;
    @InjectView(R.id.recyclerView)
    public VerticalRecyclerView recyclerView;
    @InjectView(R.id.rightContainer)
    public ViewGroup rightContainer;
    private VerticalList.Direction scrollDirection;
    @InjectView(R.id.selectedCustomView)
    public FrameLayout selectedCustomView;
    @InjectView(R.id.selectedIconColorImage)
    public IconColorImageView selectedIconColorImage;
    @InjectView(R.id.selectedIconImage)
    public InitialsImageView selectedIconImage;
    @InjectView(R.id.selectedImage)
    public ViewGroup selectedImage;
    @InjectView(R.id.selectedText)
    public TextView selectedText;
    private boolean stoppingFastScrolling;
    private HashMap<Integer, Bitmap> sublistAnimationCache;
    @InjectView(R.id.tooltip)
    public ToolTipView toolTip;
    public VerticalList verticalList;
    private VerticalList.Callback vlistCallback;
    
    static {
        sLogger = new Logger("VerticalMenuC");
        FAST_SCROLL_TRIGGER_TIME = (int)TimeUnit.SECONDS.toMillis(2L);
        location = new int[2];
        final Resources resources = HudApplication.getAppContext().getResources();
        closeContainerHeight = resources.getDimensionPixelSize(R.dimen.vmenu_close_height);
        closeContainerScrollY = resources.getDimensionPixelSize(R.dimen.vmenu_close_scroll_y);
        indicatorY = resources.getDimensionPixelSize(R.dimen.vmenu_indicator_y);
        animateTranslateY = resources.getDimensionPixelSize(R.dimen.vmenu_anim_translate_y);
        closeHaloColor = resources.getColor(R.color.close_halo);
    }
    
    public VerticalMenuComponent(final ViewGroup parentContainer, final Callback callback, final boolean b) {
        this.linearInterpolator = (Interpolator)new LinearInterpolator();
        this.decelerateInterpolator = (Interpolator)new DecelerateInterpolator();
        this.accelerateInterpolator = (Interpolator)new AccelerateInterpolator();
        this.overrideDefaultKeyEvents = new HashSet<InputManager.CustomKeyEvent>();
        this.closeMenuShowListener = new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                VerticalMenuComponent.this.closeMenuAnimationActive = false;
            }
            
            @Override
            public void onAnimationStart(final Animator animator) {
                VerticalMenuComponent.sLogger.v("closeMenuShowListener");
                VerticalMenuComponent.this.closeContainer.setVisibility(View.VISIBLE);
                VerticalMenuComponent.this.closeContainerScrim.setVisibility(View.VISIBLE);
                VerticalMenuComponent.this.closeContainerScrim.setAlpha(0.0f);
            }
        };
        this.fastScrollIn = new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                VerticalMenuComponent.this.handler.removeCallbacks(VerticalMenuComponent.this.fastScrollTimeout);
                VerticalMenuComponent.this.handler.postDelayed(VerticalMenuComponent.this.fastScrollTimeout, 800L);
            }
        };
        this.fastScrollOut = new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                VerticalMenuComponent.this.stoppingFastScrolling = false;
                VerticalMenuComponent.this.fastScrollContainer.setVisibility(GONE);
                VerticalMenuComponent.this.startFadeOut();
            }
        };
        this.fastScrollTimeout = new Runnable() {
            @Override
            public void run() {
                VerticalMenuComponent.this.stopFastScrolling();
            }
        };
        this.closeMenuHideListener = new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                VerticalMenuComponent.sLogger.v("closeMenuHideListener");
                VerticalMenuComponent.this.closeMenuAnimationActive = false;
                VerticalMenuComponent.this.closeContainer.setVisibility(GONE);
                VerticalMenuComponent.this.closeContainerScrim.setVisibility(GONE);
            }
        };
        this.vlistCallback = new VerticalList.Callback() {
            @Override
            public void onBindToView(final Model model, final View view, final int n, final ModelState modelState) {
                VerticalMenuComponent.this.callback.onBindToView(model, view, n, modelState);
            }
            
            @Override
            public void onItemSelected(final ItemSelectionState itemSelectionState) {
                VerticalMenuComponent.this.callback.onItemSelected(itemSelectionState);
            }
            
            @Override
            public void onLoad() {
                VerticalMenuComponent.this.listLoaded = true;
                VerticalMenuComponent.this.callback.onLoad();
            }
            
            @Override
            public void onScrollIdle() {
                VerticalMenuComponent.this.callback.onScrollIdle();
            }
            
            @Override
            public void select(final ItemSelectionState itemSelectionState) {
                VerticalMenuComponent.this.callback.select(itemSelectionState);
            }
        };
        this.containerCallback = new VerticalList.ContainerCallback() {
            @Override
            public void hideToolTips() {
                VerticalMenuComponent.this.hideToolTip();
            }
            
            @Override
            public boolean isCloseMenuVisible() {
                return VerticalMenuComponent.this.isCloseMenuVisible();
            }
            
            @Override
            public boolean isFastScrolling() {
                return VerticalMenuComponent.this.isFastScrollVisible();
            }
            
            @Override
            public void showToolTips() {
                if (VerticalMenuComponent.this.callback != null) {
                    VerticalMenuComponent.this.callback.showToolTip();
                }
            }
        };
        this.fadeOutRunnable = new Runnable() {
            @Override
            public void run() {
                VerticalMenuComponent.sLogger.v("fadeOutRunnable");
                VerticalMenuComponent.this.fadeoutIndicator(500);
            }
        };
        this.closeIconEndAction = new Runnable() {
            @Override
            public void run() {
                VerticalMenuComponent.this.callback.close();
            }
        };
        this.handler = new Handler();
        this.sublistAnimationCache = new HashMap<Integer, Bitmap>();
        if (parentContainer == null || callback == null) {
            throw new IllegalArgumentException();
        }
        int fastScrollCloseWaitPeriod;
        if (UISettings.isVerticalListNoCloseTimeout()) {
            fastScrollCloseWaitPeriod = 0;
        }
        else {
            fastScrollCloseWaitPeriod = 150;
        }
        this.fastScrollCloseWaitPeriod = fastScrollCloseWaitPeriod;
        this.callback = callback;
        ButterKnife.inject(this, (View)parentContainer);
        this.parentContainer = parentContainer;
        this.closeContainer.setY((float)(-VerticalMenuComponent.closeContainerHeight));
        this.indicator.setY((float)VerticalMenuComponent.indicatorY);
        this.fadeoutIndicator(-1);
        this.verticalList = new VerticalList(this.recyclerView, this.indicator, this.vlistCallback, this.containerCallback, b);
        this.closeHalo.setStrokeColor(VerticalMenuComponent.closeHaloColor);
        this.toolTip.setParams(VerticalViewHolder.mainIconSize, VerticalViewHolder.iconMargin, 4);
        final Context context = parentContainer.getContext();
        (this.animImageView = new ImageView(context)).setVisibility(INVISIBLE);
        this.parentContainer.addView((View)this.animImageView, (ViewGroup.LayoutParams)new ViewGroup.MarginLayoutParams(VerticalViewHolder.selectedIconSize, VerticalViewHolder.selectedIconSize));
        (this.animTitle = new TextView(context)).setTextAppearance(context, R.style.vlist_title);
        this.animTitle.setVisibility(INVISIBLE);
        this.parentContainer.addView((View)this.animTitle, (ViewGroup.LayoutParams)new ViewGroup.MarginLayoutParams(-2, -2));
        (this.animSubTitle = new TextView(context)).setTextAppearance(context, R.style.vlist_subtitle);
        this.animSubTitle.setVisibility(INVISIBLE);
        this.parentContainer.addView((View)this.animSubTitle, (ViewGroup.LayoutParams)new ViewGroup.MarginLayoutParams(-2, -2));
        (this.animSubTitle2 = new TextView(context)).setTextAppearance(context, R.style.vlist_subtitle);
        this.animSubTitle2.setVisibility(INVISIBLE);
        this.parentContainer.addView((View)this.animSubTitle2, (ViewGroup.LayoutParams)new ViewGroup.MarginLayoutParams(-2, -2));
    }
    
    private void addToCache(final ImageView imageView, final VerticalList.Model model) {
        if (model != null) {
            final BitmapDrawable bitmapDrawable = (BitmapDrawable)imageView.getDrawable();
            if (bitmapDrawable != null) {
                final Bitmap bitmap = bitmapDrawable.getBitmap();
                if (bitmap != null) {
                    this.sublistAnimationCache.put(System.identityHashCode(model), bitmap);
                }
            }
        }
    }
    
    private void changeScrollIndex(int currentItem) {
        this.handler.removeCallbacks(this.fastScrollTimeout);
        currentItem += this.fastScrollCurrentItem;
        if (currentItem >= 0 && currentItem < this.fastScrollIndex.length) {
            final String title = this.fastScrollIndex.getTitle(currentItem);
            this.fastScrollText.setText((CharSequence)title);
            this.fastScrollCurrentItem = currentItem;
            currentItem = this.fastScrollIndex.getPosition(title);
            this.indicator.setCurrentItem(currentItem);
        }
        else {
            currentItem = this.fastScrollIndex.getPosition(this.fastScrollIndex.getTitle(this.fastScrollCurrentItem));
            if (currentItem != this.indicator.getCurrentItem()) {
                this.indicator.setCurrentItem(currentItem);
            }
        }
        this.handler.postDelayed(this.fastScrollTimeout, 800L);
    }
    
    private boolean checkFastScroll(final VerticalList.Direction scrollDirection, int currentPosition, final long clickTime) {
        boolean b = true;
        if (!this.stoppingFastScrolling) {
            if (this.isFastScrollVisible()) {
                this.changeScrollIndex(currentPosition);
            }
            else {
                if (this.scrollDirection == scrollDirection) {
                    if (clickTime - this.clickTime > VerticalMenuComponent.FAST_SCROLL_TRIGGER_TIME) {
                        this.clickTime = clickTime;
                        this.clickCount = 1;
                    }
                    else {
                        ++this.clickCount;
                        if (this.clickCount >= 10) {
                            currentPosition = this.verticalList.getCurrentPosition();
                            this.fastScrollCurrentItem = this.fastScrollIndex.getIndexForPosition(currentPosition);
                            if (this.scrollDirection == VerticalList.Direction.DOWN && this.fastScrollCurrentItem == this.fastScrollIndex.getEntryCount() - 1) {
                                VerticalMenuComponent.sLogger.v("checkFastScroll: not allowed,last element");
                                this.clickCount = 0;
                                this.clickTime = clickTime;
                                b = false;
                                return b;
                            }
                            this.startFastScrolling();
                            this.fadeinIndicator(-1);
                            this.handler.removeCallbacks(this.fadeOutRunnable);
                            return b;
                        }
                    }
                }
                else {
                    this.scrollDirection = scrollDirection;
                    this.clickCount = 0;
                    this.clickTime = clickTime;
                }
                b = false;
            }
        }
        return b;
    }
    
    private Bitmap getFromCache(final VerticalList.Model model) {
        Bitmap bitmap;
        if (model == null) {
            bitmap = null;
        }
        else {
            bitmap = this.sublistAnimationCache.get(System.identityHashCode(model));
        }
        return bitmap;
    }
    
    private void hideCloseMenu() {
        if (this.isCloseMenuVisible()) {
            VerticalMenuComponent.sLogger.v("hideCloseMenu");
            final AnimatorSet set = new AnimatorSet();
            set.setDuration(150L);
            set.setInterpolator((TimeInterpolator)this.linearInterpolator);
            set.addListener((Animator.AnimatorListener)this.closeMenuHideListener);
            set.playTogether(new Animator[] { ObjectAnimator.ofFloat(this.closeContainer, View.Y, new float[] { -VerticalMenuComponent.closeContainerHeight }), ObjectAnimator.ofFloat(this.closeContainerScrim, View.ALPHA, new float[] { 0.0f }), ObjectAnimator.ofFloat(this.leftContainer, View.Y, new float[] { 0.0f }), ObjectAnimator.ofFloat(this.rightContainer, View.Y, new float[] { 0.0f }), ObjectAnimator.ofFloat(this.indicator, View.Y, new float[] { VerticalMenuComponent.indicatorY }) });
            this.closeMenuAnimationActive = true;
            set.start();
            this.verticalList.animate(this.verticalList.getRawPosition(), true, 150, false, true);
        }
    }
    
    private boolean isFastScrollAvailable() {
        return this.fastScrollIndex != null;
    }
    
    private boolean isFastScrollVisible() {
        return this.fastScrollContainer.getVisibility() == 0;
    }
    
    private void selectFastScroll() {
        VerticalMenuComponent.sLogger.v("selectFastScroll");
        this.handler.removeCallbacks(this.fastScrollTimeout);
        this.fastScrollTimeout.run();
    }
    
    private void showCloseMenu() {
        if (!this.isCloseMenuVisible()) {
            VerticalMenuComponent.sLogger.v("showCloseMenu");
            final AnimatorSet set = new AnimatorSet();
            set.setDuration(150L);
            set.setInterpolator((TimeInterpolator)this.linearInterpolator);
            set.addListener((Animator.AnimatorListener)this.closeMenuShowListener);
            set.playTogether(new Animator[] { ObjectAnimator.ofFloat(this.closeContainer, View.Y, new float[] { 0.0f }), ObjectAnimator.ofFloat(this.closeContainerScrim, View.ALPHA, new float[] { 1.0f }), ObjectAnimator.ofFloat(this.leftContainer, View.Y, new float[] { VerticalMenuComponent.closeContainerScrollY }), ObjectAnimator.ofFloat(this.rightContainer, View.Y, new float[] { VerticalMenuComponent.closeContainerScrollY }), ObjectAnimator.ofFloat(this.indicator, View.Y, new float[] { VerticalMenuComponent.indicatorY + VerticalMenuComponent.closeContainerScrollY }) });
            this.closeMenuAnimationActive = true;
            set.start();
            this.verticalList.animate(this.verticalList.getRawPosition(), false, 150, true, true);
            this.fadeoutIndicator(150);
        }
    }
    
    private void startFadeOut() {
        this.handler.removeCallbacks(this.fadeOutRunnable);
        this.handler.postDelayed(this.fadeOutRunnable, 2000L);
    }
    
    private void startFastScrolling() {
        if (this.isFastScrollAvailable()) {
            this.verticalList.lock();
            final String title = this.fastScrollIndex.getTitle(this.fastScrollCurrentItem);
            VerticalMenuComponent.sLogger.v("startFastScrolling:" + title);
            this.fastScrollContainer.setAlpha(0.0f);
            this.fastScrollContainer.setVisibility(View.VISIBLE);
            this.fastScrollText.setText((CharSequence)title);
            this.stoppingFastScrolling = false;
            this.callback.onFastScrollStart();
            this.fastScrollContainer.animate().alpha(1.0f).setDuration(50L).setListener((Animator.AnimatorListener)this.fastScrollIn).start();
        }
    }
    
    private void stopFastScrolling() {
        if (this.isFastScrollAvailable()) {
            final String value = String.valueOf(this.fastScrollText.getText());
            int position;
            final int n = position = this.fastScrollIndex.getPosition(value);
            if (this.verticalList.isFirstEntryBlank()) {
                position = n + 1;
            }
            int n2 = position;
            if (this.fastScrollIndex != null) {
                n2 = position + this.fastScrollIndex.getOffset();
            }
            VerticalMenuComponent.sLogger.v("stopFastScrolling:" + n2 + " , " + value);
            this.callback.onFastScrollEnd();
            this.stoppingFastScrolling = true;
            this.verticalList.unlock();
            this.verticalList.scrollToPosition(n2);
            this.fastScrollContainer.animate().alpha(0.0f).setDuration(50L).setListener((Animator.AnimatorListener)this.fastScrollOut).start();
        }
    }
    
    public void animateIn(final Animator.AnimatorListener animator$AnimatorListener) {
        VerticalMenuComponent.sLogger.v("animateIn");
        final AnimatorSet set = new AnimatorSet();
        set.setDuration(150L);
        set.setInterpolator((TimeInterpolator)this.decelerateInterpolator);
        if (animator$AnimatorListener != null) {
            set.addListener(animator$AnimatorListener);
        }
        set.playTogether(new Animator[] { ObjectAnimator.ofPropertyValuesHolder(this.leftContainer, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, new float[] { VerticalMenuComponent.animateTranslateY, 0.0f }), PropertyValuesHolder.ofFloat(View.ALPHA, new float[] { 1.0f }) }), ObjectAnimator.ofPropertyValuesHolder(this.rightContainer, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, new float[] { VerticalMenuComponent.animateTranslateY, 0.0f }), PropertyValuesHolder.ofFloat(View.ALPHA, new float[] { 1.0f }) }) });
        set.start();
    }
    
    public void animateOut(final Animator.AnimatorListener animator$AnimatorListener) {
        VerticalMenuComponent.sLogger.v("animateOut");
        this.indicator.setVisibility(INVISIBLE);
        final AnimatorSet set = new AnimatorSet();
        set.setDuration(150L);
        set.setInterpolator((TimeInterpolator)this.accelerateInterpolator);
        if (animator$AnimatorListener != null) {
            set.addListener(animator$AnimatorListener);
        }
        set.playTogether(new Animator[] { ObjectAnimator.ofPropertyValuesHolder(this.leftContainer, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.Y, new float[] { this.leftContainer.getY() + VerticalMenuComponent.animateTranslateY }), PropertyValuesHolder.ofFloat(View.ALPHA, new float[] { 0.0f }) }), ObjectAnimator.ofPropertyValuesHolder(this.rightContainer, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.Y, new float[] { this.rightContainer.getY() + VerticalMenuComponent.animateTranslateY }), PropertyValuesHolder.ofFloat(View.ALPHA, new float[] { 0.0f }) }), ObjectAnimator.ofPropertyValuesHolder(this.closeContainer, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, new float[] { VerticalMenuComponent.animateTranslateY }), PropertyValuesHolder.ofFloat(View.ALPHA, new float[] { 0.0f }) }) });
        set.start();
    }
    
    public void clear() {
        this.sublistAnimationCache.clear();
        this.verticalList.unlock(false);
        this.verticalList.clearAllAnimations();
    }
    
    public void fadeinIndicator(final int n) {
        VerticalMenuComponent.sLogger.v("fadeinIndicator:" + n);
        if (n == -1) {
            this.indicator.setAlpha(1.0f);
        }
        else if (this.indicator.getAlpha() != 1.0f) {
            this.indicator.animate().alpha(1.0f).setDuration((long)n).start();
        }
    }
    
    public void fadeoutIndicator(final int n) {
        if (n == -1) {
            this.indicator.setAlpha(0.0f);
        }
        else if (this.indicator.getAlpha() != 0.0f) {
            this.indicator.animate().alpha(0.0f).setDuration((long)n).start();
        }
    }
    
    public VerticalFastScrollIndex getFastScrollIndex() {
        return this.fastScrollIndex;
    }
    
    public ViewGroup getSelectedCustomImage() {
        return (ViewGroup)this.selectedCustomView;
    }
    
    public boolean handleGesture(final GestureEvent gestureEvent) {
        boolean b = false;
        if (!this.listLoaded) {
            VerticalMenuComponent.sLogger.v("list not loaded yet");
        }
        else if (this.closeMenuAnimationActive) {
            VerticalMenuComponent.sLogger.v("close menu animation is active, no-op");
        }
        else {
            switch (gestureEvent.gesture) {
                case GESTURE_SWIPE_RIGHT:
                    this.callback.close();
                    b = true;
                    break;
            }
        }
        return b;
    }
    
    public boolean handleKey(final InputManager.CustomKeyEvent customKeyEvent) {
        boolean contains = false;
        Label_0018: {
            if (!this.listLoaded) {
                VerticalMenuComponent.sLogger.v("list not loaded yet");
                contains = false;
            }
            else if (this.closeMenuAnimationActive) {
                VerticalMenuComponent.sLogger.v("close menu animation is active, no-op");
                contains = false;
            }
            else {
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                switch (customKeyEvent) {
                    default:
                        contains = this.overrideDefaultKeyEvents.contains(customKeyEvent);
                        break;
                    case LEFT:
                        if (this.verticalList.getCurrentPosition() == 0) {
                            if (this.verticalList.up().keyHandled) {
                                this.lastUpEvent = elapsedRealtime;
                                contains = true;
                                break;
                            }
                            if (SystemClock.elapsedRealtime() - this.lastUpEvent < this.fastScrollCloseWaitPeriod) {
                                VerticalMenuComponent.sLogger.v("user scrolled fast");
                                this.lastUpEvent = elapsedRealtime;
                                contains = true;
                                break;
                            }
                            this.lastUpEvent = elapsedRealtime;
                            this.showCloseMenu();
                        }
                        else {
                            if (this.isFastScrollAvailable() && this.checkFastScroll(VerticalList.Direction.UP, -1, elapsedRealtime)) {
                                contains = true;
                                break;
                            }
                            this.lastUpEvent = elapsedRealtime;
                            if (this.verticalList.up().listMoved) {
                                this.fadeinIndicator(500);
                                this.startFadeOut();
                            }
                        }
                        contains = true;
                        break;
                    case RIGHT:
                        if (this.isCloseMenuVisible()) {
                            this.hideCloseMenu();
                        }
                        else {
                            if (this.isFastScrollAvailable() && this.checkFastScroll(VerticalList.Direction.DOWN, 1, elapsedRealtime)) {
                                contains = true;
                                break;
                            }
                            this.hideToolTip();
                            if (this.verticalList.down().listMoved) {
                                this.fadeinIndicator(500);
                                this.startFadeOut();
                            }
                        }
                        contains = true;
                        break;
                    case SELECT:
                        if (this.isCloseMenuVisible() && !this.callback.isClosed()) {
                            this.closeHalo.setVisibility(INVISIBLE);
                            VerticalAnimationUtils.performClick((View)this.closeIconContainer, 50, this.closeIconEndAction);
                        }
                        else {
                            final int currentPosition = this.verticalList.getCurrentPosition();
                            final VerticalList.Model currentModel = this.verticalList.getCurrentModel();
                            if (currentModel == null) {
                                contains = false;
                                break;
                            }
                            switch (currentModel.id) {
                                default: {
                                    if (this.isFastScrollAvailable() && this.isFastScrollVisible()) {
                                        this.selectFastScroll();
                                        contains = true;
                                        break Label_0018;
                                    }
                                    final VerticalList.ItemSelectionState itemSelectionState = this.verticalList.getItemSelectionState();
                                    itemSelectionState.set(currentModel, currentModel.id, currentPosition, -1, -1);
                                    if (this.callback.isItemClickable(itemSelectionState)) {
                                        this.verticalList.select();
                                        break;
                                    }
                                    contains = false;
                                    break Label_0018;
                                }
                                case R.id.vlist_content_loading:
                                case R.id.vlist_loading:
                                    contains = false;
                                    break Label_0018;
                            }
                        }
                        contains = true;
                        break;
                }
            }
        }
        return contains;
    }
    
    public void hideToolTip() {
        this.toolTip.hide();
    }
    
    public boolean isCloseMenuVisible() {
        return this.closeContainer.getVisibility() == 0;
    }
    
    public void performBackAnimation(final Runnable runnable, final Runnable runnable2, final VerticalList.Model model, int n) {
        VerticalMenuComponent.sLogger.v("performBackAnimation");
        final float n2 = VerticalViewHolder.selectedIconSize * 0.20000005f / 2.0f;
        this.animImageView.setScaleX(1.2f);
        this.animImageView.setScaleY(1.2f);
        this.animImageView.setX(this.selectedImage.getX() + n2);
        this.animImageView.setY(this.selectedImage.getY() + n2);
        ImageView imageView;
        if (this.selectedIconColorImage.getVisibility() == 0) {
            imageView = this.selectedIconColorImage;
        }
        else {
            imageView = this.selectedIconImage;
        }
        final Bitmap fromCache = this.getFromCache(model);
        if (fromCache == null) {
            VerticalAnimationUtils.copyImage(imageView, this.animImageView);
            this.addToCache(this.animImageView, model);
        }
        else {
            this.animImageView.setImageBitmap(fromCache);
        }
        this.animImageView.setVisibility(View.VISIBLE);
        this.selectedImage.setAlpha(0.0f);
        final ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this.animImageView, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.SCALE_X, new float[] { 1.0f }), PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[] { 1.0f }), PropertyValuesHolder.ofFloat(View.X, new float[] { VerticalViewHolder.selectedImageX + n }), PropertyValuesHolder.ofFloat(View.Y, new float[] { VerticalViewHolder.selectedImageY }) });
        ofPropertyValuesHolder.setDuration((long)150);
        ofPropertyValuesHolder.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                final AnimatorSet set = new AnimatorSet();
                final AnimatorSet.Builder play = set.play((Animator)ObjectAnimator.ofFloat(VerticalMenuComponent.this.rightContainer, View.ALPHA, new float[] { 1.0f }));
                play.with((Animator)ObjectAnimator.ofFloat(VerticalMenuComponent.this.selectedImage, View.ALPHA, new float[] { 1.0f }));
                play.with((Animator)ObjectAnimator.ofFloat(VerticalMenuComponent.this.selectedText, View.ALPHA, new float[] { 1.0f }));
                set.setDuration(100L);
                set.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
                    @Override
                    public void onAnimationEnd(final Animator animator) {
                        VerticalMenuComponent.this.animImageView.setVisibility(INVISIBLE);
                        VerticalMenuComponent.this.animTitle.setVisibility(INVISIBLE);
                        VerticalMenuComponent.this.animSubTitle.setVisibility(INVISIBLE);
                        VerticalMenuComponent.this.animSubTitle2.setVisibility(INVISIBLE);
                        if (runnable2 != null) {
                            runnable2.run();
                        }
                    }
                });
                set.start();
            }
        });
        ofPropertyValuesHolder.start();
        this.animTitle.setX((float)(VerticalViewHolder.selectedTextX - VerticalMenuComponent.animateTranslateY));
        if (model.fontInfo == null) {
            VerticalList.setFontSize(model, this.verticalList.allowsTwoLineTitles());
        }
        this.animTitle.setTextSize(model.fontInfo.titleFontSize);
        this.animTitle.setAlpha(0.0f);
        this.animTitle.setVisibility(View.VISIBLE);
        n = 0;
        if (model == null || model.subTitle == null) {
            this.animSubTitle.setText((CharSequence)"");
        }
        else {
            this.animSubTitle.setX((float)(VerticalViewHolder.selectedTextX - VerticalMenuComponent.animateTranslateY));
            this.animSubTitle.setText((CharSequence)model.subTitle);
            this.animSubTitle.setTextSize(model.fontInfo.subTitleFontSize);
            this.animSubTitle.setAlpha(0.0f);
            this.animSubTitle.setVisibility(View.VISIBLE);
            n = 1;
        }
        int n3 = 0;
        if (model == null || model.subTitle2 == null) {
            this.animSubTitle2.setText((CharSequence)"");
        }
        else {
            this.animSubTitle2.setX((float)(VerticalViewHolder.selectedTextX - VerticalMenuComponent.animateTranslateY));
            this.animSubTitle2.setText((CharSequence)model.subTitle2);
            this.animSubTitle2.setTextSize(model.fontInfo.subTitle2FontSize);
            this.animSubTitle2.setAlpha(0.0f);
            this.animSubTitle2.setVisibility(View.VISIBLE);
            n3 = 1;
        }
        final AnimatorSet set = new AnimatorSet();
        final AnimatorSet.Builder play = set.play((Animator)ObjectAnimator.ofPropertyValuesHolder(this.animTitle, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.ALPHA, new float[] { 1.0f }), PropertyValuesHolder.ofFloat(View.X, new float[] { this.animTitle.getX() + VerticalMenuComponent.animateTranslateY }) }));
        if (n != 0) {
            play.with((Animator)ObjectAnimator.ofPropertyValuesHolder(this.animSubTitle, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.ALPHA, new float[] { 1.0f }), PropertyValuesHolder.ofFloat(View.X, new float[] { this.animSubTitle.getX() + VerticalMenuComponent.animateTranslateY }) }));
        }
        if (n3 != 0) {
            play.with((Animator)ObjectAnimator.ofPropertyValuesHolder(this.animSubTitle2, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.ALPHA, new float[] { 1.0f }), PropertyValuesHolder.ofFloat(View.X, new float[] { this.animSubTitle2.getX() + VerticalMenuComponent.animateTranslateY }) }));
        }
        set.setStartDelay((long)(int)(0.6666667f * 150));
        set.setDuration((long)50);
        set.start();
        final ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.selectedText, View.ALPHA, new float[] { 0.0f });
        ofFloat.setDuration((long)50);
        ofFloat.start();
        this.rightContainer.setPivotX(0.0f);
        this.rightContainer.setPivotY((float)(this.recyclerView.getMeasuredHeight() / 2));
        final AnimatorSet set2 = new AnimatorSet();
        final PropertyValuesHolder ofFloat2 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[] { 0.5f });
        final PropertyValuesHolder ofFloat3 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[] { 0.5f });
        final PropertyValuesHolder ofFloat4 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[] { 0.0f });
        set2.setDuration((long)(int)(0.6666667f * 150));
        set2.play((Animator)ObjectAnimator.ofPropertyValuesHolder(this.rightContainer, new PropertyValuesHolder[] { ofFloat2, ofFloat3, ofFloat4 }));
        set2.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                VerticalMenuComponent.this.rightContainer.setAlpha(0.0f);
                VerticalMenuComponent.this.rightContainer.setScaleX(1.0f);
                VerticalMenuComponent.this.rightContainer.setScaleY(1.0f);
                if (runnable != null) {
                    runnable.run();
                }
            }
        });
        set2.start();
    }
    
    public void performEnterAnimation(final Runnable runnable, final Runnable runnable2, final VerticalList.Model model) {
        VerticalMenuComponent.sLogger.v("performEnterAnimation");
        this.selectedImage.getLocationOnScreen(VerticalMenuComponent.location);
        this.selectedImage.setAlpha(0.0f);
        this.selectedText.setAlpha(0.0f);
        this.indicator.setAlpha(0.0f);
        this.animImageView.setScaleX(1.0f);
        this.animImageView.setScaleY(1.0f);
        this.animImageView.setImageBitmap((Bitmap)null);
        Bitmap fromCache = null;
        if (model.type != VerticalList.ModelType.ICON_OPTIONS) {
            fromCache = this.getFromCache(model);
        }
        this.verticalList.copyAndPosition(this.animImageView, this.animTitle, this.animSubTitle, this.animSubTitle2, fromCache == null);
        if (fromCache != null) {
            this.animImageView.setImageBitmap(fromCache);
        }
        else {
            this.addToCache(this.animImageView, model);
        }
        this.animImageView.setVisibility(View.VISIBLE);
        this.animTitle.setAlpha(1.0f);
        this.animTitle.setVisibility(View.VISIBLE);
        if (this.animSubTitle.getText().length() > 0) {
            this.animSubTitle.setAlpha(1.0f);
            this.animSubTitle.setVisibility(View.VISIBLE);
        }
        this.rightContainer.setAlpha(0.0f);
        final PropertyValuesHolder ofFloat = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[] { 1.2f });
        final PropertyValuesHolder ofFloat2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[] { 1.2f });
        final float n = VerticalViewHolder.selectedIconSize * -0.20000005f / 2.0f;
        final ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this.animImageView, new PropertyValuesHolder[] { ofFloat, ofFloat2, PropertyValuesHolder.ofFloat(View.X, new float[] { VerticalMenuComponent.location[0] - n }), PropertyValuesHolder.ofFloat(View.Y, new float[] { VerticalMenuComponent.location[1] - VerticalViewHolder.rootTopOffset - n }) });
        ofPropertyValuesHolder.setDuration((long)150);
        ofPropertyValuesHolder.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                VerticalMenuComponent.this.selectedImage.setAlpha(1.0f);
                VerticalMenuComponent.this.selectedText.setAlpha(1.0f);
                VerticalMenuComponent.this.animImageView.setVisibility(INVISIBLE);
                VerticalMenuComponent.this.animTitle.setVisibility(INVISIBLE);
                VerticalMenuComponent.this.animSubTitle.setVisibility(INVISIBLE);
                if (runnable2 != null) {
                    runnable2.run();
                }
            }
            
            @Override
            public void onAnimationStart(final Animator animator) {
                if (runnable != null) {
                    runnable.run();
                }
                VerticalMenuComponent.this.rightContainer.setPivotX(0.0f);
                VerticalMenuComponent.this.rightContainer.setPivotY((float)(VerticalMenuComponent.this.recyclerView.getMeasuredHeight() / 2));
                VerticalMenuComponent.this.rightContainer.setScaleX(0.5f);
                VerticalMenuComponent.this.rightContainer.setScaleY(0.5f);
            }
        });
        ofPropertyValuesHolder.start();
        this.animTitle.getLocationOnScreen(VerticalMenuComponent.location);
        final AnimatorSet set = new AnimatorSet();
        final AnimatorSet.Builder play = set.play((Animator)ObjectAnimator.ofPropertyValuesHolder(this.animTitle, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.ALPHA, new float[] { 0.0f }), PropertyValuesHolder.ofFloat(View.X, new float[] { this.animTitle.getX() - VerticalMenuComponent.animateTranslateY }) }));
        if (this.animSubTitle.getText().length() > 0) {
            play.with((Animator)ObjectAnimator.ofPropertyValuesHolder(this.animSubTitle, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.ALPHA, new float[] { 0.0f }), PropertyValuesHolder.ofFloat(View.X, new float[] { this.animSubTitle.getX() - VerticalMenuComponent.animateTranslateY }) }));
        }
        set.setDuration((long)50);
        set.start();
        final ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(this.selectedText, View.ALPHA, new float[] { 1.0f });
        ofFloat3.setDuration((long)50);
        ofFloat3.setStartDelay((long)(int)(0.6666667f * 150));
        ofFloat3.start();
        final AnimatorSet set2 = new AnimatorSet();
        final PropertyValuesHolder ofFloat4 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[] { 1.0f });
        final PropertyValuesHolder ofFloat5 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[] { 1.0f });
        final PropertyValuesHolder ofFloat6 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[] { 1.0f });
        set2.setStartDelay((long)50);
        set2.setDuration((long)(int)(0.6666667f * 150));
        set2.play((Animator)ObjectAnimator.ofPropertyValuesHolder(this.rightContainer, new PropertyValuesHolder[] { ofFloat4, ofFloat5, ofFloat6 }));
        set2.start();
    }
    
    public void performSelectionAnimation(final Runnable runnable) {
        this.performSelectionAnimation(runnable, 0);
    }
    
    public void performSelectionAnimation(final Runnable runnable, final int n) {
        VerticalMenuComponent.sLogger.v("performSelectionAnimation");
        final AnimatorSet set = new AnimatorSet();
        final AnimatorSet.Builder play = set.play((Animator)ObjectAnimator.ofFloat(this.leftContainer, View.ALPHA, new float[] { 0.0f }));
        play.with((Animator)ObjectAnimator.ofFloat(this.indicator, View.ALPHA, new float[] { 0.0f }));
        this.verticalList.addCurrentHighlightAnimation(play);
        set.setDuration(50L);
        set.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                final ViewPropertyAnimator withEndAction = VerticalMenuComponent.this.rightContainer.animate().alpha(0.0f).setDuration(150L).withEndAction(runnable);
                if (n > 0) {
                    withEndAction.setStartDelay((long)n);
                }
                withEndAction.start();
            }
        });
        set.start();
    }
    
    public void setLeftContainerWidth(final int width) {
        VerticalMenuComponent.sLogger.v("leftContainer width=" + width);
        ((ViewGroup.MarginLayoutParams)this.leftContainer.getLayoutParams()).width = width;
    }
    
    public void setOverrideDefaultKeyEvents(final InputManager.CustomKeyEvent customKeyEvent, final boolean b) {
        if (b) {
            this.overrideDefaultKeyEvents.add(customKeyEvent);
        }
        else {
            this.overrideDefaultKeyEvents.remove(customKeyEvent);
        }
    }
    
    public void setRightContainerWidth(final int width) {
        VerticalMenuComponent.sLogger.v("rightContainer width=" + width);
        ((ViewGroup.MarginLayoutParams)this.rightContainer.getLayoutParams()).width = width;
    }
    
    public void setSelectedIconColorImage(final int n, final int n2, final Shader shader, final float n3) {
        this.setSelectedIconColorImage(n, n2, shader, n3, IconColorImageView.IconShape.CIRCLE);
    }
    
    public void setSelectedIconColorImage(final int n, final int n2, final Shader shader, final float n3, final IconColorImageView.IconShape iconShape) {
        this.selectedIconColorImage.setIcon(n, n2, shader, n3);
        this.selectedIconColorImage.setVisibility(View.VISIBLE);
        this.selectedIconColorImage.setIconShape(iconShape);
        this.selectedIconImage.setVisibility(GONE);
        this.selectedCustomView.setVisibility(GONE);
    }
    
    public void setSelectedIconImage(final int n, final String s, final InitialsImageView.Style style) {
        this.selectedIconImage.setImage(n, s, style);
        this.selectedIconImage.setVisibility(View.VISIBLE);
        this.selectedIconColorImage.setVisibility(GONE);
        this.selectedCustomView.setVisibility(GONE);
    }
    
    public void setSelectedIconImage(final Bitmap imageBitmap) {
        this.selectedIconImage.setInitials(null, InitialsImageView.Style.DEFAULT);
        this.selectedIconImage.setImageBitmap(imageBitmap);
        this.selectedIconImage.setVisibility(View.VISIBLE);
        this.selectedIconColorImage.setVisibility(GONE);
        this.selectedCustomView.setVisibility(GONE);
    }
    
    public void showSelectedCustomView() {
        this.selectedCustomView.setVisibility(View.VISIBLE);
        this.selectedIconImage.setVisibility(GONE);
        this.selectedIconColorImage.setVisibility(GONE);
    }
    
    public void showToolTip(final int n, final String s) {
        this.toolTip.show(n, s);
    }
    
    public void unlock() {
        this.verticalList.unlock();
    }
    
    public void unlock(final boolean b) {
        this.verticalList.unlock(b);
    }
    
    public void updateView(final List<VerticalList.Model> list, final int n, final boolean b) {
        this.updateView(list, n, b, null);
    }
    
    public void updateView(final List<VerticalList.Model> list, final int n, final boolean b, final VerticalFastScrollIndex verticalFastScrollIndex) {
        this.updateView(list, n, b, false, verticalFastScrollIndex);
    }
    
    public void updateView(final List<VerticalList.Model> list, final int n, final boolean b, final boolean b2, final VerticalFastScrollIndex fastScrollIndex) {
        this.listLoaded = false;
        this.fastScrollIndex = null;
        if (fastScrollIndex != null) {
            final int size = list.size();
            if (size >= 40 && fastScrollIndex.getEntryCount() >= 2) {
                this.fastScrollIndex = fastScrollIndex;
                VerticalMenuComponent.sLogger.i("fast scroll available:" + size);
            }
            else {
                VerticalMenuComponent.sLogger.i("fast scroll threshold not met:" + size + "," + fastScrollIndex.getEntryCount());
            }
        }
        if (!b2) {
            this.verticalList.updateView(list, n, b);
        }
        else {
            this.verticalList.updateViewWithScrollableContent(list, n, b);
        }
    }
    
    public interface Callback
    {
        void close();
        
        boolean isClosed();
        
        boolean isItemClickable(final VerticalList.ItemSelectionState p0);
        
        void onBindToView(final VerticalList.Model p0, final View p1, final int p2, final VerticalList.ModelState p3);
        
        void onFastScrollEnd();
        
        void onFastScrollStart();
        
        void onItemSelected(final VerticalList.ItemSelectionState p0);
        
        void onLoad();
        
        void onScrollIdle();
        
        void select(final VerticalList.ItemSelectionState p0);
        
        void showToolTip();
    }
}
