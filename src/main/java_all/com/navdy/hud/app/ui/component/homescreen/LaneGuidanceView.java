package com.navdy.hud.app.ui.component.homescreen;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import com.squareup.otto.Subscribe;
import android.content.res.Resources;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.widget.ImageView;
import android.view.ViewGroup;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.hud.app.view.MainView;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.maps.MapEvents;
import com.squareup.otto.Bus;
import android.widget.LinearLayout;

public class LaneGuidanceView extends LinearLayout implements IHomeScreenLifecycle
{
    private Bus bus;
    private int iconH;
    private int iconW;
    private MapEvents.DisplayTrafficLaneInfo lastEvent;
    private Logger logger;
    private int mapIconIndicatorBottomMarginWithLane;
    private boolean needsMeasurement;
    private boolean paused;
    private int separatorColor;
    private int separatorH;
    private int separatorMargin;
    private int separatorW;
    private UIStateManager uiStateManager;
    
    public LaneGuidanceView(final Context context) {
        this(context, null);
    }
    
    public LaneGuidanceView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public LaneGuidanceView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    private void addSeparator(final Context context, final boolean b, final boolean b2) {
        final View view = new View(context);
        final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.separatorW, this.separatorH);
        layoutParams.gravity = 80;
        if (b) {
            layoutParams.leftMargin = this.separatorMargin;
        }
        if (b2) {
            layoutParams.rightMargin = this.separatorMargin;
        }
        view.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
        view.setBackgroundColor(this.separatorColor);
        this.addView(view, (ViewGroup.LayoutParams)layoutParams);
    }
    
    private int getMapIconX(final MainView.CustomAnimationMode customAnimationMode) {
        int n = 0;
        if (customAnimationMode != null) {
            switch (customAnimationMode) {
                default:
                    n = 0;
                    break;
                case SHRINK_LEFT:
                    n = HomeScreenResourceValues.iconIndicatorShrinkLeft_X;
                    break;
                case EXPAND:
                    n = HomeScreenResourceValues.iconIndicatorX;
                    break;
            }
        }
        else {
            final Main rootScreen = this.uiStateManager.getRootScreen();
            if (rootScreen.isNotificationViewShowing() || rootScreen.isNotificationExpanding()) {
                n = HomeScreenResourceValues.iconIndicatorShrinkLeft_X;
            }
            else {
                n = HomeScreenResourceValues.iconIndicatorX;
            }
        }
        return n;
    }
    
    private void hideMapIconIndicator() {
        this.uiStateManager.getHomescreenView().getLaneGuidanceIconIndicator().setVisibility(GONE);
    }
    
    private void showMapIconIndicator() {
        final ImageView laneGuidanceIconIndicator = this.uiStateManager.getHomescreenView().getLaneGuidanceIconIndicator();
        laneGuidanceIconIndicator.setX((float)this.getMapIconX(null));
        ((ViewGroup.MarginLayoutParams)laneGuidanceIconIndicator.getLayoutParams()).bottomMargin = this.mapIconIndicatorBottomMarginWithLane;
        laneGuidanceIconIndicator.setVisibility(View.VISIBLE);
    }
    
    public void getCustomAnimator(final MainView.CustomAnimationMode customAnimationMode, final AnimatorSet.Builder animatorSet$Builder) {
        if (this.getVisibility() == 0 && this.getMeasuredWidth() != 0) {
            final int measuredWidth = this.getMeasuredWidth();
            final int mapIconX = this.getMapIconX(customAnimationMode);
            animatorSet$Builder.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this, HomeScreenResourceValues.transformCenterIconWidth / 2 + mapIconX - measuredWidth / 2));
            animatorSet$Builder.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this.uiStateManager.getHomescreenView().getLaneGuidanceIconIndicator(), mapIconX));
        }
    }
    
    public void init(final HomeScreenView homeScreenView) {
        this.bus.register(this);
    }
    
    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        this.setY((float)HomeScreenResourceValues.laneGuidanceY);
        final RemoteDeviceManager instance = RemoteDeviceManager.getInstance();
        this.bus = instance.getBus();
        this.uiStateManager = instance.getUiStateManager();
        final Resources resources = this.getContext().getResources();
        this.iconH = resources.getDimensionPixelSize(R.dimen.lane_guidance_icon_h);
        this.iconW = resources.getDimensionPixelSize(R.dimen.lane_guidance_icon_w);
        this.separatorH = resources.getDimensionPixelSize(R.dimen.lane_guidance_separator_h);
        this.separatorW = resources.getDimensionPixelSize(R.dimen.lane_guidance_separator_w);
        this.separatorMargin = resources.getDimensionPixelSize(R.dimen.lane_guidance_separator_margin);
        this.separatorColor = resources.getColor(R.color.hud_white);
        this.mapIconIndicatorBottomMarginWithLane = resources.getDimensionPixelSize(R.dimen.map_icon_indicator_bottom_margin_with_lane);
    }
    
    @Subscribe
    public void onHideLaneInfo(final MapEvents.HideTrafficLaneInfo hideTrafficLaneInfo) {
        this.lastEvent = null;
        this.needsMeasurement = false;
        if (this.getVisibility() == 0) {
            this.logger.v("LaneGuidanceView:hideLanes");
            this.hideMapIconIndicator();
            this.setVisibility(GONE);
            this.removeAllViews();
        }
    }
    
    @Subscribe
    public void onLaneInfoShow(final MapEvents.DisplayTrafficLaneInfo displayTrafficLaneInfo) {
        if (this.uiStateManager.getHomescreenView().getDisplayMode() != HomeScreen.DisplayMode.MAP) {
            this.onHideLaneInfo(null);
            this.lastEvent = displayTrafficLaneInfo;
        }
        else {
            this.logger.v("LaneGuidanceView:showLanes");
            this.lastEvent = displayTrafficLaneInfo;
            if (!this.paused) {
                this.removeAllViews();
                final Context context = this.getContext();
                final int size = displayTrafficLaneInfo.laneData.size();
                this.addSeparator(context, false, true);
                for (int i = 0; i < size; ++i) {
                    final ImageView imageView = new ImageView(context);
                    imageView.setLayoutParams((ViewGroup.LayoutParams)new LinearLayout.LayoutParams(this.iconW, this.iconH));
                    final Drawable[] icons = displayTrafficLaneInfo.laneData.get(i).icons;
                    if (icons != null && icons.length > 0) {
                        imageView.setImageDrawable((Drawable)new LayerDrawable(icons));
                    }
                    else {
                        imageView.setImageResource(0);
                    }
                    this.addView((View)imageView);
                    if (i < size - 1) {
                        this.addSeparator(context, true, true);
                    }
                }
                this.addSeparator(context, true, false);
                this.showMapIconIndicator();
                this.needsMeasurement = true;
                this.setVisibility(View.VISIBLE);
            }
        }
    }
    
    protected void onMeasure(int mapIconX, int measuredWidth) {
        super.onMeasure(mapIconX, measuredWidth);
        if (this.needsMeasurement) {
            measuredWidth = this.getMeasuredWidth();
            if (measuredWidth > 0) {
                mapIconX = this.getMapIconX(null);
                this.setX((float)(HomeScreenResourceValues.transformCenterIconWidth / 2 + mapIconX - measuredWidth / 2));
                this.needsMeasurement = false;
            }
        }
    }
    
    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.logger.v("::onPause:lane");
        }
    }
    
    public void onResume() {
        if (this.paused) {
            this.paused = false;
            this.logger.v("::onResume:lane");
            if (this.uiStateManager.getHomescreenView().getDisplayMode() == HomeScreen.DisplayMode.MAP) {
                final MapEvents.DisplayTrafficLaneInfo lastEvent = this.lastEvent;
                if (lastEvent != null) {
                    this.lastEvent = null;
                    this.onLaneInfoShow(lastEvent);
                    this.logger.v("::onResume:shown last lane");
                }
            }
        }
    }
    
    public void setView(final MainView.CustomAnimationMode customAnimationMode) {
    }
    
    public void showLastEvent() {
        if (this.lastEvent != null) {
            this.onLaneInfoShow(this.lastEvent);
        }
    }
}
