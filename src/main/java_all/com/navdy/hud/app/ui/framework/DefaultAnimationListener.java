package com.navdy.hud.app.ui.framework;

import android.animation.Animator;
import android.animation.Animator;

public class DefaultAnimationListener implements Animator.AnimatorListener
{
    public void onAnimationCancel(final Animator animator) {
    }
    
    public void onAnimationEnd(final Animator animator) {
    }
    
    public void onAnimationRepeat(final Animator animator) {
    }
    
    public void onAnimationStart(final Animator animator) {
    }
}
