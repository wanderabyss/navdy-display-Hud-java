package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.view.ViewGroup;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.ImageView;
import android.view.LayoutInflater;
import android.os.Handler;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import android.view.ViewGroup;
import com.navdy.service.library.log.Logger;

public class ScrollableViewHolder extends VerticalViewHolder
{
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(ScrollableViewHolder.class);
    }
    
    public ScrollableViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        super(viewGroup, list, handler);
    }
    
    public static VerticalList.Model buildModel(final int scrollItemLayoutId) {
        final VerticalList.Model model = new VerticalList.Model();
        model.type = VerticalList.ModelType.SCROLL_CONTENT;
        model.id = R.id.vlist_scroll_item;
        model.scrollItemLayoutId = scrollItemLayoutId;
        return model;
    }
    
    public static ScrollableViewHolder buildViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        return new ScrollableViewHolder((ViewGroup)LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.vlist_scroll_item, viewGroup, false), list, handler);
    }
    
    @Override
    public void bind(final VerticalList.Model model, final VerticalList.ModelState modelState) {
    }
    
    @Override
    public void clearAnimation() {
    }
    
    @Override
    public void copyAndPosition(final ImageView imageView, final TextView textView, final TextView textView2, final TextView textView3, final boolean b) {
    }
    
    @Override
    public VerticalList.ModelType getModelType() {
        return VerticalList.ModelType.SCROLL_CONTENT;
    }
    
    @Override
    public void preBind(final VerticalList.Model model, final VerticalList.ModelState modelState) {
        if (this.layout.getChildCount() == 0) {
            this.layout.addView((View)LayoutInflater.from(this.layout.getContext()).inflate(model.scrollItemLayoutId, this.layout, false), (ViewGroup.LayoutParams)new FrameLayout.LayoutParams(-1, -2));
        }
    }
    
    @Override
    public void select(final VerticalList.Model model, final int n, final int n2) {
    }
    
    @Override
    public void setItemState(final State state, final AnimationType animationType, final int n, final boolean b) {
    }
}
