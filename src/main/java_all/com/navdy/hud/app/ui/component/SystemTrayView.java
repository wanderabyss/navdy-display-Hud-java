package com.navdy.hud.app.ui.component;

import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.view.View;
import butterknife.ButterKnife;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.util.AttributeSet;
import android.content.Context;
import butterknife.InjectView;
import android.widget.ImageView;
import android.widget.TextView;
import com.navdy.service.library.log.Logger;
import java.util.HashMap;

public class SystemTrayView extends LinearLayout
{
    static HashMap<State, Integer> dialStateResMap;
    static HashMap<State, Integer> phoneStateResMap;
    private static final Logger sLogger;
    private TextView debugGpsMarker;
    private State dialBattery;
    private boolean dialConnected;
    @InjectView(R.id.dial)
    ImageView dialImageView;
    @InjectView(R.id.gps)
    ImageView locationImageView;
    private boolean noNetwork;
    private State phoneBattery;
    private boolean phoneConnected;
    @InjectView(R.id.phone)
    ImageView phoneImageView;
    @InjectView(R.id.phoneNetwork)
    ImageView phoneNetworkImageView;
    
    static {
        sLogger = new Logger(SystemTrayView.class);
        SystemTrayView.phoneStateResMap = new HashMap<State, Integer>();
        (SystemTrayView.dialStateResMap = new HashMap<State, Integer>()).put(State.LOW_BATTERY, R.drawable.icon_dial_batterylow_2_copy);
        SystemTrayView.dialStateResMap.put(State.EXTREMELY_LOW_BATTERY, R.drawable.icon_dial_batterylow_copy);
        SystemTrayView.dialStateResMap.put(State.DISCONNECTED, R.drawable.icon_dial_missing_copy);
        SystemTrayView.phoneStateResMap.put(State.LOW_BATTERY, R.drawable.icon_device_batterylow_copy);
        SystemTrayView.phoneStateResMap.put(State.EXTREMELY_LOW_BATTERY, R.drawable.icon_device_batterylow_2_copy);
        SystemTrayView.phoneStateResMap.put(State.DISCONNECTED, R.drawable.icon_device_missing_copy);
    }
    
    public SystemTrayView(final Context context) {
        this(context, null);
    }
    
    public SystemTrayView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public SystemTrayView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.phoneBattery = State.OK_BATTERY;
        this.dialBattery = State.OK_BATTERY;
        this.init();
    }
    
    private void handleDialState(final State dialBattery) {
        boolean dialConnected = false;
        if (dialBattery == null) {
            this.dialImageView.setImageResource(0);
            this.dialImageView.setVisibility(GONE);
        }
        else {
            switch (dialBattery) {
                case CONNECTED:
                case DISCONNECTED:
                    if (dialBattery == State.CONNECTED) {
                        dialConnected = true;
                    }
                    if (this.dialConnected != dialConnected) {
                        this.dialConnected = dialConnected;
                        this.updateViews();
                        break;
                    }
                    break;
                case OK_BATTERY:
                case LOW_BATTERY:
                case EXTREMELY_LOW_BATTERY:
                    if (this.dialBattery != dialBattery) {
                        this.dialBattery = dialBattery;
                        this.updateViews();
                        break;
                    }
                    break;
            }
        }
    }
    
    private void handleGpsState(final State state) {
        switch (state) {
            case LOCATION_LOST:
                this.locationImageView.setVisibility(View.VISIBLE);
                this.setDebugGpsMarker(null);
                break;
            case LOCATION_AVAILABLE:
                this.locationImageView.setVisibility(GONE);
                break;
        }
    }
    
    private void handlePhoneState(final State phoneBattery) {
        boolean phoneConnected = true;
        if (phoneBattery == null) {
            this.phoneImageView.setImageResource(0);
            this.phoneImageView.setVisibility(GONE);
            this.phoneNetworkImageView.setVisibility(GONE);
        }
        else {
            switch (phoneBattery) {
                case CONNECTED:
                case DISCONNECTED:
                    if (phoneBattery != State.CONNECTED) {
                        phoneConnected = false;
                    }
                    if (this.phoneConnected != phoneConnected) {
                        this.phoneConnected = phoneConnected;
                        this.noNetwork = false;
                        this.phoneBattery = State.OK_BATTERY;
                        this.updateViews();
                        break;
                    }
                    break;
                case OK_BATTERY:
                case LOW_BATTERY:
                case EXTREMELY_LOW_BATTERY:
                    if (this.phoneBattery != phoneBattery) {
                        this.phoneBattery = phoneBattery;
                        this.updateViews();
                        break;
                    }
                    break;
                case PHONE_NETWORK_AVAILABLE:
                case NO_PHONE_NETWORK: {
                    final boolean noNetwork = phoneBattery == State.NO_PHONE_NETWORK;
                    if (this.noNetwork != noNetwork) {
                        this.noNetwork = noNetwork;
                        this.updateViews();
                        break;
                    }
                    break;
                }
            }
        }
    }
    
    private void init() {
        LayoutInflater.from(this.getContext()).inflate(R.layout.system_tray, (ViewGroup)this, true);
        this.setOrientation(0);
        ButterKnife.inject((View)this);
        this.dialImageView.setVisibility(GONE);
        this.phoneImageView.setVisibility(GONE);
        this.phoneNetworkImageView.setVisibility(GONE);
        this.locationImageView.setVisibility(GONE);
    }
    
    private void updateDeviceStatus(final ImageView imageView, final HashMap<State, Integer> hashMap, final State state) {
        final boolean b = false;
        final Integer n = hashMap.get(state);
        int intValue;
        if (n != null) {
            intValue = n;
        }
        else {
            intValue = 0;
        }
        imageView.setImageResource(intValue);
        int visibility;
        if (intValue != 0) {
            visibility = (b ? 1 : 0);
        }
        else {
            visibility = 8;
        }
        imageView.setVisibility(visibility);
    }
    
    private void updateViews() {
        final int n = 0;
        final ImageView phoneNetworkImageView = this.phoneNetworkImageView;
        int visibility;
        if (this.noNetwork && this.phoneConnected) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        phoneNetworkImageView.setVisibility(visibility);
        final HashMap<State, Integer> phoneStateResMap = SystemTrayView.phoneStateResMap;
        final State ok_BATTERY = State.OK_BATTERY;
        int n2 = n;
        if (this.noNetwork) {
            n2 = R.drawable.icon_device_battery_ok;
        }
        phoneStateResMap.put(ok_BATTERY, n2);
        final ImageView phoneImageView = this.phoneImageView;
        final HashMap<State, Integer> phoneStateResMap2 = SystemTrayView.phoneStateResMap;
        State state;
        if (this.phoneConnected) {
            state = this.phoneBattery;
        }
        else {
            state = State.DISCONNECTED;
        }
        this.updateDeviceStatus(phoneImageView, phoneStateResMap2, state);
        final ImageView dialImageView = this.dialImageView;
        final HashMap<State, Integer> dialStateResMap = SystemTrayView.dialStateResMap;
        State state2;
        if (this.dialConnected) {
            state2 = this.dialBattery;
        }
        else {
            state2 = State.DISCONNECTED;
        }
        this.updateDeviceStatus(dialImageView, dialStateResMap, state2);
    }

    public void setDebugGpsMarker(String s) {
        try {
            if (s != null) {
                if (this.debugGpsMarker == null) {
                    android.content.Context a = this.getContext();
                    this.debugGpsMarker = new android.widget.TextView(a);
                    this.debugGpsMarker.setTextAppearance(a, R.style.glance_title_3);
                    this.debugGpsMarker.setTextColor(-1);
                    LinearLayout.LayoutParams a0 = new LinearLayout.LayoutParams(-2, -2);
                    a0.gravity = 16;
                    this.addView(this.debugGpsMarker, 0, (ViewGroup.LayoutParams)a0);
                }
                this.debugGpsMarker.setText((CharSequence)s);
            } else if (this.debugGpsMarker != null) {
                this.removeView(this.debugGpsMarker);
                this.debugGpsMarker = null;
            }
        } catch(Throwable a1) {
            sLogger.e(a1);
        }
    }

    public void setState(final Device device, final State state) {
        switch (device) {
            case Phone:
                this.handlePhoneState(state);
                break;
            case Dial:
                this.handleDialState(state);
                break;
            case Gps:
                this.handleGpsState(state);
                break;
        }
    }
    
    public enum Device
    {
        Dial, 
        Gps, 
        Phone;
    }
    
    public enum State
    {
        CONNECTED, 
        DISCONNECTED, 
        EXTREMELY_LOW_BATTERY, 
        LOCATION_AVAILABLE, 
        LOCATION_LOST, 
        LOW_BATTERY, 
        NO_PHONE_NETWORK, 
        OK_BATTERY, 
        PHONE_NETWORK_AVAILABLE;
    }
}
