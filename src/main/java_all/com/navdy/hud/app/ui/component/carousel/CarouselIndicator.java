package com.navdy.hud.app.ui.component.carousel;

import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.maps.util.MapUtils;

public class CarouselIndicator extends FrameLayout {
    public static final int PROGRESS_INDICATOR_HORIZONTAL_THRESHOLD = 12;
    public static final int PROGRESS_INDICATOR_VERTICAL_THRESHOLD = 8;
    private int barParentSize = -1;
    private int barSize = -1;
    private int circleFocusSize;
    private int circleMargin;
    private int circleSize;
    private int currentItemPaddingRadius;
    private boolean fullBackground;
    private int itemCount;
    private int itemPadding;
    private int itemRadius;
    private Orientation orientation = Orientation.HORIZONTAL;
    private IProgressIndicator progressIndicator;
    private int roundRadius;
    private int viewPadding;

    public enum Orientation {
        HORIZONTAL,
        VERTICAL
    }

    public CarouselIndicator(Context context) {
        super(context, null);
        init(context, null);
    }

    public CarouselIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CarouselIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        Resources resources = context.getResources();
        this.circleSize = (int) resources.getDimension(R.dimen.carousel_circle_indicator_size);
        this.circleFocusSize = (int) resources.getDimension(R.dimen.carousel_circle_indicator_focus_size);
        this.circleMargin = (int) resources.getDimension(R.dimen.carousel_circle_indicator_margin);
        this.roundRadius = (int) resources.getDimension(R.dimen.carousel_progress_round_radius);
        this.itemRadius = (int) resources.getDimension(R.dimen.carousel_progress_item_radius);
        this.itemPadding = (int) resources.getDimension(R.dimen.carousel_progress_item_padding);
        this.currentItemPaddingRadius = (int) resources.getDimension(R.dimen.carousel_progress_currentitem_padding_radius);
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CarouselIndicator);
            this.circleSize = (int) typedArray.getDimension(0, (float) this.circleSize);
            this.circleFocusSize = (int) typedArray.getDimension(1, (float) this.circleFocusSize);
            this.circleMargin = (int) typedArray.getDimension(2, (float) this.circleMargin);
            this.roundRadius = (int) typedArray.getDimension(3, (float) this.roundRadius);
            this.itemRadius = (int) typedArray.getDimension(4, (float) this.itemRadius);
            this.itemPadding = (int) typedArray.getDimension(5, (float) this.itemPadding);
            this.currentItemPaddingRadius = (int) typedArray.getDimension(6, (float) this.currentItemPaddingRadius);
            this.fullBackground = typedArray.getBoolean(7, false);
            this.viewPadding = (int) typedArray.getDimension(8, 0.0f);
            this.barSize = (int) typedArray.getDimension(9, MapUtils.INVALID_DISTANCE);
            this.barParentSize = (int) typedArray.getDimension(10, MapUtils.INVALID_DISTANCE);
            typedArray.recycle();
        }
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public void setItemCount(int n) {
        if (n >= 0) {
            this.itemCount = n;
            int defaultColor = HudApplication.getAppContext().getResources().getColor(R.color.hud_white);
            LayoutParams lytParams;
            if (n > (this.orientation == Orientation.HORIZONTAL ? 12 : 8)) {
                if (this.progressIndicator == null || !(this.progressIndicator instanceof ProgressIndicator)) {
                    removeAllViews();
                    this.progressIndicator = new ProgressIndicator(getContext());
                    ((ProgressIndicator) this.progressIndicator).setProperties(this.roundRadius, this.itemRadius, this.itemPadding, this.currentItemPaddingRadius, defaultColor, -1, this.fullBackground, this.viewPadding, this.barSize, this.barParentSize);
                    this.progressIndicator.setOrientation(this.orientation);
                    Resources resources = HudApplication.getAppContext().getResources();
                    if (this.barSize == -1) {
                        int height;
                        int width;
                        if (this.orientation == Orientation.HORIZONTAL) {
                            height = (int) resources.getDimension(R.dimen.carousel_progress_indicator_h);
                            width = (int) resources.getDimension(R.dimen.carousel_progress_indicator_w);
                        } else {
                            width = (int) resources.getDimension(R.dimen.carousel_progress_indicator_vertical_w);
                            height = (int) resources.getDimension(R.dimen.carousel_progress_indicator_vertical_h);
                        }
                        lytParams = new LayoutParams(width, height);
                    } else {
                        lytParams = new LayoutParams(-1, -1);
                    }
                    lytParams.gravity = 17;
                    addView((View) this.progressIndicator, lytParams);
                }
                this.progressIndicator.setItemCount(n);
                return;
            }
            if (this.progressIndicator == null || !(this.progressIndicator instanceof CircleIndicator)) {
                removeAllViews();
                this.progressIndicator = new CircleIndicator(getContext());
                ((CircleIndicator) this.progressIndicator).setProperties(this.circleSize, this.circleFocusSize, this.circleMargin, defaultColor);
                this.progressIndicator.setOrientation(this.orientation);
                lytParams = new LayoutParams(-2, -2);
                lytParams.gravity = 17;
                addView((View) this.progressIndicator, lytParams);
            }
            this.progressIndicator.setItemCount(n);
        }
    }

    public void setCurrentItem(int n) {
        if (this.progressIndicator != null) {
            this.progressIndicator.setCurrentItem(n);
        }
    }

    public void setCurrentItem(int n, int color) {
        if (this.progressIndicator != null) {
            this.progressIndicator.setCurrentItem(n, color);
        }
    }

    public int getCurrentItem() {
        if (this.progressIndicator != null) {
            return this.progressIndicator.getCurrentItem();
        }
        return -1;
    }

    public int getItemCount() {
        return this.itemCount;
    }

    public AnimatorSet getItemMoveAnimator(int toPos, int color) {
        if (this.progressIndicator != null) {
            return this.progressIndicator.getItemMoveAnimator(toPos, color);
        }
        return null;
    }

    public RectF getItemPos(int n) {
        if (this.progressIndicator != null) {
            return this.progressIndicator.getItemPos(n);
        }
        return null;
    }
}
