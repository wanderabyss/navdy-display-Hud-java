package com.navdy.hud.app.ui.component.mainmenu;

import android.graphics.Bitmap;
import com.navdy.hud.app.framework.contacts.ContactImageHelper;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import org.jetbrains.annotations.NotNull;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import android.view.View;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.hud.app.ui.component.UISettings;
import com.navdy.hud.app.ui.component.vlist.viewholder.TitleViewHolder;
import java.util.ArrayList;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.location.Coordinate;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.service.library.events.ui.Screen;
import android.os.Parcelable;
import com.navdy.hud.app.framework.glympse.GlympseManager;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.here.HereMapsManager;
import android.os.Bundle;
import com.navdy.service.library.events.places.PlaceType;
import com.navdy.hud.app.ui.component.destination.DestinationParcelable;
import android.text.TextUtils;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.framework.contacts.Contact;
import java.util.List;
import com.squareup.otto.Bus;
import android.content.res.Resources;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.ui.component.vlist.VerticalList;

class ContactOptionsMenu implements IMenu
{
    private static final VerticalList.Model back;
    private static final int backColor;
    private static final int callColor;
    private static final Logger logger;
    private static final int messageColor;
    private static final Resources resources;
    private static final int shareLocationColor;
    private static final int shareTripLocationColor;
    private int backSelection;
    private int backSelectionId;
    private final Bus bus;
    private List<VerticalList.Model> cachedList;
    private final List<Contact> contacts;
    private boolean hasScrollModel;
    private MessagePickerMenu messagePickerMenu;
    private final String notifId;
    private Contact numberPicked;
    private final IMenu parent;
    private final MainMenuScreen2.Presenter presenter;
    private final VerticalMenuComponent vscrollComponent;
    
    static {
        logger = new Logger(ContactOptionsMenu.class);
        resources = HudApplication.getAppContext().getResources();
        backColor = ContactOptionsMenu.resources.getColor(R.color.mm_back);
        callColor = ContactOptionsMenu.resources.getColor(R.color.mm_contacts);
        shareTripLocationColor = ContactOptionsMenu.resources.getColor(R.color.mm_active_trip);
        shareLocationColor = ContactOptionsMenu.resources.getColor(R.color.share_location);
        messageColor = ContactOptionsMenu.resources.getColor(R.color.share_location);
        final String string = ContactOptionsMenu.resources.getString(R.string.back);
        final int backColor2 = ContactOptionsMenu.backColor;
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, backColor2, MainMenu.bkColorUnselected, backColor2, string, null);
    }
    
    ContactOptionsMenu(final List<Contact> list, final VerticalMenuComponent verticalMenuComponent, final MainMenuScreen2.Presenter presenter, final IMenu menu, final Bus bus) {
        this(list, null, verticalMenuComponent, presenter, menu, bus);
    }
    
    ContactOptionsMenu(final List<Contact> contacts, final String notifId, final VerticalMenuComponent vscrollComponent, final MainMenuScreen2.Presenter presenter, final IMenu parent, final Bus bus) {
        this.contacts = contacts;
        this.notifId = notifId;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
        this.bus = bus;
    }
    
    private String getContactName(final Contact contact) {
        String s;
        if (!TextUtils.isEmpty((CharSequence)contact.name)) {
            s = contact.name;
        }
        else if (!TextUtils.isEmpty((CharSequence)contact.formattedNumber)) {
            s = contact.formattedNumber;
        }
        else {
            s = "";
        }
        return s;
    }
    
    private DestinationParcelable getDestinationParcelable(final String s) {
        return new DestinationParcelable(0, s, null, false, null, false, null, 0.0, 0.0, 0.0, 0.0, R.drawable.icon_message, 0, ContactOptionsMenu.shareTripLocationColor, MainMenu.bkColorUnselected, DestinationParcelable.DestinationType.NONE, null);
    }
    
    private void launchShareTripMenu(final Contact contact) {
        final Bundle bundle = new Bundle();
        bundle.putInt("PICKER_DESTINATION_ICON", R.drawable.icon_pin_dot_destination_blue);
        bundle.putInt("PICKER_LEFT_ICON", R.drawable.icon_message);
        bundle.putInt("PICKER_LEFT_ICON_BKCOLOR", ContactOptionsMenu.resources.getColor(R.color.share_location_trip_color));
        final GeoCoordinate lastGeoCoordinate = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
        Coordinate destination = null;
        final NavigationRouteRequest currentNavigationRouteRequest = HereNavigationManager.getInstance().getCurrentNavigationRouteRequest();
        if (currentNavigationRouteRequest != null) {
            destination = currentNavigationRouteRequest.destination;
        }
        bundle.putBoolean("PICKER_SHOW_ROUTE_MAP", true);
        if (lastGeoCoordinate != null) {
            bundle.putDouble("PICKER_MAP_START_LAT", lastGeoCoordinate.getLatitude());
            bundle.putDouble("PICKER_MAP_START_LNG", lastGeoCoordinate.getLongitude());
        }
        final String contactName = this.getContactName(contact);
        if (destination != null) {
            bundle.putDouble("PICKER_MAP_END_LAT", (double)destination.latitude);
            bundle.putDouble("PICKER_MAP_END_LNG", (double)destination.longitude);
            bundle.putString("PICKER_TITLE", ContactOptionsMenu.resources.getString(R.string.share_your_trip_with, new Object[] { contactName }));
        }
        else {
            bundle.putString("PICKER_TITLE", ContactOptionsMenu.resources.getString(R.string.share_your_location_with, new Object[] { contactName }));
        }
        final String[] messages = GlympseManager.getInstance().getMessages();
        final DestinationParcelable[] array = new DestinationParcelable[messages.length];
        final String currentRouteId = HereNavigationManager.getInstance().getCurrentRouteId();
        for (int i = 0; i < messages.length; ++i) {
            array[i] = this.getDestinationParcelable(messages[i]);
            if (currentRouteId != null) {
                array[i].setRouteId(currentRouteId);
            }
        }
        bundle.putParcelableArray("PICKER_DESTINATIONS", (Parcelable[])array);
        this.presenter.close(new Runnable() {
            @Override
            public void run() {
                ContactOptionsMenu.this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_DESTINATION_PICKER, bundle, new ShareTripMenu(contact, ContactOptionsMenu.this.notifId), false));
            }
        });
    }
    
    private void makeCall(final Contact contact) {
        this.removeNotification();
        RemoteDeviceManager.getInstance().getCallManager().dial(contact.number, null, contact.name);
    }
    
    private void removeNotification() {
        if (this.notifId != null) {
            NotificationManager.getInstance().removeNotification(this.notifId);
        }
    }
    
    @Override
    public IMenu getChildMenu(final IMenu menu, final String s, final String s2) {
        return null;
    }
    
    @Override
    public int getInitialSelection() {
        int n2;
        final int n = n2 = 0;
        if (this.cachedList != null) {
            if (this.cachedList.isEmpty()) {
                n2 = n;
            }
            else {
                n2 = n;
                if (this.notifId == null) {
                    n2 = 1;
                }
            }
        }
        return n2;
    }
    
    @Override
    public List<VerticalList.Model> getItems() {
        List<VerticalList.Model> cachedList;
        if (this.cachedList != null) {
            cachedList = this.cachedList;
        }
        else {
            cachedList = new ArrayList<VerticalList.Model>();
            if (this.notifId == null) {
                cachedList.add(ContactOptionsMenu.back);
            }
            else {
                cachedList.add(TitleViewHolder.buildModel(ContactOptionsMenu.resources.getString(R.string.pick_reply)));
            }
            cachedList.add(IconBkColorViewHolder.buildModel(R.id.menu_call, R.drawable.icon_mm_contacts_2, ContactOptionsMenu.callColor, MainMenu.bkColorUnselected, ContactOptionsMenu.callColor, ContactOptionsMenu.resources.getString(R.string.call), null));
            final boolean synced = GlympseManager.getInstance().isSynced();
            ContactOptionsMenu.logger.v("glympse synced:" + synced);
            if (HereMapsManager.getInstance().isInitialized() && synced) {
                if (HereNavigationManager.getInstance().isNavigationModeOn()) {
                    cachedList.add(IconBkColorViewHolder.buildModel(R.id.menu_share_trip, R.drawable.icon_badge_active_trip, ContactOptionsMenu.shareTripLocationColor, MainMenu.bkColorUnselected, ContactOptionsMenu.shareTripLocationColor, ContactOptionsMenu.resources.getString(R.string.share_trip), null));
                }
                else {
                    cachedList.add(IconBkColorViewHolder.buildModel(R.id.menu_share_location, R.drawable.icon_share_location, ContactOptionsMenu.shareLocationColor, MainMenu.bkColorUnselected, ContactOptionsMenu.shareLocationColor, ContactOptionsMenu.resources.getString(R.string.share_location), null));
                }
            }
            final boolean b = false;
            final DeviceInfo remoteDeviceInfo = RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
            int n = b ? 1 : 0;
            if (remoteDeviceInfo != null) {
                switch (remoteDeviceInfo.platform) {
                    default:
                        n = (b ? 1 : 0);
                        break;
                    case PLATFORM_Android:
                        n = 1;
                        break;
                    case PLATFORM_iOS:
                        n = (b ? 1 : 0);
                        if (UISettings.supportsIosSms()) {
                            n = 1;
                            break;
                        }
                        break;
                }
            }
            if (n != 0) {
                cachedList.add(IconBkColorViewHolder.buildModel(R.id.menu_message, R.drawable.icon_message, ContactOptionsMenu.messageColor, MainMenu.bkColorUnselected, ContactOptionsMenu.messageColor, ContactOptionsMenu.resources.getString(R.string.message), null));
            }
            this.cachedList = cachedList;
        }
        return cachedList;
    }
    
    @Override
    public VerticalList.Model getModelfromPos(final int n) {
        VerticalList.Model model;
        if (this.cachedList != null && this.cachedList.size() > n) {
            model = this.cachedList.get(n);
        }
        else {
            model = null;
        }
        return model;
    }
    
    @Override
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    @Override
    public Menu getType() {
        return Menu.CONTACT_OPTIONS;
    }
    
    @Override
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    @Override
    public boolean isFirstItemEmpty() {
        return this.notifId == null;
    }
    
    @Override
    public boolean isItemClickable(final int n, final int n2) {
        return true;
    }
    
    @Override
    public void onBindToView(final VerticalList.Model model, final View view, final int n, final VerticalList.ModelState modelState) {
    }
    
    @Override
    public void onFastScrollEnd() {
    }
    
    @Override
    public void onFastScrollStart() {
    }
    
    @Override
    public void onItemSelected(final VerticalList.ItemSelectionState itemSelectionState) {
    }
    
    @Override
    public void onScrollIdle() {
    }
    
    @Override
    public void onUnload(final MenuLevel menuLevel) {
    }
    
    @Override
    public boolean selectItem(final VerticalList.ItemSelectionState itemSelectionState) {
        ContactOptionsMenu.logger.v("select id:" + itemSelectionState.id + " pos:" + itemSelectionState.pos);
        final int pos = itemSelectionState.pos;
        final Contact contact = this.contacts.get(0);
        switch (itemSelectionState.id) {
            case R.id.menu_back:
                ContactOptionsMenu.logger.v("back");
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId, this.hasScrollModel);
                this.backSelectionId = 0;
                break;
            case R.id.menu_call:
                ContactOptionsMenu.logger.v("call contact");
                if (this.contacts.size() == 1) {
                    AnalyticsSupport.recordMenuSelection("call_contact");
                    this.presenter.performSelectionAnimation(new Runnable() {
                        @Override
                        public void run() {
                            ContactOptionsMenu.this.presenter.close(new Runnable() {
                                @Override
                                public void run() {
                                    ContactOptionsMenu.this.makeCall(contact);
                                }
                            });
                        }
                    });
                    break;
                }
                this.presenter.loadMenu(new NumberPickerMenu(this.vscrollComponent, this.presenter, this, NumberPickerMenu.Mode.CALL, this.contacts, null, R.drawable.icon_mm_contacts_2, ContactOptionsMenu.callColor, (NumberPickerMenu.Callback)new NumberPickerMenu.Callback() {
                    @Override
                    public void selected(@NotNull final Contact contact) {
                        ContactOptionsMenu.this.makeCall(contact);
                    }
                }), MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                break;
            case R.id.menu_share_trip:
                ContactOptionsMenu.logger.v("share trip");
                AnalyticsSupport.recordMenuSelection("share_trip");
                if (this.contacts.size() == 1) {
                    this.launchShareTripMenu(contact);
                    break;
                }
                this.presenter.loadMenu(new NumberPickerMenu(this.vscrollComponent, this.presenter, this, NumberPickerMenu.Mode.SHARE_TRIP, this.contacts, null, R.drawable.icon_badge_active_trip, ContactOptionsMenu.shareTripLocationColor, (NumberPickerMenu.Callback)new NumberPickerMenu.Callback() {
                    @Override
                    public void selected(@NotNull final Contact contact) {
                        ContactOptionsMenu.this.launchShareTripMenu(contact);
                    }
                }), MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                break;
            case R.id.menu_share_location:
                ContactOptionsMenu.logger.v("share location");
                AnalyticsSupport.recordMenuSelection("share_location");
                if (this.contacts.size() == 1) {
                    this.launchShareTripMenu(contact);
                    break;
                }
                this.presenter.loadMenu(new NumberPickerMenu(this.vscrollComponent, this.presenter, this, NumberPickerMenu.Mode.SHARE_TRIP, this.contacts, null, R.drawable.icon_share_location, ContactOptionsMenu.shareLocationColor, (NumberPickerMenu.Callback)new NumberPickerMenu.Callback() {
                    @Override
                    public void selected(@NotNull final Contact contact) {
                        ContactOptionsMenu.this.launchShareTripMenu(contact);
                    }
                }), MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                break;
            case R.id.menu_message:
                ContactOptionsMenu.logger.v("send message");
                AnalyticsSupport.recordMenuSelection("send_message");
                if (this.contacts.size() == 1) {
                    if (this.messagePickerMenu == null) {
                        this.messagePickerMenu = new MessagePickerMenu(this.vscrollComponent, this.presenter, this, GlanceConstants.getCannedMessages(), contact, this.notifId);
                    }
                    this.presenter.loadMenu(this.messagePickerMenu, MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                    break;
                }
                this.presenter.loadMenu(new NumberPickerMenu(this.vscrollComponent, this.presenter, this, NumberPickerMenu.Mode.MESSAGE, this.contacts, this.notifId, R.drawable.icon_message, ContactOptionsMenu.messageColor, (NumberPickerMenu.Callback)new NumberPickerMenu.Callback() {
                    @Override
                    public void selected(@NotNull final Contact contact) {
                    }
                }), MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                break;
        }
        return true;
    }
    
    @Override
    public void setBackSelectionId(final int backSelectionId) {
        this.backSelectionId = backSelectionId;
    }
    
    @Override
    public void setBackSelectionPos(final int backSelection) {
        this.backSelection = backSelection;
    }
    
    public void setScrollModel(final boolean hasScrollModel) {
        this.hasScrollModel = hasScrollModel;
    }
    
    @Override
    public void setSelectedIcon() {
        final Contact contact = this.contacts.get(0);
        if (this.contacts.size() == 1) {
            final Bitmap bitmapfromCache = PicassoUtil.getBitmapfromCache(PhoneImageDownloader.getInstance().getImagePath(contact.number, PhotoType.PHOTO_CONTACT));
            if (bitmapfromCache != null) {
                this.vscrollComponent.setSelectedIconImage(bitmapfromCache);
            }
            else if (TextUtils.isEmpty((CharSequence)contact.name)) {
                this.vscrollComponent.setSelectedIconImage(R.drawable.icon_user_bg_4, null, InitialsImageView.Style.LARGE);
            }
            else {
                this.vscrollComponent.setSelectedIconImage(ContactImageHelper.getInstance().getResourceId(contact.defaultImageIndex), contact.initials, InitialsImageView.Style.LARGE);
            }
        }
        else {
            final ContactImageHelper instance = ContactImageHelper.getInstance();
            this.vscrollComponent.setSelectedIconImage(instance.getResourceId(instance.getContactImageIndex(contact.name)), contact.initials, InitialsImageView.Style.LARGE);
        }
        if (TextUtils.isEmpty((CharSequence)contact.name)) {
            this.vscrollComponent.selectedText.setText((CharSequence)contact.formattedNumber);
        }
        else if (this.contacts.size() == 1 && !TextUtils.isEmpty((CharSequence)contact.numberTypeStr)) {
            this.vscrollComponent.selectedText.setText((CharSequence)(contact.name + "\n" + contact.numberTypeStr));
        }
        else {
            this.vscrollComponent.selectedText.setText((CharSequence)contact.name);
        }
    }
    
    @Override
    public void showToolTip() {
    }
}
