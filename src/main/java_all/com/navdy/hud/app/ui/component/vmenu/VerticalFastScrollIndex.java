package com.navdy.hud.app.ui.component.vmenu;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Arrays;
import java.util.Map;

public class VerticalFastScrollIndex
{
    private final String[] entries;
    public final int length;
    private int[] offsetIndex;
    private final Map<String, Integer> offsetMap;
    private final int positionOffset;
    
    private VerticalFastScrollIndex(final String[] entries, final Map<String, Integer> offsetMap, final int[] offsetIndex, final int positionOffset) {
        this.entries = entries;
        this.offsetMap = offsetMap;
        this.offsetIndex = offsetIndex;
        this.positionOffset = positionOffset;
        this.length = entries.length;
    }
    
    public int getEntryCount() {
        return this.entries.length;
    }
    
    public int getIndexForPosition(int binarySearch) {
        final int n = binarySearch = Arrays.binarySearch(this.offsetIndex, 0, this.offsetIndex.length, binarySearch);
        if (n < 0) {
            final int n2 = -n - 1;
            if ((binarySearch = n2) > 0) {
                binarySearch = n2 - 1;
            }
        }
        return binarySearch;
    }
    
    public int getOffset() {
        return this.positionOffset;
    }
    
    public int getPosition(final String s) {
        return this.offsetMap.get(s);
    }
    
    public String getTitle(final int n) {
        return this.entries[n];
    }
    
    public static class Builder
    {
        private LinkedHashMap<String, Integer> map;
        private int positionOffset;
        
        public Builder() {
            this.map = new LinkedHashMap<String, Integer>();
        }
        
        public VerticalFastScrollIndex build() {
            final int size = this.map.size();
            final String[] array = new String[size];
            final int[] array2 = new int[size];
            final Iterator<String> iterator = this.map.keySet().iterator();
            int n = 0;
            while (iterator.hasNext()) {
                final String s = iterator.next();
                array[n] = s;
                array2[n] = this.map.get(s);
                ++n;
            }
            return new VerticalFastScrollIndex(array, this.map, array2, this.positionOffset, null);
        }
        
        public Builder positionOffset(final int positionOffset) {
            this.positionOffset = positionOffset;
            return this;
        }
        
        public Builder setEntry(final char c, final int n) {
            this.map.put(String.valueOf(c), n);
            return this;
        }
    }
}
