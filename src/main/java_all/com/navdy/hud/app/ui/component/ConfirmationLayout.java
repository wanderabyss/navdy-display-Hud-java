package com.navdy.hud.app.ui.component;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import com.navdy.hud.app.manager.InputManager;
import android.view.LayoutInflater;
import android.os.Looper;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.TextView;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.os.Handler;
import android.widget.RelativeLayout;

public class ConfirmationLayout extends RelativeLayout
{
    public ChoiceLayout choiceLayout;
    private ChoiceLayout.IListener choiceListener;
    private ChoiceLayout.IListener defaultChoiceListener;
    private boolean finished;
    public FluctuatorAnimatorView fluctuatorView;
    private Handler handler;
    public ImageView leftSwipe;
    private Listener listener;
    public ViewGroup mainSection;
    public ImageView rightSwipe;
    public InitialsImageView screenImage;
    public IconColorImageView screenImageIconBkColor;
    public TextView screenTitle;
    public ImageView sideImage;
    private int timeout;
    private Runnable timeoutRunnable;
    public TextView title1;
    public TextView title2;
    public TextView title3;
    public TextView title4;
    public ViewGroup titleContainer;
    
    public ConfirmationLayout(final Context context) {
        this(context, null);
    }
    
    public ConfirmationLayout(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public ConfirmationLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.handler = new Handler(Looper.getMainLooper());
        this.defaultChoiceListener = new ChoiceLayout.IListener() {
            @Override
            public void executeItem(final int n, final int n2) {
                ConfirmationLayout.this.clearTimeout();
                ConfirmationLayout.this.choiceListener.executeItem(n, n2);
            }
            
            @Override
            public void itemSelected(final int n, final int n2) {
                ConfirmationLayout.this.choiceListener.itemSelected(n, n2);
            }
        };
        this.timeoutRunnable = new Runnable() {
            @Override
            public void run() {
                ConfirmationLayout.this.finished = true;
                ConfirmationLayout.this.listener.timeout();
            }
        };
        this.init();
    }
    
    private void clearTimeout() {
        this.handler.removeCallbacks(this.timeoutRunnable);
    }
    
    private void init() {
        LayoutInflater.from(this.getContext()).inflate(R.layout.confirmation_lyt, (ViewGroup)this, true);
        this.screenTitle = (TextView)this.findViewById(R.id.mainTitle);
        this.screenImage = (InitialsImageView)this.findViewById(R.id.image);
        this.screenImageIconBkColor = (IconColorImageView)this.findViewById(R.id.imageIconBkColor);
        this.sideImage = (ImageView)this.findViewById(R.id.sideImage);
        this.titleContainer = (ViewGroup)this.findViewById(R.id.infoContainer);
        this.title1 = (TextView)this.findViewById(R.id.title1);
        this.title2 = (TextView)this.findViewById(R.id.title2);
        this.title3 = (TextView)this.findViewById(R.id.title3);
        this.title4 = (TextView)this.findViewById(R.id.title4);
        this.choiceLayout = (ChoiceLayout)this.findViewById(R.id.choiceLayout);
        this.fluctuatorView = (FluctuatorAnimatorView)this.findViewById(R.id.fluctuator);
        this.leftSwipe = (ImageView)this.findViewById(R.id.leftSwipe);
        this.rightSwipe = (ImageView)this.findViewById(R.id.rightSwipe);
        this.mainSection = (ViewGroup)this.findViewById(R.id.mainSection);
    }
    
    private void resetTimeout() {
        if (this.timeout > 0) {
            this.handler.removeCallbacks(this.timeoutRunnable);
            this.handler.postDelayed(this.timeoutRunnable, (long)this.timeout);
        }
    }
    
    public void executeSelectedItem(final boolean b) {
        if (!this.finished) {
            this.resetTimeout();
            this.choiceLayout.executeSelectedItem(b);
        }
    }
    
    public boolean handleKey(final InputManager.CustomKeyEvent customKeyEvent) {
        boolean b = true;
        switch (customKeyEvent) {
            default:
                b = false;
                break;
            case LEFT:
                this.moveSelectionLeft();
                break;
            case RIGHT:
                this.moveSelectionRight();
                break;
            case SELECT:
                this.executeSelectedItem(true);
                break;
        }
        return b;
    }
    
    public void moveSelectionLeft() {
        if (!this.finished) {
            this.resetTimeout();
            this.choiceLayout.moveSelectionLeft();
        }
    }
    
    public void moveSelectionRight() {
        if (!this.finished) {
            this.resetTimeout();
            this.choiceLayout.moveSelectionRight();
        }
    }
    
    public void setChoices(final List<String> list, final int n, final ChoiceLayout.IListener choiceListener) {
        this.finished = false;
        this.clearTimeout();
        if (list == null || list.size() == 0 || choiceListener == null) {
            throw new IllegalArgumentException();
        }
        this.choiceListener = choiceListener;
        final ArrayList<ChoiceLayout.Choice> list2 = new ArrayList<ChoiceLayout.Choice>();
        final Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(new ChoiceLayout.Choice(iterator.next(), 0));
        }
        this.choiceLayout.setChoices(ChoiceLayout.Mode.LABEL, list2, n, this.defaultChoiceListener);
    }
    
    public void setChoicesList(final List<ChoiceLayout.Choice> list, final int n, final ChoiceLayout.IListener choiceListener) {
        this.finished = false;
        this.clearTimeout();
        if (list == null || list.size() == 0 || choiceListener == null) {
            throw new IllegalArgumentException();
        }
        this.choiceListener = choiceListener;
        this.choiceLayout.setChoices(ChoiceLayout.Mode.LABEL, list, n, this.defaultChoiceListener);
    }
    
    public void setTimeout(final int timeout, final Listener listener) {
        if (!this.finished) {
            if (listener == null || timeout <= 0) {
                throw new IllegalArgumentException();
            }
            this.timeout = timeout;
            this.listener = listener;
            this.resetTimeout();
        }
    }
    
    public interface Listener
    {
        void timeout();
    }
}
