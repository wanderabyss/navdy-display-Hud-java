package com.navdy.hud.app.ui.component.carousel;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.Animator.AnimatorListener;

public interface AnimationStrategy
{
    AnimatorSet buildLayoutAnimation(final Animator.AnimatorListener p0, final CarouselLayout p1, final int p2, final int p3);
    
    AnimatorSet createHiddenViewAnimation(final CarouselLayout p0, final Direction p1);
    
    AnimatorSet createMiddleLeftViewAnimation(final CarouselLayout p0, final Direction p1);
    
    AnimatorSet createMiddleRightViewAnimation(final CarouselLayout p0, final Direction p1);
    
    AnimatorSet createNewMiddleRightViewAnimation(final CarouselLayout p0, final Direction p1);
    
    AnimatorSet createSideViewToMiddleAnimation(final CarouselLayout p0, final Direction p1);
    
    Animator createViewOutAnimation(final CarouselLayout p0, final Direction p1);
    
    public enum Direction
    {
        LEFT, 
        RIGHT;
    }
}
